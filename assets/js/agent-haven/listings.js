$(document).ready(function() {
    var a = function(a) {
            a.preventDefault(), href = base_url + "home/home/save_favorates_properties";
            var b = $(this).data("pro-id");
            if ("" == href) return !1;
            var c = href + "/" + b;
            $(".save-favorate").attr("href", c), $.ajax({
                url: c,
                type: "post",
                dataType: "json",
                success: function(a) {
                    console.log(a), a.success ? (console.log(b), $("#isSaveFavorate_" + b).html("<i class='fa fa-2x fa-heart'></i>"), $("#isSavedFavorate_" + b).html("<small>Saved</small>")) : window.location.href = a.redirect
                }
            })
        },
        // b = function(a) {
        //     a.preventDefault();
        //     var b = $(this).attr("href");
        //     $.ajax({
        //         url: b,
        //         type: "post",
        //         dataType: "json",
        //         success: function(a) {
        //             console.log(a), a.success ? $(".follow-search").html("<i class='fa fa-undo'></i> Saved Search").prop("disable", !0) : window.location.href = a.redirect
        //         }
        //     }), $(".city-search").selectize({
        //         maxOptions: 5,
        //         maxItems: 1,
        //         sortField: {
        //             field: "text",
        //             direction: "asc"
        //         }
        //     })
        // },
        // c = function(a) {
        //     a.preventDefault();
        //     var b = $(this),
        //         c = b.attr("action"),
        //         d = b.serialize();
        //     $(".submit-question-button").text("Loading..."), $.ajax({
        //         url: c,
        //         type: "post",
        //         data: d,
        //         dataType: "json",
        //         success: function(a) {
        //             console.log(a), $(".question-mess").html(a.message), $(".submit-question-button").text("Submit")
        //         }
        //     })
        // },
        d = function(a) {
            a.preventDefault();
            var b = $(this),
                c = b.attr("action"),
                d = b.serialize();
            $.ajax({
                url: c,
                type: "post",
                data: d,
                dataType: "json",
                beforeSend: function() {
                    $("#sbt-showing").prop("disabled", !0), $("#sbt-showing").text("Loading...")
                },
                success: function(a) {
                    console.log(a), $("#resultInsert").show(), $("#resultInsert").fadeIn("slow", function() {
                        $("#resultInsert").html("<i class='fa fa-info-circle'></i>" + a.message), $("#sbt-showing").prop("disabled", !1), $("#sbt-showing").text("Submit")
                    })
                }
            })
        };
    // $(".save-favorate").on("click", a), $(".follow-search").on("click", b), $(".customer_questions").on("submit", c), $(".sched_showing").on("submit", d)
    $(".save-favorate").on("click", a),$(".sched_showing").on("submit", d)

    //Submit Customer questions
    $(".customer_questions").off('submit').on('submit', function(e) {
	    e.preventDefault();
	   		var b = $(this),
                c = b.attr("action"),
                d = b.serialize();
            $(".submit-question-button").text("Loading..."), $.ajax({
                url: c,
                type: "post",
                data: d,
                dataType: "json",
                success: function(a) {
                    console.log(a), $(".question-mess").html(a.message), $(".submit-question-button").text("Submit")
                }
            })
	});


    //Save Saved Searches
    $(".follow-search").off('click').on('click', function(e) {

            e.preventDefault();
            var b = $(this).attr("href");
            $.ajax({
                url: b,
                type: "post",
                dataType: "json",
                success: function(a) {
                    console.log(a), a.success ? $(".follow-search").html("<i class='fa fa-undo'></i> Saved Search").prop("disable", !0) : window.location.href = a.redirect
                }
            })
    });

});