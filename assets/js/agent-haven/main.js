function testFunction(a) {
    console.log(a)
}

$(".featured_listing").on("click", function() {
    console.log($(this).attr("data-listingId"));
    console.log($(this).attr("data-propertyType"));
});

$("#showLoginTrigger").click(function() {
    $("#loginForm").show();
    $("#signupForm").hide();
    $("#resetPhoneForm").hide();
});

$("#showSignupTrigger").click(function() {
    $("#loginForm").hide();
    $("#resetPhoneForm").hide();
    $("#signupForm").show();
});

$("#showResetTrigger").click(function() {
    $("#loginForm").hide();
    $("#signupForm").hide();
    $("#resetPhoneForm").show();
});

$("#signupForm").on("submit", function(e) {
    e.preventDefault();

    $dataString = $(this).serialize();
    console.log($dataString);
    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $dataString,
        dataType: 'json',
        beforeSend: function() {
            console.log('sending');
        },
        success: function(data) {
            if(data.success) {
                console.log(data);
                window.location.href = data.redirect;
            } else {
                $(".sign-up-message").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
            }
        }
    });
});

$("#loginForm").on("submit", function(e) {
    e.preventDefault();

    $dataString = $(this).serialize();
    console.log($dataString);

    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $dataString,
        dataType: 'json',
        beforeSend: function() {
            console.log('sending');
        },
        success: function(data) {
            if(data.success) {
                console.log(data);
                window.location.href = data.redirect;
            } else {
                $(".sign-up-message").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
            }
        }
    });
});

$("#resetPhoneForm").on("submit", function(e) {
    e.preventDefault();

    $dataString = $(this).serialize();
    console.log($dataString);

    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $dataString,
        dataType: 'json',
        beforeSend: function() {
            console.log('sending');
        },
        success: function(data) {
            if(data.success) {
                console.log(data);
            } else {
                $(".sign-up-message").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
            }
        }
    });
});

$(window).load(function()
{
   var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];

    $('.phone_number').inputmask({ 
        mask: phones, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
});

function setFieldValue(a, b) {
    $(document).ready(function() {
        "Any" === b ? ($("#" + a).val(""), $("#" + a + "_label").html(b)) : ($("#" + a).val(b), $("#" + a + "_label").html(b + "+"))
    })
}

function addCommas(a) {
    a += "", x = a.split("."), x1 = x[0], x2 = x.length > 1 ? "." + x[1] : "";
    for (var b = /(\d+)(\d{3})/; b.test(x1);) x1 = x1.replace(b, "$1,$2");
    return x1 + x2
}

function calculate() {
    var a = $("#mortgageCalculator").serialize(),
        b = $("input[name='calculator_url']").val();
    $.ajax({
        url: b,
        type: "POST",
        data: a,
        dataType: "json",
        beforeSend: function() {
            console.log(a), 
            console.log("sending data"), 
            $("#morg_calc_btn").text("Calculating...")
        },
        success: function(a) {
            $("#mortgage-result").html("<h4 class='total-mortgage'>Monthly: $" + a.monthly_payment + "</h4>"), $("#morg_calc_btn").text("Calculate")
        }
    })
}

function customer_fb_login() {
    var a = screen.width / 6,
        b = screen.height / 6;
    window.open(customer_fb_url, "targetWindow", "toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top=" + b + ",left=" + a)
}

function modal_fb_login() {
    var a = screen.width / 6,
        b = screen.height / 6;
    window.open(modal_fb_url, "targetWindow", "toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top=" + b + ",left=" + a)
}

$(document).ready(function() {

    $("#saved-searches").owlCarousel({
        navigation : true,
        pagination : true,
        slideSpeed : 700,
        paginationSpeed : 400,
        navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"],
        items : 6,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });

    //property-detail
    var photosync1 = $("#photosync1");
    var photosync2 = $("#photosync2");
         
    photosync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });
    
    photosync2.owlCarousel({
        items : 5,
        itemsDesktop      : [1199,10],
        itemsDesktopSmall     : [979,10],
        itemsTablet       : [768,8],
        itemsMobile       : [479,4],
        navigation: true,
        pagination:false,
        navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"],
        responsiveRefreshRate : 100,
        afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
        }
    });
         
    function syncPosition(el) {

        var current = this.currentItem;
        $("#photosync2")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")

        if($("#photosync2").data("owlCarousel") !== undefined) {
          center(current)
        }
    }    
    
    $("#photosync2").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        photosync1.trigger("owl.goTo",number);
    });
         
    function center(number) {
        var photosync2visible = photosync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;

        for(var i in photosync2visible){
            if(num === photosync2visible[i]){
                var found = true;
            }
        }
         
        if(found===false){
            if(num>photosync2visible[photosync2visible.length-1]){
                photosync2.trigger("owl.goTo", num - photosync2visible.length+2)
            } else {
                if(num - 1 === -1){
                    num = 0;
                }
                photosync2.trigger("owl.goTo", num);
            }
        } else if(num === photosync2visible[photosync2visible.length-1]){
            photosync2.trigger("owl.goTo", photosync2visible[1])
        } else if(num === photosync2visible[0]){
            photosync2.trigger("owl.goTo", num-1)
        }
            
    }

    $('.city-search').selectize({
        maxOptions:10,
        maxItems:5,
        openOnFocus:false,
        sortField: {
            field: 'text',
            direction: 'asc'
        }
    });
    
    function f(a) {
        $(a.target).prev(".panel-heading").find("i.indicator").toggleClass("glyphicon-chevron-down glyphicon-chevron-up")
    }
    $("#mortgageCalculator").on("submit", function(a) {
        calculate(), a.preventDefault()
    }), $("#sale_price").on("input", function() {
        $("#sale_price").val() && $("#sale_price_label").html(addCommas($("#sale_price").val()))
    }), $("#down_percent").on("input", function() {
        $("#down_percent").val() && $("#down_percent_label").html($("#down_percent").val())
    }), $("#mortgage_interest_percent").on("input", function() {
        $("#mortgage_interest_percent").val() && $("#mortgage_interest_percent_label").html($("#mortgage_interest_percent").val())
    }), $("#year_term").on("input", function() {
        $("#year_term").val() && $("#year_term_label").html($("#year_term").val())
    });
    var a = 0;
    $("#A").click(function() {
        $(this).is(":checked") ? ($("#type_a").css("display", "block"), a += 1) : ($("#type_a").css("display", "none"), $("input[name='a_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#B").click(function() {
        $(this).is(":checked") ? ($("#type_b").css("display", "block"), a += 1) : ($("#type_b").css("display", "none"), $("input[name='b_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $(window).scroll(function(a) {
        var b = $(this).scrollTop();
        b > 25 ? ($(".home-navbar").addClass("nav-lighten"), $(".main-nav").removeClass("nav-darken")) : ($(".home-navbar").removeClass("nav-lighten"), $(".main-nav").addClass("nav-darken")), b > 100 ? $(".scrolltop").fadeIn() : $(".scrolltop").fadeOut()
    }), $(".scrolltop").click(function() {
        return $("html, body").animate({
            scrollTop: 0
        }, 600), !1
    }), $("#C").click(function() {
        $(this).is(":checked") ? ($("#type_c").css("display", "block"), a += 1) : ($("#type_c").css("display", "none"), $("input[name='c_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#D").click(function() {
        $(this).is(":checked") ? ($("#type_d").css("display", "block"), a += 1) : ($("#type_d").css("display", "none"), $("input[name='d_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#E").click(function() {
        $(this).is(":checked") ? ($("#type_e").css("display", "block"), a += 1) : ($("#type_e").css("display", "none"), $("input[name='e_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#F").click(function() {
        $(this).is(":checked") ? ($("#type_f").css("display", "block"), a += 1) : ($("#type_f").css("display", "none"), $("input[name='f_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#A, #B, #C, #D, #E, #F").on("click", function() {
        a > 1 ? ($("#property_sub_type_container").css("display", "none"), $("input[name='a_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='b_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='c_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='d_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='e_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='f_type[]']").each(function() {
            $(this).prop("checked", !1)
        })) : 0 == a ? $("#property_sub_type_container").css("display", "none") : $("#property_sub_type_container").css("display", "block")
    }), $("#more-property-types").click(function() {
        $(".show-more-property-types").toggle()
    }), $(".moreFiltersToggle").click(function() {
        $("#more-filters-area").toggle()
    }), $(".dropdown-toggle").click(function() {
        $("#more-filters-area").toggle(), "block" == $("#more-filters-area").css("display") && $("#more-filters-area").hide()
    }), $("#min_price").on("input", function() {
        $("#min_price").val() && $("#price_label").html(addCommas($("#min_price").val())), console.log("Value: " + $("#min_price").val())
    }), $("#max_price").on("input", function() {
        $("#min_price").val() && $("#max_price").val() ? $("#price_label").html(addCommas($("#min_price").val()) + " - " + addCommas($("#max_price").val())) : $("#price_label").html(addCommas($("#max_price").val()))
    }), $("#bs-example-navbar-collapse-1 .dropdown-menu a").click(function(a) {
        a.preventDefault()
    });
    var c = ($(".photo-gallery .col-md-2").length, $(".photo-gallery .col-md-2")),
        d = 0;
    c.each(function() {
        8 == ++d ? c.css("display", "block") : d > 8 && $(this).css("display", "none")
    }), $(".view-map-button").click(function(a) {
        $(".property-map").toggleClass("toggle-map"), $(".property-map").hasClass("toggle-map") ? $(".view-map-button").html("<i class='fa fa-map'></i> <span class='show-map-label'>Hide Map</span>") : $(".view-map-button").html("<i class='fa fa-map'></i> <span class='show-map-label'>Show Map</span>")
    }), $(".filter-search-button").click(function() {
        $(".filter-panel,.contact-panel-show").slideToggle("slow")
    }), $(".contact-agent-button").click(function() {
        $(".contact-agent").slideToggle("slow")
    }), $(".filter-link").click(function() {
        $(this);
        $(this).stop().next(".sub-filter-simple,.sub-filter,.price-filter").css("display", "block").parent().siblings().find(".sub-filter-simple,.sub-filter,.price-filter").css("display", "none")
    }), $(".sub-filter-simple li a").on("click", function() {
        var a = $(this),
            b = a.text();
        a.parents("li").find(".chosen-option").text(b), a.stop().parents(".sub-filter-simple").slideUp(150)
    }), $(".property-main-content").click(function() {
        $(this);
        $(".sub-filter-simple,.sub-filter,.price-filter").stop().css("display", "none")
    }), $("#min-price").on("change", function() {
        var a = $(this),
            b = a.val();
        console.log(b), a.parents("li").find(".chosen-min").text(b)
    }), $("#max-price").on("change", function() {
        var a = $(this),
            b = a.val();
        console.log(b), a.parents("li").find(".chosen-max").text(b)
    }), $(".btn-show-filter").click(function() {
        $(".advance-search-form .navbar").slideToggle(250), $(".advance-search-form .navbar").is(":visible") && $("#more-filters-area").hide()
    }), $(".home-search").on("focus", function() {
        var a = $(this);
        0 == a.val() ? $(".help-info").stop().show() : $(".help-info").stop().slideUp()
    }), $(".home-search").on("keypress", function() {
        var a = $(this);
        0 == a.val() ? $(".help-info").stop().show() : $(".help-info").stop().slideUp()
    }), $(".home-search").on("mouseleave", function() {
        $(".help-info").stop().slideUp()
    });
    var e = window.location;
    $('ul.main-nav a[href="' + e + '"]').parent().addClass("active"), $("ul.main-nav a").filter(function() {
        return this.href == e
    }).parent().addClass("active"), $("#accordion").on("hidden.bs.collapse", f), $("#accordion").on("shown.bs.collapse", f), $(".view-full").click(function() {
        $("li.view-full-spec").addClass("active"), $("li.view-full-details").removeClass("active")
    }), $(".advance-search").click(function() {
        $(".search-info").show().delay(2e3).fadeOut(2e3)
    }), $("a").tooltip(), $(".alert-flash").delay(3e3).fadeOut(2e3)


    // iframe width switcher
	$('.size-container ul li').on('click',function(){
		var $this = $(this); 
		var $switcher = $('#iframe-switcher');
		var $iframeWrapper = $('.iframe-wrapper');

		if($this.is('#phone-portrait')){
			
			$iframeWrapper.css('height',1000+'px');
			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('phone-portrait');
		}
		else if($this.is('#tablet-portrait')){

			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('tablet-portrait');	
		}
		else if($this.is('#tablet-landscape')){
			
			$iframeWrapper.css('height',768+'px');
			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('tablet-landscape');	
		}
		else if($this.is('#phone-landscape')){

			$iframeWrapper.css('height',375+'px');
			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('phone-landscape');	
		}
		else{
			$iframeWrapper.css('height',1000+'px');
			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('desktop');
		}

	});

	// dynamic width for featured title
	$(window).load(function(){
		var $trapezoid = $('.trapezoid');
		var $titleWidth = $('.featured-listing-area h2.section-title span').width();
		
		$trapezoid.width($titleWidth+'px');
	});

});

//=================MENU JS============//

  //add dynamic class in default ul submenu
    $( "ul.nav li.parent-li ul" ).removeClass( "nav navbar-nav navbar-right main-nav nav-darken" ).addClass( "dropdown-menu" );
    $( "ul.nav li.parent-li ul li ul" ).removeClass( "dropdown-menu" ).addClass( "dropdown-submenu" );
    $( "ul.nav li.parent-li ul li" ).removeClass( "parent-li" ).addClass( "parent-submenu" );
    $( "ul.nav li.parent-li ul li ul li" ).removeClass( "parent-submenu" ).addClass( "parent-subsubmenu" );


    //add dynamic class in custom ul submenu
    $( "ul.custom-pages li.custom-li ul" ).removeClass( "custom-pages" ).addClass( "custom-pages-dropdown container hasDropdown-area");
    $( "ul.custom-pages li.custom-li ul li" ).removeClass( "custom-li" ).addClass( "custom-submenu" );
    $( "ul.custom-pages li.custom-li ul li ul" ).removeClass( "container hasDropdown-area" ).addClass( "" );
    $( "ul.custom-pages li.custom-li ul li ul li" ).removeClass( "custom-submenu" ).addClass( "custom-subsubmenu" );


    // For Sherry Owen dropwdown Property Videos
    $( ".custom-pages-dropdown .custom-submenu a, .custom-pages .custom-li a" ).each(function() {
      if ($( this ).text() == " Property Videos") {
        $(this).attr('target', '_blank')
      }
    });
    
    if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $( "ul.custom-pages li.custom-pages-hasdropdown ul.custom-pages-dropdown li.custom-submenu i" ).removeClass( "fa-angle-down" ).addClass( "fa-angle-right" );
    }

    //write a review
    $('.write-review-button').on('click', function() {
        $reviewArea = $('.review-area');
        $('#testimonial-message').value='';
        $reviewArea.css('display','block');
    });

    //RateYo (Getting Ratings From Review)
    $("div.ratefixed").each(function(){
        $rate = $(this).attr("data-rate");
        $(this).rateYo({
            rating: $rate,
            readOnly: true,
            fullStar: true
        });
    });

    //Display average ratings
    $(".average-rating").each(function(){
        $rate = $(this).attr("data-average-reviews");
        $(this).rateYo({
            rating:$rate,
            readOnly:true,
            fullStar:true,
        });
    });

    $("#reviews_order").on("change", function(){
        console.log($(this).val());
        $("#sort_reviews_form").submit();
    });

    $(".write-review-button").click(function(){
        $(".new").rateYo({
            fullStar: true,
            rating: 5,
            onInit: function (rating, rateYoInstance) {
              console.log("RateYo initialized! with " + rating);
            }
        });
    });

    $("#review_form").off('submit').on('submit', function(e) {
        e.preventDefault();
        $rate = $(".new").rateYo();
        $rating = $rate.rateYo("rating");
        $("#rating").val($rating);

        $.ajax({
            type: 'POST',
            url: $(this).attr("action"),
            data: $(this).serialize(),
            dataType: 'json',
            beforeSend: function(data) {
                console.log(data);
            },
            success: function(data) {
                if(data.success) {
                    window.location.href = data.redirect;
                    //location.reload();
                } else {
                    window.location.href = data.redirect;
                    //console.log("Error!");
                }
            }
        });
        return false;
    });


    $( window ).scroll(function() {
      var b = $(this).scrollTop();

      if (b > 25) {
        $(".sub-navbar-menu").addClass("sub-dark-menu");
      } else {
        $(".sub-navbar-menu").removeClass("sub-dark-menu")
      }
    });


    $(window).load(function () {
        $("#custom-navbar").removeClass("hide-menu");
    });

    var window_height = $(window).height();
    $(".page-content").css('min-height', window_height-290);

    // toggle more pages button
    $(".subnavbar-menu-btn").click(function(){
    $( ".custom-pages" ).slideToggle("fast");
    $( ".subnavbar-menu-btn a i" ).toggleClass("fa-plus");
    $( ".subnavbar-menu-btn a i" ).toggleClass("fa-minus");
    });
    
    $(".navbar-toggle").click(function(){
        $( ".custom-pages" ).hide();
        $( ".subnavbar-menu-btn a i" ).removeClass("fa-minus");
        $( ".subnavbar-menu-btn a i" ).addClass("fa-plus");
    });


    // adjust find-a-home's sub-navbar-menu and advance search when use is logged in 
    if( /Android|webOS|iPhone|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

        if ($('.userlog').length != 1){
            $('.sub-navbar-menu').css("cssText", "top: 116px !important");

            if ($('.sub-navbar-menu').length == 1){
                $('.advance-search-area').css("cssText", "margin-top: 158px !important;");
            }
        }else {
            if ($('.sub-navbar-menu').length == 1){
                $('.advance-search-area').css("cssText", "margin-top: 177px !important;");
            } else {
                $('.advance-search-area').css("cssText", "margin-top: 137px !important;");
            } 
        }

    } else {    

        if ($('.userlog').length != 1){
            $('.sub-navbar-menu').css("cssText", "top: 105px !important");

            if ($('.sub-navbar-menu').length == 1){
                $('.advance-search-area').css("cssText", "margin-top: 145px !important;");
            }
        }else {
            if ($('.sub-navbar-menu').length == 1){
                $('.advance-search-area').css("cssText", "margin-top: 148px !important;");
            } else {
                $('.advance-search-area').css("cssText", "margin-top: 109px !important;");
            } 
        }  
        
    }
    // Applicable only for mobile
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        // prevent redirect when clicking down button
        $('.custom-li.custom-pages-hasdropdown > i').on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            // var $dropdown = $this.parent('').find('.hasDropdown-area');
            var $dropdown = $this.parents('.custom-pages-hasdropdown').find('.hasDropdown-area');
            $dropdown.toggle();
            
        });

        // prevent redirect when clicking down button
        $('.custom-pages-hasdropdown.custom-submenu > i').on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            // var $dropdown = $this.parent('').find('.hasDropdown-area');
            var $dropdown1 = $this.parents('.custom-pages-hasdropdown.custom-submenu').find('.custom-pages-dropdown');
            $dropdown1.toggle();
            
        });


        // prevent redirect on main nav
        $('.main-nav .has-dropdown > span').on('click', function(e){
            var $windowWidth = $(window).width;

            if($windowWidth < 768){
                e.preventDefault();
                var $this = $(this);

                var $dropdown = $this.parent('.has-dropdown').find('ul.dropdown-menu');
                $dropdown.toggle();    
            }
            
        });

         // dynamic width for featured title
        $(window).load(function(){
            var $trapezoid = $('.trapezoid');
            var $titleWidth = $('.featured-listing-area h2.section-title span').width();
            
            $trapezoid.width($titleWidth+'px');
        });
    }

