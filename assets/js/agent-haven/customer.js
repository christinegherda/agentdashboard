$(document).ready(function() {

	$.ajax({
        url: base_url+"home/customer/check_subscription",
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if(data.is_subscribe_email == 1) {
                $('input[name="email_updates"]').bootstrapSwitch('state', true);
                $('select[name="email_preference"]').val(data.email_schedule);
            } else {
                $('input[name="email_updates"]').bootstrapSwitch('state', false);
                $('select[name="email_preference"]').val(data.email_schedule);
            }
        }
    });
    
    $('input[name="email_updates"]').on('switchChange.bootstrapSwitch', function(event, state) {
        console.log(this); // DOM element
        console.log(event); // jQuery event
        console.log(state); // true | false

        if(state) {
            $post = 'subscribe';
        } else {
            $post = 'unsubscribe';
        }

        $.ajax({
            url: base_url+"home/customer/set_email_subscription",
            type: "POST",
            data: { 
                switch_post:$post
            },
            dataType:'json',
            beforeSend: function() {
                console.log(state);
            },
            success: function(data) {
                console.log(data);
            }
        });
    });

    $("select[name='email_preference']").on("change", function(e) {
        e.preventDefault();
        console.log($(this).val());
        $schedule = $(this).val();

        //if($schedule!="default") {
        $.ajax({
            url: base_url+"home/customer/set_email_schedule",
            type: "POST",
            data: {
                schedule:$schedule
            },
            dataType:'json',
            success:function(data) {
                if(data.success) {
                    $(".display-status").fadeIn(2000);
                    $(".display-status").fadeOut(2000);
                }
            }
        });
        //}
    });

    var sbt_new_password = function ( e )
    {   
        e.preventDefault();

        var url = $(this).attr("action");
        
        $.ajax({
            url : url,
            type : "post",
            data : $(this).serialize(),
            dataType: "json",
            success: function( data){
                console.log(data);
                if(data.success)
                {
                    $(".show-mess").html(data.message);
                }
                else
                {
                    $(".show-mess").html(data.errors);
                }
            }
        });
    };

    var delete_save_property = function (e)
    {
        e.preventDefault();
        var $this = $(this);
        var link = $this.attr("href");
        var property_id = $this.data("id");
        var mls_id = $this.data("property-id");
        var action = link+"/"+property_id+"/"+mls_id;
        //alert(property_id)
        $.msgBox({
            title: "Are You Sure",
            content: "Would you like to remove this property?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "Cancel"}],
            success: function (result) {
                if (result == "Yes") {
                    $.ajax({
                        url : action,
                        type : "post",
                        dataType: "json",
                        beforeSend: function(){
                            $this.text("Loading...");
                        },
                        success: function( data){
                            console.log(data);
                            if(data.success)
                            {
                                $( "#"+property_id ).fadeOut( "slow", function() {
                                    $.msgBox({
                                        title:"Success",
                                        content:"Property has been removed!",
                                        type:"info"
                                    });
                                 });
                            }
                            else
                            {
                                $.msgBox({
                                    title: "Error",
                                    content: "Failed to Delete property!",
                                    type: "error",
                                    buttons: [{ value: "Ok" }]                                
                                });
                            }
                        }
                    });
                }
            }
        });

    };

    var delete_save_searches = function (e)
    {
        e.preventDefault();
        var $this = $(this);
        var link = $this.attr("href");
        var property_id = $this.data("id");
        var action = link+"/"+property_id;
        //alert(property_id)
        $.msgBox({
            title: "Are You Sure",
            content: "Would you like to remove this property search?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "Cancel"}],
            success: function (result) {
                if (result == "Yes") {
                    $.ajax({
                        url : action,
                        type : "post",
                        dataType: "json",
                        beforeSend: function(){
                            $this.text("Loading...");
                        },
                        success: function( data){
                            console.log(data);
                            if(data.success)
                            {
                                $( "#search_"+property_id ).fadeOut( "slow", function() {
                                    $.msgBox({
                                        title:"Success",
                                        content:"Record has been removed!",
                                        type:"info"
                                    });
                                 });
                            }
                            else
                            {
                                $.msgBox({
                                    title: "Error",
                                    content: "Failed to Delete record!",
                                    type: "error",
                                    buttons: [{ value: "Ok" }]                                
                                });
                            }
                        }
                    });
                }
            }
        });

    }

    var change_password_modal = function( e ){
        e.preventDefault();
        
        $('#change-pass-modal').modal('show');

        $("#customer-change-pass").on("submit", sbt_new_password);
    };

    var sbt_edit_profile = function ( e )
    {   
        e.preventDefault();

        var url = $(this).attr("action");
        $(".edit-profile-btn").prop('disabled',true);
        $(".edit-profile-btn").text('Loading..')
        $.ajax({
            url : url,
            type : "post",
            data : $(this).serialize(),
            dataType: "json",
            success: function( data){
                console.log(data);
                
                if(data.success)
                {
                    //$(".show-mess").html(data.message);
                    $.msgBox({
                        title:"Success",
                        content: data.message,
                        type:"info"
                    });

                    location.reload();
                }
                else
                {
                    //$(".show-mess").html(data.errors);
                    $.msgBox({
                        title: "Error",
                        content: data.message,
                        type: "error",
                        buttons: [{ value: "Ok" }]                                
                    });
                }
                $(".edit-profile-btn").prop('disabled',false);
                $(".edit-profile-btn").text('Submit')
            }
        });
    };

    var edit_profile = function( e )
    {
        e.preventDefault();

        $('#customer-profile-modal').modal('show');
        $("#customer-profile").on("submit", sbt_edit_profile);
    };

    var update_email_settings = function(e)
    {
        var val = $(this).val();
        var url = base_url+"home/customer/update_email_preferences_settings";
        $.ajax({
            url : url,
            type : "post",
            data : ( {"settings" : val }),
            dataType: "json",
            success: function( data){
                console.log(data);
                if(data.success)
                {
                    $.msgBox({
                        title:"Success",
                        content: "Alerts and Email Preferences has been successfully updated",
                        type:"info",
                        buttons: [{ value: "Ok" }]
                    });
                }
                else
                {
                    $.msgBox({
                        title: "Error",
                        content: "Failed to update Alerts and Email Preferences!",
                        type: "error",
                        buttons: [{ value: "Ok" }]                                
                    });
                }
            }
        });

    }

    $("#customer-change-password").on("click", change_password_modal);
    $(".delete-save-property").on("click", delete_save_property);
    $(".delete-save-searches").on("click", delete_save_searches);

    $("#customer-edit-profile").on( "click", edit_profile);
    $(".updates-email-settings").on("click", update_email_settings);

});