 $(document).ready(function() {
    // Add window height css
    var window_height = $(window).height();
    $(".sign-up-page").css("min-height", window_height);
    /*
        $(document).ready(function() {

          var timer = setInterval(function() {

            var count = parseInt($('#theTarget').html());
            if (count !== 0) {
              $('#theTarget').html(count - 1);
            } else {
              clearInterval(timer);
            }
          }, 1000);
        });
    */

    var timer = setInterval(function() {
        var count = parseInt($('#theTarget').html());
        if (count !== 0) {
            $('#theTarget').html(count - 1);
        } else {
          clearInterval(timer);
          window.location.href = "https://sparkplatform.com/ticket";
        }
    }, 1000);

 	$("div#response-text").hide();
 	$("div#idx-response-text").hide();

	//Sign Up Form Validations
	var signup = $("#signupForm");

	signup.validate({
		rules: {
			email: {
				required: true,
				email: true
			},
			password: "required",
			confirm_password: {
				required: true,
				equalTo: "#password"
			}
		}
	});

	function emptyFields() {
		var email = $('input[name=email]').val();
		var pw = $('input[name=password]').val();
		var con_pw = $('input[name=confirm_password]').val();

		if(email == "" || pw == "" || con_pw == "") {
			return true;
		} else {
			return false;
		}
	}

	$("#signupForm").on('submit', function(e) {

		var type = $('.create-account').data('type');
		if(type == "idx") {
			submit_idx();
		} else if(type == "spw") {
			submit_spw();
		} else {
			submit_trial();
		}
		e.preventDefault();
     	return false;
	});

	function submit_trial() {
		if(emptyFields()) {
			console.log("Some fields are empty");
		} else {
			var formData = {
				'email' : $('input[name=email]').val(),
				'password' : $('input[name=password]').val()
			};
			
			$.ajax({	
				type: "POST",
				url: "landing_page/sign_up/signup_trial",
				data: formData,
				dataType: 'json',
				success: function(data) {
					console.log(data);

					if(data.success)
					{
						window.location.href = data.redirect;
					}
					else
					{
						$("span.display-status").text("Email Address Already Exist!").show().fadeOut(3000).css({
                            "display": "block",
                            "text-align": "center"
                        });
					}
				}
			});
		}
	}

	function submit_spw() {
		if(emptyFields()) {
			console.log("Some fields are empty");
		} else {
			var formData = {
				'email' : $('input[name=email]').val(),
				'password' : $('input[name=password]').val()
			};
			
			$.ajax({	
				type: "POST",
				url: "landing_page/sign_up/signup",
				data: formData,
				dataType: 'json',
				success: function(data) {
					console.log(data);

					if(data.success)
					{
						window.location.href = data.redirect;
					}
					else
					{
						$("span.display-status").text("Email Address Already Exist!").show().fadeOut(3000).css({
                            "display": "block",
                            "text-align": "center"
                        });
					}
				}
			});
		}
	}

	//Check the email if it is already in the database
 	function submit_idx() {

 		if($('input[name=email]').val() != "" && $('input[name=password]').val() && $('input[name=confirm_password]').val()) {
			
			var formData = {
				'email' : $('input[name=email]').val()
			};
			
			$.ajax({	
				type: "POST",
				url: "landing_page/email_exist",
				data: formData,
				success: function(data) {
					if(data == "1") {
						$("span.display-status").text("Email Address Already Exist!").show().fadeOut(3000).css({
                            "display": "block",
                            "text-align": "center"
                        }); 
					} else {
						//Setting value for the hidden email and password
						$email = $('input#email').val();
						$password = $('input#password').val();
						$("#agent_email").val($email);
						$("#agent_password").val($password); 
						$("#wizard-modal").modal();			
					}
				}
			});
		}
 	}

 	function check_idx(finish) {
        //$(this).text("Loading");
        //var finish = wizard.find(".actions a[href$='#finish']").prop('disabled', true);
 		if($('input[name=api_key]').val() != "" && $('input[name=api_secret]').val() != "") {
			
			var formData = {
				'api_key' : $('input[name=api_key]').val(),
				'api_secret' : $('input[name=api_secret]').val()
			};
			
			$.ajax({	
				type: "POST",
				url: "landing_page/check_idx",
				data: formData,
				dataType: 'json',
                beforeSend: function() {
                    wizard.find(".actions a[href$='#finish']").text("Submitting...").attr('disabled', true);
                },
				success: function(data) {
					if(data.success) {
						//Submit form if the the idx is valid
                        //wizard.find(".actions a[href$='#finish']").hide();
						$("form#wizard-form").submit();
					} else {
						//displaying error message if the idx key is invalid
                        finish.text("Finish").attr({"disabled": false, "href": "#finish"}).css({"cursor": "pointer"});
						$("span.display-status").text("The IDX Key you entered is incorrect. Please go back to step 1.").show().fadeOut(10000).css({
                            "display": "block",
                            "text-align": "center"
                        }); 	
					}
				}
			});
		}
 	}

	//Wizard Form Validations
	var wizardForm = $("form#wizard-form");

	wizardForm.validate({
		rules: {
			api_key: "required",
			api_secret: "required",
			site_name: "required",
            tag_line: "required",
		}
	});

	var wizard = $('#wizard');
    wizard.steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true,
        onStepChanging: function (event, currentIndex, newIndex)
    	{
	        wizardForm.validate().settings.ignore = ":disabled,:hidden";
	        return wizardForm.valid();
	    },
	    onFinishing: function (event, currentIndex)
	    {
	        wizardForm.validate().settings.ignore = ":disabled";
	        return wizardForm.valid();
	    },
	    onFinished: function (event, currentIndex)
	    {  
            var $finish = wizard.find(".actions a[href$='#finish']").text("Submitting...").attr({"disabled": true, "href": "javascript:void(0);"}).css({ "background": "gray !important", "cursor": "default"});
	    	check_idx($finish);
	    }
    });

    $("#domain_yes").on("click", function() {
        $("#domain_exist_container").show("slow");
        $("#domain_container").hide("slow");
    });

    $("#domain_no").on("click", function() {
        $("#domain_container").show("slow");
        $("#domain_exist_container").hide("slow");
    });

    $(".domain-ajax").on("click", function(){
 		
 		$(".return-mess").hide();
        var domain_check = $(".domain_check").val();
        var domain_name = $("#domain_name").val();
        var domain_suffix = $("#suffix").val();
        var domain_url = $(".domain_url").val();

        if(domain_name == "")
        {
            alert("Domain name is empty!")
        }
        console.log(domain_url); 
        datastring = {'suffix':domain_suffix, 'domain_name' : domain_name, "domain_check": domain_check};
        $.ajax({
            type: "GET",
            url: domain_url,
            data: datastring,
            beforeSend: function() {
                $("#check_domain_sbt").hide("fast");
                $("#checkDomainLoad").css("display", "block");
            },
            success: function(e) {
                
                var c = jQuery.parseJSON(e);   
                
                var domainName = $.map(c.domainName, function(value, index) {
                    return [value];
                });
                console.log(c.status);
                if(c.status == 1) {
                    // checkDomain
                    console.log(domainName);
                    var domainPrice = $.map(c.domainPrice, function(value, index) {
                        return [value];
                    });
                    $("#domainResult").show();
                    $("#domainResult").html("<h4>Domain <span>" + domainName + "</span> is available. Click <a href='javascript:void(0)' class='purchase-domain' data-target='#domainModal' data-domain_name_avail="+domainName+" >here</a> to reserve this domain</h4>"); 
                	//$(".availableDomains").hide();
               		$("#domainResultAv").hide(); 
               		$(".purchase-domain").on("click", show_domain_modal_available);

                    $("#check_domain_sbt").show("fast");
                	$("#checkDomainLoad").css("display", "none");
                }
                else{
                    console.log(c);
                    //window.location = $("#domainUrl").val() + "?domain=" + c.domain + "&tld=" + c.tld + "&status=0";
                    //$("#domainResult").html("<h4 class='taken'><span>"+domainName+"</span> is already taken.</h4><h4 class='featured'>Featured Domains:</h4>");
                
                    $.ajax({
                        type: "GET",
                        url: $("#domainUrl").val(),
                        data: {
                            domain_tld_get: "1",
                            domain : $("#domain_name").val()
                        },
                        beforeSend: function() {
                            $("#check_domain_sbt").hide("fast");
                			$("#checkDomainLoad").css("display", "block");
                        },
                        success: function(e) {
          
                            $("#domainResult").hide(); 
                            $(".availableDomains").show();
                            $("#domainResultAv").show(); 
                            
                            var c = jQuery.parseJSON(e);

                            var domainCount = $.map(c.domainCount, function(value, index) {
                                return [value];
                            });
                            
                            var splitStr = '';
                            var domainStatus = [];
                            var statusCode = [];
                            console.log(c.domains.length);

                            for(var i = 0; i < c.domains.length; i ++ ) {
                                splitStr = c.domains[i].split("_");
                                for (var j = 0; j < domainCount; j++) {
                                    if( splitStr[0] == "Domain" + (j + 1) ) {
                                        domainStatus.push(splitStr[1]); 
                                    }
                                    if( splitStr[0] == "RRPCode" + (j + 1) ) {
                                        statusCode.push(splitStr[1]); 
                                    }
                                }
                            }
                            
                            var htmlInsertString = "";
                            var other_ext = false;
                            console.log(domainStatus.length);

                            for(var i = 0; i < domainStatus.length; i ++) {
                                console.log(statusCode[i]);
                                if(statusCode[i] == '210') {                    
                                    var domainName = domainStatus[i].split(".");                    
                                    
                                    allowDomainExt  = ["com","org","net"];
                                    console.log(domainName);
                                    if(jQuery.inArray(domainName[1], allowDomainExt) != -1)
                                    {
                                        other_ext = true;
                                        // Domain is Available
                                    	/*htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+domainStatus[i]+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-domain_name="'+domainStatus[i]+'" >Reserve this domain free for me</a></div></li>';    */
                                    	htmlInsertString += '<li style="line-height: 40px;"><div class="col-md-5 col-sm-5">'+domainStatus[i]+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-target="#domainModal" data-domain_name_avail="'+domainStatus[i]+'" >Reserve this domain</a></div></li>';
                                    }               
                                }
                            }

                            if(!other_ext)
                            {
                                for(var i = 0; i < domainStatus.length; i ++) {
                                    if(statusCode[i] == '210') {                    
                                        var domainName = domainStatus[i].split(".");           
                                            // Domain is Available
                                    		/*htmlInsertString += '<li style="line-height: 40px;"><div class="col-md-5 col-sm-5">'+domainStatus[i]+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-domain_name="'+domainStatus[i]+'">Reserve this domain free for me</a></div></li>';    */
                                    		htmlInsertString += '<li style="line-height: 40px;"><div class="col-md-5 col-sm-5">'+domainStatus[i]+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-target="#domainModal" data-domain_name_avail="'+domainStatus[i]+'">Reserve this domain</a></div></li>';
                                    }
                                    
                                }
                            }
                            //alert(htmlInsertString);
                            if( htmlInsertString == "" )
                            {
                                htmlInsertString += '<li style="line-height: 40px;"><div class="col-md-5 col-sm-5">No Domain Available!</div></li>';
                            }
                            
                            $("#domainResultAv ul.domain-list").html(htmlInsertString);   
                            //$(".purchase-domain").on("click", select_domain_suggest);
                            $(".purchase-domain").on("click", show_domain_modal_available);
                            $("#check_domain_sbt").show("fast");
                			$("#checkDomainLoad").css("display", "none");
                        }
                    });
                }
            } 
        });
    });

 	$('#btnDomainCancel').click(function(){
        $("#domainModal").modal('toggle');
   	}); 

    var select_domain_suggest = function() {
    	var $this = $(this);
    	var domain_name = $(this).attr("data-domain_name");
    	var url = $("#check_domain_exist").val();
    	$("input[name='domain_name_holder']").val(domain_name);

    	var formData = {
    		'domain_name_holder' : domain_name
    	};

    	$.ajax({
    		type: "POST",
 			url: url,
 			data: formData,
 			dataType: 'json',
 			beforeSend: function() {
 				console.log("Checking...");
 			},
 			success: function(data) {
 				if(data.success) {
 					//display error message if domain is already in the database
 					console.log("Positive")
 					$(".return-mess").show().css({"margin-top" : "50px"});
    				$(".return-mess").html(domain_name + " was alreadytaken by another agent!");
 				} else {
 					//display success message
 					console.log("Negative");
 					$(".return-mess").show().css({"margin-top" : "50px"});
    				$(".return-mess").html(domain_name + " successfully reserved!");
 				}
 			}
    	});
    }

     var show_domain_modal_available = function () {        

        var $this = $(this);            
        var domain_name = $this.attr("data-domain_name_avail");
        console.log($("#domain_name").val());
        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);

        $("#domainModal").modal('show');
        $(".confirm-purchase-domain").on("click", purchase_domain_avail);
    };

    var purchase_domain_avail = function (){
        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        // console.log("domain name..");
        // console.log(domain_name);
        // alert(domain_name)
        var url = $("#purchaseDomainUrl").val();
        
        $.ajax({
            type: "POST",
            url: url,
            data: {
                domain_name: domain_name,
                //agentId : user_id,
                //user_name : user_name
            },
            dataType : "json",
            beforeSend: function() {
                console.log("Sending..");
                $this.text("Loading...");
            },
            success: function(data) {
                console.log(data);
               	if(data.success) {
    				console.log("true");
    				$("input[name='domain_name_holder']").val(domain_name);
                    $(".purchased_domain_id").val(data.domain_id);
                    $(".return-hide").hide();
                    $("#domainModal").modal('hide');
                    $(".return-mess").show().css({"margin-top" : "50px"});
                    //$(".return-mess").html("Your domain has been successfully reserved!")
                    $(".return-mess").html(data.message);
 				} else {
 					console.log("false");
                    $(".return-mess").show().css({"margin-top" : "50px"});
                    $("#domainModal").modal('hide');
                    //$(".return-mess").html("Failed to reserve your domain!");
                    $(".return-mess").html(data.error);
 				}

                $this.text("Continue");
            }
        });
        console.log(url);
    };

	// accordion
	$(".panel-collapse").hide();
	$(".moreDetails").show();
	$(".panel-heading").on("click",function(){
		var $this = $(this);
		
		$this.stop().next().slideDown().parent().siblings().find('.panel-collapse').slideUp();
		$this.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-minus-circle')
			.parents('.panel-default').siblings().find('.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
	});

	//Choose theme
	$("#agent-haven-btn").click(function() {
		$("input[name='theme']").val("agent-haven");
	});

	$("#greenery-btn").click(function() {
		$("input[name='theme']").val("greenery");
	});

	$("#proper-btn").click(function() {
		$("input[name='theme']").val("proper");
	});

	// ease scrolling
	$('a[href^="#"]').on("click", function(t) {
        t.preventDefault();
        var o = this.hash,
            n = $(o);
        $("html, body").stop().animate({
            scrollTop: n.offset().top
        }, 900, "swing", function() {
            window.location.hash = o
        });
    });
});
