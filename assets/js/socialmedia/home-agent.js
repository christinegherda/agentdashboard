
jQuery(document).ready(function($){

	$(".alert-flash").delay(6000) .fadeOut(2000);

	//social-post button
	$('#cmdPost').click(
		function(){
			if($('#post_facebook').val() == 0 && $('#post_twitter').val() == 0 && $('#post_linkedin').val() == 0 && $('#post_googleplus').val() == 0){
				$('#post_select_sm_status').removeClass('hide');
				$('#post_select_sm_status').html('Please select at least 1 social media.');
			} else {
				$('#socialPost').submit();
			}
		}
	);

	// $('button.btn-preview').click(function() {
	//      $('.preview-text-post').text($('#post-area').val());
	// });

	$('button.btn-save-modal').on('click', function() {
		var $this = $(this);
		$this.button('loading');
		setTimeout(function() {
			$this.button('reset');
		}, 8000);
	});

	var title = $("#prop-title");
	var link = $("#prop-link");
	var photo = $("#prop-photo");
	var post = $("#post-area");
	var notif = $('#propertySelectedNotFound');
	var fbBtn = $('#fbshare-prop-post');

	$("#propertySelected").change(function(){
		var option = $("#propertySelected option:selected");
		var selectedProp = option.val();

		if (selectedProp !== "") {
			$('#postSelected').change();
		}

		if ($(this).val().length === 0) {
			fbBtn.attr('disabled', true);
			title.val('');
			link.val('');
			photo.val('');
			post.val('');
			return;
		}

		title.val(option.data('address'));
		link.val(option.data('url'));
		photo.val(option.data('priPhoto'));
		post.val(`Check out my new listing at ${option.data('address')}`);
		fbBtn.attr('disabled', false);
	});

	$("#postSelected").change(function(){

		var data_type = $("#propertySelected option:selected").data("type");

		if(data_type === "spw_property"){

			var url = base_url+"agent_social_media/get_property_activate_data";

		} else {

			var url = base_url+"agent_social_media/get_property_listing_data";
		}

		// var url = base_url+"agent_social_media/get_property_activate_data";
		var selectedProp = $("#propertySelected option:selected").val();
		var selectedOpt = $("#postSelected option:selected").val();

		//console.log(selectedProp);
		$.ajax({
			type: 'POST',
			url: url,
			data : { id: selectedProp, select: selectedOpt },
			dataType: 'json',
			success: function(data) {
				//console.log(data);

				// get the current date
				var d = new Date();
				var month = d.getMonth()+1;
				var day = d.getDate();
				var dateToday = d.getFullYear() + '/' +
					((''+month).length<2 ? '0' : '') + month + '/' +
					((''+day).length<2 ? '0' : '') + day;


				if( data.success )
				{
					var price = numeral(data.price).format('0,0.00');
					var price_previous = numeral(data.price_previous).format('0,0.00');

					if(data_type === "spw_property"){

						$("#selected-option1").html('Check out my new listing at '+data.unparseAddress+'! ');
						$("#selected-option2").html('Come to my open house at '+data.unparseAddress+' on '+dateToday+'! ');
						$("#selected-option3").html('Price Reduction at '+data.unparseAddress+' was $'+price_previous+' now only $'+price+' ');
						$("#selected-option4").html('Sold '+data.unparseAddress+' in [days] at [x%] of $'+price+' ');

					} else {

						$("#selected-option1").html('Check out my new listing at '+data.address+'! ');
						$("#selected-option2").html('Come to my open house at '+data.address+' on '+dateToday+'! ');
						$("#selected-option3").html('Price Reduction at '+data.address+' was $'+price_previous+' now only $'+price+' ');
						$("#selected-option4").html('Sold '+data.address+' in [days] at [x%] of $'+price+' ');
					}

					// change .post-area value
					PopulateOption();
				}

			},
			error: function(e) {
				console.log(e);
			}
		});
	});

	$("#btn-preview-post").click(function(){

		var  photo = $('#prop-photo').val();

		$('.preview-text-post').text($('#post-area').val());
		$('#preview-img').html('<img src= '+photo+' />');
		$(".property-address").html($('#prop-title').val());
		$('.property-url').html($('#prop-link').val());

	});

	// $("#btn-preview-post").click(function(){

	//     var data_type = $("#propertySelected option:selected").data("type");
	//     var url = (data_type === "spw_property") ? base_url+"agent_social_media/get_property_activate_data" : base_url+"agent_social_media/get_property_listing_data";
	//     var selectedProp = $("#propertySelected option:selected").val();

	//     $.ajax({
	//         type: 'POST',
	//         url: url,
	//         data : { id: selectedProp },
	//         dataType: 'json',
	//         beforeSend: function() {
	//           console.log("Fetching data");
	//           $(".fetch-active-loading").show();
	//         },
	//         success: function(data) {
	//             //console.log(data);
	//             if(data.success)
	//             {
	//                 var full_desc = data.site_description;
	//                 var limited_desc = full_desc.substring(0,180);

	//                 $('.preview-text-post').text($('#post-area').val());
	//                 $('#preview-img').html('<img src= '+data.primary_photo+' />');
	//                 $(".property-description").html(limited_desc+'..');
	//                 $(".property-address").html('<strong>'+data.address+'</strong>');
	//                 $(".property-broker").html('Listed by: '+data.agent_name+' , Phone: '+data.agent_phone+' ');
	//                 $('.property-url').html('<a href='+data.site_url+'>'+data.site_url+'</a>');
	//             }

	//             $(".fetch-active-loading").hide();
	//         },
	//         error: function(e) {
	//            console.log(e);
	//             $(".fetch-active-loading").hide();
	//         }
	//     });
	// });

	//disable second select option when option 1 is not selected
	// $('#btn-preview-post').attr('disabled', true);

	$("#propertySelected").on("change",function(){
		var selectedProp = $("#propertySelected option:selected").val();

		if (selectedProp != "") {
			$("#postSelected").removeAttr("disabled");
			$("#btn-preview-post").removeAttr("disabled");
		} else {
			$('#btn-preview-post').attr('disabled', true);
		}

	});

	$(".is_selected_post").on("click", function (e) {

		var id = $(this).data("id");
		var val = $(this).val();
		var url = base_url + "agent_social_media/is_page_selected";

		$.ajax({
			url : url,
			type : "post",
			data : {"is_selected" : val, "facebook_id" : id},
			dataType: "json",
			beforeSend: function(){
				$('#cover-spin').show();
			},
			success: function( data ){
				console.log(data);
				if(data.success)
				{
					$("#is_page_selected_"+id).val(data.val);
					location.reload(true);
					//setTimeout(function(){location.reload();},3000);
				}

			}
		})
	})
});