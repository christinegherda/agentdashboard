var fbActivation = fbActivation || {};

window.fbAsyncInit = function () {
    window.FB.init({
        appId  : window.FACEBOOK_APP_ID,
        version: window.FACEBOOK_APP_VERSION,
        cookie: false,
        xfbml: false
    })
}

function fb() {
    if (window.FB) {
        return window.FB
    }

    window.fbAsyncInit = function () {
        window.FB.init({
            appId  : window.FACEBOOK_APP_ID,
            version: window.FACEBOOK_APP_VERSION,
            cookie: false,
            xfbml: false
        })
    }

    return window.FB
}

function popupWindowCenter(url, w = 500, h = 500) {
    const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screen.left;
    const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screen.top;
    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : window.screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : window.screen.height;
    const left = ((width / 2) - (w / 2)) + dualScreenLeft;
    const top = ((height / 2) - (h / 2)) + dualScreenTop;
    const popWindow = window.open(url, 'targetWindow', `toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=${w}, height=${h}, top=${top}, left=${left}`);

    // Puts focus on the newWindow
    if (popWindow.focus) {
        popWindow.focus();
    }

    return popWindow;
}

//LinkedIn Popup Login
function linkdn_login(){
    return popupWindowCenter(window.linkdn_url, 760, 590)
}

//Twitter Popup Login
function twitt_login(){
    return popupWindowCenter(window.twitt_url, 760, 590)
}

//Googleplus Popup Login
function gplus_login(){
    return popupWindowCenter(window.gplus_url, 760, 590)
}

function fb_login() {
    return fb().login(loginResponseHandler, {
        scope: window.FACEBOOK_APP_PERM_SCOPES,
        return_scopes: true
    })
}

var isEmpty = function (input) {
    if ( Array.isArray(input) ) {
        return input.length === 0;
    }

    return !input || Object.keys(input).length === 0;
}

var loginResponseHandler = function (response) {
    if (typeof response.authResponse === "undefined") {
        // Account activation has been cancelled
        return;
    }

    return fb().api('/me?fields=id,name,email,picture&access_token='+response.authResponse.accessToken, function(user) {
        $('#cover-spin').text('please wait...').show();
        return $.when(fbActivation.saveProfile(user, response.authResponse), fbActivation.savePages(response.authResponse))
            .done(function (data) {
                console.log(data);
                window.location.reload();
            }, function (onError) {
                console.log(onError.toString());
                $('#cover-spin').hide()
            })
    })
}

fbActivation = {
    saveProfile: function (data, authResponse) {
        $.extend(data, {
            email: typeof authResponse.email !== "undefined" ? authResponse.email : '',
            expiresIn: authResponse.expiresIn,
            accessToken: authResponse.accessToken
        })
        return $.post('save_fb_user', { user: data })
    },

    savePages: function (authResponse) {
        return fb().api('/me/accounts?fields=id,name,access_token,picture&access_token='+authResponse.accessToken, function(accounts) {
            if (!isEmpty(accounts.data)) {
                return $.post('save_fb_accounts', { accounts: accounts.data })
            }
            return Promise.reject('No pages account received')
        })
    }
}

$(function () {
    var shareBtn = $('#fbshare-prop-post'),
        title = $("#prop-title"),
        link = $("#prop-link"),
        photo = $("#prop-photo"),
        alert = $("#alert-div"),
        post = $('#post-area');

    var notify = function (sl, msg) {
        alert.addClass(sl).text(msg).fadeIn();
        setTimeout(function () {
            alert.removeClass(sl).fadeOut();
        }, 2500);
    };

    var postToFbPages = function () {
        var data = {
            post_facebook: 1,
            property_link: link.val(),
            message: post.val(),
            source: shareBtn.data()
        };

        $.post('social_post', data).then(function (response) {
            console.log(response);
        }, function (error) {
            console.log(error);
        })
    };

    var postResponseHandler = function (response) {
        if ( "undefined" !== typeof response && response.hasOwnProperty('error_message') ) {
            console.log(response);
            notify('alert-warning', 'Unable to share your post to facebook at the moment, please try again later.');
        }
        else if ( undefined === response ) {
            // action cancelled
        }
        else {
            notify('alert-success', 'Posted to Facebook successfully.');
            postToFbPages();
        }
    };

    var showFbShareDialog = function (event) {
        event.preventDefault();
        return (function () {
            var config = {
                method: 'share',
                href: link.val(),
                link: link.val(),
                source: photo.val(),
                quote: title.val() +"\n"+ post.val()
            };
            fb().ui(config, postResponseHandler)
        })()
    };

    shareBtn.on('click', showFbShareDialog);
});
