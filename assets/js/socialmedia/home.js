
jQuery(document).ready(function($){


    $(".alert-flash").delay(6000) .fadeOut(2000);

    //social-post button
    $('#cmdPost').click(
        function(){
            if($('#post_facebook').val() == 0 && $('#post_twitter').val() == 0 && $('#post_linkedin').val() == 0 && $('#post_googleplus').val() == 0){
            $('#post_select_sm_status').removeClass('hide');
            $('#post_select_sm_status').html('Please select at least 1 social media.');
            } else {
            $('#socialPost').submit();
            }
        }
    );

    $('button.btn-preview').click(function() {
         $('.preview-text-post').text($('#post-area').val());
    });

    $('button.btn-save-modal').on('click', function() {
        var $this = $(this);
        $this.button('loading');
        setTimeout(function() {
            $this.button('reset');
        }, 8000);
    });
    
});
    
    //select social media to post
    // function post_to(social_media){
    //     if(social_media == 'facebook'){
    //         if($('#post_facebook').val() == 0){
    //             $('#post_facebook').val(1);
    //             $('i.fa-facebook-square').removeClass('disable');
    //         } else {
    //             $('#post_facebook').val(0);
    //             $('i.fa-facebook-square').addClass('disable');
    //         }
    //     } else if(social_media == 'twitter'){
    //         if($('#post_twitter').val() == 0){
    //             $('#post_twitter').val(1);
    //             $('i.fa-twitter-square').removeClass('disable');
    //         } else {
    //             $('#post_twitter').val(0);
    //             $('i.fa-twitter-square').addClass('disable');
    //         }
    //     }else if(social_media == 'googleplus'){
    //         if($('#post_googleplus').val() == 0){
    //             $('#post_googleplus').val(1);
    //             $('i.fa-google-plus-square').removeClass('disable');
    //         } else {
    //             $('#post_googleplus').val(0);
    //             $('i.fa-google-plus-square').addClass('disable');
    //         }
    //     }else if(social_media == 'linkedin'){
    //         if($('#post_linkedin').val() == 0){
    //             $('#post_linkedin').val(1);
    //             $('i.fa-linkedin-square').removeClass('disable');
    //         } else {
    //             $('#post_linkedin').val(0);
    //             $('i.fa-linkedin-square').addClass('disable');
    //         }
    //     }
    // }


     //social media default post content
    $(document).ready(function() { 
        $("#defaultPost").on("change", function() {
          $("#post-area").val($(this).find("option:selected").attr("value"));
        });

        $('#defaultPost option[value="Check out my new listing at '+unparsedaddr+'"]').prop('selected','true').change();
        });




