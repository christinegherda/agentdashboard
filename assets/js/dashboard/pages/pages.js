

  jQuery(document).ready(function($){

     //Change the form action based on selected value ie.. move_trash
    $('#bulk-action').change(function(){
      var formAction = $("#bulk-select").val() == "trash" ? "bulk_trash" : "";
      $("#bulk-action").attr("action", "/pages/" + formAction);
    });

    //Change the form action based on selected value ie.. restore/delete
    $('#bulk-trash').change(function(){
      var formAction = $("#bulk-select").val() == "restore" ? "bulk_restore" : "bulk_delete";
      $("#bulk-trash").attr("action", "/pages/" + formAction);
    });

    //Change the form action based on selected value ie.. filter
    $('#filter-date').change(function(){
      var formAction = $("#select-pages").val();
      $("#filter-date").attr("action", "/pages/filter" + formAction);
    });

     //Change the form action based on selected value ie.. filter_trash
    $('#filter-trash').change(function(){
      var formAction = $("#select-trash").val();
      $("#filter-trash").attr("action", "/pages/filter_trash" + formAction);
    });

    // Change the select name based on selected action ie.. restore/delete
    $('#bulk-select').change(function(){
      var selectedVal = $("#bulk-select").val() == "restore" ? "bulk_restore[]" : "bulk_delete[]";
      $(".select-action").attr("name", selectedVal);
    });

    //disable publish button when text area/title area is empty
    $(document).ready(function() {

        $('#new-page-sbt').on('submit', function(e) {

          var textarea_value =  tinymce.get('pageTextarea').getContent();
          var title_value = $(".page-titlearea").val();

          if(title_value == '') {
            $(".page-titlearea").css("border-color", "red");
            $("#publish_ret_msg").show();
          }

          if(textarea_value == '') {
            $(".mce-tinymce").css("border-color", "red");
            $("#publish_ret_msg").show();
          }

          if(title_value != '' && textarea_value != '') {
            $("#publish_ret_msg").hide();
            $(this).submit();
          }

          e.preventDefault();

        });

        //$('.page-publish').attr('disabled', true);

        /*$('.page-titlearea, .note-editable').on('keyup',function() {
            var textarea_value = $(".note-editable").text();
            var title_value = $(".page-titlearea").val();
            if(title_value != '' && textarea_value != '') {
                $('.page-publish').attr('disabled' , false);
            } else{
                $('.page-publish').attr('disabled' , true);
            }
        });*/
        
        // count title character
        $('.seo-section .title-char').keyup(function() {
          var maxLength = 62;
          var length = $(this).val().length;
          var length = maxLength-length;
          $('.title-count').text(length);
        });

        // count desc character
        $('.seo-section .desc-char').keyup(function() {
          var maxLength = 160;
          var length = $(this).val().length;
          var length = maxLength-length;
          $('.desc-count').text(length);
        });


          //Get 24 savedsearch via ajax on add/edit page
        $( document ).ready(function(){

              //get page_id via url
              function getPageId() {

                var page_id = [], hash, hashes = null;
                if (window.location.href.indexOf("?") && window.location.href.indexOf("&")) {
                    hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                } else if (window.location.href.indexOf("?")) {
                    hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
                }
                if (hashes != null) {
                    for (var i = 0; i < hashes.length; i++) {
                        hash = hashes[i].split('=');
                        page_id[hash[0]] = hash[1];
                    }
                }
                return page_id;
              }

              var data = getPageId();

              if(data.page_id){
                var url = base_url+"pages/get_saved_search_24?page_id="+data.page_id+"";
              }else{
                var url = base_url+"pages/get_saved_search_24";
              }

              $.ajax({
                  url: url,
                  type: 'POST',
                  dataType: 'json',
                  beforeSend: function() {
                    console.log("Fetching SavedSearches");
                    $(".fetch-ss-info").show();
                    $(".fetch-ss-loading").show();
                    $(".fetch-ss-text").text('Please wait while we are fetching your saved search data!'); 
                  },
                  success: function(data){
                    console.log(data);

                    if(data.success){

                      //for edit page
                      if(data.saved_search_id){
                        //console.log(data.saved_search_id);

                        //for add page
                        var options = '<option value="">Select Savedsearch</option>';
                        for (var x = 0; x < data.items.length; x++) {

                          options += '<option value="' + data.items[x]['Id'] + '" '+(data.saved_search_id === data.items[x]['Id'] ? 'selected' : '')+' >' + data.items[x]['Name'] + '</option>';
                        }

                        if(data.count != data.items.length){
                          $(".show-all-ss").show();
                        } else{
                          $(".show-all-ss").hide();
                        }

                        $('#SavedSearchSelect').html(options);

                      } else {

                         //for add page
                        var options = '<option value="">Select Savedsearch</option>';
                        for (var x = 0; x < data.items.length; x++) {

                          options += '<option value="' + data.items[x]['Id'] + '" >' + data.items[x]['Name'] + '</option>';
                        }

                        if(data.count != data.items.length){
                          $(".show-all-ss").show();
                        } else{
                          $(".show-all-ss").hide();
                        }

                        $('#SavedSearchSelect').html(options);
                      }

                    $(".fetch-ss-info").hide();
                    $(".fetch-ss-loading").hide();
                    $(".fetch-ss-text").hide(); 
                  }
              },
              error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                $('#SavedSearchSelect').html(errorMsg);
                $(".fetch-ss-info").hide();
                $(".fetch-ss-loading").hide();
                $(".fetch-ss-text").hide(); 
              }
          });

      });//end of 24 saved search data

          //trigger on click to show all savedsearches!
           $('body').on('click', '.show-all-ss', function (){
               //Get All savedsearch via ajax on add/edit page
                //get page_id via url
                function getPageId() {

                  var page_id = [], hash, hashes = null;
                  if (window.location.href.indexOf("?") && window.location.href.indexOf("&")) {
                      hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                  } else if (window.location.href.indexOf("?")) {
                      hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
                  }
                  if (hashes != null) {
                      for (var i = 0; i < hashes.length; i++) {
                          hash = hashes[i].split('=');
                          page_id[hash[0]] = hash[1];
                      }
                  }
                  return page_id;
                }

                var data = getPageId();

                if(data.page_id){
                  var url = base_url+"pages/get_saved_search_all?page_id="+data.page_id+"";
                }else{
                  var url = base_url+"pages/get_saved_search_all";
                }

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function() {
                      console.log("Fetching All SavedSearches");
                      $(".fetch-ss-info").show();
                      $(".fetch-ss-loading").show();
                      $(".fetch-ss-text-all").text('Please wait while we are fetching all your saved search data!'); 
                    },
                    success: function(data){

                      if(data.success){

                        //for edit page
                        if(data.saved_search_id){

                          var options = '<option value="">Select Savedsearch</option>';
                          for (var x = 0; x < data.items.length; x++) {

                            options += '<option value="' + data.items[x]['Id'] + '" '+(data.saved_search_id === data.items[x]['Id'] ? 'selected' : '')+' >' + data.items[x]['Name'] + '</option>';
                          }
                          $('#SavedSearchSelect').html(options);

                        } else {

                           //for add page
                          var options = '<option value="">Select Savedsearch</option>';
                          for (var x = 0; x < data.items.length; x++) {

                            options += '<option value="' + data.items[x]['Id'] + '" >' + data.items[x]['Name'] + '</option>';
                          }
                          $('#SavedSearchSelect').html(options);

                        }

                        if(data.count != data.items.length){
                          $(".show-all-ss").show();
                        } else{
                          $(".show-all-ss").hide();
                        }
                         
                      }

                      $(".fetch-ss-info").hide();
                      $(".fetch-ss-loading").hide();
                      $(".fetch-ss-text-all").hide(); 
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                        $('#SavedSearchSelect').html(errorMsg);

                        $(".fetch-ss-info").hide();
                        $(".fetch-ss-loading").hide();
                        $(".fetch-ss-text-all").hide(); 
                      }
                });

          });//end show-all-ss


       // Saved Search  Select Link inside PAGE
        $('#SavedSearchSelectLink').select2();
        $("#SavedSearchSelectLink").on("select2:select", function () {

          const ssName = $('#SavedSearchSelectLink').find(':selected').text();
          const ssId = $('#SavedSearchSelectLink').find(':selected').val();
          const ssUrl = base_url+"saved_searches/"+ssId;

          var ssLink = '<p> <a href="'+ssUrl+'" target="_blank" /a>' +ssName+'</p>'
          var parentEditor = parent.tinyMCE.activeEditor;
          parentEditor.execCommand('mceInsertContent', false, ssLink);

        });

         //Get Savedsearch data(24)
        $( document ).ready(function(){

            $("#select2-SavedSearchSelectLink-container").on("mouseover", function () {
               $('.select2-selection__rendered').removeAttr('title');
            });

            $.ajax({
                url: base_url+"pages/get_saved_search_24",
                type: 'POST',
                dataType: 'json',
                beforeSend: function() {
                  $(".fetch-ss-info-link").show();
                  $(".fetch-ss-loading-link").show();
                  $(".fetch-ss-text-link").text('Please wait while we are fetching your saved search data!'); 
                },
                success: function(data){
                  if(data.success){
                    var options = '<option value="">Select Savedsearch</option>';
                    for (var x = 0; x < data.items.length; x++) {

                      options += '<option value="' + data.items[x]['Id'] + '" >' + data.items[x]['Name'] + '</option>';
                    }

                    if(data.count != data.items.length){
                      $(".show-all-ss-link").show();
                    } else{
                      $(".show-all-ss-link").hide();
                    }

                    $('#SavedSearchSelectLink').html(options);
                  }

                  $(".fetch-ss-info-link").hide();
                  $(".fetch-ss-loading-link").hide();
                  $(".fetch-ss-text-link").hide(); 
            },
            error: function (xhr, ajaxOptions, thrownError) {
              var errorMsg = 'Ajax request failed: ' + xhr.responseText;
              $('#SavedSearchSelectLink').html(errorMsg);
              $(".fetch-ss-info-link").hide();
              $(".fetch-ss-loading-link").hide();
              $(".fetch-ss-text-link").hide(); 
            }
        });
      });

      //Get Savedsearch data(all)
      $('body').on('click', '.show-all-ss-link', function (){
          $.ajax({
              url: base_url+"pages/get_saved_search_all",
              type: 'POST',
              dataType: 'json',
              beforeSend: function() {
                console.log("Fetching All SavedSearches");
                $(".fetch-ss-info-link").show();
                $(".fetch-ss-loading-link").show();
                $(".fetch-ss-text-all-link").text('Please wait while we are fetching all your saved search data!'); 
              },
              success: function(data){

                if(data.success){
                  var options = '<option value="">Select Savedsearch</option>';
                  for (var x = 0; x < data.items.length; x++) {

                    options += '<option value="' + data.items[x]['Id'] + '" >' + data.items[x]['Name'] + '</option>';
                  }
                  $('#SavedSearchSelectLink').html(options);
                }

                if(data.count != data.items.length){
                    $(".show-all-ss-link").show();
                } else {
                    $(".show-all-ss-link").hide();
                }

                $(".fetch-ss-info-link").hide();
                $(".fetch-ss-loading-link").hide();
                $(".fetch-ss-text-all-link").hide(); 
              },
              error: function (xhr, ajaxOptions, thrownError) {
                  var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                  $('#SavedSearchSelectLink').html(errorMsg);

                  $(".fetch-ss-info-link").hide();
                  $(".fetch-ss-loading-link").hide();
                  $(".fetch-ss-text-all-link").hide(); 
              }
          });
      });

    });

});
