"use strict";

jQuery(document).ready(function($) {
    var timeoutId;
    var lastChangedVal = '';
    var saveSocialMediaLinkUrl = function (target) {
        var $target = $(target);
        var form = $target.closest('form');
        var label = form.find('.control-label');
        var ico = label.find('i.fa');
        var toggleSuccessClass = function () {
            $target.parent().removeClass('has-success');
            $target.removeAttr('aria-describedby');
            $("span.display-status").fadeOut("slow").hide();
        };
        var showSpinner = function () {
            ico.removeClass(ico.data('originalIco')).addClass('fa-spinner fa-spin')
        };
        var hideSpinner = function () {
            ico.removeClass('fa-spinner fa-spin').addClass(ico.data('originalIco'))
        };

        return function () {
            var re = new RegExp("^[a-zA-Z0-9-_.,?=/:\\s&+]+$");

            if ( !target.value.match(re) ) {
                $target.parent().addClass('has-error').removeClass('has-success');
                $target.parent().find('.form-control-feedback').addClass('glyphicon-remove').removeClass('glyphicon-ok');
                $target.attr('aria-describedby', `${$target.data('link')}-url-input-error-status`);

                $("span.display-status").fadeOut("fast").hide();
                $("span.display-error")
                    .text(`Please enter a valid ${$target.data('link')} profile url!`)
                    .fadeIn("slow").css({'display': 'block'});

                return;
            }

            $target.parent().removeClass('has-error').addClass('has-success');
            $target.parent().find('.form-control-feedback').addClass('glyphicon-ok').removeClass('glyphicon-remove');
            $target.attr('aria-describedby', `${$target.data('link')}-url-input-success-status`);

            if ( target.value.trim() !== lastChangedVal ) {
                showSpinner();
                $.post(form.attr('action'), form.serialize())
                    .done(function (response) {
                        response = JSON.parse(response);
                        if (response.success) {
                            target.value = response.data.link;
                        }

                        $("span.display-error").fadeOut("slow").hide();
                        $("span.display-status").fadeOut("fast")
                            .fadeIn("slow").css({'display': 'block'})
                            .text(`${$target.data('link')} link has been saved!`);

                        hideSpinner();
                        setTimeout(toggleSuccessClass, 10000);
                    })
                    .error(function (error) {
                        console.error(error);
                        hideSpinner();
                        toggleSuccessClass();
                    })
            }

            lastChangedVal = target.value.trim();
        }
    };

    $(document).on('input propertychange change', 'input.social-link-input', function (event) {
        if (window.disallowUser) {
            $("#modalPremium").modal();
            $(this).val("");
            return;
        }

        clearTimeout(timeoutId);
        timeoutId = setTimeout(saveSocialMediaLinkUrl(event.target), 1000);
    });

    $(document).on('click', '.delete-link', function (event) {
        event.preventDefault();
        var $target = $(event.currentTarget);

        if ($(`input.${$target.data('link')}-link`).val() === '') {
            $("span.display-error")
                .text(`${$target.data('link')} link is empty!`)
                .fadeIn("slow").css({'display': 'block'});

            setTimeout(function () {
                $("span.display-error").fadeOut("slow").hide();
            }, 3000);

            return;
        }

        $.get($target.attr('href'))
         .done(function () {
             $(`input.${$target.data('link')}-link`).val('');
             $("span.display-error").fadeOut("slow").hide();
             $("span.display-status").fadeOut("fast")
                 .fadeIn("slow").css({'display': 'block'})
                 .text(`${$target.data('link')} link has been deleted!`);
         })
         .error(function (error) {
             $("span.display-error")
                 .text("Unable to delete the selected link!")
                 .fadeIn("slow").css({'display': 'block'});
             console.error(error);
         })
    });

    $('.social-link-forms').submit(function(e) {
        e.preventDefault();
    });
});


//Social Media Popup Activate
    function fb_login(){
            var w_left = (screen.width/6);
            var w_top = (screen.height/6);
            window.open(fb_url,'targetWindow','toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top='+w_top+',left='+w_left+'');   
    }

    function linkdn_login(){

            var w_left = (screen.width/6);
            var w_top = (screen.height/6);
            window.open(linkdn_url,'targetWindow','toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top='+w_top+',left='+w_left+''); 
    }

    function twitt_login(){

        var w_left = (screen.width/6);
        var w_top = (screen.height/6);
        window.open(twitt_url,'targetWindow','toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top='+w_top+',left='+w_left+''); 
    }