/**
* Liondesk dashboard javascript
*
* Author: Jovanni G;
* Author URL: http://jovanni.mousbook.com;
*/
window.isDefined = function($var){
  return typeof $var !== 'undefined';
}

window.formatDate = function($date){
  var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  var date = new Date($date);

  var ampm = function($value){
    if($value > 11){
      return 'pm';
    }
    return 'am';
  }

  var hour_12 = function($value){

    if($value > 12){
      return $value - 12;
    }else if($value === 0){
      return 12;
    }
    return $value;
  }

  var fix_zero = function($value){
    if($value < 10){
      return '0'+$value;
    }
    return $value;
  }

  formated = [
    months[date.getMonth()],
    date.getDate()+',',
    date.getFullYear(),
    '@',
    hour_12(date.getHours())+':'+fix_zero(date.getMinutes()),
    ampm(date.getHours()),
  ];

  return formated.join(' ');
}

class Loader{
  constructor(){
    var body = document.getElementsByTagName('body');
    this.body = body[0];

    var overlay = document.createElement('div');
    overlay.className = 'loader-overlay';

    this.overlay = overlay;
  }

  message($message = ''){
    this.overlay.innerHTML = '<div>'+$message+'</div>';
  }

  show(){
    this.body.appendChild(this.overlay);
  }

  hide(){
    this.body.removeChild(this.overlay);
  }
}

class MagicTable{

  constructor(){

    this.table = document.createElement('table');
    this.table.className = 'table stripe';
    this.table.memory = null;

    this.thead = document.createElement('thead');
    this.tbody = document.createElement('tbody');

    this.table.appendChild(this.thead);
    this.table.appendChild(this.tbody);

  }

  init($args = {}){
    var self = this;

    self.args = $args;

    if(isDefined($args.id)){
      this.target = document.getElementById($args.id);
    }else if(isDefined($args.target)){
      this.target = $args.target;
    }
    

    if(isDefined($args.columns)){

      self.columns = $args.columns;

      var tr = document.createElement('tr');

      var th = document.createElement('th');
      tr.appendChild(th);

      var keys = Object.keys(self.columns);
      keys.forEach(function($key){
        
        var th = document.createElement('th');
        th.textContent = self.columns[$key];

        tr.appendChild(th);

      });

      self.thead.appendChild(tr);

    }else{
      console.error("Undefined 'columns'. You must initialize the columns for this table.");
    }
  }

  feed($array = []){
    var self = this;

    var columns = Object.keys(self.columns);

    if($array.length === 0){
      var tr = document.createElement('tr');
      var td = document.createElement('td');
      td.setAttribute('colspan', columns.length + 1);
      td.textContent = 'No data available.';
      tr.appendChild(td);
      self.tbody.appendChild(tr);
      return;
    }

    $array.forEach(function($item, $index){
      var tr = document.createElement('tr');
      tr.data = $item;

      var td = document.createElement('td');
      tr.appendChild(td);

      columns.forEach(function($column){
        
        var td = document.createElement('td');

        if(isDefined(self.args.filter)){

          if(self.args.filter.hasOwnProperty($column)){

            td.textContent = self.args.filter[$column]($item[$column], td);
          }else{
            td.textContent = $item[$column];
          }
        }else{
          td.textContent = $item[$column];
        }

        tr.appendChild(td);

      });

      if(isDefined(self.args.click)){
        tr.addEventListener('click', function(e){
          if(self.table.memory !== null){

            if(self.table.memory.data.id !== this.data.id){
              self.table.memory.removeAttribute('active');
            }
          }
          this.setAttribute('active', '');

          self.args.click(e, this, self.table.memory);

          self.table.memory = this;
        });
      }

      self.tbody.appendChild(tr);

      var trs = Array.prototype.slice.call(self.tbody.children);

      tr.firstChild.textContent = trs.indexOf(tr) + 1;
    });
  }

  update($array = []){
    this.reset();

    this.feed($array);
  }

  reset(){
    while (this.tbody.firstChild) {
      this.tbody.removeChild(this.tbody.firstChild);
    }
  }

  render(){
    this.target.appendChild(this.table);
  }

}

class MagicContact{
  constructor(){
    var self = this;

    var container = document.createElement('div');
    container.id = 'contact-details';
    container.tabs = {
      'tasks': 'Tasks',
      'profile': 'Profile'
    };

    var tabs = document.createElement('ul');
    tabs.className = 'nav nav-tabs';
    
    // create the tab component
    var keys = Object.keys(container.tabs);
    keys.forEach(function($tab){
      var li = document.createElement('li');
      li.setAttribute('role', 'presentation');

      var a = document.createElement('a');
      a.setAttribute('data-toggle', 'tab');
      a.href = '#'+$tab;
      a.textContent = container.tabs[$tab];

      li.appendChild(a);

      tabs.appendChild(li);

      var lis = Array.prototype.slice.call(tabs.children);
      if(lis.indexOf(li) === 0){
        li.className = 'active';
      }
    });

    var btn_close = document.createElement('button');
    $(btn_close).addClass('btn-close'); 
    btn_close.addEventListener('click', function(){
      self.destroy();
    });

    container.appendChild(btn_close);
    container.appendChild(tabs);

    self.container = container;
    self.tabs = tabs;
  }

  init($args = {}){

    this.target = document.getElementById($args.id);
    this.owner = $args.owner;
    if(isDefined($args.onDestroy)){
      this.onDestroy = $args['onDestroy'];
    }

    var contents = document.createElement('div');
    contents.className = 'tab-contents';

    var tabs = Object.keys(this.container.tabs);
    tabs.forEach(function($tab){
      var content = document.createElement('div');
      content.id = $tab;
      content.className = 'tab-pane';

      contents.appendChild(content);

      var divs = Array.prototype.slice.call(contents.children);
      if(divs.indexOf(content) === 0){
        $(content).addClass('active');
      }
    });

    this.container.appendChild(contents);
  }

  render(){
    var self = this;

    this.target.appendChild(this.container);
    if($(this.target).hasClass('show')){
      $(this.target).removeClass('show');
    }else{
      $(this.target).addClass('show');
    }

    return {
      then: function($function){
        $function(self);
      },
    }
  }

  destroy(){
    if(this.target.hasChildNodes()){
      this.target.removeChild(this.container);
    }
    $(this.target).removeClass('show');
    delete this.owner.container;

    if(isDefined(this.onDestroy)){
      this.onDestroy();
    }

    return {
      then: function($function){
        $function(self);
      }
    }
  }

}

class Pagination{
  constructor(){
    var self = this;

    this.container = document.createElement('nav');
    this.ul = document.createElement('ul');
    this.ul.className = 'pagination-list';

    this.container.appendChild(this.ul);
  }
  init($args = {}) {
    console.log($args);
    var self = this;
    if(isDefined($args.id)){
      this.target = document.getElementById($args.id);
    }

    var numpage = $args.totalRows / $args.pageSize;

    for (var i = 1; i < numpage; i++) { 
      var li = document.createElement('li');
      li.innerHTML = "<a href="+$args.url+i+">"+i+"</a>";
      self.ul.appendChild(li);
    }

  }
  action(){
    console.log("test");
  }

  render(){
    this.target.appendChild(this.container);
  }

}

(function($){

  var magictable = new MagicTable();
  var loader = new Loader();
  var pagination = new Pagination();

  magictable.init({
    id: 'magictable',
    columns: {
      id: 'ID',
      last_name: 'Last Name',
      first_name: 'First Name',
      email: 'Email',
      home_phone: 'Home #',
      mobile_phone: 'Mobile #',
      created_at: 'Date Created',
    },
    filter: {
      created_at: function($value){
        return formatDate($value);
      },
    },
    click: function(e, tr, memory){

      loader.message('Getting contact information. Please wait.');

      loader.show();

      $.ajax({
        url: base_url + 'liondesk/ajax/contact/' + tr.data.id + '/task/-1',
        method: 'get',
        success: function(response){

          if(response.success){

            if(memory !== null){
              memory.mc.destroy();
            }

            tr.mc = new MagicContact();

            tr.mc.init({
              id: 'magiccontact',
              owner: tr,
              onDestroy: function(){
                tr.removeAttribute('active');
              },
            });

            tr.mc.render().then(function(self){

              var ul = document.createElement('ul');
              ul.className = 'custom-contents';
              var name = document.createElement('li');
              name.innerHTML = '<h3>' + tr.data.last_name + ', ' + tr.data.first_name + '</h3>';
              var email = document.createElement('li');
              email.innerHTML = '<h5>' + tr.data.email + '</h5>';
              ul.appendChild(name);
              ul.appendChild(email);

              $(self.tabs).before(ul);

              var tasks = document.getElementById('tasks');

              var table = new MagicTable();

              table.init({
                target: tasks,
                columns: {
                  id: 'ID',
                  description: 'Description',
                  reminder_type_id: 'Reminder',
                  due_at: 'Due Date',
                  status: 'Status',
                },
                filter: {
                  due_at: function($value){
                    return formatDate($value);
                  },
                  reminder_type_id: function($value){
                    return globals.reminder_type[$value];
                  },
                },
                click: function(e, task_tr, memory){
                  
                  var container = document.createElement('div');
                  container.id = 'task-detail';

                  var btn_close = document.createElement('button');
                  btn_close.className = 'btn-close';
                  btn_close.addEventListener('click', function(e){

                    $(container).removeClass('show');
                    task_tr.removeAttribute('active');
                    setTimeout(function(){
                      tasks.removeChild(container);
                    }, 100);

                  });

                  var urlRegex = /(https?:\/\/[^\s]+)/g;
                  var note = task_tr.data.notes;
                  var new_note = note.replace(urlRegex, function(url) {
                      return '<a target="_blank" href="' + url + '">' + url + '</a>';
                  })

                  var title = document.createElement('h4');
                  title.innerHTML = 'Task Details';
                  var task_info = document.createElement('div');
                  task_info.innerHTML = '<p> ID: <span>' + task_tr.data.id + '</span> </p>';
                  task_info.innerHTML += '<p> Description: <span>' + task_tr.data.description + '</span> </p>';
                  task_info.innerHTML += '<p> Reminder: <span>' + globals.reminder_type[task_tr.data.reminder_type_id] + '</span> </p>';
                  task_info.innerHTML += '<p> Created: <span>' + formatDate(task_tr.data.created_at) + '</span> </p>';
                  task_info.innerHTML += '<p> Due Date: <span>' + formatDate(task_tr.data.due_at) + '</span> </p>';
                  task_info.innerHTML += '<p> Notes: <span>' + new_note + '</span> </p>';
                  task_info.innerHTML += '<p> Status: <span>' + task_tr.data.status + '</span> </p>';


                  tasks.appendChild(container);
                  // console.log(task_tr);
                  setTimeout(function(){

                    $(container).addClass('show');
                    container.appendChild(btn_close);
                    container.appendChild(title);
                    container.appendChild(task_info);

                  }, 100);
                },
              });

              table.feed(response.query.data);

              table.render();

              var profile = document.getElementById('profile');
              // add profile here.
              var lead_info = document.createElement('div');
              lead_info.className = 'lead-info';
              lead_info.innerHTML = '<p> ID: <span>' + tr.data.id + '</span> </p>';
              lead_info.innerHTML += '<p> Name: <span>' + tr.data.first_name + tr.data.last_name+'</span> </p>';
              lead_info.innerHTML += '<p> Email: <span>' + tr.data.email + '</span> </p>';
              lead_info.innerHTML += '<p> Secondary Email: <span>' + (tr.data.secondary_email ? tr.data.secondary_email : "") + '</span> </p>';
              lead_info.innerHTML += '<p> Company: <span>' + (tr.data.company ? tr.data.company : "")+ '</span> </p>';
              lead_info.innerHTML += '<p> Mobile Number: <span>' + (tr.data.mobile_phone ? tr.data.mobile_phone : "") + '</span> </p>';
              lead_info.innerHTML += '<p> Home Phone: <span>' + (tr.data.home_phone ? tr.data.home_phone : "") + '</span> </p>';
              lead_info.innerHTML += '<p> Office Number: <span>' + (tr.data.office_phone ? tr.data.office_phone : "") + '</span> </p>';
              lead_info.innerHTML += '<p> Fax: <span>' + (tr.data.fax ? tr.data.fax : "") + '</span> </p>';
              lead_info.innerHTML += '<p> Spouse Name: <span>' + (tr.data.spouse_name ? tr.data.spouse_name : "") + '</span> </p>';
              lead_info.innerHTML += '<p> Spouse Email: <span>' + (tr.data.spouse_email ? tr.data.spouse_email : "") + '</span> </p>';
              lead_info.innerHTML += '<p> Spouse Phone: <span>' + (tr.data.spouse_phone ? tr.data.spouse_phone : "" ) + '</span> </p>';
              lead_info.innerHTML += '<p> Spouse Birthday: <span>' + (tr.data.spouse_birthday ? tr.data.spouse_phone : "" )+ '</span> </p>';

              profile.appendChild(lead_info);

            });

            loader.hide();
          }
        }
      }); // end of $.ajax
    },
  });

  magictable.feed(globals.contacts.data);

  magictable.render();

  console.log(globals.contacts);

  // pagination.init({
  //   id: 'pagination',
  //   url: base_url + 'liondesk/ajax/contacts',
  //   totalRows: globals.contacts.data.length,
  //   pageSize: '3',
  //   data: globals.contacts.data
  // });

  // pagination.render();

  window.syncContact = function($items, $index = 0, $loader = null){

    if($index < $items.length){

      $.ajax({
        url: base_url + 'liondesk/ajax/sync',
        method: 'post',
        processData: false,
        contentType: 'application/json',
        data: JSON.stringify({
          id: $items[$index].id,
          first_name: $items[$index].first_name,
          last_name: $items[$index].last_name,
          email: $items[$index].email,
          home_phone: ($items[$index].phone !== null)? $items[$index].phone : '',
          mobile_phone: ($items[$index].mobile !== null)? $items[$index].mobile : '',
        }),
        beforeSend: function(){
          if($loader !== null){
            var left = globals.unsynced - $index;

            $loader.message('Syncing '+left+' contacts. Please wait.');
          }
        },
        success: function(response){
          if(response.success){
            var print = {};
            print[ $items[$index].id.toString() ] = response;
            console.log(print);
            
            syncContact($items, $index + 1, $loader);
          }else{
            console.error(response);

            var message = response.result.error.message.replace('contacts', 'contact') + ' with email ' + $items[$index].email;

            if(isDefined(response.result.error.data)){
              //var keys = Object.keys(response.result.error.data);
            }

            $loader.message(message);

            syncContact($items, $index + 1, $loader);
          }
        },
        error: function(xhr){
          console.log({ error: xhr });
        },
      });
    }else{
      $loader.message('Done syncing all '+globals.unsynced+' contacts. Thank you for waiting.');

      setTimeout(function(){
        // $loader.hide(); // remove for now since the page redirects anyway.
        window.location = base_url + 'liondesk'
      }, 500);
    }
  }

  $('#syncToLiondesk').on('click', function(e){
    var loader = new Loader();

    loader.show();

    $.ajax({
      url: base_url + 'liondesk/ajax/unsynced',
      method: 'get',
      success: function(response){
        console.log(response);
        if(response.success && response.query.length > 0){
          
          loader.message('Fetched ' + response.query.length + ' contacts.');

          syncContact(response.query, 0, loader);

        }
      },
      error: function(xhr){
        console.log(xhr);
      }
    });
  });

})(jQuery);
