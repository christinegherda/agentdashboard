(function($) {

    var aws_sbt_domain = function (eve){
        eve.preventDefault();
        $("#aws_check_domain_sbt").prop('disabled', true);
        var datastring = $("#aws-domain-form").serialize();
        $.ajax({
            type: "GET",
            url: $("#aws-domain-form").attr('action'),
            data: datastring,
            beforeSend: function() {
               $("#checkDomain").hide("fast");
                $("#checkDomainLoad").show("fast");
            },
            success: function(e) {
                $("#checkDomain").show("fast");
                $("#checkDomainLoad").hide("fast");
                
                var c = jQuery.parseJSON(e);
                var domainName = c.domainName;

                if( c.status == 1 ) {
                    $("#awsDomainResult").html("<h4>Domain <span>" + domainName + "</span> is available. Click <a href='javascript:void(0)' class='purchase-domain' data-domain_name_avail="+domainName+" >here</a> to reserve this domain</h4>"); 
                }
                else {
                    console.log(c);
                    window.location = $("#domainUrl").val() + "?domain=" + c.domain + "&tld=" + c.tld + "&status=0";
                    // $("#domainResult").html("<h4 class='taken'><span>"+domainName+"</span> is already taken.</h4><h4 class='featured'>Featured Domains:</h4>");
                }

                $('#aws_check_domain_sbt').prop('disabled', false);
                $(".purchase-domain").on("click", show_domain_modal_available);
            } 
        });
    }

    $("#aws-domain-form").on("submit", aws_sbt_domain);

    var purchase_domain_avail = function () {

        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        var user_id = $("#user_agent_id").val();
        var url = $("#purchaseDomainUrl").val();
        var user_name = $("#user_name").val();
        var site_dir = $("#domainDirectory").val();

        $.ajax({
            type: "GET",
            url: url,
            data: {
                domain_name: domain_name,
                agentId : user_id,
                site_dir : site_dir
            },
            dataType : "json",
            beforeSend: function() {
                console.log("Sending.. Site DIR: " + site_dir);
                $this.text("Loading...");
            },
            success: function(data) {
                
                if(data.success)
                {   
                    location.reload();
                    $("#domainModal").fadeOut("slow", function () {
                        $('#domainModal').modal('hide');
                    });

                    // $( "#custom_domain_panel" ).fadeOut( "slow", function() {
                    //     $("#custom_domain_panel").hide();
                    //     var message = '<div class="alert alert-success" role="alert">'+data.message+'<div>';
                    //     $(".response_domain").append(message);
                    // });           
                }
                else
                {   
                    location.reload();
                    $("#domainModal").fadeOut("slow", function () {
                        $('#domainModal').modal('hide');
                    });
                    // $this.text("reserve this domain free for me");
                    // var message = '<div class="alert alert-danger" role="alert">'+data.error+'<div>';
                    // $(".moreDetails").append(message);
                }
            
            }
        });

        console.log(url);
    };

    var show_domain_modal_available = function () {        

        var $this = $(this);            
        var domain_name = $(this).attr("data-domain_name_avail");
        
        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);

        $("#domainModal").modal('show');
        $(".confirm-purchase-domain").on("click",purchase_domain_avail);
    };

})(jQuery);

$(document).ready(function () {

    $("#domain_yes").click(function(){
        $("#domain-result").hide();
        $("#existing_domain").show();
        $("#domain_container").hide();
        $("#subdomain_container").show();
    });

    $("#domain_no").click(function() {
        $("#domain-result").show();
        $("#existing_domain").hide();
        $("#domain_container").show();
        $("#subdomain_container").show();
    });

    //Saving sub domain
    $("#save_sub_domain").click(function() {

        var url = $("input[name='sub_domain_url']").attr('value');
        var subdomain_name = $("input[name='subdomain_name']").attr('value');
        var user_id = $("#user_agent_id").val();

        $('#save_sub_domain').attr('disabled','disabled');
        $('#subdomaininfo').css("display","none");
        $('#subdomainstatus').css("display","block");
        
        $.ajax({
            type: 'POST',
            url: url,
            data: { 
                domain_name: subdomain_name,
                agentId: user_id,
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                window.location.href = data.redirect;
            }
        });
    });
    
    //Validate domain name upon typing
    $("input[name='existing_domain_name']").on("keypress", function(e) {

        if(e.keyCode != 13) {

            var the_domain = $(this).val();

            the_domain = the_domain.replace("http://", "");
            the_domain = the_domain.replace("https://", "");
            the_domain = the_domain.replace("www", "");

            var reg = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/;

            console.log(the_domain);

            if(reg.test(the_domain) == false) {
               $(this).focus();
               $("#exist_message").text("Please enter a valid domain name");
               $("#existing_domain_btn").prop('disabled', true);
            } else {
                $("#exist_message").text("");
                $("#existing_domain_btn").prop('disabled', false);
            }

        }

    });

    //Submitting existing domain
    $("#existing_domain_form").on("submit", function(e) {

        e.preventDefault();

        var dataString = $("#existing_domain_form").serialize();
        console.log(dataString);
        var url = $("input[name='existing_domain_url']").val();
        console.log(url);

        $.ajax( {
            type: 'POST',
            url: url,
            data: dataString,
            dataType: 'json',
            beforeSend: function() {
                $("#existing_domain_btn").text("Loading...");
                $("#existing_domain_btn").prop('disabled', true);
            },
            success: function(data) {
                console.log(data);
                $("#existing_domain_btn").text("Submit");
                $("#existing_domain_btn").prop('disabled', false);

                if(data.success) {
                    //window.location.href = data.redirect;
                    $("#callback_text").html("<h4 class='alert alert-success alert-dismissable'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a> "+data.message+"</h4>")
                    location.reload();
                } else {
                    $("#callback_text").html("<h4 class='alert alert-danger alert-dismissable'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a> "+data.message+"</h4>")
                }
            }
        });

    });

    /*
    $("#domain_name" ).on('keypress',function() {

        var the_domain = $(this).val();

        // strip off "http://" and/or "www."
        the_domain = the_domain.replace("http://","");
        the_domain = the_domain.replace("www.","");

        var reg = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/;
        if(reg.test(the_domain) == false) {
           $('#domain_name').focus();
           $('.rez').text('Please Enter a Valid Domain Name');
        } else { $('.rez').text('Valid Domain Name');}

    });
    */
    console.log("Domains Loaded"); 

    $("#domainUrl").val();

    $.ajax({
        type: "GET",
        url: $("#domainUrl").val(),
        data: {
            domain_tld_get: "1",
            domain : $("#getDomain").val(),
            suffix : $("#suffix").val()
        },
        beforeSend: function() {
            console.log("Sending..");
        },
        success: function(e) {
            $(".fetch-available-domains").hide();
            $(".availableDomains").show();
            var c = jQuery.parseJSON(e);
            
            var htmlInsertString = "";
            var other_ext = false;
            var user_id = $("#user_agent_id").val(); 
            console.log(user_id); 
            if(c.status){
                for(var i = 0; i < c.domainCount; i ++) {
                    htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+c.domains[i].DomainName+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-user_id="'+user_id+'" data-domain_name="'+c.domains[i].DomainName+'" >Reserve this domain free for me</a></div></li>';    
                }
            }
            //alert(htmlInsertString);
            if( htmlInsertString == "" )
            {
                htmlInsertString += '<li style="line-height: 40px;"><div class="col-md-5 col-sm-5">No Domain Available!</div></li>';
            }

            $("#awsDomainResult ul.domain-list").html(htmlInsertString);
            $(".purchase-domain").on("click", show_domain_modal);
        }
    });

    var purchase_domain = function () {
        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        var user_id = $("#user_agent_id").val();
        var url = $("#purchaseDomainUrl").val();
        var user_name = $("#user_name").val();
        var site_dir = $("#domainDirectory").val();

        $.ajax({
            type: "GET",
            url: url,
            data: {
                domain_name: domain_name,
                agentId : user_id,
                site_dir : site_dir,
                user_name : user_name
            },
            dataType : "json",
            beforeSend: function() {
                console.log("Site DIR: " + site_dir);
                console.log("Sending.. " );
                $this.text("Loading...");
            },
            success: function(data) {
                window.location.href = data.redirect;
                //location.reload();            
            }
        });
        //console.log(url);
    };

    var show_domain_modal = function () {        
       
        var $this = $(this);            
        var domain_name = $this.data("domain_name");

        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);

        $("#domainModal").modal('show');
        $(".confirm-purchase-domain").on("click",purchase_domain);
    };
});


// New design
$(document).ready(function () {

    $(".domain-choose .btn-continue").on("click", function(e){
        var domainVal = $(".new-existing-domain input[name='domain-name']").val();
        $(".domain-section").hide();
        if (domainVal == 'new-domain') {
            $(".domain-preferred").show();
            $(".own-domain-hide").show();
        } else if (domainVal == 'existing-domain'){
            $(".domain-exist").show();
            $(".own-domain-hide").hide();
        }else{
            $('.domain-area').text(domainVal);
            $('.domain-name-selected').text(domainVal);
            $('.domain-confirmation').show();

            $(".domain-confirmation .btn-back").on("click", function(){
                $(".domain-confirmation").hide();
                $(".domain-choose").show();
            });

            $('.reserve-domain').attr("data-domain-name",domainVal);
            $(".domain-confirmation .btn-continue").on("click", aws_purchase_domain);  
        };
    });

    $(".new-existing-domain input[type='radio']").on("change", function(){
        var domainVal = $(this).val();
        console.log(domainVal);
        $(".domain-choose .btn-continue").on("click", function(e){
            $(".domain-section").hide();
            if (domainVal == 'new-domain') {
                $(".domain-preferred").show();
                $(".own-domain-hide").show();
            } else if (domainVal == 'existing-domain'){
                $(".domain-exist").show();
                $(".own-domain-hide").hide();
            }else{
                $('.domain-area').text(domainVal);
                $('.domain-name-selected').text(domainVal);
                $('.domain-confirmation').show();

                $(".domain-confirmation .btn-back").on("click", function(){
                    $(".domain-confirmation").hide();
                    $(".domain-choose").show();
                });

                $('.reserve-domain').attr("data-domain-name",domainVal);
                $(".domain-confirmation .btn-continue").on("click", aws_purchase_domain);  
            };
        });
    });

    $(".domain-options input[type='radio']").on("change", function(){
        var domainVal = $(this).val();
        var url = $("#purchase_url").val();
        console.log(domainVal);  
        $(".domain-new .btn-continue").on("click", function(e){
            e.preventDefault();
            $(".domain-section").hide();
            if (domainVal != 'custom-domain') {
                $(".domain-check").show();
                
                $('.domain-section').hide();
                $('.domain-area').text(domainVal);
                $('.domain-name-selected').text(domainVal);
                $('.domain-confirmation').show();
                

                $(".domain-confirmation .btn-back").on("click", function(){
                    $(".domain-confirmation").hide();
                    $(".new-existing-domain").show();
                });
                $('.reserve-domain').attr("data-domain-name",domainVal);
                $(".domain-confirmation .btn-continue").on("click", aws_purchase_domain);  

            } else {
                $(".domain-preferred").show();                
                return false;
            }
        });
    });

    // $(".domain-new .btn-back").on("click", function(){
    //     $(".domain-new").hide();  
    //     $(".domain-section").hide();      
    //     $(".domain-choose").show();
    // });

    // $(".domain-exist .btn-continue").on("click", function(){
    //     $('.domain-section').hide();
    //     // $(".domain-new").hide();  
    //     $('.domain-success').show();
    // });

    $(".domain-exist .btn-back").on("click", function(){
        $(".domain-section").hide();
        // $(".domain-new").hide();  
        $(".domain-choose").show();
    });
    
    $(".btn-finish").on("click", function(){
        window.location.href = base_url + '/custom_domain/domain';
    });

    $(".domain-preferred .dropdown-menu a").on("click", function(){
        var domain_extension = $(this).text();
        $("#domain-suffix").val(domain_extension);
        $(".domain-preferred .dropdown-toggle").html(domain_extension + " <span class='fa fa-chevron-down'></span>");
    });

    $(".domain-preferred .btn-back").on("click", function(){
        $(".domain-section").hide();
        $(".domain-choose").show();
    });

    $(".domain-preferred .btn-search").on("click", function(){
        $(".domain-preferred .domain-options").hide();
        $(".domain-preferred .error-nomatch-mess").hide();
        $(".domain-preferred .button-action .btn-continue").hide();
        $(".domain-preferred .confirmation-mess").hide();
        var $this = $(this);
        $this.button('loading');
        var domain_name = $("#domain-input").val();
        var domain_suffix = $("#domain-suffix").val();
        var url_checker = $("#domain-url").val();
        var suggested_url = $("#suggest-url").val();
        var $return;
       
       if(domain_name) {
        $(".domain-preferred .domain-empty").hide();

        $.ajax({
            type: "GET",
            url: url_checker,
            data: {
                domain_name: domain_name,
                suffix: domain_suffix,
            },
            dataType : "json",
            beforeSend: function() {
                $this.text("Loading...");
            },
            success: function(data) {
                console.log(data)
                if(data.isInValidDomain)
                {
                    $(".domain-preferred .domain-available").hide();
                    $(".domain-preferred .domain-options").hide();
                    $(".domain-preferred .error-nomatch-mess").hide();
                    $(".domain-inValid").show();
                    $this.button('reset');
                }else{
                                  
                    if(data.status == true){
                        $this.button('reset');
                        $(".domain-inValid").hide();
                        $(".domain-avail").text(data.domainName);
                        $(".domain-name-selected").text(data.domainName);
                        $(".domain-avail-input").val(data.domainName);
                        $(".domain-preferred .domain-options").hide();
                        $(".domain-preferred .button-action").hide();
                        $(".domain-preferred .error-nomatch-mess").hide();
                        $(".domain-preferred .domain-available").show();

                        // $(".domain-available .btn-continue").on("click", function(e){
                        //     //$(".domain-section").hide();
                        //     if (data.domainName == 'custom-domain') {
                        //         $(".domain-preferred").show();
                        //     } else {
                                
                        //         $(".domain-preferred .btn-back").on("click", function(){
                        //             $(".domain-confirmation").hide();
                        //             $(".domain-new").show();
                        //         });

                        //         $(".aws-confirm-purchase-domain").attr("data-domain-name", data.domainName);
                        //         //$("#domainModal-confirmation").modal("show");
                        //         $(".aws-confirm-purchase-domain").on("click",aws_purchase_domain);            
                        //     }
                        // });  
                        $('.domain-available .btn-continue').attr("data-domain-name",data.domainName);
                        $(".domain-available .btn-continue").on("click", aws_purchase_domain); 

                        $(".domain-available .btn-back").on("click", function(){
                            $(".domain-preferred").hide();
                            $(".new-existing-domain").show();
                        });

                    }else{
                        $(".domain-preferred .domain-available").hide();
                        $(".domain-inValid").hide();
                        $.ajax({
                            type: "GET",
                            url: suggested_url,
                            data: {
                                domain: data.domainName,
                            },
                            dataType : "json",
                            beforeSend: function(){
                                console.log('loader')
                            },success: function(data){
                                $domain = "";
                                if(data.domainCount > 0){
                                    $(".domain-preferred .error-nomatch-mess").show();
                                    for (var i = 0; i < data.domainCount; i++) {
                                        $domain += '<div class="form-group">';
                                        $domain += '<label><input type="radio" class="option-input radio" name="domain-name" value="'+data.domains[i].DomainName+'" />';
                                        $domain += data.domains[i].DomainName;
                                        $domain += '</label></div>';
                                    }
                                }else{
                                   //$domain = "<br /><br /><p class='label label-warning'>No suggestion domain result. Please search another custom domain!</p>";
                                    $domain = "<br /><br /><p class='label label-warning'>Sorry this name is not available search again!</p>";
                                }
                                $("#suggestedList").html($domain);
                                $(".domain-preferred .domain-options").show();
                                $(".domain-preferred .button-action").show();
                                $this.button('reset');

                                $(".domain-options input[type='radio']").on("change", function(){
                                    var domainVal = $(this).val();
                                    var url = $("#purchase_url").val();
                                    $(".confirmation-mess").show();
                                    $('.domain-name-selected').text(domainVal);
                                    $(".domain-preferred .button-action .btn-continue").show();
                                    $('.domain-preferred .btn-continue').attr("data-domain-name",domainVal);
                                    $(".domain-preferred .btn-continue").on("click", aws_purchase_domain); 
                                    // $(".domain-preferred .btn-continue").on("click", function(e){
                                    //     //$(".domain-section").hide();

                                    //     if (domainVal == 'custom-domain') {
                                    //         $(".domain-preferred").show();
                                    //     } else {
                                            
                                    //         $(".domain-option .btn-back").on("click", function(){
                                    //             $(".domain-confirmation").hide();
                                    //             //$(".domain-new").show();
                                    //         });
                                    //         $(".aws-confirm-purchase-domain").attr("data-domain-name", domainVal);
                                    //         $("#domainModal-confirmation").modal("show");
                                    //         $(".aws-confirm-purchase-domain").on("click",aws_purchase_domain);            
                                    //     }
                                    // });

                                });                            
                            }
                        })

                        $(".domain-options .btn-back").on("click", function(){
                            $(".domain-preferred").hide();
                            // $(".domain-new").show();
                        });   
                    }   
                }

            }
        });
       } else {
        $(".domain-preferred .domain-empty").show();
        $this.button('reset');
       }

    });

    var aws_purchase_domain = function () {
        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        var url = $("#purchase_url").val();

        $.ajax({
            type: "GET",
            url: url,
            data: {
                domain_name: domain_name,
            },
            dataType : "json",
            beforeSend: function() {
                $this.text("Loading...");
            },
            success: function(data) {
                $("#domainModal-confirmation").modal("hide");
                $(".domain-confirmation").hide();
                $(".domain-success_mess").show();  
                $(".domain-preferred").hide();
            }
        });
        //console.log(url);
    };

    //Submitting existing domain
    $(".existing-domain-continue").on("click", function(e) {
        var $this = $(this);
        e.preventDefault();

        var domain_name = $("input[name='existing_domain_name']").val();
        console.log(domain_name);
        var url = $("input[name='existing_domain_url']").val();
        console.log(url);

        $.ajax( {
            type: 'POST',
            url: url,
            data: {existing_domain_name:domain_name},
            dataType: 'json',
            beforeSend: function() {
                $this.text("Loading...");
                $this.prop('disabled', true);
            },
            success: function(data) {
                console.log(data);
                $this.text("Continue");
                $this.prop('disabled', false);

                if(data.success) {
                    $("#callback_text").html("<h4 class='alert alert-success alert-dismissable'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a> "+data.message+"</h4>")
                    $('.domain-section').hide();
                    $('.domain-name-selected').text(domain_name);
                    $('.domain-success_mess').show();
                    // location.reload();
                } else {
                    $("#callback_text").html("<h4 class='alert alert-danger alert-dismissable'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a> "+data.message+"</h4>")
                }
            }
        });

    });

    

});