$(document).ready(function () {

    var spw_username = "";

    $(".custom_domain_button").click(function(){
        var $this = $(this);            
        var spw_username = $(this).attr("data-username");
        var suggested_domain_name = $(this).attr("data-domain-suggest");
        var site_dir = $(this).attr("data-domain-directory"); //data-domain-directory="<?php echo $siteDir;?>"
        var site_city = $(this).attr("data-domain-city");
        var site_state = $(this).attr("data-domain-state");

        $("#domain_suggest").text(suggested_domain_name+".com");
        $("#domainDirectory").val(site_dir);
        $("#domainCity").val(site_city);
        $("#domainState").val(site_state);

        console.log(spw_username);

        $("#domain_suggest").click(function(){
            $("#domain_name").val(suggested_domain_name);
            $("#domain-form").submit();
        });

        $("#domain-form").on("submit", function(eve) {
            //console.log($("#web_type").val());
            eve.preventDefault();

            $("#check_domain_sbt").prop('disabled', true);

            var datastring = $("#domain-form").serialize();
            $.ajax({
                type: "GET",
                url: $("#domain-form").attr('action'),
                data: datastring,
                beforeSend: function() {
                    $("#checkDomain").hide("fast");
                    $("#checkDomainLoad").show("fast");
                    $("#fetch-available-domains").show("fast");
                    $("#domainResultAv").css("display", "none");
                    $(".availableDomains").css("display", "none");
                },
                success: function(e) {
                    $("#checkDomain").show("fast");
                    $("#checkDomainLoad").hide("fast");
                    $("#fetch-available-domains").hide("fast");

                    var c = jQuery.parseJSON(e);   
                    var domainName = $.map(c.domainName, function(value, index) {
                        return [value];
                    });

                    if(c.status == 1) {
                        var domainPrice = $.map(c.domainPrice, function(value, index) {
                            return [value];
                        });
                        $("#domainResult").html("<h4>Domain <span>" + domainName + "</span> ($"+ domainPrice +") is available. Click <a href='javascript:void(0)' class='purchase-domain' data-domain_name_avail="+domainName+" data-domain_directory="+site_dir+" data-domain_price="+domainPrice+" >here</a> to reserve this domain. </h4>"); 
                        $("#domainResultAv").css("display", "none");
                        $(".availableDomains").css("display", "none");
                    }
                    else {
                        console.log(c);
                        $("#domainResult").html("<h4>Domain <span>" +c.domain+"."+c.tld + "</span> is already taken.</h4>"); 
                        $.ajax({
                            type: 'GET',
                            url: $("#domainUrl").val(),
                            data: {
                                domain_tld_get: "1",
                                domain : $("#domain_name").val()
                            },
                            beforeSend: function() {
                                console.log("Sending..");
                                $("#fetch-available-domains").show("fast");
                            },
                            success: function(e) {
                                $("#fetch-available-domains").hide("fast");
                                $("#domainResultAv").show(); 
                                $(".availableDomains").show();

                                var c = jQuery.parseJSON(e);
                                //console.log(c.tldPrice);
                                var domainCount = $.map(c.domainCount, function(value, index) {
                                    return [value];
                                });

                                var splitStr = '';
                                var domainStatus = [];
                                var statusCode = [];
                                console.log(c.domains.length);

                                for(var i = 0; i < c.domains.length; i++) {
                                    splitStr = c.domains[i].split("_");
                                    for (var j = 0; j < domainCount; j++) {
                                        if( splitStr[0] == "Domain" + (j + 1) ) {
                                            domainStatus.push(splitStr[1]); 
                                        }
                                        if( splitStr[0] == "RRPCode" + (j + 1) ) {
                                            statusCode.push(splitStr[1]); 
                                        }
                                    }
                                }
                                var htmlInsertString = "";
                                var other_ext = false;
                                for(var i = 0; i < domainStatus.length; i ++) {
                                    console.log( c.tldPrice.product.length );
                                    var getDomainName = domainStatus[i].split(".");
                                    allowDomainExt  = ["com","org","net"];

                                    for(var x = 0; x < c.tldPrice.product.length; x ++) {

                                        if( c.tldPrice.product[x].tld == getDomainName[1] )
                                        {
                                            var domainPrice = c.tldPrice.product[x].registerprice;
                                            //c.tldPrice.product[x].tld
                                            break;
                                        }

                                    }

                                    if(statusCode[i] == '210') {
                                        var domainName = domainStatus[i].split(".");
                                        var user_id = $("#user_agent_id").val();

                                         // var domainPriceIn = $.map(c.domainPrice, function(value, index) {
                                         //        return [value];
                                         //    });

                                        if(jQuery.inArray(domainName[1], allowDomainExt) != -1) {
                                            other_ext = true;
                                            // Domain is Available
                                            htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+domainStatus[i]+' - $'+ domainPrice +'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-user_id="'+user_id+'" data-domain_name="'+domainStatus[i]+'" data-domain_price="'+domainPrice+'" >Reserve this domain for me</a></div></li>';    
                                        }
                                    }
                                }
                                if(!other_ext) {
                                    for(var i = 0; i < domainStatus.length; i ++) {
                                        if(statusCode[i] == '210') {                    
                                            var domainName = domainStatus[i].split(".");
                                            var user_id = $("#user_agent_id").val();                              
                                            htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+domainStatus[i]+' - $'+ domainPrice +'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-user_id="'+user_id+'" data-domain_name="'+domainStatus[i]+'" data-domain_price="'+domainPrice+'" >Reserve this domain for me</a></div></li>';                 
                                        }
                                    }
                                }
                                if(htmlInsertString == "") {
                                    htmlInsertString += '<li style="line-height: 40px;"><div class="col-md-5 col-sm-5">No Domain Available!</div></li>';
                                }
                                $("#domainResultAv ul.domain-list").html(htmlInsertString);   
                                $(".purchase-domain").on("click", show_domain_modal);
                                console.log(domainStatus);
                                console.log(statusCode);
                            }
                        });
                    }
                    $('#check_domain_sbt').prop('disabled', false);
                    $(".purchase-domain").on("click", show_domain_modal_available);
                } 
            });
        });
        
        var purchase_domain_avail = function (event) {
            var $this = $(this);            
            var domain_name = $this.data("domain-name");
            var user_id = $("#user_agent_id").val();
            var url = $("#purchaseDomainUrl").val();
            var user_name = spw_username;
            var type = "spw";
            var site_dir = $("#domainDirectory").val();
            var site_city = $("#domainCity").val();
            var site_state = $("#domainState").val();

            $.ajax({
                type: "GET",
                url: url,
                data: {
                    domain_name: domain_name,
                    agentId : user_id,
                    user_name : user_name,
                    site_dir : site_dir,
                    city : site_city,
                    state : site_state,
                    type : type
                },
                dataType : "json",
                beforeSend: function() {
                    console.log("Sending.." + site_dir);
                    $this.text("Loading...");
                },
                success: function(data) {
                    if(data.success) {    
                        $("#message-container").css("display", "block");
                        $("#return-message").text(data.message);
                        $("#domain_purchase_modal").modal('hide');
                        $("#domainResult").html("");
                        $("#domainResultAv").css("display", "none");
                        $(".availableDomains").css("display", "none");
                    }
                }
            });
            event.preventDefault();
        };

        var show_stripe_form = function( e ){
            e.preventDefault();
            var $this = $(this);        
            var $user_name = spw_username; 

            var $site_dir = $("#domainDirectory").val();
            var site_city = $("#domainCity").val();
            var site_state = $("#domainState").val();

            $("#domain_purchase_modal").modal('hide');
            //$("#domainmodal").modal('hide');

            $domainName = $this.data("domain-name");
            $domainPrice = $this.data("domain-price");

            $("#cart-domain-name").html($domainName);
            $("#cart-domain-price").html($domainPrice);

            $("#cart-total-amount").html( $this.data("domain-price"));

            //append to input form hidden
            $("#form_domain_name").val( $domainName );
            $("#form_domain_price").val( $domainPrice );
            $("#form_user_name").val( $user_name );
            $("#form_site_dir").val( $site_dir );
            $("#form_site_city").val( site_city );
            $("#form_site_state").val( site_state );

            $("#stripeFormModal").modal('show');
        }

        var show_domain_modal_available = function () {        
           
            var $this = $(this);            
            //var domain_name = $(this).attr("data-domain_name_avail");
            var domain_name = $(this).data("domain_name_avail");
            var domain_price = $this.data("domain_price");

            $(".confirm-purchase-domain").attr("data-domain-name", domain_name);
            $(".direct-to-stripe").attr("data-domain-name", domain_name);
            $(".direct-to-stripe").attr("data-domain-price", domain_price);

            $("#domain_purchase_modal").modal('show');
            //$(".confirm-purchase-domain").on("click", purchase_domain_avail);
            $(".direct-to-stripe").on("click", show_stripe_form);
        };

        var purchase_domain = function(event) {
            var $this = $(this);            
            var domain_name = $this.data("domain-name");
            var user_id = $("#user_agent_id").val();
            var url = $("#purchaseDomainUrl").val();
            var user_name = spw_username;
            var type = "spw";

            $.ajax({
                type: "GET",
                url: url,
                data: {
                    domain_name: domain_name,
                    agentId : user_id,
                    user_name : user_name,
                    type : type
                },
                dataType : "json",
                beforeSend: function() {
                    console.log("Sending..");
                    $this.text("Loading...");
                    console.log(type);
                },
                success: function(data) {
                    if(data.success) {   
                        //message here   
                        $("#message-container").css("display", "block");
                        $("#return-message").text(data.message);
                        $("#domain_purchase_modal").modal('hide');
                        $("#domainResult").html(""); 
                        $("#domainResultAv").css("display", "none");
                        $(".availableDomains").css("display", "none");
                    }
                }
            });

            event.preventDefault();
        };

        var show_domain_modal = function () {        

            var $this = $(this);            
            var domain_name = $this.data("domain_name");
            var domain_price = $this.data("domain_price");

            $(".confirm-purchase-domain").attr("data-domain-name", domain_name);
            $(".direct-to-stripe").attr("data-domain-name", domain_name);
            $(".direct-to-stripe").attr("data-domain-price", domain_price);

            $("#domain_purchase_modal").modal('show');
            //$(".confirm-purchase-domain").on("click", purchase_domain);
            $(".direct-to-stripe").on("click", show_stripe_form);
        };
    });

    //reset schedule showing form modal after submit
    $('#domainmodal').on('hidden.bs.modal', function(){
        /*$(this).find('form')[0].reset();
        $("#message-container").css("display", "none");*/
        location.reload();
    });

    $('#domain_purchase_modal').on('hidden.bs.modal', function(){
        $(".confirm-purchase-domain").attr("data-domain-name", "");
        $(".confirm-purchase-domain").text("Continue");
    });

});
