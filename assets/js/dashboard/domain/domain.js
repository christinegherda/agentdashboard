(function($) {

    var sbt_domain = function (eve) {

        eve.preventDefault();

        $("#check_domain_sbt").prop('disabled', true);

        var datastring = $("#domain-form").serialize();

        $.ajax({

            type: "GET",
            url: $("#domain-form").attr('action'),
            data: datastring,
            beforeSend: function() {
                $("#checkDomain").hide("fast");
                $("#checkDomainLoad").show("fast");
            },
            success: function(e) {
                $("#checkDomain").show("fast");
                $("#checkDomainLoad").hide("fast");
                
                var c = jQuery.parseJSON(e);   
                // console.log(c.domainName);
                var domainName = $.map(c.domainName, function(value, index) {
                    return [value];
                });

                if( c.status == 1 ) {
                    // checkDomain
                    var domainPrice = $.map(c.domainPrice, function(value, index) {
                        return [value];
                    });
                    /*$("#domainResult").html("<h4>Domain <span>" + domainName + "</span> is available at price <strong>$" + domainPrice + "</strong>. Click <a href='javacript:void(0);'>here</a> to purchase this domain</h4>"); */
                    $("#domainResult").html("<h4>Domain <span>" + domainName + "</span> is available. Click <a href='javascript:void(0)' class='purchase-domain' data-domain_name_avail="+domainName+" >here</a> to reserve this domain</h4>"); 
                }
                else {
                    console.log(c);
                    window.location = $("#domainUrl").val() + "?domain=" + c.domain + "&tld=" + c.tld + "&status=0";
                    // $("#domainResult").html("<h4 class='taken'><span>"+domainName+"</span> is already taken.</h4><h4 class='featured'>Featured Domains:</h4>");
                }

                $('#check_domain_sbt').prop('disabled', false);
                $(".purchase-domain").on("click", show_domain_modal_available);
            }

        });

    }

    $("#domain-form").on("submit", sbt_domain);

    var purchase_domain_avail = function () {

        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        var user_id = $("#user_agent_id").val();
        var url = $("#purchaseDomainUrl").val();
        var user_name = $("#user_name").val();
        var site_dir = $("#domainDirectory").val();

        $.ajax({
            type: "GET",
            url: url,
            data: {
                domain_name: domain_name,
                agentId : user_id,
                site_dir : site_dir
            },
            dataType : "json",
            beforeSend: function() {
                console.log("Sending.. Site DIR: " + site_dir);
                $this.text("Loading...");
            },
            success: function(data) {
                
                if(data.success)
                {   
                    location.reload();
                    $("#domainModal").fadeOut("slow", function () {
                        $('#domainModal').modal('hide');
                    });

                    // $( "#custom_domain_panel" ).fadeOut( "slow", function() {
                    //     $("#custom_domain_panel").hide();
                    //     var message = '<div class="alert alert-success" role="alert">'+data.message+'<div>';
                    //     $(".response_domain").append(message);
                    // });           
                }
                else
                {   
                    location.reload();
                    $("#domainModal").fadeOut("slow", function () {
                        $('#domainModal').modal('hide');
                    });
                    // $this.text("reserve this domain free for me");
                    // var message = '<div class="alert alert-danger" role="alert">'+data.error+'<div>';
                    // $(".moreDetails").append(message);
                }
            
            }
        });

        console.log(url);
    };

    var show_domain_modal_available = function () {        

        var $this = $(this);            
        var domain_name = $(this).attr("data-domain_name_avail");
        
        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);

        $("#domainModal").modal('show');
        $(".confirm-purchase-domain").on("click",purchase_domain_avail);
    };

})(jQuery);

$(document).ready(function () {

    $("#domain_yes").click(function(){
        $("#domain-result").hide();
        $("#existing_domain").show();
        $("#domain_container").hide();
        $("#subdomain_container").show();
    });

    $("#domain_no").click(function() {
        $("#domain-result").show();
        $("#existing_domain").hide();
        $("#domain_container").show();
        $("#subdomain_container").show();
    });

    //Saving sub domain
    $("#save_sub_domain").click(function() {

        var url = $("input[name='sub_domain_url']").attr('value');
        var subdomain_name = $("input[name='subdomain_name']").attr('value');
        var user_id = $("#user_agent_id").val();

        $('#save_sub_domain').attr('disabled','disabled');
        $('#subdomaininfo').css("display","none");
        $('#subdomainstatus').css("display","block");
        
        $.ajax({
            type: 'POST',
            url: url,
            data: { 
                domain_name: subdomain_name,
                agentId: user_id,
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                window.location.href = data.redirect;
            }
        });
    });
    
    //Validate domain name upon typing
    $("input[name='existing_domain_name']").on("keypress", function(e) {

        if(e.keyCode != 13) {

            var the_domain = $(this).val();

            the_domain = the_domain.replace("http://", "");
            the_domain = the_domain.replace("https://", "");
            the_domain = the_domain.replace("www", "");

            var reg = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/;

            console.log(the_domain);

            if(reg.test(the_domain) == false) {
               $(this).focus();
               $("#exist_message").text("Please enter a valid domain name");
               $("#existing_domain_btn").prop('disabled', true);
            } else {
                $("#exist_message").text("");
                $("#existing_domain_btn").prop('disabled', false);
            }

        }

    });

    //Submitting existing domain
    $("#existing_domain_form").on("submit", function(e) {

        e.preventDefault();

        var dataString = $("#existing_domain_form").serialize();
        console.log(dataString);
        var url = $("input[name='existing_domain_url']").val();
        console.log(url);

        $.ajax( {
            type: 'POST',
            url: url,
            data: dataString,
            dataType: 'json',
            beforeSend: function() {
                $("#existing_domain_btn").text("Loading...");
                $("#existing_domain_btn").prop('disabled', true);
            },
            success: function(data) {
                console.log(data);
                $("#existing_domain_btn").text("Submit");
                $("#existing_domain_btn").prop('disabled', false);

                if(data.success) {
                    //window.location.href = data.redirect;
                    $("#callback_text").html("<h4 class='alert alert-success alert-dismissable'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i> "+data.message+"</h4>")
                    location.reload();
                } else {
                    $("#callback_text").html("<h4 class='alert alert-danger alert-dismissable'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i> "+data.message+"</h4>")
                }
            }
        });

    });

    /*
    $("#domain_name" ).on('keypress',function() {

        var the_domain = $(this).val();

        // strip off "http://" and/or "www."
        the_domain = the_domain.replace("http://","");
        the_domain = the_domain.replace("www.","");

        var reg = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/;
        if(reg.test(the_domain) == false) {
           $('#domain_name').focus();
           $('.rez').text('Please Enter a Valid Domain Name');
        } else { $('.rez').text('Valid Domain Name');}

    });
    */
    console.log("Domains Loaded"); 

    $("#domainUrl").val();

    $.ajax({
        type: "GET",
        url: $("#domainUrl").val(),
        data: {
            domain_tld_get: "1",
            domain : $("#getDomain").val(),
            suffix : $("#suffix").val()
        },
        beforeSend: function() {
            console.log("Sending..");
        },
        success: function(e) {
            $(".fetch-available-domains").hide();
            $(".availableDomains").show();
            var c = jQuery.parseJSON(e);
            // var domainCount = c.tlds.DomainCount[0];   

            var domainCount = $.map(c.domainCount, function(value, index) {
                return [value];
            });
            var splitStr = '';
            var domainStatus = [];
            var statusCode = [];
            console.log(c);
            for(var i = 0; i < c.domains.length; i ++ ) {
                splitStr = c.domains[i].split("_");
                for (var j = 0; j < domainCount; j++) {
                    if( splitStr[0] == "Domain" + (j + 1) ) {
                        domainStatus.push(splitStr[1]); 
                    }
                    if( splitStr[0] == "RRPCode" + (j + 1) ) {
                        statusCode.push(splitStr[1]); 
                    }
                }
            }
            var htmlInsertString = "";
            var other_ext = false;
            for(var i = 0; i < domainStatus.length; i ++) {
                if(statusCode[i] == '210') {                    
                    var domainName = domainStatus[i].split(".");
                    var user_id = $("#user_agent_id").val();                    
                    
                    allowDomainExt  = ["com","org","net"];

                    if( jQuery.inArray(domainName[1], allowDomainExt) != -1 )
                    {
                        other_ext = true;
                        // Domain is Available
                        $("#domain_container").show();
                        htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+domainStatus[i]+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-user_id="'+user_id+'" data-domain_name="'+domainStatus[i]+'" >Reserve this domain free for me</a></div></li>';    
                    }
                                        
                }
            }

            if(!other_ext)
            {
                for(var i = 0; i < domainStatus.length; i ++) {
                    if(statusCode[i] == '210') {                    
                        var domainName = domainStatus[i].split(".");
                        var user_id = $("#user_agent_id").val();                    
                                               
                            // Domain is Available
                            $("#domain_container").show();
                            htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+domainStatus[i]+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-user_id="'+user_id+'" data-domain_name="'+domainStatus[i]+'" >Reserve this domain free for me</a></div></li>';                 
                    }
                }
            }

            //alert(htmlInsertString);
            if( htmlInsertString == "" )
            {
                //$("#domain_container").show();
                htmlInsertString += '<li style="line-height: 40px;"><div class="col-md-5 col-sm-5">No Domain Available!</div></li>';
            }

            $("#domainResult ul.domain-list").html(htmlInsertString);
            $(".purchase-domain").on("click", show_domain_modal);

            console.log(domainStatus);
            console.log(statusCode);
        }
    });

    var purchase_domain = function () {
        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        var user_id = $("#user_agent_id").val();
        var url = $("#purchaseDomainUrl").val();
        var user_name = $("#user_name").val();
        var site_dir = $("#domainDirectory").val();

        $.ajax({
            type: "GET",
            url: url,
            data: {
                domain_name: domain_name,
                agentId : user_id,
                site_dir : site_dir,
                user_name : user_name
            },
            dataType : "json",
            beforeSend: function() {
                console.log("Site DIR: " + site_dir);
                console.log("Sending.. " );
                $this.text("Loading...");
            },
            success: function(data) {
                
                location.reload();

                // if(data.success)
                // {
                //     location.reload();

                    // $("#domainModal").fadeOut("slow", function () {
                    //     $('#domainModal').modal('hide');
                    // });

                    // $( "#custom_domain_panel" ).fadeOut( "slow", function() {
                    //     $("#custom_domain_panel").hide();
                    //     var message = '<div class="alert alert-success" role="alert">'+data.message+'<div>';
                    //     $(".response_domain").append(message);
                    // });           
                // }
                // else
                // {
                //     location.reload(); 

                    // $("#domainModal").fadeOut("slow", function () {
                    //     $('#domainModal').modal('hide');
                    // });
                    // $this.text("reserve this domain free for me");
                    // var message = '<div class="alert alert-danger" role="alert">'+data.error+'<div>';
                    // $(".moreDetails").append(message);
                //}
            
            }
        });
        //console.log(url);
    };

    var show_domain_modal = function () {        
       
        var $this = $(this);            
        var domain_name = $this.data("domain_name");

        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);

        $("#domainModal").modal('show');
        $(".confirm-purchase-domain").on("click",purchase_domain);
    };
});

// New design
$(document).ready(function () {

    $(".new-existing-domain input[type='radio']").on("change", function(){
        var domainVal = $(this).val();

        $(".domain-choose .btn-continue").on("click", function(e){
            $(".domain-section").hide();
            if (domainVal == 'new-domain') {
                $(".domain-new").show();
            } else if (domainVal == 'existing-domain'){
                $(".domain-exist").show();
            };
        });
    });

    $(".domain-options input[type='radio']").on("change", function(){
        var domainVal = $(this).val();

        $(".domain-new .btn-continue").on("click", function(e){
            $(".domain-section").hide();
            if (domainVal == 'custom-domain') {
                $(".domain-preferred").show();
            } else {
                $(".domain-check").show();
                setTimeout(function(){
                   $('.domain-section').hide();
                   $('.domain-success').show();
                }, 3000);
            }
        });
    });

    $(".domain-new .btn-back").on("click", function(){
        $(".domain-section").hide();
        $(".domain-choose").show();
    });

    $(".domain-exist .btn-continue").on("click", function(){
        $('.domain-section').hide();
        $('.domain-success').show();
    });

    $(".domain-exist .btn-back").on("click", function(){
        $(".domain-section").hide();
        $(".domain-choose").show();
    });
    
    $(".btn-finish").on("click", function(){
        $(".domain-section").hide();
        $(".domain-review").show();
    });

    $(".domain-preferred .dropdown-menu a").on("click", function(){
        var domain_extension = $(this).text();
        $(".domain-preferred .dropdown-toggle").html(domain_extension + " <span class='fa fa-chevron-down'></span>");
    });

    $(".domain-preferred .btn-search").on("click", function(){
        $(".domain-preferred .domain-options").hide();

        var $this = $(this);
          $this.button('loading');
            setTimeout(function() {
               $this.button('reset');
               $(".domain-preferred .domain-options").show();
           }, 1000);

    });

    $(".domain-preferred .btn-continue").on("click", function(){
        $('.domain-section').hide();
        $(".domain-check").show();
        setTimeout(function(){
            $('.domain-section').hide();
            $('.domain-success').show();
        }, 3000);
    });
    
});
