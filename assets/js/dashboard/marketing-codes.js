/**
  Description: Used in settings page. I don't recall why I named this file marketing-codes.js, maybe that was not me after all.
  Author: Jovanni G
*/

jQuery(document).ready(function($){
  console.log('Settings Page');

  $('#update-button').on('click', function(e){
    // add loading page overlay here. It should be available on branch feature-theme
    var overlay = document.createElement('div');
    overlay.className = 'loader-overlay';
    $('body').append(overlay);

    $.ajax({
      url: '/settings/update',
      method: 'POST',
      dataType: 'json',
      data: {
        page: 'settings'
      },
      beforeSend: function(xhr){

        var fields = $('[data-field]');
        
        var data = {};
        for(var i = 0; i < fields.length; i++){
          switch($(fields[i]).attr('data-type')){
            case 'checkbox':
              data[$(fields[i]).attr('data-id')] = fields[i].checked;
              break;
            default:
              data[$(fields[i]).attr('data-id')] = fields[i].value;
          }
        }
        if(typeof this.data !== 'undefined'){
          this.data += '&' + $.param(data);
        }else{
          this['data'] = $.param(data);
        }

      },
      success: function(response){
        console.log(response);
        if(response.success){
          $(overlay).remove();
          var span = document.createElement('span');
          span.style.color = 'green';
          span.style.marginRight = '10px';
          span.textContent = response.message;

          $(e.target.previousSibling).remove();
          $(e.target).before(span);
          $(span).fadeOut(3000, function(e){
            $(span).remove();
          });
        }

        // remove save overlay here.
      },
      error: function(xhr){
        console.log(xhr);

        // remove save overlay here.
      }
    });



  }); // end of $('#update-button') click

  $('li[role="presentation"]').on('click', function(e){
    $('#update-button').prev().remove();
  });

  var multiple_checkboxes = $('[data-type="multiple-checkbox"]'); // get all dynamically created multiple checkboxes

  for(var i = 0; i < multiple_checkboxes.length; i++){ // loop through all multiple checkboxes

    var m_checkbox = multiple_checkboxes.get(i);

    m_checkbox['fillData'] = function(){

      var self = this;

      var field_value = ($(self).val() !== '')? JSON.parse($(self).val()) : '';
      var id = $(self).attr('data-id');
      var choices = $('[data-for="'+id+'"]'); // get the choices

      if(field_value !== ''){ // if there is already data from the database      
        var keys = Object.keys(field_value);

        keys.forEach(function(key, index){
          for(var j = 0; j < choices.length; j++){
            if($(choices[j]).attr('data-choice') === key){

              choices[j].checked = field_value[key];

              $(choices[j]).on('change', function(e){
                var key = $(this).attr('data-choice');

                if( $(self).val() !== ''){
                  var m_checkbox_value = JSON.parse($(self).val());
                  m_checkbox_value[key] = this.checked;

                  $(self).val(JSON.stringify(m_checkbox_value)); // update value of main field for every checkbox change
                  console.log(m_checkbox_value);
                }else{
                  field_choices[key] = this.checked;

                  $(self).val(JSON.stringify(field_choices)); // update value of main field for every checkbox change
                   console.log(field_choices);
                }
              });

              $('label[for="'+key+'"]').on('click', function(e){ // if respective input checkbox labels are clicked
                var key = $(this).attr('for');          
                var checkornot = !$('[data-choice="' + key + '"]').get(0).checked;
                $('[data-choice="' + key + '"]').get(0).checked = checkornot;

                if( $(self).val() !== '' ){
                  var m_checkbox_value = JSON.parse($(self).val());
                  m_checkbox_value[key] = checkornot;

                  $(self).val(JSON.stringify(m_checkbox_value)); // update value of main field for every checkbox change
                }else{
                  field_choices[key] = checkornot;

                  $(self).val(JSON.stringify(field_choices)); 
                }
              });
            }
          }
        });

      }else{ // if first time on this field type.
        var field_choices = {};

        for(var j = 0; j < choices.length; j++){

          var data_choice = $(choices[j]).attr('data-choice');
          field_choices[data_choice] = choices[j].checked;

          $(choices[j]).on('change', function(e){ // input check box change events updates the main field
            var key = $(this).attr('data-choice');

            if( $(self).val() !== '' ){
              var m_checkbox_value = JSON.parse($(self).val());
              m_checkbox_value[key] = this.checked;

              $(self).val(JSON.stringify(m_checkbox_value));
            }else{
              field_choices[key] = this.checked;

              $(self).val(JSON.stringify(field_choices));
            }
          });

          $('label[for="'+data_choice+'"]').on('click', function(e){ // if respective input checkbox labels are clicked
            var key = $(this).attr('for');          
            var checkornot = !$('[data-choice="' + key + '"]').get(0).checked;
            $('[data-choice="' + key + '"]').get(0).checked = checkornot;

            if( $(self).val() !== '' ){
              var m_checkbox_value = JSON.parse($(self).val());
              m_checkbox_value[key] = checkornot;

              $(self).val(JSON.stringify(m_checkbox_value));
            }else{
              field_choices[key] = checkornot;

              $(self).val(JSON.stringify(field_choices));
            }
          });

        }
        
        $(self).val(JSON.stringify(field_choices));
      }
    };

    m_checkbox.fillData();
  }

});

