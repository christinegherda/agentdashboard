$(document).ready(function() {

  $.ajax({
    url: base_url+'crm/leads/check_capture_leads',
    dataType: 'json',
    beforeSend: function() {
      console.log('check capture leads');
    },
    success: function(data) {
      console.log(data);
      if(data.capture_leads == 1) {
        $('input[name="my-checkbox"]').bootstrapSwitch('state', true);
        $("#lead_config_btn").prop("disabled", false);
      } else {
        $('input[name="my-checkbox"]').bootstrapSwitch('state', false);
        $("#lead_config_btn").prop("disabled", true);
      }

      if(data.is_email_market == 1) {
        $('input[name="email_marketing"]').bootstrapSwitch('state', true);
      } else {
        $('input[name="email_marketing"]').bootstrapSwitch('state', false);
      }
    }
  });

  if($("input[name='by_min_pass_chk']").is(":checked")) {
    $("#minute_capture").prop("disabled", false);
  } else {
    $("#minute_capture").prop("disabled", true);
  }

  if($("input[name='by_search_perf_chk']").is(":checked")) {
    $("#search_count").prop("disabled", false);
  } else {
    $("#search_count").prop("disabled", true);
  }

  if($("input[name='by_listing_view_count_chk']").is(":checked")) {
    $("#listing_view_count").prop("disabled", false);
  } else {
    $("#listing_view_count").prop("disabled", true);
  }  
  
});

(function($) {

    $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {

      console.log(this); // DOM element
      console.log(event); // jQuery event
      console.log(state); // true | false

      if(state) {
        $post = 'capture_leads';
        $("#lead_config_btn").prop("disabled", false);
      } else {
        $post = 'dont_capture';
        $("#lead_config_btn").prop("disabled", true);
      }

      $.ajax({
        url: $(this).attr("data-url"),
        type: "POST",
        data: {
          switch_post:$post
        },
        dataType:'json',
        beforeSend: function() {
          console.log(state);
        },
        success: function(data) {
          console.log(data);
        }
      });
    });

    $('input[name="email_marketing"]').on('switchChange.bootstrapSwitch', function(event, state) {

      console.log(this); // DOM element
      console.log(event); // jQuery event
      console.log(state); // true | false

      if(state) {
        $post = 'set_email';
      } else {
        $post = 'dont_set';
      }

      $.ajax({
        url: $(this).attr("data-url"),
        type: "POST",
        data: {
          switch_post:$post
        },
        dataType:'json',
        beforeSend: function() {
          console.log(state);
        },
        success: function(data) {
          console.log(data);
        }
      });

    });

    $("#lead_capture_config_form").on("submit", function(e) {

      e.preventDefault();

      $dataString = $(this).serialize();
      console.log($dataString);

      $.ajax({
        url: $(this).attr("action"),
        type: "POST",
        data: $dataString,
        dataType: 'json',
        beforeSend: function() {
          console.log("sending configuration");
          // if($("input[name='by_min_pass_chk']").is(":checked")) {
          //     $("#minute_capture").prop('disabled', false);
          // }
        },
        success: function(data) {
          console.log(data);
          if(data.success) {
            $("#ret_msg").html("<div class='alert alert-success'><strong>Success!</strong> Successfully Saved!</span>");
            setTimeout(function() {
              $("#ret_msg .alert").remove();
              $("#lead_config_modal").modal('toggle');
            }, 4000);
          } else {
            $("#ret_msg").html("<div class='alert alert-danger'><strong>Error! </strong> "+data.message+"</span>");
          }
          console.log("close leads modal");
        }
      });

    });

    $("input[name='by_min_pass_chk']").on("change", function() {
      if($(this).is(":checked")) {
        $("#minute_capture").prop("disabled", false);
      } else {
        $("#minute_capture").prop("disabled", true);
      } 
    });

    $("input[name='by_search_perf_chk']").on("change", function() {
      if($(this).is(":checked")) {
        $("#search_count").prop("disabled", false);
      } else {
        $("#search_count").prop("disabled", true);
      } 
    });

    $("input[name='by_listing_view_count_chk']").on("change", function() {
      if($(this).is(":checked")) {
        $("#listing_view_count").prop("disabled", false);
      } else {
        $("#listing_view_count").prop("disabled", true);
      } 
    });

    //Sync My Contacts
    $("#sync_contacts").on("click", function(e) {
        e.preventDefault();

        $.ajax({
            url: base_url + 'crm/leads/SaveContactList',
            dataType: 'json',
            beforeSend: function() {
                console.log("loading...");
                $("#sync_contacts").text("Syncing Contacts...");
            },
            success: function(data) {
                console.log(data);
                if(data.success) {
                    window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        });
    });

    //$("#sync_contacts").on("click", sync_contacts);

    //Sync My Saved Searches
    $("#sync_saved_searches").on("click", function(e) {
        e.preventDefault();

        $.ajax({
            url: 'property/saved_search_db',
            dataType: 'json',
            beforeSend: function() {
                console.log("loading...");
                $("#sync_saved_searches").text("Syncing Saved Searches...");
            },
            success: function(data) {
                console.log(data);
                window.location.href = data.redirect;
            }
        });
    });

    sbt_send_email = function(eve) {

      eve.preventDefault();
      var url = $(this).attr('action');
      var formData = $(this).serialize();// serializes the form's elements.
      console.log(formData);

       //reset form on close modal
      $('#SendEmail').on('hidden.bs.modal', function(){
        $('.note-editable').empty();
      });

      $.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "json",
        beforeSend: function(xhr) {
          $("#btn-send").text("Sending...");
          $("#btn-send").prop("disable", true);
        },
        success: function(data) {

          if(data.success) {
            $.msgBox({
                  title: "Success",
                  content: data.message,
                  type: "info"
              });

            location.reload();
          } else {
            $("#btn-send").text("Send");
            $("#btn-send").prop("disable", false);
              $.msgBox({
                    title: "Error",
                    content: data.message,
                    type: "error",
                    buttons: [{ value: "Ok" }]
                });
          }
          //location.reload(true);
        },
      });
    };

    view_email_content = function() {

      var message_id = $(this).attr("data-message-id");
      var url = base_url + 'crm/leads/get_message_contect_ajax?message_id=' + message_id;

      $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            success: function(data) {
              //var jqObj = jQuery(data); // You can get data returned from your ajax call here. ex. jqObj.find('.returned-data').html()
              // alert(data.message);
              $("#sent-email-content").html(data.message);
              $("#SentEmailView").modal("show");
            }
        });
    };

    sbt_add_notes = function(eve) {

        eve.preventDefault();

        var url = $(this).attr('action');
        var formData = $(this).serialize();
        
        $("#btn-notes").text("Sending...");

        $('.save-loading').show();

        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            dataType: "json",
            beforeSend: function(xhr) {
              $("#btn-send").prop("disable", true);
            },
            success: function(data) {

              if(data.success) {

                  $.msgBox({
                        title: "Success",
                        content: data.message,
                        type: "info"
                    });

                  location.reload();

              } else {
                
                $("#btn-notes").text("Submit");
                $("#btn-send").prop("disable", false);

                $.msgBox({
                      title: "Error",
                      content: data.message,
                      type: "error",
                      buttons: [{ value: "Ok" }]
                });
              }
            },
        });
    };

    sbt_add_tasks = function(eve) {

        eve.preventDefault();

        var url = $(this).attr('action');
        var formData = $(this).serialize();// serializes the form's elements.

        $("#btn-tasks").text("Sending...");

        $('.save-loading').show();

        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            dataType: "json",
            beforeSend: function(xhr) {
              $("#btn-tasks").prop("disable", true);
            },
            success: function(data) {

              if(data.success) {

                $.msgBox({
                    title: "Success",
                    content: data.message,
                    type: "info"
                });

                location.reload();

              } else {

                $("#btn-tasks").text("Submit");
                $("#btn-send").prop("disable", false);

                $.msgBox({
                  title: "Error",
                  content: data.message,
                  type: "error",
                  buttons: [{ value: "Ok" }]
                });
              }
            },
        });
    };

    var delete_notes = function(e){
      e.preventDefault();

      var url = $(this).attr("href");
      var noteid = $(this).data("id");
      var contact_id = $(this).data("contact-id");

      $.msgBox({
        title: "Are You Sure",
        content: "Would you like to delete notes?",
        type: "confirm",
        buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel"}],
        success: function (result) {
          if(result == "Yes") {
            $.ajax({
              url: url,
              type: "POST",
              data: {
                'noteid': noteid,
                'contact_id': contact_id
              },
              dataType: "json",
              success: function(data) {
                console.log(data);
                if(data.success) {

                  $("#notesRow"+noteid).fadeOut("slow", function() {
                      $("#notesRow"+noteid).hide();
                  });

                  $.msgBox({
                    title: "Success",
                    content: "Note Successfully Deleted!",
                    type: "info",
                    buttons: [{ value: "Ok" }]
                  });

                  location.reload();

                } else{
                  $.msgBox({
                    title: "Error",
                    content: "Failed to Delete Notes!",
                    type: "error",
                    buttons: [{ value: "Ok" }]
                  });
                }
              },
            });
          }
        }
      });


    };

    var delete_tasks = function(e){
      e.preventDefault();

      var url = $(this).attr("href");
      var taskid = $(this).data("id");
      var contact_id = $(this).data("contact-id");

      $.msgBox({
        title: "Are You Sure",
        content: "Would you like to delete task?",
        type: "confirm",
        buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel"}],
        success: function (result) {
          if (result == "Yes") {
            $.ajax({
                url: url,
                type: "POST",
                data: {
                  'taskid':taskid,
                  'contact_id':contact_id
                },
                dataType: "json",
                success: function(data) {
                  console.log(data);
                  if(data.success) {

                    $("#taskRow"+taskid ).fadeOut("slow", function() {
                        $("#taskRow"+taskid).hide();
                    });

                    $.msgBox({
                      title: "Success",
                      content: "Task Successfully Deleted!",
                      type: "info",
                      buttons: [{ value: "Ok" }]
                    });

                    location.reload();

                  }else{
                    $.msgBox({
                        title: "Error",
                        content: "Failed to Delete Tasks!",
                        type: "error",
                        buttons: [{ value: "Ok" }]
                    });
                  }
                },
            });
          }
        }
      });


    };

    var leads_send_email = function(e) {

      e.preventDefault();

      var href = $(this).attr("href");

      $("#sbt-send-email").attr("action", href);
      $("#sbt-send-email input[name='contact_id']").val($(this).attr("data-contact-id"));
      $("#SendEmailLead").modal("show");

    };

    var import_contacts_confirmation = function(e){
      e.preventDefault();

      var href = $(this).attr("href");

      $("#sbtImportContacts").attr("action",href );
      $("#importContacts").modal("show");
    };

    var delete_leads = function( eve ){

      eve.preventDefault();

      $href = $(this).attr("href");

      $.msgBox({
        title: "Are You Sure",
        content: "Would you like to delete the lead?",
        type: "confirm",
        buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel"}],
        success: function (result) {
          if (result == "Yes") {
            $.ajax({
                url: $href,
                type: "POST",
                dataType: "json",
                success: function(data) {
                  //console.log(data);
                    if( data.status )
                    {
                      //location.reload();
                      $( "#lead_tr_"+data.lead_id ).fadeOut( "slow", function() {
                          $("#lead_tr_"+data.lead_id).hide();
                      });
                    }else{
                      $.msgBox({
                          title: "Error",
                          content: "Failed to Delete Lead!",
                          type: "error",
                          buttons: [{ value: "Ok" }]
                      });
                    }
                },
            });
          }
        }
      });

    };


    var delete_spw_schedule = function( eve ){

      eve.preventDefault();
      var $this = $(this);
      $href = $(this).attr("href");

      $.msgBox({
        title: "Are You Sure",
        content: "Would you like to delete schedule?",
        type: "confirm",
        buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel"}],
        success: function (result) {
          if (result == "Yes") {
            $.ajax({
                url: $href,
                type: "POST",
                dataType: "json",
                success: function(data) {
                  console.log(data);
                    if( data.status )
                    {
                      //location.reload();
                      $this.closest('tr').fadeOut( "slow", function() {
                          $this.closest('tr').hide();
                      });
                    }else{
                      $.msgBox({
                          title: "Error",
                          content: "Failed to Delete Lead!",
                          type: "error",
                          buttons: [{ value: "Ok" }]
                      });
                    }
                },
            });
          }
        }
      });

    };

    $("#sbt-send-email").on("submit", sbt_send_email);
    $(".email-view").on("click", view_email_content );
    $("#sbt-add-notes").on("submit", sbt_add_notes);
    $("#sbt-add-tasks").on("submit", sbt_add_tasks);
    $(".delete_notes").on("click", delete_notes);
    $(".delete_tasks").on("click", delete_tasks);

    //leads table
    $(".leads_send_email").on("click", leads_send_email );
    $("#import_contacts").on( "click", import_contacts_confirmation);
    $(".delete-leads").on( "click", delete_leads);

    $(".delete-spw_schedule").on( "click", delete_spw_schedule);



})(jQuery);
