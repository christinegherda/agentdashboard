$(document).ready(function() {
  $('#createReviewBtn').on('click', function(e) {
    var $self = 
    e.preventDefault();
    var formArr = {
      ':input[name=name]' : {
        'validation' : 'required'
      },
      ':input[name=email]' : {
        'validation' : 'required|email'
      },
      ':input[name=subject]' : {
        'validation' : 'required'
      },
      ':input[name=message]' : {
        'validation' : 'required'
      },
    };
    var formError = false;
    $.each(formArr, function(input, value){
      var error = false;  
      var validations = value.validation.split("|");
      $(input).css({'border-color': '#d2d6de'});
      $.each(validations, function(index, v) {
        switch(v) {
          case 'required':
            if ($(input).val().trim() == '') {
              formError = error = true;
              
            }
            break;
          case 'email':
            if (!validateEmail($(input).val())) {
              formError = error = true;
            }
            break;
        }
      });

      if (error) {
        $(input).css({'border-color': 'red'});
      }
    });

    if (!formError) {
      $('#createReviewForm').submit();
    }
    
  });
  $("div.ratefixed").each(function(){
    $rate = $(this).attr("data-rate");
    $(this).rateYo({
      rating: $rate,
      readOnly: true,
      fullStar: true
    });
  });
});

$(".message-modal").on('click', function(){
  $(".review-subject").text($(this).attr("data-title"));
  $(".message-content").text($(this).attr("data-content"));
});

$(".delete_review_btn").click(function() {

  var id = $(this).data("id");

  $.ajax({
    url: $(this).data("action"),
    type: "POST",
    data: {
      "id": id,
    },
    dataType: "json",
    beforeSend: function() {
      $(this).prop("disabled", true);
    },
    success: function(data) {
      console.log(data);
      if(data.success) {
        $("#review"+id).fadeOut();
      }

      $(this).prop("disabled", false);

    }

  });

});

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}