/**
| ---------------------------------------
| @author: Jovanni G <jovanni@agentsquared.com>
| ---------------------------------------
| @requires
|   - jQuery
*/

/**
|-----------------------------------------
| Drip JS API
|-----------------------------------------
*/
// var _dcq = _dcq || [];
// var _dcs = _dcs || {};
// _dcs.account = '3445542';

// (function() {
//   var dc = document.createElement('script');
//   dc.type = 'text/javascript'; dc.async = true;
//   dc.src = 'https://tag.getdrip.com/3445542.js';
//   var s = document.getElementsByTagName('script')[0];
//   s.parentNode.insertBefore(dc, s);
// })();
/**
* Check if a property or method is defined or not.
* @param    string    $name (required) the name of the property or method.
* @return   boolean   default: false
*/
// Object.prototype.isDefined = function($name){
//   return typeof this[$name] !== 'undefined';
// };

(function($){
  /**
   |-----------------------------------------
   | AJAX - alternate to jQuery ajax calls. For some reason jQuery ajax calls are not working in my local.
   |-----------------------------------------
   | @requires Object.isDefined()
   */
  class Ajax {
    constructor($args = {}) {
      const form = new FormData();
      const xhr = new XMLHttpRequest();

      self.request = {
        url: $args.url,
        method: (typeof $args.method !== 'undefined')? $args.method.toUpperCase() : 'GET',
        data: {},
        async: (typeof $args.async !== 'undefined')? $args.async : true,
        type: (typeof $args.type !== 'undefined')? $args.type : 'json',
        raw: (typeof $args.raw !== 'undefined')? $args.raw : false,
      };

      if(typeof $args.data !== 'undefined'){
        self.request.data = $args.data;
      }

      if(!self.request.raw){
        var data = Object.keys(self.request.data);
        data.forEach(function(key, i){
          form.append(key, self.request.data[key]);
        });
      }

      xhr.open( self.request.method, self.request.url, self.request.async );
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

      if(typeof $args.headers !== 'undefined'){
        self.request.headers = $args.headers;
        var headers = Object.keys(self.request.headers);
        headers.forEach(function(key, i){
          xhr.setRequestHeader(key, self.request.headers[key]);
        });
      }
      // AJAX events
      xhr.addEventListener('loadstart', function(e){
        if (e.target.readyState === 4) {
          if (e.target.status === 200 && typeof $args.pre !== 'undefined') {
            $args.pre(e.target.response, e.target, e);
          }
        }
      });
      xhr.addEventListener('load', function(e){
        if (e.target.readyState === 4) {
          if (e.target.status === 200 && typeof $args.success !== 'undefined') {
            var response = (self.request.type === 'json')? JSON.parse(e.target.response) : e.target.response ;
            $args.success( response, e.target, e);
          }
        }
      });
      xhr.addEventListener('progress', function(e){
        if (e.target.status === 200 && typeof $args.progress !== 'undefined') {
          $args.progress(e);
        }
      });
      xhr.addEventListener('error', function(e){
        console.error(e.target);
        if(typeof $args.error !== 'undefined'){
          $args.error(e.target.response, e.target, e);
        }
      });

      self.form = form;
      self.xhr = xhr;
    }

    send(){

      if(!self.request.raw){
        self.xhr.send(self.form);
      }else{
        self.xhr.send(JSON.stringify(self.request.data));
      }
    }
  }

  /**
  |-----------------------------------------
  | Modal - modal object
  |-----------------------------------------
  | @requires jQuery
  */
  class Modal{

    constructor(_args = {}){

      var self = this;

      var body = document.getElementsByTagName('body')[0];

      var overlay = document.createElement('div');
      var container = document.createElement('div');
      var btn_close = document.createElement('button');

      $(overlay).addClass('modal-overlay');
      $(container).addClass('modal-container');
      $(btn_close).addClass('modal-close');

      container.appendChild(btn_close);
      overlay.appendChild(container);

      self.body = body;
      self.overlay = overlay;
      self.container = container;
      self.buttons = {
        close: btn_close
      };
      self.args = _args;

      self.init(_args);

      return self;
    }

    init(_args = {}){
      var self = this;

      // self explainatory
      self.buttons.close.addEventListener('click', function(e){

        var close = true;

        if(typeof _args.events !== 'undefined' && typeof _args.events.close !== 'undefined'){

          close = _args.events.close(e);
          close = (typeof close === 'undefined' )? true : close ;
        }
        
        if(close === true){
          self.close();
        }
      });

      var content = document.createElement('div');
      $(content).addClass('modal-content');

      if(typeof _args.content !== 'undefined'){

        _args.content(content, self.container, self);

      }

      self.container.appendChild(content);

      return self;
    }

    show(){
      var self = this;
      self.body.appendChild(self.overlay);
      $(self.body).addClass('modal-show');

      return self;
    }

    close(){
      var self = this;
      self.body.removeChild(self.overlay);
      $(self.body).removeClass('modal-show');

      return self;
    }

    then(_callback){
      var self = this;
      _callback(self);
    }
  }

  var getQueryParameter = function (name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  };

  var showModal = function () {
    // for some reason, when the modal is rendered through URI '/updatecc=true'
    // the modal close method does not removed the previous modal element;
    $('.modal-updatcc').parent().remove();

    // new Modal instance
    var modal = new Modal({
      events: {
        // close: function(e){

        //   window.location = '/dashboard';
        //   return false; // do not close modal.
        // },
        update: function(e, _modal){

          $(_modal.content.message).removeClass('cc-error');

          _modal.stripe.createToken(_modal.fields.cardNumber).then(function(result){

            if (result.error) {

              _modal.content.message.textContent = result.error.message;
              $(_modal.content.message).addClass('cc-error');

            } else {

              var ajax = new Ajax({
                url: '/dashboard/update_cc',
                method: 'post',
                data: result,
                raw: true,
                success: function(response){

                  if(response.success){

                    _modal.content.innerHTML = '<h2>Card successfully updated. Thank you.</h2>';
                    console.log(response);
                    setTimeout(function(){ window.location = '/dashboard'; }, 200);

                  }else{
                    _modal.content.message.textContent = response.message;
                    $(_modal.content.message).addClass('cc-error');
                  }
                },
                error: function(response){
                  console.error(response);
                },
              });

              ajax.send();
            }
          });

        },
      },
      content: function(_content, _container, _modal){
        $(_container).addClass('modal-updatcc');

        var title = document.createElement('h1');
        title.textContent = 'Update Credit Card';
        $(title).addClass('modal-title');

        var message = document.createElement('div');
        $(message).addClass('cc-message');


        var form = document.createElement('form');
        $(form).addClass('cc-form');
        form.innerHTML = '<div class="col-md-12"><div id="cc-number" class="form-control"></div></div><div><div class="col-md-8"><div id="cc-expiry" class="form-control"></div></div><div class="col-md-4"><div id="cc-cvc" class="form-control"></div></div></div><div class="col-md-12"><img src="/assets/images/dashboard/payment_method.png" class="cc-accepted" style="width:200px;"></div>';
        form.appendChild(message);



        var leftdiv = document.createElement('div');
        $(leftdiv).addClass('col-md-12 modal-left');
        leftdiv.appendChild(form);

        var btn_update = document.createElement('button');
        btn_update.id = 'cc-update';
        btn_update.type = 'submit';
        $(btn_update).addClass('btn btn-border-green');
        btn_update.textContent = 'Update';
        btn_update.addEventListener('click', function(e){

          if(typeof _modal.args.events !== 'undefined' && typeof _modal.args.events.update !== 'undefined'){
            _modal.args.events.update(e, _modal);
          }
        });
        leftdiv.appendChild(btn_update);

        // var rightdiv = document.createElement('div');
        // $(rightdiv).addClass('col-md-6 modal-right');
        // rightdiv.style.fontSize = '20px';
        // rightdiv.innerHTML = '<p style="font-size: 20px;">By clicking on the <strong>Update</strong> button, you are consenting to be bound by our terms and conditions contained in these <a href="/dashboard/terms_conditions" target="_blank">Terms and Conditions</a> and appearing anywhere on AgentSquared website.</p><p class="text-right" style="font-size: 20px;"></p>';
        // rightdiv.appendChild(btn_update);

        _content.appendChild(title);
        _content.appendChild(leftdiv);
        //_content.appendChild(rightdiv);

        _content.message = message;
        _content.btn_update = btn_update;
        //_content.rightdiv = rightdiv;
        _content.leftdiv = leftdiv;

        _modal.content = _content;
        _modal.stripe = Stripe(STRIPE_PUB_KEY);
        _modal.form = form;
      },
    });

    modal.show().then(function(_modal){
      var elements = _modal.stripe.elements({
        fonts: [
          {
            cssSrc: 'https://fonts.googleapis.com/css?family=Quicksand',
          },
        ],
        // Stripe's examples are localized to specific languages, but if
        // you wish to have Elements automatically detect your user's locale,
        // use `locale: 'auto'` instead.
        locale: window.__exampleLocale,
      });

      var cardNumber = elements.create('cardNumber');
      cardNumber.mount('#cc-number');

      var cardExpiry = elements.create('cardExpiry');
      cardExpiry.mount('#cc-expiry');

      var cardCvc = elements.create('cardCvc');
      cardCvc.mount('#cc-cvc');

      _modal.fields = {
        cardNumber: cardNumber,
        cardExpiry: cardExpiry,
        cardCvc: cardCvc,
      };
    });
  };

  $(document).ready( function () {
    var isUpdateCC = getQueryParameter('updatecc');

    if ( parseInt(isUpdateCC, 10) === 1 ) {
      showModal();
    }

    $(document).on('click', '#show-update-cc-modal', {}, function (event) {
      event.preventDefault();
      showModal();
    });
  });

})(jQuery);
