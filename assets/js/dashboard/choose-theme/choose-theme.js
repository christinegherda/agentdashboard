(function($) {

	var themeActive = $(".theme-block.active").data("theme");
	var themecover = $(".theme-block.active").data("img");
	var themeprice = $(".theme-block.active").data("price");
	$(".preview-section  .theme_name").val(themeActive.replace(/\s+/g, '-').toLowerCase());
	$(".preview-section  .preview-img").css("background-image", 'url(' + themecover + ')');
	$(".preview-section  .preview-info.price span").text(themeprice);
	$("#modalPreview .modal-title").text(themeActive);

	$(".btn-preview").click(function(e){ 
		var theme_name = $('.theme_name').val();
		
        e.preventDefault();
        console.log(theme_name);
        if (theme_name == "desert") {
        	$(".theme-preview").hide();
        	$(".theme-desert").show();
        } else {
        	$(".theme-preview").hide();
        	$(".theme-agenthaven").show();
        }
	});	

	$(".theme-block").click(function(){
		var themeName = $(this).data("theme");
		var themePrice = $(this).data("price");
		var themeFeatures = "";
		var themeImage = "";
		var agentSubscription = $('.agent_subscription').val();

		// console.log(themeName);

		if (agentSubscription != "1") {
			// Subscriber
			if ($(this).hasClass("active")) {
				$(".preview-action .action-active-select .btn-select").hide();
				$(".preview-action .action-active-select .btn-current").show();
			} else {
				$(".preview-action .action-active-select .btn-select").show();
				$(".preview-action .action-active-select .btn-current").hide();
			};

		} else {
			// Freemium
			if (themeName != "Desert") {
				if ($(this).hasClass("active")) {
					$(".preview-action .action-active-select .btn-select").hide();
					$(".preview-action .action-active-select .btn-premium").hide();
					$(".preview-action .action-active-select .btn-current").show();
				} else {
					$(".preview-action .action-active-select .btn-select").show();
					$(".preview-action .action-active-select .btn-current").hide();
				};
			} else {
				if ($(this).hasClass("active")) {
					$(".preview-action .action-active-select .btn-premium").hide();
					$(".preview-action .action-active-select .btn-current").show();
					$(".preview-action .action-active-select .btn-select").hide();
				} else {
					$(".preview-action .action-active-select .btn-premium").show();
					$(".preview-action .action-active-select .btn-current").hide();
				};
			}

		}

		if ($(this).data("features")) {
			$(".preview-section .preview-info.features").show();
			themeFeatures = $(this).data("features");
			$(".preview-section .preview-info.features span").text(themeFeatures);
		} else {
			$(".preview-section .preview-info.features").hide();
		}

		if ($(this).data("img")) {
			$(".preview-section  .preview-action").show();
			$(".preview-section  .preview-img p").remove();
			$(".preview-section .preview-img").show();
			themeImage = $(this).data("img");
			$(".preview-section  .preview-img").css("background-image", 'url(' + themeImage + ')');
		} else {
			$(".preview-section  .preview-img").css("background-image", "none");
			$(".preview-section  .preview-img").html("<p>This theme is not available at the moment.</p>");
			$(".preview-section  .preview-action").hide();
		};
		

		$('.theme_name').val(themeName.replace(/\s+/g, '-').toLowerCase());
		$(".theme-block.active").css("border","1px solid #ccc");
		$(".theme-block").removeClass("selected");
		$(this).addClass("selected");
		$(".theme-block.active.selected").css("border","3px solid #12bc00");
		$(".preview-section .preview-theme-name span").text(themeName);
		$(".preview-section .preview-info.price span").text(themePrice);

		$(".btn-select").click(function(e){ 
			var theme_name = $('.theme_name').val();
			
	        e.preventDefault();

	        $.ajax({
	            type: 'POST',
	            url: 'choose_theme/update_theme',
	            data: { 
	                theme_name: theme_name,
	            },
	            dataType: 'json',
	            beforeSend: function() {
	            	$(".theme-loader").show();
	            },
	            success: function(data) {
	                location.reload();
	            }
	        });
		});

		$(".btn-preview").click(function(e){ 
			var theme_name = $('.theme_name').val();
			
	        e.preventDefault();
	        console.log(theme_name);
	        $("#modalPreview .modal-title").text(themeName);
	        if (theme_name == "desert") {
	        	$(".theme-preview").hide();
	        	$(".theme-desert").show();
	        } else {
	        	$(".theme-preview").hide();
	        	$(".theme-agenthaven").show();
	        }
		});	
	})

})(jQuery);