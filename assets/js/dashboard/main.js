$(document).ready(function() {
    // form user-menu right side
    $('li.dropdown.user-menu a').on('click', function (event) {
        $(this).parent().toggleClass('open');
    });
    $('body').on('click', function (e) {
        if (!$('li.dropdown.user-menu').is(e.target) 
            && $('li.dropdown.user-menu').has(e.target).length === 0 
            && $('.open').has(e.target).length === 0
        ) {
            $('li.dropdown.user-menu').removeClass('open');
        }
    });

    // Detailed Target
    $(".exclude-link").click(function(){
        $(this, ".or-text").toggle();
        $(".exclude-detail").toggle();
    });
    $(".narrow-link").click(function(){
        $(this, ".or-text").toggle();
        $(".narrow-detail").toggle();
    });
    $(".target").click(function(){
        console.log($(this).parent().parent());
        $(this).parent().parent().find(".is-dropdown.browse").toggle();
        $(this).parent().parent().find(".has-dropdown-browse").toggleClass("btn-active");
    });

    $(".dropdown-submenu a").click(function(){
        $(this).parent().find(".sub-dropdown-menu").toggle();

    });

     $(".dropdown-submenu-2 a").click(function(){
        $(this).parent().find(".sub-dropdown-menu-2").toggle();
    });

    $(".has-dropdown-suggested").click(function(){
        $(this).toggleClass("btn-active");
        $(".has-dropdown-browse").removeClass("btn-active");
        $(this).parent().parent().find(".suggested").toggle();
        $(this).parent().parent().find(".browse").hide();
    });
    $(".has-dropdown-browse").click(function(){
        $(this).toggleClass("btn-active");
        $(".has-dropdown-suggested").removeClass("btn-active");
        $(this).parent().parent().find(".browse").toggle();
        $(this).parent().parent().find(".suggested").hide();
    });
    $(".close-detail").click(function(){
        $(this).parent().parent().hide();
    });
    // Video tour
    $('.video-box .checkbox').change(function () {
        console.log('changed');
        // $('.video-box .checkbox input').prop('checked', true);
        setTimeout(function(){ 
            $(".video-box").addClass("video-collapse");
            $(".video-box").parent().removeClass("col-md-6");
            $(".video-box").parent().addClass("col-md-3 pull-right");
            $(".video-box .col-md-5").addClass("col-sm-5 col-xs-5");
            $(".video-box .col-md-7").addClass("col-sm-7 col-xs-7");
        }, 300);

     });
    $('.video-box h1').click(function () {
        console.log('changeds');
        $('.video-box .checkbox input').prop('checked', false);
        $(".video-box").removeClass("video-collapse");
        $(".video-box").parent().addClass("col-md-6");
        $(".video-box").parent().removeClass("col-md-3 pull-right");
        $(".video-box .col-md-5").removeClass("col-sm-5 col-xs-5");
        $(".video-box .col-md-7").removeClass("col-sm-7 col-xs-7");
     });
    $('.video-wrapper .video-dashboard .checkbox').change(function () {
        console.log('changed');
        // $('.video-dashboard .checkbox input').prop('checked', true);
        setTimeout(function(){ 
            $(".video-dashboard").addClass("video-collapse");
            $(".video-dashboard").parent().removeClass("col-md-12");
            $(".video-dashboard").parent().addClass("col-md-7 pull-right");
            $(".video-dashboard .col-md-5").addClass("col-sm-5 col-xs-5");
            $(".video-dashboard .col-md-7").addClass("col-sm-7 col-xs-7");
        }, 300);

     });
    $('.video-wrapper .video-dashboard h1').click(function () {
        console.log('changeds');
        $('.video-dashboard .checkbox input').prop('checked', false);
        $(".video-dashboard").removeClass("video-collapse");
        $(".video-dashboard").parent().addClass("col-md-12");
        $(".video-dashboard").parent().removeClass("col-md-7 pull-right");
        $(".video-dashboard .col-md-5").removeClass("col-sm-5 col-xs-5");
        $(".video-dashboard .col-md-7").removeClass("col-sm-7 col-xs-7");
     });
    // accordion
    $(".panel-collapse").hide();
    $(".moreDetails").show();
    $(".panel-heading").on("click",function(){
        var $this = $(this);

        $this.stop().next().slideDown(250).parent().siblings().find('.panel-collapse').slideUp(250);
        $this.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-minus-circle')
            .parents('.panel-default').siblings().find('.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
    });

    //preview
    $('.calltoaction').on('change', function() {
      console.log( this.value ); // or $(this).val()
      if (this.value == "No Button") {
        $('.calltoactionbtn').hide();
      } else {
        $('.calltoactionbtn').show();
      }
      $('.calltoactionbtn').text(this.value);
    });

    $('.headline').on('change', function() {
      var hline = $(".headline option:selected").text();
      $('.head-line').text(hline);
    });
    $('.price-option .price-list .option-input').on('change', function() {
      var plan_price = $(this).val()
      var plan_type = $(this).data('plan');
      console.log("hey  "+plan_price);
      $(".product-mount").val(plan_price);
      $(".product-type").val(plan_type);
      $(".selected").text("$"+plan_price);
      $(".product-sub-amount").text(plan_price);
    });

    $("input[name='subscribe-plan']").on('change', function() {
      var plan_price = $(this).val()
      var plan_type = $(this).data('plan');
      console.log("hey  "+plan_price);
      $(".product-mount").val(plan_price);
      $(".product-type").val(plan_type);
    });

    $('.headline').keyup(function() {
        var headline = this.value;
        $('.head-line').text(this.value);
    });
    $('#abouttext').keyup(function() {
        var abouttext = this.value;
        $(".abouttext").text(abouttext);
    });
    $('#linkdesc').keyup(function() {
        var linkdesc = this.value;
        $(".linkdesc").text(linkdesc);
    });
    $('#displaylink').keyup(function() {
        var displaylink = this.value;
        $(".displaylink").text(displaylink);
    });

    // add photo upload
    $('#three').fileUploader({
        useFileIcons: false,
        fileMaxSize: 50,
        totalMaxSize: 50,
        name: "three",
        linkButtonContent: '<i>Open</i>',
        deleteButtonContent: '<i>Remove</i>',
        filenameTest: function(fileName, fileExt, $container) {
            var allowedExts = ["jpg", "jpeg" , "png"];
            var $info = $('<div class="center"></div>');
            var proceed = true;

            // length check
            if (fileName.length > 50) {
                $info.html('Name too long!');
                proceed = false;
            }
            // replace not allowed characters
            fileName = fileName.replace(" ", "-");

            // extension check
            if (allowedExts.indexOf(fileExt) < 0) {
                $info.html('Filetype not allowed!');
                proceed = false;
            }

            // show an error message
            if (!proceed) {
                $container.append($info);

                setTimeout(function() {
                    $info.animate({opacity: 0}, 300, function() {
                        $(this).remove();
                    });
                }, 2000);
                return false;
            }
            console.log($container);
            return fileName;
        },
        langs: {
            "en":{
                dropZone_msg: "Drop your images here or click to upload."
            }
        }
    });

    $("#update-account-info").on("click", function(e) {
        e.preventDefault();
        $this = $(this);
        $url = window.location.protocol + "//" + window.location.host + "/" + "dashboard/update_account_info";
        $.ajax({
            url: $url,
            dataType: 'json',
            beforeSend: function() {
                console.log("Loading");
                $this.text("Updating Account Information...");
            },
            success: function(data) {
                console.log(data);
                if(data.success) {
                    location.reload();
                }
            }
        });
    });

    $("#update-system-info").on("click", function(e) {
        e.preventDefault();
        $this = $(this);
        $url = window.location.protocol + "//" + window.location.host + "/" + "dashboard/update_system_info";
        $.ajax({
            url: $url,
            dataType: 'json',
            beforeSend: function() {
                console.log("Loading");
                $this.text("Updating System Information...");
            },
            success: function(data) {
                console.log(data);
                if(data.success) {
                    location.reload();
                }
            }
        });
    });

    var str  = document.URL;
    var n=str.split("/");
    var word = n[n.length - 1];
    // console.log(word);

    $(".disabled-agent-btn").click(function(){
        //alert("Hello World!");
        $("#agent-idx-website-modal").modal("show");
    });

    if(word == "messages" || word == "buyer_leads" || word == "seller_leads") {
        var b = $(window).height() - 200;
        $(".message-clients").css("min-height", b - $(".main-footer").outerHeight());
    }

    //Sync My Saved Searches
    $("#sync_saved_searches").on("click", function(e) {
        e.preventDefault();

        $.ajax({
            url: 'property/saved_search_db',
            dataType: 'json',
            beforeSend: function() {
                console.log("loading...");
                $("#sync_saved_searches").text("Syncing Saved Searches...");
            },
            success: function(data) {
                console.log(data);
                if(data.success) {
                    window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        });
    });

    //Update Saved Searches
    $("#update_saved_searches").on("click", function(e) {
        e.preventDefault();

        $.ajax({
            url: 'property/update_saved_search_db',
            dataType: 'json',
            beforeSend: function() {
                console.log("loading...");
                $("#update_saved_searches").text("Updating Saved Searches...");
            },
            success: function(data) {
                console.log(data);
                if(data.success) {
                    window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        });
    });



    //Submitting property details
    $("button#property-details-submit").click(function() {
        submit_property_details();
    });

    function submit_property_details() {

        var dataString = $("#property-form-description, #property-form-specification").serialize();
        console.log(dataString);

        $.ajax( {
            type: 'POST',
            url: 'save_property_details',
            data: dataString,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if(data.success)
                {
                    window.location.href = data.redirect;
                    location.reload();
                }
            }
        });
    }

    //Choose theme
    $("#agent-haven-btn").click(function() {
        $("input[name='theme']").val("agent-haven");
    });

    $("#greenery-btn").click(function() {
        $("input[name='theme']").val("greenery");
    });

    $("#proper-btn").click(function() {
        $("input[name='theme']").val("proper");
    });

	// toggle option
    $('.toggle-option').click(function(){
    	$(this).stop().parents('.page-actions').find('.options').slideToggle(150);
    });

    // close option
    $('.btn-cancel, .options .btn-submit').click(function(){
    	$(this).stop().parents('.page-actions').find('.options').hide(150);
    });

    // option status
    $('.options .btn-submit').click(function(){
        if($(this).parents('.page-actions').find('.options input').is(':checked')){
            var checkedValue = $(this).parents('.page-actions').find('.options input:checked').val();
            $(this).parents('.page-actions').find('.toggle-option').html(checkedValue);
        }
    });

	// Choose Theme
	$("div#selected").show();
	$("div#preview").hide();
    $("span.flash-data").fadeOut(3000);
	$("div.flash-data").fadeOut(5000);

    function saveIdx() {

        $('.save-loading').show();

        var formData = $('#saveIdxForm').serialize();

        $.ajax({
            url: 'idx_integration/check_idx',
            type: 'POST',
            data: formData,
            dataType: 'json',
            beforeSend: function() {
                console.log(formData);
            },
            success: function(data) {
                if(data.success) {
                    console.log("valid");
                    $.ajax({
                        url: 'idx_integration/add_credentials',
                        type: 'POST',
                        data: formData,
                        dataType: 'json',
                        success: function(data) {
                            if(data.success == 'insert') {
                                $("span.display-status").text("Successfully Inserted").show().fadeOut(3000).css({
                                    "display": "block",
                                    "color": "green",
                                    "text-align": "center"
                                });
                                $('.save-loading').hide();
                            } else {
                                $("span.display-status").text("Successfully Updated").show().fadeOut(3000).css({
                                    "display": "block",
                                    "color": "green",
                                    "text-align": "center"
                                });
                                $('.save-loading').hide();
                            }
                        }
                    });
                } else {
                    console.log("invalid");
                    $("span.display-status").text("Invalid IDX Key").show().fadeOut(3000).css({
                        "display": "block",
                        "color": "green",
                        "text-align": "center"
                    });
                    $('.save-loading').hide();
                }
            }
        });
    }

    $('#saveIdxForm').submit(function(e) {
        saveIdx();
        e.preventDefault();
    });

   //===============autosave Property  Status ==================//

     var timeoutId;

    $('.select-stat').on('select propertychange change', function() {
        console.log('Textarea Change');

        var form_id = $(this).attr('id');
        console.log(form_id);

        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            // Runs 1 second (1000 ms) after the last change
            save_property_status( form_id );
        }, 1000);
    });

    function save_property_status( form_id )
    {

        console.log('Saving to the db');

        $('.save-loading-'+form_id+'').show();

       var property_status = $('#'+form_id).val();
       var querystring = "property_status="+property_status+"&property_ListingID="+form_id;
       //console.log(querystring);
       // alert( $('#propertyStatusForm-'+form_id+'').attr('action') );

        $.ajax({
            url: $('#propertyStatusForm-'+form_id+'').attr('action'),
            type: "POST",
            data : querystring,
            success: function(data) {

                location.reload(true);
            },
        });
    }


    //added delay on alert flash data
    $(".alert-flash").delay(3000) .fadeOut(2000);

    function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('fa-caret-down fa-caret-up');
    }
    $('#menus-accordion').on('hidden.bs.collapse', toggleChevron);
    $('#menus-accordion').on('shown.bs.collapse', toggleChevron);

     function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('fa-caret-down fa-caret-up');
    }
    $('#page-accordion').on('hidden.bs.collapse', toggleChevron);
    $('#page-accordion').on('shown.bs.collapse', toggleChevron);

    // file uploader
    $('#two').fileUploader({
        useFileIcons: false,
        fileMaxSize: 1.7,
        totalMaxSize: 5,
        name: "two",
        linkButtonContent: '<i>Open</i>',
        deleteButtonContent: '<i>Remove</i>',
        filenameTest: function(fileName, fileExt, $container) {
            var allowedExts = ["jpg", "jpeg" , "png"];
            var $info = $('<div class="center"></div>');
            var proceed = true;

            // length check
            if (fileName.length > 13) {
                $info.html('Name too long...');
                proceed = false;
            }
            // replace not allowed characters
            fileName = fileName.replace(" ", "-");

            // extension check
            if (allowedExts.indexOf(fileExt) < 0) {
                $info.html('Filetype not allowed...');
                proceed = false;
            }

            // show an error message
            if (!proceed) {
                $container.append($info);

                setTimeout(function() {
                    $info.animate({opacity: 0}, 300, function() {
                        $(this).remove();
                    });
                }, 2000);
                return false;
            }

            return fileName;
        },
        langs: {
            "en":{
                dropZone_msg: "Drop your images here or click to upload."
            }
        }
    });


   // declare height of modal
   var wHeight = $(window).height();
   $(".spw-modal .modal-body").css("min-height", wHeight-100);

    // check domain
    $('.checkdomainbtn').click(function(){
        $('.domain-result').show();
    });

    // change profile pic
    $('.change-profile-container input').click(function(){
        var $imageUpload = $('.image-upload');

        $imageUpload.click();
        $imageUpload.change(function(){
            var $agentImg = $('.agent-img img');
            $('.user-menu').addClass('open');
            $('.show-btn').show();
            // console.log($agentImg);
            // var $image = $imageUpload.val().split('\\').pop();
            // var $image = 'img/' + $image;
            // console.log($image);
            // $agentImg.attr('src', $image);
        });

    });

     $('.upload-agent-photo').click(function(){
        $('.show-btn').show();
    });

      $(".upload-agent-photo").change(function () {
         $('.show-btn').show();
    });


    //check all in PAGE module
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    //phone validation
    var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];

    $('.phone_number').inputmask({
        mask: [{ "mask": "(###) ###-####"}],
        greedy: false,
        definitions: { '#': { validator: "[0-9]", cardinality: 1}}
    });

    //add Homepage option
    $("#add_homepage_option").on('submit', function(e) { 
      e.preventDefault();
      e.stopImmediatePropagation();
      var dataString = $(this).serialize();

      $.ajax({
          type: 'POST',
          url: $(this).attr('action'), 
          data: dataString,
          dataType: 'json',

          beforeSend: function() {
                $(".save-loading-add-option").show();
                $(".btn-remove-attr-addoption").prop('disabled', true);
         },         
         success: function(data) {

            $(".show-notif").html(data.msg);
            $(".show-notif").show().delay(2000).fadeOut(2000);
            $(".save-loading-add-option").hide();
            $(".btn-remove-attr-addoption").prop('disabled', false);

            location.reload(true);

          }
      });

    });

    //add Savedsearch option
    $(document).on('submit', '#add_savedSearch_option', function(e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      var dataString = $(this).serialize();

      $.ajax({
          type: 'POST',
          url: $(this).attr('action'), 
          data: dataString,
          dataType: 'json',

          beforeSend: function() {
                $(".save-loading-add-savedsearch").show();
                $(".btn-remove-attr-addsavedsearch").prop('disabled', true);
         },      
          success: function(data) {
              
                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);
                $(".save-loading-add-savedsearch").hide();
                $(".btn-remove-attr-addsavedsearch").prop('disabled', false);

                location.reload(true);
          }
      });

    });

    //add SoldListings option
    $(document).on('submit', '#add_soldListings_option', function(e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      var dataString = $(this).serialize();

      $.ajax({
          type: 'POST',
          url: $(this).attr('action'), 
          data: dataString,
          dataType: 'json',

          beforeSend: function() {
                $(".save-loading-add-soldListings").show();
                $(".btn-remove-attr-soldListings").prop('disabled', true);
         },      
          success: function(data) {
              
                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);
                $(".save-loading-add-soldListings").hide();
                $(".btn-remove-attr-soldListings").prop('disabled', false);

                location.reload(true);
          }
      });

    });


    //update Homepage option
    $(document).on('submit', '#homepage-option', function(e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      var dataString = $(this).serialize();

      $.ajax({
          type: 'POST',
          url: $(this).attr('action'), 
          data: dataString,
          dataType: 'json',

         beforeSend: function() {
                $(".save-loading-option-title").show();
                $(".btn-remove-attr-homeoption").prop('disabled', true);
         },     
        success: function(data) {

            $(".show-notif").html(data.msg);
            $(".show-notif").show().delay(2000).fadeOut(2000);
            $(".save-loading-option-title").hide();
            $(".btn-remove-attr-homeoption").prop('disabled', false);

            if(data.status == "success") {
                $(".featured-option-name-"+data.featured_id).text(data.featured_name);
                $(".selected-"+data.featured_id).text(data.featured_name);
                $("#saved-"+data.featured_id).val(data.featured_name);
            } 
          }
      });

        return false;

    });

     //update Save search title
    $("#saved-search-title").on('submit', function(e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      var dataString = $(this).serialize();

      $.ajax({
          type: 'POST',
          url: $(this).attr('action'), 
          data: dataString,
          dataType: 'json',

          beforeSend: function() {
                $(".save-loading-update-title").show();
                $(".btn-remove-attr-savedsearchtitle").prop('disabled', true);
         },  
          success: function(data) {

            $(".show-notif").html(data.msg);
            $(".show-notif").show().delay(2000).fadeOut(2000);
            $(".save-loading-update-title").hide();
            $(".btn-remove-attr-savedsearchtitle").prop('disabled', false);

          }
      });

    });

     //update Sold listings title
    $("#sold-listings-title").on('submit', function(e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      var dataString = $(this).serialize();

      $.ajax({
          type: 'POST',
          url: $(this).attr('action'), 
          data: dataString,
          dataType: 'json',

          beforeSend: function() {
                $(".save-loading-update-title").show();
                $(".btn-remove-attr-savedsearchtitle").prop('disabled', true);
         },  
          success: function(data) {

            $(".show-notif").html(data.msg);
            $(".show-notif").show().delay(2000).fadeOut(2000);
            $(".save-loading-update-title").hide();
            $(".btn-remove-attr-savedsearchtitle").prop('disabled', false);

          }
      });

    });

     //update Save search name
    $(document).on('submit', '#saved-search-name', function(e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      var dataString = $(this).serialize();

      $.ajax({
          type: 'POST',
          url: $(this).attr('action'), 
          data: dataString,
          dataType: 'json',

          beforeSend: function() {
                $(".save-loading-savedsearch-title").show();
                $(".btn-remove-attr-savedsearch").prop('disabled', true);
         },  
          success: function(data) {

                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);
                $(".save-loading-savedsearch-title").hide();
                $(".btn-remove-attr-savedsearch").prop('disabled', false);

                if(data.status == "success") {
                    $(".featured-savedsearch-name-"+data.savedsearch_id).text(data.savedsearch_name);
                    $("#search-name-"+data.savedsearch_id).val(data.savedsearch_name);
                    
                } 
          }
      });
      return false;

    });

      //update Sold Listings name
    $(document).on('submit', '#sold-listings-name', function(e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      var dataString = $(this).serialize();

      $.ajax({
          type: 'POST',
          url: $(this).attr('action'), 
          data: dataString,
          dataType: 'json',

          beforeSend: function() {
                $(".save-loading-soldListings-title").show();
                $(".btn-remove-attr-soldListings").prop('disabled', true);
         },  
          success: function(data) {

                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);
                $(".save-loading-soldListings-title").hide();
                $(".btn-remove-attr-soldListings").prop('disabled', false);

                if(data.status == "success") {
                    $(".selected-soldListings-name-"+data.sold_id).text(data.soldListings_name);
                    $("#sold-name-"+data.sold_id).val(data.soldListings_name);
                    
                } 
          }
      });
      return false;

    });

    //remove featured option
    $('.remove-featured-option').click(function(e){
        e.preventDefault();

        var $this = $(this);
        var url = $this.attr("href");
        var featuredId = $this.data("id");

        $.ajax({
            url: url,
            type: "POST",
            dataType:'json', 
            success:function(data) {

                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);

                if(data.status =="success"){
                    $("#option-item-"+featuredId ).fadeOut( "slow", function() {
                        $(".featured-"+featuredId).attr('checked', false);
                        $(".featured-option-info").hide();
                        
                    });
                }

                location.reload(true);
            }      
        });

    });

    //remove featured savedSearch
    $('.remove-featured-savedSearch').click(function(e){
        e.preventDefault();
        var $this = $(this);
        var url = $this.attr("href");
        var featuredId = $this.data("id");

        $.ajax({
            url: url,
            type: "POST",
            dataType:'json',
            success:function(data) {

                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);

                if(data.status =="success"){
                    $("#featuredsaved-item-"+featuredId ).fadeOut( "slow", function() {
                        $(".featured-savedSearch-"+featuredId).prop('checked', false);
                        $(".featured-savedSearch-"+featuredId).removeAttr('disabled');
                        $(".featured-option-info").hide();
                    });
                }
            }      
        });

    });

    //remove homepage option
    $('.remove-homepage-option').click(function(e){
        e.preventDefault();

        var $this = $(this);
        var url = $this.attr("href");
        var optionId = $this.data("id");

        $.ajax({
            url: url,
            type: "POST",
            dataType:'json',
            success:function(data) {

                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);

                if(data.status =="success"){
                    $("#option-item-"+optionId ).fadeOut( "slow", function() {
                        $(".selected-"+optionId).attr('checked', false);
                        $(".featured-"+optionId).attr('checked', false);
                        $(".selected-"+optionId).removeAttr('disabled');
                    });
                } 

                location.reload(true);
            }        
        });

    });

    //remove saved search
    $('.remove-saved-search').click(function(e){
        e.preventDefault();

        var $this = $(this);
        var url = $this.attr("href");
        var savedSearchId = $this.data("id");

        $.ajax({
            url: url,
            type: "POST",
            dataType:'json',
            success:function(data) {

                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);

                if(data.status =="success"){
                    $("#saved-item-"+savedSearchId ).fadeOut( "slow", function() {
                        $(".selected-savedSearch-"+savedSearchId).prop('checked', false);
                        $(".selected-savedSearch-"+savedSearchId).removeAttr('disabled');
                    });
                }
            }      
        });

    });

     //remove sold listings
    $('.remove-sold-listings').click(function(e){
        e.preventDefault();

        var $this = $(this);
        var url = $this.attr("href");
        var soldListingsId = $this.data("id");

        $.ajax({
            url: url,
            type: "POST",
            dataType:'json',
            success:function(data) {

                $(".show-notif").html(data.msg);
                $(".show-notif").show().delay(2000).fadeOut(2000);

                if(data.status =="success"){
                    $("#sold-item-"+soldListingsId ).fadeOut( "slow", function() {
                        $(".selected-soldListings-"+soldListingsId).prop('checked', false);
                        $(".selected-soldListings-"+soldListingsId).removeAttr('disabled');
                    });
                }
            }      
        });

    });

    $("#upload-slider-photo").off('submit').on('submit', function(e) {
         e.preventDefault();

         $(".save-loading").show(); 

        $formData = new FormData($("#upload-slider-photo")[0]);
        //console.log($formData); 

       $.ajax({
             url:base_url+"agent_sites/upload_slider_photo",         
             type:"POST",        
             data:$formData,         
             dataType:'json',              
             async:true,
             cache: false,
             contentType: false,
             processData: false,

             beforeSend: function() {
                console.log("Uploading Slider Photo");
                $(".save-loading").show();
                $(".upload-slider-photo").prop('disabled', true);
            },          
            success: function(data) {
                //console.log(data);

                var scrollTop     = $(window).scrollTop(),
                    elementOffset = $('.otherpage.video-box').offset().top,
                    distance      = (elementOffset - scrollTop);

                $('html, body').animate({scrollTop:elementOffset},200); 

                if(data.is_freemium){

                    var src = "https://s3-us-west-2.amazonaws.com/agentsquared-assets/uploads/slider/"+data.photo_url;
                } else{
                    var src = base_url+"assets/upload/slider/"+data.photo_url;
                }
                console.log(src);

                if(data.status !="error"){
                    $(".alert-status-slider-upload").html("<div class='alert-flash alert alert-success' role='alert'>"+data.msg+"</div>");
                    $(".alert-flash").delay(1000) .fadeOut(4000);
                    $(".save-loading ").hide();
                    $(".upload-slider-photo").prop('disabled', false);
                    $(".slider-photos-preview ul").append("<li id='slider-photo-"+data.photo_id+"' class='img-preview'><span><a href='#' data-toggle='modal' data-target='#delete_slider_photo-modal-"+data.photo_id+"' data-id='"+data.photo_id+"'><i class='fa fa-trash-o'></i></a></span><img src='"+src+"' width='200' height='150' alt='Slider Photos'></li> <div id='delete_slider_photo-modal-"+data.photo_id+"' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='delete_slider_photo-modal-"+data.photo_id+"' aria-hidden='false'><div class='modal-dialog modal-md'><div class='modal-content'><div class='modal-body text-center'><p class='del-photo'>Are you sure you want to delete this photo?</p><img src='"+src+"' width='300' height='200' alt='Slider Photos'></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>No</button><a class='delete_slider_photo' data-id='"+data.photo_id+"' href='"+base_url+"agent_sites/delete_slider_photo?photo_url="+data.photo_url+"&photo_id="+data.photo_id+"'><button type='button' class='btn btn-primary'>Yes</button></a></div></div></div></div>");
                } else {
                    $(".alert-status-slider-upload").html("<div class='alert-flash alert alert-danger' role='alert'>"+data.msg+"</div>");
                    $(".alert-flash").delay(1000) .fadeOut(4000);
                    $(".save-loading ").hide();
                    $(".upload-slider-photo").prop('disabled', false);
                } 

                //delete slider photo upload
                $('.delete_slider_photo').click(function(e){
                    e.preventDefault();

                    var $this = $(this);
                    var url = $this.attr("href");
                    var id = $this.data("id");
                    console.log(id);

                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType:'json',
                        success:function(data) {
                            if(data.status !="error"){

                                $("#delete_slider_photo-modal-"+id).modal('hide');
                                    console.log("deleted");
                                $( "#slider-photo-"+id ).fadeOut( "slow", function() {
                                    $.msgBox({
                                        title:"Success",
                                        content:"Successfully deleted slider photo!"
                                    });
                                 });

                            } 
                        }      
                    });

                });

            }

        });
       //reset upload form
       $('#upload-slider-photo')[0].reset();
        return false;

    });

    //delete slider photo upload
    $('.delete_slider_photo').click(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var $this = $(this);
        var url = $this.attr("href");
        var id = $this.data("id");
        console.log(id);

        $.ajax({
            url: url,
            type: "POST",
            dataType:'json',
            success:function(data) {
                if(data.status !="error"){

                    $("#delete_slider_photo-modal-"+id).modal('hide');
                        console.log("deleted");
                    $( "#slider-photo-"+id ).fadeOut( "slow", function() {
                        $.msgBox({
                            title:"Success",
                            content:"Successfully deleted slider photo!"
                        });
                     });

                } 
            }      
        });

    });

    $("#cancel-yes-btn").on('click', function() {

        $("#cancelProDialog").modal("show");
        $("#cancelPro").modal("hide");

    });

    $('#cancelProDialog').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
        $('#cancel_ret_msg').html('');
    })

    $("#cancel_agent_subscription_form").submit(function(e) {

        e.preventDefault();

        $.ajax({
            url: base_url+"agent_sites/cancel_agent_subscription",
            type: "POST",
            data: $(this).serialize(),
            dataType: "json",
            beforeSend: function() {
                $("#cancel_agent_account").prop("disabled", true);
                $("#cancel_agent_account").text('Submitting...');
            },
            success: function(data) { 
                if(data.success) {
                    setTimeout(function() {
                        $("#cancel_agent_account").text('Submit');
                        $("#cancel_agent_account").prop('disabled', false);
                        $("#cancel_ret_msg").html('<div class="alert alert-success alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Request successfully submitted!</strong> We are now processing your request, we will reach you out as soon as possible.</div>')
                    }, 3000);
                } else {
                    $("#cancel_ret_msg").html('<div class="alert alert-danger alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Failed to submit!</strong> Something went wrong while submitting request, please try again later.</div>')
                }
            }
        });

    });

    //retain recently tab open after page refresh
    $(document).ready(function(){
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });

        var activeTab = localStorage.getItem('activeTab');
        console.log('Tab Open: '+activeTab);

        if (activeTab) {
           $('a[href="' + activeTab + '"]').tab('show');
        }
    });

});