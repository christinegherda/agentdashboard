$(document).ready(function() {

	// Handle default data for lead capture switch
	$.ajax({
		url: base_url + 'single_property_listings/checkCaptureLeads',
		dataType: 'json',
		success: function(data) {
			console.log(data);
			if(data.spw_capture_leads == 1) {
				$('input[name="my-checkbox"]').bootstrapSwitch('state', true);
				$('#lead_config_btn_spw').prop('disabled', false);
			} else {
				$('input[name="my-checkbox"]').bootstrapSwitch('state', false);
				$('#lead_config_btn_spw').prop('disabled', true);
			}
		}
	});

	// Handle lead capture switch
	$('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {

		$url = $(this).attr('data-url');
		$.ajax({
			url: $url,
			type: 'POST',
			data: {
				captureSpwLeads: state == true ? 1 : 0
			},
			dataType: 'json',
			success: function(data) {
				console.log('data');
			}
		});

	});

	// handle input of minutes capture
	if($("#minute_capture_spw").is(":checked")) {
		$("#minutes_pass_spw").prop("disabled", false);
	} else {
		$("#minutes_pass_spw").prop("disabled", true);
	}

});

// Handle submitting of lead capture settings form
$('#lead_capture_config_form_spw').on('submit', function(e) {

	e.preventDefault();

	$formData = $(this).serialize();
	$url = $(this).attr('action');
	console.log('url', $url);
	$.ajax({
		url: $url,
		type: 'POST',
		data: $formData,
		dataType: 'JSON',
		success: function(data) {
			console.log(data);
			if(data.success) {
				$("#ret_msg").html("<div class='alert alert-success'><strong>Success!</strong> Successfully Saved!</span>");
				setTimeout(function() {
					$("#ret_msg .alert").remove();
					$("#lead_config_spw_modal").modal('toggle');
				}, 4000);
			} else {
				$("#ret_msg").html("<div class='alert alert-danger'><strong>Error! </strong> "+data.message+"</span>");
			}
		}
	});

});

// Handle minute capture checkbox
$("#minute_capture_spw").on("change", function() {
	if($(this).is(":checked")) {
		$("#minutes_pass_spw").prop("disabled", false);
	} else {
		$("#minutes_pass_spw").prop("disabled", true);
	}
});