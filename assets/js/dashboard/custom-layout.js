jQuery(document).ready(function($){

// from main.js
$(".target").click(function(){
    console.log($(this).parent().parent());
    $(this).parent().parent().find(".is-dropdown.browse").toggle();
    $(this).parent().parent().find(".has-dropdown-browse").toggleClass("btn-active");
});

$(".dropdown-submenu a").click(function(){
    $(this).parent().find(".sub-dropdown-menu").toggle();

});

 $(".dropdown-submenu-2 a").click(function(){
    $(this).parent().find(".sub-dropdown-menu-2").toggle();
});

$(".has-dropdown-suggested").click(function(){
    $(this).toggleClass("btn-active");
    $(".has-dropdown-browse").removeClass("btn-active");
    $(this).parent().parent().find(".suggested").toggle();
    $(this).parent().parent().find(".browse").hide();
});
$(".has-dropdown-browse").click(function(){
    $(this).toggleClass("btn-active");
    $(".has-dropdown-suggested").removeClass("btn-active");
    $(this).parent().parent().find(".browse").toggle();
    $(this).parent().parent().find(".suggested").hide();
});
$(".close-detail").click(function(){
    $(this).parent().parent().hide();
});

// initialize layout builder ===============================
var layout_canvas = $('#layout-builder').get(0);
var btn_add_row = $('#add-row').get(0);
var btn_save = $('#btn-save-layout').get(0);
var btn_reset = $('#btn-reset-layout').get(0);
var toggle_elements = $('#hide-others-toggler').get(0);
if(layout_settings.hide_elements){
  toggle_elements.click();
  $('.page-representation').addClass('hide-others');
}
toggle_elements.addEventListener('change', function(e){
  if(e.target.checked){
    $('.page-representation').addClass('hide-others');
  }else{
    $('.page-representation').removeClass('hide-others');
  }
});

// remove bogus text nodes on layout_canvas that i have no idea what created it.
if(typeof btn_add_row.previousSibling !== 'undefined'){
  layout_canvas.removeChild(btn_add_row.previousSibling);
}
if(typeof btn_add_row.nextSibling !== 'undefined'){
  layout_canvas.removeChild(btn_add_row.nextSibling);
}

// add widgets finder
widgets['find'] = function(_widget){
  if(typeof _widget !== 'undefined' && _widget){
    for(var j = 0; j < widgets.length; j++){
      if(widgets[j].id === _widget.id){
        return widgets[j];
      }
    }
  }
  return false;
};

// Objects here ===================================
function Loader(args = {}){
  var self = this;
  self.args = args;

  self.overlay = document.createElement('div');
  self.overlay.className = 'loader-overlay';

  self.render = function(){
    $('body').append(self.overlay);
  };

  self.stop = function(){
    $(self.overlay).remove();
  };

  return self;
}

function Widget(args = {}){
  var self = this;
  self.args = args;

  var widget = document.createElement('div');
  widget.className = 'modal-item';
  $(widget).attr('role', 'button');

  if(typeof self.args.icon !== 'undefined'){
    var icon = document.createElement('img');
    icon.className = 'modal-item-icon';
    icon.src = self.args.icon;
    widget.appendChild(icon);
  }
  if(typeof self.args.name !== 'undefined'){
    var title = document.createElement('h5');
    title.innerHTML = self.args.name;
    widget.appendChild(title);
    widget.title = self.args.name;
  }
  
  widget.addEventListener('click', function(e){
    self.render();
  });

  self.render = function(){

    var img = document.createElement('img');
    img.src = self.args.image;

    var _widget = document.createElement('div');
    _widget.className = 'lb-widget';
    $(_widget).attr('widget-id', self.args.id);
    _widget.appendChild(img);
    _widget.addEventListener('click', function(e){

      var title = self.args.title;
      if(_widget.firstChild.nodeName === 'H3'){
        title = _widget.firstChild.innerHTML; // replace the current h3 if it already exists
      }

      var fields = [{ // this is the title fields which all widgets will have
        label: 'Title',
        element: 'input',
        type: 'text',
        placeholder: 'Enter title here.',
        id: 'widget-title',
        value: title
      }];

      if(typeof self.args.fields !== 'undefined'){
        fields = fields.concat(self.args.fields);
      }
      if($(_widget).attr('widget-value')){
        var w_value = JSON.parse($(_widget).attr('widget-value'));
        for(var i = 0; i < fields.length; i++){
          fields[i].value = w_value[fields[i].id];
        }
      }
      
      //console.log({widget: self.args, fields: fields});
      // display widget modal
      var modal = new Modals({
        title: self.args.name + ' Settings',
        args: self.args,
        btnConfirmText: 'Update Widget',
        btnDelete: true,
        btnDeleteText: 'Delete Widget',
        isDelete: function(){
          $(_widget).remove();
        },
        content: '<p>'+self.args.content+'</p>',
        fields: fields,
        isConfirm: function(values){
          console.log(values);
          if(_widget.firstChild.nodeName === 'H3'){
            $(_widget.firstChild).remove(); // remove current h3 title
          }
          var title = document.createElement('h3');
          title.innerHTML = values['widget-title'];

          $(_widget).prepend(title);
          _widget.removeChild(_widget.lastChild);
          var widget_label = (self.args.id == 'savedsearches-carousel')? self.args.name : values['data-feed'];
          $(_widget).append('<span class="data-feed">'+widget_label+'</span>');
          $(_widget).attr('widget-value', JSON.stringify(values));
        },
      });
      modal.render();
    });

    var values = {};

    if(typeof self.args.title !== 'undefined'){
      var title = document.createElement('h3');
      title.innerHTML = self.args.title;
      
      values['widget-title'] = self.args.title;

      $(_widget).prepend(title);
    }

    if(typeof self.args.fields !== 'undefined'){
      self.args.fields.forEach(function(field){
        values[field.id] = field.value;
      });
    }
    
    // add widget label here
    var widget_label = (self.args.id == 'savedsearches-carousel')? self.args.name : values['data-feed'];
    $(_widget).append('<span class="data-feed">'+widget_label+'</span>');

    if(typeof self.args.column !== 'undefined'){

      $(_widget).attr('widget-value', JSON.stringify(values));

      var column = self.args.column;
      column.appendChild(_widget);
    }

    if(typeof self.args.postcallback !== 'undefined'){
      self.args.postcallback();
    }

  };

  self.icons = widget;

  return self;
}

function Modals(args = {}){
  var self = this;
  self.args = args;

  self.title = document.createElement('h2');
  self.title.className = 'modal-title';
  if(typeof args.title !== 'undefined'){
    self.title.textContent = args.title;
  }else{
    self.title.textContent = 'Modal Pop Up';
  }

  self.container = document.createElement('div');
  self.container.className= 'modals modal-builder';
  self.container.appendChild(self.title);

  var overlay = document.createElement('div');
  overlay.className = 'modals-overlay';
  overlay.appendChild(self.container);

  // modal buttons
  self.okay = document.createElement('button');
  self.okay.className = 'modal-okay';
  if(typeof args.btnConfirmClass !== 'undefined'){
    self.okay.className += ' ' + args.btnConfirmClass;
  }
  if(typeof args.btnConfirmText !== 'undefined'){
    self.okay.textContent = args.btnConfirmText;
  }else{
    self.okay.textContent = 'Okay';
  }
  self.okay.addEventListener('click', function(e){
    if(typeof args.preConfirm !== 'undefined'){
      args.preConfirm();
    }
    if(typeof args.isConfirm !== 'undefined'){
      // modify data to be passed after update button press
      if(typeof self.args.fields !== 'undefined'){
        var field_values = {};
        for(var i = 0; i < self.fields.childElementCount; i++){
          
          if(self.fields.children[i].childElementCount > 0){
            
            if(self.fields.children[i].lastChild.localName === 'select'){

              field_values[self.fields.children[i].lastChild.id] = self.fields.children[i].lastChild.value;

            }else if(self.fields.children[i].lastChild.localName === 'textarea'){

              field_values[self.fields.children[i].lastChild.id] = self.fields.children[i].lastChild.value;
            }else{ // default to input element
              switch(self.fields.children[i].lastChild.type){
                default: // default is text input element
                  field_values[self.fields.children[i].lastChild.id] = self.fields.children[i].lastChild.value;
              }
            }
          }
        }
        args.isConfirm(field_values);
      }else{
        args.isConfirm();
      }
    }
    $(overlay).remove();
  });

  self.cancel = document.createElement('button');
  self.cancel.className = 'modal-cancel';
  self.cancel.innerHTML = '<i class="fa fa-times"></i>';
  self.cancel.addEventListener('click', function(e){
    $(overlay).remove();
  });

  self.selection = document.createElement('div');
  self.selection.className = 'modal-selection';
  if(typeof self.args.items !== 'undefined'){

    for(var i = 0; i < self.args.items.length; i++){
      var widget = new Widget({
        id: self.args.items[i].id,
        icon: self.args.items[i].icon,
        name: self.args.items[i].name,
        image: self.args.items[i].image,
        column: self.args.items[i].column,
        content: self.args.items[i].description,
        fields: self.args.items[i].fields,
        postcallback: function(){
          $(overlay).remove();
        }
      });

      self.selection.appendChild(widget.icons);
    }
  }

  self.delete = document.createElement('button');
  self.delete.className = 'modal-delete';
  if(typeof args.btnDeleteClass !== 'undefined'){
    self.okay.className += ' ' + args.btnDeleteClass;
  }
  if(typeof args.btnDeleteText !== 'undefined'){
    self.delete.textContent = args.btnDeleteText;
  }else{
    self.delete.textContent = 'Okay';
  }
  self.delete.addEventListener('click', function(e){
    if(typeof args.isDelete !== 'undefined'){
      args.isDelete(self.container);
    }
    $(overlay).remove();
  });

  if(typeof self.args.fields !== 'undefined'){

    self.fields = document.createElement('div');
    self.fields.className = 'modal-fields';

    // get all fields on this specific modal
    for(var i = 0; i < self.args.fields.length; i++){

      var div = document.createElement('div');

      // check what type of element is the field
      if(self.args.fields[i].element === 'select'){

        div.className = 'input-group';

        var span = document.createElement('span');
        span.className = 'input-group-addon';
        span.innerHTML = self.args.fields[i].label;
        div.appendChild(span);

        var select = document.createElement('select');
        select.className = 'form-control'
        select.id = self.args.fields[i].id; // the id is very important. else the layout builder wont be able to pin point the field values for that exact widget

        if(typeof self.args.fields[i].options !== 'undefined'){
          self.args.fields[i].options.forEach(function(item, index){
            var option = document.createElement('option');
            option.value = item.key;
            option.innerHTML = item.value;

            if(item.key === self.args.fields[i].value){
              option.selected = true;
            }

            select.appendChild(option);
          });
        }
        div.appendChild(select);

      }else if(self.args.fields[i].element === 'multiselect'){

        div.className = 'input';
        // this is where the json data of the selections will be stored and will be used on the 
        var textarea = document.createElement('textarea');
        textarea.className = 'form-control hidden';
        textarea.id = self.args.fields[i].id;
        textarea.type = 'hidden';
        if(typeof self.args.fields[i].value !== 'undefined'){
          textarea.value = self.args.fields[i].value;
        }

        var checklistgroup = document.createElement('ul');
        checklistgroup.className = 'checklist-group';
        // create the checkbox input field group
        if(typeof self.args.fields[i].options !== 'undefined'){

          if(self.args.args.id === 'savedsearches-carousel'){ // dedicated for saved search modal choices

            // function to create a single checklist
            function createChecklist(item, index = null){
              // get the data from the textarea
              var _json = JSON.parse(textarea.value);

              var checkbox = document.createElement('input');
              checkbox.type = 'checkbox';
              $(checkbox).attr('data-id', item.Id );
              checkbox.checked = false;
              if(_json.hasOwnProperty(item.Id)){ // checks if it exists in the textarea value
                checkbox.checked = true;
              }

              var label = document.createElement('label');
              label.textContent = item.Name;
              $(checkbox).attr('data-title', item.Name );
              //console.log(item.Name);

              checkbox.addEventListener('change', function(e){
                var json = JSON.parse(textarea.value);

                if(e.target.checked){
                  json[$(e.target).attr('data-id')] = $(e.target).attr('data-title');
                }else{
                  delete json[$(e.target).attr('data-id')];
                }

                textarea.value = JSON.stringify(json);
              });

              var checklist = document.createElement('li');
              checklist.appendChild(checkbox);
              checklist.appendChild(label);
              checklist.title = item.Name;

              checklistgroup.appendChild(checklist);
            }

            // added properties to the checklistgroup so we can use it on any events later.
            checklistgroup['fetch'] = false; // this is to ensure ajax is called one at a time.
            checklistgroup['Pagination'] = self.args.fields[i].options.Pagination;
            checklistgroup['Results'] = self.args.fields[i].options.Results;
            checklistgroup['agent_id'] = agent_id;
            checklistgroup['disableScroll'] = false; // this is to tell the checklistgroup when to update during scrolling.

            // loop through all the options of this multiselect element
            self.args.fields[i].options.Results.forEach(function(item, index){ createChecklist(item); }); // end of self.args.fields[i].options.Results.forEach(function(item, index)

            var btn_get_selected = document.createElement('button');
            btn_get_selected.className = 'btn-show-selected';
            btn_get_selected.innerHTML = 'Show My Selected Items';
            btn_get_selected.addEventListener('click', function(e){

              checklistgroup.disableScroll = true;

              ss_input.reset();

              $(div).addClass('loading');

              $(checklistgroup).empty();

              var _json = JSON.parse(textarea.value);

              var _keys = Object.keys(_json);
              _keys.forEach(function(item, index){ createChecklist({ Id: item, Name: _json[item] }); });

              $(div).removeClass('loading');
            });
            var btn_load_ss = document.createElement('button');
            btn_load_ss.className = 'btn-show-selected';
            btn_load_ss.innerHTML = 'Reload Savedsearches';
            btn_load_ss.addEventListener('click', function(e){

              if( checklistgroup.Pagination.CurrentPage < checklistgroup.Pagination.TotalPages ){
                checklistgroup.disableScroll = false;

                ss_input.reset();

                $(div).addClass('loading');

                $(checklistgroup).empty();

                checklistgroup.Results.forEach(function(item, index){ createChecklist(item); });

                $(div).removeClass('loading');
              }
            });

            var buttons_container = document.createElement('div');
            buttons_container.className = 'ss_buttons';
            buttons_container.appendChild(btn_get_selected);
            buttons_container.appendChild(btn_load_ss);

            // search saved search field
            var ss_input = document.createElement('input');
            ss_input.type = 'text';
            ss_input.placeholder = 'Search your saved searches.';
            ss_input.className = 'form-control';
            ss_input['reset'] = function(){
              this.value = '';
              $(this).removeClass('error');
            };
            ss_input.addEventListener('keydown', function(e){
              if(e.keyCode === 13){ // if enter was pressed
                $(ss_input_submit).click();
              }
            });

            var ss_input_submit = document.createElement('button');
            ss_input_submit.innerHTML = '<i class="fa fa-search"></i>';
            ss_input_submit.addEventListener('click', function(e){

              if($(ss_input).val() !== ''){

                checklistgroup.disableScroll = true; // stop the fetch during scroll down event

                $(ss_input).removeClass('error');

                $(div).addClass('loading');

                $(checklistgroup).empty();

                function searchSavedSearches(agent_id, filter = null, page = 1, limit = 25){ // secret ingredient to my wonderful recipe.

                  $.ajax({
                    url: '/customize_homepage/getSavedSearches',
                    method: 'post',
                    type: 'json',
                    data: {
                      agent_id: agent_id,
                      page: page,
                      limit: limit,
                      filter: filter,
                    },
                    success: function(response){
                      
                      if(response && response.Success){
                        console.log('Looping SavedSearches: '+page);
                        // add the response saved search results to the checklist
                        response.Results.forEach(function(item, index){ createChecklist(item, index); });

                        $(checklistgroup).animate({
                          scrollTop: checklistgroup.scrollHeight,
                        }, 500);

                        if(response.Pagination.CurrentPage < response.Pagination.TotalPages){

                          searchSavedSearches( agent_id, filter, page + 1 );

                        }else{
                          $(div).removeClass('loading'); // end loop.
                        }
                      }else{
                        console.log(response);
                      }
                    },
                    error: function(xhr){
                      console.log(xhr);
                    }
                  });
                }

                searchSavedSearches( agent_id, $(ss_input).val(), 1 );

              }else{
                $(ss_input).addClass('error');
              }

            });

            var search_field = document.createElement('div');
            search_field.className = 'ss_search';
            search_field.appendChild(ss_input);
            search_field.appendChild(ss_input_submit);

            // this will be the container for all elements used to control the checklistgroup
            var controls = document.createElement('div');
            controls.className = 'savedsearch-controls';
            controls.appendChild(buttons_container);
            controls.appendChild(search_field);
    
            checklistgroup.addEventListener('scroll', function(e){
              if( !e.target.disableScroll ){
                var trigger = e.target.scrollHeight - e.target.offsetHeight;

                if(e.target.scrollTop > (trigger - 15) && e.target.fetch === false){
                  
                  e.target.fetch = true;
                  $(controls).addClass('fetching');

                  if(e.target.Pagination.CurrentPage < e.target.Pagination.TotalPages){
                    var next_page = e.target.Pagination.CurrentPage + 1;
                    console.log('started fetching page ' + next_page);

                    $.ajax({
                      url: '/customize_homepage/getSavedSearches',
                      method: 'post',
                      type: 'json',
                      data: {
                        agent_id: e.target.agent_id,
                        page: next_page,
                        limit: e.target.Pagination.PageSize,
                      },
                      success: function(response){

                        // get pagination data for load more
                        var data_keys = Object.keys(e.target.Pagination);
                        //console.log(response);
                        if(response){

                          e.target.Pagination = response.Pagination;

                          // add the response saved search results to the checklist
                          response.Results.forEach(function(item, index){
                            e.target.Results.push(item);
                            createChecklist(item);
                          });

                          e.target.fetch = false;
                          $(controls).removeClass('fetching');
                        }
                      },
                      error: function(xhr){
                        console.log(xhr);
                      }
                    });
                  }
                }
              }
            });

            div.appendChild(checklistgroup);
            div.appendChild(controls);
          }else{ // if widget is something else.
            // loop through all the options of this multiselect element
            self.args.fields[i].options.forEach(function(item, index){
              // get the data from the textarea
              var _json = JSON.parse(textarea.value);

              var checkbox = document.createElement('input');
              checkbox.type = 'checkbox';
              $(checkbox).attr('data-id', item.Id );
              checkbox.checked = false;
              if(_json.hasOwnProperty(item.Id)){
                checkbox.checked = true;
              }

              var label = document.createElement('label');
              label.textContent = item.Name;
              $(checkbox).attr('data-title', item.Name );

              checkbox.addEventListener('change', function(e){
                var json = JSON.parse(textarea.value);

                if(e.target.checked){
                  json[$(e.target).attr('data-id')] = $(e.target).attr('data-title');
                }else{
                  delete json[$(e.target).attr('data-id')];
                }

                textarea.value = JSON.stringify(json);
              });

              var checklist = document.createElement('li');
              checklist.appendChild(checkbox);
              checklist.appendChild(label);

              checklistgroup.appendChild(checklist);
            });

            div.appendChild(checklistgroup);
          }
        }
        div.appendChild(textarea);

      }else{ // default is input element

        switch(self.args.fields[i].type){
          case 'number':
            div.className = 'input-group';

            var span = document.createElement('span');
            span.className = 'input-group-addon';
            span.innerHTML = self.args.fields[i].label;
            div.appendChild(span);

            var input = document.createElement('input');
            input.className = 'form-control';
            input.type = self.args.fields[i].type;
            if(typeof self.args.fields[i].min !== 'undefined'){
              input.min = self.args.fields[i].min;
            }else{
              input.min = 1;
            }
            if(typeof self.args.fields[i].max !== 'undefined'){
              input.max = self.args.fields[i].max;
            }else{
              input.max = 1;
            }
            input.addEventListener('keydown', function(e){
              if(parseInt($(this).val()) >= parseInt(e.target.min) && parseInt($(this).val()) <= parseInt(e.target.max)){

              }else{
                //e.target.disabled = true;
                //console.log(e.keyCode);
                if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode >= 96 && e.keyCode <= 110)) {
                  e.preventDefault();
                }
              }
            });
            input.id = self.args.fields[i].id;
            if(typeof self.args.fields[i].placeholder !== 'undefined'){
              $(input).attr('placeholder', self.args.fields[i].placeholder);
            }
            if(typeof self.args.fields[i].value !== 'undefined'){
              input.value = self.args.fields[i].value;
            }
            div.appendChild(input);
          break;
          default: // default type is text
            div.className = 'input-group';

            var span = document.createElement('span');
            span.className = 'input-group-addon';
            span.innerHTML = self.args.fields[i].label;
            div.appendChild(span);

            var input = document.createElement('input');
            input.className = 'form-control';
            input.type = 'text';
            input.id = self.args.fields[i].id;
            if(typeof self.args.fields[i].placeholder !== 'undefined'){
              $(input).attr('placeholder', self.args.fields[i].placeholder);
            }
            if(typeof self.args.fields[i].value !== 'undefined'){
              input.value = self.args.fields[i].value;
            }
            div.appendChild(input);
        }

      }
      self.fields.appendChild(div);
    }
  }

  self.render = function(){
    if(typeof args.content !== 'undefined'){
      if(typeof args.content !== 'Node'){

        //self.container.appendChild($(args.content).get(0));
        $(self.container).append(args.content);
      }else{
        self.container.appendChild(args.content);
      }
    }

    // append the buttons to the modal
    if(typeof args.items !== 'undefined'){
      self.container.appendChild(self.cancel);
      self.container.appendChild(self.selection);
    }else{
      self.container.appendChild(self.cancel);
      if(typeof self.args.fields !== 'undefined'){
        self.container.appendChild(self.fields);
      }
      if(typeof args.btnDelete !== 'undefined' && args.btnDelete){
        self.container.appendChild(self.delete);
      }
      self.container.appendChild(self.okay);
    }
    
    $('body').append(overlay);
  };

  return self;
}

function Controls(target){
  this.width = function(precallback = null, postcallback = null, args = {}){
    var button = document.createElement('button');
    $(button).append('<i class="fa fa-arrows-h"></i>');
    button.title = 'Toggle full width or contained';
    button.className = 'btn-width';

    button.addEventListener('click', function(e){
      if(precallback !== null){
        precallback(target);
      }

      if($(target.lastChild).hasClass('fullwidth')){
        $(target.lastChild).removeClass('fullwidth');
        $(target.lastChild).addClass('contained');
      }else{
        $(target.lastChild).addClass('fullwidth');
        $(target.lastChild).removeClass('contained');
      }

      if(postcallback !== null){
        postcallback();
      }
    });

    return button;
  };

  this.column = function(precallback = null, postcallback = null, args = {}){
    var button = document.createElement('button');
    $(button).append('<i class="btn-column-icon"></i>');
    button.title = 'Change number of columns';
    button.className = 'btn-column';
    if(typeof args.size !== 'undefined'){
      $(button).attr('size', args.size);
    }else{
      $(button).attr('size', globals.min_columns);
    }

    button.addEventListener('click', function(e){
      var button = $(this);
      var size = parseInt(button.attr('size'));
      if(typeof args.modal !== 'undefined'){

        args.modal['isConfirm'] = function(){
          if(precallback !== null){
            precallback(target);
          }
          if(size < globals.max_columns){
            button.attr('size', size += 1);
          }else{
            button.attr('size', globals.min_columns);
          }
          if(postcallback !== null){
            postcallback();
          }
        };

        var modal = new Modals(args.modal);
        modal.render();
      }else{
        if(precallback !== null){
          precallback(target);
        }

        if(size < globals.max_columns){
          button.attr('size', size + 1);
        }else{
          button.attr('size', globals.min_columns);
        }

        if(postcallback !== null){
          postcallback();
        }
      }
      
    });

    return button;
  };

  this.down = function(precallback = null, postcallback = null, args = {}){
    var button = document.createElement('button');
    $(button).append('<i class="fa fa-arrow-down"></i>');
    button.title = 'Move this row down';
    button.className = 'btn-down';

    button.addEventListener('click', function(e){
      if(precallback !== null){
        precallback(target);
      }

      if(typeof $(target).next().get(0) !== 'undefined'){
        var parent = $(target).parent().get(0);
        var next = $(target).next().get(0);
        var next_index = $(next).index();
        // move elements down but not after the add row button
        if(typeof next.role !== 'undefined' && next.role === 'row'){
          $(target).insertAfter(next);
        }
      }

      if(postcallback !== null){
        postcallback();
      }
    });

    return button;
  };

  this.up = function(precallback = null, postcallback = null, args = {}){
    var button = document.createElement('button');
    $(button).append('<i class="fa fa-arrow-up"></i>');
    button.title = 'Move this row up';
    button.className = 'btn-up';

    button.addEventListener('click', function(e){
      if(precallback !== null){
        precallback(target);
      }

      if(typeof $(target).prev().get(0) !== 'undefined'){
        var parent = $(target).parent().get(0);
        var prev = $(target).prev().get(0);
        var prev_index = $(prev).index();
        parent.insertBefore(target, parent.childNodes[prev_index]);
      }

      if(postcallback !== null){
        postcallback();
      }
    });

    return button;
  };

  this.delete = function(precallback = null, postcallback = null, args = {}){
    var button = document.createElement('button');
    $(button).append('<i class="fa fa-trash"></i>');
    button.title = 'Delete this row';
    button.className = 'btn-delete';

    button.addEventListener('click', function(e){
      
      if(typeof args.modal !== 'undefined'){

        args.modal['isConfirm'] = function(){
          if(precallback !== null){
            precallback(target);
          }
          $(target).remove();

          if(postcallback !== null){
            postcallback();
          }
        };

        var modal = new Modals(args.modal);
        modal.render();

      }else{
        if(precallback !== null){
          precallback(target);
        }
        $(target).remove();

        if(postcallback !== null){
          postcallback();
        }
      }
    });

    return button;
  };
}

function Column(args = {}){
  var self = this;
  self.columns = globals.min_columns;
  self.args = args;

  self.container = document.createElement('div');
  self.container.className = 'lb-columns';
  if(typeof self.args.width !== 'undefined'){
    self.container.className += ' ' + self.args.width;
  }
  if(typeof self.args.class !== 'undefined'){
    self.container.className += ' ' + self.args.class;
  }

  self.mouseEnter = function(e){
    var button = document.createElement('button');
    button.className = 'btn-add-widget';
    button.textContent = 'Add Widget';
    button.addEventListener('click', function(e){

      function get_widgets(allowed_column = 1){
        var w = [];
        for(var i = 0; i < widgets.length; i++){
          if(widgets[i].columns.includes(allowed_column)){
            widgets[i]['column'] = e.target.parentElement;
            w.push(widgets[i]);
          }
        }
        return w;
      };

      var modal = new Modals({
        title: 'Select a widget to display in this column?',
        items: get_widgets(self.columns)
      });

      modal.render();
    });
    if(e.target.childElementCount === 0){
      if(e.target.getElementsByClassName('btn-add-widget').length === 0){
        e.target.appendChild(button);
      }
    }
  };

  self.mouseLeave = function(e){
    var btns = e.target.getElementsByClassName('btn-add-widget');
    if(btns.length > 0){
      btns[0].remove();
    }
  };

  self.update = function(args = {}){
    if(self.columns < globals.max_columns){
      if(typeof args.columns !== 'undefined'){
        self.columns = args.columns;
      }else{
        self.columns += 1;
      }
      if(typeof self.args.parent !== 'undefined'){
        $(self.container).attr('size', self.columns);

        var column = document.createElement('div');
        column.className = 'lb-column column-' + self.columns;
        // add column click event if its defined.
        if(typeof self.args.clickEvent !== 'undefined'){
          column.addEventListener('click', self.args.clickEvent());
        }

        column.addEventListener('mouseenter', function(e){
          self.mouseEnter(e);
        });
        column.addEventListener('mouseleave', function(e){
          self.mouseLeave(e);
        });

        for(var i = 0; i < self.container.children.length; i++){
          $(self.container.children[i]).empty();
        }
        self.container.appendChild(column);
      }else{
        console.log('Error: missing parent element to create these columns.');
      }
    }else{
      self.columns = globals.min_columns;

      if(typeof self.args.parent !== 'undefined'){
        $(self.container).attr('size', self.columns);

        for(var i = (self.container.childElementCount - 1); i >= globals.min_columns; i--){
          self.container.removeChild(self.container.childNodes[i]);
        }
        // remove the contents of the column
        $(self.container.childNodes[globals.min_columns]).empty();
      }else{
        console.log('Error: missing parent element to create these columns.');
      }
    }
  };

  self.render = function(){
    if(typeof self.args.columns !== 'undefined'){
      self.columns = self.args.columns;
    }
    if(typeof self.args.parent !== 'undefined'){
      
      $(self.container).attr('size', self.columns);

      for(var i = 0; i < self.columns; i++){
        var column = document.createElement('div');
        column.className = 'lb-column column-' + (i + 1);
        if(typeof self.args.clickEvent !== 'undefined'){
          column.addEventListener('click', self.args.clickEvent());
        }

        column.addEventListener('mouseenter', function(e){
          self.mouseEnter(e);
        });
        column.addEventListener('mouseleave', function(e){
          self.mouseLeave(e);
        });

        // create widgets here if its defined in the theme_config or saved from the database
        if(typeof self.args.widgets !== 'undefined' && self.args.widgets.length > 0){
          //for(var j = 0; j < self.args.widgets.length; j++){
            if(typeof self.args.widgets[i] !== 'undefined' && self.args.widgets[i]){
              var widget = new Widget({
                id: self.args.widgets[i].id,
                icon: self.args.widgets[i].icon,
                name: self.args.widgets[i].name,
                image: self.args.widgets[i].image,
                title: self.args.widgets[i].title,
                content: self.args.widgets[i].description,
                fields: self.args.widgets[i].fields,
                column: column
              });

              widget.render();
            }
            
          //}
        }

        self.container.appendChild(column);
      }

      self.args.parent.appendChild(self.container);
    }else{
      console.log('Error: missing parent element to create these columns.');
    }
  };

  return self;
}

function Row(args = {}){
  var self = this;

  var bar = document.createElement('div');
  bar.className = 'row-menu-bar';

  var container = document.createElement('div');
  container.className = 'lb-row';
  container.role = 'row';

  self.controls = new Controls(container);

  // create initial column here.
  var column_args = {
    parent: container,
    width: (typeof args.width !== 'undefined')? args.width : 'contained',
    widgets: (typeof args.widgets !== 'undefined' && args.widgets)? args.widgets: [],
  };
  if(typeof args.class !== 'undefined'){
    column_args['class'] = args.class;
  }
  if(typeof args.columns !== 'undefined'){
    column_args['columns'] = args.columns;
  }
  self.columns = new Column(column_args);

  function updateRowTitles(target){
    var result = $(target).find('.lb-row');
    if(result.length > 0){
      for(var i = 0; i < result.length; i++){
        result[i].firstChild.firstChild.textContent = 'Row ' + i;
        $(result[i]).attr('row', i + 1);
        $(result[i]).attr('rowcount', result.length)
      }
    }
  };

  self.render = function(){
    if(typeof args.base_element !== 'undefined'){

      // render the row
      $(container).on('render', function(e){

        var parentElement = this.parentElement;
        var target_id = $(this).index();
        var menu_bar = $(this).get(0).firstChild;

        // add title to this row menu bar
        menu_bar.textContent = 'Row ' + target_id;
        updateRowTitles(parentElement);

        // add control to change the number of columns here.
        menu_bar.appendChild(self.controls.column(function(target){
            self.columns.update();
          }, null, {
            modal: {
              title: 'Are you sure you want to change the number of columns?',
              content: '<p><span class="text text-danger">Warning:</span> All elements inside the columns will be deleted and is not recoverable, are you sure about this?</p>',
              btnConfirmText: 'Proceed'
            },
            size: args.columns
        }));

        // add control to move this row up and down here.
        menu_bar.appendChild(self.controls.down(null, function(){
          updateRowTitles(parentElement);
        }));
        menu_bar.appendChild(self.controls.up(null, function(){
          updateRowTitles(parentElement);
        }));

        // add control to switch between fullwidth and contained
        menu_bar.appendChild(self.controls.width());

        // add control to delete this row
        menu_bar.appendChild(self.controls.delete(function(target){ // pre delete
            if($(parentElement).find(btn_add_row).length === 0){
              // append back the add row button after it reached maximum rows then decreased
              parentElement.appendChild(btn_add_row);
            }
          }, function(){ // post delete
            // renew row titles after delete
            updateRowTitles(parentElement);
          }, {
            modal: {
              title: 'Are you sure you want to delete this row?',
              content: '<p><span class="text text-danger">Warning:</span> All columns inside this row, including the elements inside it will also be deleted.<br/>Press okay to proceed.</p>',
              btnConfirmText: 'Delete',
              btnConfirmClass: 'btn-modal-red'
            },
        }));

        self.columns.render();
      });

      $(container).prepend(bar);

      // add columns here.

      if(typeof args.method !== 'undefined'){
        switch(args.method){
          case 'after':
            $(args.base_element).after(container);
            break;
          default:
            $(args.base_element).before(container);
        }
      }else{
        $(args.base_element).before(container);
      }
      $(container).trigger("render");
      self.element = container;
    }else{
      console.log('missing parameter `base_element`');
    }
  };

  return self;
}

// prepare builder ===============================

function render_rows(_row){
  var data = [];
  if(_row.widgets.length > 0){
    console.log(_row.widgets.length);
    for(var i = 0; i < _row.widgets.length; i++){ // maximum of 1 widget per column, so i is therefore the column index
      var _widget = _row.widgets[i];
      var _w = widgets.find(_widget);
      if(_w){
        var actual_widget = {
          title: _widget.title,
          fields: [],
        };

        for(var j = 0; j < _w.fields.length; j++){ // populate field values here. there was a bug doing a direct assigning of values, so I ended up creating a new variable then populate the values.
          if(typeof _w.fields[j].id !== 'undefined'){
            var keys = Object.keys(_w.fields[j]); // get the keys of the predefined widget configuration
            var field = {};
            keys.forEach(function(key){ // assign the values to those keys on a new variable
              if(key !== 'value'){
                field[key] = _w.fields[j][key];
              }else{
                field[key] = _widget[_w.fields[j].id];
              }
            });
            actual_widget.fields.push(field);
          }
        }

        var keys = Object.keys(_w);
        keys.forEach(function(key){ // populate the remaining key pair values
          if(key !== 'title' && key !== 'fields'){
            actual_widget[key] = _w[key];
          }
        });
        data[i] = actual_widget; // assign the widget to its designated column
      }else{
        data[i] = _w;
      }
    }
  }
  console.log(data);
  var row = new Row({
    base_element: $(btn_add_row),
    method: 'before',
    columns: _row.columns,
    widgets: data,
    width: _row.width
  });

  row.render();

  if(globals.max_rows === (layout_canvas.childElementCount - 1)){
    $(btn_add_row).remove();
  }
}

function load_default(){
  theme_config.builder.homepage.forEach(function(_row){
    render_rows(_row);
  });
}

if(homepage_layout.length > 0){
  console.log(homepage_layout);
  homepage_layout.forEach(function(row){
    // if data is available from the database, do your thing here.
    render_rows(row);
  });
}else{
  load_default();
}

// events here ===================================
btn_add_row.addEventListener('click', function(e){
  
  var row = new Row({
    base_element: $(this),
    method: 'before',
    columns: 1,
    width: "contained"
  });

  row.render();

  if(globals.max_rows === (layout_canvas.childElementCount - 1)){
    $(this).remove();
  }
});

btn_save.addEventListener('click', function(e){
  var loader = new Loader();
  loader.render();

  var current_layout = [];
  var rows = layout_canvas.getElementsByClassName('lb-row');
  var widgets_used = [];

  for(var i = 0; i < rows.length; i++){
    // get columns
    var columns = rows[i].lastChild.children;
    var widgets = [];

    for(var j = 0; j < columns.length; j++){
      if(columns[j].children.length > 0){
        // get widgets inside columns
        var w_value = JSON.parse($(columns[j].firstChild).attr('widget-value'));
        var w_id = $(columns[j].firstChild).attr('widget-id');
        var widget = {
          id: w_id,
          title: w_value['widget-title']
        };
        widgets_used.push(w_id);
        if(typeof w_value['data-json'] !== 'undefined') widget['data-json'] = w_value['data-json'] ;
        if(typeof w_value['data-feed'] !== 'undefined') widget['data-feed'] = w_value['data-feed'] ;
        if(typeof w_value['data-count'] !== 'undefined') widget['data-count'] = w_value['data-count'] ;

        widgets.push(widget);
      }else{
        widgets.push(false);
      }
    }

    var row_json = {
      columns: parseInt($(rows[i].lastChild).attr('size')),
      size: "equal",
      width: ($(rows[i].lastChild).hasClass('fullwidth'))? 'fullwidth': 'contained',
      widgets: widgets
    };

    current_layout.push(row_json);
  } // end of for loop

  console.log(current_layout);
  var settings = {
    hide_elements: $('#hide-others-toggler').get(0).checked,
    widgets_used: widgets_used
  };
  // save data here
  $.ajax({
    url: base_url + '/customize_homepage/update_builder',
    method: 'post',
    data: {
      theme: theme_config.theme,
      layout: JSON.stringify(current_layout),
      settings: JSON.stringify(settings)
    },
    success: function(response){
      console.log(response);
      if(response.success){

      }
      loader.stop();
    },
    error: function(response){
      console.log(response);
      if(typeof response.success !== 'undefined' && !response.success){
        // display error here;
      }
    },
  });
});

btn_reset.addEventListener('click', function(e){
  var modal = new Modals({
    title: 'Are you sure you want to reset to default?',
    content: '<p><span class="text text-danger">Warning: </span> All recent changes will be ignored prior to saving and unrecoverable.<br>Would you like to proceed?</p>',
    btnConfirmText: 'Reset Anyway',
    isConfirm: function(){
      $(layout_canvas).empty();
      layout_canvas.appendChild(btn_add_row);

      load_default();
    }
  });
  
  modal.render();
});

});
