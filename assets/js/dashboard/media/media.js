

  jQuery(document).ready(function($){


     //Change the form action based on selected value ie.. bulk_delete GRID VIEW
    $('#bulk-action-grid').change(function(){
      var formAction = $("#bulk-select-grid").val() == "delete" ? "bulk_delete_grid" : "";
      $("#bulk-action-grid").attr("action", "/media/" + formAction);
    });


     //Change the form action based on selected value ie.. bulk_delete LIST VIEW
    $('#bulk-action-list').change(function(){
      var formAction = $("#bulk-select-list").val() == "delete" ? "bulk_delete_list" : "";
      $("#bulk-action-list").attr("action", "/media/" + formAction);
    });

    //Change the form action based on selected value ie.. filter_date GRID VIEW
    $('#filter-date-grid').change(function(){
      var formAction = $("#select-pages").val();
      $("#filter-date-grid").attr("action", "/media/filter_grid" + formAction);
    });

     //Change the form action based on selected value ie.. filter_date LIST VIEW
    $('#filter-date-list').change(function(){
      var formAction = $("#select-pages").val();
      $("#filter-date-list").attr("action", "/media/filter_list" + formAction);
    });

    // Change the select name based on selected action ie.. restore/delete
    $('#bulk-select').change(function(){
      var selectedVal = $("#bulk-select").val() == "delete" ? "bulk_delete[]" : "";
      $(".select-action").attr("name", selectedVal);
    });

    //show checkbox on HOVER
    // $( ".list-item-grid" ).hover(
    //   function() {
    //     $( this ).addClass( "display-checkbox" );
    //   }, function() {
    //     $( this ).removeClass( "display-checkbox" );
    //   }
    // );

    $('.media-grid-actions .btn-selected').on("click", function (e) { 
          e.preventDefault();
           var checkbox = $(this).find(':checkbox');
           if (checkbox.is(":checked")) {
              checkbox.prop('checked', false);
              $(this).parent().parent().removeClass("media-selected");
              $(this).find("span").html("Select");
           } else {
              checkbox.prop('checked', true);
              $(this).parent().parent().addClass("media-selected");
              $(this).find("span").html("<i class='fa fa-check'></i>");
              $(".image-details-header").show();
           }
     });

    $('.insertMediaModal .close').on("click", function (e) { 
          e.preventDefault();
              $(".list-item-grid").removeClass("media-selected");
              $("span.checked").css("display","none");
     });

    //remove checked checkbox and insert into page button after closing the modal without saving
    $('.insertMediaModal').on('hidden.bs.modal', function(e) {
      $(".insertMediaModal .modal-dialog").find('input:radio, input:checkbox').prop('checked', false);
      $(".insertPage").hide();
    })

    //show media actions on hover list view
    // $( ".media-hover-actions" ).hover(
    //   function() {
    //     $( this ).addClass( "display-action" );
    //   }, function() {
    //     $( this ).removeClass( "display-action" );
    //   }
    // );

    //show all actions on Grid view on chech checkbox
    // $('input').change(function(){
    //   if ($(this).is(':checked')) {
    //       $(this).parent().parent().addClass("show-grid-actions");
    //   }
    // });


    $("#insertIntoPage").on("click", function(e) {
          e.preventDefault();
          $dataString = $("#insertMedia").serialize();

          $.ajax({
              type: 'POST',
              url: $("#insertMedia").attr('action'),
              data: $dataString,
              dataType: 'json',
              success: function(data) {
                  if(data.success) {

                      var parentEditor = parent.tinyMCE.activeEditor;
                      parentEditor.execCommand('mceInsertContent', false, data.mediaUrl);
                      $('.insertMediaModal').modal('hide');

                      // reset selected media
                      $(".modal-checkbox").prop('checked',false);
                      $(".list-item-grid").removeClass("media-selected");
                      $(".checked").hide();
                  }
              }
          });

      });

  $('.media-grid-actions .ischeckbox').on("click", function (e) { 
        e.preventDefault();
         var checkbox = $(this).find(':checkbox');
          $(".modal-checkbox").prop('checked',false);
          $(".list-item-grid").removeClass("media-selected");
          $(".checked").hide();
          checkbox.prop('checked', true);
          checkbox.parent().parent().parent().addClass("media-selected");
          checkbox.parent().parent().find(".checked").show();
          $(".image-details-header").show();
   });

  // $(document).ready(function(){
  //   // set min-height for modal
  //   var window_height = $(window).height();
  //   var content_height = parseInt($(".content-wrapper").css("min-height"));
  //   $(".insertMediaModal .modal-body").css('min-height', window_height-153);
  //   $("#agentinfo-mediamodal").css('min-height', content_height-394);
  // });

  $(document).ready(function() {
      $(".hide-media").hide();

        $( ".insertMediaModal" ).on( "click", ".ischeckbox", function() {
            $(".hide-media").hide();
              var dataType = $(this).attr('data-id');
              $("#" + dataType).show();

              $(".insertPage").show();
        });

        //update media details
        $(".updateMedia").on("submit", function(e) {
          e.preventDefault();

          var dataString = $(this).serialize();
          console.log(dataString);

          $.ajax({
              type: 'POST',
              url: $(this).attr('action'),
              data: dataString,
              dataType: 'json',
              success: function(data) {
                 console.log(data);
                  if(data.success) {

                        $(".show-notif").html(data.message);
                        $(".show-notif").show().delay(2000).fadeOut(1000);
                        //window.location.href = data.redirect;
                        //$('.media'+data.media_id+'').html(data.html_data);

                    } else {
                        $(".show-notif").html(data.message);
                        $(".show-notif").show().delay(2000).fadeOut(1000);
                    }
              }
          });

        });



      $("#upload-media-page").off('submit').on('submit', function(e) {
         e.preventDefault();

         $(".save-loading").show(); 

        $formData = new FormData($("#upload-media-page")[0]);
        //console.log($formData); 

       $.ajax({
             url:base_url+"pages/upload_media_page",         
             type:"POST",        
             data:$formData,         
             dataType:'json',              
             async:true,
             cache: false,
             contentType: false,
             processData: false,

             beforeSend: function() {
                console.log("Uploading Media");
                $(".save-loading").show();
                $(".upload-media-page").prop('disabled', true);
            },          
            success: function(data) {
                //console.log(data);
                if(data.status !="error"){

                  if(data.is_freemium){
                    var src = "https://s3-us-west-2.amazonaws.com/agentsquared-assets/uploads/file/"+data.media_url;
                  } else{
                    var src = base_url+"assets/upload/file/"+data.media_url;
                  }

                  var pdf_val = '<a href="'+src+'" target="_blank"><img style="width:15%;display:block;" src="'+base_url+'assets/images/pdf-icon.png"></a>';
                  var excel_val = '<a href="'+src+'" target="_blank"><img style="width:15%;display:block;" src="'+base_url+'assets/images/excel-icon.png"></a>';
                  var csv_val = '<a href="'+src+'" target="_blank"><img style="width:15%;display:block;" src="'+base_url+'assets/images/csv-icon.png"></a>';
                  var txt_val = '<a href="'+src+'" target="_blank"><img style="width:15%;display:block;" src="'+base_url+'assets/images/txt-icon.png"></a>';
                  var doc_val = '<a href="'+src+'" target="_blank"><img style="width:15%;display:block;" src="'+base_url+'assets/images/doc-icon.png"></a>';
                  var media_val = '<a href="'+src+'" target="_blank"><img style="width:15%;display:block;" src="'+src+'"></a>';

                    var media = "<li class='list-item-grid'><div class='media-grid-actions'><span class='checked'><i class='fa fa-check'></i></span><div class='ischeckbox' data-id='media"+data.media_id+"'><input type='checkbox' class='modal-checkbox' name='bulk_insertMedia' value='"+data.media_id+"'></div></div>";
                      if(data.media_file_type == "application/pdf") {

                        media += "<div class='item-grid media"+data.media_id+"'><img src='"+base_url+"assets/images/pdf-icon.png'><input type='hidden' name='media_url"+data.media_id+"' value='"+pdf_val+"'/></div>";
                      } else if(data.media_file_type == "application/vnd.openxmlformats-officedocument.word" || data.media_file_type === "application/msword"){

                         media += "<div class='item-grid media"+data.media_id+"'><img src='"+base_url+"assets/images/doc-icon.png'><input type='hidden' name='media_url"+data.media_id+"' value='"+doc_val+"'/></div>";
                      
                      } else if(data.media_file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
                         media += "<div class='item-grid media"+data.media_id+"'><img src='"+base_url+"assets/images/excel-icon.png'><input type='hidden' name='media_url"+data.media_id+"' value='"+excel_val+"'/></div>";

                      } else if(data.media_file_type == "text/plain"){
                         media += "<div class='item-grid media"+data.media_id+"'><img src='"+base_url+"assets/images/txt-icon.png'><input type='hidden' name='media_url"+data.media_id+"' value='"+txt_val+"'/></div>";

                          } else if(data.media_file_type == "text/csv"){
                         media += "<div class='item-grid media"+data.media_id+"'><img src='"+base_url+"assets/images/csv-icon.png'><input type='hidden' name='media_url"+data.media_id+"' value='"+csv_val+"'/></div>";

                      } else {

                         media += "<div class='item-grid media"+data.media_id+"'><img src='"+src+"'>";
                    
                         media += "<input type='hidden' name='media_url"+data.media_id+"' value='"+media_val+"'/></div>";
                      }

                       //append data on modal
                      $(".upload-media-preview ul.list-item").append(media);

                      //hide after first upload
                      $(".no-media-uploaded").hide();

                        var media_title = (data.media_title) ? data.media_title : '';
                        var media_alt = (data.media_alt) ? data.media_alt : '';
                        var media_caption = (data.media_caption) ? data.media_caption : '';
                        var media_desc = (data.media_desc) ? data.media_desc : '';

                       var detail = "<div id='media"+data.media_id+"' class='media-details hide-media'>";
                         detail += "<form id='updateMediaForm' class='updateMedia' action='"+base_url+"pages/update_details/"+data.media_id+"' method='POST'>";
                         detail += "<input type='hidden' name='media_id' value='"+data.media_id+"'>";
                         detail += "<input type='hidden' name='page_id' value='"+data.page_id+"'>";
                         detail += "<input type='hidden' name='file_name' value='"+data.media_url+"'>";
                         detail += "<p><label for='media-title'>Title</label></p><input type='text' name='media_title' id='media-title' value='"+media_title+"' class='form-control' maxlength='30' pattern='[a-zA-Z0-9-_.,\s]+' title='Allowed characters: A-Z, 0-9, ., -, _'>";

                      if(data.media_file_type == "application/pdf") {

                        detail += "<p class='media-icon'><img src='"+base_url+"assets/images/pdf-icon.png' alt='"+media_alt+"' width='80' height='80'></p><br>";
                      } else if(data.media_file_type == "application/vnd.openxmlformats-officedocument.word" || data.media_file_type === "application/msword"){

                        detail += "<p class='media-icon'><img src='"+base_url+"assets/images/doc-icon.png' alt='"+media_alt+"' width='80' height='80'></p><br>";

                      } else if(data.media_file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ){

                        detail += "<p class='media-icon'><img src='"+base_url+"assets/images/excel-icon.png' alt='"+media_alt+"' width='80' height='80'></p><br>";

                      } else if(data.media_file_type == "text/plain" ){

                        detail += "<p class='media-icon'><img src='"+base_url+"assets/images/txt-icon.png' alt='"+media_alt+"' width='80' height='80'></p><br>";

                      } else if(data.media_file_type == "text/csv" ){

                        detail += "<p class='media-icon'><img src='"+base_url+"assets/images/csv-icon.png' alt='"+media_alt+"' width='80' height='80'></p><br>";


                      } else {

                        detail += "<p class='media-icon'><img src='"+src+"' alt='"+media_alt+"' width='80' height='80'></p><br>";
                      }

                     detail += "<label for='media-link'>Image Link</label><input type='text' name='media_link' id='media-link' value='"+src+"' class='form-control' disabled> <br>";
                     
                     detail += "<label for='media-caption'>Caption</label><input type='text' name='media_caption' id='media-caption' value='"+media_caption+"' class='form-control' maxlength='100' pattern='[a-zA-Z0-9-_.,\s]+' title='Allowed characters: A-Z, 0-9, ., -, _'> <br>";
                    
                     detail += " <label for='media-alt-text'>Alternative Text</label><input type='text' name='media_alt_text' id='media-alt-text' value='"+media_alt+"' class='form-control' maxlength='50' pattern'[a-zA-Z0-9-_.,\s]+' title='Allowed characters: A-Z, 0-9, ., -, _'> <br>";
                    
                     detail += "<label for='media-description'>Description</label><input type='text' name='media_description' id='media-description' value='"+media_desc+"' class='form-control' maxlength='200' pattern='[a-zA-Z0-9-_.,\s]+' title='Allowed characters: A-Z, 0-9, ., -, _'> <br>";
                    
                     detail += "<button type='submit' class='btn btn-default btn-submit'>Update</button></form></div>";
                      
                      //append data on modal
                      $(".image-deatils-preview").append(detail);

                      $(".alert-status-media-upload").html("<div class='alert-flash alert alert-success' role='alert'>"+data.msg+"</div>");
                      $(".alert-flash").delay(1000) .fadeOut(2000);
                      $(".save-loading ").hide();
                      $(".upload-media-page").prop('disabled', false);

                      //make it selected on check
                      $('.media-grid-actions .ischeckbox').on("click", function (e) { 
                          e.preventDefault();
                           var checkbox = $(this).find(':checkbox');
                            $(".modal-checkbox").prop('checked',false);
                            $(".list-item-grid").removeClass("media-selected");
                            $(".checked").hide();
                            checkbox.prop('checked', true);
                            checkbox.parent().parent().parent().addClass("media-selected");
                            checkbox.parent().parent().find(".checked").show();

                            $(".image-details-header").show();
                     });

                       $(".hide-media").hide();
                       $(".ischeckbox").click(function() {    
                          $(".hide-media").hide();
                          var dataType = $(this).attr('data-id');
                          $("#" + dataType).show();
                      });

                      //update media details
                      $(".updateMedia").on("submit", function(e) {
                        e.preventDefault();

                        var dataString = $(this).serialize();
                        console.log(dataString);

                        $.ajax({
                            type: 'POST',
                            url: $(this).attr('action'),
                            data: dataString,
                            dataType: 'json',
                            success: function(data) {
                               console.log(data);
                                if(data.success) {

                                      $(".show-notif").html(data.message);
                                      $(".show-notif").show().delay(2000).fadeOut(1000);
                                      //window.location.href = data.redirect;
                                      //$('.media'+data.media_id+'').html(data.html_data);

                                  } else {
                                      $(".show-notif").html(data.message);
                                  }
                            }
                        });

                      });

                  } else {

                    $(".alert-status-media-upload").html("<div class='alert-flash alert alert-danger' role='alert'>"+data.msg+"</div>");
                    $(".alert-flash").delay(1000) .fadeOut(2000);
                    $(".save-loading ").hide();
                    $(".upload-media-page").prop('disabled', false);
                }    

            }

        });
       //reset upload form
       $('#upload-media-page')[0].reset();
        return false;

    });

    }); 


    $(".btn-show-upload-aws").click(function(){
        $(".show-upload-aws").slideToggle("slow");
    });

     $(".btn-show-upload").click(function(){
        $(".show-upload").slideToggle("slow");
    });

    
});
