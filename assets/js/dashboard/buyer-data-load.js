$(document).ready(function(){
	var $this = $('.message-clients li.selected');
	var $name = $this.find('a').text();
	
	//$.getJSON(base_url+'/assets/js/dashboard/buyer-data.json', function(data){
	$.getJSON(base_url+'crm/buyer_leads/get_buyer_json', function(data){	
		if($this.hasClass('selected')){
			var output="<h4>Personal Information</h4>";
			output += '<ul class="lead-items">';
			$.each(data, function(key, val){
				if($name == val.fname+" "+val.lname){
					agent = ( val.are_you_currently_with_an_agent == 1) ? "Yes" : "No";
					output += '<li><div class="col-md-4"><label>Name:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.fname +' '+ val.lname+'</p></div></li>';
					output += '<li><div class="col-md-4"><label>Email:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.email + '</p></div></li>';
					output += '<li><div class="col-md-4"><label>Phone:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.phone + '</p></div></li>';
					output += '<li><div class="col-md-4"><label>Mobile:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.mobile + '</p></div></li>';
					output += '<li><div class="col-md-4"><label>Fax:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.fax + '</p></div></li>';
					output += '<li><div class="col-md-4"><label>Address:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.address + '</p></div></li>';
					output += '<li><div class="col-md-4"><label>City:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.city + '</p></div></li>';	
					output += '<li><div class="col-md-4"><label>State:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.state + '</p></div></li>';
					output += '<li><div class="col-md-4"><label>Zip Code:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.zip_code + '</p></div></li>';
					output += '<h4>Property Information</h4>'
					output += '<li><div class="col-md-5"><label>Number of Bedrooms:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.bedrooms + '</p></div></li>';
					output += '<li><div class="col-md-5"><label>Number of Bathrooms:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.bathrooms + '</p></div></li>';
					output += '<li><div class="col-md-5"><label>Square Feet:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.square_feet + '</p></div></li>';
					output += '<li><div class="col-md-5"><label>Contact:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.contact_by + '</p></div></li>';
					output += '<li><div class="col-md-5"><label>Price:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.price_range + '</p></div></li>';
					output += '<h4>Moving Details</h4>'
					output += '<li><div class="col-md-7"><label>When do you want to move?</label></div>';
					output += '<div class="col-md-5"><p>'+ val.when_do_you_want_to_move + '</p></div></li>';
					output += '<li><div class="col-md-7"><label>When did you start looking?</label></div>';
					output += '<div class="col-md-5"><p>'+ val.when_did_you_start_looking + '</p></div></li>';
					output += '<li><div class="col-md-7"><label>Where would you like to own?</label></div>';
					output += '<div class="col-md-5"><p>'+ val.wher_would_you_like_to_own + '</p></div></li>';
					output += '<li><div class="col-md-7"><label>Are you currently with an agent?</label></div>';
					output += '<div class="col-md-5"><p>'+ agent  + '</p></div></li>';
					output += '<li><div class="col-md-7"><label>Describe your Dream Home:</label></div>';
					output += '<div class="col-md-5"><p>'+ val.describe_your_dream_home + '</p></div></li>';
				}
			});

			output += '</ul>';
		}
		else
		{
			output = '';
		}
		
		$('.lead-details').html(output);
	});
	

	$('.message-clients li').click(function(){
		var $this = $(this);
		var $name = $(this).find('a').text();
		
		$this.addClass('selected').siblings().removeClass('selected');

		//$.getJSON(base_url+'assets/js/dashboard/buyer-data.json', function(data){
		$.getJSON(base_url+'crm/buyer_leads/get_buyer_json', function(data){	
			if($this.hasClass('selected')){
				var output="<h4>Personal Information</h4>";
				output += '<ul class="lead-items">';
				$.each(data, function(key, val){
					if($name == val.fname+" "+val.lname){
						agent = ( val.are_you_currently_with_an_agent == 1) ? "Yes" : "No";
						output += '<li><div class="col-md-4"><label>Name:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.fname +' '+ val.lname+'</p></div></li>';
						output += '<li><div class="col-md-4"><label>Email:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.email + '</p></div></li>';
						output += '<li><div class="col-md-4"><label>Phone:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.phone + '</p></div></li>';
						output += '<li><div class="col-md-4"><label>Mobile:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.mobile + '</p></div></li>';
						output += '<li><div class="col-md-4"><label>Fax:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.fax + '</p></div></li>';
						output += '<li><div class="col-md-4"><label>Address:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.address + '</p></div></li>';
						output += '<li><div class="col-md-4"><label>City:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.city + '</p></div></li>';	
						output += '<li><div class="col-md-4"><label>State:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.state + '</p></div></li>';
						output += '<li><div class="col-md-4"><label>Zip Code:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.zip_code + '</p></div></li>';
						output += '<h4>Property Information</h4>'
						output += '<li><div class="col-md-5"><label>Number of Bedrooms:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.bedrooms + '</p></div></li>';
						output += '<li><div class="col-md-5"><label>Number of Bathrooms:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.bathrooms + '</p></div></li>';
						output += '<li><div class="col-md-5"><label>Square Feet:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.square_feet + '</p></div></li>';
						output += '<li><div class="col-md-5"><label>Contact:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.contact_by + '</p></div></li>';
						output += '<li><div class="col-md-5"><label>Price:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.price_range + '</p></div></li>';
						output += '<h4>Moving Details</h4>'
						output += '<li><div class="col-md-7"><label>When do you want to move?</label></div>';
						output += '<div class="col-md-5"><p>'+ val.when_do_you_want_to_move + '</p></div></li>';
						output += '<li><div class="col-md-7"><label>When did you start looking?</label></div>';
						output += '<div class="col-md-5"><p>'+ val.when_did_you_start_looking + '</p></div></li>';
						output += '<li><div class="col-md-7"><label>Where would you like to own?</label></div>';
						output += '<div class="col-md-5"><p>'+ val.wher_would_you_like_to_own + '</p></div></li>';
						output += '<li><div class="col-md-7"><label>Are you currently with an agent?</label></div>';
						output += '<div class="col-md-5"><p>'+ agent + '</p></div></li>';
						output += '<li><div class="col-md-7"><label>Describe your Dream Home:</label></div>';
						output += '<div class="col-md-5"><p>'+ val.describe_your_dream_home + '</p></div></li>';
					}
				});
			}
			output += '</ul>';
			$('.lead-details').html(output);
		});
	});
});