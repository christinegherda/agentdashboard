 //SOLD LISTINGS TOGGLE OPTIONS
    $(document).ready(function() {

      $.ajax({
        url: base_url+'agent_sites/check_sold_listings_option',
        dataType: 'json',
        beforeSend: function() {
          console.log('check sold listings');
        },
        success: function(data) {
          console.log(data);
          if(data.show_sold_listings == 1) {
            $('input[name="sold-checkbox"]').bootstrapSwitch('state', true);
          } else {
            $('input[name="sold-checkbox"]').bootstrapSwitch('state', false);
          }
        }
      });
      
    });

   $('input[name="sold-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {

    console.log(this); // DOM element
    console.log(event); // jQuery event
    console.log(state); // true | false

    if(state) {
      $post = 'show_sold_listings';

    } else {
      $post = 'dont_show_sold_listings';
    }

    $.ajax({
      url: $(this).attr("data-url"),
      type: "POST",
      data: {
        switch_post:$post
      },
      dataType:'json',
      beforeSend: function() {
        console.log(state);
      },
      success: function(data) {
        console.log(data);
      }
    });
  });