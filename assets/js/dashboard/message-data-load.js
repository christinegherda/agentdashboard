$(document).ready(function(){
	var $this = $('.message-clients li.selected');
	var $name = $this.find('a').text();
	
	//$.getJSON(base_url+'/assets/js/dashboard/buyer-data.json', function(data){
	$.getJSON(base_url+'crm/messages/get_message_json', function(data){	
		if($this.hasClass('selected')){
			var output="<h4>Personal Information</h4>";
			output += '';
			$.each(data, function(key, val){
					if($name == val.name){
						output += '<h4>'+ val.name + '</h4>';
						output += '<p>'+ val.email + '</p>';
						output += '<hr>';
						$.each(val.messages, function(key2, val2){
							if(val2.type == 0){
								output += '<div class="client-reply">';
						        output += '<div class="col-md-9">';
						        output += '<p class="message-content">';
						        output += val2.message; 
						        output += '</p></div> ';			                    
						        output += '<div class="col-md-3"><span>'+val2.created+'</span>    </div>  </div>';
					   		}
					        if(val2.type == 1){
						        output += '<div class="agent-reply"><div class="col-md-3">';
					            output += '<span>'+val2.created+'</span> </div>';
					            output += '<div class="col-md-9"><p class="message-content">  ';    
					            output += val2.message; 
					            output +=' </p>  </div></div>';
				        	}
			        	});
						output += '<div class="agent-new-reply"></div>';
			            output += '<form action="'+base_url+'" method="post" class="send-reply-message">';
			            output += '<input type="hidden" name="mid" value="'+val.cqid+'" >';
			            output += '<div class="textbox-area">';
			            output += '<textarea name="message" id="" cols="10" rows="5" class="form-control message-textarea" placeholder="click here to reply"></textarea>';
			            output += '</div>';

			            output += '<br /><button type="submit" class="btn btn_sbt btn-submit" >Send</button></form>';
			            //output += '<p>press enter to send</p>';
					}
				});
		}
		
		$('.mess-details').html(output);

		$(".send-reply-message").on("submit", sbt_agent_reply);
	});

	var sbt_agent_reply = function(e)
	{
	 	e.preventDefault();

	 	var action = base_url + 'crm/messages/message_reply';
	 	var datastring = $(".send-reply-message").serialize();
	 	var reply_content = $(".message-textarea").val();

	 	if( reply_content == "" )
	 	{
	 		alert("Message box is empty!");
	 		return false;
	 	}

	 	$.ajax({
	 		type: "POST",
            url: action,
            data: datastring,
            dataType: "json",            
            beforeSend: function() {
                $(".btn_sbt").text("loading...");
            },
            success: function(data) {
            	console.log(data);

            	if(data.success)
            	{
            		//append new reply
            		$(".agent-new-reply").append(data.to_append);
            	}

            	$(".btn_sbt").text("Submit");
            }
	 	});
	 	
	}
	

	$('.message-clients li').click(function(){
		var $this = $(this);
		var $name = $(this).find('a').text();
		
		$this.addClass('selected').siblings().removeClass('selected');
		
		//$.getJSON(base_url+'assets/js/dashboard/buyer-data.json', function(data){
		$.getJSON(base_url+'crm/messages/get_message_json', function(data){	
			if($this.hasClass('selected')){
				var output="<h4>Personal Information</h4>";
				output += '';
				$.each(data, function(key, val){
					
					if($name == val.name){ 
						output += '<h4>'+ val.name + '</h4>';
						output += '<p>'+ val.email + '</p>';
						output += '<hr>';
						$.each(val.messages, function(key2, val2){
							if(val2.type == 0){
								output += '<div class="client-reply">';
						        output += '<div class="col-md-9">';
						        output += '<p class="message-content">';
						        output += val2.message; 
						        output += '</p></div> ';			                    
						        output += '<div class="col-md-3"><span>'+val2.created+'</span>    </div>  </div>';
					   		}
					        if(val2.type == 1){
						        output += '<div class="agent-reply"><div class="col-md-3">';
					            output += '<span>'+val2.created+'</span> </div>';
					            output += '<div class="col-md-9"><p class="message-content">  ';    
					            output += val2.message; 
					            output +=' </p>  </div></div>';
				        	}
			        	});
						output += '<div class="agent-new-reply"></div>';
			            output += '<form action="'+base_url+'" method="post" class="send-reply-message">';
			            output += '<input type="hidden" name="mid" value="'+val.cqid+'" >';
			            output += '<div class="textbox-area">';
			            output += '<textarea name="message" id="" cols="10" rows="5" class="form-control message-textarea" placeholder="click here to reply"></textarea>';
			            output += '</div>';

			            output += '<br /><button type="submit" class="btn btn_sbt btn-submit" >Send</button></form>';
			            //output += '<p>press enter to send</p>';
					}
				});
			}
			// /output += '</ul>';
			$('.mess-details').html(output);
			$(".send-reply-message").on("submit", sbt_agent_reply);
		});
	});
});