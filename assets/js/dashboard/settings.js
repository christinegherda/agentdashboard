/**
* Description: Settings page dependencies
* Author: Jovanni G
*/

class Items{

  constructor(){
    this.target = null;
    this.items = [];
    this.container = document.createElement('ul');
    this.args = {};
  }

  init(args = {}){
    this.args = args;

    this.target =  document.getElementById(args.target); // required id of the element

    this.items = (typeof args.items !== 'undefined')? args.items : []; // optional

    if(this.items.length === 0){
      this.target.textContent = (args.ifEmpty)? args.ifEmpty : 'No items.';
    }

  }

  add(item, category = '', args = {}){
    var self = this;

    if(self.items.length === 0){
      self.target.textContent = '';
      self.target.appendChild(self.container);
    }

    if(self.items.indexOf(item) < 0){
      self.items.push(item);

      var li = document.createElement('li');
      li.data = {
        field: item,
        category: category
      };

      var close = document.createElement('button');
      close.className = 'item-close';
      close.addEventListener('click', function(e){
        var _li = e.target.parentElement;

        if(typeof args.close !== 'undefined'){
          args.close(e, _li.data);
        }

        _li.parentElement.removeChild(_li);

        var childNodes = Array.prototype.slice.call( self.container.children ); // get all children of the list container.

        self.items.splice(childNodes.indexOf(_li), 1);

        if(self.items.length === 0){
          self.target.textContent = (self.args.ifEmpty)? self.args.ifEmpty : 'No items.';
        }
      });

      //li.textContent = category.replace(/_/g, ' ') + ' - ' + item.replace(/_/g, ' ');
      li.innerHTML = '<span>'+category.replace(/_/g, ' ')+'</span><span>'+item.replace(/_/g, ' ')+'</span>';
      li.appendChild(close);

      self.container.appendChild(li);

      if(typeof args.load !== 'undefined'){
        args.load(li);
      }
    }
  }

  render(){
    var self = this;

    self.items.forEach(function(item, index){
      self.add(item);
    });

    self.target.appendChild(self.container);

  }
}

function updateStringField($data = {}){

  var string = '(';

  var keys = Object.keys($data);
  keys.forEach(function($key, $i){

    $data[$key].forEach(function($fields, $index){

      string += '\"' + $key.replace(/_/g, ' ') + '\".\"' + $fields.replace(/_/g, ' ') + '\" Eq true';

      if( $index < ($data[$key].length - 1) ){
        string += ' OR ';
      }

    });

    if( $i < (keys.length - 1) ){
      string += ' OR ';
    }
    
  });

  string += ')';

  console.log(string);
  return (keys.length > 0)? string : '';
}

function updateStringCustomField($data = {}){

  var string = '(';

  var keys = Object.keys($data);
  keys.forEach(function($category, $i){

    var fields = Object.keys($data[$category]);
    fields.forEach(function($field, $index){

      if($data[$category][$field].type === 'Decimal' || $data[$category][$field].type === 'Integer'){

        string += '\"' + $category.replace(/_/g, ' ') + '\".\"' + $field.replace(/_/g, ' ') + '\" Eq ' + $data[$category][$field].value;

      }else if($data[$category][$field].type === 'Character'){

        string += '\"' + $category.replace(/_/g, ' ') + '\".\"' + $field.replace(/_/g, ' ') + '\" Eq \'' + $data[$category][$field].value + '\'';

      }else{
        string += '\"' + $category.replace(/_/g, ' ') + '\".\"' + $field.replace(/_/g, ' ') + '\" Eq true';
      }

      if( $index < (fields.length - 1) ){
        string += ' OR ';
      }

    });

    if( $i < (keys.length - 1) ){
      string += ' OR ';
    }
    
  });

  string += ')';

  console.log(string);
  return (keys.length > 0)? string : '';
}

function collapse(target){

  var target_id = target.getAttribute('data-target');

  $(target).toggleClass('open');
  //$('#'+target_id).toggleClass('in');
  $(target.nextElementSibling).toggleClass('in');

}

function clearSearch(button, $section){
  
  $('#'+$section+' .panel[data-search]').removeClass('hide');
  $('#'+$section+' .collapse').removeClass('in');
  $(button.previousElementSibling).val('');
  $(button).addClass('hide');
  $('#'+$section+' [data-toggle="collapse"]').removeClass('open');
}

function searchFields(input, $section){

  if(input.value.length > 0){
    $(input.nextElementSibling).removeClass('hide');
  }else{
    $(input.nextElementSibling).addClass('hide');
  }

  if(input.value !== '' && input.value.length > 1){

    $('#'+$section+' .collapse').removeClass('in');
    $('#'+$section+' .panel[data-search]').addClass('hide');
    $('#'+$section+' .panel[data-search*="'+input.value.toLowerCase()+'"]').removeClass('hide');
    $('#'+$section+' .panel[data-search*="'+input.value.toLowerCase()+'"] .collapse').addClass('in');
    $('#'+$section+' [data-toggle="collapse"]').addClass('open');

    $('#'+$section+' .field-item[data-search]').removeClass('highlight');
    $('#'+$section+' .field-item[data-search*="'+input.value.toLowerCase()+'"]').addClass('highlight');

  }else{
    $('#'+$section+' .panel[data-search]').removeClass('hide');
    $('#'+$section+' .collapse').removeClass('in');
    $('#'+$section+' [data-toggle="collapse"]').removeClass('open');

    $('#'+$section+' .field-item[data-search]').removeClass('highlight');
  }
}

(function($){
  $('li.dropdown.user-menu a').on('click', function (event) {
      $(this).parent().toggleClass('open');
  });
  $('body').on('click', function (e) {
      if (!$('li.dropdown.user-menu').is(e.target) 
          && $('li.dropdown.user-menu').has(e.target).length === 0 
          && $('.open').has(e.target).length === 0
      ) {
          $('li.dropdown.user-menu').removeClass('open');
      }
  });
})(jQuery);
