(function($) {

  var add_location = function( eve ){
    eve.preventDefault();
    $("#countries-clists").show();
    $count = $('.state-item-location').length;
    $count = $count + 1;

    var locationName = $("#audience-location").val();
    
    //li_html = '<li id="'+$count+'" class="country-item-location"><button type="button" class="close close_location_list" data-locname="'+$count+'" data-dismiss="country-item" aria-hidden="true">×</button><input type="hidden" name="location_list[]" value="'+ locationName +'"><p class="country-name">' + locationName + '</p></li>';
    li_html = '<li id="'+$count+'" class="state-item-location"><button type="button" class="close close_location_list" data-locname="'+$count+'" data-dismiss="country-item" aria-hidden="true">×</button><input type="hidden" name="location_list[]" value="'+ locationName +'">' + locationName + '</li>';
    console.log(li_html);
    $("ul.slists").append(li_html);

    $(".close_location_list").on("click", delete_location_list);
  };

  var delete_location_list = function (eve){
    eve.preventDefault();
    var lname = $(this).data("locname");
    console.log(lname);
    $("#"+lname).hide();
  };

  var connection_type = function (eve){
    eve.preventDefault();

    var conType = $(this).data("val");

    $("#connection-type-area").val(conType);
    
    $("#connection-area").text(conType);
  };

  var sbt_save_audience = function( eve ){
    eve.preventDefault();
    //alert("sd");
    var formData = $(this).serialize();
    var $url = $(this).attr("action");

    $.ajax({
            url: $url,
            type: 'POST',
            data : formData,
            dataType: 'json',
            beforeSend: function (){         
              $("#sbt-save-audience").text("Saving Audience... ");
            },            
            success: function(data) {
                console.log(data);
                $("#sbt-save-audience").text("Saved Audience");                
            }
        });

  };

  var sbt_placement_sched = function (e){
    e.preventDefault();
    //alert("sdsd");
    var formData = $(this).serialize();
    var $url = $(this).attr("action");

    $.ajax({
            url: $url,
            type: 'POST',
            data : formData,
            dataType: 'json',
            beforeSend: function (){         
              //$("#sbt-placement-sche").text("Saving Data... ");
             // setTimeout(1000);
              $(".loader").fadeIn("slow");
            },            
            success: function(data) {
                console.log(data);
                
                if(data.success)
                {
                   window.location.href = data.redirect; 
                }    
                else
                {
                  $(".loader").fadeOut("slow"); 
                }

                
            }
        });

  };

  var sbt_ad_set = function( e ){
    e.preventDefault();

    var formData = $(this).serialize();   
    var $url = $(this).attr("action");
   
    $.ajax({
            url: $url,
            type: 'POST',
            data : formData,
            dataType: 'json',
            beforeSend: function (){         
              $(".loader").fadeIn("slow");
            },            
            success: function(data) {
                console.log(data);
                
                if(data.success)
                {
                   window.location.href = data.redirect; 
                }    
                else
                {
                  $(".loader").fadeOut("slow"); 
                }

                $(".loader").fadeOut("slow");   
            }
        });

  };


  var placement_area = function(){
   
    $val = $(this).val();
    
    if( $val == "edit_placements" )
    {
      $("#edit-placement-area").show();
    }
    else
    {
      $("#edit-placement-area").hide();
    }
  };

  var budget_price_display = function(){
      $val = $(".budget-price-input").val();
      $val_per_week = $val * 7;

     // $('#budget-per-week-p').show();
      $("#budget-per-week").text($val_per_week);
  };

  var choose_property = function(e) {
    e.preventDefault();

    $property_id = $(this).val();
    $url = base_url + 'fb_ads/fb_ads/getPropertyDatas/' + $property_id;

    $.ajax({
            url: $url,
            type: 'POST',
            dataType: 'json',
                      
            success: function(data) {
                console.log(data);

                $("#countries-clists").show();
                $("#state-lists").show();
                $count = $('.state-item-location').length;
                $count = $count + 1;

                if( data.is_idx )
                {
                  var locationState = data.datas[0].StandardFields.StateOrProvince;
                  var locationName = data.saveKeyCountryCode;
                  var ad_set_name = data.datas[0].StandardFields.UnparsedFirstLineAddress;
                }
                else
                {
                  var locationState = data.datas.state;
                  var locationName = data.saveKeyCountryCode;
                  var ad_set_name = data.datas.unparsedaddr;
                }
                
                //li_html = '<li id="'+$count+'" class="country-item-location"><button type="button" class="close close_location_list" data-locname="'+$count+'" data-dismiss="country-item" aria-hidden="true">×</button><input type="hidden" name="location_list[]" value="'+ locationName +'"><p class="country-name">' + locationName + '</p><ul class="subclists"><li><input type="hidden" name="location_state" value="'+ locationState +'">'+locationState+'</li><ul/></li>';
                li_html = '<li id="'+$count+'" class="state-item-location" ><input type="hidden" name="location_list[]" value="'+ locationState +'">' + locationState + '</li>';
               
                $("ul.slists").append(li_html);

                // populate ad set name
                $("#ad-set-name").val(ad_set_name);

                //populate in right side bar
                audience_li_html = '<li>Location: <ul class="sub-audience-details"><li>United States: '+ locationState +'</li></ul></li>';
                $("ul.audience-details-ul").append(audience_li_html);

                $(".close_location_list").on("click", delete_location_list);
            }
        });

  };

  $(".add-location").on( "click", add_location);
  $(".connection_type").on("click", connection_type);

  $("#save-audience").on( "submit" , sbt_save_audience);
  //
  $("#sbt-placement-sched").on("submit", sbt_placement_sched); 

  //
  $("#sbt-ad-setup").on( "submit", sbt_ad_set );
  //placement
  $(".placement-area").on("click", placement_area);

  // 
  $(".budget-price-input").on("change", budget_price_display);

  //
  $('#choose-property').on("change", choose_property);

  //age change
  $("#min_age").on("change", function () {
      $this = $(this);
      $min_age =  $this.val();
      $("#min-age").text($min_age);

  });

  //age change
  $("#max_age").on("change", function () {
      $this = $(this);
      $max_age =  $this.val();

      if( $max_age == 65){
        $max_age = $max_age + "+";     
      }
      $("#max-age").text($max_age);

  });

})(jQuery);