<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <link rel="icon" type="image/x-icon"  href="<?php echo isset($branding->favicon) ? base_url().'assets/upload/favicon/'.$branding->favicon :  base_url().'assets/images/default-favicon.png';?>"/>
    <link rel="apple-touch-icon" href="<?= base_url()?>assets/img/home/apple-touch-icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/fonts/fontello/css/fontello.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard.css">
    <?php if(isset($css)) :
        foreach($css as $key => $val ) :  ?>
        <link rel="stylesheet" href="<?= base_url()?>assets/css/<?php echo $val?>" type="text/css"> </link>
        <?php endforeach;?>
    <?php endif;?>
    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
    <style type="text/css">
        #instruction {
            margin-top: 220px;
        }
        #instruction button {
            padding: 20px;
        }
    </style>
</head>
<body>
    <div id="wrapper" >
        <div class="container">
            <div class="row" >
                <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-0">
                    <h1 class="page-header text-center">Setting Up CName on your HOST</h1>
                    <?php
                        $cname = str_replace(array('http://','https://'), '', $cname->subdomain_url);
                    ?>

                    <!-- Hide CNAME as we are not using it anymore-DEV-3041 -->
                    <!-- <div class="alert alert-success">
                      <span class="glyphicon glyphicon-info-sign"></span> Your CNAME address is <strong><?php echo $cname?></strong>
                    </div> -->

                    <!-- ENOM INSTRUCTIONS HERE -->
                    <div class="alert alert-info fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a> 
                        <span class="glyphicon glyphicon-info-sign"></span> For  <strong>ENOM</strong> domain registrant please follow the steps below:
                    </div>

                    <div class="enom-instruction">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#enomOne"> 1. Go to your ENOM website if you are using this domain provider (ENOM) to https://www.enom.com/ <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="enomOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>Go to your ENOM website if you are using this domain provider (ENOM) to <a href="https://www.enom.com/">https://www.enom.com/</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#enomTwo"> 2. Click on My account. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="enomTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <img src="<?php echo base_url()?>/assets/images/enom-image2.png"/>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#enomThree">  3. Login to your ENOM account. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="enomThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <img src="<?php echo base_url()?>/assets/images/enom-image3.png"/>
                                    </div>
                                </div>
                            </div>
                             <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#enomFour">  4. Search for your registered domain on your ENOM account and click on magnifying glass after. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="enomFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Select and click on the Host Records.</p><br>
                                       <img src="<?php echo base_url()?>/assets/images/enom-image4.png"/>
                                    </div>
                                </div>
                            </div>
                             <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#enomFive">  5. Follow the Settings below. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="enomFive" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>5.1 On Manage Domain, click and select Host Records.</p>
                                        <p>5.2 Follow the settings for A records on the screenshot below and click on save.</p>
                                       <img src="<?php echo base_url()?>/assets/images/enom-image5.png"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- GODADDY INSTRUCTIONS HERE -->
                     <div class="alert alert-info fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <span class="glyphicon glyphicon-info-sign"></span> For  <strong>GODADDY</strong> domain registrant please follow the steps below:
                    </div>

                    <div class="godaddy-instruction">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#godaddyOne"> 1. Go to GoDaddy site and click on Sign-in <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="godaddyOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/godaddy-image1.png"/><br>
                                        </div>
                                        <!--   <div class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/godaddy-img1.2.png"/>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#godaddyTwo">2. Look for your domain and click on DNS right beside it. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="godaddyTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/godaddy-image2.png"/><br>
                                        </div>
                                         <!--  <div class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/godaddy-img2.1.png"/>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#godaddyThree"> 3. To Add A records, click on ADD Button on the bottom most part. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                   
                                </div>
                                <div id="godaddyThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/godaddy-image3.png"/><br>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#godaddyFour"> 4. Follow these settings below and click on save. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                   
                                </div>
                                <div id="godaddyFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/godaddy-image4.png"/><br>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- NETWORK SOLUTIONS INSTRUCTIONS HERE -->
                     <div class="alert alert-info fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <span class="glyphicon glyphicon-info-sign"></span> For  <strong>NETWORK SOLUTIONS</strong> domain registrant please follow the steps below:
                    </div>

                    <div class="network-instruction">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#networkOne"> 1. Login to your Network Solutions Account <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="networkOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img1.png"/><br>
                                        </div>
                                          <div class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img1.2.png"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#networkTwo">2. Click on My Domains Name. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                </div>
                                <div id="networkTwo" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <p>Once you have found your desired domain name, click on change where my domain points.</p>
                                        <p>Click on Advanced DNS.</p>
                                        <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img2.png"/><br>
                                        </div>
                                        <div class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img2.1.png"/>
                                        </div>
                                         <div class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img2.2.png"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#networkThree"> 3.  Click on Edit A record to configure A record IP address. <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                    </h4>
                                   
                                </div>
                                <div id="networkThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img3.0.png"/><br>
                                        </div>
                                        <p><strong>Follow the format below when you configure A record IP address and use this IP address 52.27.197.57</strong></p>
                                            <p>Once you are done, click on save and proceed to configuring CNAMES.</p>
                                          <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img3.1.png"/>
                                        </div>

                                        <p><strong>Scroll all the way down and look for CNAME Records and click on Edit CNAME Records</strong></p>

                                        <div style="padding-bottom: 30px;" class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img3.2.png"/>
                                        </div>

                                        <p><strong>Follow the format below when you add CNAMES and configure the A record.</strong></p>
                                        <p>Once you are done, click on continue and confirm your settings.</p>

                                        <div class="col-md-12">
                                            <img src="<?php echo base_url()?>/assets/images/network-img3.3.png"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="<?= base_url()?>assets/js/dashboard/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        

    </script>
    <!-- <script src="<?= base_url()?>assets/js/dashboard/main.js"></script> -->
</body>
</html>
