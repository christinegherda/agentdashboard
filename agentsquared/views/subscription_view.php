<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <link rel="icon" type="image/x-icon"  href="<?php echo isset($branding->favicon) ? base_url().'assets/upload/favicon/'.$branding->favicon :  base_url().'assets/images/default-favicon.png';?>"/>
    <link rel="apple-touch-icon" href="<?=base_url()?>assets/img/home/apple-touch-icon.png">
    <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/fonts/fontello/css/fontello.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/dashboard/plugins/msgBoxLight.css">
    <!-- <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard.css"> -->
    <?php if(isset($css)) :
        foreach($css as $key => $val ) :  ?>
        <link rel="stylesheet" href="<?= base_url()?>assets/css/<?php echo $val?>" type="text/css"> </link>
        <?php endforeach;?>
    <?php endif;?>
    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
    <style type="text/css">
        
        table.tablesorter thead tr .headerSortUp {
            background-image: url('../assets/images/asc.gif');
            background-repeat: no-repeat;
            background-position: center right;
            cursor: pointer;
            background-color: #3399FF; 
        } 

        table.tablesorter thead tr .headerSortDown {
            background-image: url('../assets/images/desc.gif');
            background-color: #3399FF;
            background-repeat: no-repeat;
            background-position: center right;
            cursor: pointer;
        }

        th.header { 
            background-image: url('../assets/images/bg.gif');
            background-repeat: no-repeat;
            background-position: center right;
            cursor: pointer;
        }

    </style>
</head>
<body>
    <div id="wrapper" >
        <div class="container-fluid">
            <br>
            <br>
            <div class="row" >
                <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-0">
                    <div class="col-md-6 pull-left">
                        <a href="<?=base_url().'users_subscription/admin_logout';?>" class="btn btn-danger btn-sm">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>
                    </div>
                    <div class="col-md-6 pull-right">
                        <form action="<?=base_url().'users_subscription'?>" method="GET" id="search_subscription_form" class="form-inline">
                            <div class="col-md-5">
                                <div class="form-group pull-right">
                                    <label for="type">Subscription Type: </label>
                                    <select name="type" class="form-control">
                                        <option value="all" <?=($this->input->get('type') == 'all') ? 'selected' : '';?>>All</option>
                                        <option value="trial" <?=($this->input->get('type') == 'trial') ? 'selected' : '';?>>Trial</option>
                                        <option value="paid" <?=($this->input->get('type') == 'paid') ? 'selected' : '';?>>Paid</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-7 text-center">
                                <div class="form-group">
                                    <label for="keywords">Keywords: </label>
                                    <input type="text" name="keywords" placeholder="Name, Email, Agent ID, MLS Name" class="form-control" value="<?=($this->input->get('keywords')) ? $this->input->get('keywords') : '';?>">
                                    <button type="submit" class="btn btn-success" style="margin-left: 50px;">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row" >
                <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-0">
                    <div class="table-responsive">      
                        <table class="table table table-hover tablesorter" id="myTable">
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Agent ID</th>
                                    <th>MLS Name</th>
                                    <th>Trial</th>
                                    <th>Sync Status</th>
                                    <th>Date Signed</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(isset($users) && !empty($users)) {

                                        $count=0;

                                        foreach($users as $user) { ?>
                                            <tr class="<?=($count%2 == 0) ? 'success' : 'warning';?> <?=($user->sync_status) ? '' : 'danger';?>" id="tr-<?=$user->id;?>">
                                                <td><?=$user->id?></td>
                                                <td><?=$user->first_name. " " .$user->last_name?></td>
                                                <td><?=$user->email?></td>
                                                <td><?=$user->agent_id?></td>
                                                <td><?=$user->mls_name?></td>
                                                <td><?=($user->trial) ? 1 : 0;?></td>
                                                <td>
                                                    <!-- <?=($user->sync_status) ? 'success' : '<button class="btn btn-danger btn-sm">Sync Now!</button>';?> -->
                                                    <?php
                                                        if($user->sync_status) { ?>
                                                            <button type="button" class="btn btn-success btn-sm">Successful</button>
                                                    <?php
                                                        } else { 

                                                            $url = AGENT_DASHBOARD_URL."/sales_auth/authenticate?agent_id=".$user->agent_id."&token=".$user->access_token; ?>

                                                            <a href='<?=$url?>' target='_blank' class="btn btn-danger btn-sm"> Sync Now!</a>

                                                    <?php

                                                        }
                                                    ?>       
                                                </td>
                                                <td><?=$user->date_signed?></td>
                                                <td>
                                                    <a href="#" style="color:red" class="trash_talker" data-user_id="<?=$user->id;?>" data-agent_id="<?=$user->agent_id?>">
                                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                    </a>                    
                                                </td>
                                            </tr>
                                <?php
                                            $count++;
                                        }

                                    } else { ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ul class="pagination">
                            <?=$pagination;?>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src="<?= base_url()?>assets/js/dashboard/jquery.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/dashboard/plugins/tablesorter/jquery.tablesorter.js"></script>
    <script src="<?=base_url()?>assets/js/plugins/jquery.msgBox.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#myTable").tablesorter();
        });

        $(".trash_talker").on("click", function() {

            $user_id = $(this).attr("data-user_id");
            $agent_id = $(this).attr("data-agent_id");

            $.msgBox({

                title: "Delete User Confirmation",
                content: "All records from this user will be deleted. Are you sure you want to delete this user?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "Cancel"}],
                success: function (result) {

                    console.log(result);

                    if(result == "Yes") {
                        $.ajax({
                            url : base_url+"users_subscription/removed_users_record",
                            type : "POST",
                            data: {
                                'user_id': $user_id,
                                'agent_id': $agent_id
                            },
                            dataType: "json",
                            beforeSend: function() {
                                console.log("sending...");
                            },
                            success: function(data) {

                                console.log(data);
                                
                                if(data.success) {

                                    $("#tr-"+$user_id).fadeOut("slow", function() {
                                        $.msgBox({
                                            title:"Success",
                                            content:"User Successfully Deleted!",
                                            type:"info"
                                        });
                                    }); 

                                } else {

                                    $.msgBox({
                                        title: "Error",
                                        content: "Failed to Delete user!",
                                        type: "error",
                                        buttons: [{ value: "Ok" }]                                
                                    });
                                }
                            }
                        });
                    }
                }
            });

        });

    </script>
</body>
</html>
