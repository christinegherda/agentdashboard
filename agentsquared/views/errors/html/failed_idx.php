<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo 'Failed to Create IDX Website'; ?></title>
    <link rel="icon" type="image/x-icon"  href="<?=base_url().'assets/images/default-favicon.png';?>"/>
    <link rel="apple-touch-icon" href="<?= base_url()?>assets/img/home/apple-touch-icon.png">
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard.css">
    <style type="text/css">
        #instruction {
            margin-top: 220px;
        }
        #instruction button {
            padding: 20px;
        }
    </style>
</head>
<body>
    <div id="wrapper" >
        <div class="container">
            <div class="row" >
                <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-0">
                    <h1 class="page-header text-center"><span class="glyphicon glyphicon-exclamation-sign" style="color: red"></span> Could Not Create IDX Website</h1>
                    <?php
                        switch($err_type) {
                            case 'email': ?>
                                <div class="alert alert-info">
                                  <span class="glyphicon glyphicon-info-sign"></span> Sorry, we're unable to complete your IDX website because your email was not properly set or empty in your FlexMLS profile.
                                </div>
                                <div class="alert alert-info">
                                  <span class="glyphicon glyphicon-info-sign"></span> To troubleshoot the problem, please follow the steps below:
                                </div>

                                <div class="enom-instruction">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex1"> 1. Login to your <b>FlexMLS</b> account <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex1" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <ul>
                                                        <li>Go to <a href="https://www.flexmls.com/" target="_blank">https://www.flexmls.com/</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex2"> 2. Go to <b>My Profile</b><span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex2" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Go to upper corner right of the page and click <b>My Profile</b> </p>
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email1.png"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex3">  3. Click <b>My E-Mail Addresses</b> <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex3" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email2.png"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex4">  4. Click <b>Edit</b> <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex4" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email3.png"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex5">  5. Change <b>E-Mail Type</b> to <b>Office</b> and click <b>Next&raquo;&raquo;</b> <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex5" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email4.png"/>
                                                </div>
                                                <div class="panel-body">
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email5.png"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex6">  6. <b>E-Mail</b> Successfully Changed.<span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex6" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>After successfully changing E-Mail, wait for a few minutes before launching the <b>AgentSquared - Instant IDX Website.</b></p>
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email6.png"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex7">  7. Launch <b>AgentSquared - Instant IDX Website</b><span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>Click <b>Menu</b>, and the <b>Products</b> column, click <b>AgentSquared - Instant IDX Website</b>.</p>
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email7.png"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                    <?php
                                break;
                            case 'name': ?>

                                <div class="alert alert-info">
                                  <span class="glyphicon glyphicon-info-sign"></span> Sorry, we're unable to complete your IDX website because your <b>first name</b> or <b>last name</b> is empty in your <b>FlexMLS</b> profile.
                                </div>
                                <div class="alert alert-info"><span class="glyphicon glyphicon-info-sign"></span> To troubleshoot the problem, please follow the steps below:</div>

                                <div class="enom-instruction">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex1"> 1. Login to your <b>FlexMLS</b> account <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex1" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <ul>
                                                        <li>Go to <a href="https://www.flexmls.com/" target="_blank">https://www.flexmls.com/</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex2"> 2. Go to <b>My Profile</b><span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex2" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Go to upper corner right of the page and click <b>My Profile</b> </p>
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email1.png"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex3">  3. Click <b>My Names</b> <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex3" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/noname1.png"/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex6">  4. Set <b>first name</b> and <b>last name</b>.<span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex6" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>Fill first name and last name then click Next &raquo;&raquo;. Wait for few minutes before launching <b>AgentSquared - Instant IDX Website</b></p>
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/noname2.png"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#flex7">  5. Launch <b>AgentSquared - Instant IDX Website</b><span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span></a>
                                                </h4>
                                            </div>
                                            <div id="flex7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>Click <b>Menu</b>, and the <b>Products</b> column, click <b>AgentSquared - Instant IDX Website</b>.</p>
                                                    <img src="<?=getenv('AGENT_DASHBOARD_URL')?>/assets/images/no-email7.png"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    <?php
                                break;
                            
                            case 'rate_limit': ?>
                                <div class="alert alert-info">
                                    <span class="glyphicon glyphicon-info-sign"></span> Sorry, we're unable to complete your IDX website because we are currently hitting the API's rate limit. Please try again later or you can contact our <a href='https://www.agentsquared.com/support/' target="_blank"><b>support</b></a> team for this matter. 
                                </div>
                    <?php
                                break;
                            default: ?>
                                <div class="alert alert-info">
                                    <span class="glyphicon glyphicon-info-sign"></span> Sorry, we're unable to complete your IDX website because something is not right regarding your FlexMLS profile. Please contact our <a href='https://www.agentsquared.com/support/' target="_blank"><b>support</b></a> team for this matter. 
                                </div>

                    <?php            
                                break;
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script src="<?= base_url()?>assets/js/dashboard/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>
