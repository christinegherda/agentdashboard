<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no" />
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>

   <?php if(!empty(is_freemium()) && !empty($branding->favicon)){
        $favicon = getenv('AWS_S3_ASSETS') . "uploads/favicon/".$branding->favicon;
    } elseif(!empty($branding->favicon)){
      $favicon = AGENT_DASHBOARD_URL."/assets/upload/favicon/".$branding->favicon;
    } else{
      $favicon = AGENT_DASHBOARD_URL."/assets/images/default-favicon.png";
    }?>

    <link rel="icon" type="image/x-icon"  href="<?php echo $favicon?>"/>
    
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="apple-touch-icon" href="<?= base_url()?>assets/img/home/apple-touch-icon.png">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/fonts/fontello/css/fontello.css?version=<?=AS_VERSION?>" type="text/css">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/ionicons.min.css?version=<?=AS_VERSION?>">
  <!-- <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard-compressed.css.php"> -->
   <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/AdminLTE.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/skin-blue.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/common.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/fileUploader.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/summernote.css?version=<?=AS_VERSION?>" type="text/css">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/jquery.domenu-0.48.53.css?version=<?=AS_VERSION?>" type="text/css">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/plugins/msgBoxLight.css?version=<?=AS_VERSION?>" type="text/css"> 
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" type="text/css">
  <!-- <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/owl.carousel.css" type="text/css"> -->
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/bootstrap-datepicker.css?version=<?=AS_VERSION?>" type="text/css">
  <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css" type="text/css"> 
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/plugins/jquery.rateyo.min.css?version=<?=AS_VERSION?>" type="text/css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.min.css" type="text/css"/>
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/menu.css?version=<?=AS_VERSION?>" type="text/css"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" />
  <?php if(isset($css)) :
      foreach($css as $key => $val ) :  ?>
      <link rel="stylesheet" href="<?= base_url()?>assets/css/<?php echo $val?>?version=<?=AS_VERSION?>" type="text/css"> </link>
      <?php endforeach;?>
  <?php endif;?>
  <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>

  <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '662338450585957');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=662338450585957&ev=PageView&noscript=1"
  /></noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->

  <!-- Start Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MNK2G28');</script>
  <!-- End Google Tag Manager -->
  <!-- Google ReCAPTCHA -->
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <!-- End Google ReCAPTCHA -->
  <!-- Pardot Implementation -->
  <script type="text/javascript">
    piAId = '712493';
    piCId = '2130';
    piHostname = 'pi.pardot.com';

    (function() {
      function async_load(){
        var s = document.createElement('script'); s.type = 'text/javascript';
        s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
        var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
      }
      if(window.attachEvent) { window.attachEvent('onload', async_load); }
      else { window.addEventListener('load', async_load, false); }
    })();
  </script>
  <!-- End Pardot Implementation -->

  <!--Start of Help.com Live Chat Script-->
  <script>
    window.Helpdotcom=window.Helpdotcom||function(a){window.Helpdotcom.q.push(a)},window.Helpdotcom.q=[];var d=document,s=d.createElement("script");s.type="text/javascript",s.async=!0,s.src="//plugins.help.com/livechat.js",s.id="helpdotcom-script",s.setAttribute("data-org-id","a6c44c0f-e550-429e-b00b-91e060bec82f"),s.setAttribute("data-widget-id","70303600-8f95-47ed-8d02-522bb4f2fc62");var x=d.getElementsByTagName("script")[0];x.parentNode.insertBefore(s,x);
  </script>
  <!--End of Help.com Live Chat Script-->

  <!-- Hotjar Tracking Code for dashboard.agentsquared.com -->
  <script>
    (function(h,o,t,j,a,r){
    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
    h._hjSettings={hjid:1372704,hjsv:6};
    a=o.getElementsByTagName('head')[0];
    r=o.createElement('script');r.async=1;
    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
    a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>

  <?php
    enqueue_styles();

    get_jsvars();
  ?>
</head>
