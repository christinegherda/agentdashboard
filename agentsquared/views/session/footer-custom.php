<footer class="main-footer footer-links">
    <div class="mobile-center">
      <b>AgentSquared</b>
      <a href="https://www.facebook.com/AgentSquared/"><i class="fa fa-facebook-f"></i></a>
      <a href="https://twitter.com/agentsquared"><i class="fa fa-twitter"></i></a>
      <a href="https://plus.google.com/+Agentsquared/posts"><i class="fa fa-google"></i></a>
      <a href="https://www.linkedin.com/company/agentsquared"><i class="fa fa-linkedin"></i></a>
    </div>
  </div>
</footer>
</div>

  <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js?version=<?=AS_VERSION?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/additional-methods.min.js"></script>
  <script src="<?= base_url()?>assets/js/dashboard/agent-social-media.js?version=<?=AS_VERSION?>"></script>


<?php if(isset($js)) :
  foreach($js as $key => $val ) :  ?>
    <script src="<?= base_url()?>assets/js/dashboard/<?php echo $val?>?version=<?=AS_VERSION?>" type="text/javascript"> </script>
<?php endforeach;?>
<?php endif;?>

  <?php if($this->uri->segment(1) == "agent_social_media" || $this->uri->segment(2) == "activate" || $this->uri->segment(1) == "signup" ) { ?>
    <script src="<?= base_url()?>assets/js/socialmedia/home-agent.js?version=<?=AS_VERSION?>"></script>
    <script src="<?= base_url()?>assets/js/socialmedia/popup.js?version=<?=AS_VERSION?>"></script>
    <script>
          var fb_url = <?php echo json_encode($this->facebook->login_url());?>;
          var linkdn_url = <?php echo json_encode(site_url('agent_social_media/linkedin_login'));?>;
          var twitt_url = <?php echo json_encode(site_url('agent_social_media/twitter_redirect'));?>;
    </script>

    <!-- <?php
      if(isset($_GET['post_socialmedia'])){
    ?> -->
      <script type="text/javascript">
           window.close();
           window.opener.location.reload(true);
      </script>
    <!-- <?php } ?> -->

  <?php } ?>

<script type="text/javascript">
    var launch_site = function(e)
    {
      e.preventDefault();
      var url = base_url + "dashboard/get_customer_infos";
      $('.launch-site-now').text('Loading...');
      $.ajax({
          url : url,
          type : "post",
          dataType: "json",
          beforeSend: function()
          {
            $('.launch-site-now').prop('disabled', true);
          },
          success: function( data)
          {
            console.log(data);

            if( data.domain_name == null )
            {
                window.location.href = base_url + "custom_domain"
            }
            else
            {
              $('.launch-site-now').text('Launch your site now');
              //idx key
              $("#apikey").val(data.api_key);
              $("#apisecret").val(data.api_secret);
              //branding
              $("#sitename").val(data.site_name);
              $("#tagline").val(data.tag_line);
              //profile
              $("#fname").val(data.first_name);
              $("#email").val(data.email);
              $("#onumber").val(data.phone);
              //domain info
              $("#domainname").val(data.domain_name);
              $("#launch-site").modal("show");
            }
          }
      });

    };

    var launch_site_now_temp = function (e){
      e.preventDefault();
      var url = base_url + "dashboard/launch_site_now";
      $('.launch-site-now-temp').text('Loading...');

      $.ajax({
          url : url,
          type : "post",
          dataType: "json",
          beforeSend: function()
          {
            $('.launch-site-now-temp').prop('disabled', true);
          },
          success: function( data)
          {
            if(data.success)
            {
              location.reload(true);
            }
            else
            {
              location.reload(true);
            }
          }
      });

    };

    var check_agent_domain = function(e) {
      e.preventDefault();
      var url = base_url + "dashboard/check_domain_complete";

      $.ajax({
          url : url,
          type : "POST",
          dataType: "json",
          beforeSend: function() {
            console.log(url);
          },
          success: function(data) {
            if(data.success) {
              if(data.domain_complete) {
                launch_site_now_temp(e);
              }
            } else {
              if(data.empty_domain) {
                $("#message-container").css("display", "block");
                $("#launch-response-message").html(" You don't have a domain yet. Click <a href='custom_domain'><strong>here</strong></a> to go to Custom Domain page.");
              } else {
                $("#message-container").css("display", "block");
                $("#launch-response-message").html("  Your custom domain may take up to 48 hours to work as the global DNS propagates.");
              }
            }
          }
      });
    }

    $(".launch-site-now").on("click", launch_site);
    $(".launch-site-now-temp").on("click", check_agent_domain);
</script>

<!-- DASHBOARD -->
<?php if(isset($_GET['custom_menu'])){ ?>
      <script type="text/javascript">
       $(document).ready(function() {
          $( "#default-menu" ).removeClass( "active" );
          $( "#defaultMenu" ).removeClass( "active" );
          $( "#custom-menu" ).addClass( "active" );
          $( "#customMenu" ).addClass( "active" );
          $("#custom-page-area").show();
          $("#default-page-area").hide();
        });
      </script>
<?php } ?>

 <?php if(isset($_GET['media_modal'])){ ?>
      <script type="text/javascript">
       $(document).ready(function() {
         $('#modalAddPage').modal('show');
        });
      </script>
<?php } ?>

  <?php enqueue_scripts(); ?>
</body>
</html>
