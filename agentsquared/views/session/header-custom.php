<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
  <link rel="icon" type="image/x-icon"  href="<?php if (isset($branding->favicon) AND !empty($branding->favicon)) {?><?php echo AGENT_DASHBOARD_URL . "assets/upload/favicon/$branding->favicon"?><?php } else { ?><?php echo AGENT_DASHBOARD_URL . "assets/images/default-favicon.png"?><?php } ?>"/>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="apple-touch-icon" href="<?= base_url()?>assets/img/home/apple-touch-icon.png">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/fonts/fontello/css/fontello.css?version=<?=AS_VERSION?>" type="text/css">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/ionicons.min.css?version=<?=AS_VERSION?>">
  <!-- <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard-compressed.css.php"> -->
   <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/AdminLTE.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/skin-blue.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/common.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/jquery.domenu-0.48.53.css?version=<?=AS_VERSION?>" type="text/css">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/menu.css?version=<?=AS_VERSION?>" type="text/css"/>
  <?php if(isset($css)) :
      foreach($css as $key => $val ) :  ?>
      <link rel="stylesheet" href="<?= base_url()?>assets/css/<?php echo $val?>?version=<?=AS_VERSION?>" type="text/css"> </link>
      <?php endforeach;?>
  <?php endif;?>
  <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>

  <!--Start of Help.com Live Chat Script-->
  <script>
    window.Helpdotcom=window.Helpdotcom||function(a){window.Helpdotcom.q.push(a)},window.Helpdotcom.q=[];var d=document,s=d.createElement("script");s.type="text/javascript",s.async=!0,s.src="//plugins.help.com/livechat.js",s.id="helpdotcom-script",s.setAttribute("data-org-id","a6c44c0f-e550-429e-b00b-91e060bec82f"),s.setAttribute("data-widget-id","70303600-8f95-47ed-8d02-522bb4f2fc62");var x=d.getElementsByTagName("script")[0];x.parentNode.insertBefore(s,x);
  </script>
  <!--End of Help.com Live Chat Script-->
  <!-- Pardot Implementation -->
  <script type="text/javascript">
    piAId = '712493';
    piCId = '2130';
    piHostname = 'pi.pardot.com';

    (function() {
      function async_load(){
        var s = document.createElement('script'); s.type = 'text/javascript';
        s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
        var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
      }
      if(window.attachEvent) { window.attachEvent('onload', async_load); }
      else { window.addEventListener('load', async_load, false); }
    })();
  </script>
  <!-- End Pardot Implementation -->

  <?php 
    enqueue_styles();

    get_jsvars();
  ?>
</head>