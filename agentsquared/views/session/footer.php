<!-- Agent IDX Website -->
<div id="agent-idx-website-modal" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="col-md-offset-5">
          <div class="main-header-container">
            <h3 class="main-header">Sell More Homes</h3>
            <h4 class="main-header">Build Your Online Presence Today!</h4>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="col-md-offset-5">
          <div class="main-body">
            <h4>Websites integrate with your MLS through a direct connection in FlexMLS. Buyers can search your website in your local market with real-time, instant updates.</h4>
            <ul class="idx-modal-list">
              <li>Capture Leads</li>
              <li>Add Unlimited Pages</li>
              <li>Add Saved Searches to any page</li>
            </ul>
            <div class="pricing-section clearfix">
              <div class="pricing-header">
                <h4 class="text-center">PRICING PLANS</h4>
              </div>
              <div class="col-sm-offset-3 col-md-offset-3 col-md-6 col-sm-6 no-padding-left">
                <div class="large-monthly pricing">
                  <h1>
                    <span class="dollar">$</span>
                    99
                    <span class="permonth">/month</span>
                  </h1>
                  <p>Billed Monthly<!--  - <span class="discount">Save 20%</span> --></p>
                  <div class="bottom-trial">
                    <p>15 days FREE trial<!-- . $59 monthly until canceled. --></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <a href="http://www.sparkplatform.com/appstore/apps/instant-idx-website-powered-by-flexmls" class="btn btn-default btn-block" target="_blank">Click Here to Start Your FREE 15 Days Trial</a>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal SPW Pricing -->
<div class="modal fade" id="modalSpwPricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Purchase Single Property Website</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <!-- <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="price-item">
              <h4 class="price-title">Single Property Website</h4>
              <div class="price-desc">
                 <p><sup>$</sup>99.00</p>
                 <small>per year</small>
              </div>
              <a href="javascript:void(0)" data-type="monthly" data-price="99" data-prop-form-id="" class="price-text purchase-spw">Purchase</a>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <div class="modal-body">
          <div class="row">
            <div class="col-md-5 col-sm-5">
                <div class="price-item">
                  <h4 class="price-title">Single Property Website</h4>
                  <div class="price-desc">
                      <p><sup>$</sup>10</p>
                      <small>per month</small>
                  </div>
                  <a href="javascript:void(0)" data-type="monthly" data-price="10" data-prop-form-id="" class="price-text purchase-spw">Purchase</a>
                </div>
            </div>
            <div class="col-md-1 col-sm-1">
                <p class="or-text">OR</p>
            </div>
            <div class="col-md-5 col-sm-5">
                <div class="price-item">
                  <h4 class="price-title">Single Property Website Annual</h4>
                  <div class="price-desc">
                      <p><sup>$</sup>99</p>
                      <small>per year</small>
                  </div>
                  <a href="javascript:void(0)" data-type="annual" data-price="99" data-prop-form-id="" class="price-text purchase-spw">Purchase</a>
                </div>
            </div>
          </div>
      </div>
  </div>
</div>
</div>

<!-- Modal Payment -->
<div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-label" id="myModalLabel">Checkout</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <p class="modal-text">Review Your Order:</p>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Amount</th>
                    <th>Sub-Total</th>
                    <!-- <th></th> -->
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span id="product-name"></span></td>
                    <td>$<span id="product-amount"></span></td>
                    <td>$<span class="product-sub-amount"></span></td>
                    <!-- <td><a href="#"><i class="fa fa-close"></i></a></td> -->
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="col-md-12 text-right">
            <p class="text-total">Total:$<span class="product-sub-amount"></span></p>
          </div>
          <div class="col-md-12">
            <div class="border-gray"></div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-5">
                <p class="modal-text">Payment Method:</p>
              </div>
              <div class="col-md-6">
                <img src="<?= base_url()?>assets/images/dashboard/payment_method.png" class="cc-accepted">
              </div>
            </div>
            <div class="cc-detail">
              <h3>Credit Card Details:</h3> </br>
              <form class="form-horizontal trial_stripe_sbt" id="trial_stripe_sbt" action="" method="POST">
                <div class="col-md-12"><h3><div class="col-md-12 payment-errors label label-danger"></div></h3><br></div>
                <div class="col-md-12"><h3><div class="col-md-12 payment-message label label-success"></div></h3><br></div>

                <input type="hidden" name="product_amount" value="" class="product-mount" >
                <input type="hidden" name="product_type" value="" class="product-type" >
                <input type="hidden" name="property_id" value="" class="property_id" >

                <div class="row">
                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="cardnumber" class="col-md-4">Card number:</label>
                      <div class="col-md-8 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="number" placeholder="Card Number" required >
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="cardnumber" class="col-md-4">CVV code:</label>
                      <div class="col-md-8 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="cvc" placeholder="CVV" required >
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="expirationdate" class="col-md-4">Expiration date:</label>
                      <div class="col-md-2 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="exp-month" placeholder="MM" required >
                      </div>
                      <div class="col-md-2 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="exp-year" placeholder="YYYY" required >
                      </div>
                    </div>
                  </div>

                </div>

            </div>
          </div>
          <div class="col-md-6 text-center mclear">
            <h1 class="payment-total">$<span class="product-sub-amount"></span></h1>
            <p class="payment-total-note1">Total is inclusive of VAT</p>
            <p class="text-left payment-total-note2">
              By clicking on the 'Place Order' button at the end of the order process, you are consenting to be bound by our terms and conditions contained in these <a href="<?=base_url()?>dashboard/terms_conditions" target="_blank">Terms and Conditions</a> and appearing anywhere on AgentSquared website.
            </p>
            <!-- <a href="#" class="btn btn-border-green">Place Order</a> -->
            <button type="submit" class="btn btn-border-green" id="trial_pay_now_btn"> Place Order</button>
          </div>
          </form>
          <div class="col-md-12">
            <div class="col-md-6">
              <ul class="list-link">
                <li><a href="#" target="_blank">Disclaimer</a></li>
                <li><a href="<?=base_url()?>dashboard/privacy_policy" target="_blank">Privacy Policy</a></li>
                <li><a href="<?=base_url()?>dashboard/terms_conditions" target="_blank">Terms &amp; Conditions</a></li>
              </ul>
            </div>
            <div class="col-md-6 text-right">
              <p class="powered-by">Powered By: <img src="<?= base_url()?>assets/images/dashboard/stripe-logo.png"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Sync Update -->
<div class="modal fade" id="myModalSyncUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <p class="check-mark-icon"><i class="fa fa-check-circle-o"></i></p>
        <p>Already Updated</p>
      </div>
    </div>
  </div>
</div>
<!-- Reset password modal -->
<div id="reset_pass_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reset Password</h4>
      </div>
      <div class="modal-body">
        <?php
          //$session = $this->session->flashdata('session');
          //if(isset($session)) { ?>
            <!-- <div class="alert <?=($session['status']) ? 'alert-success' : 'alert-danger'?>" role='alert'><?= $session["message"]; ?></div> -->
        <?php //} ?>
        <div class="message_password"></div>
        <form action="<?=base_url();?>login/auth/change_password_ajax" method="POST" id="change_pass_form" role="form">
          <div class="form-group">
            <label for="default">Default Password</label> <!--(Your default password is: test1234)-->
            <input type="text" name="old" class="form-control" id="old" value="test1234">
            <!-- <input type="text" class="form-control" name="default" id="default" value="test1234" disabled/> -->
          </div>
          <div class="form-group">
            <label for="new_password">New Password</label>
            <!-- <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter New Password" required/> -->
            <input type="password" name="new" value="" class="form-control" id="new" placeholder="Enter New Password">
          </div>
          <div class="form-group">
            <label for="confirm_new_password">Confirm New Password</label>
            <input type="password" name="new_confirm" value="" class="form-control" id="new_confirm" placeholder="Confirm New Password">
            <!-- <input type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password" required/> -->
          </div>
          <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6Lf9eRsUAAAAAB5lP4Em8vm4L-c1m0gSyAKOTort"></div>
          </div>
          <button type="submit" class="btn btn-submit" id="submit_change_pass_btn">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Upgrade Premium -->
<div class="modal premium" id="upgradePremium">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-close"></span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-offset-1 col-lg-10">
            <div class="text-center">
              <p>Upgrade to <span class="bold-blue-text"><span>P</span>remium</span></p>
              <p class="text-md">to unlock other features</p>
            </div>
          </div>
          <div class="col-lg-offset-2 col-lg-9 col-sm-offset-2 col-sm-9">
            <div class="price-option">
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                <div class="price-list text-center">
                  <span class="text-large"><sup>$</sup>120</span>
                  <p class="font-m">per month</p>
                  <p class="font-s">if paid monthly</p>
                  <a href="" class="btn btn-green">Upgrade</a>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <p class="or">or</p>
              </div>
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                <div class="price-list text-center">
                  <span class="text-large"><sup>$</sup>990</span>
                  <p class="font-m">per year</p>
                  <p class="font-s">if paid annually</p>
                  <a href="" class="btn btn-blue">Upgrade</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Dasboard Tour Video Modal -->
<div class="modal" id="tour-video">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-close"></span></button>
        <?php if($this->uri->segment(1) == "dashboard" ){

            $videoUrl = base_url()."assets/video/google-analytics.mp4";
          
        } elseif($this->uri->segment(1) == "agent_sites" && $this->uri->segment(2) != "saved_search"){

          $videoUrl = base_url()."assets/video/customize-homepage.mp4";

        } elseif($this->uri->segment(1) == "customize_homepage"){

          $videoUrl = base_url()."assets/video/customize-homepage-search.mp4";

        } elseif($this->uri->segment(2) == "leads"){

          $videoUrl = base_url()."assets/video/crm-lead-capture.mp4";

        } elseif($this->uri->segment(1) == "pages"){

          $videoUrl = base_url()."assets/video/add-page.mp4";

        } elseif($this->uri->segment(1) == "menu"){

          $videoUrl = base_url()."assets/video/menu.mp4";

        } elseif($this->uri->segment(1) == "agent_social_media" || $this->uri->segment(2) == "activate"){

          $videoUrl = base_url()."assets/video/activate-social-media.mp4";

        }else{
          
          $videoUrl = base_url()."assets/video/google-analytics.mp4";
        }?>
        <video controls>
            <source id="videoTour" src="<?php echo $videoUrl?>" type="video/mp4">
        </video>
      </div>
    </div>
  </div>
</div>

<!-- Cancel Pro -->
<div class="modal" id="cancelPro" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-close"></span></button>
        <div class="panel panel-danger">
          <div class="panel-heading"><h2>ARE YOU SURE YOU WANT TO CANCEL?</h2></div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
                <button type="button" class="btn btn-danger" id="cancel-yes-btn"> YES </button>
              </div>
              <div class="col-md-6">
                <button type="button" class="btn btn-default" data-dismiss="modal"> NO </button>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="cancelProDialog" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-close"></span></button>
        <div class="panel panel-primary">
          <div class="panel-heading"><h3>We hate to see you go. Please tell us below why you wish to cancel.</h3></div>
          <div class="panel-body">
            <span id="cancel_ret_msg"></span>
            <form action="<?=base_url().'agent_sites/cancel_agent_subscription'?>" method="POST" id="cancel_agent_subscription_form">
              <div class="text-center">
                <textarea name="cancel_reason" id="cancel_reason" cols="30" rows="5" class="form-control" placeholder="Please enter your cancellation reason here." required></textarea>
                <button type="submit" class="btn btn-darkblue" id="cancel_agent_account">Submit</button>
              </div>
            </form>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('session/freemium-modal'); ?>
<?php $this->load->view('session/custom-domain-modal'); ?>

<footer class="main-footer footer-links">
    <div class="mobile-center">
      <b>AgentSquared</b>
      <a href="https://www.facebook.com/AgentSquared/"><i class="fa fa-facebook-f"></i></a>
      <a href="https://twitter.com/agentsquared"><i class="fa fa-twitter"></i></a>
      <a href="https://plus.google.com/+Agentsquared/posts"><i class="fa fa-google"></i></a>
      <a href="https://www.linkedin.com/company/agentsquared"><i class="fa fa-linkedin"></i></a>
    </div>
  </div>
</footer>
</div>
  <?php 
    if($this->config->item('disallowUser')) { ?>
      <script type="text/javascript">var disallowUser=true;</script>
  <?php 
    } else { ?>
      <script type="text/javascript">var disallowUser=false;</script>
  <?php
    } 
  ?>
  <!-- ./wrapper -->
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MNK2G28" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <script src="<?= base_url()?>assets/js/dashboard/jquery.js?version=<?=AS_VERSION?>"></script>
  <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js?version=<?=AS_VERSION?>"></script>
  <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/additional-methods.min.js"></script>
  <!--<script src="<?= base_url()?>assets/js/dashboard/main.js"></script>-->
   <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
  <script src="<?= base_url()?>assets/js/dashboard/bootstrap-filestyle.js?version=<?=AS_VERSION?>"></script>
  <script src="<?= base_url()?>assets/js/dashboard/summernote.js?version=<?=AS_VERSION?>"></script>
  <script src="<?= base_url()?>assets/js/dashboard/agent-social-media.js?version=<?=AS_VERSION?>"></script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"></script>
  <script src="<?= base_url()?>assets/js/dashboard/bootstrap-datepicker.js?version=<?=AS_VERSION?>"></script>
  <script src="<?= base_url()?>assets/js/dashboard/buyer-data-load.js?version=<?=AS_VERSION?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
  <!-- <script src="<?= base_url()?>assets/js/dashboard/seller-data-load.js"></script>-->

  <!-- Select2 Js used in PAGES (insert saved search link into editor) -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

  <!-- CRM Leads Sorting -->
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

    <!-- Slider Photos JS -->
    <script src="<?= base_url()?>assets/js/slider/jquery.knob.js?version=<?=AS_VERSION?>"></script>
    <script src="<?= base_url()?>assets/js/slider/jquery.ui.widget.js?version=<?=AS_VERSION?>"></script>
    <script src="<?= base_url()?>assets/js/slider/jquery.iframe-transport.js?version=<?=AS_VERSION?>"></script>
    <script src="<?= base_url()?>assets/js/slider/jquery.fileupload.js?version=<?=AS_VERSION?>"></script>
    <script src="<?= base_url()?>assets/js/slider//script.js?version=<?=AS_VERSION?>"></script>
  

  <script src="<?= base_url()?>assets/js/dashboard/jquery.slimscroll.min.js?version=<?=AS_VERSION?>"></script>
  <!-- FastClick -->
  <script src="<?= base_url()?>assets/js/dashboard/fastclick.min.js?version=<?=AS_VERSION?>"></script>
  <script src="<?= base_url()?>assets/js/dashboard/app.min.js?version=<?=AS_VERSION?>"></script>

  <script src="<?= base_url()?>assets/js/plugins/jquery.msgBox.js?version=<?=AS_VERSION?>"></script>

  <!-- Location -->
<!--<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBHzIGp5FJxKOLe5NtgKoLlNg6beKYdtm8'></script> -->
  <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBHzIGp5FJxKOLe5NtgKoLlNg6beKYdtm8'></script>
  <script src="<?= base_url()?>assets/js/dashboard/locationpicker.jquery.min.js?version=<?=AS_VERSION?>"></script>
  <!-- File Uploader -->
  <script src="<?= base_url()?>assets/js/dashboard/fileUploader.min.js?version=<?=AS_VERSION?>"></script>
  <!-- rating -->
  <script src="<?= base_url()?>assets/js/dashboard/plugins/jquery.rateyo.min.js?version=<?=AS_VERSION?>"></script>

<?php if(isset($js)) :
  foreach($js as $key => $val ) :  ?>
    <script src="<?= base_url()?>assets/js/dashboard/<?php echo $val?>?version=<?=AS_VERSION?>" type="text/javascript"> </script>
<?php endforeach;?>
<?php endif;?>
  <?php if ($this->uri->segment(1) == "customize_homepage"): ?>
    
  <!-- Fix Sortbale js issue -->
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

  <script>


     //options-accordion settings
    $(document).ready(function(){
        $('#options-accordion').sortable({
            cancel: ".fixed,input",
            connectWith:".panel-default",
            delay: 100,
            stop: function (event, ui) {
                //p.HandleSortPareto(ui, ui.item)  // note this just handles the change in order
            },
            start: function (event, ui) {}
        });

         //fadeout alert data
        $(".alert-flash").fadeOut(1000);
    });    
  </script>

  <?php endif ?>
<!-- <?php if($this->uri->segment(1) == "crm") { ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
  <script>
  new Morris.Bar({
        // ID of the element in which to draw the chart.
        element: 'salesummary',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: [
          { sold: 'January', Rent:2, Sale:1 },
          { sold: 'February', Rent:5, Sale:5 },
          { sold: 'March', Rent:2, Sale:1 },
          { sold: 'April', Rent:8, Sale:0 },
          { sold: 'May', Rent:1, Sale:6 }
        ],
        // The name of the data record attribute that contains x-values.
        xkey: 'sold',
        // A list of names of data record attributes that contain y-values.
        ykeys: ['Rent','Sale'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Rentals Sold','House Sales']
  </script>
<?php } ?> -->
<?php if ($this->input->get('upgrade_premium')==TRUE): ?>
  <script>
    $(document).ready(function() {
      $("#upgradePremium").modal("show");
    });
  </script>
<?php endif ?>
<?php if ($this->input->get('trial_checkout')==TRUE): ?>
  <script>
    $(document).ready(function() {
      $("#modalTrialForm").modal("show");
    });
  </script>
<?php endif ?>
<?php if ($this->input->get('modal_premium')==TRUE): ?>
  <script>
    $(document).ready(function() {
      $("#modalPremium").show();
      $("#modalPremium .close, #modalPremium .link-decline").click(function(){
        $("#modalPremium").hide();
      });
      $("#modalPremium a").click(function(){
        $(".helpdotcom-livechat.helpdotcom-bottom-right").css("z-index","1");
      });
    });
  </script>
<?php endif ?>
<?php if ( $this->input->get('modal_welcome')==TRUE && !$this->config->item('trial') ): // For freemium agents ?>
  <script>
    $(document).ready(function() {
      $("#modalWelcome").modal("show");
    });
  </script>
<?php endif ?>
<?php if ($this->input->get('modal_sales')==TRUE): ?>
  <script>
    $(document).ready(function() {
      $("#modalSales").modal("show");
      $(".helpdotcom-livechat").addClass("modal_show");
    });
  </script>
<?php endif ?>
<?php if ($this->input->get('modal_checkout')==TRUE): ?>
  <script>
    $(document).ready(function() {
      $("#modalCheckout").modal("show");
    });
  </script>
<?php endif ?>
<?php if($this->input->get('welcome_agent')==TRUE) { ?>
  <script>
    $(document).ready(function() {
      $("#modalWelcomeAgent").modal('show');
    });
  </script>
<?php } ?>
<?php if($this->input->get('reset_pass')==TRUE) { ?>
  <script>
    $(document).ready(function() {

      $("#reset_pass_modal").modal('show');

      $("#change_pass_form").on("submit", function(e) {
        e.preventDefault();
        $dataString = $(this).serialize();
        $.ajax({
          type:'POST',
          url: $(this).attr('action'),
          data: $dataString,
          dataType: 'json',
          beforeSend: function() {
            $("#submit_change_pass_btn").text("Submitting...");
            $("#submit_change_pass_btn").prop("disabled", true);
          },
          success: function(data) {
            console.log(data);
            if(data.success) {
              $(".message_password").html("<div class='alert alert-success alert-dismissable'><a href='#'' class='close' data-dismiss='alert' aria-label='close'>×</a><strong>"+data.message+"</strong></div>");
            } else {
              $(".message_password").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
            }
            $("#submit_change_pass_btn").text("Submit");
            $("#submit_change_pass_btn").prop("disabled", false);
            setTimeout(function() {
                window.location.href = data.redirect;
            }, 3000);
          }

        });
      });

    });
  </script>
<?php } if($this->uri->segment(2) == "schedules") { ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.datepicker-area').datepicker({
          'todayHighlight':true
      });

      $('.select-date .input-group.date').datepicker({
          'todayHighlight':true
      });
    });
  </script>
<?php } ?>

<script src="<?= base_url()?>assets/js/dashboard/owl.carousel.js?version=<?=AS_VERSION?>"></script>
  <script type="text/javascript">
    $(document).ready(function(){

      $('#audience-map').hide();
      $('#audience-map').locationpicker({
          location: {
              latitude: 0,
              longitude: 0
          },
          radius: 300,
          inputBinding: {
              latitudeInput: $('#us3-lat'),
              longitudeInput: $('#us3-lon'),
              radiusInput: $('#us3-radius'),
              locationNameInput: $('#audience-location')
          },
          enableAutocomplete: true,
          onchanged: function (currentLocation, radius, isMarkerDropped) {
              // Uncomment line below to show alert on each Location Changed event
              // var addressComponents = $(this).locationpicker('map').location.addressComponents;
              // updateControls(addressComponents);
          }
      });

      $( "#startdatepicker" ).datepicker({format: "dd-mm-yyyy"});
      $( "#enddatepicker" ).datepicker({format: "dd-mm-yyyy"});

      $( "#startdatepickerfb" ).datepicker({format: "dd-mm-yyyy"});
      $( "#enddatepickerfb" ).datepicker({format: "dd-mm-yyyy"});

      $(".property-detail-images").owlCarousel({
          navigation : true,
          pagination : true,
          slideSpeed : 700,
          paginationSpeed : 400,
          navigationText: ["<i class='fa fa-angle-left fa-lg'></i> <span>prev</span>","<span>next</span> <i class='fa fa-angle-right fa-lg'></i>"],
          items : 4,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
          // "singleItem:true" is a shortcut for:
          // items : 1,
          // itemsDesktop : false,
           // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
    });
  </script>

  <?php if($this->uri->segment(1) === "agent_social_media" || $this->uri->segment(2) === "activate" || $this->uri->segment(1) === "signup" ) { ?>
	<script src="<?= base_url()?>assets/js/socialmedia/home-agent.js?version=<?=AS_VERSION?>"></script>
    <script src="<?= base_url()?>assets/js/socialmedia/popup.js?version=<?=AS_VERSION?>"></script>
    <script>
          var fb_url = "<?php echo $this->facebook->getReAuthenticationUrl();?>";
          var linkdn_url = <?php echo json_encode(site_url('agent_social_media/linkedin_login'));?>;
          var twitt_url = <?php echo json_encode(site_url('agent_social_media/twitter_redirect'));?>;
    </script>
    <script>
      function PopulateOption() {
          var dropdown = document.getElementById("postSelected");
          var field = document.getElementById("post-area");
          field.value = dropdown.value;
      }
    </script>

    <!-- <?php
      if(isset($_GET['post_socialmedia'])){
    ?> -->
      <script type="text/javascript">
           window.close();
           window.opener.location.reload(true);
      </script>
    <!-- <?php } ?> -->

  <?php } ?>

  <?php if($this->uri->segment(1) == "menu" || $this->uri->segment(1) == "customize_homepage" ) { ?>

    <script type="text/javascript">
      $(document).ready(function(){
        //custom menu link validation
        $("#customLinkMenu").validate({
            rules: {
              custom_menu_url: {
                required: true,
                url: true
              },
              custom_link_menu_name: {
                required: true,
                maxlength: 40
              }
            },
            messages: {
              custom_menu_url: {
                required: "Enter page's link",
                url: "Enter a valid url"
              },
              custom_link_menu_name: {
                required: "Enter menu name",
              }
            }
        });

        //Customize homepage edit label
        $("#navLabel").validate({
            rules: {
              search_name: {
                required: true,
              },
            }
        });
      });
    </script>
  <?php } ?>

  <?php if($this->uri->segment(1) == "agent_social_media" || $this->uri->segment(2) == "activate" ) { ?>
   <div id="fb-root"><!-- for facebook UI share dialogs --></div>
   <script id="facebook-jssdk" async src="https://connect.facebook.net/en_US/sdk.js"></script>
   <script type="text/javascript">
    $(document).ready(function(){
      $(".panel-collapse").hide();
      $(".moreDetails").show();
      $(".panel-heading").on("click",function(){
        var $this = $(this);

        $this.stop().next().slideDown().parent().siblings().find('.panel-collapse').slideUp();
        $this.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-minus-circle')
          .parents('.panel-default').siblings().find('.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
      })
    })
   </script>
  <?php } ?>
  <?php if($this->uri->segment(1) == "choose_theme") { ?>

  <script>
    $(document).ready(function() {

    $('.owl-carousel-preview').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        items:1,
        smartSpeed:450,
        autoplay:true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"]
    })

    });
  </script>
  <?php } ?>

  <script type="text/javascript">

    $('#cancelPro').on('hidden.bs.modal', function(){
      $(this).find('form')[0].reset(); //Empty all fields in the form upon closing the modal
    });

    // This identifies your website in the createToken call below
    //Stripe.setPublishableKey('pk_test_EIuy5UKYdWcXPNBDUZIh50vD');
    //Stripe.setPublishableKey('pk_test_dyT268VmJk1HwZd7K9HwkRjr');
    Stripe.setPublishableKey('<?php echo getenv('STRIPE_PUB_KEY');?>');
  </script>
  <script type="text/javascript">
    jQuery(function($) {
      $('#stripe_sbt').submit(function(e) {

        $(".loader").fadeIn("slow");

        var $form = $('#stripe_sbt');

        // Disable the submit button to prevent repeated clicks
        $('#pay_now_btn').prop('disabled', true);
        Stripe.card.createToken($form, stripeResponseHandler);

        // Prevent the form from submitting with the default action
        return false;
      });

      $('#trial_stripe_sbt').submit(function(e) {

        $(".loader").fadeIn("slow");

        var $form = $('#trial_stripe_sbt');

        // Disable the submit button to prevent repeated clicks
        $('#trial_pay_now_btn').prop('disabled', true);
        Stripe.card.createToken($form, stripeResponse);

        // Prevent the form from submitting with the default action
        return false;
      });
      //for freemium payment
      $('#freemium_stripe_sbt').submit(function(e) {

        $(".loader").fadeIn("slow");

        var $form = $('#freemium_stripe_sbt');

        // Disable the submit button to prevent repeated clicks
        $('#freemium_pay_now_btn').prop('disabled', true);
        
        Stripe.card.createToken($form, freemiunStripeResponse);

        // Prevent the form from submitting with the default action
        return false;
      });

      var stripeResponse = function(status, response) {
        var $form = $('#trial_stripe_sbt');

        if (response.error) {
          $(".loader").fadeOut("slow");
          // Show the errors on the form
          $form.find('.payment-errors').css({"font-size" : "14px"});
          $form.find('.payment-errors').text(response.error.message);
          $('#trial_pay_now_btn').prop('disabled', false);

        } else {
          // token contains id, last4, and card type
          var token = response.id;
          // Insert the token into the form so it gets submitted to the server
          $form.append($('<input type="hidden" name="stripeToken" />').val(token));
          // and re-submit
          //$form.get(0).submit();
          trial_stripe_form_submit( $form.serialize() );

        }
      };

      var freemiunStripeResponse = function(status, response) {
        var $form = $('#freemium_stripe_sbt');

        if (response.error) {
          $(".loader").fadeOut("slow");
          // Show the errors on the form
          $form.find('.payment-error').css({"font-size" : "14px"});
          $form.find('.payment-error').text(response.error.message);
          $('#freemium_pay_now_btn').prop('disabled', false);

        } else {
          // token contains id, last4, and card type
          var token = response.id;
          // Insert the token into the form so it gets submitted to the server
          $form.append($('<input type="hidden" name="stripeToken" />').val(token));
          // and re-submit
          //$form.get(0).submit();
          freemium_stripe_form_submit( $form.serialize() );

        }
      };

      function trial_stripe_form_submit( post )
      {
        var url = base_url + "payment/pay_trial";
        var datas = post;

        $.ajax({
          url : url,
          type : "post",
          data : datas,
          dataType: "json",
          success: function( data){
            console.log(data);
            $(".loader").fadeOut("slow");
            if(data.success) {

              //For spw website creation
              if(data.property_id != "") {
                $("#"+data.property_id).submit();
              }

              $(".payment-error").hide();

              $(".payment-message").fadeIn( function () {
                  $(".payment-message").html(data.message);
                  setTimeout(function(){
                    if(window.location.search.indexOf('payment=success') === -1){
                      window.location.href += '?payment=success';
                    }
                    //location.reload();
                  }, 1000);
              });
            }
            else
            {
              $(".payment-error").fadeIn( function () {
                  $(".payment-error").html(data.message);
              });
            }
          }
        });
      }

      function freemium_stripe_form_submit( post )
      {
        var url = base_url + "payment/pay_freemium";
        var datas = post;

        $.ajax({
          url : url,
          type : "post",
          data : datas,
          dataType: "json",
          success: function(data){
            console.log(data);
            $(".loader").fadeOut("slow");
            if(data.success) {

              //For spw website creation
              if(data.property_id != "") {
                $("#"+data.property_id).submit();
              }

              $(".payment-error").hide();

              $(".payment-message").fadeIn( function () {
                  $(".payment-message").html(data.message);
                  setTimeout(function(){
                    if(window.location.search.indexOf('payment=success') === -1){
                      //window.location.href += '?payment=success';
                      window.location.href = data.redirect;
                    }
                    //location.reload();
                  }, 1000);
              });
            }
            else
            {
              $(".payment-error").fadeIn( function () {
                  $(".payment-error").html(data.message);
              });
            }
          }
        });
      }

      var stripeResponseHandler = function(status, response) {
        var $form = $('#stripe_sbt');

        if (response.error) {
          $(".loader").fadeOut("slow");
          // Show the errors on the form
          $form.find('.payment-errors').css({"font-size" : "14px"});
          $form.find('.payment-errors').text(response.error.message);
          $('#pay_now_btn').prop('disabled', false);

        } else {
          // token contains id, last4, and card type
          var token = response.id;
          // Insert the token into the form so it gets submitted to the server
          $form.append($('<input type="hidden" name="stripeToken" />').val(token));
          // and re-submit
          //$form.get(0).submit();
          stripe_form_submit( $form.serialize() );

        }
      };

      function stripe_form_submit( post )
      {
          var url = base_url + "payment/pay";
          var datas = post;

          $.ajax({
              url : url,
              type : "post",
              data : datas,
              dataType: "json",
              success: function( data){
                  console.log(data);
                  $(".loader").fadeOut("slow");
                  if( data.success )
                  {
                    $(".payment-error").hide();

                    $(".payment-message").fadeIn( function () {
                        $(".payment-message").html(data.message);
                        setTimeout(function(){
                          if(window.location.search.indexOf('payment=success') === -1){
                            window.location.href += '?payment=success';
                          }
                          //location.reload();
                        }, 1000);
                    });

                  }
                  else
                  {

                    $(".payment-error").fadeIn( function () {
                        $(".payment-error").html(data.message);
                    });

                  }

              }
          });
      }
    });
</script>
<script type="text/javascript">
    var launch_site = function(e)
    {
      e.preventDefault();
      var url = base_url + "dashboard/get_customer_infos";
      $('.launch-site-now').text('Loading...');
      $.ajax({
          url : url,
          type : "post",
          dataType: "json",
          beforeSend: function()
          {
            $('.launch-site-now').prop('disabled', true);
          },
          success: function( data)
          {
            console.log(data);

            if( data.domain_name == null )
            {
                window.location.href = base_url + "custom_domain"
            }
            else
            {
              $('.launch-site-now').text('Launch your site now');
              //idx key
              $("#apikey").val(data.api_key);
              $("#apisecret").val(data.api_secret);
              //branding
              $("#sitename").val(data.site_name);
              $("#tagline").val(data.tag_line);
              //profile
              $("#fname").val(data.first_name);
              $("#email").val(data.email);
              $("#onumber").val(data.phone);
              //domain info
              $("#domainname").val(data.domain_name);
              $("#launch-site").modal("show");
            }
          }
      });

    };

    var launch_site_now_temp = function (e){
      e.preventDefault();
      var url = base_url + "dashboard/launch_site_now";
      $('.launch-site-now-temp').text('Loading...');

      $.ajax({
          url : url,
          type : "post",
          dataType: "json",
          beforeSend: function()
          {
            $('.launch-site-now-temp').prop('disabled', true);
          },
          success: function( data)
          {
            if(data.success)
            {
              location.reload(true);
            }
            else
            {
              location.reload(true);
            }
          }
      });

    };

    var check_agent_domain = function(e) {
      e.preventDefault();
      var url = base_url + "dashboard/check_domain_complete";

      $.ajax({
          url : url,
          type : "POST",
          dataType: "json",
          beforeSend: function() {
            console.log(url);
          },
          success: function(data) {
            if(data.success) {
              if(data.domain_complete) {
                launch_site_now_temp(e);
              }
            } else {
              if(data.empty_domain) {
                $("#message-container").css("display", "block");
                $("#launch-response-message").html(" You don't have a domain yet. Click <a href='custom_domain'><strong>here</strong></a> to go to Custom Domain page.");
              } else {
                $("#message-container").css("display", "block");
                $("#launch-response-message").html("  Your custom domain may take up to 48 hours to work as the global DNS propagates.");
              }
            }
          }
      });
    }

    $(".launch-site-now").on("click", launch_site);
    $(".launch-site-now-temp").on("click", check_agent_domain);
</script>

<?php //if($this->uri->segment(1) == "dashboard" ) { ?>
<!-- DASHBOARD -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?= base_url()?>assets/js/dashboard/morris.min.js?version=<?=AS_VERSION?>"></script>
<script>
</script>
<?php //} ?>
<?php
if($this->input->get("is_trial") || $this->config->item('trial') /*&& $this->config->item("left") >= 30)*/) { ?>
  <script type="text/javascript">
    $(window).load(function() {

        $("#launch_site_btn").on("click", function(){
          $('#modalPricing').modal('show');
        });

        var purchase_idx = function (){

          var type = $(this).data("type");
          var price = $(this).data("price");
          var sub_total = price;
          var pname = (type == "monthly") ? "Instant IDX Website": "IDX Annual" ;

          $("#product-name").text(pname);
          $("#product-amount").text(price);
          $(".product-sub-amount").text(sub_total);
          $(".product-type").val(type);
          $(".product-mount").val(sub_total);

          $('#modalPayment').modal('show');
        };

        $(".purchase-idx").on( "click", purchase_idx );
    });
  </script>
<?php
  if($this->config->item('left') >= 15) { ?>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#modalPricing').modal('show');
        $('#myModalLabel').text("Your trial period is over. Please click purchase below.");
      });
    </script>
<?php
  }
}

if($this->uri->segment(1) == "single_property_listings") { ?>
  <script type="text/javascript">
    $(window).load(function() {
      $(".create_spw_btn").on("click", function(){

        var form_id = $(this).data("form-id")
        $(".purchase-spw").attr("data-prop-form-id", form_id);
        $("#modalSpwPricing").modal("show");

      });

      var purchase_spw = function () {

        var type = $(this).data("type");
        var price = $(this).data("price");
        var property_id = $(this).data("prop-form-id");
        var sub_total = price;
        var pname = "Single Property Website";

        $("#product-name").text(pname);
        $("#product-amount").text(price);
        $(".product-sub-amount").text(sub_total);
        $(".product-type").val(type);
        $(".product-mount").val(sub_total);
        $(".property_id").val(property_id);

        $('#modalPayment').modal('show');
      };

      $(".purchase-spw").on( "click", purchase_spw );

    });

    //create spw via ajax
    $(document).ready(function() {

      $(".activateForm").on("submit", function(e) {
        e.preventDefault();

        var listingId = $(this).attr("data-id");
        var dataString = $(this).serialize();

        $.ajax({
          type:'POST',
          url: $(this).attr('action'),
          data: dataString,
          dataType: 'json',
          beforeSend: function(data) {
            console.log("creating spw...");
            $(".create-spw-loading-"+listingId+"").show();
            $(".create_spw-"+listingId+"").prop("disabled", true);
          },
          success: function(data) {
            console.log(data);
            $(".create-spw-loading-"+data.listing_id+"").show();
            $(".create_spw-"+data.listing_id+"").prop("disabled", true);
             location.reload();
          },
          error: function(data){
            $(".create_spw-"+listingId+"").prop("disabled", false);
            $(".create-spw-loading-"+listingId+"").hide();
            location.reload();
          }

        });
      });

    });
  </script>


<?php
}
?>

<?php if(isset($_GET['custom_menu'])){ ?>
      <script type="text/javascript">
       $(document).ready(function() {
          $( "#default-menu" ).removeClass( "active" );
          $( "#defaultMenu" ).removeClass( "active" );
          $( "#custom-menu" ).addClass( "active" );
          $( "#customMenu" ).addClass( "active" );
          $("#custom-page-area").show();
          $("#default-page-area").hide();
        });
      </script>
<?php } ?>

 <?php if(isset($_GET['media_modal'])){ ?>
      <script type="text/javascript">
       $(document).ready(function() {
         $('#modalAddPage').modal('show');
        });
      </script>
<?php } ?>

<script type="text/javascript">
  function downloadJSAtOnload() {
    var element = document.createElement("script");
    element.src = "<?= base_url()?>assets/js/dashboard/main.js?version=<?=AS_VERSION?>";
    document.body.appendChild(element);
  }

  if (window.addEventListener){
    window.addEventListener("load", downloadJSAtOnload, false);
  } else if(window.attachEvent){
     window.attachEvent("onload", downloadJSAtOnload);
  } else {
    window.onload = downloadJSAtOnload;
  }
</script>

<?php

  if(!isset($_GET["reset_pass"]) ){?>
   <script>
    $(document).ready(function() {
      $("#tour-video").hide();
      $(".help-icon").show();
      $('.tour-video').css( 'cursor', 'pointer' );

        var url = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');
        var pathArray = window.location.pathname.split( '/' );
        var url_segment = pathArray[2];

        // console.log(url_segment);
        // console.log(url);

        if(url == "dashboard"){

          var ga_video = localStorage.getItem('ga_video');
        
          if (ga_video == null || ga_video == '') {
              localStorage.setItem('ga_video', 1);
              //$('#tour-video').modal('show'); 
          }

        }else if(url == "agent_sites" && url_segment != "saved_searches"){

           var ch_video = localStorage.getItem('ch_video');
        
          if (ch_video == null || ch_video == '') {
              localStorage.setItem('ch_video', 1);
              $('#tour-video').modal('show');
          }

        }else if(url == "customize_homepage"){

           var chs_video = localStorage.getItem('chs_video');
        
          if (chs_video == null || chs_video == '') {
              localStorage.setItem('chs_video', 1);
              $('#tour-video').modal('show');
          }
        }else if(url == "pages"){

           var ap_video = localStorage.getItem('ap_video');
        
          if (ap_video == null || ap_video == '') {
              localStorage.setItem('ap_video', 1);
              $('#tour-video').modal('show');
          }
        }else if(url == "menu"){

           var menu_video = localStorage.getItem('menu_video');
        
          if (menu_video == null || menu_video == '') {
              localStorage.setItem('menu_video', 1);
              $('#tour-video').modal('show');
          }

        }else if(url == "crm" && url_segment == "leads"){

           var crm_video = localStorage.getItem('crm_video');
        
          if (crm_video == null || crm_video == '') {
              localStorage.setItem('crm_video', 1);
              $('#tour-video').modal('show');
          }

        }else if(url == "agent_social_media"){

           var asm_video = localStorage.getItem('asm_video');
        
          if (asm_video == null || asm_video == '') {
              localStorage.setItem('asm_video', 1);
              $('#tour-video').modal('show');
          }

        }
        
        //show video on clicking info icon
        $('.tour-video').click(function(){
          console.log("show");
          $('#tour-video').modal('show');
        });

        //stop the audio/video on modal close
        $('body').on('hidden.bs.modal', '.modal', function () {
          $('video').trigger('pause');
        });
    });
  </script>
<?php } ?>
  
  <!-- Used for deleting local storage in testing -->
  <?php if(isset($_GET['remove_ls'])){ ?>
      <script type="text/javascript">
       $(document).ready(function() {
          localStorage.clear();
        });
      </script>
 <?php }?>

 <?php if($this->uri->segment(1) == "customize_homepage") { ?>
    <script>
      $('.cancel-menu').click(function(){
        $('form').trigger("reset");
      });
    </script>
 <?php } ?>

 <script>
  $(document).ready(function() {
     $('body').on('hidden.bs.modal', '.insertMediaModal', function () {
       $('.media-details form').trigger("reset");
    });
  });
 </script>

  <script>
    var copyToClipboard = function(mediaID) {

      var input = document.getElementById(mediaID);
      var isiOSDevice = navigator.userAgent.match(/ipad|iphone/i);

      if (isiOSDevice) {
        
        var editable = input.contentEditable;
        var readOnly = input.readOnly;

        input.contentEditable = true;
        input.readOnly = false;

        var range = document.createRange();
        range.selectNodeContents(input);

        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);

        input.setSelectionRange(0, 999999);
        input.contentEditable = editable;
        input.readOnly = readOnly;

      } else {
        input.select();
      }

      var copied = document.execCommand('copy');
            if(copied){
              $(".copied-success").show();
            }else{
                $(".copied-failed").show();
            }
      $(".copied-status").delay(1000) .fadeOut(1000);
    }
  </script>
  
  <?php enqueue_scripts(); ?>
</body>
</html>
