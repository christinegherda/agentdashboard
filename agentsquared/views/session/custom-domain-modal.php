<div class="modal custom-domain" id="modalWelcomeAgent">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="close-icon">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-close"></span></button>
        </div>
        <div class="ceo-image">
          <img src="<?=base_url();?>assets/images/dashboard/ceo-image.png" alt="">
        </div>
        <div class="content">
          <p>
            Hi <span>{$firstname}</span> and welcome! I’m Troy McCasland, CEO of AgentSquared.com,
          </p>
          <p>
            This is your FREE Instant IDX™ Website Account powered by <span>{$mlsname}</span>.
          </p>
          <div class="features">
            <h3>All-in-one marketing platform to help you close more deals</h3>
            <div class="row">
              <div class="col-md-3">
                <div class="feature-item">
                  <img src="<?=base_url();?>assets/images/dashboard/feature-1.png" alt="">
                  <p>1-Click Instant Setup</p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="feature-item">
                  <img src="<?=base_url();?>assets/images/dashboard/feature-2.png" alt="">
                  <p>Powered by your MLS</p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="feature-item">
                  <img src="<?=base_url();?>assets/images/dashboard/feature-3.png" alt="">
                  <p>Lead Capture & CRM</p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="feature-item">
                  <img src="<?=base_url();?>assets/images/dashboard/feature-4.png" alt="">
                  <p>Social Media</p>
                </div>
              </div>
            </div>
          </div>
          <p>
            While you were reading this message, your Instant IDX™ Website was generated using a direct
            database connection with {$mlsname}. How cool is that!
          </p>
          <p>
            Here’s your the website address. Be sure to bookmark it. Click the link below to checkout your new
            Instant IDX™ Website.
          </p>
          <div class="domain-selected text-center">
            <a href="#">{$firstname}{$lastname}.agentsquared.com <i class="fa fa-external-link"></i></a>
          </div>
          <p>
            PS. Your new website requires no updates on your part. We import and continuously synchronize all of
            your MLS data automatically. However, we also have some really cool and awesome PRO features that
            include unlimited website editing and a shorter branded web address.
          </p>
          <div class="text-center">
            <a href="" class="btn btn-green">Learn more about Instant IDX™ Website PRO  <i class="fa  fa-chevron-right"></i></a>
          </div>
          <p>
            Thanks again for choosing AgentSquared. Please contact our customer care agents if you have any
            questions getting started.
          </p>
          <br>
          <br>
          <p>Yours Truly,</p>
          <br>
          <br>
          <p class="ceo-name">Troy MacCasland</p>
        </div>
      </div>
    </div>
  </div>
</div>