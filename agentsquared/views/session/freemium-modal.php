<div class="modal premium" id="modalPremiumOld">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-close"></span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-offset-1 col-lg-10">
            <div class="text-center">
              <p>Only <span class="bold-blue-text"><span>P</span>remium</span> users have access</p>
              <p class="mobile-mb-20px">to this feature</p>
              <br>
              <br>
              <p>Want to know what other features</p>
              <p>you've been missing?</p>
            </div>
          </div>
          <div class="col-lg-offset-3 col-lg-6 col-sm-offset-2 col-sm-9">
            <div class="text-center">
              <a href="#modalSales" data-toggle="modal" data-dismiss="modal" class="btn btn-green">Learn More</a>
              <a href="javascript:;" class="link-decline" data-dismiss="modal" aria-label="Close">No thank you</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="popup-upgrade" id="modalPremium" style="display:none;">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-close"></span></button>
  <a href="#modalSales" data-toggle="modal" data-dismiss="modal">Upgrade to Use this Feature <i class="fa fa-arrow-right"></i></a>
</div>

<div class="modal premium" id="modalWelcome">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-close"></span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="text-center">
              <p>Welcome to</p>
              <div class="aslogo">
                <img src="<?=base_url();?>assets/images/dashboard/logo.png" alt="">
              </div>
              <div class="col-lg-offset-2 col-lg-9">
                <p class="text-md">Your FREE Instant IDX™ Website compliments of <?php echo !empty($mls_name) ? $mls_name : "MLS"; ?> and powered by Flexmls is live!</p>
                <?php
                    if(isset($domain_info[0]->is_ssl) && ($domain_info[0]->is_ssl == 1)) {
                        $protocol = "https://";
                    } else {
                        $protocol = "http://";
                    }

                    if(isset($domain_info[0]->status) && isset($domain_info[0]->is_subdomain)) :
                        if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain) { ?>
                            <a href="<?php echo $protocol ?><?php echo isset($is_reserved_domain['domain_name']) ? $is_reserved_domain['domain_name'] : site_url("home"); ?>" class="btn btn-blue" target="_blank">Check it out here</a>
                <?php   } 
                        elseif($domain_info[0]->status != "completed" && !$domain_info[0]->is_subdomain) { ?>
                            <a href="<?php echo !empty($domain_info[0]->subdomain_url) ? $domain_info[0]->subdomain_url : site_url("home"); ?>" class="btn btn-blue" target="_blank">Check it out here</a>
                <?php   }
                        else { ?>
                            <a href="<?php echo site_url("home"); ?>" class="btn btn-blue" target="_blank">Check it out here</a>
                <?php   }
                    else : ?>
                        <a href="<?php echo site_url("home"); ?>" class="btn btn-blue" target="_blank">Check it out here</a>
                <?php
                    endif;
                ?>


              </div>
            </div>
            <div class="col-md-12">
              <div class="text-justify">
                <p>AgentSquared Freemium is designed for Agents just starting out.  It gives you everything you need to start building your brand, and look professional with a stunning online presence – FREE </p>  

                <p>The Premium Plan is designed for those who want to customize every aspect of your site, and capture leads, using CRM integrated with Flexmls. Go Pro to sell more homes, and get more listings!</p>
              </div>
            </div>
          </div>
          <div class="btn-action">
            <div class="col-md-6 col-sm-6">
              <a href="<?=base_url().'agent_sites';?>" class="btn btn-green">Customize your site now!</a>
            </div>
            <div class="col-md-6 col-sm-6">
              <a href="#modalSales" data-toggle="modal" data-dismiss="modal" class="btn btn-blue">Go Pro Only $82.50/mo</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- 
  <a href="#modal-2" data-toggle="modal" data-dismiss="modal">Next ></a>
 -->
<div class="modal" id="modalSales">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="text-right">
          <a href="#" data-dismiss="modal" aria-label="Close"> <i class="fa fa-close"></i></a>
        </div>
      </div>
      <div class="modal-body">
        <div class="col-md-6">
        <h2>Why Premium?</h2>
        <div class="col-md-5 col-sm-5">
          <div class="feature">
            <h3 class="text-center">
              Free
            </h3>
            <ul>
              <li>Instant IDX™</li>
              <li>Live Property Listings</li>
              <li>Search MLS Database</li>
              <li>Free Web Hosting</li>
            </ul>
          </div>
        </div>
        <div class="col-md-7 col-sm-7">
          <div class="feature">
            <h3 class="text-center">
              Premium Features
            </h3>
            <ul>
              <li>Custom Domain</li>
              <li>Add Unlimited Pages</li>
              <li>Customize Every Page</li>
              <li>Add FlexMLS Saved Search to any page</li>
              <li>Change Menu Navigation</li>
              <li>Lead Capture</li>
              <li>CRM</li>
              <li>Listing Alerts</li>
              <li>Google Analytics</li>
              <li>Integrates with Flexmls Smart Phone Apps</li>
              <li>Single Property Websites</li>
              <li>1 Click Social Media Postings</li>
              <li>Free Phone Support</li>
            </ul>
            <a href="#modalCheckout" data-toggle="modal" data-dismiss="modal" class="btn btn-green">Buy Now!</a>
          </div>
        </div> 
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal modalCheckoutForm" id="modalCheckout">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-label" id="myModalLabel">Checkout</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="checkout-top">
            <div class="col-lg-12">
              <div class="text-center">
                <p class="font-m">Go Pro With the Premium Instant IDX Website</p>
                <p>Personalize your site, capture leads, sell more homes, win more listings, look professional! </p>
              </div>
              <div class="price-option">
                <div class="price-list">
                  <label>
                    <input checked="checked" type="radio" class="option-input radio" name="subscribe-plan" value="990" data-plan="yearly" />
                  </label>
                  <p><b>Yearly Subscription:</b> <span class="text-blue">$82.50</span> <span class="font-dark">x 12</span> months = <span class="text-blue">$990</span> </p>
                  <div class="coupon">
                    <img src="<?=base_url();?>assets/images/dashboard/checkout-coupon.png" alt="">
                  </div>
                </div>
              </div>
              <div class="price-option">
                <div class="price-list">
                  <label>
                    <input type="radio" class="option-input radio" name="subscribe-plan" value="99" data-plan="monthly" />
                  </label>
                  <p><b>Monthly Subscription:</b> <span class="text-blue">$99</span> per month </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="text-center">
              <p class="money-back">$500 Setup Fee may apply</p>
            </div>
          </div>
          <div class="col-md-12">
            <div class="border-gray"></div>
          </div>
          <div class="checkout-bottom">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="row">
                <div class="col-md-6">
                  <p class="modal-text">Payment Method:</p>
                </div>
                <div class="col-md-6">
                  <img src="<?= base_url()?>assets/images/dashboard/payment_method.png" class="cc-accepted">
                </div>
              </div>
              <div class="cc-detail">
                <h3>Credit Card Details:</h3> </br>
                <form class="form-horizontal freemium_stripe_sbt" id="freemium_stripe_sbt" action="" method="POST">
                  <div class="col-md-12"><h3><div style="white-space: normal;" class="payment-error label label-danger"></div></h3><br></div>
                  <div class="col-md-12"><h3><div class="col-md-12 payment-message label label-success"></div></h3><br></div>

                  <input type="hidden" name="product_amount" value="990" class="product-mount" >
                  <input type="hidden" name="product_type" value="yearly" class="product-type" >
                  <input type="hidden" name="property_id" value="" class="property_id" >

                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="cardnumber" class="col-md-4">Card number:</label>
                      <div class="col-md-8 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="number" placeholder="Card Number" required >
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="cardnumber" class="col-md-4">CVV code:</label>
                      <div class="col-md-8 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="cvc" placeholder="CVV" required >
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="expirationdate" class="col-md-4 col-sm-12 col-xs-12">Expiration date:</label>
                      <div class="col-md-2 col-sm-6 col-xs-6 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="exp-month" placeholder="MM" required >
                      </div>
                      <div class="col-md-2 col-sm-6 col-xs-6 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="exp-year" placeholder="YYYY" required >
                      </div>
                    </div>
                  </div>

              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
              <h1 class="payment-total">$<span class="product-sub-amount">990</span></h1>
              <p id="payment-note" class="payment-total-note1">Total is inclusive of setup fee.</p>
              <p class="text-left payment-total-note2">
                By clicking on the 'Place Order' button at the end of the order process, you are consenting to be bound by our terms and conditions contained in these <a id="terms-link-1" href="https://www.agentsquared.com/annual-terms-and-conditions/" target="_blank">Terms and Conditions</a> and appearing anywhere on AgentSquared website.
              </p>
              <button type="submit" class="btn btn-border-green" id="freemium_pay_now_btn"> Place Order</button>
            </div>
            </form>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <ul class="list-link">
                  <li><a href="#" target="_blank">Disclaimer</a></li>
                  <li><a href="<?=base_url()?>dashboard/privacy_policy" target="_blank">Privacy Policy</a></li>
                  <li><a id="terms-link-2" href="https://www.agentsquared.com/annual-terms-and-conditions/" target="_blank">Terms &amp; Conditions</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                <p class="powered-by">Powered By: <img src="<?= base_url()?>assets/images/dashboard/stripe-logo.png"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-trial modalCheckoutForm modalTrialCheckout" id="modalTrialForm">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="premium-features">
          <!-- <button class="close" data-dismiss="modal"><i class="fa fa-close"></i></button> -->
          <div class="top">
            <div class="row">
              <div class="col-md-9">
                <h3>Get <b>15</b> Days <b>FREE</b> Trial with <br>
                  Premium Features <br>
                  <b>IDX</b> Website</h3>
                  <div class="premium-list">
                    <ul class="list">
                      <li>Custom Domain*</li>
                      <li>Add Unlimited Pages</li>
                      <li>Customize Every Page</li>
                      <li>Add FlexMLS Saved Search <br>to any page</li>
                      <li>Change Menu Navigation</li>
                      <li>Lead Capture</li>
                      <li>CRM</li>
                    </ul>
                    <ul class="list">
                      <li>Listing Alerts</li>
                      <li>Google Analytics</li>
                      <li>Integrates with FlexMLS Smart</li>
                      <li>Phone apps</li>
                      <li>Single Property Websites</li>
                      <li>1 Click Social Media Postings</li>
                      <li>Free Phone Support</li>
                    </ul>
                  </div>
              </div>
            </div>
          </div>
          <div class="bottom">
            <div class="testimonial">
              <img src="/assets/images/testimonial.png" alt="">
              <div class="row">
                <div class="col-md-12 text-center">
                  <a class="btn btn-primary" href="#modalCCForm" data-toggle="modal" data-dismiss="modal">Start with 15 Day Free Trial</a>
                </div>
                <div class="col-md-12 text-center">
                  <p><i>* Custom Domain is only available for paid users.</i></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-trial modalCCForm" id="modalCCForm">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="premium-plans">
          <div class="top">
            <div class="row">
              <div class="col-md-5">
                <h3>Choose your plan</h3>
                <ul class="list">
                  <li>You won’t be charged until after
                  your 15 days free trial.</li>

                  <li>We’ll remind you three days before
                  your trial ends.</li>

                  <li>No commitments, cancel anytime.</li>
                </ul>
              </div>
              <div class="col-md-7">
                <div class="radio-container">
                  <div class="radio-group">
                    <input type="radio" id="yearly" name="subscribe-plan" id="plan-yearly" value="990" data-plan="yearly" checked>
                    <label for="yearly">
                      <b>Yearly <br> Subscription</b>
                      <p>$82.50 x 12 months</p>
                      <p class="price"><sup>$</sup>990</p>
                      <p class="discount">save 20% ($200)</p>
                    </label>
                  </div>
                  <div class="radio-group">
                    <input type="radio" id="monthly"  name="subscribe-plan" id="plan-monthly" value="99" data-plan="monthly">
                    <label for="monthly">
                      <b>Monthly <br> Subscription</b>
                      <p class="price"><sup>$</sup>99</p>
                      <p class="discount">per month</p>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="bottom">
            <div class="row">
              <form action="<?=base_url().'idx_login/create_stripe_subscription'?>" method="post" id="payment-form">
                <div class="col-md-7">
                  <div class="card">
                    <h3>Credit Card Details</h3>
                    <img src="/assets/images/cc-accepted.png" alt="" class="cc-accepted">
                      <div class="stripe-form">
                        <div class="col-md-12">
                          <input type="hidden" name="product_amount" value="990" class="product-mount" >
                          <input type="hidden" name="product_type" value="yearly" class="product-type" >
                          <div id="card-element"></div><!-- A Stripe Element will be inserted here. -->
                          <div id="card-errors" role="alert"></div><!-- Used to display form errors. -->
                        </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-5">
                  <p class="terms-link">
                    By clicking on the ‘Get Started’ button
                    at the end of the order process, you are
                    consenting to be bound by our <a href="https://www.agentsquared.com/annual-terms-and-conditions/">terms
                    and conditions</a> and appearing
                    anywhere on AgentSquared website.
                  </p>
                  <button class="btn btn-primary">Get Started</button>
                </div>
              </form>
              <div class="col-md-8">
                <ul class="link-list">
                  <li><a href="#">Disclaimer</a></li>
                  <li><a href="<?=base_url()?>dashboard/privacy_policy">Privacy Policy</a></li>
                  <li><a href="https://www.agentsquared.com/annual-terms-and-conditions/">Terms & Conditions</a></li>
                </ul>
              </div>
              <div class="col-md-4 text-right">
                <img src="/assets/images/powered-by-stripe.png" alt="" class="mt-20">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php if ($this->input->get('trial_checkout')==TRUE): ?>
  <script src="https://js.stripe.com/v3/"></script>

  <script>
    // Create a Stripe client.
    var stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
      base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
      event.preventDefault();

      stripe.createToken(card).then(function(result) {
        if (result.error) {
          // Inform the user if there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
        } else {
          // Send the token to your server.
          stripeTokenHandler(result.token);
        }
      });
    });

    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
      // Insert the token ID into the form so it gets submitted to the server
      var form = document.getElementById('payment-form');
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'stripeToken');
      hiddenInput.setAttribute('value', token.id);
      form.appendChild(hiddenInput);

      // Submit the form
      form.submit();
    }
  </script>
<?php endif ?>