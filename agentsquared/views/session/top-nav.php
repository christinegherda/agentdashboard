<body class="hold-transition skin-blue fixed layout-top-nav">
<div class="wrapper">
<header class="main-header">
    <nav class="navbar navbar-static-top">
    <?php 
        if($this->config->item('agent_idx_website')) { ?>
        <div class="navbar-header">
            <?php $agent_data = $this->session->userdata("agentdata"); ?>
            <a href="<?php echo site_url("dashboard"); ?>" class="navbar-brand">
            <?php if(isset($branding->logo) AND !empty($branding->logo)) {

                    $logo = getenv('AWS_S3_ASSETS') . "uploads/logo/".$branding->logo;

                } else{

                    $logo = AGENT_DASHBOARD_URL."/assets/img/home/agentsquared-logo.png";

                }
               // $logo = 'https://d2svk5q5uf24n2.cloudfront.net/uploads/logo/logo4.jpg'; ?>

             <img src="<?php echo $logo ?>" class="dashboard-logo" alt="Agent Logo"/> 

            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?php echo ($this->uri->segment(1) == "dashboard") ? "active" : "" ?>">
                    <a href="<?php echo site_url("dashboard"); ?>"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a>
                </li> 
                <li class="dropdown <?php if($this->uri->segment(1) == 'customize_homepage' || $this->uri->segment(1) == 'featured_listings' || $this->uri->segment(1) == 'choose_theme' || $this->uri->segment(1) == 'agent_sites' || $this->uri->segment(2) == 'saved_searches' || $this->uri->segment(1) == 'custom_domain' || $this->uri->segment(1) == 'pages' || $this->uri->segment(1) == 'menu' || $this->uri->segment(1) == 'dashboard_blogs' || $this->uri->segment(1) == 'home') echo 'active';?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-secret"></i> <span>Agent Site</span> <i class="caret"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="<?php echo ($this->uri->segment(1) == "agent_sites") ? "active" : "" ?>"><a href="<?php echo site_url("agent_sites"); ?>"><span class="icon"><i class="fa fa-info-circle"></i></span> Customize Homepage</a></li>
                        <li class="<?php echo ($this->uri->segment(1) == "custom_domain") ? "active" : "" ?>"><a href="<?php echo site_url("custom_domain/domain"); ?>"><span class="icon"><i class="fa fa-globe"></i></span> Custom Domain Name</a></li>
                        <li class="<?php echo ($this->uri->segment(1) == "pages") ? "active" : "" ?>"><a href="<?php echo site_url("pages"); ?>"><span class="icon"><i class="fa fa-book"></i></span> Page  </a></li>
                        <li class="<?php echo ($this->uri->segment(1) == "menu") ? "active" : "" ?>"><a href="<?php echo site_url("menu"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Menu</a></li>
                        <li class="<?php echo ($this->uri->segment(1) == "customize_homepage") ? "active" : "" ?>"><a href="<?php echo site_url("customize_homepage"); ?>"><span class="icon"><i class="fa fa-cog"></i></span> Customize Homepage Layout</a></li>
                        <li class="<?php echo ($this->uri->segment(1) == "reviews") ? "active" : "" ?>"><a href="<?php echo site_url("reviews"); ?>"><span class="icon"><i class="fa fa-star"></i></span> Reviews</a></li>
                        <li class="<?php echo ($this->uri->segment(2) == "saved_searches" || $this->uri->segment(2) == "saved_search") ? "active" : "" ?>"><a href="<?php echo site_url("agent_sites/saved_searches"); ?>"><span class="icon"><i class="fa fa-save"></i></span> Saved Searches</a></li>
                        <li class="<?php echo ($this->uri->segment(1) == "media") ? "active" : "" ?>"><a href="<?php echo site_url("media"); ?>"><span class="icon"><i class="fa fa-file-image-o"></i></span> Media</a></li>
                        <li>
                        <?php
                            if(isset($domain_info[0]->is_ssl) && ($domain_info[0]->is_ssl == 1)) {
                                $protocol = "https://";
                            } else {
                                $protocol = "http://";
                            }

                            if(isset($domain_info[0]->status) && isset($domain_info[0]->is_subdomain)) :
                                if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain) { ?>
                                    <a href="<?php echo $protocol ?><?php echo isset($is_reserved_domain['domain_name']) ? $is_reserved_domain['domain_name'] : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php   } 
                                elseif($domain_info[0]->status != "completed" && !$domain_info[0]->is_subdomain) { ?>
                                    <a href="<?php echo !empty($domain_info[0]->subdomain_url) ? 'http://' . strip_protocol($domain_info[0]->subdomain_url) : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php   }
                                else { ?>
                                    <a href="<?php echo site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php   }
                            else : ?>
                                <a href="<?php echo site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php
                            endif;
                        ?>
                        </li>
                        <!-- <li class="<?php echo ($this->uri->segment(1) == "choose_theme") ? "active" : "" ?>"><a href="<?php echo site_url("choose_theme"); ?>"><span class="icon"><i class="fa fa-wpforms"></i></span> Choose Theme</a></li> -->
                        <li class="<?php echo ($this->uri->segment(1) == "settings") ? "active" : "" ?>"><a href="<?php echo site_url("settings");?>"><span><i class="fa fa-cog"></i></span> Site Settings</a></li>
                    </ul>
                </li>
                <li class="dropdown <?php if($this->uri->segment(2) == 'reports' || $this->uri->segment(2) == 'buyer_leads' || $this->uri->segment(2) == 'seller_leads' || $this->uri->segment(2) == 'messages' || $this->uri->segment(2) == 'customer_leads' || $this->uri->segment(2) == 'schedules') echo 'active';?> ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-line-chart"></i> <span>CRM</span> <i class="caret"></i></a>
                    <ul class="dropdown-menu" role="menu" id="crm" class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                        <li class="<?php echo ($this->uri->segment(2) == "leads") ? "active" : "" ?>"><a href="<?php echo site_url("crm/leads"); ?>"><span class="icon"><i class="fa fa-file-text-o"></i></span> Leads</a></li>
                        <li class="<?php echo ($this->uri->segment(2) == "schedules") ? "active" : "" ?>"><a href="<?php echo site_url("crm/spw_schedules"); ?>"><span class="icon"><i class="fa fa-folder"></i></span> Scheduled showings</a></li>
                        <li class="<?php echo ($this->uri->segment(2) == "liondesk") ? "active" : "" ?>"><a href="<?php echo site_url("liondesk"); ?>"><span class="icon"><i class="fa fa-folder"></i></span> Liondesk</a></li>
                        <?php /* ?>
                        <li class="<?php echo ($this->uri->segment(1) == "listtrac") ? "active" : "" ?>">
                             <a href="<?php echo site_url("listtrac"); ?>"><span class="icon"><i class="fa fa-users"></i></span> Listtrac</a>
                        </li>
                        <?php */?>
                    </ul>
                </li>
                <li class="<?php echo ($this->uri->segment(1) == "single_property_listings" || $this->uri->segment(2) == "spark_member") ? "active" : "" ?>">
                    <a href="<?php echo site_url("single_property_listings"); ?>"> <i class="fa fa-building"></i>  <span>Single Property Listings</span></a>
                </li>
                <li class="dropdown <?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'active';?> ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i> <span>Marketing </span> <i class="caret"></i></a>
                    <ul class="dropdown-menu" role="menu" id="agent-site-social" class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                        <li class="<?php echo ($this->uri->segment(2) == "activate") ? "active" : "" ?>"><a href="<?php echo site_url("agent_social_media/activate"); ?>"><span class="icon"><i class="fa fa-cog"></i></span> Activate Social Media</a></li>
                        <li class="<?php echo ($this->uri->segment(2) == "links") ? "active" : "" ?>"><a href="<?php echo site_url("agent_social_media/links"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Social Media Links</a></li>
                        <!-- <li class="<?php echo ($this->uri->segment(1) == "gsc") ? "active" : "" ?>"><a href="<?php echo site_url("gsc"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Google Search Console</a></li> -->
                    </ul>
                </li>
                <li><a href="http://www.agentsquared.com/support/" target="_blank"><i class="fa fa-gear"></i> <span>Support</span></a></li>
                <li class="viewsite">
                <?php 
                    if(isset($domain_info[0]->status) && isset($domain_info[0]->is_subdomain)) :
                        if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain) { ?>
                            <a class="btn-view btn-primary" href="<?php echo $protocol ?><?php echo isset($is_reserved_domain['domain_name']) ? $is_reserved_domain['domain_name'] : site_url("home"); ?>" target="_blank"> View Site</a>
                <?php   } 
                        elseif($domain_info[0]->status != "completed" && !$domain_info[0]->is_subdomain) { ?>
                            <a class="btn-view btn-primary" href="<?php echo !empty($domain_info[0]->subdomain_url) ? 'http://' . strip_protocol($domain_info[0]->subdomain_url) : site_url("home"); ?>" target="_blank"> View Site</a>
                <?php   }
                        else { ?>
                            <a class="btn-view btn-primary" href="<?php echo site_url("home"); ?>" target="_blank"> View Site</a>
                <?php   }
                    else : ?>
                        <a class="btn-view btn-primary" href="<?php echo site_url("home"); ?>" target="_blank"> View Site</a>
                <?php
                    endif;
                ?>
                </li>
                <?php if ($this->config->item('disallowUser')/* || $this->config->item('trial')*/): ?>
                    <li class="premium-btn">
                        <a href="#modalSales" class="btn btn-green" href="#modalSales" data-toggle="modal" data-dismiss="modal"><i class="fa fa-trophy"></i> Upgrade To Pro </a>
                    </li>
                <?php endif ?>

            </ul>
        </div>
        <?php
        } else { ?>
        <!-- SPW tabs -->
            <div class="navbar-header">
            <?php $agent_data = $this->session->userdata("agentdata"); ?>
                <a href="<?php echo site_url("dashboard"); ?>" class="navbar-brand">
                <?php if(isset($branding->logo) AND !empty($branding->logo)) {

                        $logo = getenv('AWS_S3_ASSETS') . "uploads/logo/".$branding->logo;
                       
                    } else{

                        $logo = AGENT_DASHBOARD_URL."/assets/img/home/agentsquared-logo.png";
                    }?>

                     <img src="<?php echo $logo ?>" class="dashboard-logo" alt="Agent Logo"/> 

                </a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></button>
            </div>
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-secret"></i> <span>Agent Site</span> <i class="caret"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-info-circle"></i></span> Customize Homepage</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-globe"></i></span> Custom Domain Name</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-book"></i></span> Page</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-cog"></i></span> Customize Homepage Search</a>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-star"></i></span> Reviews</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-save"></i></span> Saved Searches</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-link"></i></span> Menu</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-file-image-o"></i></span> Media</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-line-chart"></i> <span>CRM</span> <i class="caret"></i></a>
                        <ul class="dropdown-menu" role="menu" id="crm" class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-file-text-o"></i></span> Leads</a></li>
                            <li class="<?php echo ($this->uri->segment(2) == "schedules") ? "active" : "" ?>"><a href="<?php echo site_url("crm/spw_schedules"); ?>"><span class="icon"><i class="fa fa-folder"></i></span> Scheduled showings</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo ($this->uri->segment(1) == "single_property_listings" || $this->uri->segment(2) == "spark_member") ? "active" : "" ?>">
                      <a href="<?php echo site_url("single_property_listings"); ?>"><i class="fa fa-building"></i> <span>Single Property Listings</span></a>
                    </li>
                    <li class="dropdown <?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'active';?> ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i> <span>Marketing </span> <i class="caret"></i></a>
                        <ul class="dropdown-menu" role="menu" id="agent-site-social" class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                            <li class="<?php echo ($this->uri->segment(2) == "activate") ? "active" : "" ?>"><a href="<?php echo site_url("agent_social_media/activate"); ?>"><span class="icon"><i class="fa fa-cog"></i></span> Activate Social Media</a></li>
                            <li class="<?php echo ($this->uri->segment(2) == "links") ? "active" : "" ?>"><a href="<?php echo site_url("agent_social_media/links"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Social Media Links</a></li>
                        </ul>
                    </li>
                    <li><a href="http://www.agentsquared.com/support/" target="_blank"><i class="fa fa-gear"></i> <span>Support</span></a></li>
                    <li class="viewsite"><a class="btn-view btn-primary disabled-agent-btn" href="javascript:;"> View Site</a></li>
                    <?php if ($this->config->item('disallowUser')/* || $this->config->item('trial')*/): ?>
                        <li class="premium-btn">
                            <a href="#modalSales" class="btn btn-green" href="#modalSales" data-toggle="modal" data-dismiss="modal"><i class="fa fa-trophy"></i> Upgrade To Pro </a>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
        <?php
            }
        ?>
        <!-- End of SPW tabs -->

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <?php if(!$this->config->item('agent_idx_website')) : ?>
            <?php endif ?>
                <!-- User Account Menu -->
                <li class="user user-menu user-setting-dropdown">
                    <!-- Menu Toggle Button -->
                    <a id="toggle-user-setting-menu"
                       href="<?php  echo site_url(); ?>"
                       data-target=".user-setting-dropdown"
                       class="dropdown-toggle"
                       data-toggle="dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       role="button">
                        <!-- The user image in the navbar-->
                        <?php if( isset($branding->agent_photo) && !empty($branding->agent_photo))
                        {
                            $agent_photo = getenv('AWS_S3_ASSETS') . "uploads/photo/".$branding->agent_photo;

                        } elseif (isset($account_info->Images[0]->Uri) && !empty($account_info->Images[0]->Uri))
                        {
                            $agent_photo = $account_info->Images[0]->Uri;
                        } else
                        {
                            $agent_photo = AGENT_DASHBOARD_URL."/assets/images/no-profile-img.gif";
                        } ?>

                        <img  src="<?php  echo $agent_photo ?>" class="user-image" alt="Agent Photo" >
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <strong><?php echo $branding->first_name ? $branding->first_name: "Admin";?></strong> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu agent-left-box" aria-labelledby="toggle-user-setting-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <div class="agent-img">
                                <?php if( isset($branding->agent_photo) && !empty($branding->agent_photo)) {

                                        $agent_photo = getenv('AWS_S3_ASSETS') . "uploads/photo/".$branding->agent_photo;
                                    } elseif(isset($account_info->Images[0]->Uri) && !empty($account_info->Images[0]->Uri)){
                                        
                                        $agent_photo = $account_info->Images[0]->Uri;
                                    } else{
                                        $agent_photo = AGENT_DASHBOARD_URL."/assets/images/no-profile-img.gif";
                                    } ?>

                                    <img  src="<?php  echo $agent_photo ?>" class="img-circle" alt="Agent Photo" > 

                                <p style="margin-top:5px; color:#fff;"> <?php echo ($branding->first_name) ? $branding->first_name: "Admin";?></p>
                                <div class="change-profile-container">
                                    <form action="<?php echo base_url()?>agent_sites/upload_agent_photo" class="form-horizontal content-form agent-upload-form" method="POST" enctype="multipart/form-data" id="updateAgentPhoto">
                                        <div class="form-group">
                                            <div class="upload-agent-photo">
                                                <input type="file" name="userfile" class="upload-btn">
                                            </div>
                                            <?php
                                                if($this->config->item('disallowUser')) { ?>
                                                    <a href="?modal_premium=true" style="display:none" type="button" class="btn btn-default btn-submit show-btn">
                                                        Upload
                                                    </a>
                                            <?php
                                                } else { ?>
                                                    <button style="display:none" type="submit" class="btn btn-default btn-submit btn-agent-photo show-btn">Upload</button>
                                            <?php 
                                                }
                                            ?>
                                        </div>
                                    </form>
                                </div>

                                <div class="overlay-photo"></div>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <!-- <a href="<?php  echo site_url('login/auth/change_password'); ?>" class="btn btn-default btn-transparent btn-flat">Change Password</a> -->
                                <div class="setting-dropdown">
                                    <button class="btn btn-default btn-transparent btn-flatdropdown-toggle"> <span class="fa fa-gear"></span> Settings</button>
                                    <ul class="isdropdown-setting" style="width: 156px;">
                                        <li><a href="#cancelPro" data-toggle="modal" data-target="#cancelPro" class="cancelpro-button">Cancel Account</a></li>
                                        <li><a href="<?php echo site_url('login/auth/change_password'); ?>">Change Password</a></li>
                                        <li><a href="<?php echo site_url('/dashboard/?updatecc=1'); ?>" id="show-update-cc-modal">Update Credit Card</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pull-right">
                                <a href="<?php  echo site_url('login/auth/logout'); ?>" class="btn btn-default btn-transparent btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-custom-menu -->
    </nav>
  </header>
