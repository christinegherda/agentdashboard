<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <link rel="icon" type="image/x-icon"  href="<?php echo isset($branding->favicon) ? base_url().'assets/upload/favicon/'.$branding->favicon :  base_url().'assets/images/default-favicon.png';?>"/>
    <link rel="apple-touch-icon" href="<?= base_url()?>assets/img/home/apple-touch-icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/fonts/fontello/css/fontello.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard.css">
    <?php if(isset($css)) :
        foreach($css as $key => $val ) :  ?>
        <link rel="stylesheet" href="<?= base_url()?>assets/css/<?php echo $val?>" type="text/css"> </link>
        <?php endforeach;?>
    <?php endif;?>
    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
    <style type="text/css">
        #instruction {
            margin-top: 220px;
        }
        #instruction button {
            padding: 20px;
        }
    </style>
</head>
<body>
    <div id="wrapper" >
        <div class="container-fluid" >
            <div class="row" >
                <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-0">
                    <h1 class="page-header text-center">Setting Up A-record and DNS for Digital Ocean</h1><hr style="border: solid 1px #dadada;">
                    <div class="row" id="instruction">
                        <div class="col-md-6">
                            <button class="btn btn-success btn-lg pull-right" role="button" data-toggle="modal" data-target="#with_email"><span class="icon"><i class="fa fa-sitemap"></i></span> Existing domain <strong>with</strong></strong> email address</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-warning btn-lg" role="button" data-toggle="modal" data-target="#without_email"><span class="icon"><i class="fa fa-sitemap"></i></span> Existing domain <strong>without</strong> email address</button>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div id="with_email" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title text-center"></h2>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <h4>If you have an existing domain with an email address and would want to use that domain as the URL for your <a href="https://www.agentsquared.com/" target="_blank">agentsquared.com</a> website, You need to follow the following steps and only point the A record IP address of your existing custom domain.<h4><br>

                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-success"> 1. Go and login to your Registrar’s Account (GoDaddy, Enom, Tucows,CrazyDomains, etc)</li>
                                            <li class="list-group-item list-group-item-info"> 2. Look and click on <strong>My account</strong>.</li>
                                            <li class="list-group-item list-group-item-success"> 3. Click on <strong>Domains</strong> section.</li>
                                            <li class="list-group-item list-group-item-info"> 4. Search for your the custom domain that you want to point to our servers.</li>
                                            <li class="list-group-item list-group-item-success"> 5. Click on the desired domain to change.</li>
                                            <li class="list-group-item list-group-item-info"> 6. Click on <strong>Manage DNS</strong>.</li>
                                            <li class="list-group-item list-group-item-success"> 7. Click on the Pencil button to edit the A record at the top.</li>
                                            <li class="list-group-item list-group-item-info"> 8. Change it to this IP address <strong>138.197.236.202</strong></li>
                                            <li class="list-group-item list-group-item-success"> 9. Click on <strong>Update</strong> or <strong>Save</strong>.</li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="without_email" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title text-center"></h2>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <h4>If you have an existing domain without an email address and would want to use that domain as the URL for your agentsquared.com website
                                            You need to follow the following steps below to point the DNS (Domain Name Servers) of your existing custom domain to <a href="https://www.agentsquared.com/" target="_blank">agentsquared.com</a>
                                        <h4>
                                        <br>
                                        <h4><strong>Changing the DNS (Domain Name Servers):</strong> </h4>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-success"> 1. Go and login to your Registrar’s Account (GoDaddy, Enom, Tucows,CrazyDomains, etc)</li>
                                            <li class="list-group-item list-group-item-info"> 2. Look and click on <strong>My Account</strong>.</li>
                                            <li class="list-group-item list-group-item-success"> 3. Click on <strong>Domains</strong> section.</li>
                                            <li class="list-group-item list-group-item-info"> 4. Search for your the custom domain that you want to point to our servers.</li>
                                            <li class="list-group-item list-group-item-success"> 5. Select the domain name that you want to use with your Droplet</li>
                                            <li class="list-group-item list-group-item-info"> 6. Under <strong>Nameservers</strong>, click <strong>Manage</strong>.</li>
                                            <li class="list-group-item list-group-item-success"> 7. Under <strong>Setup type,</strong>, select <strong>Custom</strong>.</li>
                                            <li class="list-group-item list-group-item-info"> 8. Select <strong>Add Nameserver</strong>.</li>
                                            <li class="list-group-item list-group-item-success"> 9. Enter the following nameservers:
                                                <ul>
                                                    <li>ns1.digitalocean.com</li>
                                                    <li>ns2.digitalocean.com</li>
                                                    <li>ns3.digitalocean.com</li>
                                                </ul>
                                            </li>
                                            <li class="list-group-item list-group-item-info"> 9. Click on <strong>Update</strong> or <strong>Save</strong>.</li>
                                        </ul>

                                        <h4>
                                            Proceed to change the DNS records.
                                            Use and click on this link <a href="https://www.digitalocean.com/community/tutorials/how-to-point-to-digitalocean-nameservers-from-common-domain-registrars#registrar-godaddy" target="_blank">https://www.digitalocean.com/community/tutorials/how-to-point-to-digitalocean-nameservers-from-common-domain-registrars#registrar-godaddy</a> to point your DNS records to us.
                                            It contains a walk through with pictures on how you can point your DNS records to our servers here at <a href="https://www.agentsquared.com/" target="_blank">AgentSquared.com</a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--
                    <div style="padding: 28px 55px;">
                        <h4>For existing domains, all you have to do is to edit your website’s DNS Record and point it to our IP. After you have done that, you are all set. Note that this will take up to 48 hours to propagate.</h4>
                        <br/>
                        <h4>How do I create an A record to point my domain to an IP address?</h4>
                        <br/>
                        <h4>To create an A-Record, please follow these steps:</h4>
                        <ul>
                            <li>Sign in to your Hosting Account</li>
                            <li>Select your Domain that you want to move over to <strong>AgentSquared Servers</strong>.</li>
                            <!-- <li>Edit the DNS Settings of the selected Domain and Change the A-Record to <strong> 138.197.236.202 <!-- 104.236.110.186 --> <!--</strong>.</li>
                            <li>Edit the DNS Settings of the selected Domain</li>
                              <li>Enter the following nameservers:</li>
                              <ul>
                                  <strong><li>ns1.digitalocean.com</li></strong>
                                  <strong><li>ns2.digitalocean.com</li></strong>
                                  <strong><li>ns3.digitalocean.com</li></strong>
                              </ul>
                            <li>Then hit <strong>Save</strong>.</li>
                        </ul>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
    <script src="<?= base_url()?>assets/js/dashboard/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        

    </script>
    <!-- <script src="<?= base_url()?>assets/js/dashboard/main.js"></script> -->
</body>
</html>
