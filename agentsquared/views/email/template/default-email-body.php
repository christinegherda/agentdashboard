<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');?>
<table cellpadding="0" cellspacing="0" border="0" width="640" style="border:0; border-collapse:collapse; background-color:#ffffff;">
	<tbody>
		<tr><td colspan="3" cellpadding="0" cellspacing="0" border="0" width="100%" height="20"></td></tr>
		<tr>
			<td cellpadding="0" cellspacing="0" border="0" width="60" height="100%"></td>
			<td style="border-collapse:collapse; vertical-align:middle;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border:0; border-collapse:collapse; background-color:#ffffff;">
					<tbody>
						<tr>
							<td style="border-collapse:collapse; vertical-align:middle;font-family:helvetica, Arial, sans-serif; font-size: 14px; text-align: left; color:#87919F; line-height: 24px;">
								<?php echo isset($message) ? $message : '';?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td cellpadding="0" cellspacing="0" border="0" width="60" height="100%"></td>
			<tr><td colspan="3" cellpadding="0" cellspacing="0" border="0" width="100%" height="20"></td></tr>
		</tr>
	</tbody>
</table>
