<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');?>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title><?php echo isset($subject) ? $subject : '';?></title>
	</head>
	<body style="margin:0; padding:0; width:100%; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; background-color: #F4F5F7;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
			<tbody>
				<tr>
					<td width="100%" height="30"></td>
				</tr>
			</tbody>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border:0; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; background-color: #F4F5F7;">
			<tbody>
				<tr>
					<td align="center" style="border-collapse: collapse;">
						<table width="640" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
							<tbody>
								<tr>
									<td width="100%" height="2" bgcolor="#0099CC"></td>
								</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="640" style="border:0; border-collapse:collapse; background-color:#ffffff;">
							<tbody>
								<tr>
									<td width="50%" style="border-collapse:collapse; vertical-align:middle; text-align center; padding:5px;">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tbody>
												<tr>
													<td width="100%" style="font-family: helvetica, Arial, sans-serif; font-size: 18px; letter-spacing: 0px; text-align: left;">
														<a href="#" style="text-decoration: none;">
															<img src="<?php echo isset($logo_url) ? $logo_url : AGENT_DASHBOARD_URL . 'assets/images/email-logo.png'; ?>" alt="Agent-Squared" border="0" width="auto" height="40px" style="with: 100%; height: 40px; border: 4px solid #ffffff;">
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td width="50%" style="border-collapse:collapse; vertical-align:middle; text-align center; padding:5px;">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tbody>
												<tr>
													<td width="80%" style="font-family: helvetica, Arial, sans-serif; font-size: 18px; letter-spacing: 0px; text-align: right;color: #87919F;">
														<?php echo ucwords(isset($recipient_name) ? $recipient_name : '');?>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
							<tbody>
								<tr>
									<td width="100%" height="30"></td>
								</tr>
							</tbody>
						</table>
						<?php echo isset($content) ? $content : '';?>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
							<tbody>
								<tr>
									<td width="100%" height="30"></td>
								</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="640" style="border:0; border-collapse:collapse; background-color:#ffffff;">
							<tbody>
								<tr>
									<td width="50%" valign="bottom" style="border-collapse:collapse; vertical-align:middle; border-right:#eeeeee 1px dashed; text-align center;">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" height="auto" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tbody>
												<tr><td colspan="3" cellpadding="0" cellspacing="0" border="0" width="100%" height="20"></td></tr>
												<tr>
													<td cellpadding="0" cellspacing="0" border="0" width="50" height="100%"></td>
													<td width="100%" style="border-collapse:collapse; vertical-align:middle;">
														<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
															<tbody>
																<tr>
																	
																</tr>
															</tbody>
														</table>
													</td>
													<td cellpadding="0" cellspacing="0" border="0" width="50" height="100%"></td>
												</tr>
												<tr><td colspan="3" cellpadding="0" cellspacing="0" border="0" width="100%" height="20"></td></tr>
											</tbody>
										</table>
									</td>
									<td width="50%" style="border-collapse:collapse; vertical-align:middle; text-align center;">
										<!-- copyright-->
										<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tbody>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<table width="640" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
							<tbody>
								<tr>
									<td width="100%" height="2" bgcolor="#64645E"></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
			<tbody>
				<tr>
					<td width="100%" height="30"></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
