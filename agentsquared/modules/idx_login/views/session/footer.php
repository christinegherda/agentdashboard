    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript">
	    $(document).ready(function() {
	        var window_height = $(window).height();
	        $(".content-wrapper").css('min-height', window_height);

	        var $user_id = "<?php echo $user_id; ?>";
			var $agent_web_type = "<?php echo $agent_idx_type; ?>";
			var formData = {'user_id' : $user_id};

			//Fetching metas for new agent
			$.ajax({
				method: "POST",
				url: "<?php echo base_url(); ?>idx_login/sync_new_agent",
				data: formData,
				dataType: 'json',
				beforesSend: function() {
					console.log("Fetching data");
				},
				success: function(data) {
					$('.bar').css("width", "25%");
					$bar.text("Fetching Meta Informations");
					$('#theprogressbar').attr('aria-valuenow', "8");
					nearby_listings();
				}
			});

			//Fetching nearby listings
			function nearby_listings() {

				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>idx_login/sync_nearby_listings",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching Nearby Listings");
					},
					success: function(data) {
						$('.bar').css("width", "50%");
						$bar.text("Creating user id");
						$('#theprogressbar').attr('aria-valuenow', "8");
						new_listings();
					}
				});
			}

			//Fetch new listings
			function new_listings() {

				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>idx_login/sync_new_listings",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching New Listings");
					},
					success: function(data) {
						$('.bar').css("width", "75%");
						$bar.text("Creating user id");
						$('#theprogressbar').attr('aria-valuenow', "8");
						listings();
					}
				});

			}	

			//Fetch all agent's listings
			function listings() {

				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>idx_login/sync_agent_listings",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching Agent Listings");
					},
					success: function(data) {
						$('.bar').css("width", "100%");
						$bar.text("Creating user id");
						$('#theprogressbar').attr('aria-valuenow', "8");
						update_sync_status();
					}
				});

			}

			//Update Sync Status
			function update_sync_status() {
				var syncStatus = {'id' : $user_id, 'sync_status' : 1};
				var $status_url = "<?php echo base_url(); ?>idx_login/update_sync_status/";
				$.ajax({
					method: "POST",
					url: $status_url,
					data: syncStatus,
					dataType: 'json',
					beforeSend: function() {
						console.log("Updating Sync Status");
					},
					success: function(data) {
						console.log(data);
						if(data.success) {
							console.log("Successfully Synced!");
							$bar.text("Completed");
		    				$('.progress').removeClass('active');
							
							if($agent_web_type == "TRUE") {
								window.location.href = "<?php echo base_url(); ?>dashboard";
		    				} else {
		    					window.location.href = "<?php echo base_url(); ?>agent_sites";
		    				}
						}
					}
				});
			}
	    });
    </script>
</body>
</html>