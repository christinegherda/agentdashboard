<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?= (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/sync/register.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="<?= base_url()?>assets/js/sync/modernizr.custom.js"></script>
</head>
<body>
	<section class="content-wrapper section-loader">
		<div class="container">
			<div class="section-load" style="display:none;">
				<div class="as-brand">
					<img src="<?= base_url().'assets/images/logo-brand-no-shadow'?>">
				</div>
				<p id="sync_text">We are building your IDX website now...</p>
				<div class="text-center">
			    	<img src="<?= base_url().'assets/img/sync/squares.gif'?>">
			    </div>
			</div>
		</div>
	</section>
	<script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript">
	    $(document).ready(function() {

	    	$(".section-load").show('slow');

	        var window_height = $(window).height();
	        $(".content-wrapper").css('min-height', window_height);
	        
	        setTimeout(function() {
	    		$("#sync_text").text("We are slurping up all your information, and properties you have sold, and all the property for sale in your market...");
	    		console.log("We are slurping up all your information, and properties you have sold, and all the property for sale in your market...");
	    	}, 4000);

	    	setTimeout(function() {
	    		$("#sync_text").text("We are formatting this into a beautiful mobile / seo friendly website...");
	    		console.log("We are formatting this into a beautiful mobile / seo friendly website...");
	    	}, 8000);

	    	setTimeout(function() {

				var $base_url = "<?php echo base_url(); ?>";
		        var $user_id = "<?php echo $user_id; ?>";
				var formData = {'user_id' : $user_id};

				//Fetching metas for new agent
				$.ajax({
					method: "POST",
					//url: $base_url+"idx_login/sync_new_agent",
					url: $base_url+"idx_login/new_sync_agent",
					data: formData,
					dataType: 'json',
					beforeSend: function() {
						$("#sync_text").text("This could take minute or two - soon you will be able view and customize your new website...");
						console.log("This could take minute or two - soon you will be able view and customize your new website...");
					},
					success: function(data) {
						$("#sync_text").text("Redirecting you to your IDX dashboard...");
						console.log("Redirecting you to your IDX dashboard...");
						update_sync_status();
					}
					/*error: function() {
						var url = "<?php echo getenv('AGENT_DASHBOARD_URL') . '/idx_login/resync_data_view?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id').'&reload=1'; ?>";
						window.location.href = url;
					}*/
				});
				
				//Update Sync Status
				function update_sync_status() {
					var syncStatus = {'id' : $user_id, 'sync_status' : 1};
					$.ajax({
						method: "POST",
						url: $base_url+"idx_login/update_sync_status",
						data: syncStatus,
						dataType: 'json',
						beforeSend: function() {
							console.log("Updating Sync Status");
						},
						success: function(data) {
							console.log(data);
							if(data.success) {
								console.log("Successfully Synced!");
								window.location.href = data.redirect;
							}
						}
					});
				}

			}, 12000);

	    });
    </script>
</body>
</html>