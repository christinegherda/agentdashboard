<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?= (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css?version=<?=AS_VERSION?>">
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard.css?version=<?=AS_VERSION?>">
    <link rel="stylesheet" href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css?version=<?=AS_VERSION?>">
</head>
<body>
  <div class="page-bg">
    <div class="modal modal-trial modalCheckoutForm modalTrialCheckout" id="modalTrialForm">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="premium-features">
              <!-- <button class="close" data-dismiss="modal"><i class="fa fa-close"></i></button> -->
              <div class="top">
                <div class="row">
                  <div class="col-md-9">
                    <h3>Get <b>15</b> Days <b>FREE</b> Trial with <br>
                      Premium Features <br>
                      <b>IDX</b> Website</h3>
                      <div class="premium-list">
                        <ul class="list">
                          <li>Custom Domain*</li>
                          <li>Add Unlimited Pages</li>
                          <li>Customize Every Page</li>
                          <li>Add FlexMLS Saved Search <br>to any page</li>
                          <li>Change Menu Navigation</li>
                          <li>Lead Capture</li>
                          <li>CRM</li>
                        </ul>
                        <ul class="list">
                          <li>Listing Alerts</li>
                          <li>Google Analytics</li>
                          <li>Integrates with FlexMLS Smart</li>
                          <li>Phone apps</li>
                          <li>Single Property Websites</li>
                          <li>1 Click Social Media Postings</li>
                          <li>Free Phone Support</li>
                        </ul>
                      </div>
                  </div>
                </div>
              </div>
              <div class="bottom">
                <div class="testimonial">
                  <img src="/assets/images/testimonial.png" alt="">
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <a class="btn btn-show-ccform" >Start with 15 Day Free Trial</a>
                    </div>
                    <div class="col-md-12 text-center">
                      <p><i>* Custom Domain is only available for paid users.</i></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal modal-trial modalCCForm" id="modalCCForm">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <!-- <button class="close" data-dismiss="modal"><i class="fa fa-close"></i></button> -->
            <div class="premium-plans">
              <div class="top">
                <div class="row">
                  <div class="col-md-5">
                    <h3>Choose your plan</h3>
                    <ul class="list">
                      <li>You won’t be charged until after
                      your 15 days free trial.</li>

                      <li>We’ll remind you three days before
                      your trial ends.</li>

                      <li>No commitments, cancel anytime.</li>
                    </ul>
                  </div>
                  <div class="col-md-7">
                    <div class="radio-container">
                      <div class="radio-group">
                        <input type="radio" id="yearly" name="subscribe-plan" id="plan-yearly" value="990" data-plan="yearly" checked>
                        <label for="yearly">
                          <b>Yearly <br> Subscription</b>
                          <p>$82.50 x 12 months</p>
                          <p class="price"><sup>$</sup>990</p>
                          <p class="discount">save 20% ($200)</p>
                        </label>
                      </div>
                      <div class="radio-group">
                        <input type="radio" id="monthly"  name="subscribe-plan" id="plan-monthly" value="99" data-plan="monthly">
                        <label for="monthly">
                          <b>Monthly <br> Subscription</b>
                          <p class="price"><sup>$</sup>99</p>
                          <p class="discount">per month</p>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="bottom">
                <div class="row">
                  <form action="<?=base_url().'payment/create_stripe_subscription'?>" method="post" id="payment-form">
                    <div class="col-md-7">
                      <div class="card">
                        <h3>Credit Card Details</h3>
                        <img src="/assets/images/cc-accepted.png" alt="" class="cc-accepted">
                          <div class="stripe-form">
                            <div class="col-md-12">
                              <input type="hidden" name="email" value="<?=$email?>">
                              <input type="hidden" name="name" value="<?=$first_name.''.$last_name?>">
                              <input type="hidden" name="subdomain_url" value="<?=$subdomain_url?>">
                              <input type="hidden" name="agent_id" value="<?=$id?>">
                              <input type="hidden" name="product_amount" value="990" class="product-mount" >
                              <input type="hidden" name="product_type" value="yearly" class="product-type" >
                              <div id="card-element"></div><!-- A Stripe Element will be inserted here. -->
                              <div id="card-errors" role="alert"></div><!-- Used to display form errors. -->
                              <div id="return-message" role="alert" style="margin-top: 2px"></div>
                            </div>
                          </div>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <p class="terms-link">
                        By clicking on the ‘Get Started’ button
                        at the end of the order process, you are
                        consenting to be bound by our <a href="https://www.agentsquared.com/annual-terms-and-conditions/">terms
                        and conditions</a> and appearing
                        anywhere on AgentSquared website.
                      </p>
                      <button class="btn submit-cc-btn">Get Started</button>
                    </div>
                  </form>
                  <div class="col-md-8">
                    <ul class="link-list">
                      <li><a href="#">Disclaimer</a></li>
                      <li><a href="<?=base_url()?>dashboard/privacy_policy">Privacy Policy</a></li>
                      <li><a href="https://www.agentsquared.com/annual-terms-and-conditions/">Terms & Conditions</a></li>
                    </ul>
                  </div>
                  <div class="col-md-4 text-right">
                    <img src="/assets/images/powered-by-stripe.png" alt="" class="mt-20">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <script src="<?= base_url()?>assets/js/dashboard/jquery.js?version=<?=AS_VERSION?>"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js?version=<?=AS_VERSION?>"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        $(document).ready(function() {

            // $("#modalTrialForm").modal("show");
            $('#modalTrialForm').modal({
                backdrop: 'static',
                keyboard: false
            })

            $(".btn-show-ccform").click(function() {
              $('#modalTrialForm').modal('hide');
              $('#modalCCForm').modal({
                  backdrop: 'static',
                  keyboard: false
              });
            });

            var plan = "<?php echo $plan; ?>"

            if(plan === 'monthly') {

                var amount = $("#plan-monthly").val();

                $("#plan-monthly").prop("checked", true);
                $(".product-sub-amount").text();
                $("#product_amount").val(amount);
                $("#product_plan").val(plan);

            } else {

                var amount = $("#plan-yearly").val();

                $("#plan-yearly").prop("checked", true);
                $(".product-sub-amount").text(amount);
                $("#product_amount").val(amount);
                $("#product_plan").val(plan);

            }

            $("input[name='subscribe-plan']").on('change', function() {

                var amount = $(this).val();
                var plan = $(this).attr('data-plan');

                $(".product-sub-amount").text(amount);
                $("#product_amount").val(amount);
                $("#product_plan").val(plan);

            });

            // Create a Stripe client.
            var stripe = Stripe('pk_test_bQRUjx4h1bciPKtv7fJqYT6f');

            // Create an instance of Elements.
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
              base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                  color: '#aab7c4'
                }
              },
              invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
              }
            };

            // Create an instance of the card Element.
            var card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');

            // Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
              var displayError = document.getElementById('card-errors');
              if (event.error) {
                displayError.textContent = event.error.message;
              } else {
                displayError.textContent = '';
              }
            });

            // Handle form submission.
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function(event) {
              event.preventDefault();

              stripe.createToken(card).then(function(result) {
                if (result.error) {
                  // Inform the user if there was an error.
                  var errorElement = document.getElementById('card-errors');
                  errorElement.textContent = result.error.message;
                } else {
                  // Send the token to your server.
                  stripeTokenHandler(result.token);
                }
              });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {
              // Insert the token ID into the form so it gets submitted to the server
              var form = document.getElementById('payment-form');
              var hiddenInput = document.createElement('input');
              hiddenInput.setAttribute('type', 'hidden');
              hiddenInput.setAttribute('name', 'stripeToken');
              hiddenInput.setAttribute('value', token.id);
              form.appendChild(hiddenInput);

              // Submit the form
              var formData = $('#payment-form').serialize();//form.submit();
              var url = $('#payment-form').attr('action');

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    beforeSend: function() {
                        console.log("Sending...");
                        $('.submit-cc-btn').text('Sending...');
                        $('.submit-cc-btn').prop('disabled', true);
                    },
                    success: function(data) {

                        if(data.success) {

                            $('.submit-cc-btn').prop('disabled', false);
                            $('#return-message').html('<div class="alert alert-success"><strong>Success!</strong> Your 15 days trial will now start. You\'ll be redirecting to your IDX website.</div>');

                            setTimeout(function() {
                                window.location.href = data.redirect+'?modal_welcome=true';
                            }, 5000);

                        }
                    }
                });
            }
        });
    </script>
</body>
</html>