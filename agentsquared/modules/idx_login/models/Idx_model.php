<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Idx_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert_freemium($data=array()) {

        $ret = FALSE;

        if($data) {

            $this->db->insert("freemium", $data);
            $ret = $this->db->insert_id();

            /*if($data['plan_id']==1) {
                $this->db->where("id", $data['subscriber_user_id']);
                $this->db->update("users", array('trial'=>1));
            }*/
            
        }

        return $ret;

    }

    public function isFreemium($user_id=NULL) {

        if($user_id) {

            $this->db->select("plan_id")
                    ->from("freemium")
                    ->where("subscriber_user_id", $user_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            return (!empty($query)) ? TRUE : FALSE;

        }

        return FALSE;
    }

    public function insert_pricing($data=array()) {

        if($data) {
            $this->db->insert('pricing', $data);
        }
    }

    public function update_sync_status($id=NULL, $arr=array()) {

        $ret = FALSE;

        if($id && $arr) {

            $this->db->where("id", $id);
            $this->db->update("users", $arr);

            $ret = ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }

        return $ret;
    }

    public function save_agent_ip() {

        $agent_ip_add = getenv('HTTP_X_FORWARDED_FOR');

        $data = array(
            'agent_id'  => $this->session->userdata('agent_id'),
            'ip_address' => !empty($agent_ip_add) ? $agent_ip_add : $this->input->ip_address()
        );

        $this->db->select('agent_id')
                ->from('agent_ip')
                ->where($data)
                ->limit(1);

        $query = $this->db->get()->row();

        if(empty($query)) {
            $agent_ip_cookie = 'agent_ip_'.$this->session->userdata('agent_id');
            if($this->input->cookie('agent_ip_'.$this->session->userdata('agent_id'))) {
                $count = count($this->input->cookie($agent_ip_cookie))-1;
                if(!in_array($data['ip_address'], $this->input->cookie($agent_ip_cookie))) {
                    $count++;
                    setcookie($agent_ip_cookie.'['.$count.']', $data['ip_address'], time() + (86400 * 360), "/");
                }
            } else {
                setcookie($agent_ip_cookie."[]", $data['ip_address'], time() + (86400 * 360), "/");
            }

            $data['date_created'] = date("Y-m-d h:m:s");
            $this->db->insert('agent_ip', $data);

        }

        return TRUE;
        
    }
    
    public function read_users_data($agent_id = NULL) {

        $ret = FALSE;

        if($agent_id) {

            $this->db->select("id, agent_id, sync_status")
                    ->from("users")
                    ->where("agent_id", $agent_id);

            $query = $this->db->get()->row();

            $ret = (!empty($query)) ? $query : FALSE;

        }

        return $ret;
    }

    public function fetch_user($user_id=NULL, $agent_id=NULL) {

        $ret = FALSE;
        $where = array();

        if(!empty($user_id)) {
            $where['id'] = $user_id;
        } elseif(!empty($agent_id)) {
            $where['agent_id'] = $agent_id;
        } else {
            return FALSE;
        }

        $this->db->select("id, agent_id, mls_id, mls_name, email, code, first_name, last_name, created_on, site_name, sync_status, address, license_number")
                ->from("users")
                ->where($where)
                ->limit(1);

        $result = $this->db->get()->row();

        $ret = (!empty($result)) ? $result : FALSE;

        return $ret;
    }

    public function insert_agent_subscription($data=array()) {
        $this->db->insert("agent_subscription", $data);
    }

    public function insert_spark_user($user=array()) {

        if($user) {

            $this->db->insert('users', $user);

            $user_id = $this->db->insert_id();

            $group["group_id"] = 4;
            $group["user_id"] = $user_id;

            $this->db->insert('users_groups', $group);

            return $user_id;

        }

        return FALSE;

    }

    public function fetch_agent_token($agent_id=NULL) {

        $ret = FALSE;

        if($agent_id) {

            $this->db->select("access_token")
                    ->from("users_idx_token")
                    ->where("agent_id", $agent_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            if(empty($query)) {

                $this->db->select("bearer_token")
                        ->from("user_bearer_token")
                        ->where("agent_id", $agent_id)
                        ->limit(1);

                $query = $this->db->get()->row();

            }

            if(!empty($query))
                $ret = TRUE;
            
        }

        return $ret;
    }

    public function save_token($agent_info = array()) {
        $this->db->insert("users_idx_token", $agent_info);
        return ( $this->db->insert_id() ) ? TRUE : FALSE ;
    }

    public function update_token($agent_id, $token_info = array()) {

        $this->db->where("agent_id", $agent_id);
        $this->db->update("users_idx_token", $token_info);

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;

    }

    public function fetch_all_token() {

        $this->db->select("users_idx_token.*, users.id as system_user_id, users.email, users.first_name, users.last_name")
                ->from("users_idx_token")
                ->join("users", "users.agent_id = users_idx_token.agent_id", "left")
                ->where("DATE(users_idx_token.expected_token_expired)=CURDATE()");
                //->where("users_idx_token.expected_token_expired > NOW()")
                //->where("users_idx_token.expected_token_expired > (NOW() - INTERVAL 24 HOUR)");

        return $this->db->get()->result();
    }

    public function update_refresh_token($id = NULL, $token_info = array()) {

        if($id) {

            $this->db->where("id", $id);
            $this->db->update("users_idx_token", $token_info);

            return TRUE;
        }
    }

    public function fetch_agent_subscription($agent = array()) {

        $ret = FALSE;

        if(isset($agent)) {
            $this->db->select("*")
                    ->from("agent_subscription")
                    ->where($agent);

            $ret = $this->db->get()->result();
        }

        return $ret;
    }

    public function fetch_agent_user_id($agent_id) {
        $ret = FALSE;

        if($agent_id) {
            $this->db->select("id")->from("users")->where("agent_id", $agent_id);
            $id = $this->db->get()->row();

            if($id)
                $ret = $id->id;
        }

        return $ret;
    }

    public function save_bearer_token($data=array()) {

        $ret=FALSE;

        if($data) {
            $this->db->insert("user_bearer_token", $data);
        }

        return $ret;
    }

    public function fetch_acc_token() {

        $ret = FALSE;

        if($this->session->userdata("agent_id")) {

            $query = $this->db->select("access_token")
                        ->from("users_idx_token")
                        ->where("agent_id", $this->session->userdata("agent_id"))
                        ->get()->row();

            if($query) {
                $ret = $query->access_token;
            }
        }

        return $ret;
    }
    
    public function register_mls_data_search_fields($data = null) {
        $ret = false;
        if($data) {
            $this->db->insert('mls_data_search_fields_list_data', $data);
            $ret = $this->db->insert_id();
        }
        return $ret;
    }

    public function register_search_fields($data = NULL) {
        $ret = false;
        if($data) {
            $this->db->insert('mls_data_search_fields', $data);
            $ret = $this->db->insert_id();
        }
        return $ret;
    }

    public function get_agents() {
        
        $offset = ($this->input->get("offset")) ? $this->input->get("offset") : 0;

        $this->db->select("u.id, uit.access_token")
                ->from("users u")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id", "inner")
                ->where("mls_id", "")
                ->where("mls_name", "")
                ->where("uit.is_valid", 1)
                ->offset($offset);

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function search_fields_count($mls_id=NULL) {

        if($mls_id) {

            $query = $this->db->select("*")
                                ->from("mls_data_search_fields")
                                ->where("mls_id", $mls_id)
                                ->limit(1)
                                ->get()->result();

            return $query;

        }

        return FALSE;

    }

    public function check_pages() {

        $pages["home"] = $this->db->select("slug")
                                ->from("pages")
                                ->where("agent_id", $this->session->userdata('agent_id'))
                                ->where("slug", "home")
                                ->limit(1)
                                ->get()->row();

        $pages["find-a-home"] = $this->db->select("slug")
                                ->from("pages")
                                ->where("agent_id", $this->session->userdata('agent_id'))
                                ->where("slug", "find-a-home")
                                ->limit(1)
                                ->get()->row();

        $pages["seller"] = $this->db->select("slug")
                                ->from("pages")
                                ->where("agent_id", $this->session->userdata('agent_id'))
                                ->where("slug", "seller")
                                ->limit(1)
                                ->get()->row();

        $pages["buyer"] = $this->db->select("slug")
                                ->from("pages")
                                ->where("agent_id", $this->session->userdata('agent_id'))
                                ->where("slug", "buyer")
                                ->limit(1)
                                ->get()->row();

        $pages["contact"] = $this->db->select("slug")
                                ->from("pages")
                                ->where("agent_id", $this->session->userdata('agent_id'))
                                ->where("slug", "contact")
                                ->limit(1)
                                ->get()->row();

        $pages["blog"] = $this->db->select("slug")
                                ->from("pages")
                                ->where("agent_id", $this->session->userdata('agent_id'))
                                ->where("slug", "blog")
                                ->limit(1)
                                ->get()->row();

        $pages["testimonials"] = $this->db->select("slug")
                                ->from("pages")
                                ->where("agent_id", $this->session->userdata('agent_id'))
                                ->where("slug", "testimonials")
                                ->limit(1)
                                ->get()->row();

        return $pages;

    }

    public function check_home_options() {

        $options["active"] = $this->db->select("id")
                                    ->from("home_options")
                                    ->where("agent_id", $this->session->userdata("agent_id"))
                                    ->where("option_name", "active_listings")
                                    ->limit(1)
                                    ->get()->row();

        $options["new"] = $this->db->select("id")
                                    ->from("home_options")
                                    ->where("agent_id", $this->session->userdata("agent_id"))
                                    ->where("option_name", "new_listings")
                                    ->limit(1)
                                    ->get()->row();

        $options["nearby"] = $this->db->select("id")
                                    ->from("home_options")
                                    ->where("agent_id", $this->session->userdata("agent_id"))
                                    ->where("option_name", "nearby_listings")
                                    ->limit(1)
                                    ->get()->row();

        $options["office"] = $this->db->select("id")
                                    ->from("home_options")
                                    ->where("agent_id", $this->session->userdata("agent_id"))
                                    ->where("option_name", "office_listings")
                                    ->limit(1)
                                    ->get()->row();

        return $options;

    }

    public function check_domain_exist($subdomain=NULL) {
        
        $ret = FALSE;

        if(!empty($subdomain)) {

            $this->db->select('domains')
                    ->from('domains')
                    ->where('domains', $subdomain)
                    ->where('status', 'completed')
                    ->limit(1);

            $query = $this->db->get()->row();

            if(!empty($query))
                $ret = TRUE;

        }

        return $ret;

    }

    public function saleforce_pre_data($agent_id=NULL) {

        if(!empty($agent_id)) {

            $this->db->select('u.id, u.first_name, u.last_name, u.company, u.email, u.phone, u.mls_name, u.date_signed, as.LaunchFrom, as.isFromSpark, as.PricingPlan, f.plan_id, uit.access_token, sml.facebook, sml.twitter, sml.linkedin, sml.googleplus, d.domains, d.subdomain_url')
                    ->from('users u')
                    ->join('agent_subscription as', 'u.agent_id=as.UserId', 'left')
                    ->join('freemium f', 'f.subscriber_user_id=u.id', 'left')
                    ->join('users_idx_token uit', 'uit.agent_id=u.agent_id', 'left')
                    ->join('social_media_links sml', 'sml.user_id=u.id', 'left')
                    ->join('domains d', 'd.agent=u.id', 'left')
                    ->where('u.agent_id', $agent_id)
                    ->limit(1);

            $data['user'] = $this->db->get()->row_array();
            $data['listings_scale'] = $this->get_users_listings_scale($agent_id);

            return $data;

        }

        return FALSE;

    }

    private function get_users_listings_scale($agent_id=NULL) {

        if(!empty($agent_id)) {

            $this->load->library('idx_auth', $agent_id);

            $mls_status = Modules::run('dsl/getMlsStatus', $agent_id);

            if(in_array("Sold", $mls_status)) {
                $filter_sold = "Or MlsStatus Ne 'Sold'";
            }

            if(in_array("Leased", $mls_status)){
                $filter_leased = true;
            }

            if(in_array("Pending", $mls_status)){
                $filter_pending = true;
            }

            if(isset($filter_pending) && isset($filter_leased)){
                $lp_filter = "MlsStatus Ne 'Leased' Or MlsStatus Ne 'Pending'";
            } elseif(isset($filter_leased)){
                $lp_filter = "MlsStatus Ne 'Leased'";
            } elseif(isset($filter_pending)){
                $lp_filter = "MlsStatus Ne 'Pending'";
            }

            $sold_filter = isset($filter_sold)? $filter_sold :"";
            $filter = isset($lp_filter) ? ") And (".$lp_filter.")" : "";
            $filterStr = "(MlsStatus Ne 'Closed' ".$sold_filter." ".$filter."";
            $system_info = Modules::run('mysql_properties/get_system_info', $agent_id);//$this->get_system_info($agent_id);

            if(!empty($system_info) && $system_info->MlsId == "20170119210918715503000000"){
                $filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Active Under Contract')";
            }

            $filter_url = rawurlencode($filterStr);
            $filter_string = substr($filter_url, 3, -3);

            $active = $this->idx_auth->GetMyListings(
                array(
                    '_pagination' => 1,
                    '_page'       => 1,
                    '_limit'      => 1,
                    '_filter'     => $filter_string,
                    '_orderby'    => '-OnMarketDate'
                )
            );

            $data['total_active']           = (isset($active['pagination']->TotalRows) && !empty($active['pagination']->TotalRows)) ? $active['pagination']->TotalRows : 0;
            $data['recent_active_date']     = (isset($active['results'][0]->StandardFields->OnMarketDate)) ? $active['results'][0]->StandardFields->OnMarketDate : '';
            $data['recent_active_property'] = (isset($active['results'][0]->StandardFields->UnparsedAddress)) ? $active['results'][0]->StandardFields->UnparsedAddress : '';
            $data['active_listing_id']      = (isset($active['results'][0]->Id)) ? $active['results'][0]->Id : '';

            $filterStr_Sold = "(MlsStatus Eq 'Closed' Or MlsStatus Eq 'Sold' Or MajorChangeType Eq 'Closed')";
            $filter_url_sold = rawurlencode($filterStr_Sold);
            $filter_string_sold = substr($filter_url_sold, 3, -3);

            $sold = $this->idx_auth->GetMySoldListing(
                array(
                    '_pagination' => 1,
                    '_page'       => 1,
                    '_limit'      => 1,
                    '_filter'     => $filter_string_sold,
                    '_orderby'    => '-CloseDate'
                )
            );
            
            $data['total_sold']             = (isset($sold['pagination']->TotalRows) && !empty($sold['pagination']->TotalRows)) ? $sold['pagination']->TotalRows : 0;
            $data['recent_sold_date']       = (isset($sold['results'][0]->StandardFields->CloseDate)) ? $sold['results'][0]->StandardFields->CloseDate : '';
            $data['recent_sold_property']   = (isset($sold['results'][0]->StandardFields->UnparsedAddress)) ? $sold['results'][0]->StandardFields->UnparsedAddress : '';
            $data['sold_listing_id']        = (isset($sold['results'][0]->Id)) ? $sold['results'][0]->Id : '';

            return $data;

        }

        return FALSE;
    
    }

    public function save_manual_tokens($tokens, $agent_id) {
        if(!empty($agent_id)){
            $token_info["access_token"] = $tokens['access_token'];
            $token_info["refresh_token"] = $tokens['refresh_token'];
            $token_info["date_modified"] = date("Y-m-d h:m:s");
            $token_info["expected_token_expired"] = date("Y-m-d h:m:s", strtotime('+23 hours'));
            $token_info["is_valid"] = 1;

            $this->db->where("agent_id", $agent_id);
            $this->db->update("users_idx_token", $token_info);

            return true;
        }

        return false;
    }

    public function check_return_agent() {

        $data['success'] = FALSE;

        $this->db->select('ApplicationType')
                ->from('agent_subscription')
                ->where('UserId', $this->session->userdata('agent_id'))
                ->where('EventType', 'purchased')
                ->limit(1);

        $query = $this->db->get()->row();

        if(empty($query)) {
            //update subscription status
            $this->db->where('UserId', $this->session->userdata('agent_id'));
            $this->db->update('agent_subscription', array('CallbackName'=>'Agent Purchase','EventType'=>'purchased'));

            if($this->db->affected_rows()>0) {
                $data['success'] = TRUE;
                $data['redirect'] = 'dashboard';
            }

        }

        return $data;

    }

    /**
     * Get pricing data of specific user
     *
     * @param       Int  $user_id    Input Int
     * @return      object array
     */
    public function get_pricing(Int $user_id) {

        $this->db->select('plan, type')
                ->from('pricing')
                ->where('user_id', $user_id)
                ->limit(1);

        return $this->db->get()->row();

    }

    /**
     * Get subscription launch type
     *
     * @param       Array  $data    Input Int
     * @return      object array isFromSpark
     */
    public function get_subscription(Array $data) {

        $this->db->select('isFromSpark')
                ->from('agent_subscription')
                ->where($data)
                ->limit(1);

        return $this->db->get()->row();

    }

    /**
     * Get data for trial page
     *
     *  receipt data
     *  users data
     *  pricing data
     *
     * @param       Int  $user_id    Input Int
     * @return      Array
     */
    public function get_trial_data(Int $user_id) {

        $this->db->select('users.id, users.email, users.first_name, users.last_name, domains.subdomain_url, pricing.plan')
                ->from('users')
                ->join('domains', 'domains.agent = users.id')
                ->join('pricing', 'pricing.user_id = users.id', 'left')
                ->where('users.id', $user_id)
                ->limit(1);

        return $this->db->get()->row();

    }

    /**
     * Check if trial user already has stripe data
     *
     * @param       Array  $data    Input Int
     * @return      object array rid
     */
    public function isTrialUnique(Int $user_id) {

        $this->db->select('rid')
                ->from('receipt')
                ->where('agent_id', $user_id)
                ->limit(1);

        return $this->db->get()->row() ? FALSE : TRUE;

    }

}