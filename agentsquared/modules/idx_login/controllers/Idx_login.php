<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Idx_login extends MX_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('pages/page_model', 'page');
		$this->load->model('customize_homepage/customize_homepage_model', 'custom');
		$this->load->model('idx_login/idx_model', 'idx_model');
		$this->load->model('crm/leads_model', 'leads');
		$this->load->model('mysql_properties/properties_model', 'pm');
		$this->load->model('register/registration_model', 'reg_model');

		//$this->output->enable_profiler(TRUE);
	
	}

	public function index() {

		//Identify where the user came from (is it from spark or from landing page)
		$from = ($this->input->get('from')) ? $this->input->get('from') : 'spark';

		if($from == "website" || $from == "landingpage") {

			$pricing 	= ($this->input->get("pricing")) 	? $this->input->get("pricing") 	: "";
			$app_type 	= ($this->input->get("app_type")) 	? $this->input->get("app_type") : "";
			$price 		= ($this->input->get("price")) 		? $this->input->get("price") 	: "";
			$plan_id 	= ($this->input->get("plan_id")) 	? $this->input->get("plan_id") 	: "";

			$website['pricing'] 	= $pricing;
			$website['price'] 		= $price;
			$website['app_type']	= $app_type;
			$website['plan_id'] 	= $plan_id;

			if($from == "website") {
				$website['from_website']= TRUE;
			} else {
				$website['landingpage']= TRUE;
			}

			$this->session->set_userdata($website);

		}


		if(!$this->session->userdata('access_token')) {
			if($from == "armls") {
				$this->armls_login();
				exit;
			} 
			$this->spark_login();
			exit;
		}

	    $account_information = $this->myaccount($this->session->userdata('access_token'));

		if(!isset($account_information["D"]["Results"][0]["Emails"][0]["Address"]) || empty($account_information["D"]["Results"][0]["Emails"][0]["Address"]) || empty($account_information["D"]["Results"][0]["FirstName"]) || empty($account_information["D"]["Results"][0]["LastName"])) {

			$this->load->library("email");

			$agent_Id 			= (isset($account_information["D"]["Results"][0]["Id"])) ? $account_information["D"]["Results"][0]["Id"] : '';
			$agent_mls			= (isset($account_information["D"]["Results"][0]["Mls"])) ? $account_information["D"]["Results"][0]["Mls"] : '';
			$agent_email 		= (isset($account_information["D"]["Results"][0]["Emails"][0]["Address"]) && !empty($account_information["D"]["Results"][0]["Emails"][0]["Address"])) ? $account_information["D"]["Results"][0]["Emails"][0]["Address"] : NULL;
			$agent_first_name 	= (isset($account_information["D"]["Results"][0]["FirstName"])) ? $account_information["D"]["Results"][0]["FirstName"] : '';
			$agent_last_name	= (isset($account_information["D"]["Results"][0]["LastName"])) ? $account_information["D"]["Results"][0]["LastName"] : '';
			$agent_phone		= (isset($account_information["D"]["Results"][0]["Phones"][0]["Number"]) && !empty($account_information["D"]["Results"][0]["Phones"][0]["Number"])) ? $account_information["D"]["Results"][0]["Phones"][0]["Number"] : '';

			if(isset($account_information['D']['Code'])) { //Check if we're not yet over the limit.

				if($account_information['D']['Code']=='1550') {

					$data['err_type'] = 'rate_limit';
					$failed_idx_reason = 'Code: '.$account_information['D']['Code'].'. <br> Message: '.$account_information['D']['Message'];

					$this->email->send_email_v3(
			        	'admin@agentsquared.com', //sender email
			        	'AgentSquared', //sender name
			        	'support@agentsquared.com', //recipient email
			        	'alvin@agentsquared.com,honradokarljohn@gmail.com,paul@agentsquared.com,joce@agentsquared.com,aldwinj12345@gmail.com', //bccs
			        	'f1bd638d-fe3b-4b1e-acd1-17c8335b7342', //'b581f7e2-befe-4162-94b3-d5f470216a34', //template_id
			        	$email_data = array(
			        		'subs' => array(
			    				'agent_id' 			=> '',
			    				'agent_first_name' 	=> '',
			    				'agent_last_name' 	=> '',
			    				'agent_email' 		=> '',
			    				'agent_mls' 		=> '',
			    				'agent_phone' 		=> '',
			    				'failed_idx_reason'	=> $failed_idx_reason
			        		)
						)
			        );

				}

			} else {
				
				//Send an email to support, that this agent failed to creat idx
				if(!isset($account_information["D"]["Results"][0]["Emails"][0]["Address"])) {
					$failed_idx_reason = "The agent's email was not set in flexmls.";
					$data['err_type'] = 'email';
				} elseif(empty($account_information["D"]["Results"][0]["Emails"][0]["Address"])) {
					$failed_idx_reason = "The agent's email in flexmls is empty.";
					$data['err_type'] = 'email';
				} elseif(empty($account_information["D"]["Results"][0]["FirstName"])) {
					$failed_idx_reason = "The agent's first name in flexmls is empty.";
					$data['err_type'] = 'name';
				} elseif(empty($account_information["D"]["Results"][0]["LastName"])) {
					$failed_idx_reason = "The agent's last name in flexmls is empty.";
					$data['err_type'] = 'name';
				} else {
					$failed_idx_reason = "Some of the agent's important details was not properly set in the flexmls.";
					$data['err_type'] = 'default';
				}

				$this->email->send_email_v3(
			    	'admin@agentsquared.com', //sender email
			    	'AgentSquared', //sender name
			    	'support@agentsquared.com', //recipient email
			    	'alvin@agentsquared.com,honradokarljohn@gmail.com,paul@agentsquared.com,joce@agentsquared.com,aldwinj12345@gmail.com', //bccs
			    	'f1bd638d-fe3b-4b1e-acd1-17c8335b7342', //'b581f7e2-befe-4162-94b3-d5f470216a34', //template_id
			    	$email_data = array(
			    		'subs' => array(
							'agent_id' 			=> $agent_Id,
							'agent_first_name' 	=> $agent_first_name,
							'agent_last_name' 	=> $agent_last_name,
							'agent_email' 		=> $agent_email,
							'agent_mls' 		=> $agent_mls,
							'agent_phone' 		=> $agent_phone,
							'failed_idx_reason'	=> $failed_idx_reason
			    		)
					)
			    );

			}

			$this->session->unset_userdata('access_token'); //unset access token session
			$this->session->unset_userdata('refresh_token'); //unset refresh token session

			$this->load->view('errors/html/failed_idx', $data);

		} else {

			//Set email and agent id
			$email 		= $account_information["D"]["Results"][0]["Emails"][0]["Address"];
			$agent_id 	= $account_information["D"]["Results"][0]["Id"];
			$mls_id 	= $account_information["D"]["Results"][0]["MlsId"];

			//Fetch Existing User Data
			$agent_result = $this->idx_model->fetch_user(NULL, $agent_id);

			//Set session for agent photo
			$agent_arr = array(
	        	'agent_name'  	=> (isset($account_information["D"]["Results"][0]["FirstName"]) && (!empty($account_information["D"]["Results"][0]["FirstName"]))) ? $account_information["D"]["Results"][0]["FirstName"] : 'Admin',
	           	'agent_photo'	=> (isset($account_information["D"]["Results"][0]['Images'][0]['Uri']) && (!empty($account_information["D"]["Results"][0]['Images'][0]['Uri']))) ? $account_information["D"]["Results"][0]['Images'][0]['Uri'] : AGENT_DASHBOARD_URL . 'assets/images/no-profile-img.gif',
	            'site_title' 	=> (isset($agent_result->site_name) && (!empty($agent_result->site_name))) ? $agent_result->site_name : 'Your IDX Website',
	        );

			$this->session->set_userdata('agentdata', $agent_arr);

			if(!empty($agent_result)) { // If the agent already have record

				//Update token in DSL
				$this->update_spark_token($agent_result->id, $agent_result->agent_id, $this->session->userdata("access_token"), $this->session->userdata("refresh_token"));

				$arr = array(
					'user_id'		=> $agent_result->id,
					'agent_id'		=> $agent_result->agent_id,
					'email'	 		=> $agent_result->email,
					'code' 			=> $agent_result->code,
					'identity' 		=> $agent_result->email,
					'old_last_login'=> $agent_result->created_on,
					'logged_in_auth'=> true,
				);

				$this->session->set_userdata($arr); //Set session to bypass login

				if($agent_result->sync_status == 0) { //If last sync or on board of agent was not successful

					//Check pages
					$page = $this->idx_model->check_pages();

					if(empty($page['home'])) {
						$this->page->insert_home_page($agent_result->agent_id);
					}
					if(empty($page['find-a-home'])) {
						$this->page->insert_find_home_page($agent_result->agent_id);
					}
					if(empty($page['seller'])) {
						$this->page->insert_seller_page($agent_result->agent_id);
					}
					if(empty($page['buyer'])) {
						$this->page->insert_buyer_page($agent_result->agent_id);
					}
					if(empty($page['contact'])) {
						$this->page->insert_contact_page($agent_result->agent_id);
					}
					if(empty($page['blog'])) {
						$this->page->insert_blog_page($agent_result->agent_id);
					}
					if(empty($page['testimonials'])) {
						$this->page->insert_testimonial_page($agent_result->agent_id);
					}

					//Check home options
					$option = $this->idx_model->check_home_options();

					if(empty($option['active'])) {
						$this->custom->insert_active_option($agent_result->agent_id);
					}
					if(empty($option['new'])) {
						$this->custom->insert_new_option($agent_result->agent_id);
					}
					if(empty($option['nearby'])) {
						$this->custom->insert_nearby_option($agent_result->agent_id);
					}
					if(empty($option['office'])) {
						$this->custom->insert_office_option($agent_result->agent_id);
					}

					$this->resync_data_view($agent_result->id, $agent_result->agent_id);

				} else {

					$this->check_user_dsl($agent_result->id); //Sync DSL Data
						
					$subscription = $this->agent_subscription($agent_result->agent_id);

					if($subscription == "idx") {
						redirect('dashboard', 'refresh');
					} elseif($subscription == "spw") {
						redirect('single_property_listings', 'refresh');
					} else {
						// Check if the agent has returned from canceled
						$returning = $this->idx_model->check_return_agent();

						if($returning['success']) {
							redirect($returning['redirect'], 'refresh');						
						} else {
							redirect('login/auth/logout', 'refresh');	
						}
					}
					
				}

			} else { // If agent is just new here


				$user = array(
					'agent_id' 		=> $agent_id,
					'mls_id'		=> (isset($account_information["D"]["Results"][0]["MlsId"]) && !empty($account_information["D"]["Results"][0]["MlsId"])) ? $account_information["D"]["Results"][0]["MlsId"] : "",
					'ip_address' 	=> $this->input->ip_address(),
					'email' 		=> $email,
					'first_name'	=> (isset($account_information["D"]["Results"][0]["FirstName"]) && !empty($account_information["D"]["Results"][0]["FirstName"])) ? $account_information["D"]["Results"][0]["FirstName"] : "",
					'last_name'		=> (isset($account_information["D"]["Results"][0]["LastName"]) && !empty($account_information["D"]["Results"][0]["LastName"])) ? $account_information["D"]["Results"][0]["LastName"] : "",
					'company' 		=> (isset($account_information["D"]["Results"][0]["Office"]) && !empty($account_information["D"]["Results"][0]["Office"])) ? $account_information["D"]["Results"][0]["Office"] : "",
					'mls_name'		=> (isset($account_information["D"]["Results"][0]["Mls"]) && !empty($account_information["D"]["Results"][0]["Mls"])) ? $account_information["D"]["Results"][0]["Mls"] : "",
					'password' 		=> '$2y$10$2VAtH6N0ewVv7dlEJjEG9eVbf13MZ4zS5Alp1sx0Uc4bXGcwRMRWG',//'test1234',
					'phone' 		=> (isset($account_information["D"]["Results"][0]["Phones"][0]["Number"]) && !empty($account_information["D"]["Results"][0]["Phones"][0]["Number"])) ? $account_information["D"]["Results"][0]["Phones"][0]["Number"] : "",
					'city' 			=> (isset($account_information["D"]["Results"][0]["Addresses"][0]["City"]) && !empty($account_information["D"]["Results"][0]["Addresses"][0]["City"])) ? $account_information["D"]["Results"][0]["Addresses"][0]["City"] : "",
					'zip' 			=> (isset($account_information["D"]["Results"][0]["Addresses"][0]["PostalCode"]) && !empty($account_information["D"]["Results"][0]["Addresses"][0]["PostalCode"])) ? $account_information["D"]["Results"][0]["Addresses"][0]["PostalCode"] : "",
					'address' 		=> (isset($account_information["D"]["Results"][0]["Addresses"][0]["Address"]) && !empty($account_information["D"]["Results"][0]["Addresses"][0]["Address"])) ? $account_information["D"]["Results"][0]["Addresses"][0]["Address"] : "",
					'broker' 		=> (isset($account_information["D"]["Results"][0]["Office"]) && !empty($account_information["D"]["Results"][0]["Office"])) ? $account_information["D"]["Results"][0]["Office"] : "",
					'license_number'=> (isset($account_information["D"]["Results"][0]["LicenseNumber"]) && !empty($account_information["D"]["Results"][0]["LicenseNumber"])) ? $account_information["D"]["Results"][0]["LicenseNumber"] : "",
					'theme' 		=> 'agent-haven',
					'active' 		=> 1,
					'created_on' 	=> date('Y-m-d H:i:s'),
					'code' 			=> generate_key(8),
					'sync_status'   => 0,
					'date_signed' 	=> date('Y-m-d H:i:s'),
					'auth_type'		=> 0
				);

				//Identify if the user launches from spark or from pricing page
				if($from == "spark" || $from == "armls" || $from == "akmls" || $from == "cpar" || $from == "momls" || $from == "space_coast" || $from == "ventura" || $from == "lake_havasu_association" || $from == "gepar" || $from == "east_mississippi_realtors") {
					//$purch_arr = $this->purchased_from_spark($user);
					$user_id = $this->purchased_from_spark($user, $from);
				} else {
					//$purch_arr = $this->purchased_from_agentsquared($user);
					$user_id = $this->purchased_from_agentsquared($user, $from);
				}

				//insert pages
				$this->page->insert_home_page($user['agent_id']);
				$this->page->insert_find_home_page($user['agent_id']);
				$this->page->insert_seller_page($user['agent_id']);
				$this->page->insert_buyer_page($user['agent_id']);
				$this->page->insert_contact_page($user['agent_id']);
				$this->page->insert_blog_page($user['agent_id']);
				$this->page->insert_testimonial_page($user['agent_id']);

				//insert home options
				$this->custom->insert_active_option($user['agent_id']);
				$this->custom->insert_new_option($user['agent_id']);
				$this->custom->insert_nearby_option($user['agent_id']);
				$this->custom->insert_office_option($user['agent_id']);

				//save latlon to database
				Modules::run('mysql_properties/save_nearby_latlon', $user['agent_id']);

				//save sold listings to database
				Modules::run('mysql_properties/syncAgentSoldListings', $user_id, $user['agent_id'], FALSE);

				//Syncing of all datas
				$this->sync_data_view($user_id, $user['agent_id']); //Load sync loader view
				
			}
		}
	}

	private function purchased_from_spark($user=array(), $from='') {

		if($user) {

			$mlsListed = array(
				"20070913202326493241000000", 
				"20140801122353246389000000", 
				"20140402194227539412000000", 
				"20040823195642954262000000",
				"20150128160104800461000000",
				"20140311223451933927000000",
				"20130226165731231246000000",
				"20160622112753445171000000",
				"20171027162842062220000000",
				"20180216183507684268000000",
				"19991216223731149258000000"
			);

			if(in_array($user['mls_id'], $mlsListed)) {
			  $isListedMLS = TRUE;
			}else{
			  $isListedMLS = FALSE;
			}

			if($isListedMLS) {

				return $this->purchased_from_agentsquared($user, $from);

			} else {

				$user['trial'] = 1;

				$user_id = $this->idx_model->insert_spark_user($user);

				if($user_id) {

					//Set session to by pass login
					$arr = array(
						'user_id' 		=> $user_id,
						'agent_id' 		=> $user['agent_id'],
						'email' 		=> $user['email'],
						'code' 			=> $user['code'],
						'identity' 		=> $user['email'],
						'old_last_login'=> $user['created_on'],
						'logged_in_auth'=> TRUE,
					);

					$this->session->set_userdata($arr);

					//Save important data 
					$this->save_spark_tokens($user_id, $user['agent_id'], $this->session->userdata("access_token"), $this->session->userdata("refresh_token")); //Save tokens to DSL

					//sync account info
					//$this->save_account_metas_segment($user_id, $user['agent_id']); //Save Account Metas

					//sync mysql properties
					$call_url= getenv('AGENT_SYNCER_URL').'onboard-agent-listings-updater/'.$user_id.'/'.$user['agent_id'].'/onboarding?access_token='.$this->session->userdata('access_token');

		 			$headers = array('Content-Type: application/json');

		 			$body=array();
		 			$body_json = json_encode($body, true);

		 			$ch = curl_init();
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, 0);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
					curl_setopt($ch, CURLOPT_URL, $call_url);

					$result_info  = curl_exec($ch);

					return $user_id;
					/*if(strpos($endpoint, 'spark-')) {
						$this->dsl_model->log_request();
						$this->dsl_model->log_request_history($endpoint, 'POST', $body);
					}

					if(!$result_info) {
						$result_info = curl_error($ch);
					}*/
					
					//$this->save_account_metas_segment($user_id, $user['agent_id']); //Save Account Metas
					//$bench = $this->save_mysql_properties($user_id, $user['agent_id']); //Save homepage properties
					//return array('bench'=>$bench, 'user_id'=>$user_id);
					//return $user_id;
				}

			}
			
		}

	}

	/**
     * Process agent's purchased subscription. Insert to agent_subscription table
     *
     *  Insert to freemium if agent's MLS ID is a partner in freemium program
     *  If agent lauches from landing page created from FB ads, set it to 15 days trial
     *
     * @param  array  $user
     * @param  String  $from 
     *
     * @return view redirect to agentsite
     */
	private function purchased_from_agentsquared($user=array(), $from='') {

		if($user) {

			$app_type = $this->session->userdata('app_type');
			$launch_from = !empty($from) ? $from : $this->session->userdata('from');

			switch ($app_type) {
				case 'idx':
					$ApplicationType = 'Agent_IDX_Website';
					break;
				case 'spw':
					$ApplicationType = 'Single_Property_Website';
					break;
				case 'idxpro':
					$ApplicationType = 'Agent_IDX_Website_Pro';
					break;
				default:
					$ApplicationType = 'Agent_IDX_Website';
					break;
			}

			if($launch_from == 'landingpage') {

				$isListedMLS = FALSE;

			} else {

				$mlsListed = array(
					"20070913202326493241000000",
					"20140801122353246389000000",
					"20140402194227539412000000",
					"20040823195642954262000000",
					"20150128160104800461000000",
					"20140311223451933927000000",
					"20130226165731231246000000",
					"20160622112753445171000000",
					"20171027162842062220000000",
					"20180216183507684268000000",
					"19991216223731149258000000"
				);

				$isListedMLS = (in_array($user['mls_id'], $mlsListed)) ? TRUE : FALSE;

			}

			//Insert to agent_subscription
			$data = array(
			  	'ApplicationName' 	=> ($app_type==='spw') ? 'Single Property Website Social Media Sharing Tool' : 'Instant IDX Website powered by Flexmls',
			   	'ApplicationUri' 	=> ($this->session->userdata('plan_id')) ? 'https://www.agentsquared.com/pricing/' : 'https://www.agentsquared.com/freemium/',
			   	'CallbackName' 		=> "Agent Purchase",
			   	'EventType' 		=> "purchased",
			   	'PricingPlan' 		=> ($this->session->userdata('pricing')) ? $this->session->userdata('pricing') : 0,
			   	'TestOnly' 			=> 'false',
			   	'Timestamp' 		=> date('Y-m-d H:i:s'),
			   	'UserEmail' 		=> $user['email'],
			   	'UserId' 			=> $user['agent_id'],
			   	'UserName' 			=> $user['first_name'].' '.$user['last_name'],
			   	'ApplicationType' 	=> $ApplicationType,
			   	'isFromSpark' 		=> 0,
			   	'LaunchFrom' 		=> ($isListedMLS) ? 'Flex' : 'Corp Site'
			);

			$this->idx_model->insert_agent_subscription($data);

			//Register agent who purchased from corp site to spark appstore
			$this->register_external_subscribers($user['agent_id'], $this->session->userdata("access_token"));

			//Insert to users table
			$user['trial'] = ($isListedMLS) ? 0 : 1;
			$user_id = $this->idx_model->insert_spark_user($user);

			if($user_id) {

				//Set session to bypass login
				$arr = array(
					'user_id' 		=> $user_id,
					'agent_id' 		=> $user['agent_id'],
					'email' 		=> $user['email'],
					'code' 			=> $user['code'],
					'identity' 		=> $user['email'],
					'old_last_login'=> $user['created_on'],
					'logged_in_auth'=> TRUE,
				);

				$this->session->set_userdata($arr);

				//Save tokens to dsl
				$this->save_spark_tokens($user_id, $user['agent_id'], $this->session->userdata('access_token'), $this->session->userdata('refresh_token')); //Save tokens to DSL

				//sync account info to mysql db
				//$this->save_account_metas_segment($user_id, $user['agent_id']); //Save Account Metas

				//sync mysql properties
				$obj = modules::load('dsl');
				$call_url= getenv('AGENT_SYNCER_URL').'onboard-agent-listings-updater/'.$user_id.'/'.$user['agent_id'].'/onboarding?access_token='.$this->session->userdata('access_token');

	 			$headers = array('Content-Type: application/json');

	 			$body=array();
	 			$body_json = json_encode($body, true);

	 			$ch = curl_init();
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 0);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
				curl_setopt($ch, CURLOPT_URL, $call_url);

				$result_info  = curl_exec($ch);
				
				//Save pricing info
				$price_info = array(
					'user_id' 		=> $user_id,
					'type'			=> ($this->session->userdata('app_type')) ? $this->session->userdata('app_type') : 'idx',
					'price' 		=> ($this->session->userdata('price')) ? $this->session->userdata('price') : 0,
					'plan'			=> ($this->session->userdata('pricing')) ? $this->session->userdata('pricing') : 'monthly',
					'date_created' 	=> date('Y-m-d H:i:s')
				);

				$this->idx_model->insert_pricing($price_info);

				//If the agent is from ARMLS, MichRic, AKMLS and Sibor save to freemium table
				if($isListedMLS) {
					//Saving into Freemium table
					$freemium = array(
						'plan_id' 			=> ($this->session->userdata('plan_id')) ? $this->session->userdata('plan_id') : 1,
						'subscriber_user_id'=> $user_id,
						'date_created'		=> date('Y-m-d H:i:s')
					);

					$this->idx_model->insert_freemium($freemium);

					//Set isFreemium session
					$this->session->set_userdata("isFreemium", TRUE);
				}

				//Slider Photos for ARMLS/MICHRIC/AKMLS/CPAR/MOMLS/SPACE-COAST/LAKE-HAVASU/VENTURA/GEPAR/EAST-MISSISSIPPI
				$default_slider_mls_id = array(
					"20070913202326493241000000","20140402194227539412000000","20040823195642954262000000",
					"20150128160104800461000000","20140311223451933927000000","20130226165731231246000000",
					"20171027162842062220000000","20160622112753445171000000","20180216183507684268000000",
					"19991216223731149258000000"
				);

				if(in_array($user['mls_id'], $default_slider_mls_id)) {
				  Modules::run('agent_sites/upload_default_slider_photo',$user['mls_id'],$user_id);
				}

				//return array('bench'=>$bench, 'user_id'=>$user_id);
				return $user_id;

			}

		}

	}

	public function spark_login() {
		$client_id     = getenv('SPARK_CLIENT_ID');
		$client_secret = getenv('SPARK_CLIENT_SECRET');
		$redirect_uri  = getenv('AGENT_DASHBOARD_URL') . "idx_login/idx_callback";
		$url = "https://sparkplatform.com/openid?openid.mode=checkid_setup&openid.return_to=".$redirect_uri."&openid.spark.client_id=".$client_id."&openid.spark.combined_flow=true";
		header('Location: ' . $url);
		exit;
	}

	public function armls_login() {
		$client_id     = 'c417twiz2tfscsj1f73e2zg7y';//getenv('ARMLS_CLIENT_ID');
		$client_secret = '81e57w3hijt0howtpk5gjath3';//getenv('ARMLS_CLIENT_SECRET');
		$redirect_uri  = getenv('AGENT_DASHBOARD_URL') . "idx_login/idx_callback/armls";
		$url = "https://sparkplatform.com/openid?openid.mode=checkid_setup&openid.return_to=".$redirect_uri."&openid.spark.client_id=".$client_id."&openid.spark.combined_flow=true";
		header('Location: ' . $url);
		exit;
	}

	private function register_external_subscribers($agent_id=NULL, $access_token=NULL) { //Register the agents who purchase from our corp site to sparkstore 

		$url = "https://sparkapi.com/v1/appstore/users/".$agent_id;

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls',
			'Authorization: OAuth '.$access_token
		);		

		$post = array('D'=>array("Purchased"=>TRUE));

		$body = json_encode($post);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		return $result_info;

	}

	private function read_registered_external_subscribers($access_token=NULL) {

		$url = "https://sparkapi.com/v1/appstore/users/";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls',
		    'Authorization: OAuth '.$access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return $result_info;

	}

	public function check_user_dsl($user_id=NULL) {

		if($user_id) {

			$dsl = modules::load('dsl/dsl');

			$check_agent_dsl = "agent/".$user_id."/check";
			$result = $dsl->get_endpoint($check_agent_dsl);
			$chk_result = json_decode($result, true);

			if($chk_result["status"] == "success") {

				$body = array(
					"access_token" => $this->session->userdata("access_token"),
					"bearer_token" => ""
				);

				if(!$chk_result["in_agents"]) {
					$result = $dsl->post_endpoint("sync-new-agent", $body);
				}

				if(!$chk_result["has_listings"]) {
					$result = $dsl->post_endpoint("listings/new", $body);
				}

				if(!$chk_result["has_new_listings"]) {
					$result = $dsl->post_endpoint("listings/new", $body);
				}

				if(!$chk_result["has_nearby_listings"]) {

					$listings = $this->get_listing($this->session->userdata("access_token"));

					$latitude = '';
					$longitude = '';

					if(!empty($listings)) {
						foreach($listings as $listing) {
							if($listing["StandardFields"]["Latitude"] !='' && $listing["StandardFields"]["Longitude"] !='') {
								$latitude = $listing["StandardFields"]["Latitude"];
								$longitude = $listing["StandardFields"]["Longitude"];
								break;
							}
						}
					}

					if(!empty($latitude) && !empty($longitude)) {
						$endpoint = "listings/nearby?_lat=".$latitude."&_lon=".$longitude."&_expand=PrimaryPhoto";
						$result = $dsl->post_endpoint($endpoint, $body);
					}
				}

				if(!$chk_result["has_saved_searches"]) {
					$result = $dsl->post_endpoint("sync-saved-searches", $body);
				}

			}
		}
	}

	public function myaccount($access_token) {

		if($access_token) {
			$url = "https://sparkapi.com/v1/my/account";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls',
			    'Authorization: OAuth '. $access_token,
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			curl_close($ch);
			
			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
			//printA($result_info);
			return $result_info;
		}
	}

	public function get_listing($access_token) {

		if($access_token) {

			$obj = modules::load("dsl");

			$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold')";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$body = array("access_token"=>$access_token);

			$endpoint = "spark-listings?_limit=10&_pagination=1&_page=1&_filter=(".$filter_string.")&_expand=PrimaryPhoto";

			$results = $obj->post_endpoint($endpoint, $body);
			$return = json_decode($results, true);

			return (isset($return["data"])) ? $return["data"] : FALSE;
		}
	}

	public function send_notification_to_user_idx($user = array()) {

	 	$this->load->library("email");

	 	$user_id 		= $user['id'];
	 	$agent_id 		= $user['agent_id'];
		$email 			= $user['email'];
		$password 		= $user['password'];
		$first_name 	= $user['first_name'];
		$last_name 		= $user['last_name'];
		$sub_domain 	= $user['sub_domain'];
		$sub_domain_url = $user['sub_domain_url'];

		//Send Email Through Sendgrid
		$this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	$email, //recipient email
        	'alur@agentsquared.com,alvin@agentsquared.com,paul@agentsquared.com,raul@agentsquared.com', //bccs
        	'3a128de2-9f5e-4fa5-b1f9-b280077b6e4d', //template_id
        	$email_data = array(
        		'subs' => array(
    				'agent_domain' 	=> $sub_domain,
    				'first_name' 	=> $first_name,
    				'login_url' 	=> getenv('AGENT_DASHBOARD_URL').'login/auth/login',//'https://dashboard.agentsquared.com/login/auth/login',
    				'site_url'		=> $sub_domain_url,
    				'agent_email' 	=> $email,
    				'agent_password'=> $password
        		)
			)
        );

		//Add drip email subscribers to be used in campaign
		$data = array(
			'subscribers' => array(
				array(
					'email' 			=> $email,
					'time_zone' 		=> 'America/Los_Angeles',
					'custom_fields' 	=> array(
						'first_name'	=> $first_name,
						'last_name' 	=> $last_name,
						'domain' 		=> $sub_domain,
						'login_url' 	=> getenv('AGENT_DASHBOARD_URL').'login/auth/login',
						'redirect_url' 	=> getenv('AGENT_DASHBOARD_URL').'cron/redirect_to_pages?id='.$user_id,
						'password' 		=> $password,
						'site_url' 		=> $sub_domain_url,
						'user_id'		=> $user_id
					),
				)
			),
		);

		$this->email->send_drip_email($data); //Send Email through Drip

		return TRUE;

	}

	public function sync_data_view($user_id="", $agent_id="") {
		
		if(!empty($user_id) && !empty($agent_id)) {
			
			//$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');
			$data['title'] = "Sync MLS Data";
			$data['user_id'] = $user_id;

			$agent_ip_add = getenv('HTTP_X_FORWARDED_FOR');
			$data['ip_address'] = !empty($agent_ip_add) ? $agent_ip_add : $this->input->ip_address();
			
			$subscription = $this->agent_subscription($agent_id);

			$this->load->view('idx_login/sync_loader', $data);
			
		}else{
			echo "Error: Please contact agentsquared regarding this problem! support@agentsquared.com";
		}

	}

	public function resync_data_view($user_id="", $agent_id="") {

		if($this->input->get('user_id') && $this->input->get('agent_id')) {
			$user_id = $this->input->get('user_id');
			$agent_id = $this->input->get('agent_id');
		}

		if(!empty($user_id) && !empty($agent_id)) {

			$data['title'] = "Resync MLS Data";
			$data['user_id'] = $user_id;

			if(!$this->session->userdata('access_token')) {

				$acc_token = $this->idx_model->fetch_acc_token();

				if($acc_token) {
					$this->session->set_userdata("access_token", $acc_token);
				}

			}

			//Check if the agent already saved property datas in database
			$hasProperty = $this->pm->isHasProperties(array('user_id'=>$user_id));
			$hasAccountMeta = $this->pm->isHasAccountMeta(array('user_id'=>$user_id));

			$subscription = $this->agent_subscription($agent_id);

			if($this->input->get('reload')==1) {
				$data['title'] = "Sync MLS Data";
				$this->load->view('idx_login/reload_sync', $data);
			} else {
				if(!$hasProperty) {
					$this->save_mysql_properties($user_id, $agent_id); //Save homepage properties
				}
				if(!$hasAccountMeta) {
					$this->save_account_metas_segment($user_id, $agent_id); //Save Account Metas
				}
				$this->load->view('idx_login/resync', $data);
			}

		}
	}

	//{POST} /sync-new-agent (metas)
	public function sync_new_agent() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-new-agent";
		$body = array("access_token" => $this->session->userdata("access_token"), "bearer_token" => "");

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->session->userdata('user_id');

		do {//Syncing while account_status is still true. If false, it will exit and proceed to next syncing

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

			$account_status = $check['account_status'];

		} while($check['account_status'] == true);

		if(!$account_status) {
			$this->sync_new_listings($body);
		}

		exit(json_encode(array('success'=>TRUE)));

	}

	//{POST} /listings/new (reserve)
	public function sync_new_listings($body=array()) {

		$dsl = modules::load('dsl/dsl');
		$endpoint = "listings/new";

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->session->userdata('user_id');

		do {//Syncing while newlistings_status is still true. If false, it will exit and proceed to next syncing

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);
			$newlistings_status = $check['newlistings_status'];

		} while($check['newlistings_status'] == true);

		if(!$newlistings_status) {
			$this->sync_agent_listings($body);
		}

	}

	//{POST} /sync-new-agent-listing (property_listing_data)
	public function sync_agent_listings($body=array()) {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-new-agent-listings";

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->session->userdata('user_id');

		do {//Syncing while listings_status is still true. If false, it will exit and proceed to next syncing

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

			$listings_status = $check['listings_status'];

		} while($check['listings_status'] == true);

		if(!$listings_status) {
			$this->sync_nearby_listings($body);
		}

	}

	//{POST} /listings/nearby (4 nearby)
	public function sync_nearby_listings($body=array()) {

		$dsl = modules::load('dsl/dsl');

		$listings = $this->get_listing($this->session->userdata("access_token"));

		$latitude = '';
		$longitude = '';

		if(!empty($listings)) {

			foreach($listings as $listing) {

				if($listing["StandardFields"]["Latitude"] !='' && $listing["StandardFields"]["Longitude"] !='') {
					$latitude = $listing["StandardFields"]["Latitude"];
					$longitude = $listing["StandardFields"]["Longitude"];
					break;
				}

			}
		}

		if(!empty($latitude) && !empty($longitude)) {

			$endpoint = "listings/nearby?_lat=".$latitude."&_lon=".$longitude."&_expand=PrimaryPhoto";

			$result = $dsl->post_endpoint($endpoint, $body);

			$check_syncing = "sync-status/".$this->session->userdata('user_id');

			do {//Syncing while nearby_status is still true. If false, it will exit and proceed to next syncing

				$ch = $dsl->get_endpoint($check_syncing);
				$check = json_decode($ch, true);
				$nearby_status = $check['nearby_status'];

			} while($check['nearby_status'] == true);

			if(!$nearby_status) {
				$this->sync_saved_searches();
			}

		}

	}

	//{POST} /sync-saved-searches
	public function sync_saved_searches() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-saved-searches";

		$body = array("access_token" => $this->session->userdata("access_token"));

		$res = $dsl->post_endpoint($endpoint, $body);
		$result = json_decode($res, true);

		//Sync Agent contacts
		//$this->sync_agent_contacts();
		$this->migrate_search_fields();
	}

	//Save contact list to database
	public function sync_agent_contacts() {
		
		$this->load->library("idx_auth", $this->session->userdata("agent_id"));

		$contact = $this->idx_auth->GetContacts(1);

		if(isset($contact['pagination']['TotalPages']) && !empty($contact['pagination']['TotalPages'])) {

			//for($i=1; $i<=$contact['pagination']['TotalPages']; $i++) {
			for($i=1; $i<=4; $i++) {

				$contactsave = $this->idx_auth->GetContacts($i);

				if(!empty($contactsave['results'])) {

					foreach($contactsave['results'] as $contacts) {
						if(!$this->leads->is_contacts_exists($contacts)) {
							$this->leads->save_contacts($contacts);
						}
					}

				}
			}
		}

		$this->migrate_search_fields();
	}

	public function migrate_search_fields() {

		$user_id = $this->session->userdata('user_id');
		$agent_id = $this->session->userdata('agent_id');

		$mysql_properties = Modules::load("mysql_properties/mysql_properties");
		$system_info = $mysql_properties->get_account($user_id, "system_info");

		$isHere = $this->idx_model->search_fields_count($system_info->MlsId);

		if(count($isHere) == 0) {
			
			$call_url = 'https://sparkapi.com/v1/standardfields';

			$header = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls',
				'Authorization: OAuth '.$this->session->userdata('access_token')
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$content = curl_exec($ch);
			$json_data = json_decode($content);
			
			$standardfields = (isset($json_data->D->Results)) ? $json_data->D->Results: NULL;

			if(!empty($standardfields)) {
				foreach($standardfields[0] as $sf) {
					if($sf->Searchable) {
						$field = str_replace("/v1/standardfields/", "", $sf->ResourceUri);
						$this->register_field_v2($field);
					}
				}
			}
		}

		//$bench = $this->save_mysql_properties($user_id, $agent_id);
		//$this->save_account_metas_segment($user_id, $agent_id);

		exit(json_encode(array("success"=>TRUE)));

	}

	public function update_sync_status() {

		$info = $this->idx_model->fetch_user($this->session->userdata('user_id'), NULL);

		if($info) {

			$user = array(
				'id'		=> $info->id,
				'agent_id'	=> $info->agent_id,
				'email' 	=> $info->email,
				'first_name'=> $info->first_name,
				'last_name' => $info->last_name,
				'password' 	=> 'test1234'
			);

			$agent_name = $info->first_name."".$info->last_name;
			$clean_name = clean_url($agent_name); //removed unwanted characters
			$subdomain = strtolower($clean_name)."."."agentsquared.com";

			$isExist = $this->idx_model->check_domain_exist($subdomain); //Check if subdomain is unique

			if($isExist) { //subdomain already exist (same firstname and lastname)
				if(!empty($info->license_number)) {
					$clean_name = clean_url($agent_name.'-'.$info->license_number); //add license number after lastname
				} else {
					$clean_name = clean_url($agent_name.'-'.$info->id); //if license number is empty add userid after lastname
				}
				$subdomain = strtolower($clean_name).'.agentsquared.com';
			}

			$create_subdomain  = $this->add_sub_domain($subdomain); //Generate sub domain

			//added slack notif in agentdashboard for new signup
			$payload = "{
		      	'text': 'New Customer Signup',
				'attachments': [
					{
			          	'title': '".$subdomain."',
	          		  	'title_link': 'http://".$subdomain."',
			          	'text': 'Email: ".$user['email']."\n MLS: ".$info->mls_name."\n Date Created: ".date('Y-m-d H:i:s')."',
					}
			  	],
		   	}";
	
			$this->slack_webhook($payload);

			$subscription = $this->agent_subscription($this->session->userdata('agent_id'));

			if($subscription == "idx") {

				$user['sub_domain'] = $subdomain;
				$user['sub_domain_url'] = "http://".$subdomain;

				$this->send_notification_to_user_idx($user);

			}

			$success = $this->idx_model->update_sync_status($this->session->userdata('user_id'), array('sync_status' => 1));

			$response['success'] = ($success) ? TRUE : FALSE;

			if($this->session->userdata('app_type') == 'spw' || $subscription == 'spw') {

				$this->create_salesforce_lead(array(
						'app_type' => 'Single_Property_Website',
						'domain_url' => 'http://'.$subdomain,
						'sub_domain_url' => 'http://'.$subdomain
					)
				);

				$response['redirect'] = AGENT_DASHBOARD_URL.'single_property_listings';

			} else {

				$this->create_salesforce_lead(array(
						'app_type' => 'Agent_IDX_Website',
						'domain_url' => 'http://'.$subdomain,
						'sub_domain_url' => 'http://'.$subdomain
					)
				);

				$this->idx_model->save_agent_ip();

				//isFromSpark
				$launch = $this->idx_model->get_subscription([
					'UserId' => $this->session->userdata('agent_id'),
					'EventType' => 'purchased',
					'ApplicationType' => 'Agent_IDX_Website'
				]);

				if($this->session->userdata('landingpage')) {
					$response['redirect'] = AGENT_DASHBOARD_URL.'idx_login/free_trial_subscription?user_id='.$this->session->userdata('user_id');
				} else {
					$response['redirect'] = 'http://'.$subdomain.'?modal_welcome=true';
				}

			}

		}

		exit(json_encode($response));

	}

	/**
     * Display stripe free trial plan modal
     *
     * @param  Int $user_id
     * @return view | dashboard
     */
	public function free_trial_subscription() {

		$user_id = $this->input->get('user_id') ? $this->input->get('user_id') : $this->session->userdata('user_id');

		//Fetch receipt, users, domain data
		$data = $this->idx_model->get_trial_data($user_id);

		if($this->idx_model->isTrialUnique($user_id)) {
			$this->load->view('idx_login/free_trial', $data);
		} else { //If user has already stripe data in DB
			redirect('dashboard', 'refresh');
		}

	}

	private function create_salesforce_lead($data=array()) {

		if(!empty($data)) {

			$user_data = $this->idx_model->saleforce_pre_data($this->session->userdata('agent_id'));
			
			if(!empty($user_data)) {

				$this->load->library('salesforce');
				$this->load->library('Gohighlevel');

				$token_string = $this->session->userdata('code').' '.$this->session->userdata('agent_id').' '.$this->session->userdata('user_id');
				$token = base64_encode($token_string);

				$lead_data = [
					'Command_Center_Id__c' => $this->session->userdata('user_id'),
					'FirstName' => $user_data['user']['first_name'],
					'LastName' => $user_data['user']['last_name'],
					'Email' => $user_data['user']['email'],
					'Phone' => $user_data['user']['phone'],
					'Company' => $user_data['user']['company'],
					'Plan__c' => ($user_data['user']['plan_id']==1) ? 'FREEMIUM '.$user_data['user']['PricingPlan'] : (($user_data['user']['plan_id']==2) ? 'PREMIUM '.$user_data['user']['PricingPlan'] : '15 Days Trial'),
					'Product__c' => $data['app_type'],
					'Subscribed_Via__c' => ($user_data['user']['isFromSpark']) ? 'Spark' : 'Flex',
					'Token_Status__c' => 'LIVE',
					'MLS_Association__c' => $user_data['user']['mls_name'],
					'Registration_Date__c' => date('Y-m-d'),
					'Domain_URL__c' => $data['domain_url'],
					'Subdomain_URL__c' => $data['sub_domain_url'],
					'Total_Properties_Sold__c' => $user_data['listings_scale']['total_sold'],
					'Total_Active_Properties__c' => $user_data['listings_scale']['total_active'],
					'Recent_Active_Property_Date__c' => $user_data['listings_scale']['recent_active_date'],
					'Recent_Sold_Property_Date__c' => $user_data['listings_scale']['recent_sold_date'],
					'Access_Token__c' => $this->session->userdata('access_token'),
					'Agent_ID__c' => $this->session->userdata('agent_id'),
					'Paying_Customer__c' => 'No',
					'LeadSource' => 'AgentSquared Instant IDX Website',
					'Agent_Dashboard_Url__c' => AGENT_DASHBOARD_URL.'sales_auth/authenticate_saleforce?token='.$token
				];

				//.Insert lead data to salesforce
				$this->salesforce->create($lead_data, 'Lead');

				// Insert lead data to Gohighlevel CRM
				$ghl = new Gohighlevel;

				// additional fields for GHL
				$lead_data['Date_of_Upgrade__c'] = '';
				$lead_data['Cancellation_Date__c'] = '';
				$lead_data['Reason_for_Cancellation_from_Customer__c'] = '';

				$ghl->process($lead_data, 'lead');

			}

		}

		return TRUE;

	}

	private function get_agent_existing_website() {

		$data = $this->myaccount($this->session->userdata('access_token'));	

		if(!empty($data)) {
			if(isset($data['D']['Results'][0]['Websites'][0]['Uri']) && !empty($data['D']['Results'][0]['Websites'][0]['Uri'])) {
				return $data['D']['Results'][0]['Websites'][0]['Uri'];
			}
		}

		return FALSE;

	}

	private function add_sub_domain($domain=NULL) {

		if($domain) {

			$post = array(
				'agent' 			=> $this->session->userdata("user_id"),
				'domains'			=> $domain,
				'status' 			=> "completed",
				'type' 				=> "agent_site_domain",
				'date_created' 		=> date("Y-m-d h:m:s"),
				'existing_domain' 	=> 0,
				'is_subdomain' 		=> 1,
				'subdomain_url' 	=> "http://".$domain,
			);

			$result = $this->reg_model->add_subdomain($post);

			if($result) {
				if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))) { // Check if localhost
					//$return = Modules::run('custom_domain/create_as_domain', $domain);
					$return = Modules::run('custom_domain/aws_domain/create_aws_subdomain', $domain);
				}
				return TRUE; 
			}
		}

	}

	public function save_mysql_properties($user_id=NULL, $agent_id=NULL) {

		if($user_id && $agent_id) {

			//$this->benchmark->mark('dog');

			$this->load->library("idx_auth", $agent_id);

			$active_listings = Modules::run('mysql_properties/get_my_listings', $agent_id);
			//$office_listings = Modules::run('mysql_properties/get_office_listings', $agent_id);
			//$new_listings = Modules::run('mysql_properties/get_new_listings', $agent_id);

			if(!empty($active_listings)) {
				$this->save_listings($user_id, $active_listings, "active");
			} else{
				$this->delete_listings($user_id, "active");
			}

			// if(!empty($office_listings)) {
			// 	$this->save_listings($user_id, $office_listings, "office");
			// } else{
			// 	$this->delete_listings($user_id, "office");
			// }

			// if(!empty($new_listings)) {
			// 	$this->save_listings($user_id, $new_listings, "new");
			// } else{
			// 	$this->delete_listings($user_id, "new");
			// }
			//$this->benchmark->mark('cat');
		}

		return TRUE;
		//return array('usage'=>$this->benchmark->memory_usage(), 'time' => $this->benchmark->elapsed_time('dog', 'cat'));
		//return $arrBench;
	}

	public function save_mysql_properties2($user_id=NULL, $agent_id=NULL) {

		if($user_id && $agent_id) {

			//$this->benchmark->mark('dog');

			$this->load->library("idx_auth", $agent_id);

			$sold_listings = Modules::run('mysql_properties/get_sold_listings', $agent_id);
			$nearby_listings = Modules::run('mysql_properties/get_nearby_listings',$user_id, $agent_id);

			if(!empty($sold_listings)) {
				$this->save_listings($user_id, $sold_listings, "sold");
			} else{
				$this->delete_listings($user_id, "sold");
			}

			if(!empty($nearby_listings)) {
				$this->save_listings($user_id, $nearby_listings, "nearby");
			} else{
				$this->delete_listings($user_id, "nearby");
			}

			//$this->benchmark->mark('cat');

		}

		//return array('usage'=>$this->benchmark->memory_usage(), 'time' => $this->benchmark->elapsed_time('dog', 'cat'));

		return TRUE;
	}

	public function save_listings($user_id, $listings, $type) {

		$listings_data = $listings;

		for ($x = 0; $x <= count($listings_data); $x++) {

			$count = 0;

		   	foreach($listings_data as $listing) {

		   		$listing_id = ($type == "active" || $type == "sold") ? $listing->Id : $listing->StandardFields->ListingKey;

			   	$countType = ($type == "sold") ? "8" : "4";

			   	if($count < $countType) {

					$data = array(
						'user_id'		=> $user_id,
						'listing_id' 	=> $listing_id,
						'listing' 		=> json_encode($listing),
						'type' 			=> $type,
						'data_type' 	=> '1',
						'date_created' 	=> date('Y-m-d H:i:s')
					);

					$this->pm->insert_homepage_listings($data);

				} $count++;
			}

		} 

		return TRUE;

	}

	public function delete_listings($user_id,$type) {

		$data = array(
			'user_id'	=> $user_id,
			'type' 		=> $type,
		);

		$this->pm->delete_listings($data);

		return TRUE;

	}

	public function save_account_metas_segment($user_id = "", $agent_id="") {

		if($user_id && $agent_id) {

			$account_info = Modules::run('mysql_properties/get_account_info', $agent_id);
			$system_info = Modules::run('mysql_properties/get_system_info', $agent_id);

			if(!empty($account_info)) {
				$this->compress_metas($user_id, $account_info, "account_info");
			}

			if(!empty($system_info)) {
				$this->compress_metas($user_id, $system_info, "system_info");
			}
		}

		return TRUE;

	}

	//this is for DSL updater to DB
	public function save_account_metas() {

		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];

		if($user_id && $agent_id) {

			$account_info = Modules::run('mysql_properties/get_account_info', $agent_id);
			$system_info = Modules::run('mysql_properties/get_system_info', $agent_id);

			if(!empty($account_info)) {
				$this->compress_metas($user_id, $account_info, "account_info");
			}

			if(!empty($system_info)) {
				$this->compress_metas($user_id, $system_info, "system_info");
			}
		}

	}

	public function compress_metas($user_id, $meta, $type) {

		$json = json_encode($meta);
		$json_meta = gzencode($json, 9);

		$data = array(
			'user_id' => $user_id,
			'account' => $json_meta,
			'type' => $type,
			'date_created' => date('Y-m-d H:i:s')
		);

		$this->pm->insert_account($data);

	}

	public function register_field_v2($field_name=NULL) {

		if($field_name) {

			$field_type = 'standardfields';

			/* Make API Call here */
			$dsl = modules::load("dsl");

			$access_token = array(
				'access_token' 	=> $this->session->userdata('access_token'),
				'field_name'	=> $field_name,
				'field_type'	=> $field_type
			);

			$endpoint = "spark-fields";
			$field = $dsl->post_endpoint($endpoint, $access_token);
			$field = json_decode($field);

			$endpoint = "agent/".$this->session->userdata('user_id')."/account-info";
			$agent = $dsl->get_endpoint($endpoint);

			$agentDataObj = json_decode($agent);
			$fieldItemName = urldecode($field_name);

			if(isset($field->status) && ($field->status == 200)) {
				$fieldData = $field->data;
				if($fieldData) {
					if(isset($fieldData[0]) && !empty($fieldData[0])) { // Check if field has options
						if($fieldData[0]->{"{$fieldItemName}"}->Searchable) {
							/* Save field to database */
							$data = array(
								'field_name' 		=> urldecode($field_name),
								'field_datatype' 	=> isset($fieldData[0]->{"{$fieldItemName}"}->Type) ? $fieldData[0]->{"{$fieldItemName}"}->Type : NULL,
								'field_result_json' => json_encode($fieldData[0]),
								'field_haslist'		=> isset($fieldData[0]->{"{$fieldItemName}"}->HasList) ? $fieldData[0]->{"{$fieldItemName}"}->HasList : NULL,
								'field_category'	=> isset($fieldData[0]->{"{$fieldItemName}"}->FieldCategory) ? $fieldData[0]->{"{$fieldItemName}"}->FieldCategory : "",
								'field_label'		=> isset($fieldData[0]->{"{$fieldItemName}"}->Label) ? $fieldData[0]->{"{$fieldItemName}"}->Label : NULL,
								'field_max_size'	=> isset($fieldData[0]->{"{$fieldItemName}"}->MaxListSize) ? $fieldData[0]->{"{$fieldItemName}"}->MaxListSize : NULL,
								'field_multiselect'	=> isset($fieldData[0]->{"{$fieldItemName}"}->MultiSelect) ? $fieldData[0]->{"{$fieldItemName}"}->MultiSelect : NULL,
								'field_type'		=> $field_type,
								'mls_id'			=> isset($agentDataObj->MlsId) ? $agentDataObj->MlsId : NULL
							);

							$dataInserted = $this->idx_model->register_search_fields($data);

							if($dataInserted!=false) {
								/*Register Fields List Items*/
								if(isset($fieldData[0]->{"{$fieldItemName}"}->FieldList)) {
									foreach($fieldData[0]->{"{$fieldItemName}"}->FieldList as $flItems) {
										$dataFields = array(
											'mls_searc_field_id' 	=> $dataInserted,
											'field_name' 			=> $flItems->Name,
											'field_value' 			=> $flItems->Value,
											'field_applies_to_json' => json_encode($flItems->AppliesTo),
										);
										$this->idx_model->register_mls_data_search_fields($dataFields);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public function agent_subscription($agent_id) {

		$ret = FALSE;

		if($agent_id) {

			$arr = array(
				'UserId' 			=> $agent_id,
				'EventType' 		=> 'purchased',
				'ApplicationType' 	=> 'Agent_IDX_Website'
			);

			$idx = $this->idx_model->fetch_agent_subscription($arr);

			if(!empty($idx)) {

				$ret = "idx";

			} else {

				$arr1 = array(
					'UserId' 			=> $agent_id,
					'EventType' 		=> 'purchased',
					'ApplicationType' 	=> 'Single_Property_Website'
				);

				$spw = $this->idx_model->fetch_agent_subscription($arr1);

				if(!empty($spw)) {
					$ret = "spw";
				}
			}

		}

		return $ret;
	}

	public function save_spark_tokens($user_id, $agent_id, $access_token, $refresh_token) {

		//Check token in DB
		$is_agent_exist = $this->idx_model->fetch_agent_token($agent_id);
		$agent_result = $this->idx_model->fetch_user(NULL, $agent_id);
		
		if($is_agent_exist) {//If agent already exist, update the token

			//update token info
			$token_info = array(	
				'access_token' 			=> $access_token,
				'refresh_token' 		=> $refresh_token,
				'date_modified' 		=> date("Y-m-d h:m:s"),
				'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours')),
				'is_valid' 				=> 1,
				'counter' 				=> 0,
				//'oauth_key' 			=> ($agent_result->mls_id=='20070913202326493241000000') ? 'armls' : 'default'
			);

			$this->idx_model->update_token($agent_id, $token_info);

		} else {//Save the token

			//insert agent info
			$agent_info = array( 
				'agent_id' 				=> $agent_id,
				'access_token' 			=> $access_token,
				'refresh_token' 		=> $refresh_token,
				'date_created' 			=> date("Y-m-d h:m:s"),
				'date_modified' 		=> date("Y-m-d h:m:s"),
				'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours')),
				'is_valid' 				=> 1,
				'counter' 				=> 0,
				'oauth_key' 			=> ($agent_result->mls_id=='20070913202326493241000000') ? 'armls' : 'default'
			);

			$this->idx_model->save_token($agent_info);
		}
		
		$obj = modules::load("dsl");

		$body = array(
    		'system_user_id'		=> (int)$user_id,
			'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours'))
		);

		$endpoint = "agent_idx_token";

		$ret = $obj->post_endpoint($endpoint, $body);

	}

	public function update_spark_token($user_id, $agent_id, $access_token, $refresh_token) {

		//Check token in DB
		$is_agent_exist = $this->idx_model->fetch_agent_token($agent_id);
		$agent_result = $this->idx_model->fetch_user(NULL, $agent_id);

		if($is_agent_exist) {//If agent already exist, update the token

			//update token info
			$token_info = array(	
				'access_token' 			=> $access_token,
				'refresh_token' 		=> $refresh_token,
				'date_modified' 		=> date("Y-m-d h:m:s"),
				'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours')),
				'is_valid' 				=> 1,
				'counter' 				=> 0,
				'oauth_key' 			=> ($agent_result->mls_id=='20070913202326493241000000') ? 'armls' : 'default'//'default',
			);

			$this->idx_model->update_token($agent_id, $token_info);

		} else {//Save the token

			//insert agent info
			$agent_info = array( 
				'agent_id' 				=> $agent_id,
				'access_token' 			=> $access_token,
				'refresh_token' 		=> $refresh_token,
				'date_created' 			=> date("Y-m-d h:m:s"),
				'date_modified' 		=> date("Y-m-d h:m:s"),
				'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours')),
				'is_valid' 				=> 1,
				'counter' 				=> 0,
				'oauth_key' 			=> ($agent_result->mls_id=='20070913202326493241000000') ? 'armls' : 'default'
			);

			$this->idx_model->save_token($agent_info);
		}

		$obj = modules::load("dsl");

		$body = array(
    		'system_user_id'		=> (int)$user_id,
			'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);

		$endpoint = "agent_idx_token/".$agent_id."/update";

		$ret = $obj->put_endpoint($endpoint, $body);

		return TRUE;
	}

	public function set_agent_tokens() {

		$this->session->set_userdata("access_token", $this->input->get("access_token"));
		$this->session->set_userdata("refresh_token", $this->input->get("refresh_token"));
		//$this->session->set_userdata("user_id", $this->input->get("user_id"));
		printA($this->session->all_userdata());

	}

	public function add_mls_name() {

		$ret = FALSE;

		$data=array();

		$agents = $this->idx_model->get_agents();

		if($agents) {

			$count=0;

			foreach($agents as $key) {

				$account = $this->myaccount($key->access_token);

				if(!empty($account)) {

					$user = array(
						'mls_id' => (isset($account["D"]["Results"][0]["MlsId"]) && !empty($account["D"]["Results"][0]["MlsId"])) ? $account["D"]["Results"][0]["MlsId"] : "",
						'mls_name' => (isset($account["D"]["Results"][0]["Mls"]) && !empty($account["D"]["Results"][0]["Mls"])) ? $account["D"]["Results"][0]["Mls"] : ""
					);

					$update = $this->idx_model->update_sync_status($key->id, $user);

					if($update) {

						$data[$count]['user_id'] = $key->id;
						$data[$count]['mls_id'] = $user['mls_id'];
						$data[$count]['mls_name'] = $user['mls_name'];

						$count++;
					}

				}

			}

		}

		printA($data);
	}

	public function verify_new_email($param_string) {

		$params = urldecode($param_string)."<br><br>";

		$arr = explode("/", $params);

		$data = array(
			'user_id' 		=> $arr[0],
			'email' 		=> $arr[1],
			'agent_id' 		=> $arr[2],
			'code' 			=> $arr[3],
			'old_last_login' => $arr[4]
		);

		$this->session->set_userdata($data);

		redirect('dashboard', 'refresh');

	}

	public function sync_agent_listings_bearerToken() {

		$bearer_token = $_GET["bearer_token"];

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-new-agent-listings";
		$body = array("access_token" => "", "bearer_token" => $bearer_token);

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->session->userdata('user_id');

		do {//Syncing while account_status is still true. If false, it will exit and proceed to next syncing

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

			$account_status = $check['account_status'];

		} while($check['account_status'] == true);

		if(!$account_status) {
			$this->sync_new_listings($body);
		}

		exit(json_encode(array('success'=>TRUE)));

	}
	
	public function new_sync_agent(){
		
		$endpoint = "sync-new-agent";
		$body = array("access_token" => $this->session->userdata("access_token"), "bearer_token" => "","system_user_id" => $this->session->userdata('user_id') );
		
		$result = $this->sync_post_endpoint($endpoint, $body);

		$this->new_sync_saved_searches();
		exit(json_encode(array('success'=>TRUE)));
	}

	//{POST} /sync-saved-searches
	public function new_sync_saved_searches() {
		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-saved-searches";
		$body = array("access_token" => $this->session->userdata("access_token"),"system_user_id" => $this->session->userdata('user_id'));

		$res = $this->sync_post_endpoint($endpoint, $body);
		$result = json_decode($res, true);

		$endpoint2 = "sync-new-agent-listings";
		$body2 = array("access_token" => $this->session->userdata("access_token"), "bearer_token" => "");

		$result2 = $dsl->post_endpoint($endpoint2, $body2);

		// $endpoint2 = "my-listings";
		// $body2 = array("access_token" => $this->session->userdata("access_token"),"system_user_id" => $this->session->userdata('user_id'));

		// $res2 = $this->sync_post_endpoint($endpoint2, $body2);
		// $result2 = json_decode($res2, true);

		//Sync Agent contacts
		$this->migrate_search_fields();
		
		//$this->sync_agent_contacts();

	}

	/* Returns JSON String of Agent Information */
 	public function sync_get_endpoint($endpoint = NULL) {

 		if($endpoint) {
 			$call_url = rtrim(getenv('AS_SHAPER_SERVER_URL'), '/')."/".$endpoint; 
 			//$call_url = "172.18.0.1:3000/".$endpoint;
	 		$ch = curl_init();
	 		curl_setopt($ch, CURLOPT_URL, $call_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic YWdlbnRzcXVhcmVkOjhjMTViYjAwNjYzYTRjdHkxMjcyODY1MzBjZmVlYzAyYTM=', 'Content-Type:application/json'));
			$content = curl_exec($ch);
			
			return $content;
 		}
 		else {
 			echo 0;
 		}
 	}

 	public function sync_post_endpoint($endpoint = NULL, $body=array()) {

 		if($endpoint) {

 			$call_url = rtrim(getenv('AS_SHAPER_SERVER_URL'), '/')."/".$endpoint;
 			//$call_url = "172.18.0.1:3000/".$endpoint;
 			$headers = array(
	    		'Content-Type: application/json',
	    		'Authorization: Basic YWdlbnRzcXVhcmVkOjhjMTViYjAwNjYzYTRjdHkxMjcyODY1MzBjZmVlYzAyYTM='
	    	);

 			$body_json = json_encode($body, true);
 			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$result_info  = curl_exec($ch);
			//$result_info = json_decode($result, true);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}

			return $result_info;
 		}
 	}

 	public function slack_webhook($payload = NULL) {

		$url = "https://hooks.slack.com/services/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";
		
		$ch = curl_init( $url );
		# Setup request to send json via POST.
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		# Send request.
		$result = curl_exec($ch);
		curl_close($ch);

	}

	public function agent_listings_updater($data=array()) {

		if(!empty($data)) {

			$call_url= getenv('AGENT_SYNCER_URL').'onboard-agent-listings-updater/'.$data['user_id'].'/'.$data['agent_id'].'/onboarding?access_token='.$data['access_token'];

			$headers = array('Content-Type: application/json');

			$body=array();
			$body_json = json_encode($body, true);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$result_info  = curl_exec($ch);

			return TRUE;

		}

		return FALSE;

	}

}