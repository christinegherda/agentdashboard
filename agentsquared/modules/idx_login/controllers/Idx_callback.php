<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Idx_callback extends MX_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model("idx_model");
        //$this->output->enable_profiler(TRUE);
	}

	public function index() {
		
		if($this->input->get('openid_spark_code')) { // We've been authorized by the end user.

			$this->session->set_userdata("openid_spark_code", $this->input->get('openid_spark_code'));

		    $tokens = $this->authorization($this->input->get('openid_spark_code'));

		    if($tokens) {

		    	$agent_id = ($this->input->get('openid_sreg_nickname')) ? $this->input->get('openid_sreg_nickname') : ''; //openid_sreg_nickname is equivalent to agent_id

		    	if(!empty($agent_id)) {

		    		$is_agent_exist = $this->idx_model->fetch_agent_token($agent_id);

					if($is_agent_exist) {//If agent already exist, update the token

						//update token info
						$token_info = array(	
							'access_token' 			=> $tokens['access_token'],
							'refresh_token' 		=> $tokens['refresh_token'],
							'date_modified' 		=> date("Y-m-d h:m:s"),
							'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours')),
							'is_valid' 				=> 1,
							'counter' 				=> 0,
							'oauth_key' 			=> 'default',
						);

						$this->idx_model->update_token($agent_id, $token_info);

					} else {//Save the token

						//insert agent info
						$agent_info = array( 
							'agent_id' 				=> $agent_id,
							'access_token' 			=> $tokens['access_token'],
							'refresh_token' 		=> $tokens['refresh_token'],
							'date_created' 			=> date("Y-m-d h:m:s"),
							'date_modified' 		=> date("Y-m-d h:m:s"),
							'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours')),
							'is_valid' 				=> 1,
							'counter' 				=> 0,
							'oauth_key' 			=> 'default',
						);

						$this->idx_model->save_token($agent_info);
					}

					//Set tokens as sessions
					$session['access_token'] = $tokens['access_token'];
					$session['refresh_token'] = $tokens['refresh_token'];
					$this->session->set_userdata($session);

					if($this->session->userdata('from_website')) {
						//redirect(getenv('AGENT_DASHBOARD_URL').'idx_login?from=website&pricing='.$this->session->userdata('pricing').'&app_type='.$this->session->userdata('app_type').'&price='.$this->session->userdata('price').'&plan_id='.$this->session->userdata('plan_id'), 'refresh');
						header('Location: '. getenv('AGENT_DASHBOARD_URL').'idx_login?from=website&pricing='.$this->session->userdata('pricing').'&app_type='.$this->session->userdata('app_type').'&price='.$this->session->userdata('price').'&plan_id='.$this->session->userdata('plan_id'));
					} elseif($this->session->userdata('landingpage')) {
						header('Location: '. getenv('AGENT_DASHBOARD_URL').'idx_login?from=landingpage&pricing='.$this->session->userdata('pricing').'&app_type='.$this->session->userdata('app_type').'&price='.$this->session->userdata('price').'&plan_id='.$this->session->userdata('plan_id'));
					} else {
						header('Location: '. getenv('AGENT_DASHBOARD_URL').'idx_login?from=spark');
						//redirect(getenv('AGENT_DASHBOARD_URL').'idx_login?from=spark', 'refresh');
					}

		    	} else {
		    		die('We\'re sorry, but we can\'t get your account information');
		    	}
		    } else {
		     	die("We're sorry, but the Grant request failed!");
		    }
	    } elseif($this->input->get('openid_mode') == 'error') {
	        die("Please verify that redirect uri set in the appstore matches exactly the redirect URI on record for your key.");
	    } else {
	    	die("No <b><i>openid_spark_code</i></b> found!");
	    }
	}

	public function authorization($code=NULL) {

		$result_info = FALSE;

		if($code) {
			
			$client_id = getenv('SPARK_CLIENT_ID');
			$client_secret = getenv('SPARK_CLIENT_SECRET');
			$redirect_uri = getenv('AGENT_DASHBOARD_URL')."idx_login/idx_callback";

			$url = "https://sparkapi.com/v1/oauth2/grant";

			$post = array(
				'client_id' 	=> $client_id,
				'client_secret' => $client_secret,
				'grant_type'	=> 'authorization_code',
				'code'			=> $code,
				'redirect_uri' 	=> $redirect_uri,
			);

			$headers = ['X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls'];

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);
		}

		return $result_info;
	}

	public function myaccount($tokens) {

		if(isset($tokens["access_token"]) && !empty($tokens["access_token"])) {

			$url = "https://sparkapi.com/v1/my/account";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls',
			    'Authorization: OAuth '. $tokens['access_token'],
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}

			return $result_info;

		} else {
			die("Spark did not return access token");
		}
	}


	public function armls() {

		if($this->input->get('openid_spark_code')) { // We've been authorized by the end user.

			$this->session->set_userdata("openid_spark_code", $this->input->get('openid_spark_code'));

		    $tokens = $this->armls_authorization($this->input->get('openid_spark_code'));

		    if($tokens) {

		    	$agent_id = ($this->input->get('openid_sreg_nickname')) ? $this->input->get('openid_sreg_nickname') : ''; //openid_sreg_nickname is equivalent to agent_id

		    	if(!empty($agent_id)) {

		    		$is_agent_exist = $this->idx_model->fetch_agent_token($agent_id);

					if($is_agent_exist) {//If agent already exist, update the token

						//update token info
						$token_info = array(	
							'access_token' 			=> $tokens['access_token'],
							'refresh_token' 		=> $tokens['refresh_token'],
							'date_modified' 		=> date("Y-m-d h:m:s"),
							'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours')),
							'is_valid' 				=> 1,
							'counter' 				=> 0,
							'oauth_key' 			=> 'armls',
						);

						$this->idx_model->update_token($agent_id, $token_info);

					} else {//Save the token

						//insert agent info
						$agent_info = array( 
							'agent_id' 				=> $agent_id,
							'access_token' 			=> $tokens['access_token'],
							'refresh_token' 		=> $tokens['refresh_token'],
							'date_created' 			=> date("Y-m-d h:m:s"),
							'date_modified' 		=> date("Y-m-d h:m:s"),
							'expected_token_expired'=> date("Y-m-d h:m:s", strtotime('+24 hours')),
							'is_valid' 				=> 1,
							'counter' 				=> 0,					
							'oauth_key' 			=> 'armls',
						);

						$this->idx_model->save_token($agent_info);
					}

					//Set tokens as sessions
					$session['access_token'] = $tokens['access_token'];
					$session['refresh_token'] = $tokens['refresh_token'];
					$this->session->set_userdata($session);

					redirect(getenv('AGENT_DASHBOARD_URL')."idx_login?from=armls", "refresh");

		    	}

		    } else {
		      die("We're sorry, but the Grant request failed!");
		    }
	    } elseif($this->input->get('openid_mode') == 'error') {
	        die("Please verify that redirect uri set in the appstore matches exactly the redirect URI on record for your key.");
	    } else {
	    	die("No <b><i>openid_spark_code</i></b> found!");
	    }
	}

	public function armls_authorization($code=NULL) {

		$result_info = FALSE;

		if($code) {
			
			$client_id = 'c417twiz2tfscsj1f73e2zg7y';//getenv('SPARK_CLIENT_ID');
			$client_secret = '81e57w3hijt0howtpk5gjath3';//getenv('SPARK_CLIENT_SECRET');
			$redirect_uri = getenv('AGENT_DASHBOARD_URL')."idx_login/idx_callback/armls";

			$url = "https://sparkapi.com/v1/oauth2/grant";

			$post = array(
				'client_id' 	=> $client_id,
				'client_secret' => $client_secret,
				'grant_type'	=> 'authorization_code',
				'code'			=> $code,
				'redirect_uri' 	=> $redirect_uri,
			);

			$headers = ['X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0'];

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);
		}

		return $result_info;
	}

	public function debugger() {

		echo "SERVER : <br>";
		printA($_SERVER);
		echo "<br>";

		echo "SESSIONS : <br>";
		printA($this->session->all_userdata());
		echo "<br>";

		$code = $this->session->userdata("openid_spark_code");
		$tokens = $this->authorization($code);

		echo "TOKENS : <br>";
		printA($tokens);
		echo "<br>";

		$myaccount = $this->myaccount($tokens);
		echo "Account Information: ";
		printA($myaccount);

	}
}
