<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');
			if(!check_agent_ip_cookies())

		if(empty(config_item("is_paid"))) {
			$this->db->select("rid")->from("receipt")->where("agent_id", $this->session->userdata('user_id'));
			$receipt = $this->db->get()->row();

			if($receipt) {
				$this->config->set_item('is_paid', TRUE);
			} else {
				$this->config->set_item('is_paid', FALSE);
			}
		}

		$this->load->model( array("agent_sites/Agent_sites_model", "featured_listings/featured_listings_model" => "featured", "Dashboard_model" => "dashboard" ,"home/home_model" => "home_model","single_property_listings/Single_property_listings_model" => "single_property") );
		$this->message_start_delimiter = $this->config->item('message_start_delimiter', 'ion_auth');
	}

	public function countProperty( $countProperty = "" ){
		$returnArr = array(
			"countPending1" => 0,
			"countPending2" => 0,
			"countPending3" => 0,
			"countPending4" => 0,
			"countPending5" => 0,
			"countPending6" => 0,
			"countPending7" => 0
			);

		if(!empty($countProperty)){

			foreach ($countProperty as $property => $value) {
				$pDate = date("Y", strtotime($value->date_created));
				if(date("Y") == $pDate){
					$returnArr["countPending1"]++;
		 		}elseif(date("Y",strtotime("+1 year")) == $pDate){
		 			$returnArr["countPending2"]++;
				}elseif(date("Y",strtotime("-1 year")) == $pDate){
					$returnArr["countPending3"]++;
				}elseif(date("Y",strtotime("-2 year")) == $pDate){
					$returnArr["countPending4"]++;
				}elseif(date("Y",strtotime("-3 year")) == $pDate){
					$returnArr["countPending5"]++;
				}elseif(date("Y",strtotime("-4 year")) == $pDate){
					$returnArr["countPending6"]++;
				}elseif(date("Y",strtotime("-5 year")) == $pDate){
					$returnArr["countPending7"]++;
				}
			}
		}

		return $returnArr;
	}

	public function index() {
		
		$this->load->model('agent_model');

		$data['require_cc_update'] = $this->agent_model->isRequireUpdateCC( $this->session->userdata('user_id'));

        if( $data['require_cc_update'] && false === isset($_GET['updatecc']) )
        {
            // redirect user to dashboard with modal popped up.
            redirect('/dashboard?updatecc=1');
        }

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data["mls_name"] = $siteInfo['account_info']->Mls;

	 	$data["branding"] = $this->Agent_sites_model->get_branding_infos();
	 	$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		
		if(isset($data["domain_info"][0]->status) && isset($data["domain_info"][0]->is_subdomain)) {
			if($data["domain_info"][0]->status == "completed" || $data["domain_info"][0]->is_subdomain == "1") {
				$this->config->set_item('is_domain_completed', TRUE);
			}
		}
		$data['title'] = "Dashboard";

		$propertyPending = Modules::run('dsl/getAgentActivePropertyListings','Pending');
		$propertyActive = Modules::run('dsl/getAgentActivePropertyListings','Active,Contingent');
		$propertyClosed = Modules::run('dsl/getAgentActivePropertyListings','Closed,Sold');


		$countPropPending = $this->countProperty($propertyPending);
		$countPropActive = $this->countProperty($propertyActive);
		$countPropClosed = $this->countProperty($propertyClosed);

		$spw_created = $this->Agent_sites_model->get_your_properties_listings(FALSE, array('mlsID'=>$this->session->userdata('agent_id')));
		$propertyActiveFromSpark = Modules::run('single_property_listings/getallActiveProperties');
		//$isToBeCreated = (count($propertyActive) - count($spw_created));

		//count sites to be created
		$total_spw = array();
		if(!empty($propertyActiveFromSpark)) {
              foreach($propertyActiveFromSpark as $active_prop) {
                foreach($spw_created as $spw) {
                  if($active_prop->Id == $spw->property_id) {
                  	$total_spw[] = $active_prop->Id;
                   }
               }
           }
        }

		$count_active = !empty($propertyActiveFromSpark) ? count($propertyActiveFromSpark) : 0;
        $count_spw = !empty($total_spw) ? count($total_spw) : 0;
        $isToBeCreated = ($count_active - $count_spw);

		$data["countNewContacts"] = $this->dashboard->get_new_contacts(TRUE);
		$data["countAllContacts"] = $this->dashboard->get_new_contacts(FALSE, TRUE);
		$data["new_contact_list"] = $this->dashboard->get_new_contacts();

		$newLeadRecords = $this->dashboard->get_month_records();
		//$newProperyRecords = $this->dashboard->get_day_records_property();

		$leads_chart_per_month = array(
			array("period" => date("Y-m",strtotime("-6 month")), "a_leads" => $newLeadRecords["day7"]->countLead, "b_leads" => $newLeadRecords["day7"]->countShowing),
			array("period" => date("Y-m",strtotime("-5 month")), "a_leads" => $newLeadRecords["day6"]->countLead, "b_leads" => $newLeadRecords["day6"]->countShowing),
			array("period" => date("Y-m",strtotime("-4 month")), "a_leads" => $newLeadRecords["day5"]->countLead, "b_leads" => $newLeadRecords["day5"]->countShowing),
			array("period" => date("Y-m",strtotime("-3 month")), "a_leads" => $newLeadRecords["day4"]->countLead, "b_leads" => $newLeadRecords["day4"]->countShowing),
			array("period" => date("Y-m",strtotime("-2 month")), "a_leads" => $newLeadRecords["day3"]->countLead, "b_leads" => $newLeadRecords["day3"]->countShowing),
			array("period" => date("Y-m",strtotime("-1 month")), "a_leads" => $newLeadRecords["day2"]->countLead, "b_leads" => $newLeadRecords["day2"]->countShowing),
			array("period" => date("Y-m"), "a_leads" => $newLeadRecords["day1"]->countLead, "b_leads" => $newLeadRecords["day1"]->countShowing)
		);

		$property_chart = array(
			array("period" => date("Y",strtotime("-5 year")), "a_leads" => $countPropActive["countPending7"], "b_leads" => $countPropClosed["countPending7"],"c_leads" => $countPropPending["countPending7"]),
			array("period" => date("Y",strtotime("-4 year")), "a_leads" => $countPropActive["countPending6"], "b_leads" => $countPropClosed["countPending6"],"c_leads" => $countPropPending["countPending6"]),
			array("period" => date("Y",strtotime("-3 year")), "a_leads" => $countPropActive["countPending5"], "b_leads" => $countPropClosed["countPending5"],"c_leads" => $countPropPending["countPending5"]),
			array("period" => date("Y",strtotime("-2 year")), "a_leads" => $countPropActive["countPending4"], "b_leads" => $countPropClosed["countPending4"],"c_leads" => $countPropPending["countPending4"]),
			array("period" => date("Y",strtotime("-1 year")), "a_leads" => $countPropActive["countPending3"], "b_leads" => $countPropClosed["countPending3"],"c_leads" => $countPropPending["countPending3"]),
			array("period" => date("Y"), "a_leads" => $countPropActive["countPending1"], "b_leads" => $countPropClosed["countPending1"] ,"c_leads" => $countPropPending["countPending1"]),
			//array("period" => date("Y",strtotime("+1 year")), "a_leads" => $countPropActive["countPending2"], "b_leads" => $countPropClosed["countPending2"] ,"c_leads" => $countPropPending["countPending2"])
		);

		//echo date("M",strtotime("- month")); exit;
		$chart_days = array(
			date( 'Y-m-d', time()-(6*86400) ),
			date( 'Y-m-d', time()-(5*86400) ),
			date( 'Y-m-d', time()-(4*86400) ),
			date( 'Y-m-d', time()-(3*86400) ),
			date( 'Y-m-d', time()-(2*86400) ),
			date( 'Y-m-d', time()-86400 ),
			date( 'Y-m-d' )
		);

		$data["leads_chart"] = json_encode($leads_chart_per_month);
		$data["chart_days"] = json_encode($chart_days);
		$data["property_chart"] = json_encode($property_chart);
		$data["siteToBeCreated"] = !empty($isToBeCreated) ? $isToBeCreated : 0;
		$data["js"] = array("dashboard.js");

		//Set config of trial info
		$trial_info = $this->dashboard->is_trial_version_end();

		if($trial_info['trial']) {

			$this->config->set_item('trial', TRUE);
			$this->config->set_item('left', $trial_info['left']);
			$this->config->set_item('pricing', $trial_info['pricing']);
			$this->config->set_item('price', $trial_info['price']);
		}

        $data['strip_cust_id'] = $this->agent_model->getStripeCustID($this->session->userdata('user_id'));

        // only show update cc modal for customers with stripe id
        if( $data['strip_cust_id'] !== false || (isset($_GET['updatecc']) && (bool)$_GET['updatecc'] === true) )
        {
            add_jsvar('stripe_cust_id', $data['strip_cust_id'], 'string', true);
            add_jsvar('STRIPE_PUB_KEY', getenv('STRIPE_PUB_KEY'), 'string', true);
            add_jsvar('base_url', base_url(), 'string', true);
            add_jsvar('agent_email', $this->session->userdata('email'), 'string', true);

            enqueue_style('updatecc', '/assets/css/dashboard/updatecc.css');
            enqueue_script('stripe-v3', 'https://js.stripe.com/v3/');
            enqueue_script('updatecc', '/assets/js/dashboard/updatecc.js');
        }

		$this->load->view('dashboard', $data);
	}

    /**
     * @TODO: We need to break this apart and optimize
     */
	public function sync_update(){
    set_time_limit(0);

		 //comment this bec. getenv('DSL_UPDATE_SERVER') is not set! check here: https://dashboard.agentsquared.com/idx_login/test
		//echo Modules::run('sync_loader/sync_update');
		echo Modules::run('sync_loader');
		echo Modules::run('idx_login/save_account_metas_segment',$this->session->userdata("user_id"),$this->session->userdata("agent_id"));
		echo Modules::run('idx_login/save_mysql_properties',$this->session->userdata("user_id"),$this->session->userdata("agent_id"));
		echo Modules::run('crm/leads/SyncSaveContactList');
		echo Modules::run('dsl/updateSavedSearches');

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated!</h4>');

		redirect("dashboard","refresh");

	}

	public function launch_site()
	{
		if( $this->input->is_ajax_request() )
		{
			printA( $this->input->post() ); exit;
		}
	}

	public function get_customer_infos()
	{
		if( $this->input->is_ajax_request() )
		{
			$infos = $this->Agent_sites_model->get_customer_all_infos();
			if( $infos->domain_name == NULL )
			{
				$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'You need to set your domain name before launching your site.'));
			}

			exit( json_encode($infos) );
		}
	}

	public function launch_site_now()
	{

		if( $this->input->is_ajax_request() )
		{
			$this->load->model("payment/Payment_model", "payment");

			$infos = $this->Agent_sites_model->get_customer_all_infos();
			if( $infos->domain_name == NULL )
			{
				$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'You need to set your domain name before launching your site.'));

				exit( json_encode($infos) );
			}

			//send email to admin
			$this->send_notification_to_admin( $infos );

			//update datas and receipt
			if( $this->payment->launch_site_free( $infos ) )
			{
				//$this->config->set_item('is_paid',TRUE);
				$this->db->select("r.rid, d.domains as domain_name, d.status as domain_status, d.is_subdomain as subdomain")
					->from("receipt r")
					->join("domains d", "r.agent_id = d.agent", "left")
					->where("r.agent_id", $this->session->userdata("user_id"))
					->limit(1);

				$receipt = $this->db->get()->row();

				if($receipt)
				{
					$this->config->set_item('is_paid', TRUE);
					$this->config->set_item('status_domain', $receipt->domain_status);
					$this->config->set_item('subdomain', $receipt->subdomain);
				}
				else
				{
					$this->config->set_item('is_paid', FALSE );
				}
			}

			$this->session->set_flashdata('session' , array('status' => TRUE,'message' => ''));
			$response = array("success" => TRUE);

			exit(json_encode($response));
		}
	}

	public function send_notification_to_admin( $datas = array() )
	{
	 	$this->load->library("email");

	 	$content = "Hi Admin <br><br>

	 		A new customer launches his/her site.<br><br>

	 		Name: ".$datas->first_name.' '.$datas->last_name." <br>
			Domain Name: ".$datas->domain_name." <br> <br>

	 		Best Regards,<br><br>

	 		Agent Squared Team
	 	";

	 	$subject ="New Site Launch";

	 	return $this->email->send_email(
	 		'automail@agentsquared.com',
	 		'admin',
	 		'alvinucsk@gmail.com,jocereymondelo@gmail.com,rolbru12@gmail.com',
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			'recipient_name' => "Admin",
	 		)
	 	);
	}

	public function change_password()
	{
		$data['title'] = "Dashboard";

		if( $_POST )
		{
			$this->form_validation->set_rules('old_password', "Old Password", 'required');
			$this->form_validation->set_rules('new_password', "New Password", 'required||matches[confirm_password]');
			$this->form_validation->set_rules('confirm_password', "Confirm New PAssword", 'required');

			if ($this->form_validation->run() == false)
			{
				// display the form
				// set the flash data error message if there is one
				$data['message'] = (validation_errors()) ? validation_errors() : "";
				$this->session->set_flashdata('session' , array('status' => FALSE,'message' =>  validation_errors() ));

			}
			else
			{
				$identity = $this->session->userdata('identity');
				$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			}

		}

		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

		$this->load->view('dashboard/change_password', $data);
	}

	public function change_pass_ajax() {

			if($this->input->post('new_password') && $this->input->post('confirm_new_password') && $this->input->post('g-recaptcha-response')) {

				if($this->input->post('new_password') != $this->input->post('confirm_new_password')) {
					$this->session->set_flashdata('session' , array('status' => FALSE, 'message' =>  'New Password and Confirm Password did not matched!'));
					redirect('dashboard?reset_pass=TRUE', 'refresh');
					//$response = array("success"=>FALSE, "message"=>"New Password and Confirm Password did not matched!");
				} else {

					$url = "https://www.google.com/recaptcha/api/siteverify";

					$post = array(
						'secret' 	=> "6Lf9eRsUAAAAABfHFErjABI8DUaozthQj06y_95G",
						'response' => $this->input->post('g-recaptcha-response'),
						'remoteip'	=> $this->input->ip_address(),
					);

					$ch = curl_init();

					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, 0);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
					curl_setopt($ch, CURLOPT_URL, $url);

					$result = curl_exec($ch);
					$result_info = json_decode($result, true);

					if(!empty($result_info)) {

						if($result_info['success']) {
							//$update_pass = $this->dashboard->update_password(array('password' => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)));
							$update_pass = $this->dashboard->update_password($this->input->post('new_password'));

							if($update_pass) {
								$this->change_pass_email($this->input->post('new_password'));
								$this->session->set_flashdata('session' , array('status' => TRUE, 'message' =>  'Password Successfully Reset!'));
								//$response = array("success"=>TRUE, "redirect"=>AGENT_DASHBOARD_URL."dashboard");
							} else {
								$this->session->set_flashdata('session' , array('status' => FALSE, 'message' => 'Something went wrong during update of password! Please try again later.'));
								redirect('dashboard?reset_pass=TRUE', 'refresh');
								//$response = array("success"=>FALSE, "message"=>"Something went wrong during update of password!");
							}
						} else {
							$this->session->set_flashdata('session' , array('status' => FALSE, 'message' => $result_info['error-codes'][0]));
							redirect('dashboard?reset_pass=TRUE', 'refresh');
							//$response = array("success"=>FALSE, "message"=>$result_info['error-codes'][0]);
						}
					}
				}
			} else {
				$this->session->set_flashdata('session' , array('status'=>FALSE, 'message'=>'Please complete all fields!'));
				//$response = array("success"=>FALSE, "message"=>"Please complete all fields!", "redirect"=> AGENT_DASHBOARD_URL."dashboard?reset_pass=TRUE");
			}

			redirect('dashboard', 'refresh');
			//exit(json_encode($response));
	}

	private function change_pass_email($password) {

		$this->load->library("email");

		$agent_info = $this->Agent_sites_model->get_branding_infos();

		$content = "Hi ".ucfirst($agent_info->first_name).", <br><br>

			You have reset your password successfully. Below is your new login access to your dashboard<br><br>

			Login URL: <a href='" . AGENT_DASHBOARD_URL . "login/auth/login' target='_blank'>" . AGENT_DASHBOARD_URL . "login/auth/login</a><br>
			Email: ".$agent_info->email."<br>
			Password: ".$password."

			<br><br>
			If you have any questions or would like assistance in setting up your IDX website feel free contacting us direct at (800) 901-4428 or send an email to support@agentsquared.com
			<br>
			We look forward to serving you!
			<br><br>
			All the best, <br>
			Team AgentSquared <br>
			<a href='www.AgentSquared.com'>www.AgentSquared.com</a>";

		$subject = "Your password has been reset successfully!";

	 	return $this->email->send_email(
	 		'automail@agentsquared.com',
	 		'AgentSquared',
	 		$agent_info->email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, raul@agentsquared.com, joce@agentsquared.com"
	 		)
	 	);

	}

	public function check_domain_complete() {

		$isEmpty = TRUE;

		$domain_info = $this->Agent_sites_model->fetch_domain_info();

		if(isset($domain_info)) {
			foreach($domain_info as $key) {
				if($key->agent == $this->session->userdata('user_id')) {
					if($key->type == 'agent_site_domain') {
						$isEmpty = FALSE;
						if($key->status == 'completed') {
							$flag = TRUE;
						} else {
							$flag = FALSE;
						}
					}
					/*if($key->type == 'agent_site_domain' && $key->status == 'completed') {
						$flag = TRUE;
						$response = array('success' => TRUE, 'domain_name' => $key->domains);
						exit(json_encode($response));
					}*/
				}
			}
		}

		if($isEmpty) {
			$response = array('success' => FALSE, 'empty_domain' => TRUE);
		} else {
			if($flag)
				$response = array('success' => TRUE, 'domain_complete' => TRUE);
			else
				$response = array('success' => FALSE, 'domain_complete' => FALSE);
		}

		exit(json_encode($response));
	}

	public function leads_chart_filter_by_date()
	{
		if( $this->input->is_ajax_request() )
		{

			$startTimeStamp = strtotime($this->input->post("startdate"));
			$endTimeStamp = strtotime($this->input->post("enddate"));

			$timeDiff = abs($endTimeStamp - $startTimeStamp);
			$numberDays = $timeDiff / 86400;  // 86400 seconds in one day
			// and you might want to convert to integer
			$numberDays = intval($numberDays);

			$leads_chart = array();
			$chart_days = array();

			for ($i=0; $i <= $numberDays; $i++) {
				$result = $this->dashboard->get_leads_chart_by_date( date( 'Y-m-d', strtotime($this->input->post("startdate")) + ($i * 86400) ) );
				$leads_chart[] = array("period" => date( 'Y-m-d', strtotime($this->input->post("startdate")) + ($i * 86400) ), "a_leads" => $result->countLead, "b_leads" => $result->countShowing );
				$chart_days[] = date( 'Y-m-d', strtotime($this->input->post("startdate")) + ($i * 86400) );
			}

			exit( json_encode(array("status" => TRUE,"leads_chart" => $leads_chart, "chart_days" => $chart_days)) );
		}

		exit( json_encode(array("status" => FALSE )) );
	}

	public function property_chart_filter_by_date()
	{
		if( $this->input->is_ajax_request() )
		{

			$startTimeStamp = strtotime($this->input->post("startdate"));
			$endTimeStamp = strtotime($this->input->post("enddate"));

			$timeDiff = abs($endTimeStamp - $startTimeStamp);
			$numberDays = $timeDiff / 86400;  // 86400 seconds in one day
			// and you might want to convert to integer
			$numberDays = intval($numberDays);

			$property_chart = array();
			$chart_days = array();

			for ($i=0; $i <= $numberDays; $i++) {
				$result = $this->dashboard->get_property_chart_by_date( date( 'Y-m-d', strtotime($this->input->post("startdate")) + ($i * 86400) ) );
				$property_chart[] = array("period" => date( 'Y-m-d', strtotime($this->input->post("startdate")) + ($i * 86400) ), "a_leads" => $result->countActive, "b_leads" => $result->countClose,"c_leads" => $result->countPending);
				$chart_days[] = date( 'Y-m-d', strtotime($this->input->post("startdate")) + ($i * 86400) );
			}

			exit( json_encode(array("status" => TRUE,"property_chart" => $property_chart, "chart_days" => $chart_days)) );
		}

		exit( json_encode(array("status" => FALSE )) );
	}

	public function connectRet()
	{
		/*$config = new \PHRETS\Configuration;
		$config->setLoginUrl('https://rets-stage-paragon.sandicor.com/rets/fnisrets.aspx/sandicor/login?rets-version=RETS/1.7.2')
		        ->setUsername('500770')
		        ->setPassword('j^?_!baJ7v')
		        ->setRetsVersion('1.7.2');

		$rets = new \PHRETS\Session($config);

		$connect = $rets->Login();

		$system = $rets->GetSystemMetadata();
		var_dump($system); */

		$config = new \PHRETS\Configuration;

		$config->setLoginUrl('https://rets-stage-paragon.sandicor.com/rets/fnisrets.aspx/sandicor/login?rets-version=RETS/1.7.2');
		$config->setUsername('500770');
		$config->setPassword('j^?_!baJ7v');

		// optional.  value shown below are the defaults used when not overridden
		$config->setRetsVersion('1.7.2'); // see constants from \PHRETS\Versions\RETSVersion
		//$config->setUserAgent('PHRETS/2.0');
		$config->setUserAgentPassword(''); // string password, if given
		$config->setHttpAuthenticationMethod('basic'); // or 'basic' if required
		$config->setOption('use_post_method', true); // boolean
		$config->setOption('disable_follow_location', false); // boolean

		$rets = new \PHRETS\Session($config);
		$bulletin = $rets->Login();

		//$system = $rets->GetSystemMetadata();
		$system = $rets->GetSystemMetadata();
		var_dump($system);
		var_dump($bulletin);


	}

	public function curlConnect(){

		$url = "https://rets-stage-paragon.sandicor.com/rets/fnisrets.aspx/sandicor/login?rets-version=RETS/1.7.2";
		$username = "500770";
		$password = "j^?_!baJ7v";

		$post['username'] = $username;
		$post['password'] = $password;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		//curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPGET, $post);
		//curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_USERNAME, $username);
		curl_setopt($ch, CURLOPT_KEYPASSWD, $password);
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
		$result = curl_exec($ch);
		//$resut = curl_exec($ch);
		//$result_info = json_decode($result, true);
		//curl_close ($ch);
		var_dump($status_code); exit;
		//printA( $result ); exit;

	}

	public function terms_conditions() {
		$this->load->view('terms_conditions');
	}

	public function privacy_policy() {
		$this->load->view('privacy_policy');
	}

	//============================FACEBOOK LISTINGS SCRAPER============================//
	public function facebook_url_scraper( $url=""){

        $link = $url;
        //$access_token = getenv('FACEBOOK_APP_TOKEN');
        $access_token = "867016703396837|HUy0w10u4ijhj49ohLJwXA2RbzQ";
        $url = 'https://graph.facebook.com/v2.9/?scrape=true&id='.$link.'&access_token='.$access_token.'';

        $json = file_get_contents($url, false, stream_context_create(
            array (
                'http' => array(
                    'method'    => 'POST',
                    'header'    => 'Content-type: application/x-www-form-urlencoded',
                    'content'   =>  $url,
                    'ignore_errors' => TRUE
                )
            )
        ));
        $obj = json_decode($json, true);
        //printA($obj);exit;

        return TRUE;  
    }

    public function scrape_spw_url(){

    	$user_id = $_GET["user_id"];

        //scrape spw url before posting to facebook
        $param["mlsID"] = $this->session->userdata("agent_id");
        $spw_listings = $this->single_property->get_your_properties_listings(FALSE, $param);
        //printA($spw_listings);exit;

        //get custom domain url
        $domain_info = $this->Agent_sites_model->fetch_domain_info();
        if(!empty($domain_info)) {
            $base =  "https://www.";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $base_url = "https://www.agentsquared.com";
        }

        if(!empty($spw_listings)){

            foreach($spw_listings as $spw){

            	//get spw url in domains table
	            $listingid = $spw->ListingID;
	            $property_name = url_title($spw->unparsedaddr, '-', TRUE);
	            $property_uri = implode(
	              '~',
	              array(
	                  $property_name,
	                  $listingid,
	                  $this->session->userdata("user_id")
	              )
	            );

                $this->facebook_url_scraper($base_url."property-site/".$property_uri); 
            }
        }

        return TRUE;
    }

     public function scrape_active_url(){

     	$user_id = $_GET["user_id"];

        //scrape active properties before posting to facebook
        $domain_info = $this->Agent_sites_model->fetch_domain_info();
        if(!empty($domain_info)) {
            $base =  "https://www.";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $base_url = base_url();
        }

        $active_listings = Modules::run('single_property_listings/getallActiveProperties');
        //printA($active_listings);exit;
                                
        if(!empty($active_listings)){

            foreach($active_listings as $active){

                $listing_id = $active->Id;
                $url_rewrite = url_title("{$active->StandardFields->UnparsedFirstLineAddress} {$active->StandardFields->PostalCode}");
               	$url = $base_url."property-details/".$url_rewrite."/".$listing_id."";

               if(!empty($url)){
                    $this->facebook_url_scraper($url);
               }
                
            }
        }

        return TRUE;
    }

    public function update_cc(){

    	if($this->input->is_ajax_request()){

    		header('Cache-Control: no-cache, must-revalidate');
    		header('Content-type: application/json');

    		$raw = @file_get_contents('php://input');
        $post = json_decode($raw);

        // this might come in handy.
        $user_id = (!empty($this->session->userdata('user_id')))? $this->session->userdata('user_id') : $post->user_id;

        $this->load->model('agent_model');
				$strip_cust_id = $this->agent_model->getStripeCustID($user_id);

        if($strip_cust_id !== false){
        	$ch = curl_init();

        	$url = "https://api.stripe.com/v1/customers/$strip_cust_id";

					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'source='.$post->token->id);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_USERPWD, getenv('STRIPE_API_KEY') . ":" . "");

					$headers = array();
					$headers[] = "Content-Type: application/x-www-form-urlencoded";
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

					$result = curl_exec($ch);

					if (curl_errno($ch)) {

					  echo json_encode(array(
		    			'success' => false,
		    			'post' => $_POST,
		    			'raw' => $post,
		    			'strip_cust_id' => $strip_cust_id,
		    			'message' => curl_error($ch),
		    		));
		    		exit;
					}
					curl_close ($ch);

					$response = array(
		    		'success' => true,
		    		'post' => $_POST,
		    		'raw' => $post,
		    		'strip_cust_id' => $strip_cust_id,
		    		'response' => json_decode($result),
		    	);

					$this->agent_model->setPremium($user_id, array(
						'require_cc_update' => 0,
					));

					// if(!empty($this->session->userdata('email'))){

					// 	$email = $this->session->userdata('email');

					// }else if(isset($response['response']) && property_exists($response['response'], 'email')){

					// 	$email = $response['response']->email;
					// }else{

					// 	$email = $post->email;
					// }
					//$email = urlencode($email); // remove it for now. cause it does not make sense.

					// $headers = array(
					// 	"User-Agent: AgentSquared Failed CC 30 Days Notification Email",
					// 	"Content-Type: application/vnd.api+json"
					// );

					// $ch_drip = curl_init();
					// $response['drip_url'] = $url = "https://api.getdrip.com/v2/3445542/subscribers/".$email."/remove?campaign_id=288263414";

					// curl_setopt($ch_drip, CURLOPT_URL, $url);
					// curl_setopt($ch_drip, CURLOPT_HTTPHEADER, $headers);
					// curl_setopt($ch_drip, CURLOPT_POST, 1);
					// curl_setopt($ch_drip, CURLOPT_USERPWD, "08a5998bd33b987460a6aa477b8bfb66" . ":" . "");
					// curl_setopt($ch_drip, CURLOPT_SSL_VERIFYPEER, false);
					// curl_setopt($ch_drip, CURLOPT_RETURNTRANSFER, 1);

					// $response['drip_result'] = json_decode(curl_exec($ch_drip));

					// if (curl_errno($ch_drip)) {
					//   $response['drip_error'] = curl_error($ch_drip);
					// }
					// curl_close ($ch_drip);

					echo json_encode($response);
		    	exit;
					
        }

    		echo json_encode(array(
    			'success' => false,
    			'post' => $_POST,
    			'raw' => $post,
    			'strip_cust_id' => $strip_cust_id,
    		));
    		exit;
    	}

    	show_404('URL does not exist.');
    }

    public function sync_agent_data() {

    	$this->load->model('dsl/dsl_model');

    	$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = $token->access_token;

		$call_url = getenv('AGENT_SYNCER_URL').'onboard-agent-listings-updater/'.$this->session->userdata('user_id').'/'.$this->session->userdata('agent_id').'/onboarding?access_token='.$access_token;

		$headers = array('Content-Type: application/json');

		$body = array();
		$body_json = json_encode($body, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$result_info  = curl_exec($ch);

		exit(json_encode(array('success'=>TRUE)));

    }
}
