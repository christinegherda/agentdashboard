<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<!-- Full Width Column -->
<div class="loader"></div>
<div class="content-wrapper">

  <div class="page-title">
    <h3 class="heading-title">
      <span>Dashboard</span>
    </h3>
  </div>

  <div class="sync-button">
    <a href="javascript:void(0)" class="sync-update btn-lightgreen" id="sync-update-btn"><i class="fa fa-cloud-download"></i> Sync update from Flex</a>
  </div>

      <!-- Main content -->
      <section class="content">
        <?php
          $session = $this->session->flashdata('session');
          if(isset($session)) { ?>
            <div class="alert <?=($session['status']) ? 'alert-success' : 'alert-danger'?> alert-flash" role='alert'><?= $session["message"]; ?></div>
        <?php } ?>
        <div class="display-alert">
          <?php echo $this->session->flashdata('msg'); ?>
        </div>
        <?php $this->load->view('session/launch-view'); ?>
        <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="box-bordergray" id="box-chart-morris-area">
                  <div id="chart-morris-area"></div>
                  <div class="row">
                      <div class="col-md-8">
                          <h1 class="box-gray-title">Leads</h1>
                          <div class="box-gray-body chart-responsive">
                            <div class="chart" data-chart-days='<?php echo $chart_days; ?>' data-leads-chart='<?php echo $leads_chart;?>' id="leads-chart" style="height: 300px;"></div>
                          </div>
                      </div>
                      <div class="col-md-4 no-padding-left graph-left">
                        <div class="form-group dashboard-date">
                          <form action="<?php echo site_url('dashboard/leads_chart_filter_by_date'); ?>" method="POST" class="form-inline" id="chart-leads-filter-date">
                            <div class="form-group">
                                <label class="col-md-2  no-padding">From: </label>
                                <input type="text" name="startdate" value="" class="form-control pull-right" id="datepickerfrom" style="width: 90px;" required>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 no-padding">To: </label>
                                <input type="text" name="enddate" value="" class="form-control pull-right" id="datepickerto" style="width: 90px;" required>
                            </div>
                            <button class="btn btn-success btn-xs">Go</button>
                          </form>

                          <!-- /.input group -->
                        </div>

                        <ul class="label-list">
                            <li>
                                <p class="label-title">New Leads</p>
                                <p class="bar bar-blue"></p>
                            </li>
                            <li>
                                <p class="label-title">Scheduled for showing <br> for new leads</p>
                                <p class="bar bar-orange"></p>
                            </li>
                        </ul>

                        <div class="btn-graph">
                            <a class="btn btn-darkblue" href="crm/leads">Check Leads</a>
                        </div>
                      </div>
                  </div>
              </div>
              <!-- /.leads -->
              <div class="box-bordergray">
                  <h1 class="box-gray-title">New Contacts for Today</h1>
                  <div class="table-responsive">
                    <table class="table table-striped tablesorter" id="myTable">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Last Activity</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php if( !empty( $new_contact_list ) ) :
                            foreach ($new_contact_list as $contact) :
                          ?>
                            <tr>
                              <td><?php echo $contact->first_name .' '. $contact->last_name; ?></td>
                              <td><?php echo $contact->email; ?></td>
                              <td><?php echo ($contact->phone) ? $contact->phone : "--"; ?></td>
                              <td><?php echo date("l Y:m:d H:i:s", strtotime($contact->modified)); ?></td>
                            </tr>
                            <?php endforeach; ?>
                          <?php else : ?>
                            <tr class="center"><td colspan="10"><i>No new contact found!</i></td></tr>
                          <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <p class="text-blue">Today: <span><?php echo $countNewContacts;?></span> &nbsp; &nbsp; &nbsp; All: <span><?php echo $countAllContacts; ?></span></p>
                      </div>
                      <div class="col-md-6 text-right">
                          <a href="crm/leads" class="btn btn-darkblue">View All Contacts</a>
                      </div>
                  </div>
              </div>
              <!-- /.contacts -->
              <div class="box-bordergray">
                  <h1 class="box-gray-title">SINGLE PROPERTY LISTING</h1>
                  <div class="row">
                      <div class="col-md-6">
                          <p>You have (<?php echo $siteToBeCreated; ?>) <?php echo  ($siteToBeCreated >=2) ? "sites" :"site";?> to be created.</p>
                      </div>
                      <div class="col-md-6 text-right">
                          <a class="btn btn-darkblue" href="<?php echo site_url('single_property_listings'); ?>">Take Action</a>
                      </div>
                  </div>
              </div>
              <!-- /.single property listing -->
            </div>
            <div class="col-md-6 col-sm-6 paddingmobile-0">
              <?php 
              if(!isset($_GET["reset_pass"])){?>
                  <div class="col-sm-12  col-xs-12 video-wrapper col-md-7 pull-right">
                    <div class="video-dashboard video-collapse">
                      <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-5">
                          <h1>Need Help?</h1>
                          <p>See how by watching this short video.</p>
                          <div class="checkbox">
                              <input type="checkbox" id="hide" name="hide" value="hide">
                              <label for="hide">Minimize this tip.</label>
                          </div>
                        </div>
                        <div class="col-md-1">
                          <div class="text-center">
                            <span class="fa fa-long-arrow-right"></span>
                          </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                          <div class="tour-thumbnail tour-video">
                            <img src="<?php echo base_url()."assets/images/dashboard/v_dashboard.png"; ?>" alt="Google Analytics" title="Google Analytics Guide" style="max-width: 100%;"/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              <?php }?>
              <div class="col-md-12 col-sm-12 col-xs-12 ">
                <?php display_ga_widget();?>
                <div class="box-bordergray property-list-graph" id="box-chart-morris-area-property">
                    <div id="chart-morris-area-property"></div>
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="box-gray-title">PROPERTY LISTINGS</h1>
                            <div class="box-gray-body chart-responsive">
                              <div class="chart" data-property-chart='<?php echo $property_chart;?>' id="propertylist-chart" style="height: 300px;"></div>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding-left">
                          <div class="form-group dashboard-date">
                            <!-- <form action="<?php echo site_url('dashboard/property_chart_filter_by_date'); ?>" method="POST" class="form-inline" id="chart-property-filter-date">
                              <div class="form-group">
                                  <label class="col-md-2  no-padding">From: </label>
                                  <input type="text" name="startdate" value="" class="form-control pull-right" id="datepickerfrom1" style="width: 80px;" required>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 no-padding">To: </label>
                                  <input type="text" name="enddate" value="" class="form-control pull-right" id="datepickerto1" style="width: 80px;" required>
                              </div>
                              <button class="btn btn-success btn-xs" style="margin-top: -5px;">Go</button>
                            </form> -->
                          </div>

                          <ul class="label-list">
                              <li>
                                  <p class="label-title">Active Listing</p>
                                  <p class="bar bar-green"></p>
                              </li>
                              <li>
                                  <p class="label-title"> Closed Listing</p>
                                  <p class="bar bar-red"></p>
                              </li>
                              <li>
                                  <p class="label-title">Pending Listing</p>
                                  <p class="bar bar-orange"></p>
                              </li>
                          </ul>

                          <div class="btn-graph">
                              <a class="btn btn-darkblue" href="<?php echo site_url('/featured_listings'); ?>">Review Listings</a>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
              <!-- /.property listings -->
            </div>
        </div>
    </section>
</div>
<?php $this->load->view('session/footer'); ?>
