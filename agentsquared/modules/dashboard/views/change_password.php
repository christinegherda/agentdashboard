<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<!-- Full Width Column -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
     <?php //if(!empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret"))) : ?>
        <?php $this->load->view('session/launch-view'); ?>
    <?php //endif; ?>
    <div class="row">
        <div class="col-lg-12">



          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Change Password</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo site_url("login/auth/change_password");?>" method="post">
              <div class="box-body">
                <?php $session = $this->session->flashdata('session');
                    if(isset($session)) { ?>
                      <div class="alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
                <?php } ?>

                <div class="form-group">
                    <label for="oldpassword" class="col-sm-2 control-label">Old Password</label>
                    <div class="col-sm-8">
                      <input type="password" name="old" value="" class="form-control" id="old" placeholder="Old Password">
                    </div>
                </div>

                <div class="form-group">
                    <label for="newpassword" class="col-sm-2 control-label">New Password</label>
                    <div class="col-sm-8">
                      <input type="password" name="new" value="" class="form-control" id="new" placeholder="New Password">
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirmnewpassword" class="col-sm-2 control-label">Confirm New Password</label>
                    <div class="col-sm-8">
                      <input type="password" name="new_confirm" value="" class="form-control" id="new_confirm" placeholder="Confirm New Password">
                    </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                      <button type="submit" class="btn btn-submit pull-right">Change</button>
                </div>
              </div>
            </form>
          </div>
          <!-- /.box -->

    </div>
  </section>
</div>
<?php $this->load->view('session/footer'); ?>
