<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model{

    function __construct() {
        parent:: __construct();
    }

    public function get_password() {

        $this->db->select("password")
                ->from("users")
                ->where("id", $this->session->userdata('user_id'));

        return $this->db->get()->row();
    }

    public function update_password($password=NULL) {
        //echo $password;//exit;
        //printA($this->input->post());exit;
        if($password) {

            $hash_pass = password_hash($password, PASSWORD_DEFAULT);

            if($hash_pass == 0 || $hash_pass==NULL || empty($hash_pass)) {
                $hash_pass = password_hash($password, PASSWORD_DEFAULT);
            }
            //echo $hash_pass;exit;
            $data = array('password'=>$hash_pass);

            $this->db->where('id', $this->session->userdata('user_id'));
            $this->db->update('users', $data);
            //return $hash_pass;
            return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        }
    }

    public function get_new_contacts( $count=FALSE, $countALl=FALSE, $type = "today" ){
    	$this->db->select("*")
    			->from("contacts")
    			->where("agent_id", $this->session->userdata("agent_id") )
                ->where("user_id", $this->session->userdata("user_id") )
    			->where("is_deleted !=", 1)
                ->group_by("contacts.email");

        if( $countALl )
        {
            return $this->db->count_all_results();
        }

    	if( $type == "today" )
    	{
    		//$now = date( 'Y-m-d H:i:s', time()-86400 ); // 7 days ago
    		$this->db->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d' )."' ");
    	}

    	if( $type == "1week" )
    	{
    		$date = date( 'Y-m-d H:i:s', time()-(7*86400) ); // 7 days ago

    		$this->db->where(" created >= '".$date."' ");
    	}

        if( $count )
        {
            return $this->db->count_all_results();
        }

    	$this->db->order_by("id" , "desc");
    	$query = $this->db->get();

    	if( $query->num_rows() > 0 )
    	{
    		$datas = $query->result();
    		return $datas;
    	}

    	return FALSE;
    }

    public function get_month_records()
    {
        $result["day1"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( date_scheduled, '%Y-%m' ) = '".date( 'Y-m' )."' ) as countShowing")
                    ->from("contacts")
                    ->where("agent_id", $this->session->userdata("agent_id") )
                    ->where("is_deleted !=", 1)
                    ->where(" DATE_FORMAT( created, '%Y-%m' ) = '".date( 'Y-m' )."' ")
                    ->get()->row();

        $result["day2"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( date_scheduled, '%Y-%m' ) = '".date("Y-m",strtotime("-1 month"))."' ) as countShowing")
                    ->from("contacts")
                    ->where("agent_id", $this->session->userdata("agent_id") )
                    ->where("is_deleted !=", 1)
                    ->where(" DATE_FORMAT( created, '%Y-%m' ) = '".date("Y-m",strtotime("-1 month"))."' ")
                    ->get()->row();

        $result["day3"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( date_scheduled, '%Y-%m' ) = '".date("Y-m",strtotime("-2 month"))."' ) as countShowing")
                    ->from("contacts")
                    ->where("agent_id", $this->session->userdata("agent_id") )
                    ->where("is_deleted !=", 1)
                    ->where(" DATE_FORMAT( created, '%Y-%m' ) = '".date("Y-m",strtotime("-2 month"))."' ")
                    ->get()->row();

        $result["day4"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( date_scheduled, '%Y-%m' ) = '".date("Y-m",strtotime("-3 month"))."' ) as countShowing")
                    ->from("contacts")
                    ->where("agent_id", $this->session->userdata("agent_id") )
                    ->where("is_deleted !=", 1)
                    ->where(" DATE_FORMAT( created, '%Y-%m' ) = '".date("Y-m",strtotime("-3 month"))."' ")
                    ->get()->row();

        $result["day5"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( date_scheduled, '%Y-%m' ) = '".date("Y-m",strtotime("-4 month"))."' ) as countShowing")
                    ->from("contacts")
                    ->where("agent_id", $this->session->userdata("agent_id") )
                    ->where("is_deleted !=", 1)
                    ->where(" DATE_FORMAT( created, '%Y-%m' ) = '".date("Y-m",strtotime("-4 month"))."' ")
                    ->get()->row();

        $result["day6"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( date_scheduled, '%Y-%m' ) = '".date("Y-m",strtotime("-5 month"))."' ) as countShowing")
                    ->from("contacts")
                    ->where("agent_id", $this->session->userdata("agent_id") )
                    ->where("is_deleted !=", 1)
                    ->where(" DATE_FORMAT( created, '%Y-%m' ) = '".date("Y-m",strtotime("-5 month"))."' ")
                    ->get()->row();

        $result["day7"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( date_scheduled, '%Y-%m' ) = '".date("Y-m",strtotime("-6 month"))."' ) as countShowing")
                    ->from("contacts")
                    ->where("agent_id", $this->session->userdata("agent_id") )
                    ->where("is_deleted !=", 1)
                    ->where(" DATE_FORMAT( created, '%Y-%m' ) = '".date("Y-m",strtotime("-6 month"))."' ")
                    ->get()->row();

        //printA( $result ); exit;
        return $result;

    }

    // public function get_day_records()
    // {
    //     $result["day1"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d' )."' ) as countShowing")
    //                 ->from("contacts")
    //                 ->where("agent_id", $this->session->userdata("agent_id") )
    //                 ->where("is_deleted !=", 1)
    //                 ->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d' )."' ")
    //                 ->get()->row();

    //     $result["day2"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-86400 )."' ) as countShowing")
    //                 ->from("contacts")
    //                 ->where("agent_id", $this->session->userdata("agent_id") )
    //                 ->where("is_deleted !=", 1)
    //                 ->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-86400 )."' ")
    //                 ->get()->row();

    //     $result["day3"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(2*86400) )."' ) as countShowing")
    //                 ->from("contacts")
    //                 ->where("agent_id", $this->session->userdata("agent_id") )
    //                 ->where("is_deleted !=", 1)
    //                 ->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(2*86400) )."' ")
    //                 ->get()->row();

    //     $result["day4"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(3*86400) )."' ) as countShowing")
    //                 ->from("contacts")
    //                 ->where("agent_id", $this->session->userdata("agent_id") )
    //                 ->where("is_deleted !=", 1)
    //                 ->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(3*86400) )."' ")
    //                 ->get()->row();

    //     $result["day5"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(4*86400) )."' ) as countShowing")
    //                 ->from("contacts")
    //                 ->where("agent_id", $this->session->userdata("agent_id") )
    //                 ->where("is_deleted !=", 1)
    //                 ->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(4*86400) )."' ")
    //                 ->get()->row();

    //     $result["day6"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(5*86400) )."' ) as countShowing")
    //                 ->from("contacts")
    //                 ->where("agent_id", $this->session->userdata("agent_id") )
    //                 ->where("is_deleted !=", 1)
    //                 ->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(5*86400) )."' ")
    //                 ->get()->row();

    //     $result["day7"] = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(6*86400) )."' ) as countShowing")
    //                 ->from("contacts")
    //                 ->where("agent_id", $this->session->userdata("agent_id") )
    //                 ->where("is_deleted !=", 1)
    //                 ->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(6*86400) )."' ")
    //                 ->get()->row();


    //     return $result;

    // }

    public function get_leads_chart_by_date( $date = "" )
    {
        $query = $this->db->select("count(id) as countLead, (SELECT count(cssid) FROM customer_schedule_of_showing WHERE agent_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( created, '%Y-%m-%d' ) = '".$date ."' ) as countShowing")
                    ->from("contacts")
                    ->where("agent_id", $this->session->userdata("agent_id") )
                    ->where("is_deleted !=", 1)
                    ->where(" DATE_FORMAT( created, '%Y-%m-%d' ) = '".$date."' ")
                    ->get()->row();

        return $query;
    }

    public function get_day_records_property()
    {
        $result["day1"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("+1 year"))."' AND property_status = 'Closed') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("+1 year"))."' AND property_status = 'Sold') as countSold,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("+1 year"))."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("+1 year"))."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day2"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y")."' AND property_status = 'Closed') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y")."' AND property_status = 'Sold') as countSold,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y")."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y' ) = '".date("Y")."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day3"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-1 year"))."' AND property_status = 'Closed') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-1 year"))."' AND property_status = 'Sold') as countSold,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-1 year"))."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-1 year"))."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day4"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-2 year"))."' AND property_status = 'Closed') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-2 year"))."' AND property_status = 'Sold') as countSold,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-2 year"))."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Yd' ) = '".date("Y",strtotime("-2 year"))."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day5"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-3 year"))."' AND property_status = 'Closed') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-3 year"))."' AND property_status = 'Sold') as countSold,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-3 year"))."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-3 year"))."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day6"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-4 year"))."' AND property_status = 'Closed') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-4 year"))."' AND property_status = 'Sold') as countSold,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-4 year"))."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-4 year"))."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day7"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-5 year"))."' AND property_status = 'Closed') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-5 year"))."' AND property_status = 'Sold') as countSold,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-5 year"))."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y' ) = '".date("Y",strtotime("-5 year"))."' ")
                ->where("property_status", "Active")
                ->get()->row();

        return $result;
    }

    public function get_day_records_property_old()
    {
        $result["day1"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d' )."' AND property_status = 'Close') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d' )."' AND property_status = 'Close') as countSold,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d' )."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d' )."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day2"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-86400 )."' AND property_status = 'Close') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-86400 )."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-86400 )."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day3"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(2*86400) )."' AND property_status = 'Close') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(2*86400) )."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(2*86400) )."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day4"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(3*86400) )."' AND property_status = 'Close') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(3*86400) )."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(3*86400) )."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day5"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(4*86400) )."' AND property_status = 'Close') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(4*86400) )."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(4*86400) )."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day6"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(5*86400) )."' AND property_status = 'Close') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(5*86400) )."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(5*86400) )."' ")
                ->where("property_status", "Active")
                ->get()->row();

        $result["day7"] = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(6*86400) )."' AND property_status = 'Close') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(6*86400) )."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".date( 'Y-m-d', time()-(6*86400) )."' ")
                ->where("property_status", "Active")
                ->get()->row();


        return $result;
    }

    public function get_property_chart_by_date( $date = "" )
    {
        $query = $this->db->select("count(id) as countActive,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".$date ."' AND property_status = 'Close') as countClose,
                (SELECT count(id) FROM property_listing_data WHERE user_id = '".$this->session->userdata("user_id")."' AND DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".$date ."' AND property_status = 'Pending') as countPending"
                )
                ->from("property_listing_data")
                ->where("user_id", $this->session->userdata("user_id") )
                ->where(" DATE_FORMAT( datecreated, '%Y-%m-%d' ) = '".$date."' ")
                ->where("property_status", "Active")
                ->get()->row();


        return $query;
    }

    public function is_trial_version_end() {

        $ret = FALSE;

        $this->db->select("u.trial, u.date_signed, p.price, p.plan")
                ->from("users u")
                ->join("pricing p", "u.id = p.user_id", "left")
                ->where("u.id", $this->session->userdata('user_id'))
                ->where("u.trial", 1)
                ->where("p.type", "idx")
                ->limit(1);

        $res = $this->db->get()->row();

        if(!empty($res)) {

            $num = 0;

            if(!empty($res->date_signed)) {

                $from=date_create(date('Y-m-d'));
                $to=date_create(date("Y-m-d", strtotime($res->date_signed)));
                $diff=date_diff($from,$to);
                $num = $diff->days;

            }

            $ret = array("left" => $num, "trial" => $res->trial, "pricing" => $res->plan, "price" => $res->price);
        }

        return $ret;
    }

    public function get_bearer_token() {

        $this->db->select("bearer_token")
                ->from("user_bearer_token")
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->limit(1);

        return $this->db->get()->row();

    }

    /*public function is_trial_version_end()
    {
        $this->db->select("trial,date_signed")
            ->from("users")
            ->where("id", $this->session->userdata("user_id") )
            ->limit(1);

        $res = $this->db->get()->row();

        if( !empty($res) )
        {
            $num = 0;

            if( !empty($res->date_signed) ){

                $from=date_create(date('Y-m-d'));
                $to=date_create(date("Y-m-d", strtotime($res->date_signed)));
                $diff=date_diff($from,$to);
                //printA($diff2); exit;
                $num = $diff->days;
            }

            return array("left" => $num, "trial" => $res->trial, "pricing" => $res->pricing);
        }

        return FALSE;
    }*/

}
