<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\Sqs\SqsClient;
use Aws\Exception\AwsException;
class Aws_domain extends MX_Controller {

	private $apiurl = "";

	function __construct() {
		parent::__construct(); 

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("agent_sites/Agent_sites_model", "domain");

        $this->apiurl = getenv('A2_AWS_URL');
	}

	public function index() {

		$data['title'] = "Custom Domain";

		$data["js"] = array("domain/aws_domain.js");
		$data["css"] = array("dashboard/domain.css");

		//check domain user if have a domain already
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$this->load->view('aws_domain', $data);
	}

	public function domains() {
		
		if($this->input->get('domain_check') == 1) {
			$d['status'] = 0;

			if(isset($_GET['domain_name'])) {

				$domain_name = trim($this->input->get('domain_name'));
				$suffix = trim($this->input->get('suffix'));
				$domain = $domain_name.'.'.$suffix;
				
				$check_availability = 'domain/'.$domain.'/availability';
				$result = $this->get($check_availability);
				$d['domainName'] = $domain;

				if($result['Availability'] == 'AVAILABLE') {
					$d['status'] = 1;
				}
				else {

					$d['domain'] 	= $domain_name;
					$d['tld']		= $suffix;
				}
			}

			echo json_encode($d);
			exit;
		}		
	}

	public function aws_domain_suggestions(){	

		$data['title'] = "Custom Domain";

		$data["js"] = array("domain/aws_domain.js");
		$data["css"] = array("dashboard/domain.css");

		//check domain user if have a domain already
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		$data["domain_info"] = $this->domain->fetch_domain_info();	
		$d['status'] = 0;
		if( isset($_GET['domain_tld_get']) AND $_GET['domain_tld_get'] == 1 && isset( $_GET['domain'])) {
			$d['status'] = 1;
			$d['domainCount'] = 0;
			$domains = array();
			$domainSuggestion = $this->input->get("domain").'.'.$this->input->get("suffix");

			$get_suggestions = 'domain/'.$domainSuggestion.'/suggestions';
			$result = $this->get($get_suggestions);
			$suffix_arr = array("com","net","org","us");

			if(isset($result["SuggestionsList"]) && !empty($result["SuggestionsList"]))
			{
				foreach ($result["SuggestionsList"] as $domain) {
				
					$dname = explode(".", $domain["DomainName"]);
					$suffix = $dname[1];
					if(in_array($suffix, $suffix_arr)){
						$domains[] = $domain;
					} 
				}
				//$domains = $result["SuggestionsList"];				
			}

			$d['domainCount'] = count($domains);
			$d['domains'] = $domains;
			
			echo json_encode($d); // $_GET['domain']
			exit;
		}

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$this->load->view('aws_domain', $data);
	}

	public function purchase_domain() {
		$response = array("success" => FALSE);
		
		if(isset($_GET['domain_name'])) {

			$post['domain_name'] = $_GET["domain_name"];
            $post['user_name'] = $this->session->userdata("user_id");

            if(isset($_GET["user_name"]) && isset($_GET["type"])) {
	          	$post['spw_username'] = $_GET["user_name"];
	          	$post['type'] = $_GET["type"];
            }

            $result = $this->domain->add_purchase_domain($post);
           
            $result["success"] = 1;
            if( $result["success"] ==  1) 
            {
            	$d['status'] = 0;
				if(isset($_GET['domain_name'])) { 
					$service = 'domain';

					$obj = Modules::load("mysql_properties/mysql_properties");
					$account_info = $obj->get_account($this->session->userdata("user_id"), "account_info");
					/*
					$domain_infos["domain_name"] = $_GET['domain_name'];
					$domain_infos["address1"] = $account_info->Addresses[0]->Street1;
					$domain_infos["address2"] = $account_info->Addresses[0]->Street2;
					$domain_infos["city"] = $account_info->Addresses[0]->City;
					$domain_infos["country_code"] = "US";
					$domain_infos["email"] = $account_info->Emails[0]->Address;
					$domain_infos["fax"] = $account_info->Phones[0]->Number;
					$domain_infos["first_name"] = $account_info->FirstName;
					$domain_infos["last_name"] = $account_info->LastName;
					$domain_infos["organization_name"] = $account_info->Office;
					$domain_infos["phone_number"] = "+1.".str_replace('-', '', $account_info->Phones[0]->Number);
					$domain_infos["state"] = $account_info->Addresses[0]->Region;
					$domain_infos["zip_code"] = $account_info->Addresses[0]->PostalCode;*/

					//for testing purpose only
					$domain_infos["domain_name"] = strtolower($_GET['domain_name']);
					$domain_infos["address1"] = "888 Prospect Ave";
					$domain_infos["address2"] = "Suite 200";
					$domain_infos["city"] = "La Jolla";
					$domain_infos["country_code"] = "US";
					$domain_infos["email"] = "albert@agentsquared.com";
					$domain_infos["fax"] = "+1.8009014428";
					$domain_infos["first_name"] = "Albert";
					$domain_infos["last_name"] = "Lopez";
					$domain_infos["organization_name"] = "Agentsquared";
					$domain_infos["phone_number"] = "+1.8009014428";
					$domain_infos["state"] = "CA";
					$domain_infos["zip_code"] = "92037";
					
					$return = $this->post($service, $domain_infos);
					$return = $this->sqs_purchase_domain() && $return;
					//if(isset($return["statusCode"]) && $return["statusCode"] == 400)
					if(!$return)
					{	
						$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Failed to add your custom domain. Please contact support@agentsquared.com for more information!'));
            			$response = array("success" => FALSE, "redirect" => base_url()."custom_domain/domain");
					}else{
						// save operational ID which is returned from aws
						// if(isset($return['OperationId']) && !empty($return['OperationId'])){
						// 	$update['domain_name'] = $_GET['domain_name'];
						// 	$update['operationalId'] = $_GET['OperationId'];

						// 	$this->domain->update_domain($update);
						// }
						$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'You have successfully reserve a domain name [ '.$_GET["domain_name"].']. We will contact you as soon as possible once your domain is ready.'));

            			$response = array("success" => TRUE, 'message' => 'You have successfully reserve a domain name [ '.$_GET["domain_name"].']. We will contact you as soon as possible once your domain is ready.', "redirect" => base_url()."custom_domain/domain");
					}				
				}

				exit(json_encode($response));
            }
            else
            {
            	$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Failed to add your custom domain. Please contact support@agentsquared.com for more information!'));
            	$response = array("success" => FALSE, 'error' => $result["error"], "redirect" => base_url()."custom_domain/domain");
            }
		}
		exit(json_encode($response));
	}

	public function sqs_purchase_domain(){

   		$client = new SqsClient([
            'region' => getenv('AWS_REGION'),
            'version' => '2012-11-05'
		]);

        $messageBody = [
            'action' => 'mapDomain',
            'domain' => strtolower($this->input->get('domain_name')),
            'user_id' => $this->session->userdata("user_id")
        ];

		$params = [
		    'DelaySeconds' => 5,
		    'MessageAttributes' => [
		    	"user_id" => ["DataType" => "Number", "StringValue" => $this->session->userdata("user_id") ],
		    	"domain" => ["DataType" => "String", "StringValue" => strtolower($this->input->get('domain_name'))],
		    	"action" => ["DataType" => "String", "StringValue" => 'mapDomain'],
		    	"date_submitted" => ["DataType" => "String", "StringValue" => date("Y-m-d H:i:s") ],
		    ],
		    'MessageBody' => json_encode($messageBody),
		    'QueueUrl' => getenv('PROCESS_DOMAIN_QUEUE')
		];

		try {
		    $result = $client->sendMessage($params);
		    return TRUE;
		} catch (AwsException $e) {
		    // output error message if fails
		    return FALSE;
		    //error_log($e->getMessage());
		}
    }

	public function sqs_generate_ssl($domain_name){

   		$client = new SqsClient([
            'region' => getenv('AWS_REGION'),
            'version' => '2012-11-05'
		]);

        $messageBody = [
            'action' => 'generateSSL',
            'domain' => $domain_name,
            'user_id' => $this->session->userdata("user_id")
        ];

		$params = [
		    'DelaySeconds' => 5,
		    'MessageAttributes' => [
		    	"user_id" => ["DataType" => "Number", "StringValue" => $this->session->userdata("user_id") ],
		    	"domain" => ["DataType" => "String", "StringValue" => $domain_name],
		    	"action" => ["DataType" => "String", "StringValue" => 'generateSSL'],
		    	"date_submitted" => ["DataType" => "String", "StringValue" => date("Y-m-d H:i:s") ],
		    ],
		    'MessageBody' => json_encode($messageBody),
		    'QueueUrl' => getenv('PROCESS_DOMAIN_QUEUE')
		];

		try {
		    $result = $client->sendMessage($params);
		    return TRUE;
		} catch (AwsException $e) {
		    // output error message if fails
		    return $e->getMessage();
		    //error_log($e->getMessage());
		}
    }

	public function aws_add_existing_domain() {

		if($this->input->is_ajax_request()) {

			if($this->input->post('existing_domain_name')) {
				if(preg_match("/^(?:[-A-Za-z0-20]+\.)+[A-Za-z]{2,20}$/", $this->input->post('existing_domain_name'))) {

					$domain_name = trim($this->input->post('existing_domain_name'));
					$domain_name = str_replace('https://', '', $domain_name);
					$domain_name = str_replace('http://', '', $domain_name);
					$domain_name = str_replace('www.', '', $domain_name);
					$domain_name = str_replace('/', '', $domain_name);

					$post['domain_name'] = strtolower($domain_name);
					$post['user_name'] = $this->session->userdata("user_id");

					$result = $this->domain->add_existing_domain($post);
                    $sqs_result = $this->sqs_generate_ssl($domain_name);
					
					if($result["success"] == TRUE && $sqs_result === TRUE) {

						if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1')) ){ // Check if localhost

							$this->create_as_domain($post['domain_name']); //Execute if not in local
							
							//request CNAME
							//$service = 'zone';
							//$domain_infos['domain_name'] = $domain_name;
							//$this->post($service, $domain_infos);							
							// $service2 = 'zone/'.$domain_name.'/populate_mx';
							// $this->post($service2, $domain_infos);
						}

						$this->send_existing_domain_email($post['domain_name']); //send email to agent
						$response = array("success" => TRUE, "redirect" => base_url().'custom_domain', "message" => "Domain Successfully Reserved");

					} else {
						$response = array("success" => FALSE, "message" => $result["error"] . ' ' . $sqs_result);
					}

				} else {
					$response = array("success" => FALSE,  "message" => "Invalid Domain Name");
				}

			} else {
				$response = array("success" => FALSE, "message" => "Please enter a domain name");
			}

			exit(json_encode($response));
		}

	}

	public function create_aws_subdomain($subdomain = NULL){
		if(!empty($domain)){
			$domain_name = "agentsquared.net";
			$service = 'domain/'.$domain_name.'/subdomain';

			$domain_infos["subdomain"] = $subdomain;
					
			$return = $this->post($service, $domain_infos);
			
			if(isset($return["statusCode"]) && $return["statusCode"] == 400)
			{	
				return FALSE;
			}else{
				return TRUE;
			}

		}
	}

	private function send_existing_domain_email($domain_name = "") {

		if(!empty($domain_name)) {
			$this->load->library("email");

			$info = $this->domain->get_branding_infos();

			//Send Email Through Sendgrid
			$this->email->send_email_v3(
	        	'support@agentsquared.com', //sender email
	        	'AgentSquared', //sender name
	        	$info->email, //recipient email
	        	'paul@agentsquared.com,joce@agentsquared.com,aldwinj12345@gmail.com,raul@agentsquared.com', //bccs
	        	'17eba51b-8dee-46ea-b39e-df7966e1082a', //template_id
	        	$email_data = array(
	        		'subs' => array(
	    				'first_name' 	=> $info->first_name,
	    				'custom_domain'	=> $domain_name,
	        		)
				)
	        );
		}
        /*$content = "Hi ".$info->first_name." <br><br>

            Your agent site (".$domain_name.") is almost ready. Since you have an existing domain, you need to point your domain to our DNS. Follow the instructions in the link below. <br><br>

            " . AGENT_DASHBOARD_URL . "faq/aws_change_existing_domain

            <br><br>
            Best Regards,<br><br>

            AgentSquared Team
        ";

        $subject ="Agent Site with Existing Domain" ;

        $this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            $info->email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "troy@agentsquared.com,albert@agentsquared.com,raul@agentsquared.com,joce@agentsquared.com,paul@agentsquared.com"
            )
        );*/
	}

	/*
     * @Deprecated: Deprecated since we are now using single source
     */
	public function create_as_domain($domain = NULL) {
		$ret = false;
		return $ret;
	}

	public function domain() {
		
		$data['title'] = "Custom Domain";

		$data["js"] = array("domain/aws_domain.js");
		$data["css"] = array("dashboard/domain.css");

		//check domain user if have a domain already
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$clean_sugdomain = preg_replace('/[^A-Za-z0-9\-]/', '', $data['branding']->first_name.$data['branding']->last_name); 
		$prefered_domain = strtolower($clean_sugdomain).'.us';
		$domainSuggestion = $prefered_domain;
		
		$get_suggestions = 'domain/'.$domainSuggestion.'/suggestions';
		$data['prefered_domains'] = $this->get($get_suggestions);
		$data['pref_domain'] = strtolower($clean_sugdomain);
		$data['first_name'] = $data['branding']->first_name;

		$this->load->view('aws_custom_domain', $data);
	}

	public function is_available() {

		if(isset($_GET['domain_name'])) {
			
				$domain_name = trim($this->input->get('domain_name'));
				$suffix = trim($this->input->get('suffix'));
				$domain = $domain_name.'.'.$suffix;
				
				if(preg_match("/^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/", $this->input->get('domain_name'))) {
					$dn = explode(".", $this->input->get('domain_name'));
					$domain_name = trim($dn[0]);
					$suffix = trim($dn[1]);
					$domain = $domain_name.'.'.$suffix;
				}

				if(!preg_match("/^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/", $domain)) {
					$d['status']    = FALSE;
					$d['isInValidDomain'] = TRUE;
					exit(json_encode($d));		
				}

				$check_availability = 'domain/'.$domain.'/availability';
				$result = $this->get($check_availability);
				$d['domainName'] = $domain;

				if(isset($result['Availability']) && $result['Availability'] == 'AVAILABLE') {
					$d['status']    = TRUE;
					$d['isInValidDomain'] = FALSE;
				}
				else{
					$d['domain'] 		= $domain_name;
					$d['tld']			= $suffix;
					$d['status'] 		= FALSE;
					$d['isInValidDomain']     = FALSE;
				}
			
		}
		exit(json_encode($d));			
	}

	public function aws_suggestions(){	

		if(isset( $_GET['domain'])) {
			$d['status'] = 1;
			$d['domainCount'] = 0;
			$domains = array();
			$domainSuggestion = $this->input->get("domain");

			$get_suggestions = 'domain/'.$domainSuggestion.'/suggestions';
			$result = $this->get($get_suggestions);
			$suffix_arr = array("com","net","org","us");
			//echo $domainSuggestion;
			//printA($result); exit;
			if(isset($result["SuggestionsList"]) && !empty($result["SuggestionsList"]))
			{
				foreach ($result["SuggestionsList"] as $domain) {
				
					$dname = explode(".", $domain["DomainName"]);
					$suffix = $dname[1];
					if(in_array($suffix, $suffix_arr)){
						$domains[] = $domain;
					} 
				}
				//$domains = $result["SuggestionsList"];				
			}

			$d['domainCount'] = count($domains);
			$d['domains'] = $domains;
			
			echo json_encode($d); // $_GET['domain']
			exit;
		}
	}

	private function get($service = "") {

		$url = $this->apiurl.$service;

		$headers = array(
			'Content-Type: application/json',
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		
		if(!empty($result_info))
		{
			return $result_info;
		}

		return false;
	}

	private function post($service = "", $post = array()) {

		$url = $this->apiurl.$service;

		$headers = array(
			'Content-Type: application/json',
		);
		$body = json_encode($post);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		
		if(!empty($result_info))
		{
			return $result_info;
		}

		return false;
	}
}

?>
