<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_domain extends MX_Controller {

	function __construct() {
		parent::__construct(); 

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("agent_sites/Agent_sites_model", "domain");
	}

	function index() {

		$data['title'] = "Custom Domain";

		$data["js"] = array("domain/domain.js");
		$data["css"] = array("dashboard/domain.css");

		//check domain user if have a domain already
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		
		if( isset($_GET['domain_tld_get']) AND $_GET['domain_tld_get'] == 1) {
			$d['status'] = 1;

			$id = 'troymccas';
			$pw = 'b5n7RolJukp3';
			$apiurl = "https://reseller.enom.com/";

			// $getTLDs = $apiurl . 'interface.asp?command=check&sld='.$sld.'&tld=*1&responsetype=xml&uid='.$id.'&pw='.$pw;
			// $tlds = simplexml_load_file($getTLDs);
			/* Recommended TLD's */
			$prioDomains = array(
				"com", 
			 	"net", 
			 	"me",  
			 	"org", 
			 	"us",  
			 	"info",
			 	"biz" 
			);
			$domainList = "DomainList=";

			if(isset($_GET['domain'])) {
				foreach($prioDomains as $prioDomain) {
					// DomainList=
				 	$domainList .= $_GET['domain'].".".$prioDomain.",";
				}
			}
			
			$getTLDs = $apiurl . 'interface.asp?Command=Check&responsetype=xml&uid='.$id.'&pw='.$pw.'&'.rtrim($domainList, ",");
			$tlds = simplexml_load_file($getTLDs);

			$getTLDPrice = $apiurl . 'interface.asp?Command=PE_GETDOMAINPRICING&responsetype=xml&uid='.$id.'&pw='.$pw;
			$tldPrice = simplexml_load_file($getTLDPrice);
			
			//printA( $tldPrice->pricestructure ); exit;

			$domains = array();
			$dStatus = array();
			foreach($tlds as $key => $val) {
				$domains[] = $key . "_" . $val;
			}

			// $xmlTlds->tldlist
			$d['domainCount'] = $tlds->DomainCount;

			$d['domains'] = $domains;
			$d['dstatus'] = $dStatus;

			$d['tlds'] = $tlds;
			$d['tldPrice'] = $tldPrice->pricestructure;
			echo json_encode($d); // $_GET['domain']
			exit;
		}
		if(isset($_GET['get_domain_tlds'])) {
			$d['status'] = 1;
			$apiurl = "https://reseller.enom.com/";
			$id = 'troymccas';
			$pw = 'b5n7RolJukp3';
			$getTLDs = $apiurl . 'interface.asp?Command=TLD_GetTLD&responsetype=xml&uid='.$id.'&pw='.$pw.'&Category=Real%20Estate';
			$tlds = simplexml_load_file($getTLDs);
			// echo "<pre>";
			// var_dump($tlds->TLD);
			// echo "</pre>";
			$d['tld'] = $tlds->TLD_GetTLD;
			echo json_encode($d); // $_GET['domain']
			exit;
		}
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$this->load->view('custom_domain', $data);
	}

	public function change_custom_domain() {
		$data['title'] = "Change Custom Domain";
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$this->load->view('change_custom_domain', $data);
	}

    /*
     * @Deprecated: Deprecated since we are now using single source
     */
	public function create_as_domain($domain = NULL) {
		$ret = false;
		return $ret;
	}

	public function create_spw_domain($domain = NULL) {
		$ret = false;
		/*
			{
				"user_id": "425",
			  	"agent_id": "20160229214607793818000000",
			  	"domain": "www.alur2.agentsquared.com"
			}
		*/
		if($domain) {
		}
		return $ret;
	}

	public function manual_create_spw_domain() {

		//if($domain && $user_id && $agent_id) {

			$dsl = modules::load('dsl/dsl');
			$endpoint = 'spw-create-server-config';

			$post_body = array(
				"property_name" => "40 Kents Avenue",
				"agent_user_id" => 655,
				"agent_id" 	=> "20151106220329108487000000",
				"domain"	=> "40kentsavenue.com",
				"city"	=>	"Rio Rico",
				"state"	=> 	"AZ",
			);

			$spw = $dsl->post_command_center_endpoint($endpoint, $post_body);

			printA($spw);
		//}

	}
	
	public function add_sub_domain() {

		if($this->input->post('domain_name')) {
			
			$post['domain_name'] = $this->input->post('domain_name');
			$post['user_name'] = $this->session->userdata("user_id");
			$result = $this->domain->add_subdomain($post);

			if($result["success"] == TRUE) {

				$response = array("success" => TRUE, "redirect" => base_url().'custom_domain');

				if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))){ // Check if localhost

					$this->create_as_domain($_POST['domain_name']); //Execute if not in local
				}

				exit(json_encode($response));

			} else {
				
				$response = array("success" => FALSE, "redirect" => base_url().'custom_domain');
				exit(json_encode($response));
			}
		}
	}

	public function domains() {

		if($this->input->get('domain_check') == 1) {
			
			$id = 'troymccas';
			$pw = 'b5n7RolJukp3';
			$d['status'] = 0;

			if(isset($_GET['domain_name'])) {

				$sld = trim($this->input->get('domain_name'));
				$tld = trim($this->input->get('suffix'));
				$apiurl = "https://reseller.enom.com/";

				//load xml document for to check if the domain is available or not
				$check = $apiurl.'interface.asp?command=check&sld='.$sld.'&tld='.$tld.'&responsetype=xml&uid='.$id.'&pw='.$pw;
				$xmlCheck = simplexml_load_file($check); //or die("Error: Cannot create object");

				$d['domainName'] = $xmlCheck->DomainName;
				$rrpCode = $xmlCheck->RRPCode;

				if($rrpCode == '210') {
					$d['status'] = 1;
					$getPrice = $apiurl . 'interface.asp?command=PE_GETPRODUCTPRICE&uid='.$id.'&pw='.$pw.'&ProductType=10&tld='.$tld.'&responsetype=xml';
					$getpricexml = simplexml_load_file($getPrice);

					$d['domainPrice'] = $getpricexml->productprice->price;
				}
				else {

					$d['domain'] 	= $sld;
					$d['tld']		= $tld;
				}
			}

			echo json_encode($d);
			exit;
		}
		
	}

	public function purchase_domain() {

		$response = array("success" => FALSE);

		if(isset($_GET['domain_name'])) {

			$post['domain_name'] = $_GET["domain_name"];
            $post['user_name'] = $this->session->userdata("user_id");

            if(isset($_GET["user_name"]) && isset($_GET["type"])) {
	          	$post['spw_username'] = $_GET["user_name"];
	          	$post['type'] = $_GET["type"];
            }

            $result = $this->domain->add_purchase_domain($post);
            
            if( $result["success"] ==  1) 
            {
            	//purchase to ENUM API
	        	$id = 'troymccas';
				$pw = 'b5n7RolJukp3';
				$d['status'] = 0;
				if(isset($_GET['domain_name'])) { 
					$domain_name = explode(".", $_GET['domain_name']);
					$sld = trim($domain_name[0]);
					$tld = trim($domain_name[1]);
					$apiurl = "https://reseller.enom.com/";

					//send email to admin
					$agent_name = $this->session->userdata("agentdata");
					
					$domain_array["user_name"] = $agent_name["agent_name"];
					$domain_array["domain_name"] = $_GET['domain_name'];

					$this->domain->send_notification_to_admin( $domain_array );

					//load xml document for to check if the domain is available or not
					/*$check = $apiurl.'interface.asp?command=Purchase&sld='.$sld.'&tld='.$tld.'&responsetype=xml&uid='.$id.'&pw='.$pw;
					$xmlCheck = simplexml_load_file($check); //or die("Error: Cannot create object");
					//print_r($xmlCheck ); exit;
					$rrpCode = $xmlCheck->RRPCode;*/
					$rrpCode = '200';
					if($rrpCode == '200') {						
						//update to database API REST
						//$res = $this->domain->update_agent_domain_status_post();

						// Create Domain Files

						if( !in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1')) ){ // Check if localhost
						    // not local
							// do operation

							if($post['type'] == "spw") {
								// $filedir = '/etc/httpd/spwv2/';
								// $filename = $_GET['domain_name'].'.conf';
								// shell_exec('touch '.$filedir.$filename);

								// $siteconf = fopen($filedir.$filename, "w");
								// $siteconfTXT = "<VirtualHost *:80>
								// 	Options Indexes FollowSymLinks

								// 	ServerName www.".$_GET['domain_name']."
								//     ServerAlias ".$_GET['domain_name']."
								// 	DocumentRoot /var/www/html/spw/".$_GET['site_dir']."
								// 	ErrorLog /var/www/html/spw/".$_GET['site_dir']."/error.log
								//     CustomLog /var/www/html/spw/".$_GET['site_dir']."/requests.log combined
								// </VirtualHost>";
								// fwrite($siteconf, $siteconfTXT);
								// fclose($siteconf);

								
							}
							else {
								$this->create_as_domain($_GET['domain_name']);
							}
						}
					}

				}

				$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'You have successfully reserve a domain name [ '.$_GET["domain_name"].']. We will contact you as soon as possible once your domain is ready.'));

            	$response = array("success" => TRUE, 'message' => 'You have successfully reserve a domain name [ '.$_GET["domain_name"].']. We will contact you as soon as possible once your domain is ready.');
            }
            else
            {
            	$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Failed to add your custom domain. Please contact support@agentsquared.com for more information!'));
            	$response = array("success" => FALSE, 'error' => $result["error"]);
            }
		}
		exit(json_encode($response));
	}

	public function add_existing_domain() {

		if($this->input->is_ajax_request()) {

			if($this->input->post('existing_domain_name')) {

				//if(preg_match("/^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/", $this->input->post('existing_domain_name'))) {

					$domain_name = trim($this->input->post('existing_domain_name'));
					$domain_name = str_replace('https://', '', $domain_name);
					$domain_name = str_replace('http://', '', $domain_name);
					$domain_name = str_replace('www.', '', $domain_name);
					$domain_name = str_replace('/', '', $domain_name);


					$post['domain_name'] = $domain_name;
					$post['user_name'] = $this->session->userdata("user_id");

					$result = $this->domain->add_existing_domain($post);
					
					if($result["success"] == TRUE) {

						if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1')) ){ // Check if localhost

							$this->create_as_domain($post['domain_name']); //Execute if not in local
							
						}

						$this->send_existing_domain_email($post['domain_name']); //send email to agent
						$response = array("success" => TRUE, "redirect" => base_url().'custom_domain', "message" => "Domain Successfully Reserved");

					} else {
						$response = array("success" => FALSE, "message" => $result["error"]);
					}

				//} else {
				//	$response = array("success" => FALSE,  "message" => "Invalid Domain Name");
				//}

			} else {
				$response = array("success" => FALSE, "message" => "Please enter a domain name");
			}

			exit(json_encode($response));
		}

	}

	private function send_existing_domain_email($domain_name = "") {
		$this->load->library("email");

		$info = $this->domain->get_branding_infos();

        $content = "Hi ".$info->first_name." <br><br>

            Your agent site (".$domain_name.") is almost ready. Since you have an existing domain, you need to point your domain to our DNS. Follow the instructions in the link below. <br><br>

            " . AGENT_DASHBOARD_URL . "faq/change_existing_domain

            <br><br>
            Best Regards,<br><br>

            AgentSquared Team
        ";

        $subject ="Agent Site with Existing Domain" ;

        $this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            $info->email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "troy@agentsquared.com,albert@agentsquared.com,raul@agentsquared.com,joce@agentsquared.com,paul@agentsquared.com"
            )
        );
	}
}

?>
