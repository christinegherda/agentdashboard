<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<!-- Full Width Column -->
<div class="content-wrapper custom-domain-page">
    <!-- <div class="page-title"> -->
        <ul class="page-title">
            <li><h3 class="heading-title"><span> Custom Domain</span></h3></li>
        </ul>
<!--         <h3 class="heading-title"><span> Instant IDX Website Pro</span></h3>
        <h4 class="heading-steps"><span> STEP 1 of 3: Select a custom domain for your branded web address</span></h4> -->
    <!-- </div> -->
    <!-- Main content -->
    <section class="content">
        <div class="domain-section domain-choose">
            <h2>Congratulations. A branded web addresss with your name is available.</h2>
            <?php if($is_reserved_domain["success"]) { ?>
                <div class="alert alert-info" role="alert">
                    <?php if($is_reserved_domain["is_subdomain"]){ ?>
                        <p>Free Subdomain "<?= $is_reserved_domain["domain_name"]?>". Domain Status: <span class="label label-info"><?php echo ucwords($is_reserved_domain["status"]) ?></span>.</p>

                    <?php } else{ ?>
                        <p>You have already selected a domain name "<?= $is_reserved_domain["domain_name"]?>". Domain Status: <span class="label label-info"><?php echo ucwords($is_reserved_domain["status"]) ?></span>.</p>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($is_reserved_domain["domain_exist"] == 1): ?>
                <p>Need help pointing your domain to our servers? <a href="<?php echo base_url('faq/change_existing_domain'); ?>" class="click-here" target="_blank">Click here.</a></p>
            <?php endif ?>
            <br>
            <p>Please select one...</p>
            <div class="radio-selection new-existing-domain">
                <?php if(!empty($prefered_domains["SuggestionsList"])) { $suffix_arr = array("com","net","org"); ?>
                    <?php foreach($prefered_domains["SuggestionsList"] as $key => $domain) {
                        $dname = explode(".", $domain["DomainName"]);
                        $sugs_domain = $dname[0];
                        $suffix = $dname[1];
                    ?>
                        <?php if(in_array($suffix, $suffix_arr) && $sugs_domain == $pref_domain) { ?>
                        <div class="form-group">
                            <label>
                                <input type="radio" class="option-input radio" name="domain-name" value="<?php echo $domain["DomainName"];?>" <?php if ($key == 0) { echo "checked"; }  ?> />
                                <?php echo $domain["DomainName"];?>
                            </label>
                        </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>

                
                <div class="form-group">
                    <label>
                        <input type="radio" class="option-input radio" name="domain-name" value="new-domain" />
                        I would like to search for a different domain name.
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input type="radio" class="option-input radio" name="domain-name" value="existing-domain" />
                        I own a domain name that I would like to use.
                    </label>
                </div>
                <br>
                <div class="form-group">
                    <a href="#" class="btn btn-blue btn-continue">Continue <i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
            
        </div>
        <div class="domain-section domain-new" style="display:none;">           
         <h2>These are our highly suggested domain names for you.</h2>
            <div class="radio-selection domain-options">
                <input type="hidden" id="purchase_url" name="purchase_domain" value="<?php echo base_url('custom_domain/aws_domain/purchase_domain');?>" style="display:none">
                <!-- <?php if(!empty($prefered_domains["SuggestionsList"])) { $suffix_arr = array("com","net","org"); ?>
                    <?php foreach($prefered_domains["SuggestionsList"] as $domain) {
                        $dname = explode(".", $domain["DomainName"]);
                        $sugs_domain = $dname[0];
                        $suffix = $dname[1];
                    ?>
                        <?php if(in_array($suffix, $suffix_arr) && $sugs_domain == $pref_domain) { ?>
                        <div class="form-group">
                            <label>
                                <input type="radio" class="option-input radio" name="domain-name" value="<?php echo $domain["DomainName"];?>" checked/>
                                <?php echo $domain["DomainName"];?>
                            </label>
                        </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?> -->
                <div class="form-group">
                    <label>
                        <input type="radio" class="option-input radio" name="domain-name" value="custom-domain"/>
                        I want to search for a new domain myself.
                    </label>
                </div>
                <div class="form-group">
                    <a href="#" class="btn btn-gray btn-back"><i class="fa fa-chevron-left"></i> Back </a>
                    <a href="#" class="btn btn-blue btn-continue">Continue <i class="fa fa-chevron-right"></i></a>
                </div>
            </div>  
        </div>
        <div class="domain-section domain-preferred" style="display:none;">           
         <h2>Search for your preferred domain name.</h2>
            <div class="row">
              <div class="col-lg-6 col-md-7 col-sm-8 col-xs-12">
                <div class="input-group">
                  <input type="hidden" name="domain_suffix" value="com" id="domain-suffix">
                  <input type="hidden" name="domain_checker" value="<?php echo base_url('custom_domain/aws_domain/is_available'); ?>" id="domain-url">
                  <input type="hidden" name="domain_suggest" value="<?php echo base_url('custom_domain/aws_domain/aws_suggestions'); ?>" id="suggest-url">
                  <input type="text" name="domain_name" value="" id="domain-input" class="form-control" aria-label="Text input with dropdown button">
                  
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">com <i class="fa fa-chevron-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#">com</a>
                      <a class="dropdown-item" href="#">net</a>
                      <a class="dropdown-item" href="#">org</a>
                    </div>
                   
                  </div>
                </div>
              </div>
              <div class="col-lg-1 col-md-3 col-sm-3 col-xs-12">
                <button class="btn btn-blue btn-search" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Searching">Search</button>
              </div>
              <div class="col-lg-7 col-sm-10 col-xs-12">
                  <div class="error-nomatch-mess" style="display:none;">
                    <br>
                    <p class="label label-danger">This domain is already taken. You may select from our recommended domain names below.</p>
                  </div>
              </div>
              <div class="col-lg-7 col-sm-10 col-xs-12">
                <div class="domain-inValid" style="display:none;">
                    <br />
                    <p class="label label-danger">You entered an invalid custom domain name!</p>
                </div> 
                <div class="domain-empty" style="display:none;">
                    <br />
                    <p class="label label-danger">Please enter a domain name</p>
                </div> 
              </div>
            </div>
            <div class="radio-selection domain-options" style="display:none;">
                <div id="suggestedList"></div>
                <div class="confirmation-mess" style="display:none;">
                    <br>
                    <br>
                    <p>Click reserve domain if you are sure this is the domain you want.</p>
                </div>
                <!-- <div class="form-group">
                    <label>
                        <input type="radio" class="option-input radio" name="domain-name" value="" checked/>
                        www.yourpreferreddomain.com
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input type="radio" class="option-input radio" name="domain-name" value=""/>
                        www.yourpreferreddomain.net
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input type="radio" class="option-input radio" name="domain-name" value=""/>
                        www.yourpreferreddomain.org
                    </label>
                </div> -->
            </div> 
            <div class="domain-available" style="display:none;">
                <div class="">
                    <h3>Your custom domain <span class="label label-success domain-avail"></span> is <b>AVAILABLE</b></h3>
                </div>
                <br />
                <br />
                <p>Click reserve domain if you are sure this is the domain you want.</p>
                <input type="hidden" name="" value="" class="domain-avail-input">
                <div class="form-group">
                    <a href="#" class="btn btn-gray btn-back"><i class="fa fa-chevron-left"></i> Back </a>
                    <?php
                        if($this->config->item("disallowUser")) { ?>
                            <a href="<?=base_url().'custom_domain/domain?modal_premium=true'?>" class="btn btn-blue">Reserve Domain <i class="fa fa-chevron-right"></i></a>
                    <?php
                        } else { ?>
                            <a href="#" class="btn btn-blue btn-continue">Reserve Domain <i class="fa fa-chevron-right"></i></a>
                    <?php
                        }
                    ?>
                    <!-- <a href="#" class="btn btn-blue btn-continue">Reserve Domain <i class="fa fa-chevron-right"></i></a> -->
                </div>
            </div> 
            <br>
            <br>
            <div class="button-action">
                <div class="form-group">
                    <a href="#" class="btn btn-gray btn-back"><i class="fa fa-chevron-left"></i> Back </a>
                    <?php
                        if($this->config->item("disallowUser")) { ?>
                            <a href="<?=base_url().'custom_domain/domain?modal_premium=true'?>" class="btn btn-blue">Reserve Domain <i class="fa fa-chevron-right"></i></a>
                    <?php
                        } else { ?>
                            <a href="#" class="btn btn-blue btn-continue" style="display:none;">Reserve Domain <i class="fa fa-chevron-right"></i></a>
                    <?php
                        }
                    ?>

                    <!-- <a href="#" class="btn btn-blue btn-continue" style="display:none;">Reserve Domain <i class="fa fa-chevron-right"></i></a> -->
                </div>
            </div> 
        </div>
        <div class="domain-section domain-exist" style="display:none;">
            <div class="row">
                <div class="col-md-8">
                    <h2>Enter the domain you already own.</h2><br>
                    <!-- <p style="font-size:15px">Do you need help configuring your domain to point to AgentSquared?</p>
                    <a href="<?php echo base_url('faq/change_existing_domain'); ?>" class="btn btn-green" target="_blank">Please click here for instructions</a> -->
                    <div class="row">
                        <div class="col-md-10">
                            <p class="" id="callback_text"></p>
                        </div>
                    </div>
                    <input type="hidden" name="existing_domain_url" value="<?php echo base_url('custom_domain/aws_domain/aws_add_existing_domain'); ?>" id="existing_domain_url">
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" name="existing_domain_name" value="" class="form-control" placeholder="yourdomain.com">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-gray btn-back"><i class="fa fa-chevron-left"></i> Back </a>
            <?php
                if($this->config->item("disallowUser")) { ?>
                    <a href="<?=base_url().'custom_domain/domain?modal_premium=true'?>" class="btn btn-blue">
            <?php
                } else { ?>
                   <a href="#" class="btn btn-blue existing-domain-continue">
            <?php 
                }
            ?>
                Continue <i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
        <div class="domain-section domain-check" style="display:none;">
            <h2>Checking domain name availability...</h2>
            <p>Please hold your horses while we check the availability of your domain name.</p>
            <div class="domain-loader">
                <img src="<?=base_url();?>assets/images/dashboard/dashboard-loader.svg" alt="">
            </div>
        </div>
        <div class="domain-section domain-success"  style="display:none;">
            <h2>Congratulations!</h2>
            <h2>You have successfully configured your domain name.</h2>
            <div class="row">
                <!-- <div class="col-lg-8">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus voluptatum quasi tempore, repellat nihil et dolorem vel, vero error tempora sit sunt eveniet inventore, nam voluptates ullam quam expedita unde.</p>
                </div> -->
                <div class="col-lg-12">
                    <div class="form-group">
                        <a href="#" class="btn btn-blue btn-finish">Finish <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="domain-section domain-review" style="display:none;">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="notification">
                        <p>Your custom domain may take up to 24-48 hours to propagate over the internet.</p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <p>Your custom domain: <b>yourselecteddomain.com</b></p>
                    <br>
                    <br>
                    <p>Do you need help configuring your domain to point to AgentSquared?</p>
                    <p>Please click <a href="#">here</a> and we will show you how.</p>
                </div>
            </div>
        </div>

        <div class="domain-section domain-success_mess" style="display:none;">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <h2>Thank you <?=$first_name;?></h2>
                    <br>
                    <p>Your branded website <b class="domain-name-selected"></b> will be <span class="own-domain-hide">registered and</span> ready to use with your Instant IDX&trade; Website Pro account shortly.</p>
                    <!-- <p>Need help pointing your domain to our servers? <a href="<?php echo base_url('faq/change_existing_domain'); ?>" class="click-here" target="_blank">Click here.</a></p> --> 
                    <p>Now, let's customize your home page. <a href="/customize_homepage" class="btn btn-blue btn-finish">Next<i class="fa fa-chevron-right"></i></a> </p>
                    <br>
                    <!-- <p>Need help pointing your domain to our servers?</p> -->
                </div>
            </div>
        </div>

        <div class="domain-section domain-confirmation" style="display:none;">
            <h2>:) Good news! Your <b><span class="label label-success domain-area"></span></b> is available.</h2>
            <p>You choose a great domain. There is only one more thing to left to do. <br />
            Click reserve domain if you are sure this is the domain you want.
            </p>
            <!-- <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="notification">
                        <h2>Your custom domain is <b>AVAILABLE</b></h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <p>Custom Domain: <b><span class="domain-area">yourselecteddomain.com</span></b></p>
                    <br>
                    <br>
                </div>
            </div> -->
            <div class="form-group">
                <a href="#" class="btn btn-gray btn-back"><i class="fa fa-chevron-left"></i> Back </a>
                <?php
                    if($this->config->item("disallowUser")) { ?>
                        <a href="<?=base_url().'custom_domain/domain?modal_premium=true'?>" class="btn btn-blue">Reserve Domain <i class="fa fa-chevron-right"></i></a>
                <?php
                    } else { ?>
                        <a href="#" class="btn btn-blue btn-continue reserve-domain" data-domain-name="">Reserve Domain <i class="fa fa-chevron-right"></i></a>
                <?php
                    }
                ?>
            </div>
        </div>
        <div class="domain-section domain-thankyou" style="display:none;">
            <h2>Thank you {$firstname}</h2>
            <p>Your branded web address will be registered and ready to use with your Instant IDX&trade; Website Pro account shortly.
            </p> 
            <button class="btn btn-blue" >Finish</button>
        </div>
    </section>
</div>

<div id="domainModal-confirmation" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-width="800">
    <div class="modal-content" style="width: 82%; margin-left: 139px;margin-top: 20px;">
        <div class="modal-header">
            <h1>Reserve a Domain</h1>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body2" style="height: 300px; width: 100%; position: relative; padding: 15px;">
            <p style="font-size: 18px;">Click continue if you are sure this is the domain you want.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a type="button" class="btn btn-primary aws-confirm-purchase-domain" data-domain-name="" data-domain-price="" >Continue</a>
        </div>
    </div>
</div>
<?php $this->load->view('session/footer'); ?>
