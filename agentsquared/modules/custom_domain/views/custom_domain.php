<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<!-- Full Width Column -->
<div class="content-wrapper">
    <div class="page-title">
        <h3 class="heading-title"><span> Custom Domain</span></h3>
    </div>
    <!-- Main content -->
    <section class="content">

    <?php 
        $this->load->view('session/launch-view');

        if(!$is_reserved_domain["success"]) : ?>

            <div class="row">
                <div class="col-md-12 ">
                <?php
                    $session = $this->session->flashdata('session');

                    if(isset($session)) { ?>
                        <div class="alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
                <?php 
                    } 
                ?>
                </div>
                <br/>
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Custom Domain</h3>
                        </div>
                        <div class="box-body">
                            <?php $this->load->view('custom_domain/custom_domain_form'); ?>

                        </div>
                    </div>
                </div>

    <?php else : ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php $session = $this->session->flashdata('session');
                            if(isset($session))
                            {
                        ?>
                            <div class="alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
                        <?php } ?>
                    </div>
                    <br />
                    <div class="col-md-12 mobiletop-0">
                        <div class="alert alert-info" role="alert">
                            <p>You have already selected a domain name "<?= $is_reserved_domain["domain_name"]?>". Domain Status: <span class="label label-info"><?php echo ucwords($is_reserved_domain["status"]) ?></span>.</p>
                        </div>
                        <?php 
                            if($is_reserved_domain["domain_exist"] == 1) {
                                if(isset($domain_info[0])) {
                                    if($domain_info[0]->status != "completed") { ?>
                                        <h4>Need help in changing domain name servers?</h4>
                                        <h4>Click <a type="button" data-toggle="modal" data-target="#change_existing_domain" style="cursor:pointer;">here</a> and we'll show you how.</h4>
                                        <!-- <h4>Click <a href="custom_domain/change_custom_domain">here</a> and we'll show you how.</h4> -->
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Custom Domain</h3>
                                            </div>
                                            <div class="box-body">
                                                <?php $this->load->view('custom_domain/custom_domain_form'); ?>
                                            </div>
                                        </div>

                        <?php
                                        //$this->load->view('custom_domain/custom_domain_form');
                                    }
                                }
                            }
                            /*if($domain_info[0]->status == "pending") {
                                $this->load->view('custom_domain/custom_domain_form');
                            }*/
                        ?>
                    </div>
                    <?php
                        if($is_reserved_domain["is_subdomain"] == 1) {
                            $this->load->view('custom_domain/custom_domain_form');
                        } elseif($is_reserved_domain["is_subdomain"] == 0 && $is_reserved_domain["domain_exist"] == 0) {
                            if(isset($domain_info[0])) {
                                if($domain_info[0]->status == "pending") {
                                    $this->load->view('custom_domain_form');
                                }
                            }
                        }
                    ?>
    <?php endif; ?>
                </div>
                        <!-- <div class="col-md-12" id="subdomain_container">
                            <div class="col-md-8">
                                <?php
                                    $reg = array(" ",",",".");
                                    $first_name = str_replace($reg, "", $branding->first_name);
                                    $last_name = str_replace($reg, "", $branding->last_name);
                                    $subdomain = strtolower($first_name)."".strtolower($last_name)."".".agentsquared.com";
                                    //$subdomain = strtolower($first_name)."-".strtolower($last_name)."-".$branding->code.".agentsquared.com";
                                    //$subdomain = strtolower($branding->first_name)."-".strtolower($branding->last_name)."-".$branding->code.".agentsquared.com";
                                ?>
                                <h5><a type="button" data-toggle="modal" data-target="#subdomainModal" style="cursor:pointer;">No, thanks. I'm fine with <?php echo $subdomain; ?></a></h5>
                            </div>
                        </div> -->
            </div>

            <div id="domainModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-width="800">
                <div class="modal-content" style="width: 82%; margin-left: 139px;margin-top: 20px;">
                    <div class="modal-header">
                        <h1>Reserve a Domain</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body2" style="height: 300px; width: 100%; position: relative; padding: 15px;">
                        <p style="font-size: 18px;">Click continue if you are sure this is the domain you want.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a type="button" class="btn btn-primary confirm-purchase-domain" data-domain-name="" data-domain-price="" >Continue</a>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div id="subdomainModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-width="800">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center">Sub Domain</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-center" id="subdomainstatus" style="display:none;">
                                <img src="<?=base_url()?>assets/images/gears.gif" class="img-responsive text-center" style="margin:0px auto;">
                                <span class="text-center">Generating Site, this may take a while. Please wait...</span>
                            </p>
                            <div id="subdomaininfo">
                                <input type="hidden" name="sub_domain_url" value="<?php echo base_url().'custom_domain/add_sub_domain'; ?>">
                                <input type="hidden" name="subdomain_name" value="<?php echo $subdomain; ?>">
                                <p>Click continue if you are fine with <strong><?php echo $subdomain; ?></strong>. Your sub-domain will be ready in less than one hour.</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" id="save_sub_domain" data-subdomain-name="">Continue</button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="change_existing_domain" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-width="800">
                <div class="modal-content" style="width: 82%; margin-left: 139px; margin-top: 20px; padding: 10px 95px;">
                    <div class="modal-header" style="text-align:center;">
                        <h1>Existing Domains</h1>
                    </div>
                    <div class="modal-body2" style="height: 300px; width: 100%; position: relative; padding: 50px 15px;">
                        <p style="font-size: 18px;">For existing domains, all you have to do is to edit your website’s DNS Record and point it to our IP. After you have done that, you are all set. Note that this will take up to 48 hours to propagate. </p>
                        <p>How do I create an A record to point my domain to an IP address?</p>
                        <p>To create an A-Record, please follow these steps:</p>
                        <ul>
                            <li>Sign in to your Hosting Account</li>
                            <li>Select your Domain that you want to move over to AgentSquared Servers.</li>
                            <!-- <li>Edit the DNS Settings of the selected Domain and Change the A-Record to <strong>104.236.110.186</strong>.</li> -->
                            <li>Edit the DNS Settings of the selected Domain</li>
                                <li>Enter the following nameservers:</li>
                                <ul>
                                    <strong><li>ns1.digitalocean.com</li></strong>
                                    <strong><li>ns2.digitalocean.com</li></strong>
                                    <strong><li>ns3.digitalocean.com</li></strong>
                                </ul>
                            <li>Then hit <strong>Save</strong>.</li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
    
    </section>
</div>
<?php $this->load->view('session/footer'); ?>
