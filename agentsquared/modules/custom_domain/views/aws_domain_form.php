
                        <div id="callback_text" class="col-md-12">
                        </div>
                        <div class="col-md-12">
                            <?php 
                            if(isset($domain_info[0])) {
                                if($domain_info[0]->status == "pending") { ?>
                                    <div>
                                        <button class="btn btn-default btn-submit" id="domain_yes" type="button">Existing Domain</button>
                                        <button class="btn btn-default btn-submit" id="domain_no" type="button">Check Domain</button>
                                    </div>
                            <?php } else { ?>
                                    <h4 class="col-md-2">Do you have an existing domain?</h4>
                                    <div>
                                        <button class="btn btn-default btn-submit" id="domain_yes" type="button">Yes</button>
                                        <button class="btn btn-default btn-submit" id="domain_no" type="button">No</button>
                                    </div>
                            <?php } 
                            } else { ?>
                                <h4 class="col-md-2">Do you have an existing domain?</h4>
                                <div>
                                    <button class="btn btn-default btn-submit" id="domain_yes" type="button">Yes</button>
                                    <button class="btn btn-default btn-submit" id="domain_no" type="button">No</button>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="col-md-12" id="existing_domain">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <p>Enter your existing domain.</p>
                                <form method="POST" id="existing_domain_form">
                                    <div class="col-md-4 col-sm-6 col-xs-12 no-padding-left">
                                        <input type="hidden" name="existing_domain_url" value="<?php echo base_url();?>custom_domain/aws_domain/add_existing_domain">
                                        <input type="hidden" name="email" value="<?php echo $this->session->userdata("email"); ?>">
                                        <input type="hidden" name="agent_id" value="<?php echo $this->session->userdata("agent_id"); ?>">
                                        <input type="hidden" name="agent_user_id" value="<?php echo $this->session->userdata("user_id"); ?>">
                                        <input type="hidden" name="agent_old_last_login" value="<?php echo $this->session->userdata("old_last_login"); ?>">
                                        <input type="hidden" name="agent_code" value="<?php echo $this->session->userdata("code"); ?>">
                                        <input type="hidden" name="agent_identity" value="<?php echo $this->session->userdata("email"); ?>">
                                        <input type="text" class="form-control" name="existing_domain_name" placeholder="example.com" required>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12 mobile-center">
                                        <button class="btn btn-default btn-submit" id="existing_domain_btn"><i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Submit</button>
                                    </div>
                                </form>  
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <span id="exist_message" style="color:red;"></span>        
                            </div>
                        </div>
                        <div id="domain_container">
                            <div class="col-md-12">
                                <br/>
                                <div class="col-md-11">
                                    <p>Enter your desired custom domain.</p>
                                    <input type="hidden" name="domainUrl" value="<?php echo site_url('custom_domain/aws_domain/aws_domain_suggestions'); ?>" id="domainUrl">
                                    <input type="hidden" name="domain_checker" value="<?php echo site_url('/custom_domain/domains'); ?>?domain_checker=1" id="domain_checker">
                                    <input type="hidden" name="user_agent_id" value="<?php echo $this->session->userdata("user_id"); ?>" id="user_agent_id" >                       
                                    <input type="hidden" name="purchaseDomainUrl" value="<?php echo site_url('custom_domain/aws_domain/purchase_domain'); ?>" id="purchaseDomainUrl" >

                                    <?php if(isset($_GET['domain']) && ($_GET['domain'] == 0) ) { ?>
                                        <input type="hidden" name="domain" value="<?php echo $_GET['domain']; ?>" id="getDomain">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-offset-0 col-md-6">
                                <div class="col-md-12">
                                    <form method="GET" action="<?php echo base_url(); ?>custom_domain/aws_domain/domains" id="aws-domain-form" class="domain-ajax">
                                        <div class="col-md-7 clearfix">
                                            <div class="input-group">
                                                <!--<div class="input-group-addon">http://</div>-->
                                                <input type="hidden" name="domain_check" value="1">                                                          
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-7 no-padding-left">
                                            <input type="text" class="form-control" id="domain_name" name="domain_name" value="<?=isset($_GET["domain"]) ? $_GET["domain"] : ""; ?>" placeholder="Domain Name" required>       
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-4 paddingmobile-0">
                                            <select id="suffix" class="form-control" name="suffix">
                                                <option value="com" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="com") ? 'selected="selected"' : ""; ?> >.com</option>
                                                <option value="net" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="net") ? 'selected="selected"' : ""; ?> >.net</option>
                                                <option value="org" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="org") ? 'selected="selected"' : ""; ?> >.org</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12 mobile-center">
                                            <button class="btn btn-default btn-submit" id="aws_check_domain_sbt" type="submit">
                                                <i class="fa fa-search"></i>
                                                Check Domain
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <?php 
                            if(isset($_GET['status']) && ($_GET['status'] == 0) ) {
                        ?>
                        <div class="col-md-12">
                            <div class="col-md-offset-0 col-md-6">
                                <div id="domain-result" class="clearfix">
                                    <p class="clearfix"></p>
                                    <div id="awsDomainResult" class="domain-group">
                                        <h4 class='taken'><span><?php echo $_GET['domain']; ?>.<?php echo $_GET['tld']; ?></span> is already taken.</h4><h4 class='featured'>Featured Domains:</h4>
                                        <div class="fetch-available-domains">
                                            <h1 class="text-center"><i class="fa fa-cog fa-spin"></i> <b>FETCHING AVAILABLE DOMAINS<br><small>Please wait</small></b></h1>
                                        </div> 
                                        <div class="availableDomains" style="display:none;">
                                            <div class="col-md-5 col-sm-5" id="avail-domain">
                                                <h5>List Of Available Domaine Name:</h5>
                                            </div>
                                            <!--<div class="col-md-1 col-sm-2">
                                                <h5 class="green">Price</h5>
                                            </div>-->
                                            <div class="col-md-6 col-sm-5">
                                                <h5></h5>
                                            </div>
                                            <ul class="domain-list"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php
                            }
                            else {
                        ?>
                        <div class="col-md-12">
                            <div id="domain-result" class="clearfix">
                                <p class="clearfix"></p>
                                <div id="awsDomainResult"></div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>