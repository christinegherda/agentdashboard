<?php 



?>
<div class="box-bordergray">
  <div>
    <h1 class="box-gray-title">Google Analytics </h1>
  <?php if($domain_info){?>
  
    <!-- DEPENDENCIES -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
    (function(w,d,s,g,js,fs){
      g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
      js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
      js.src='https://apis.google.com/js/platform.js';
      fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
    }(window,document,'script'));
    </script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <!-- /DEPENDENCIES -->
    <style>
      #page-ga-chart,
      #as-ga{
        width: 100%;
        max-width: 100%;
        table-layout: fixed;
      }
      #as-ga .as-datepicker{
        max-width: 100px;
        border: 1px solid #bbb;
        background: white;
        text-align: center;
      }
      #as-ga #as-range-submit{
        line-height: normal;
      }
      #as-ga select{
        border: 1px solid #bbb;
        background: white;
      }
      #ga-message{
        padding: 0;
        border: none;
        box-shadow: none;
        margin: 4px 0;
      }
      #ga-message.info{
        padding: 10px 10px 10px 15px;
        border-left: 5px solid #3ab9f5;
        box-shadow: 0 0 2px rgba(0,0,0,0.3);
      }
      #ga-message.success{
        padding: 10px 10px 10px 15px;
        border-left: 5px solid #10c231;
        box-shadow: 0 0 2px rgba(0,0,0,0.3);
      }
      #ga-message.warning{
        padding: 10px 10px 10px 15px;
        border-left: 5px solid #f3b860;
        box-shadow: 0 0 2px rgba(0,0,0,0.3);
      }
      #ga-message.error{
        padding: 10px 10px 10px 15px;
        border-left: 5px solid #f36060;
        box-shadow: 0 0 2px rgba(0,0,0,0.3);
      }
      #chart-2-container{
        overflow-x: auto;
        max-width: 100%;
      }
      #chart-2-container .gapi-analytics-data-chart .google-visualization-table-table{
        max-width: 100%;
      }
      #chart-2-container .gapi-analytics-data-chart .gapi-analytics-data-chart-styles-table-th:first-child{
        width: 200px;
      }
      .gapi-analytics-data-chart .gapi-analytics-data-chart-styles-table-th{
        
      }
      #chart-1-container .gapi-analytics-data-chart > div{
        overflow-x: auto;
        overflow-y: hidden;
      }
    </style>
    <?php //echo '<pre>';print_r(array($view,$access_token));echo '</pre>';?>
    <?php if($domain_info->domains !== ''){?>
      <?php if($domain_info->status === 'completed'){?>
        <div id="ga-message"></div>
        <form id="form-ga" method="POST">
          <input type="hidden" name="domain" value="<?php echo $domain_info->domains;?>"/>
          <input type="hidden" name="agent_id" value="<?php echo $agent_id;?>"/>
          <?php if(!$view && !$access_token){?>
            
          <input id="add-ga" type="submit" class="btn btn-darkblue" value="Enable Google Analytics"/>
          <?php }?>
        </form>
        <table id="as-ga" style="display: none;">
          <tbody>
            <tr>
              <td>
                <label>Data Range</label>
                <select id="as-data-range">
                  <option value="30daysAgo">30 Days Ago</option>
                  <option value="15daysAgo">15 Days Ago</option>
                  <option value="7daysAgo">7 Days Ago</option>
                  <option value="2months">2 Months Ago</option>
                  <option value="3months">3 Months Ago</option>
                  <option value="custom">Custom</option>
                </select>
              </td>
              <td>
                <div id="as-range" style="display: none;">
                  <label>From: </label><input type="text" class="as-datepicker as-start"/> <label>To: </label><input type="text" class="as-datepicker as-end" disabled/> <input id="as-range-submit" class="btn btn-darkblue" type="button" value="Go" />
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <div id="chart-1-container"></div>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <div id="chart-2-container" class="as-overflow" style="border: 1px solid #bbb;"></div>
              </td>
            </tr>
          </tbody>
        </table>
        <div id="chart-container"></div>
        <div id="embed-api-auth-container"></div>
        <div id="view-selector-container"></div>
        <?php /* ?>
        <form id="form-tracking-code" method="POST">
          <div style="margin-top: 10px">
            <h4>Add My Tracking Code</h4>
          </div>
          <div class="" style="margin: 10px 0;">
            <textarea id="as_tracking_code" name="as_tracking_code" placeholder="Place your tracking code here." class="form-control" style="margin-bottom: 5px;" rows="6"><?php echo $client_tracking_code;?></textarea>
            <div class="text-right">
              <input type="submit" id="as-save-code" value="Save" class="btn btn-darkblue" style="font-family: 'Montserrat', sans-serif;border-radius: 5px;box-shadow: 0 4px #33529e;"/>
            </div>
          </div>
        </form>
        <?php /* if($view && $access_token){?>
          <?php if(!$client_ga){?>
            <div style="text-align: right; margin: 8px 0;">I have an existing Google Analytics data. <a id="as-client-ga">Use Mine</a></div>
          <?php }else{?>
            <div style="text-align: right; margin: 8px 0;"><a id="as-stop-client-ga">Stop using my data.</a></div>
          <?php }?>
        <?php } */?>
      <?php }elseif($domain_info->status === 'registered'){?>
      
        <div id="ga-message" class="error">You need to point your domain to our nameservers for this feature to work. <!--<a href="<?php echo base_url();?>">Click here</a> for more information.--></div>
      <?php }else{?>
      
        <div id="ga-message" class="error">Your domain is not yet ready. Please wait.</div>
      <?php }?>
    <?php }else{?>
    
      <div id="ga-message" class="error">You need to have a custom domain or a subdomain to enable this feature. <a href="<?php echo base_url();?>custom_domain">Click here</a> to add custom domain now.</div>
    <?php }?>
    
    <script>
      jQuery(document).ready(function($){
        // ==========================================================
        var access_token, accountId, webPropertyId, profileId;
        var agent_id = $('input[name="agent_id"]').val();
        
        $('.as-datepicker').on('keydown', function(e){
          return false;
        });
        
        var today = new Date;
        today.setDate(today.getDate() - 1);
        
        $('.as-datepicker.as-start').datepicker({
          maxDate: today,
          dateFormat: 'yy-mm-dd'
        });

        $('.as-datepicker.as-start').on('change', function(){
          if($(this).val() !== ''){
            $('.as-datepicker.as-end').removeAttr('disabled');
          }else{
            $('.as-datepicker.as-end').prop('disabled', true);
          }
        });

        $('.as-datepicker.as-end').datepicker({
          maxDate: new Date,
          dateFormat: 'yy-mm-dd'
        });
        
        $('input#add-ga').on('click', function(e){
          e.preventDefault();
          var domain = $('input[name="domain"]').val();
          if(domain !== ''){
            $.ajax({
              url: '<?php echo base_url();?>as_ga/addProperty',
              dataType: 'json',
              type: 'post',
              data: {
                domain: domain
              },
              success: function( response, result ){
                console.log(response);
                if(response.success){
                  initGraph(response.data.access_token, response.data.viewId);
                  printM('Congratulations! We\'ve successfully added your domain to Google Analytics.');
                }else{
                  if(response.code === 143){
                    printM(response.message, 'error');
                  }else{
                    printM('Sorry! Something went wrong when trying to add your domain to Google Analytics. Please contact support.', 'error');
                  }
                }
              },
              error: function( jqXhr, textStatus, errorThrown ){
                console.log(jqXhr);
                console.log(textStatus);
                console.log( errorThrown );
              }
            });
          }
          return false;
        });
        
        $('input#as-save-code').on('click', function(e) {
          e.preventDefault();
          var code = $('textarea#as_tracking_code').val();
          console.log(code);
          if(code != ''){
            $.ajax({
              url: '<?php echo base_url();?>as_ga/addTrackingCode',
              dataType: 'json',
              type: 'post',
              data: {
                as_tracking_code: code
              },
              success: function( response, result ){
                console.log(response);
                if(response.success){
                  printM(response.message, 'success');
                }else{
                  printM(response.message, 'error');
                }
              },
              error: function( jqXhr, textStatus, errorThrown ){
                console.log(jqXhr);
                console.log(textStatus);
                console.log( errorThrown );
              }
            });
          }
          return false;
        });
        
        $('#as-client-ga').on('click', function(e){
          e.preventDefault();
          authorize(false);
          return false;
        });
        
        // ========================================================================

        function initGraph(access_token = '', viewId = ''){
          
          gapi.analytics.ready(function() {
            gapi.analytics.auth.authorize({
              'serverAuth': {
                'access_token': access_token
              }
            });
            if(viewId !== ''){
              $('#auth-button').hide();
              //common config
              var initconfig = {
                query: {
                  'ids' : 'ga:' + viewId,
                  'start-date': '30daysAgo',
                  'end-date': 'today',
                }
              };
              
              try{
                var dataChart1 = new gapi.analytics.googleCharts.DataChart( initconfig ).set({
                  query: {
                    'metrics': 'ga:sessions,ga:users',
                    'dimensions': 'ga:date'
                  },
                  chart: {
                    'container': 'chart-1-container',
                    'type': 'LINE',
                    'options': {
                      'width': '100%'
                    }
                  }
                });
                dataChart1.execute();
              }catch(e){
                console.log(e);
              }
              
              var dataChart2 = new gapi.analytics.googleCharts.DataChart( initconfig ).set({
                query: {
                  'metrics': 'ga:pageviews,ga:sessions,ga:avgSessionDuration,ga:bounceRate',
                  'dimensions': 'ga:pagePathLevel1,ga:country,ga:region',
                  'sort': '-ga:pageviews',
                  'filters': 'ga:country!=Philippines;ga:pagePathLevel1!=/',
                  'max-results': 7
                },
                chart: {
                  'container': 'chart-2-container',
                  'type': 'TABLE',
                  'options': {
                    'width': '100%',
                  }
                }
              });
              dataChart2.execute();
              
              $('#as-data-range').on('change', function(e){
                $('#as-range').hide();
                switch($(this).val()){
                  case '30daysAgo':
                  case '15daysAgo':
                  case '7daysAgo':
                    var newquery = {
                      'start-date': $(this).val(),
                    };
                    dataChart1.set({ query: newquery });
                    dataChart1.execute();
                    dataChart2.set({ query: newquery });
                    dataChart2.execute();
                    break;
                  case '2months':
                    var newquery = {
                      'start-date': '60daysAgo',
                    };
                    dataChart1.set({ query: newquery });
                    dataChart1.execute();
                    dataChart2.set({ query: newquery });
                    dataChart2.execute();
                    break;
                  case '3months':
                    var newquery = {
                      'start-date': '90daysAgo',
                    };
                    dataChart1.set({ query: newquery });
                    dataChart1.execute();
                    dataChart2.set({ query: newquery });
                    dataChart2.execute();
                    break;
                  default:
                    $('#as-range').show();
                }	
              });

              $('#as-range-submit').on('click', function(e){
                e.preventDefault();
                
                var startDate = $('.as-datepicker.as-start').val();
                var endDate = $('.as-datepicker.as-end').val();
                
                if(startDate != '' && endDate != ''){
                  var newquery = {
                    'start-date': startDate,
                    'end-date': endDate,
                  };
                  dataChart1.set({ query: newquery });
                  dataChart1.execute();
                  dataChart2.set({ query: newquery });
                  dataChart2.execute();
                }
              });
              
              $('#form-ga').hide();
              $('#as-ga').show();
            }
          });
        }

        function printM(message = '', msgType = ''){
          var messageBox = $('#ga-message');
          if(msgType !== ''){
            messageBox.empty();
            messageBox.removeAttr('class');
            messageBox.addClass(msgType);
            messageBox.append(message);
          }else{
            messageBox.removeAttr('class');
            messageBox.empty();
          }
        }
        
        //document.getElementById('auth-button').addEventListener('click', authorize);
        //$('#auth-button').on('click', authorize);
        <?php if($view && $access_token){?>
        
          //initGraph('<?php echo $access_token;?>', '<?php echo $view->id;?>');
        <?php }?>
        
        gapi.analytics.ready(function() {
          
          gapi.analytics.auth.on('signIn', function() {
            console.log(gapi.analytics.auth.getUserProfile());
            var request = gapi.client.analytics.management.profiles.list({
              'accountId': '~all',
              'webPropertyId': '~all'
            });
            request.execute(function(result) {
              var domain = "<?php echo $domain_info->domains;?>";
              var profile;
              
              if(result.items.length > 0){
                result.items.some(function(item) {
                  domain = domain.replace('www.', '');
                  if(item.websiteUrl.indexOf(domain) !== -1){
                    profile = item;
                    return true;
                  }
                  return false;
                });
                if(typeof profile !== 'undefined'){
                  var _test = gapi.analytics.auth.getAuthResponse();
                  console.log(_test);
                  // save to database here
                  
                  var dataChart = new gapi.analytics.googleCharts.DataChart({
                    query: {
                      ids: 'ga:'+profile.id,
                      metrics: 'ga:sessions',
                      dimensions: 'ga:date',
                      'start-date': '30daysAgo',
                      'end-date': 'yesterday'
                    },
                    chart: {
                      container: 'chart-container',
                      type: 'LINE',
                      options: {
                        width: '100%'
                      }
                    }
                  });
                  dataChart.execute();
                }else{
                  gapi.analytics.auth.signOut();
                }
              } // endif
              
              // event handlers here
            });
          });
          
          gapi.analytics.auth.on('signOut', function() {
            // update chart with new data
            console.log('sign out');
          });
          
          gapi.analytics.auth.authorize({
            container: 'embed-api-auth-container',
            clientid: '364654387756-53odkr9umg6usgapd6a84debsvhl7bfn.apps.googleusercontent.com',
            overwriteDefaultScopes: true,
            scopes: ['https://www.googleapis.com/auth/analytics.readonly'],
          });
          
        });
        
        <?php /*?>
        
        const GA = {
          domain: "<?php echo $domain_info->domains;?>",
          token: "",
          authData: {
            client_id: '364654387756-53odkr9umg6usgapd6a84debsvhl7bfn.apps.googleusercontent.com',
            scope: ['https://www.googleapis.com/auth/analytics'],
            immediate: false
          },
          authorize: function() {
            
            
            
            //gapi.auth.authorize(GA.authData, function(response) {
            //  if (response.error) {
            //    printM('There is something wrong with your authentication. Please contact support.', 'error');
            //  }else{
            //    console.log({authorize: response});
            //    //queryAccounts(response.access_token);
            //  }
            //});
          },
        }
        
        $('#as-use-mine').on('click', function(e) {
          GA.authorize();
        });
        

        function queryAccounts(access_token) {
          // Load the Google Analytics client library.
          gapi.client.load('analytics', 'v3').then(function() {
            gapi.client.analytics.management.accounts.list().then(function(response) {
              if(response.result.items.length){
                for(var i = 0; i < response.result.items.length; i++){
                  //queryProperties(access_token, response.result.items[i].id);
                  //console.log(response.result.items[i]);
                  queryAllProfiles(access_token, response.result.items[i].id);
                }
              }else{
                printM('You do not have Google Analytics account. Please sign up to <a href="https://analytics.google.com/analytics/web/#provision/SignUp/" target="_blank">Google Analytics</a>. You can enable this feature once you have signed up with Google Analytics.', 'error');
              }
            });
          });
          
        }
        
        function queryAllProfiles(access_token, accountId){
          var request = gapi.client.analytics.management.profiles.list({
            'accountId': accountId,
            'webPropertyId': '~all'
          });
          console.log(request);
          request.execute(function(response){
            if(response.result.items && response.result.items.length){
              var profile = response.result.items.find(function(item){
                if(item.websiteUrl.indexOf(domain.replace('www.', '')) !== -1){
                  return item;
                }
                return false;
              });
              console.log(profile);
              if(profile && typeof profile !== undefined){
                $.ajax({
                  url: '<?php echo base_url();?>as_ga/updateClientData',
                  dataType: 'json',
                  type: 'post',
                  data: {
                    other_ga: true,
                    accountId: profile.accountId,
                    webPropertyId: profile.webPropertyId,
                    profileId: profile.id,
                  },
                  success: function( response, result ){
                    
                    console.log(response.data);
                    console.log(result);
                    
                    //insertAccountUserLink(profile.accountId);
                    initGraph(access_token, profile.id);
                    
                  },
                  error: function( jqXhr, textStatus, errorThrown ){
                    console.log(jqXhr);
                    console.log(textStatus);
                    console.log( errorThrown );
                  }
                });
              }
            }else{
              console.log({message: 'no views for this property.', queryProfiles: response.result.items});
              printM('No views (profiles) found for this user.', 'error');
            }
          });
        }
        
        function insertAccountUserLink(accountId) {
          var admin_users = [
            'jovanni@agentsquared.com',
            'as-ga-reporting@thinking-league-149104.iam.gserviceaccount.com'
          ];
          for(var i=0; i < admin_users.length; i++){
            var request = gapi.client.analytics.management.accountUserLinks.insert({
              'accountId': accountId,
              'resource': {
                'permissions': {
                  'local': [
                    'EDIT',
                    'MANAGE_USERS'
                  ]
                },
                'userRef': {
                  'email': admin_users[i]
                }
              }
            });
            request.execute(function (response) {
              console.log(response);
              // this only displays our user information after we have been added as admin to this property
            });
          }
        }

        <?php //*/?>
      });
    </script>

    <!--<script src="https://apis.google.com/js/client.js?onload=authorize"></script>-->
    <!-- <script src="https://apis.google.com/js/client.js"></script> -->
  <?php }else{?>
    <div id="ga-message" class="warning">You need to have a custom domain or a subdomain to enable this feature. <a href="<?php echo base_url();?>custom_domain">Click here</a> to add custom domain now.</div>
  <?php }?>	
  </div>
</div>
<!-- ./ google analytics graph -->