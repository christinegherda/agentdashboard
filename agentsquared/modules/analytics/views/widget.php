<!-- google analytics widget -->
<style>
<?php print($ascss);?>

</style>
<div class="module-ga box-bordergray">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script>
  (function(w,d,s,g,js,fs){
    g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
    js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
    js.src='https://apis.google.com/js/platform.js';
    fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
  }(window,document,'script'));
  </script>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <div class="ga-container">
    <h1 class="box-gray-title">Google Analytics</h1>
    <?php
      //If user has authority to access GA
      if($this->config->item("disallowUser")) { ?>
        <div class="text-danger">Please upgrade your plan to <strong>PREMIUM</strong> to access this feature.</div>
    <?php
      } else { //If user dont have authority to access GA

        if($domain || $domain != '') {
          if($domain_status === 'completed') { ?>
            <div id="ga-message"></div>
            <div id="ga-controls" style="display: none;">
              <div class="col-md-4 col-sd-4">
                <label>Data Range</label>
                <select id="as-data-range">
                  <option value="30daysAgo">30 Days Ago</option>
                  <option value="15daysAgo">15 Days Ago</option>
                  <option value="7daysAgo">7 Days Ago</option>
                  <option value="2months">2 Months Ago</option>
                  <option value="3months">3 Months Ago</option>
                  <option value="custom">Custom</option>
                </select>
              </div>
              <div class="col-md-8 col-sd-8">
                <div id="as-range" style="display: none;">
                  <label>From: </label><input type="text" class="as-datepicker as-start"/> <label>To: </label><input type="text" class="as-datepicker as-end" disabled/> <input id="as-range-submit" class="btn btn-darkblue" type="button" value="Go" />
                </div>
              </div>
            </div>

            <div id="ga-widget-content" class="col-md-12 col-sd-12"></div>
    <?php 
          } elseif($domain_status === 'registered') { ?>
              <div class="text-danger">You need to point your domain to our nameservers for this feature to work.</div>
    <?php 
          } else { ?>
              <div class="text-danger">Your domain is not yet ready. Please wait.</div>
    <?php 
          }

        } else { ?>
            <div class="text-danger">
              You need to have a custom domain or a subdomain to enable this feature. <a href="<?php echo base_url();?>custom_domain">Click here</a> to add custom domain now.
            </div>
    <?php 
        }
      }
    ?>
    
    <script type="text/javascript">
      <?php $json->render(); ?>
      <?php print($asjs); // please do not move this or add tabs on the beginning.?>
    </script>
  </div>
</div>
<!-- ./ google analytics widget -->