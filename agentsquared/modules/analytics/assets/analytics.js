jQuery(document).ready(function($) {
  
  function GA(){
    var self = this;
    this.viewId;
    this.config = {
      init: function(view_id){
        if(typeof view_id !== 'undefined'){
          return {
            query: {
              'ids' : 'ga:' + view_id,
              'start-date': '30daysAgo',
              'end-date': 'today',
            }
          };
        }
        return false;
      },
      line: {
        query: {
          'metrics': 'ga:sessions,ga:users',
          'dimensions': 'ga:date',
          'filters': 'ga:country!=Philippines',
        },
        chart: {
          'container': 'as-ga-line',
          'type': 'LINE',
          'options': {
            'width': '100%'
          }
        }
      },
      table: {
        query: {
          'metrics': 'ga:pageviews,ga:sessions,ga:avgSessionDuration,ga:bounceRate',
          'dimensions': 'ga:pagePathLevel1,ga:country,ga:region',
          'sort': '-ga:pageviews',
          'filters': 'ga:country!=Philippines',
          'max-results': 7
        },
        chart: {
          'container': 'as-ga-table',
          'type': 'TABLE',
          'options': {
            'width': '100%',
          }
        }
      },
    };
    this.chart = {};
    this.get = {
      access_token: function(callback) {
        $.ajax({
          url: base_url+'analytics/getAccessToken',
          method: 'post',
          data: {
            nonce: nonce,
          },
          success: callback,
          error: function(response) {
            console.log(response);
          },
        });
      },
      profile: function(callback){
        $.ajax({
          url: base_url+'analytics/getAnalytics',
          method: 'post',
          data: {
            nonce: nonce,
          },
          success: callback,
          error: function(response) {
            console.log(response);
          },
        });
      },
    };
    this.update = function(callback) {
      $.ajax({
        url: base_url+'analytics/updateAnalytics',
        method: 'post',
        data: {
          nonce: nonce,
        },
        success: callback,
        error: function(response) {
          console.log(response);
        },
      });
    };
    this.render = function(args = {}){
      gapi.analytics.ready(function(){
        self.viewId = args.viewId;
        
        gapi.analytics.auth.authorize(args.login);
        self.chart['line'] = new gapi.analytics.googleCharts.DataChart( self.config.init(args.viewId) ).set( self.config.line );
        self.chart['table'] = new gapi.analytics.googleCharts.DataChart( self.config.init(args.viewId) ).set( self.config.table );
        
        $(args.container).empty();
        var _line = $(document.createElement('div'));
        _line.attr('id', 'as-ga-line');
        var _table = $(document.createElement('div'));
        _table.attr('id', 'as-ga-table');
        _table.css({'border' : '1px solid #bbb'});
        
        args.container.append(_line);
        args.container.append(_table);
        
        self.chart.line.on('error', function(error) {
          if(error.code === 403){
            $('#ga-message').text('We were trying to access your Google Analytics account but we encountered an error on View ID: '+args.viewId+', insufficient permissions. Please contact support.');
          }
        });
        self.chart.table.on('error', function(error) {
          if(error.code === 403){
            $('#ga-message').text('We were trying to access your Google Analytics account but we encountered an error on View ID: '+args.viewId+', insufficient permissions. Please contact support.');
          }
        });
        self.chart.table.on('success', function(response){
          $('#ga-controls').show();
          if(!args.client){
            var use_mine = $(document.createElement('p'));
            use_mine.css({'text-align': 'right'});
            use_mine.append('I have an existing Google Analytics data, <a role="button" id="as-ga-use-mine">use mine</a>.');
            use_mine.attr('id', 'ga-message-use-mine');
            
            var tooltip = '<span class="ga-tooltip" style="display: none;">In order for this feature to work, your property (<strong style="text-decoration: underline;">'+domain+'</strong>) should exist in your Google Analytics account.</span>';
            use_mine.prepend(tooltip);
            
            console.log($(args.container).find('#ga-message-use-mine'));
            
            if($(args.container).find('#ga-message-use-mine').length === 0){
              args.container.append(use_mine);
            }
          }else{
            // display sign out and use agentsquared analytics here
          }
        });
        
        self.chart.line.execute();
        self.chart.table.execute();
        
      });
    };
    this.reload = function(args = {}){
      
    };
  }
  
  var ga = new GA();
  
  ga.get.profile(function(response){
    var container = $('#ga-widget-content');
    if(response.success){
      if(response.profile){
        container.text('Loading Google Analytics, please be patient.');
        if(response.profile.client_profile_id){
          ga.get.access_token(function(response) {
            ga.render({
              login: {
                'serverAuth': {
                  'access_token': response.access_token
                },
              },
              container: container,
              viewId: response.view_id,
              client: true,
            });
          });
        }else{
          ga.get.access_token(function(response) {
            ga.render({
              login: {
                'serverAuth': {
                  'access_token': response.access_token
                },
              },
              container: container,
              viewId: response.view_id,
              client: false,
            });
          }); //- end of ga.get.access_token 
          $('#ga-message').text('You are using AgentSquared Google Analytics account.');
        }
      }else{
        var btn_enable = $(document.createElement('button'));
        btn_enable.addClass('btn btn-darkblue');
        btn_enable.text('Enable Google Analytics');
        btn_enable.on('click', function(e) {
          // Google analytics is only available to premium users. They will not be able to proceed any further if they are freemium users.
          // if(premium){
          btn_enable.prop( "disabled", true );
          container.text('Loading Google Analytics, please be patient.');
          ga.update(function(response) {
            if(response.success){
              ga.get.access_token(function(response) {
                ga.render({
                  login: {
                    'serverAuth': {
                      'access_token': response.access_token
                    }
                  },
                  container: container,
                  viewId: response.view_id,
                  client: false,
                });
              });
              $('#ga-message').text('You are using AgentSquared Google Analytics account.');
              btn_enable.remove();
            }else{
              console.log(response);
            }
          });
          // }else{
            // show upgrade to premium modal here.
          // }
        });
        container.append(btn_enable);
      }
    }else{
      console.log(response);
    }
  });
  
  function CLIENT_GA(){
    
    this.authorize = function(args){
      var authData = {
        client_id: '364654387756-53odkr9umg6usgapd6a84debsvhl7bfn.apps.googleusercontent.com',
        scope: ['https://www.googleapis.com/auth/analytics.edit', 'https://www.googleapis.com/auth/analytics.manage.users'],
        immediate: false,
      };
      gapi.auth.authorize(authData, function(response) {
        if (response.error) {
          $('#ga-message').text('We couldn\'t log in to your account. Please contact support.');
        }else{
          //queryAccounts(response.access_token);
          gapi.client.load('analytics', 'v3').then(function(){
            var request = gapi.client.analytics.management.profiles.list({
              'accountId': '~all',
              'webPropertyId': '~all'
            });
            request.execute(function(response){
              if(response.result.items && response.result.items.length){
                var view = response.result.items.find(function(item){
                  if(item.websiteUrl.indexOf(domain.replace('www.', '')) !== -1){
                    return item;
                  }
                  return false;
                });
                if(typeof view !== 'undefined' && view){
                  $.ajax({
                    url: base_url+'analytics/updateClientData',
                    dataType: 'json',
                    type: 'post',
                    data: {
                      accountId: view.accountId,
                      webPropertyId: view.webPropertyId,
                      profileId: view.id,
                      nonce: nonce,
                    },
                    success: function( response, result ){
                      var request = gapi.client.analytics.management.webpropertyUserLinks.insert({
                        'accountId': response.data.account_id,
                        'webPropertyId': response.data.property_id,
                        'resource': {
                          'permissions': {
                            'local': [
                              'EDIT',
                              'MANAGE_USERS'
                            ]
                          },
                          'userRef': {
                            'email': 'as-ga-agents@thinking-league-149104.iam.gserviceaccount.com'
                          }
                        }
                      });
                      request.execute(function (response) {
                        // this only displays our user information after we have been added as admin to this property
                        if(response.result){
                          ga.get.profile(function(resp){
                            var container = $('#ga-widget-content');
                            if(resp.success){
                              if(resp.profile){
                                container.text('Loading Google Analytics, please be patient.');
                                if(resp.profile.client_profile_id){
                                  ga.get.access_token(function(resp) {
                                  ga.render({
                                    login: {
                                      'serverAuth': {
                                        'access_token': resp.access_token
                                      },
                                    },
                                    container: container,
                                    viewId: resp.view_id,
                                    client: true,
                                  });
                                });
                                }
                                $('#ga-message').text('You are currently using your Google Analytics account.');
                              }
                            }
                          });
                        }
                      });
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                      console.log(jqXhr);
                      console.log(textStatus);
                    }
                  });
                }else{
                  $('#ga-message').text('We did not find any property that matched your domain. Please create a property in your Google Analytics account or just use our current chart.');
                  gapi.auth.setToken(null);
                  gapi.auth.signOut();
                }
              }
            });
          });
        }
      });
    };
  }
  
  $(document).on('click', '#as-ga-use-mine', function(e){
    var client_ga = new CLIENT_GA();
    client_ga.authorize();
  });
  
  $(document).on('change', '#as-data-range', function(e){
    $('#as-range').hide();
    switch($(this).val()){
      case '30daysAgo':
      case '15daysAgo':
      case '7daysAgo':
        var newquery = {
          'start-date': $(this).val(),
        };
        ga.chart.line.set({ query: newquery });
        ga.chart.line.execute();
        ga.chart.table.set({ query: newquery });
        ga.chart.table.execute();
        break;
      case '2months':
        var newquery = {
          'start-date': '60daysAgo',
        };
        ga.chart.line.set({ query: newquery });
        ga.chart.line.execute();
        ga.chart.table.set({ query: newquery });
        ga.chart.table.execute();
        break;
      case '3months':
        var newquery = {
          'start-date': '90daysAgo',
        };
        ga.chart.line.set({ query: newquery });
        ga.chart.line.execute();
        ga.chart.table.set({ query: newquery });
        ga.chart.table.execute();
        break;
      default:
        $('#as-range').show();
    }	
  });
  
  var today = new Date;
  today.setDate(today.getDate() - 1);
  
  $('.as-datepicker.as-start').datepicker({
    maxDate: today,
    dateFormat: 'yy-mm-dd'
  });
  
  $(document).on('change', '.as-datepicker.as-start', function(e){
    if($(this).val() !== ''){
      $('.as-datepicker.as-end').datepicker('destroy');
      var startDate = new Date($(this).datepicker('getDate'));
      startDate.setDate(startDate.getDate() + 1);
      $('.as-datepicker.as-end').datepicker({
        minDate: startDate,
        maxDate: new Date,
        dateFormat: 'yy-mm-dd'
      });
      $('.as-datepicker.as-end').removeAttr('disabled');
    }else{
      $('.as-datepicker.as-end').prop('disabled', true);
    }
  });
  
  $(document).on('click', '#as-range-submit', function(e){
    e.preventDefault();
    
    var startDate = $('.as-datepicker.as-start').val();
    var endDate = $('.as-datepicker.as-end').val();
    
    if(startDate != '' && endDate != ''){
      var newquery = {
        'start-date': startDate,
        'end-date': endDate,
      };
      ga.chart.line.set({ query: newquery });
      ga.chart.line.execute();
      ga.chart.table.set({ query: newquery });
      ga.chart.table.execute();
    }
  });
});
