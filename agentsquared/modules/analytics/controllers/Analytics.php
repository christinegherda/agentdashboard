<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH .'modules/analytics/libraries/Google/vendor/autoload.php';

class Analytics extends MX_Controller {
	
	const KEY_FILE_LOCATION = APPPATH . 'modules/analytics/private/as-ga-8bc731478d1f.json';
	const AGENT_KEY_FILE_LOCATION = APPPATH . 'modules/analytics/private/as-ga-agents-ab3a6798202d.json';
	private $accessToken;
	private $views = array();
  private $accounts = array();
	
	public function __construct(){
		
		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');
		$this->load->model('agent_sites/Agent_sites_model', 'domain');
		
		$this->load->model('analytics/analytics_model', 'ga');
		$this->ga->createTable();
	}
  
	public function widget(){
		// prepare data
		$domain = $this->domain->fetch_domain_info();
    if(!empty($domain)){
      $data['domain'] = $domain[0]->domains;
      $data['domain_status'] = $domain[0]->status;
      $data['agent_id'] = $this->session->userdata('agent_id');
      $data['profile'] = $this->getClientData($data['agent_id']);
      
      $json = new JSON();
      $json->set(array(
        'domain' => $data['domain'],
        'agent_id' => $data['agent_id'],
        'nonce' => create_nonce($this->session->userdata()),
      ));
      $data['json'] = $json;
      
      $data['ascss'] = file_get_contents(APPPATH . 'modules/analytics/assets/analytics.css');
      $data['asjs'] = file_get_contents(APPPATH . 'modules/analytics/assets/analytics.js');
      
      $this->load->view('analytics/widget', $data);
    }
	}
	
  public function getAccessToken(){
    if(isset($_POST['nonce'])){
      if(verify_nonce($_POST['nonce'], $this->session->userdata())){
        
        $profile = $this->getClientData($this->session->userdata('agent_id'));
        $if_client = (!empty($profile->client_profile_id))? true: false;
        $view = ($if_client)? $profile->client_profile_id: $profile->profile_id;
        $this->refreshToken($this->client($if_client));
        header('Content-Type: application/json');
        echo json_encode(array(
          'success' => true,
          'access_token' => $this->accessToken,
          'view_id' => $view,
        ));
        return true;
      }
    }
    echo json_encode(array(
      'success' => false,
      'message' => 'Unauthorized.'
    ));
  }
  
  public function getAnalytics(){
    header('Content-Type: application/json');
    if(isset($_POST['nonce'])){
      if(verify_nonce($_POST['nonce'], $this->session->userdata())){
        echo json_encode(array(
          'success' => true,
          'profile' => $this->getClientData($this->session->userdata('agent_id')),
          'code' => 200,
        ));
        return true;
      }
    }
    echo json_encode(array(
      'success' => false,
      'message' => 'Unauthorized.',
      'code' => 401,
    ));
  }
  
  public function updateAnalytics(){
    if(isset($_POST['nonce'])){
      if(verify_nonce($_POST['nonce'], $this->session->userdata())){
        // prep data
        header('Content-Type: application/json');
        $domain_info = $this->domain->fetch_domain_info();
        $domain = $domain_info[0]->domains;
        $profile = $this->getClientData($this->session->userdata('agent_id'));
        $analytics = $this->analytics(false);
        $accounts = $this->getAccounts($analytics);
        $properties = $this->getProperties($analytics);
        $views = $this->getViews($analytics);
        
        foreach($views as $view){
          $domain = str_replace('www.', '', $domain);
          if(strpos($view->websiteUrl, $domain) !== false){
            if(!$profile){
              $this->ga->insert(array(
                'agent_id' => $this->session->userdata('agent_id'),
                'domain' => str_replace('www.', '', $domain),
                'account_id' => $view->accountId,
                'property_id' => $view->webPropertyId,
                'profile_id' => $view->id,
                'is_enabled' => '1',
              ));
              echo json_encode(array(
                'success' => false,
                'code' => 302,
                'message' => 'Your custom domain or sub domain already exists in our Google Analytics. But we have updated our database to correct your account. Please refresh page.',
                'response' => $view,
              ));
            }
            return;
          }
        }
        
        foreach($accounts as $account){
          $properties = $this->getPropertiesByAccountId($analytics, $account->getId());
          if($properties->totalResults < 50){
            try{
              $property = $this->createProperty($domain);
              $response_prop = $analytics->management_webproperties->insert($account->getId(), $property);
              
              $view = $this->createProfile($domain);
              $response_view = $analytics->management_profiles->insert($response_prop->accountId, $response_prop->id, $view);
              
              $property->setDefaultProfileId($response_view->id);
              $final = $analytics->management_webproperties->update($response_prop->accountId, $response_prop->id, $property);
              
              $this->addAccountUser($analytics, $account->getId(), 'jovanni@agentsquared.com', array('EDIT', 'MANAGE_USERS'));
              
              $this->ga->insert(array(
                'agent_id' => $this->session->userdata('agent_id'),
                'domain' => str_replace('www.', '', $domain),
                'account_id' => $final->accountId,
                'property_id' => $final->id,
                'profile_id' => $response_view->id,
                'is_enabled' => '1',
              ));
              echo json_encode(array(
                'success' => true,
                'message' => 'Successfully added '.$domain.' to our Google Analytics account.',
                'data' => array(
                  'viewId' => $response_view->id,
                  'accountId' => $final->accountId,
                  'webPropertyId' => $final->id,
                ),
                'raw' => array(
                  'final' => $final,
                ),
              ));
              return;
              
            }catch(apiServiceException $e) {
              
              echo json_encode(array(
                'success' => false,
                'code' => $e->getCode(),
                'message' => $e->getMessage()
              ));
              return;
            }catch(apiException $e) {
              
              echo json_encode(array(
                'success' => false,
                'code' => $e->getCode(),
                'message' => $e->getMessage()
              ));
              return;
            }
          }
        }
        echo json_encode(array(
          'success' => false,
          'code' => 500,
          'message' => 'There was a problem adding your domain to our Google Analytics account, please contact <a href="https://www.agentsquared.com/support" target="_blank">support</a>.'
        ));
        return;
      }
    }
    echo json_encode(array(
      'success' => false,
      'code' => 401,
      'message' => 'Unauthorized'
    ));
  }
  
	private function getClientData($agent_id = ''){
		return $this->ga->get($agent_id);
	}
  
  private function refreshToken($client){
    $client->refreshTokenWithAssertion();
		$token = $client->getAccessToken();
    
    return $this->accessToken = $token['access_token'];
  }
  
  private function client($if_agent = false){
    $client = new Google_Client();
		$client->setApplicationName("AgentSquared Analytics");
    if($if_agent){
      $client->setAuthConfig(self::AGENT_KEY_FILE_LOCATION);
    }else{
      $client->setAuthConfig(self::KEY_FILE_LOCATION);
    }
    $client->setAccessType('offline');
		$client->setScopes([
			'https://www.googleapis.com/auth/analytics.readonly', // <--- always make sure this is included.
			'https://www.googleapis.com/auth/analytics.edit', 
			'https://www.googleapis.com/auth/analytics.manage.users'
		]);
    
    return $client;
  }
  
	private function analytics($if_agent = false){
		$analytics = new Google_Service_Analytics($this->client($if_agent));
		return $analytics;
	}
  
  private function createProperty($domain = ''){
    $property = new Google_Service_Analytics_Webproperty();
    if(!empty($domain)){
      $property->setName($domain);
      $property->setWebsiteUrl('http://'.$domain.'/');
      $property->setIndustryVertical('REAL_ESTATE');
      
      return $property;
    }
    return false;
  }
  
  private function createProfile(){
    $profile = new Google_Service_Analytics_Profile();
		$profile->setName('All Web Site Data');
    return $profile;
  }
  
  private function getAccounts($analytics){
    return $analytics->management_accounts->listManagementAccounts();
  }
  
  private function getProperties($analytics){
    return $analytics->management_webproperties->listManagementWebproperties('~all');
  }
  
  private function getViews($analytics){
    return $analytics->management_profiles->listManagementProfiles('~all', '~all');
  }
  
  private function getPropertiesByAccountId($analytics, $account_id){
    return $analytics->management_webproperties->listManagementWebproperties($account_id);
  }
  
  private function addAccountUser($analytics, $accountId, $email, $userPermissions = array('COLLABORATE', 'READ_AND_ANALYZE') ){
		// set email
		$userRef = new Google_Service_Analytics_UserRef();
		$userRef->setEmail($email);
		// set permissions for the email
		$permissions = new Google_Service_Analytics_EntityUserLinkPermissions();
		$permissions->setLocal($userPermissions);
		
		// Create the user link.
		$userLink = new Google_Service_Analytics_EntityUserLink();
		$userLink->setPermissions($permissions);
		$userLink->setUserRef($userRef);
		
		// This request creates a new Account User Link.
		try{
			$analytics->management_accountUserLinks->insert($accountId, $userLink);
			return array(
				'success' => true,
				'message' => 'Successfully added '.$email.' to account '.$accountId
			);
		}catch(apiServiceException $e) {
			return array(
				'code' => $e->getCode(),
				'message' => $e->getMessage()
			);
		}catch(apiException $e) {
			return array(
				'code' => $e->getCode(),
				'message' => $e->getMessage()
			);
		}
	}
  
	public function updateClientData(){
		if(isset($_POST['nonce'])){
      if(verify_nonce($_POST['nonce'], $this->session->userdata())){
				$agent_id = $this->session->userdata('agent_id');
        
				$success = $this->ga->update($agent_id, array(
					'client_account_id' => $_POST['accountId'],
					'client_property_id' => $_POST['webPropertyId'],
					'client_profile_id' => $_POST['profileId'],
				));
				if($success){
					echo json_encode(array(
						'success' => true,
						'message' => 'Successfully added your Google Analytics Account',
            'data' => array(
              'account_id' => $_POST['accountId'],
              'property_id' => $_POST['webPropertyId'],
              'profile_id' => $_POST['profileId'],
            ),
					));
				}else{
					echo json_encode(array(
						'success' => false,
						'message' => 'There was a problem adding your account. Please contact support.'
					));
				}
			}
		}
	}
  
  public function addClientGA(){
    $message = 'You did not pass any data.';
    if(isset($_POST)){
      if(isset($_POST['account_id']) && isset($_POST['property_id']) && isset($_POST['profile_id'])){
        $agent_id = ci()->session->userdata("agent_id");
        $success = $this->ga->update($agent_id, array(
					'client_account_id' => $_POST['account_id'],
					'client_property_id' => $_POST['property_id'],
					'client_profile_id' => $_POST['profile_id'],
				));
        if($success){
					echo json_encode(array(
						'success' => true,
						'message' => 'Successfully added your Google Analytics Account'
					));
          return;
				}
      }
    }
    echo json_encode(array(
			'success' => false,
			'message' => $message,
		));
  }
	
	public function addTrackingCode(){
    if(isset($_POST)){
      if(isset($_POST['as_tracking_code'])){
        $agent_id = ci()->session->userdata("agent_id");
        $success = $this->ga->update($agent_id, array(
					'client_tracking_code' => $_POST['as_tracking_code'],
				));
        // header('Content-Type: application/json');
				if($success){
          // header($_SERVER['SERVER_PROTOCOL'] . ' ' . 'OK', true, 200);
					echo json_encode(array(
						'success' => true,
						'message' => 'Successfully added your tracking code.'
					));
				}else{
          // header($_SERVER['SERVER_PROTOCOL'] . ' ' . 'Internal Server Error', true, 500);
					echo json_encode(array(
						'success' => false,
						'message' => 'There was a problem adding your tracking code, please contact support.'
					));
				}
      }
    }
  }
	
}

class JSON {
  public $vars = array();
  public function set($args = array()){
    $keys = array_keys($args);
    foreach($keys as $key){
      array_push($this->vars, 'var '.$key.' = '.json_encode($args[$key]).';');
    }
  }
  public function render(){
    if(!empty($this->vars)){
      $vars = "";
      foreach($this->vars as $var){
        $vars .= $var."\n      ";
      }
      echo $vars;
    }
  }
}

if(!function_exists('create_nonce')){
  function create_nonce($userdata = array()) {
    if(isset($userdata['__ci_last_regenerate'])){
      return password_hash(
        md5($userdata['__ci_last_regenerate'] .
        $userdata['user_id'] . 
        $userdata['logged_in_auth'])
      , PASSWORD_DEFAULT);
    }
    return false;
  }
}

if(!function_exists('verify_nonce')){
  function verify_nonce($nonce, $userdata){
    $handshake = md5($userdata['__ci_last_regenerate'] .
        $userdata['user_id'] . 
        $userdata['logged_in_auth']);
    if(password_verify($handshake, $nonce)){
      return true;
    }
    return false;
  }
}
?>
