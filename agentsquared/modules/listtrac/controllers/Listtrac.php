<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Listtrac extends MX_Controller {
	
	public function __construct(){
		parent::__construct();
    
		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');
    
    //$this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    
		$this->load->model('agent_sites/Agent_sites_model', 'domain');
		$this->load->model('listtrac/Listtrac_model', 'listtrac');
    $this->listtrac->createTable();
	}
  
  public function index(){
    $domain = $this->domain->fetch_domain_info();
    $data = array();
    $data['agent_id'] = $this->session->userdata("agent_id");
    
    // checks default values
    $settings = $this->listtrac->get($data['agent_id']);
    $columns = $this->listtrac->columns();
    if($settings){
      $data['account_id'] = $settings->account_id;
      foreach($columns as $column){
        $column['value'] = $settings->{$column['key']};
        $data['settings'][$column['key']] = $column;
      }
    }else{
      $data['account_id'] = '';
      foreach($columns as $column){
        $data['settings'][$column['key']] = $column;
      }
    }
    
    $this->form_validation->set_rules('l_account_id', 'Listtrac Account ID', 'required');
    
    if($this->form_validation->run() !== FALSE){
      if(isset($_POST)){
        $_data = array();
        $_data['agent_id'] = $data['agent_id'];
        
        foreach($this->listtrac->fields() as $field){
          if($field['key'] == 'account_id'){
            $_data[$field['key']] = $this->input->post('l_'.$field['key']);
          }else{
            $_val = $this->input->post('l_'.$field['key']);
            $_data[$field['key']] = ($_val == 'on')? 1: 0;
          }
        }
        
        if($this->listtrac->save($_data)){
          $this->session->set_flashdata('msg','<div class="alert alert-success">Update Listtrac settings successful!</div>');
          redirect('/listtrac');
        }else{
          $this->session->set_flashdata('msg','<div class="alert alert-danger">There was a problem saving your data. Please contact support.</div>');
          redirect('/listtrac');
        }
      }
    }
    $this->load->view('listtrac/listtrac_view', $data);
  }
  
}

?>