<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap2-toggle.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap2-toggle.min.js"></script>
<style>
  .main-content{
    margin: 40px 0;
  }
  .main-content:after, #form-listtrac > div:after{
    content: '';
    width: 100%;
    display: block;
    clear: both;
  }
  #l-account-info tbody td:first-child{
    padding-right: 30px;
  }
  #l-settings{
    width: 100%;
    display: block;
  }
  #l-settings thead td:first-child{
    width: 250px;
  }
  #l-settings tbody th,
  #l-settings tbody td{
    padding: 10px 0;
  }
</style>
<!-- Full Width Column -->
<div class="loader"></div>
<div class="content-wrapper">

  <div class="page-title">
    <h3 class="heading-title">
      <span>Listtrac Settings</span>
    </h3>
  </div>
  <?php //printa($settings);?>
  <div class="main-content" style="padding: 0 20px;">
    <?php echo $this->session->flashdata('msg'); ?>
    <?php
      $attributes = array("name" => "listtracForm");
			echo form_open('/listtrac', $attributes);
    ?>
      <div class="col-md-6">
        <table id="l-account-info">
          <tbody>
            <tr>
              <td>Listtrac Account ID</td>
              <td>
                <input type="text" name="l_account_id" placeholder="Place account ID here." class="form-control" value="<?php echo (!empty($account_id))? $account_id: 'asq_100627';?>"/>
                <span class="text-danger"><?php echo form_error('l_account_id'); ?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-6">
        <table id="l-settings">
          <thead>
            <tr>
              <td>Tracking Events</td>
              <td>Toggle</td>
            </tr>
          </thead>
          <tbody>
            <?php foreach($settings as $setting) {?>
              <tr>
                <th><?php echo $setting['title'];?></th>
                <td>
                  <input id="l-<?php echo $setting['key'];?>" type="checkbox" name="l_<?php echo $setting['key'];?>" <?php echo ($setting['value'])? 'checked ':'';?>data-toggle="toggle" data-size="normal" data-onstyle="primary" data-width="60" data-height="35"/>
                </td>
              </tr>
            <?php }//endforeach?>
          </tbody>
        </table>
      </div>
      <div class="col-md-12 text-right" style="margin-top: 40px;">
        <input id="add-listtrac" type="submit" class="btn btn-darkblue" value="Save" style="border-radius: 5px; box-shadow: 0 4px #33529e; margin-right: 30px;"/>
      </div>
    <?php echo form_close(); ?>
  </div>
</div>
<?php $this->load->view('session/footer'); ?>