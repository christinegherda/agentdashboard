<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listtrac_model extends CI_Model{ 

  function __construct() {
    parent:: __construct();
    
    $this->load->helper('form');
    //$this->load->db();
    $this->load->dbforge();
  }
	// database creation depends on this function.
  // add options here. dont forget to update database with that new column
  public function columns(){
    return array(
      array( 'key' => 'pageview', 'title' => 'Page View', 'value' => 0),
    );
  }
  
  public function fields(){
    return array_merge(array(
      array( 'key' => 'account_id', 'title' => 'Listtrac Account', 'value' => ''),
    ), $this->columns());
  }
  
  public function createTable(){
    if(!$this->db->table_exists('users_listtrac')){
			// Table structure for table 'users_ga'
      $fields = array(
				'id' => array(
					'type' => 'MEDIUMINT',
					'constraint' => '8',
					'unsigned' => TRUE,
					'auto_increment' => TRUE
				),
				'agent_id' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
				),
				'account_id' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
				),
			);
      
      foreach($this->columns() as $column){
        $fields[$column['key']] = array(
					'type' => 'TINYINT',
					'default' => $column['value']
				);
      }
      
			$this->dbforge->add_field($fields);
			$this->dbforge->add_key('id', TRUE);
			return $this->dbforge->create_table('users_listtrac');
		}else{
      return false;
    }
  }
  
  public function get($agent_id = ''){
    if($agent_id == ''){
			return false;
		}
    $this->db->where('agent_id', $agent_id);
    $query = $this->db->get('users_listtrac');
    if( $query->num_rows() > 0 ){
			return $query->row();
		}
		return false;
  }
  
  public function save($data = array()) {
    if(!empty($data)){
      if(array_key_exists('agent_id', $data) && array_key_exists('account_id', $data)){
        if($this->check($data['agent_id'])){
          $this->db->where('agent_id', $data['agent_id']);
          return $this->db->update('users_listtrac', $data);
        }else{
          return $this->db->insert('users_listtrac', $data);
        }
      }
    }
    return false;
  }
  
  public function check($agent_id = ''){
    if($agent_id == ''){
			return false;
		}
    $this->db->where('agent_id', $agent_id);
    $query = $this->db->get('users_listtrac');
    if( $query->num_rows() > 0 ){
			return true;
		}
		return false;
  }
}

