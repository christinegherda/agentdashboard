<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Single_property_listings_model extends CI_Model { 

	public function __construct() {
        parent::__construct();
    }

    public function get_your_properties_listings($counter = FALSE, $datas = array()) {

        $this->db->select("*")
                ->from("property_activated")
                ->where('list_agentId', $datas["mlsID"]);

        return $this->db->get()->result();
    }

    public function create_spw_site($data = NULL) {
        $ret = false;

         $this->db->select("property_id")
                ->from("property_activated")
                ->where("list_agentId",$this->session->userdata("agent_id"))
                ->where("property_id",$data['property_id'])
                ->limit(1);

        $query = $this->db->get();
        //printA($query);exit;

        if($query->num_rows() > 0) {
            $ret = true; 
        } else {

            if($data && $data['listingid']) {
                $this->db->insert("property_activated", $data);
                $ret = true;
            }
        }

        
        return $ret;
    }

    public function upgrade_spw_site($data = NULL) {
        $ret = false;
        if($data && $data['listingid']) {
            $this->db->where('ListingID', $data['listingid']);
            $this->db->update('property_activated', $data);
            $ret = true;
        }  
        return $ret;
    }

    public function check_agent() {

        $ret = FALSE;

        $query = $this->db->get_where("domains", array("agent" => $this->session->userdata("user_id")));

        if($query->num_rows() > 0) {
            $ret = TRUE;
        }

        return $ret;
    }

    public function fetch_domain_info() {

        $this->db->select("*")
                ->from("domains")
                ->where('agent', $this->session->userdata('user_id'));

        return $this->db->get()->result();

    }

    public function fetch_spw_domain__info() {

        $this->db->select("*")
                ->from("domains");

        return $this->db->get()->result();

    }

    public function add_existing_domain($post = array()) {
        $this->db->select("domains")
                ->from("domains")
                ->where("domains",$post["domain_name"])
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return array('success' => FALSE, 'type' => "exist_domain", 'error' => 'Selected domain is already reserved by agent squared!');      
        } else {
            $domain['domains']  = $post["domain_name"];
            $domain["agent"] = $this->session->userdata('user_id');
            $domain["date_created"] = date("Y-m-d h:m:s");
            $domain["status"] = "pending";
            $domain["type"] = "agent_site_domain";
            $domain["existing_domain"] = 1;
            $domain["is_subdomain"] = 0;

            if(!$this->check_agent()) {

                $this->db->insert("domains", $domain);

                if($this->db->insert_id()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            } else {
                $this->db->where("agent", $this->session->userdata("user_id"));
                $this->db->update("domains", $domain);

                if($this->db->affected_rows()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            }
        }
    }

    public function add_subdomain($post = array()) {
        $this->db->select("domains")
                ->from("domains")
                ->where("domains",$post["domain_name"])
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return array('success' => FALSE, 'type' => "exist_domain", 'error' => 'Selected domain is already reserved by agent squared!');      
        } else {
            $domain['domains']  = $post["domain_name"];
            $domain["agent"] = $this->session->userdata('user_id');
            $domain["date_created"] = date("Y-m-d h:m:s");
            $domain["status"] = "registered";
            $domain["type"] = "agent_site_domain";
            $domain["existing_domain"] = 0;
            $domain["is_subdomain"] = 1;
            $domain["subdomain_url"] = "http://".$post['domain_name'];

            if(!$this->check_agent()) {

                $this->db->insert("domains", $domain);

                if($this->db->insert_id()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            } else {
                $this->db->where("agent", $this->session->userdata("user_id"));
                $this->db->update("domains", $domain);

                if($this->db->affected_rows()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            }
        }
    }

    public function add_purchase_domain($post = array())
    {
        $this->db->select("domains")
                ->from("domains")
                ->where("domains", $post["domain_name"])
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return array('success' => FALSE, 'type' => "exist_domain", 'error' => 'Selected domain is already reserved by agent squared!');      
        } else {
            if(isset($post["type"]) && $post["type"] == "spw") {
                $domain['domains']  = $post["domain_name"];
                $domain["agent"] = $this->session->userdata('user_id');
                $domain["user_name"] = $post["spw_username"];
                $domain["date_created"] = date("Y-m-d h:m:s");
                $domain["status"] = "pending";
                $domain["type"] = "single_property_website";
                $domain["existing_domain"] = 0;
                $domain["is_subdomain"] = 0;

                $this->db->select("domains")
                            ->from("domains")
                            ->where("user_name", $post["spw_username"])
                            ->limit(1);

                $uname_exist = $this->db->get();

                if($uname_exist->num_rows() > 0) {
                    
                    $this->db->where("user_name", $post["spw_username"]);
                    $this->db->update("domains", $domain);

                    if($this->db->affected_rows()) {
                        $response["message"] = "Domain info has been successfully added!";
                        $response["success"] = TRUE;
                        return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                    }   
                    else {
                        return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                    }

                } else {
                    $this->db->insert("domains", $domain);

                    if($this->db->insert_id()) {
                        $response["message"] = "Domain info has been successfully added!";
                        $response["success"] = TRUE;
                        return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 
                    }   
                    else {
                        return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                    }
                }
            } else {
                $domain['domains']  = $post["domain_name"];
                $domain["agent"] = $this->session->userdata('user_id');
                $domain["date_created"] = date("Y-m-d h:m:s");
                $domain["status"] = "pending";
                $domain["type"] = "agent_site_domain";
                $domain["existing_domain"] = 0;
                $domain["is_subdomain"] = 0;

                if(!$this->check_agent()) {
                    $this->db->insert("domains", $domain);

                    if($this->db->insert_id()) {
                        $response["message"] = "Domain info has been successfully added!";
                        $response["success"] = TRUE;
                        return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 
                    }   
                    else {
                        return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                    }
                } else {
                    $arr = array("agent" => $this->session->userdata("user_id"), "type" => "agent_site_domain");
                    //$this->db->where("agent", $this->session->userdata("user_id"));
                    $this->db->where($arr);
                    $this->db->update("domains", $domain);

                    if($this->db->affected_rows()) {
                        $response["message"] = "Domain info has been successfully added!";
                        $response["success"] = TRUE;
                        return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                    }   
                    else {
                        return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                    }
                }
            }
        }
    }

    public function send_notification_to_admin( $datas = array() )
    {
        $this->load->library("email");

        $content = "Hi Admin <br><br>

            You have new requested domain. Please check the details below:<br><br>

            Name: ".$datas['user_name']." <br>
            Domain Name: ".$datas['domain_name']." <br><br>

            Best Regards,<br><br>

            Agent Squared Management
        ";

        $subject ="New Domain Request" ;

        $this->email->send_email(
            'automail@agentsquared.com',
            'admin',
            'rolbru12@gmail.com',
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "troy@agentsquared.com,albert@agentsquared.com,paul@agentsquared.com,joce@agentsquared.com,raul@agentsquared.com"
            )
        );
    }

    public function update_agent_domain_status_post() {
        $update["status"] = "registered";
        $this->db->where('agent', $this->session->userdata("user_id"));
        $this->db->update("domains", $update);

        if( $this->db->affected_rows() > 0 )
        {
           return TRUE;
        }

        return FALSE;
    }

    public function is_reserved_domain() {   

        $this->db->select("id,domains,status,existing_domain,is_subdomain");
        $this->db->from("domains");
        $this->db->where("type", "agent_site_domain");
        $this->db->where("agent", $this->session->userdata("user_id"));
        $this->db->limit(1);

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            $data = $query->row();
            return array('success' => TRUE, 'domain_id' => $data->id, 'domain_name' => $data->domains, 'status' => $data->status, 'domain_exist' => $data->existing_domain, 'is_subdomain' => $data->is_subdomain);       
        }

        return array('success' => FALSE); 
    }

    public function get_idx( $user_id = NULL ) {
        return $this->db->select("*")
                ->where("user_id", $user_id)
                ->limit(1)
                ->get("property_idx")->row();

    }

    public function isLaunch( $infos = array() ) {
        $this->db->select("*")
                ->from("users")
                ->where("agent_id", $infos->UserId);

        $query = $this->db->get();

        if( $query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }   

    public function is_domain_free() {

        $this->db->select("id")
                ->from("domains")
                ->where("agent", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? FALSE: TRUE;
    }
     public function get_branding_infos()
    {
        $this->db->select("code,site_name,tag_line,banner_tagline,about_agent,agent_photo,logo,favicon,first_name,last_name,phone,mobile,city,address,zip,email,broker,broker_number,license_number,company,theme")
                ->from("users")
                ->where("id", $this->session->userdata("user_id"))
                ->limit(1);

        return $this->db->get()->row();
    }

    public function get_domain() {

        $this->db->select("domains AS name")
                ->from("domains")
                ->where("agent", $this->session->userdata("user_id"))
                ->limit(1);

        $domain = $this->db->get()->row();

        if( $domain )
            $domain = $domain->name;
        else
            $domain = '';

        return $domain;
    }

    public function get_subdomain() {

        $this->db->select("subdomain_url AS subdomain")
                ->from("domains")
                ->where("agent", $this->session->userdata("user_id"))
                ->limit(1);

        $domain = $this->db->get()->row();

        if( $domain )
            $domain = $domain->subdomain;
        else
            $domain = '';

        return $domain;
    }

     public function get_domain_status() {

        $this->db->select("status")
                ->from("domains")
                ->where("agent", $this->session->userdata("user_id"))
                ->limit(1);

        $result = $this->db->get()->row();

        if( $result )
            $domain_status = $result->status;
        else
            $domain_status = '';

        return $domain_status;
    }

    public function get_created_spw($agent_id=NULL) {

        if(!empty($agent_id)) {

            $this->db->select('pa.ListingID, property_id')
                    ->from('property_activated pa')
                    ->join('agent_subscription as', 'as.UserId=pa.list_AgentId', 'left')
                    ->where('pa.list_AgentId', $agent_id)
                    ->where('as.EventType', 'purchased');

            return $this->db->get()->result();
            
        }

        return FALSE;

    }

    public function getCaptureLeads($agent_id) {

        $this->db->select('spw_capture_leads')
                ->from('users')
                ->where('agent_id', $agent_id);

        return $this->db->get()->row();
    }

    public function getLeadsConfig() {

        return $this->db->select('view_site_spw, minutes_pass_spw, minute_capture_spw, allow_skip_spw')
                        ->from('leads_config')
                        ->where('user_id', $this->session->userdata('user_id'))
                        ->get()->row();

    }

    public function setLeadCaptureConfig($data) {
        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('users', $data);
    }

    public function saveLeadCaptureSettings(Array $data) {

        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->update('leads_config', $data);

        return ($this->db->affected_rows()) ? TRUE : FALSE;

    }
}
