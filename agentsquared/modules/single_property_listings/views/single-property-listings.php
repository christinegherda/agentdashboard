<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span>Single Property Listings</span>
    </h3>
    <p>Create and manage your Single Property websites</p>
  </div>

  <div class="col-md-8" >
      <?php
        $mess = $this->session->flashdata('to_pay_mess');
        if(isset($mess) && !empty($mess) ) { ?>
          <div class="alert alert-warning"><i class="fa fa-2x fa-info-circle"></i>  <span class="no-created-sites"> <?php echo $mess; ?></span></div>
      <?php } ?>
      <br /><br />
  </div>

  <section class="content single-property-listing">
    <div class="row">
      <div class="col-md-6">
        <div class="capture-leads-header">
          <div class="item-container-spw">
            <div class="item item-1">
              <h4>
                Capture Leads:
                <a href="javascript:void(0)" data-toggle="tooltip" title="Toggle ON to Capture Leads">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                </a>
                <input type="checkbox" name="my-checkbox" data-url="<?php echo base_url(); ?>single_property_listings/setLeadCapture">
              </h4>
            </div>
            <div class="item item-2">
              <button type="button" class="btn btn-info" id="lead_config_btn_spw" data-toggle="modal" data-target="#lead_config_spw_modal">
                <span class="glyphicon glyphicon-cog"></span> Lead Capture Settings
              </button>
              <span>
                <a href="javascript:void(0)" data-toggle="tooltip" title="Capture Leads Must be toggled ON to enable lead capture business rules, which allow you force users to register to use site.">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                </a>
              </span>
            </div>
          </div>
        </div>
        <h3 class="section-title">Sites Created</h3>
        <!-- Active Listings -->
        <div class="col-md-12 col-sm-12 col-xs-12 box-gray">
          <p class="box-title">Active Websites</p>
          <?php
            $count=0;
            if($active_properties) {
              foreach($active_properties as $active_prop) {
                foreach($spw_properties as $property) {
                  if($active_prop->StandardFields->ListingId == $property->ListingID) {
                      $count++;

                      $encrypted_name = $property->username."-6231vegdywe82372y434324-".$property->list_agentId;
                      $encrypted_key = $property->list_agentId."-".$property->password."-apiweb7342u349210";

                      $domainComplete = FALSE;

                      if(isset($spw_domain_info)) {
                        foreach($spw_domain_info as $domain) {
                          if($domain->user_name == $property->ListingID || $domain->user_name == $property->username) {
                            if($domain->status == "completed") {
                              $domainComplete = TRUE;
                            }
                          }
                        }
                      } ?>
                      <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
                        <div class="property-site" <?php if($property->spw_version != 2) { echo 'style="height:366px;"'; } elseif($property->isupgrade) { if(!$domainComplete) { echo 'style="height:325px;"'; } } ?>>
                          <div class="property-img">
                            <a href="javascript:;"><img src="<?php if($active_prop->StandardFields->Photos[0]->Uri300){ echo $active_prop->StandardFields->Photos[0]->Uri300; } else { echo $property->primary_photo; } ?>" alt="" class="img-responsive"></a>
                            <?php
                              if(isset($spw_domain_info)) {
                                foreach($spw_domain_info as $domain) {
                                  if($domain->user_name == $property->ListingID || $domain->user_name == $property->username) { ?>
                                    <br>
                                    <div class="domain-info">
                                      <p style="color:#FFF;">You have already selected a domain name <span class="label label-info"><?php echo $domain->domains; ?></span>. Domain Status: <span class="label label-info"><?php echo $domain->status; ?></span>.</p>
                                    </div>
                              <?php
                                  }
                                }
                              }
                            ?>
                          </div>
                          <?php if(isset($active_prop->StandardFields->MlsStatus)){?>
                            <div class="property-listing-status">
                              <?php echo $active_prop->StandardFields->MlsStatus;?>
                            </div>
                          <?php  } ?>
                          <div class="property-desc">
                            <p class="property-name"><?php echo $property->unparsedaddr; ?></p>
                            <p class="property-type">Type: <?php echo $property->property_type; ?></p>
                            <div class="property-btn">
                              <?php
                                $suggested_domain = $property->unparsedaddr."-".$active_prop->StandardFields->City."-".$active_prop->StandardFields->StateOrProvince;
                                $suggested_domain = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $suggested_domain));
                                if(!$domainComplete) {
                                  $siteDir = str_replace(' ', '-', $property->unparsedaddr);
                                  $siteDir = preg_replace('/[^A-Za-z0-9\-]/', '', $siteDir);
                                  ?>
                                  <!-- <div class="col-lg-7 col-md-12 col-sm-6 col-xs-6 padding-2px">
                                    <a href="javascript:;" data-toggle="modal" data-target="#domainmodal" class="btn btn-block text-center btn-blue custom_domain_button" type="button" data-username="<?php echo $property->ListingID; ?>" data-domain-suggest="<?php echo $suggested_domain; ?>" data-domain-directory="<?php echo $siteDir;?>" data-domain-city="<?php echo $active_prop->StandardFields->City;?>" data-domain-state="<?php echo $active_prop->StandardFields->StateOrProvince;?>">Custom Domain</a>
                                  </div> -->
                              <?php
                                }
                              ?>
                              <?php
                              //get spw url in domains table
                              $listingid = $active_prop->StandardFields->ListingId;
                              $property_name = url_title($active_prop->StandardFields->UnparsedFirstLineAddress, '-', TRUE);
                              $property_uri = implode(
                                  '~',
                                  array(
                                      $property_name,
                                      $listingid,
                                      $this->session->userdata("user_id")
                                  )
                              );

                              if(isset($domain_info[0]) && !empty($domain_info[0])){

                                  if($domain_info[0]->status == "completed"){
                                    $spw_url = $base_url."property-site/".$property_uri;
                                  } else {
                                    $spw_url = $domain_info[0]->subdomain_url."/property-site/".$property_uri;
                                  }
                                }
                              ?>
                              <div class="col-lg-5 col-md-12 col-sm-6 col-xs-6 padding-2px margintablettop-5"><a href="<?php echo $spw_url; ?>" target="_blank" class="btn btn-block btn-gray">View Site</a></div>
                              <?php
                                  if($property->spw_version == 2) {
                                    if($property->isupgrade) {
                                      if(!$domainComplete) { ?>
                                      <div class="col-md-12 col-sm-12 col-xs-12 padding-2px" style="margin-top:5px;"><a href="<?php echo $property->site_url_old; ?>" target="_blank" class="btn btn-block button-link">View Old Site</a></div>
                              <?php
                                      } else { ?>
                                        <div class="col-md-5 col-sm-6 col-xs-6 padding-2px"><a href="<?php echo $property->site_url_old; ?>" target="_blank" class="btn btn-block button-link" style="text-align:center;">View Old Site</a></div>
                              <?php
                                      }
                                    }
                                  } else {
                                    foreach($active_properties as $active) {
                                      if($active->StandardFields->ListingId == $property->ListingID) { ?>

                                        <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px 3px;"><small>Your site is in version 1. If you want to upgrade to version 2, click upgrade button.</small></div>
                                        <form action="<?php echo base_url(); ?>single_property_listings/upgrade_spw_v2" method="post" id="upgrade_spw_form">
                                          <input type="hidden" name="ListingKey" value='<?php echo $active->StandardFields->ListingKey; ?>'>
                                          <input type="hidden" name="site_url_old" value='<?php echo $property->site_url; ?>'>
                                          <input type="hidden" name="primary_photo" value='<?php if(isset($active->StandardFields->Photos[0]->Uri300)) { echo $active->StandardFields->Photos[0]->Uri300;} ?>'>
                                          <input type="hidden" name="list_agentId" value='<?php echo $account_info->Id; ?>'>
                                          <input type="hidden" name="unparsedaddr" value="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>">
                                          <input type="hidden" name="listingid" value="<?php echo $active->StandardFields->ListingId;?>">
                                          <input type="hidden" name="address" value="<?php echo $active->StandardFields->UnparsedAddress; ?>">
                                          <input type="hidden" name="property_type" value="<?php echo $active->StandardFields->PropertyClass; ?>">
                                          <input type="hidden" name="price" value="<?php echo $active->StandardFields->CurrentPrice; ?>">
                                          <input type="hidden" name="broker" value="<?php if(!empty($active->StandardFields->ListOfficeName) ){ echo $active->StandardFields->ListOfficeName;} else{ echo "";} ?>">
                                          <input type="hidden" name="broker_number" value="<?php if(!empty($active->StandardFields->ListOfficePhone) ){ echo $active->StandardFields->ListOfficePhone;} else{ echo "";} ?>">
                                          <input type="hidden" name="property_status" value="<?php if(!empty($active->StandardFields->MlsStatus) ){ echo $active->StandardFields->MlsStatus;} else{ echo "";} ?>">
                                          <div class="col-md-12 col-sm-12 col-xs-12 padding-2px"><button class="btn btn-block btn-light-green" type="submit" data-ListingID="<?php echo $property->ListingID; ?>" style="text-align: center; margin-top:5px;"><i class="fa fa-puzzle-piece"></i> Upgrade </button></div>
                                        </form>
                                    <?php
                                      }
                                    }
                                  }
                                ?>
                            </div>
                          </div>
                        </div>
                      </div>
          <?php
                    }
                  }
                }
              }
            if($count == 0) { ?>
              <div class="col-md-12">
                <div class="col-md-12 create-site">
                   <div class="alert alert-success"><i class="fa fa-2x fa-info-circle"></i>  <span class="no-created-sites"><?= ($property_count>0) ? "You have (".$property_count.") sites to be created." : "You have no active properties."?><!-- No Sites Created, please create a site below. --></span></div>
                </div>
              </div>
          <?php
            }
          ?>
        </div>
        <!-- Sold Listings -->
        <div class="col-md-12 col-sm-12 col-xs-12 no-padding mt-33px accordion-property">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="soldproperty">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Inactive Websites
                    <span class="pull-right">
                      <i class="icon icon-caret-down"></i>
                    </span>
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="soldproperty">
                <div class="panel-body">
                  <?php
                    if($sold_properties) {
                      foreach($sold_properties as $sold_prop) {
                        foreach($spw_properties as $property) {
                          if($sold_prop->StandardFields->ListingId == $property->ListingID) {

                              $encrypted_name = $property->username."-6231vegdywe82372y434324-".$property->list_agentId;
                              $encrypted_key = $property->list_agentId."-".$property->password."-apiweb7342u349210"; ?>

                              <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
                                <div class="property-site">
                                  <div class="property-img">
                                    <a href="javascript:;"><img src="<?= $property->primary_photo;?>" alt="" class="img-responsive"></a>
                                    <?php
                                      $domainComplete = FALSE;
                                      if(isset($spw_domain_info)) {
                                        foreach($spw_domain_info as $domain) {
                                          if($domain->user_name == $property->ListingID || $domain->user_name == $property->username) { ?>
                                            <br>
                                            <div class="domain-info">
                                              <p style="color:#FFF;">You have already selected a domain name <span class="label label-info"><?php echo $domain->domains; ?></span>. Domain Status: <span class="label label-info"><?php echo $domain->status; ?></span>.</p>
                                            </div>
                                      <?php
                                            if($domain->status == "completed") {
                                              $domainComplete = TRUE;
                                            }
                                          }
                                        }
                                      }
                                    ?>
                                  </div>
                                  <?php if(isset($sold_prop->StandardFields->MlsStatus)){?>
                                     <div class="property-listing-status">
                                        <?php echo $sold_prop->StandardFields->MlsStatus;?>
                                    </div>
                                  <?php  } ?>
                                  <div class="property-desc">
                                    <p class="property-name"><?php echo $property->unparsedaddr; ?></p>
                                    <p class="property-type">Type: <?php echo $property->property_type; ?></p>
                                    <div class="property-btn">
                                      <?php
                                        $suggested_domain = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $property->unparsedaddr));
                                        if(!$domainComplete) {
                                          $siteDir = str_replace(' ', '-', $property->unparsedaddr);
                                          $siteDir = preg_replace('/[^A-Za-z0-9\-]/', '', $siteDir);
                                          ?>
                                          <!-- <div class="col-md-6 col-sm-6 col-xs-6 padding-2px">
                                            <a href="javascript:;" data-toggle="modal" data-target="#domainmodal" class="btn btn-block btn-blue custom_domain_button" type="button" data-username="<?php echo $property->ListingID; ?>" data-domain-suggest="<?php echo $suggested_domain; ?>" data-domain-directory="<?php echo $siteDir;?>">Custom Domain</a>
                                          </div> -->
                                      <?php
                                        }
                                      ?>
                                      
                                      <?php 
                                      //get spw url in domains table
                                      $listingid = $sold_prop->StandardFields->ListingId;
                                      $property_name = url_title($sold_prop->StandardFields->UnparsedFirstLineAddress, '-', TRUE);
                                      $property_uri = implode(
                                          '~',
                                          array(
                                              $property_name,
                                              $listingid,
                                              $this->session->userdata("user_id")
                                          )
                                      );

                                      if(isset($domain_info[0]) && !empty($domain_info[0])){

                                          if($domain_info[0]->status == "completed"){
                                            $spw_url = $base_url."property-site/".$property_uri;
                                          } else {
                                            $spw_url = $domain_info[0]->subdomain_url."/property-site/".$property_uri;
                                          }
                                        }
                                      ?>
                                      <div class="col-md-6 col-sm-6 col-xs-6 padding-2px"><a href="<?php echo $spw_url ?>" target="_blank" class="btn btn-block btn-gray">View Site</a></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                  <?php
                            }
                          }
                        }
                      }
                  ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 create-sites-property">
      <h3 class="section-title">
        Create Sites
      </h3>
      <div id="single-prop-pagination" class="col-md-12 box-gray box-sites">
        <?php
          $siteCount=0;
          if(isset($active_properties) AND !empty($active_properties)) {
            foreach($active_properties as $active) {

              $iActivated = 1;
              $pData = null;

              foreach($spw_properties as $property) {
                if($active->StandardFields->ListingId == $property->ListingID) {
                  $iActivated = 0;
                  $pData = $property;
                }
              }
              if($iActivated) { $siteCount++;?>
                <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
                  <div class="property-site create-site">
                    <div class="property-img">
                      <?php
                          if($active->StandardFields->Photos) {
                      ?>
                          <a href="<?php echo $base_url; ?>property-details/<?php echo $active->StandardFields->ListingKey;?>" target="_blank"><img src="<?php echo $active->StandardFields->Photos[0]->Uri300; ?>" alt="" class="img-responsive"></a>

                      <?php  } else { ?>
                          <a href="<?php echo $base_url?>property-details/<?php echo $active->StandardFields->ListingKey;?>" target="_blank"><img src="<?= base_url()?>assets/images/dashboard/property1.jpg" alt="" class="img-responsive"></a>
                      <?php  } ?>
                    </div>
                    <?php if(isset($active->StandardFields->MlsStatus)){?>
                       <div class="property-listing-status">
                          <?php echo $active->StandardFields->MlsStatus; ?>
                      </div>
                    <?php } ?>
                    <div class="property-desc">
                      <p class="property-name"><?php echo $active->StandardFields->UnparsedAddress; ?></p>
                      <p class="property-type">Type: <?php echo $active->StandardFields->PropertyClass; ?></p>

                      <input type="hidden" name="ListingKey" value='<?php echo $active->StandardFields->ListingKey; ?>'>
                      <input type="hidden" name="primary_photo" value='<?php if(isset($active->StandardFields->Photos[0]->Uri300)) { echo $active->StandardFields->Photos[0]->Uri300;} ?>'>
                      <input type="hidden" name="list_agentId" value='<?php echo $account_info->Id; ?>'>

                      <div class="property-btn">
                        <div class="col-md-12 col-sm-12 col-xs-12 padding-2px">
                          <form action="<?php echo base_url(); ?>single_property_listings/create_single_listings" data-id="<?=$active->StandardFields->ListingKey;?>" method="post" class="activateForm changes" id="form<?=$active->StandardFields->ListingKey;?>">
                            <input type="hidden" name="ListingKey" value='<?php echo $active->StandardFields->ListingKey; ?>'>
                            <input type="hidden" name="primary_photo" value='<?php if(isset($active->StandardFields->Photos[0]->Uri300)) { echo $active->StandardFields->Photos[0]->Uri300;} ?>'>
                            <input type="hidden" name="list_agentId" value='<?php echo $account_info->Id; ?>'>
                            <input type="hidden" name="unparsedaddr" value="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>">
                            <input type="hidden" name="listingid" value="<?php echo $active->StandardFields->ListingId;?>">
                            <input type="hidden" name="address" value="<?php echo $active->StandardFields->UnparsedAddress; ?>">
                            <input type="hidden" name="state" value="<?php echo $active->StandardFields->StateOrProvince; ?>">
                            <input type="hidden" name="postal_city" value="<?php echo $active->StandardFields->PostalCity; ?>">
                            <input type="hidden" name="city" value="<?php echo $active->StandardFields->City; ?>">
                            <input type="hidden" name="property_type" value="<?php echo $active->StandardFields->PropertyClass; ?>">
                            <input type="hidden" name="price" value="<?php echo $active->StandardFields->CurrentPrice; ?>">
                            <input type="hidden" name="broker" value="<?php if(!empty($active->StandardFields->ListOfficeName) ){ echo $active->StandardFields->ListOfficeName;} else{ echo "";} ?>">
                            <input type="hidden" name="broker_number" value="<?php if(!empty($active->StandardFields->ListOfficePhone) ){ echo $active->StandardFields->ListOfficePhone;} else{ echo "";} ?>">
                            <input type="hidden" name="property_status" value="<?php if(!empty($active->StandardFields->MlsStatus) ){ echo $active->StandardFields->MlsStatus;} else{ echo "";} ?>">
                            <!-- New version of purchasing spw -->
                            <?php if($isFreeSPW) { ?>
                                <button class="btn btn-block btn-light-green create_spw_btn" type="button" data-form-id="form<?=$active->StandardFields->ListingKey;?>"><i class="fa fa-puzzle-piece"></i> Create Website </button>
                              <?php } else { ?>
                                <button type="submit" class="btn btn-block btn-light-green create_spw-<?=$active->StandardFields->ListingKey;?>" type="submit"><i class="fa fa-puzzle-piece"></i> Create Website    <i style="display:none;" class="create-spw-loading-<?=$active->StandardFields->ListingKey;?> fa fa-circle-o-notch fa-spin"></i></button>
                              <?php } ?>
                            <!--
                            <?php if($has_purchased_spw) {
                                if($isFreeSPW) { ?>
                                  <button class="btn btn-block btn-light-green create_spw_btn" type="button" data-form-id="form<?=$active->StandardFields->ListingKey;?>"><i class="fa fa-puzzle-piece"></i> Create Website </button>
                                <?php } else { ?>
                                  <button class="btn btn-block btn-light-green" type="submit"><i class="fa fa-puzzle-piece"></i> Create Website </button>
                                <?php } ?>
                            <?php } else { ?>
                              <button class="btn btn-block btn-light-green create_spw_btn" type="button" data-form-id="form<?=$active->StandardFields->ListingKey;?>"><i class="fa fa-puzzle-piece"></i> Create Website </button>
                            <?php } ?>-->
                            <!-- Old version of purchasing spw
                            <?php if($has_purchased_spw) { ?>
                              <button class="btn btn-block btn-light-green" type="submit"><i class="fa fa-puzzle-piece"></i> Create Website </button>
                            <?php } else { ?>
                              <a href="http://www.sparkplatform.com/appstore/apps/single-property-website-social-media-sharing-tool" class="btn btn-block btn-light-green" target="_blank"><i class="fa fa-puzzle-piece"></i> Create Website</a>
                            <?php } ?>
                            -->
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            <?php
              }
            }?>

            <div class="col-md-12">
              <?php if( isset($ajax_pagination) ) : ?>
                  <div class="pagination-area pull-right">
                      <nav>
                          <ul class="pagination">
                              <?php echo $ajax_pagination; ?>
                          </ul>
                      </nav>
                  </div>
              <?php endif; ?>
          </div>
           <?php if($siteCount == 0) {
              echo '<div class="alert alert-success"><i class="fa fa-2x fa-info-circle"></i>  <span class="no-available-sites">You have no available sites to be created...</span></div>';
            }
          } else {
              echo '<div class="alert alert-success"><i class="fa fa-2x fa-info-circle"></i>  <span class="no-available-sites">You have no available sites to be created...</span></div>';
          }
        ?>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="col-md-12 no-padding">
  <div class="modal fade modal-fullscreen custom-domain-modal" id="domainmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <!-- Domains -->
          <div role="tabpanel" class="tab-pane" id="domains">
            <div class="page-title-container">
                <div class="col-md-12">
                    <h3>Create Custom Domain</h3>
                </div>
            </div>
            <div class="col-md-12 text-center">
              <div class="domain-container" style="margin-top: 16px">
                <div class="col-md-offset-3 col-md-6 mt-70">
                  <div class="alert alert-info fade in" id="message-container" style="display:none;">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <span id="return-message"></span>
                  </div>
                  <h4>Sugggested Domain Name: <a id="domain_suggest" href="javascript:;" style="cursor:pointer;"></a></h4>
                  <form method="GET" action="<?php echo base_url(); ?>custom_domain/domains" id="domain-form" class="domain-ajax">
                    <div class="form-group">
                      <input type="hidden" name="domainUrl" value="<?php echo site_url('/custom_domain'); ?>" id="domainUrl">
                      <input type="hidden" name="domain_checker" value="<?php echo site_url('/custom_domain/domains'); ?>?domain_checker=1" id="domain_checker">
                      <input type="hidden" name="user_agent_id" value="<?php $this->session->userdata("user_id"); ?>" id="user_agent_id" >
                      <input type="hidden" name="purchaseDomainUrl" value="<?php echo site_url('/custom_domain/purchase_domain'); ?>" id="purchaseDomainUrl" >
                      <input type="hidden" name="domain_check" value="1">
                      <input type="hidden" name="domain_directory" id="domainDirectory">
                      <input type="hidden" name="city" id="domainCity">
                      <input type="hidden" name="state" id="domainState">
                      <?php if(isset($_GET['domain']) && ($_GET['domain'] == 0) ) { ?>
                          <input type="hidden" name="domain" value="<?php echo $_GET['domain']; ?>" id="getDomain">
                      <?php } ?>
                      <div class="col-sm-10 col-xs-9 padding-0">
                        <input type="text" class="form-control" id="domain_name" name="domain_name" value="<?=isset($_GET["domain"]) ? $_GET["domain"] : ""; ?>" placeholder="Domain Name" required>
                      </div>
                      <div class="col-sm-2 col-xs-3 paddingleft-0">
                        <select id="suffix" class="form-control" name="suffix">
                            <option value="com" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="com") ? 'selected="selected"' : ""; ?> >.com</option>
                            <option value="net" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="net") ? 'selected="selected"' : ""; ?> >.net</option>
                            <option value="org" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="org") ? 'selected="selected"' : ""; ?> >.org</option>
                        </select>
                      </div>
                      <div class="col-md-12 clearfix text-center">
                        <button class="btn btn-default btn-select checkdomainbtn" id="check_domain_sbt" type="submit"><i class="fa fa-search"></i> Check Domain</button>
                      </div>
                    </div>
                  </form>
                  <div class="col-md-offset-1 col-md-10 mt-70"><div id="domainResult" class="domain-group"></div></div>
                  <div id="fetch-available-domains" class="col-md-12" style="display:none;">
                    <h1 class="text-center"><i class="fa fa-cog fa-spin"></i> <b>FETCHING AVAILABLE DOMAINS<br><small>Please wait</small></b></h1>
                  </div>
                  <div id="domainResultAv" class="domain-group" style="display:none;">
                    <div class="availableDomains" style="display:none;">
                      <div class="col-md-12 col-sm-5">
                          <h5>Available Domaine Names:</h5>
                      </div>
                      <ul class="domain-list" style="list-style:none;"></ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- end of domain-->
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="domain_purchase_modal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-width="800">
  <div class="modal-content" style="width: 82%; margin-left: 139px;margin-top: 20px;">
    <div class="modal-header">
      <h1>Reserve a Domain</h1>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body2" style="height: 300px; width: 100%; position: relative; padding: 15px;">
      <p style="font-size: 18px;">Click continue if you are sure this is the domain you want.</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <?php if($is_domain_free) { ?>
        <a type="button" class="btn btn-primary confirm-purchase-domain" data-type="free" data-domain-name="" data-domain-price="">Continue</a>
      <?php } else { ?>
        <a href="javascript:;" class="btn btn-primary direct-to-stripe" type="button" data-type="pay" data-domain-name="" data-domain-price="" >Continue</a>
      <?php } ?>
    </div>
  </div>
</div>

  <div style="margin-left: -17px; overflow-y: scroll; " class="modal fade modal-fullscreen" id="stripeFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="loader"></div>
    <div class="modal-dialog" role="document">
      <div>
        <div class="modal-body">
          <!-- Domains -->
          <div role="tabpanel" class="tab-pane" id="domains">
            <div class="page-title-container">
                <div class="col-md-12">
                    <h3>Payment Form</h3>
                </div>
            </div>
            <div>
              <div>
                <div class="">
                    <div class="container">
                        <hr>
                        <div class="row">
                          <div class="col-md-12">
                            <h4><div class="label label-info">Payment Cart</div></h4>
                            <table class="table table-striped">
                            <thead>
                              <tr>
                                <th></th>
                                <th>Domain Name</th>
                                <th>Description</th>
                                <th>Amount</th>
                                <!-- <th>Surcharge</th> -->
                                <th>Total Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td></td>
                                <td><span id="cart-domain-name"></span></td>
                                <td>pay for domain</td>
                                <td>$<span id="cart-domain-price"></span></td>
                                <!-- <td>$1</td> -->
                                <td>$<span id="cart-total-amount"></span></td>
                              </tr>
                            </tbody>
                          </table>
                          </div>
                        </div>
                        <hr>
                        <br>
                        <div class="row">
                          <div class="col-md-12">
                            <h4><div class="label label-info">Enter Your Card Details</div></h4>
                            </br>
                            <form class="form-horizontal stripe_sbt" id="stripe_sbt" action="" method="POST">
                              <input type="hidden" name="domain_name" value="" id="form_domain_name" >
                              <input type="hidden" name="domain_price" value="" id="form_domain_price" >
                              <input type="hidden" name="type" value="spw">
                              <input type="hidden" name="user_name" value="" id="form_user_name">
                              <input type="hidden" name="site_dir" value="" id="form_site_dir">
                              <input type="hidden" name="city" value="" id="form_site_city">
                              <input type="hidden" name="state" value="" id="form_site_state">

                              <div class="col-md-12"><h3><div class="col-md-12 payment-error label label-danger"></div></h3><br></div>
                              <div class="col-md-12"><h3><div class="col-md-12 payment-message label label-success"></div></h3><br></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-3 control-label">Card Number</label>
                                  <div class="col-sm-8">
                                    <input type="input" class="form-control" size="20"  id="" data-stripe="number" placeholder="Card Number" required >
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="inputPassword3" class="col-sm-3 control-label">CVV</label>
                                  <div class="col-sm-8">
                                    <input type="input" class="form-control" size="4" id="" data-stripe="cvc" placeholder="CVV" required>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-3 control-label">Expiry(MM)</label>
                                  <div class="col-sm-8">
                                    <input type="input" class="form-control" size="2" id="" placeholder="MM" data-stripe="exp-month" required>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputPassword3" class="col-sm-3 control-label">Expiry(YYYY)</label>
                                  <div class="col-sm-8">
                                    <input type="input" class="form-control" id="" size="4" data-stripe="exp-year" placeholder="YYYY" required>
                                  </div>
                                </div>
                            </div>

                          </div>
                        </div>
                        <br/><br/>
                      <div class="row">
                        <div> Powered By: <img src="<?php echo base_url()."assets/images/stripe-logo.png"; ?>" width="10%"> </div>
                      </div>
                    </div>

                </div>
              </div>
            </div>
          </div> <!-- end of domain-->
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" id="pay_now_btn"> Pay Now</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="col-md-12">
<div class="modal fade modal-fullscreen spw-modal" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">510 Willow Place</h4>
      </div> -->
      <div class="modal-body">
        <!-- <iframe src="http://agent-squared-theme/spw-dashboard/" frameborder="0"></iframe> -->

      <nav  class="navbar navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand spw-brand" href="#">1156 Alexandria Dr</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs navbar-nav navbar-left main-menu" role="tablist">
              <li class="active">
                  <a href="#dashboard" aria-controls="dashboard" role="tab" data-toggle="tab">
                      <i class="fa fa-bar-chart" aria-hidden="true"></i><br>
                      Dashboard
                  </a>
                  <div class="arrow-up"></div>
              </li>
              <li>
                  <a  href="#choose-template" aria-controls="choose-template" role="tab" data-toggle="tab">
                      <i class="fa fa-television" aria-hidden="true"></i><br>
                      Choose Template
                  </a>
                  <div class="arrow-up"></div>
              </li>
              <li>
                  <a href="#property-info" aria-controls="property-info" role="tab" data-toggle="tab">
                      <i class="fa fa-info" aria-hidden="true"></i><br>
                      Property Info
                  </a>
                  <div class="arrow-up"></div>
              </li>
              <li>
                  <a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">
                      <i class="fa fa-picture-o" aria-hidden="true"></i><br>
                      Photos
                  </a>
                  <div class="arrow-up"></div>
              </li>
              <!--<li>
                  <a href="#agent-info" aria-controls="agent-info" role="tab" data-toggle="tab">
                      <i class="fa fa-user" aria-hidden="true"></i><br>
                      Agent Info
                  </a>
                  <div class="arrow-up"></div>
              </li>
               <li>
                  <a href="#social-media" aria-controls="social-media" role="tab" data-toggle="tab">
                      <i class="fa fa-share-alt" aria-hidden="true"></i><br>
                      Social Media
                  </a>
                  <div class="arrow-up"></div>
              </li>-->
              <li>
                  <a href="#domains" aria-controls="domains" role="tab" data-toggle="tab">
                      <i class="fa fa-globe" aria-hidden="true"></i><br>
                      Domains
                  </a>
                  <div class="arrow-up"></div>
              </li>
              <li>
                  <a href="http://1942dogwoodlane.com/" target="_blank">
                      <i class="fa fa-eye" aria-hidden="true"></i><br>
                      View Site
                  </a>
                  <div class="arrow-up"></div>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right customer-options">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <div class="col-md-5">
                            <img src="<?=base_url()?>assets/images/dashboard/Brad_Fielding_Print.jpg" alt="" class="img-circle">
                        </div>
                        <div class="col-md-7">
                            <div class="customer-name">
                                <p>Welcome!</p>
                                <p>Bradley</p>
                            </div>
                        </div>
                    </a>
                    <!-- <ul class="dropdown-menu">
                        <li><a href="">Logout</a></li>
                    </ul> -->
                </li>
            </ul>
            </div>
        </div>
      </nav>

      <section class="main-content">
        <div class="container">
            <div class="row">
              <!-- Tab panes -->
              <div class="tab-content">
                <!-- dashboard -->
                <div role="tabpanel" class="tab-pane active" id="dashboard">
                  <div class="col-md-6">
                    <div class="small-box bg-aqua">
                      <div class="inner">
                        <p>Step 1</p>
                        <h3>Create Custom Domain</h3>
                      </div>
                      <div class="icon">
                        <i class="fa fa-globe"></i>
                      </div>
                      <a  href="#domains" aria-controls="domains" role="tab" data-toggle="tab" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="small-box bg-green">
                      <div class="inner">
                        <p>Step 2</p>
                        <h3>Activate Social Media</h3>
                      </div>
                      <div class="icon">
                        <i class="fa fa-share-alt"></i>
                      </div>
                      <a href="#social-media" aria-controls="social-media" role="tab" data-toggle="tab" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                </div>

                <!-- Choose Template -->
                <div role="tabpanel" class="tab-pane" id="choose-template">
                  <div class="choose-template-area">
                      <div class="page-title-container">
                          <div class="col-md-12">
                              <h3>Choose Template</h3>
                              <span class="page-desc">Make your site standout</span>
                          </div>
                      </div>
                      <div class="template-container">
                          <div class="col-md-4">
                              <div class="template-list selected">
                                  <div class="template-screenshot">
                                      <img src="<?=base_url()?>assets/images/dashboard/spw-default-template.jpg" alt="" class="img-responsive">
                                  </div>
                                  <div class="template-select">
                                      <label>Default SPW</label>
                                      <button class="btn btn-default btn-select"><i class="fa fa-check" aria-hidden="true"></i> Active</button>
                                  </div>
                                  <div class="overlay"></div>
                              </div>
                          </div>
                          <!--
                          // If not active template
                          <div class="col-md-4">
                              <div class="template-list">
                                  <div class="template-screenshot">
                                      <img src="<?=base_url()?>assets/images/dashboard/proper.jpg" alt="" class="img-responsive">
                                  </div>
                                  <div class="template-select">
                                      <label>New SPW</label>
                                      <button class="btn btn-default btn-select">Select</button>
                                      <button class="btn btn-default btn-preview">Preview</button>
                                  </div>
                                  <div class="overlay"></div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="template-list">
                                  <div class="template-screenshot">
                                      <img src="<?=base_url()?>assets/images/dashboard/greenery.jpg" alt="" class="img-responsive">
                                  </div>
                                  <div class="template-select">
                                      <label>Newer SPW</label>
                                      <button class="btn btn-default btn-select">Select</button>
                                      <button class="btn btn-default btn-preview">Preview</button>
                                  </div>
                                  <div class="overlay"></div>
                              </div>
                          </div> -->
                      </div>
                  </div>
                </div>

                <!-- property info -->
                <div role="tabpanel" class="tab-pane" id="property-info">
                  <div class="page-title-container">
                      <div class="col-md-12">
                          <h3>Property Info</h3>
                          <span class="page-desc">Customize your site</span>
                      </div>
                  </div>

                  <div class="col-md-12">
                      <form action="" id="property-info-form" class="content-form">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active"><a href="#customizeyoursite" aria-controls="customizeyoursite" role="tab" data-toggle="tab">Customize your site</a></li>
                              <li role="presentation"><a href="#fullspecification" aria-controls="fullspecification" role="tab" data-toggle="tab"> Full Specification</a></li>
                          </ul>
                          <!-- Tab panes -->
                          <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="customizeyoursite">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <h4>Title</h4>
                                          <span>Name of your property</span>
                                          <input type="text" class="form-control" required>
                                      </div>
                                      <div class="form-group">
                                          <h4>Tagline</h4>
                                          <span>Make your pitch</span>
                                          <input type="text" class="form-control" required>
                                      </div>
                                      <div class="form-group">
                                          <h4>Description</h4>
                                          <span>Write everything about the property.</span>
                                          <textarea name="" cols="30" rows="5" class="form-control"></textarea>
                                      </div>
                                      <div class="form-group">
                                          <h4>Address</h4>
                                          <span>Complete address of your property</span>
                                          <input type="text" class="form-control">
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <h4>Property Type</h4>
                                          <span>Select property type</span>
                                          <div class="select-container">
                                              <select name="" class="form-control">
                                                  <option value="" class="" disabled>-- Select Property Type --</option>
                                                  <option value="residential">Residential</option>
                                                  <option value="commercial">Commercial</option>
                                                  <option value="condo">Condo</option>
                                                  <option value="townhome">Townhome</option>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <h4>City</h4>
                                          <span>City where the property is located</span>
                                          <input type="text" class="form-control">
                                      </div>
                                      <div class="form-group">
                                          <h4>State</h4>
                                          <span>State where the property is located</span>
                                          <input type="text" class="form-control">
                                      </div>
                                      <div class="form-group">
                                          <h4>Price</h4>
                                          <span>Price of the property</span>
                                          <input type="text" class="form-control">
                                      </div>
                                  </div>
                              </div>
                              <div role="tabpanel" class="tab-pane" id="fullspecification">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <h4>Key Facts</h4>
                                              </div>
                                              <div class="form-group">
                                                  <span>Area</span>
                                                  <input type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group">
                                                  <span>Sub Type</span>
                                                  <input type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group">
                                                  <span>Year Built</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                              <div class="form-group">
                                                  <span>Apx. SqFt.</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                              <div class="form-group">
                                                  <span>Property Class</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <h4>Schools and Neighborhood</h4>
                                              </div>
                                              <div class="form-group">
                                                  <span>Elementary School</span>
                                                  <input type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group">
                                                  <span>Middle School</span>
                                                  <input type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group">
                                                  <span>High School</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                              <div class="form-group">
                                                  <span>Subdivision</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <h4>Features</h4>
                                              </div>
                                              <div class="form-group">
                                                  <span>Bedrooms</span>
                                                  <input type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group">
                                                  <span>Bathrooms</span>
                                                  <input type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group">
                                                  <span>Garage</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                              <div class="form-group">
                                                  <span>Water/Sewer</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                              <div class="form-group">
                                                  <span>Cooling</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <h4>Property Location</h4>
                                              </div>
                                              <div class="form-group">
                                                  <span>Latitude</span>
                                                  <input type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group">
                                                  <span>Longtitude</span>
                                                  <input type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group">
                                                  <span>Directions</span>
                                                  <input type="text" class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-12">
                              <button class="btn btn-default btn-select pull-right">Submit</button>
                          </div>
                      </form>
                  </div>
                </div>

                <!-- photos -->
                <div role="tabpanel" class="tab-pane" id="photos">
                  <div class="photo-area">
                      <div class="page-title-container">
                          <div class="col-md-12">
                              <h3>Photos</h3>
                              <span class="page-desc">Upload pictures for your site</span>
                          </div>
                      </div>

                      <div class="photo-content">
                          <div class="col-md-4">
                              <div class="fileUploader" id="two"></div>
                          </div>
                          <div class="col-md-8">
                              <div class="photo-container">
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/bg01.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/bg02.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/house1.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/house2.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/bg02.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/long.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/bg02.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/bg02.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/bg02.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="photo-item">
                                          <div class="image-container">
                                              <img src="<?=base_url()?>assets/images/dashboard/bg02.jpg" alt="" class="img-responsive">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>

                <!-- Domains -->
                <div role="tabpanel" class="tab-pane" id="domains">
                  <div class="page-title-container">
                      <div class="col-md-12">
                          <h3>Domains</h3>
                          <p>Your Beautiful mobile friendly website is now available at <a href="#">http://1942dogwoodlane.com/</a></p>
                      </div>
                  </div>
                  <div class="col-md-12 text-center">
                    <div class="domain-container">
                      <div class="col-md-offset-3 col-md-6 mt-70">
                        <form method="GET" action="<?php echo base_url(); ?>custom_domain/domains" id="domain-form" class="domain-ajax">
                          <div class="form-group">
                            <input type="hidden" name="domainUrl" value="<?php echo site_url('/custom_domain'); ?>" id="domainUrl">
                            <input type="hidden" name="domain_checker" value="<?php echo site_url('/custom_domain/domains'); ?>?domain_checker=1" id="domain_checker">
                            <input type="hidden" name="user_agent_id" value="<?php $this->session->userdata("user_id"); ?>" id="user_agent_id" >
                            <input type="hidden" name="purchaseDomainUrl" value="<?php echo site_url('/custom_domain/purchase_domain'); ?>" id="purchaseDomainUrl" >
                            <input type="hidden" name="domain_check" value="1">
                            <?php if(isset($_GET['domain']) && ($_GET['domain'] == 0) ) { ?>
                                <input type="hidden" name="domain" value="<?php echo $_GET['domain']; ?>" id="getDomain">
                            <?php } ?>
                            <div class="col-sm-10 col-xs-9 padding-0">
                              <input type="text" class="form-control" id="domain_name" name="domain_name" value="<?=isset($_GET["domain"]) ? $_GET["domain"] : ""; ?>" placeholder="Domain Name" required>
                            </div>
                            <div class="col-sm-2 col-xs-3 paddingleft-0">
                              <select id="suffix" class="form-control" name="suffix">
                                  <option value="com" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="com") ? 'selected="selected"' : ""; ?> >.com</option>
                                  <option value="net" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="net") ? 'selected="selected"' : ""; ?> >.net</option>
                                  <option value="org" <?=(isset($_GET["tld"]) AND $_GET["tld"]=="org") ? 'selected="selected"' : ""; ?> >.org</option>
                              </select>
                            </div>
                            <div class="col-md-12 clearfix text-center ">
                              <button class="btn btn-default btn-select checkdomainbtn" id="check_domain_sbt" type="submit"><i class="fa fa-search"></i> Check Domain</button>
                            </div>
                          </div>
                        </form>
                        <div id="domainResult" class="domain-group">
                          <ul class="domain-list"></ul>
                        </div>
                      </div>
                      <div class="col-md-offset-3 col-md-6 mt-70">
                        <div id="domainResult" class="domain-group">
                          <ul class="domain-list"></ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> <!-- end of domain-->
              </div>
            </div>
        </div>
      </section>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>

<!-- SPW Leads Config Modal -->
<div id="lead_config_spw_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form action="single_property_listings/saveLeadCaptureSettings" method="POST" id="lead_capture_config_form_spw">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title text-center">Configure Capture Leads</h3>
        </div>
        <div class="modal-body">
          <h4> Show Registration Form: </h4><br>
          <div id="ret_msg"></div>
          <div class="form-group">
            <label><input type="checkbox" id="view_site_spw" name="view_site" <?=($leads_config->view_site_spw) ? "checked" : ""; ?>> to view whole single property website.</label>
            <span><a href="javascript:void(0)" data-toggle="tooltip" title="Pop up signup form when someone lands on your single property website."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
          </div>
          <div class="form-group">
            <label><input type="checkbox" id="minute_capture_spw" name="minute_capture" <?=($leads_config->minute_capture_spw) ? "checked" : ""; ?>> after <input type="text" name="minutes_pass" id="minutes_pass_spw" value="<?=($leads_config->minutes_pass_spw) ? $leads_config->minutes_pass_spw : '';?>" style="width: 50px; height: 25px; padding: 5px;"> minutes have passed</label>
            <span><a href="javascript:void(0)" data-toggle="tooltip" title="Set minutes before the signup form will pop up."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
          </div>
          <div class="form-group">
            <label><input type="checkbox" id="allow_skip_spw" name="allow_skip" <?=(isset($leads_config->allow_skip_spw) && $leads_config->allow_skip_spw==1) ? "checked" : ""; ?>> Allow visitors to skip the above registration options</label>
            <span><a href="javascript:void(0)" data-toggle="tooltip" title="Allow customer to skip the signup process."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
          </div>
        </div>
        <div class="modal-footer">
          <?php
            if($this->config->item('disallowUser')) { ?>
              <a href="<?=base_url().'crm/leads?modal_premium=true'?>" class="btn btn-primary">Save</a>
          <?php
            } else { ?>
              <button type="submit" class="btn btn-primary">Save</button>
          <?php 
            }
          ?>
        </div>
      </div>
    </form>
  </div>
</div>

<?php $this->load->view('session/footer'); ?>
