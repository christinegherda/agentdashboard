<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Single_property_listings extends MX_Controller {

	function __construct() {

		parent::__construct( );

		$this->load->model("Single_property_listings_model" ,"single_property");
		$this->load->model("idx_login/Idx_model");
		$this->load->model("dsl/dsl_model");

	}

	public function index() {

		$data['title'] = "Single Property Listings";

		$param["mlsID"] = $this->session->userdata("agent_id");

		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		$data["spw_properties"] = $this->single_property->get_your_properties_listings(FALSE, $param);
		$data['active_properties'] = $this->getallActiveProperties();
		$data['sold_properties'] = Modules::run('dsl/getAgentSoldPropertyListings');

		$total_spw = array();
		if(!empty($data['active_properties'])) {
              foreach($data['active_properties'] as $active_prop) {
                foreach($data["spw_properties"] as $spw) {
                  if($active_prop->Id == $spw->property_id) {
                  	$total_spw[] = $active_prop->Id;
                   }
               }
           }
        } 

        $data["property_count"] = (count($data['active_properties'] ) - count($total_spw));

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->single_property->fetch_domain_info();
		$data["spw_domain_info"] = $this->single_property->fetch_spw_domain__info();
		$data["is_reserved_domain"] = $this->single_property->is_reserved_domain();
		$data["has_purchased_spw"] = $this->has_purchased_spw();
		$data["isFreeSPW"] = $this->isFreeSPW();
		$data["is_domain_free"] = $this->single_property->is_domain_free();
		$data['leads_config'] = $this->single_property->getLeadsConfig();

		if( isset($_GET["is_to_pay"]) && $_GET["is_to_pay"] == "true")
		{
			$this->session->set_flashdata('to_pay_mess','Please select property you want to create single property website!');
		}

		$domain_info = $data["domain_info"];

		if(!empty($domain_info)) {
            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
            $base_url =  $base . (($domain_info[0]->status == "completed") ? $domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/');
        } else {
            $base_url = base_url();
        }

	    $data["base_url"] = $base_url;

		$data["js"] = array("domain/spw_domain.js", "spw/spw.js");

		$this->load->view('single-property-listings', $data);
	}


	//=====================ACTIVE LISTINGS: DIRECT TO SPARK======================//

	public function getallActiveProperties(){

		$limit = 24;
		$page = 1;
		$active_prop =  $this->get_active_from_spark($limit,$page);
		//printA($active_prop);exit;

		if(isset($active_prop->D->Results) && !empty($active_prop->D->Results)) {

			$total_pages = $active_prop->D->Pagination->TotalPages;

			for($i=1; $i <= $total_pages; $i++) {
						
				$activeList[$i] = $this->getActiveProp($i);
				$active[] = $activeList[$i];

				$results = array_merge(...$active);	
			}

			return $results;
		}

	}


	public function getActiveProp($page = NULL) {

		$limit = 24;
		$active_prop =  $this->get_active_from_spark($limit,$page);
		//printA($active_prop);exit;

		if(isset($active_prop->D->Results) && !empty($active_prop->D->Results)) {
		  	foreach ($active_prop->D->Results as $active){
		    	$activeList[] = $active;
		  	}

			return $activeList;
		}

	}

	public function get_active_from_spark($limit = NULL, $page = NULL) {

		$limit = $limit;
		$dsl = modules::load('dsl/dsl');
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array('access_token' => $token->access_token);
		
		$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";

		//DEV-1765: Using this filter for specific MLS bec. we can't filter LEASED status on this MLS
		//Pasadena-Foothills Board of Realtors MLS
		$system_info = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"system_info");
		if(!empty($system_info) && $system_info->MlsId == "20170119210918715503000000"){
			$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Active Under Contract')";
		}

		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$url = "https://sparkapi.com/v1/my/listings?_pagination=1&_page=".$page."&_expand=PrimaryPhoto&_limit=".$limit."&_filter=(".$filter_string.")";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $access_token["access_token"],
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return $result_info;

	}


	// public function getallActiveProperties(){

	// 	$dsl = modules::load('dsl/dsl');
	// 	$mls_status = Modules::run('dsl/get_mls_statuses',$this->session->userdata("agent_id"));
	// 	$endpoint = "properties/".$this->session->userdata("user_id")."/status/".$mls_status;

	// 	$active_listings = $dsl->get_endpoint($endpoint);
	// 	$activeListingData = json_decode($active_listings);
	// 	$active = array();

	// 	if(!empty($activeListingData->data)){

	// 		$total_pages = $activeListingData->total_pages;

	// 		for($i=1; $i <= $total_pages; $i++) {
						
	// 			$activeList[$i] = $this->getActiveProp($i);
	// 			$active[] = $activeList[$i];

	// 			$results = array_merge(...$active);	
	// 		}

	// 		return $results;
	// 	}
	// }

	// public function getActiveProp($page = NULL) {

	// 	$limit = 24;
	// 	$dsl = modules::load('dsl/dsl');
	// 	$mls_status = Modules::run('dsl/get_mls_statuses',$this->session->userdata("agent_id"));
	// 	$endpoint = "properties/".$this->session->userdata("user_id")."/status/".$mls_status."?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto";

	// 	$active_listings = $dsl->get_endpoint($endpoint);
	// 	$activeListingData = json_decode($active_listings);
	// 	$activeList = array();

	// 	if( !empty($activeListingData)){
	// 		if(property_exists($activeListingData, 'data') ) {
	// 			foreach ($activeListingData->data as $active){
	// 		 		$activeList[] = $active->standard_fields[0];
	// 		 	}
	// 		}

	// 		return $activeList;
	// 	}
	// }

	// public function create_single_listings() {

	// 	$listingid = $this->input->post('listingid');
	// 	$list_agentId = $this->input->post('list_agentId');
	// 	$listingKey = $this->input->post('ListingKey');
	// 	$property_name = url_title($this->input->post('unparsedaddr'), '-', TRUE);

 //        $property_uri = implode(
 //            '~',
 //            array(
 //                $property_name,
 //                $listingid,
 //                $this->session->userdata("user_id")
 //            )
 //        );

 //        $site_url = prep_url($this->single_property->get_domain() . "/property-site/$property_uri");
 //        $site_url_subdomain = $this->single_property->get_subdomain()."/property-site/$property_uri";

	// 	if( !empty($listingid) && !empty($listingKey) ) {
	// 		$data = array(
	// 			'listingid' 		=> $listingid,
	// 			'list_agentId' 		=> $list_agentId,
	// 			'property_id' 		=> $listingKey,
	// 			'unparsedaddr' 		=> $this->input->post('unparsedaddr'),
	// 			'primary_photo' 	=> $this->input->post('primary_photo'),
	// 			'address' 			=> $this->input->post('address'),
	// 			'property_type' 	=> $this->input->post('property_type'),
	// 			'price' 			=> $this->input->post('price'),
	// 			'broker' 			=> $this->input->post('broker'),
	// 			'broker_number'		=> $this->input->post('broker_number'),
	// 			'property_status' 	=> $this->input->post('property_status'),
	// 			'site_url' 			=> $site_url,
	// 			'site_url_old' 		=> $site_url_subdomain,
	// 			'username' 			=> $listingid,
	// 			'city' 				=> $this->input->post('city'),
	// 			'state' 			=> $this->input->post('state'),
	// 			'spw_version' 		=> 2,
	// 		);

	// 		$retStatus = $this->single_property->create_spw_site($data);

	// 		if($retStatus) {
	// 			// Replicate Files

	// 			if(in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))){
	// 			    header('location: '.base_url().'single_property_listings?status=localhost+found');
	// 			}
	// 			else {
	// 				$domain_status = $this->single_property->get_domain_status();
	// 				if(!empty($domain_status)){

	// 					if($domain_status == "pending"){
	// 						$site_url = $site_url_old;
	// 					}else{
	// 						$site_url = $site_url;
	// 					}
	// 				}

	// 				$this->send_notification_to_user_spw( $data, $site_url );

	// 				header('location: '.base_url().'single_property_listings?create_status=success');
	// 			}
	// 		}
	// 		else {
	// 			header('location: '.base_url().'single_property_listings?create_status=failed');
	// 			// Insert Failed;
	// 		}
	// 	}
	// 	else {

	// 		header('location: '.base_url().'single_property_listings/single_property_listings/index?modal_premium=true');
			
	// 		// echo "<p>Listing Key:</p>";
	// 		// echo "<pre>";
	// 		// var_dump($listingKey);
	// 		// echo "</pre>";
	// 		// echo "<p>Listing ID:</p>";
	// 		// echo "<pre>";
	// 		// var_dump($listingid);
	// 		// echo "</pre>";
	// 	}
	// }

	public function create_single_listings() {

		$listingid = $this->input->post('listingid');
		$list_agentId = $this->input->post('list_agentId');
		$listingKey = $this->input->post('ListingKey');
		$property_name = url_title($this->input->post('unparsedaddr'), '-', TRUE);

        $property_uri = implode(
            '~',
            array(
                $property_name,
                $listingid,
                $this->session->userdata("user_id")
            )
        );

        $site_url = prep_url($this->single_property->get_domain() . "/property-site/$property_uri");
        $site_url_subdomain = $this->single_property->get_subdomain()."/property-site/$property_uri";

		if( !empty($listingid) && !empty($listingKey) ) {
			$data = array(
				'listingid' 		=> $listingid,
				'list_agentId' 		=> $list_agentId,
				'property_id' 		=> $listingKey,
				'unparsedaddr' 		=> $this->input->post('unparsedaddr'),
				'primary_photo' 	=> $this->input->post('primary_photo'),
				'address' 			=> $this->input->post('address'),
				'property_type' 	=> $this->input->post('property_type'),
				'price' 			=> $this->input->post('price'),
				'broker' 			=> $this->input->post('broker'),
				'broker_number'		=> $this->input->post('broker_number'),
				'property_status' 	=> $this->input->post('property_status'),
				'site_url' 			=> $site_url,
				'site_url_old' 		=> $site_url_subdomain,
				'username' 			=> $listingid,
				'city' 				=> $this->input->post('city'),
				'state' 			=> $this->input->post('state'),
				'spw_version' 		=> 2,
			);

			$domain_status = $this->single_property->get_domain_status();
			if(!empty($domain_status)){

				if($domain_status == "pending"){
					$site_url = $site_url_old;
				}else{
					$site_url = $site_url;
				}
			}


			$retStatus = $this->single_property->create_spw_site($data);

			if($retStatus){

				$this->send_notification_to_user_spw( $data, $site_url );
				$status = TRUE;
			} else {
				$status = FALSE;
			}

			echo json_encode(array('status' => $status, 'listing_id' => $data["property_id"]));

		}
	}

	public function upgrade_spw_v2() {

		$listingid = $this->input->post('listingid');
		$list_agentId = $this->input->post('list_agentId');
		$listingKey = $this->input->post('ListingKey');
		$property_name = str_replace(' ', '-', $this->input->post('unparsedaddr'));
		$property_name = preg_replace('/[^A-Za-z0-9\-]/', '', $property_name);

		if(!empty($listingid) && !empty($listingKey)) {
            $property_name = url_title($this->input->post('unparsedaddr'), '-', TRUE);

            $property_uri = implode(
                '~',
                array(
                    $property_name,
                    $listingid,
                    $this->session->userdata("user_id")
                )
            );

            $site_url = prep_url($this->single_property->get_domain() . "/property-site/$property_uri");

			$data = array(
				'listingid' => $listingid,
				'list_agentId' => $list_agentId,
				'property_id' 		=> $listingKey,
				'unparsedaddr' => $this->input->post('unparsedaddr'),
				'primary_photo' => $this->input->post('primary_photo'),
				'address' => $this->input->post('address'),
				'property_type' => $this->input->post('property_type'),
				'price' => $this->input->post('price'),
				'broker' => $this->input->post('broker'),
				'broker_number' => $this->input->post('broker_number'),
				'property_status' => $this->input->post('property_status'),
				'site_url' => $site_url,
				'site_url_old' => $this->input->post('site_url_old'),
				'isupgrade' => 1,
				'spw_version' => 2,
			);

			$retStatus = $this->single_property->upgrade_spw_site($data);
			if($retStatus) {
				// Replicate Files
				if(in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))){
				    header('location: '.base_url().'single_property_listings');
				}
				else {
					$this->send_notification_to_user_spw( $data, $site_url );

					header('location: '.base_url().'single_property_listings?create_status=success');
				}
			}
			else {
				header('location: '.base_url().'single_property_listings?create_status=failed');
				// Insert Failed;
			}
		}
		else {
			echo "<p>Listing Key:</p>";
			echo "<pre>";
			var_dump($listingKey);
			echo "</pre>";
			echo "<p>Listing ID:</p>";
			echo "<pre>";
			var_dump($listingid);
			echo "</pre>";
		}
	}

	public function spark_member() {

		$data["title"]  =  "Spark Member";
		$this->load->view('spark_member', $data);
	}


	public function has_purchased_spw() {

		$arr = array(
			'UserId' => $this->session->userdata('agent_id'),
			'EventType' => 'purchased',
			'ApplicationType' => 'Single_Property_Website',
			'isFromSpark' => 1
		);

		$agent_subscription = $this->Idx_model->fetch_agent_subscription($arr);

		return ($agent_subscription) ? TRUE : FALSE;
	}

	public function isFreeSPW() {

		// $ret = FALSE;

		// if($this->has_purchased_spw()) {

		// 	$param["mlsID"] = $this->session->userdata("agent_id");
		// 	$properties = $this->single_property->get_your_properties_listings(FALSE, $param);

		// 	$ret = (count($properties) >= 1) ? TRUE : FALSE;
		// }

		// return $ret;

		$param["mlsID"] = $this->session->userdata("agent_id");
		$properties = $this->single_property->get_your_properties_listings(FALSE, $param);

		$ret = (count($properties) >= 1) ? TRUE : FALSE;

		return false;//$ret;
	}

	public function send_notification_to_user_spw( $data = array(), $property_url = "") {
	 	$this->load->library("email");

		$email = $this->session->userdata("email");
		$user_info = $this->single_property->get_branding_infos();
		$first_name = $user_info->first_name;

	 	$content = "Hi ".$first_name.", <br><br>

			Congratulations, your [".$data['address']."] single property website has been created. <br><br>

			URL: $property_url  <br><br>

			You can create more site using your dashboard below. <br><br>

			Dashboard URL: ". AGENT_DASHBOARD_URL ." <br>
			username: ".$email." <br>
			pasword: test1234
			<br><br>
			Kindly Regards,  <br>

			Team AgentSquared <br>
			<a href='www.AgentSquared.com'>www.AgentSquared.com</a>
	 	";

	 	$subject = $data['address']." has been created" ;

	 	return $this->email->send_email(
	 		'automail@agentsquared.com',
	 		'AgentSquared',
	 		$email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "troy@agentsquared.com,eugene@agentsquared.com,albert@agentsquared.com,sales@agentsquared.com,rolbru12@gmail.com,raul@agentsquared.com,joce@agentsquared.com,honradokarljohn@gmail.com,paul@agentsquared.com"
	 		)
	 	);
	}

	public function checkCaptureLeads() {
		$switch = $this->single_property->getCaptureLeads($this->session->userdata('agent_id'));
		exit(json_encode(array('spw_capture_leads' => $switch->spw_capture_leads)));
	}

	public function setLeadCapture() {
		$this->single_property->setLeadCaptureConfig(array('spw_capture_leads'=>$this->input->post('captureSpwLeads')));
	}

	/**
     * save lead capture configuration set by user
     *
     * @param  Array $_POST
     * @return JSON object
     */
	public function saveLeadCaptureSettings() {

		$minute_capture = ($this->input->post('minute_capture')==1 || $this->input->post('minute_capture')=='on') ? 1 : 0;
		$minutes_pass = !empty($this->input->post('minutes_pass')) ? $this->input->post('minutes_pass') : 0;

		if(($minute_capture == 1 && $minutes_pass <= 0) || ($minute_capture == 1 && !ctype_digit($minutes_pass))) {

			$return = array('success'=>FALSE, 'message'=>'Please define how many minutes before the signup form will pop up.');

		} else {

			$post_data = array(
				'view_site_spw' => ($this->input->post('view_site')==1 || $this->input->post('view_site')=='on') ? 1 : 0,
				'minutes_pass_spw' => $minutes_pass,
				'minute_capture_spw' => $minute_capture,
				'allow_skip_spw' => ($this->input->post('allow_skip')==1 || $this->input->post('allow_skip')=='on') ? 1 : 0,
			);

			try {

				$success = $this->single_property->saveLeadCaptureSettings($post_data);

				$return = array(
					'success' => $success ? TRUE : FALSE,
					'message' => $success ? 'Lead capture settings successfully updated!' : 'You made no changes at all!'
				);

			} catch (Exception $e) {
			
				$return = array('success'=>FALSE, 'message'=>$e->getMessage());
			}

		}

		exit(json_encode($return));

	}
}
