<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spw extends CI_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model('Single_property_listings_model', 'spw');

	}

	public function get_created_spw($agent_id=NULL) {
		header("Access-Control-Allow-Origin: *");
		$data=array('success'=>FALSE);

		if(!empty($agent_id)) {

			$this->load->library('idx_auth', $agent_id);

			$spw_data = $this->spw->get_created_spw($agent_id);

			if(!empty($spw_data)) {

				$listing = array();

				foreach($spw_data as $key) {

					$ret = $this->idx_auth->GetListing($key->property_id);

					if(!empty($ret)) {
						$data['success'] = TRUE;
						$data['property_id'][] = $key->property_id;
					}

				}
			}

		}

		exit(json_encode($data));
	}

}
