<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Idx_login_v2 extends MX_Controller {

	public $bearer_token ="c4xmoo64zm6sjdjrfa2qs14bv";

	public function __construct() {

		parent::__construct();

		$this->load->model('idx_login/idx_model', 'idx_model');
		$this->load->model('customize_homepage/customize_homepage_model', 'custom');
		$this->load->model('pages/page_model', 'page');	
		$this->load->model('crm/leads_model', 'leads');
	}


	public function index() {

		$data["title"] = "Bearer Token";
		$this->load->view('bearer_view', $data);
	}

	public function submit_bearer_token() {

		if($this->input->post('bearer_token')) {

			$this->bearer_token = $this->input->post('bearer_token');
			$this->session->set_userdata("bearer_token", $this->bearer_token);
			$account = $this->get_account();

			if($account) {
				$this->insert_user_subscription($account);
				$this->insert_user($account);
			} else {
				echo "Your bearer token is invalid!";
			}
		}
	}

	public function insert_user($account=array()) {

		if($account) {

			$code = generate_key(8);

			$user = array(
				'agent_id' 		=> $account["D"]["Results"][0]["Id"],
				'ip_address' 	=> $this->input->ip_address(),
				'email' 		=> (isset($account["D"]["Results"][0]["Emails"][0]["Address"]) && !empty($account["D"]["Results"][0]["Emails"][0]["Address"])) ? $account["D"]["Results"][0]["Emails"][0]["Address"] : "Not Specified",
				'first_name'	=> (isset($account["D"]["Results"][0]["FirstName"]) && !empty($account["D"]["Results"][0]["FirstName"])) ? $account["D"]["Results"][0]["FirstName"] : "Not Specified", 
				'last_name'		=> (isset($account["D"]["Results"][0]["LastName"]) && !empty($account["D"]["Results"][0]["LastName"])) ? $account["D"]["Results"][0]["LastName"] : "Not Specified", 
				'company' 		=> (isset($account["D"]["Results"][0]["Office"]) && !empty($account["D"]["Results"][0]["Office"])) ? $account["D"]["Results"][0]["Office"] : "Not Specified",
				'password' 		=> 'test1234',
				'phone' 		=> (isset($account["D"]["Results"][0]["Phones"][0]["Number"]) && !empty($account["D"]["Results"][0]["Phones"][0]["Number"])) ? $account["D"]["Results"][0]["Phones"][0]["Number"] : "Not Specified",
				'city' 			=> (isset($account["D"]["Results"][0]["Addresses"]["City"]) && !empty($account["D"]["Results"][0]["Addresses"]["City"])) ? $account["D"]["Results"][0]["Addresses"]["City"] : "Not Specified",
				'zip' 			=> (isset($account["D"]["Results"][0]["Addresses"]["PostalCode"]) && !empty($account["D"]["Results"][0]["Addresses"]["PostalCode"])) ? $account["D"]["Results"][0]["Addresses"]["PostalCode"] : "Not Specified",
				'address' 		=> (isset($account["D"]["Results"][0]["Addresses"]["Address"]) && !empty($account["D"]["Results"][0]["Addresses"]["Address"])) ? $account["D"]["Results"][0]["Addresses"]["Address"] : "Not Specified",
				'broker' 		=> (isset($account["D"]["Results"][0]["Office"]) && !empty($account["D"]["Results"][0]["Office"])) ? $account["D"]["Results"][0]["Office"] : "Not Specified",
				'theme' 		=> 'agent-haven',
				'active' 		=> 1,
				'created_on' 	=> date('Y-m-d H:i:s'),
				'code' 			=> generate_key(8),
				'sync_status'   => 0,
				'date_signed' 	=> date('Y-m-d H:i:s'),
				'pricing'		=> "",
				'trial'			=> 0,
				'auth_type'		=> 1,
			);
			
			//insert pages
			$this->page->insert_home_page($user);
			$this->page->insert_find_home_page($user);
			$this->page->insert_seller_page($user);
			$this->page->insert_buyer_page($user);
			$this->page->insert_contact_page($user);
			$this->page->insert_blog_page($user);
			$this->page->insert_testimonial_page($user);

			//insert home options
			$this->custom->insert_active_option($user);
			$this->custom->insert_new_option($user);
			$this->custom->insert_nearby_option($user);
			$this->custom->insert_office_option($user);
			
			$user_id = $this->idx_model->insert_spark_user($user);
			
			if($user_id) {

				//Save bearer token
				$data = array(
					'agent_id'		=> $account["D"]["Results"][0]["Id"],
					'bearer_token' 	=> $this->bearer_token,
					'date_created' 	=> date('Y-m-d H:i:s')
				);

				//Save bearer token to dsl
				$this->save_spark_tokens($user_id, $data['agent_id']);
				$this->idx_model->save_bearer_token($data);
				
				//Saving into Freemium table
				$freemium = array(
					'plan_id' => 1,
					'subscriber_user_id' => $user_id,
					'date_created'	=> date('Y-m-d H:i:s')
				);	

				$this->idx_model->insert_freemium($freemium);
				//Set isFreemium session
				$this->session->set_userdata("isFreemium", TRUE);
				

				//Set sessions to bypass login
				$arr = array(
					'user_id' 		=> $user_id,
					'agent_id' 		=> $user['id'],
					'email' 		=> $user['email'],
					'code' 			=> $code,
					'identity' 		=> $user['email'],
					'old_last_login' => $user['date_signed'],
					'logged_in_auth' => true,
					'agent_idx_website' => true
				);

				$this->session->set_userdata($arr);

				$user_info = array();
				$user_info[0]['email'] = $user['email'];
				$user_info[0]['first_name'] = $user['first_name'];

				Modules::run('idx_login/Idx_login/send_notification_to_user_idx', $user_info);

				$this->sync_view($user_id);
			}
		}
	}

	public function insert_user_subscription($account=array()) {

		if($account) {

			$data = array(
			   'ApplicationName' 	=> "Instant IDX Website powered by Flexmls",
			   'ApplicationUri' 	=> "https://www.agentsquared.com/pricing/",
			   'CallbackName' 		=> "Agent Purchase",
			   'EventType' 			=> "purchased",
			   'PricingPlan' 		=> "monthly",
			   'TestOnly' 			=> "false",
			   'Timestamp' 			=> date('Y-m-d H:i:s'),
			   'UserEmail' 			=> isset($account["D"]["Results"][0]["Emails"][0]["Address"]) ? $account["D"]["Results"][0]["Emails"][0]["Address"] : "Not Defined",
			   'UserId' 			=> $account["D"]["Results"][0]["Id"],
			   'UserName' 			=> $account["D"]["Results"][0]["Name"],
			   'ApplicationType' 	=> 'Agent_IDX_Website',
			   'isFromSpark' 		=> 0
			);
			
			$this->idx_model->insert_agent_subscription($data);
		}
	}

	public function sync_view($user_id) {
		$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');
		$data['title'] = "Sync MLS Data";
		$data['user_id'] = $user_id;

		$this->load->view('idx_login_v2/sync', $data);
	}

	public function get_account() {

		$uri = 'my/account';
		$account = $this->set_curl($uri);
		return $account;
	}

	public function get_agent_listings() {

		$uri = 'my/listings?_pagination=1';
		$listings = $this->set_curl($uri);
	}

	private function set_curl($uri=NULL) {

		if($uri) {

			$url = "https://sparkapi.com/v1/".$uri;

			$headers = array(
				//'Authorization:Bearer '.$this->bearer_token,
				'Authorization:OAuth '.$this->bearer_token,
				'Accept:application/json',
				'X-SparkApi-User-Agent:MyApplication',
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
			
			return $result_info;
		}
	}

	//{POST} /sync-new-agent (metas)
	public function sync_new_agent() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-new-agent";
		$body = array("access_token" => "", "bearer_token"=> $this->session->userdata('bearer_token') );

		$result = $dsl->post_endpoint($endpoint, $body);
		
		$check_syncing = "sync-status/".$this->session->userdata('user_id');

		do {//Syncing while account_status is still true. If false, it will exit and proceed to next syncing.

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

		} while($check['account_status'] == true);

		$response = (!empty($result)) ? array("success" => TRUE) : array("success" => FALSE);

		exit(json_encode($result));
	}

	//{POST} /listings/nearby (4 nearby)
	public function sync_nearby_listings() {

		$lat_lon = $this->get_lat_lon();

		if(!empty($lat_lon)) {

			$dsl = modules::load('dsl/dsl');

			$endpoint = "listings/nearby?_lat=".$lat_lon['latitude']."&_lon=".$lat_lon['longitude']."&_expand=PrimaryPhoto";

			$body = array("access_token" => "", "bearer_token"=> $this->session->userdata('bearer_token') );

			$result = $dsl->post_endpoint($endpoint, $body);

			$check_syncing = "sync-status/".$this->session->userdata('user_id');

			do {//Syncing while account_status is still true. If false, it will exit and proceed to next syncing

				$ch = $dsl->get_endpoint($check_syncing);
				$check = json_decode($ch, true);

			} while($check['nearby_status'] == true);

			$response = (!empty($result)) ? array("success" => TRUE) : array("success" => FALSE);
			exit(json_encode($response));

		}

		exit(json_encode(array("success" => TRUE)));
	}

	//{POST} /listings/new (reserve)
	public function sync_new_listings() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "listings/new";
		$body = array("access_token" => "", "bearer_token"=> $this->session->userdata('bearer_token') );

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->session->userdata('user_id');

		do {//Syncing while account_status is still true. If false, it will exit and proceed to next syncing.

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

		} while($check['account_status'] == true);

		$response = (!empty($result)) ? array("success" => TRUE) : array("success" => FALSE);

		exit(json_encode($response));
	}

	//{POST} /sync-new-agent-listing (property_listing_data)
	public function sync_agent_listings() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-new-agent-listings";
		$body = array("access_token" => "", "bearer_token"=> $this->session->userdata('bearer_token') );

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->session->userdata('user_id');

		do {//Syncing while account_status is still true. If false, it will exit and proceed to next syncing.

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

		} while($check['account_status'] == true);

		$response = (!empty($result)) ? array("success" => TRUE) : array("success" => FALSE);

		exit(json_encode($response));

	}

	//{POST} /sync-agent-contacts
	/*public function sync_agent_contacts() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-agent-contacts";
		$body = array("access_token" => "", "bearer_token"=>$this->session->userdata('bearer_token'));

		$res = $dsl->post_endpoint($endpoint, $body);
		$result = json_decode($res, true);

		$response = ($result["status"] == "success") ? $response = array("success" => TRUE) : $response = array("success" => FALSE);

		exit(json_encode($response));

	}*/

	public function sync_agent_contacts() {
		
		$flag = FALSE;

		$obj = modules::load('crm/leads');

		$access_token = array('bearer_token'=>$this->session->userdata('bearer_token'));

		$contact = $obj->get_contacts_by_bearer(TRUE, $access_token);

		if($contact) {
			for($i=1; $i <= $contact['TotalPages']; $i++) { 
				$contactsave = $obj->get_contacts_by_bearer(FALSE, $access_token, $i);

				if($contactsave) {
					foreach($contactsave as $contacts) {
						if(!$this->leads->is_contacts_exists( $contacts)) {
							$this->leads->save_contacts( $contacts );
						}
					}

					$flag = TRUE;
				}
			}
		}

		$response = ($flag) ? array("success" => TRUE) : array("success" => FALSE);

		exit(json_encode($response));
	}

	//{POST} /sync-saved-searches
	public function sync_saved_searches() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-saved-searches";
		$body = array("bearer_token"=>$this->session->userdata('bearer_token'));

		$res = $dsl->post_endpoint($endpoint, $body);
		$result = json_decode($res, true);

		$response = ($result["status"] == "success") ? array("success" => TRUE) : array("success" => FALSE);

		exit(json_encode($response));

	}

	public function update_sync_status() {

		$id = $this->input->post('id');
		$sync_status = $this->input->post('sync_status');

		$data = array('sync_status' => $sync_status);
		$success = $this->idx_model->update_sync_status($id, $data);

		$response = ($success) ? array("success" => TRUE) : $response = array("success" => FALSE);
		// kill me session

		$this->session->unset_userdata('bearer_token');
		
		exit(json_encode($response));
	}

	public function get_listing() {

		$obj = modules::load("dsl");

		$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Active Contingent')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$body = array("access_token" => '',"bearer_token"=>$this->bearer_token);

		$endpoint = "spark-listings?_limit=24&_pagination=1&_page=1&_filter=(".$filter_string.")&_expand=PrimaryPhoto";

		$results = $obj->post_endpoint($endpoint, $body);

		$return = json_decode($results, true);
		return $return;
	}

	public function get_lat_lon() {

		//http://138.68.10.202:3000/api/v1/properties/557
		//http://138.68.10.202:3000/api/v1/properties/557/status/Active,Contingent,Pending
		$lat_lon=array();

		$dsl = modules::load("dsl");

		$active_listing = "properties/".$this->session->userdata('user_id')."/status/Active,Contingent,Pending";

		$al = $dsl->get_endpoint($active_listing);
		$res = json_decode($al, true);

		if(!empty($res['data'])) {

			$lat_lon['latitude'] = $res['data'][0]['standard_fields'][0]['StandardFields']['Latitude'];
			$lat_lon['longitude'] = $res['data'][0]['standard_fields'][0]['StandardFields']['Longitude'];

		} else {

			$sold_listing = "properties/".$this->session->userdata('user_id')."/status/Closed,Sold";
			$sl = $dsl->get_endpoint($sold_listing);
			$res = json_decode($sl, true);

			if(!empty($res['data'])) {
				$lat_lon['latitude'] = $res['data'][0]['standard_fields'][0]['StandardFields']['Latitude'];
				$lat_lon['longitude'] = $res['data'][0]['standard_fields'][0]['StandardFields']['Longitude'];

			} else {

				$new_listing = "new-listings/".$this->session->userdata('user_id')."?limit=1";
			    $nl = $dsl->get_endpoint($new_listing);
			    $res = json_decode($nl, true);

			    if(!empty($res['data'])) {
			     	$lat_lon['latitude'] = $res['data'][0]['StandardFields']['Latitude'];
			     	$lat_lon['longitude'] = $res['data'][0]['StandardFields']['Longitude'];
			    }
			}
		}
		
		return $lat_lon;
	}

	public function save_spark_tokens($user_id, $agent_id) {
		//Insert Curl Here
    	$url = "http://138.68.10.202:3000/api/v1/agent_idx_token";

    	$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: ERI9I4ku3vDyLKlOit7m_A7hbXiuOakCLgIuszmbGnw',
    		'X-User-Id: wfnbdL5z4Z7e6Ef6i'
    	);

    	$data = array(
    		'system_user_id'		=> (int)$user_id,
			'agent_id' 				=> $agent_id,
			'bearer_token' 			=> $this->bearer_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
		);

		$data_json = json_encode($data, true);

    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
	}

}