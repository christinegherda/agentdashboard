<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debugger_model extends CI_Model{

    function __construct() {
        parent:: __construct();
    }

    public function get_account_info($user_id= NULL, $type=NULL){

        if($user_id && $type) {

            $this->db->select("date_modified")
                    ->from("account_meta")
                    ->where("type", $type)
                    ->where("user_id",$user_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }
    }
    public function get_account_address($user_id= NULL, $type=NULL){

        if($user_id && $type) {

            $this->db->select("account")
                    ->from("account_meta")
                    ->where("type", $type)
                    ->where("user_id",$user_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }
    }

    public function get_property_new($user_id=NULL, $type=NULL) {

        if($user_id && $type) {

            $this->db->select("listing,date_created")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    //remove this where clause after deleting listings(BLOB type) column in homepage properties
                    ->where("listing_id !=", "");

            $query = $this->db->get()->result();

            return ($query) ? $query : FALSE;
        }
    }
    
    public function get_property_old($user_id=NULL, $type=NULL) {

        if($user_id && $type) {

            $this->db->select("listings,date_created")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }
    }

    public function check_listing($user_id=NULL, $type=NULL) {

        if($user_id && $type) {

            $this->db->select("listing")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    ->where("listing_id !=", "")
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }
    }


     public function make_site_premium() {

         $info["plan_id"] = 2;

        $this->db->where("subscriber_user_id", $this->session->userdata("user_id"));
        $this->db->update("freemium", $info);

        $this->db->select("plan_id,subscriber_user_id")
                ->from("freemium")
                ->where("subscriber_user_id", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get()->row();

        return ($query) ? $query : FALSE;
    }

    public function make_site_freemium() {

         $info["plan_id"] = 1;

        $this->db->where("subscriber_user_id", $this->session->userdata("user_id"));
        $this->db->update("freemium", $info);

         $this->db->select("plan_id,subscriber_user_id")
                ->from("freemium")
                ->where("subscriber_user_id", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get()->row();

        return ($query) ? $query : FALSE;
    }
    public function get_date_listings_updated_3h() {

        $array =

        $this->db->select("hp.user_id,u.agent_id, d.domains,hp.date_created,max(hp.id) as 'hp.id'")
                ->from("homepage_properties hp")
                ->join("domains d", "d.agent=hp.user_id", "inner")
                ->join("users u", "u.id=hp.user_id", "inner")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id", "inner")
                ->join("agent_subscription ags", "ags.UserId=u.agent_id", "inner")
                ->where("uit.is_valid","1")
                ->where("ags.EventType","purchased")
                ->where("ags.ApplicationType","Agent_IDX_Website")
                ->where("hp.date_created < (NOW() - INTERVAL 3 HOUR)")
                ->group_by("hp.user_id");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }
    public function get_date_listings_updated_24h() {

        $array =

        $this->db->select("hp.user_id,u.agent_id, d.domains,hp.date_created,max(hp.id) as 'hp.id'")
                ->from("homepage_properties hp")
                ->join("domains d", "d.agent=hp.user_id", "inner")
                ->join("users u", "u.id=hp.user_id", "inner")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id", "inner")
                ->join("agent_subscription ags", "ags.UserId=u.agent_id", "inner")
                ->where("uit.is_valid","1")
                ->where("ags.EventType","purchased")
                ->where("ags.ApplicationType","Agent_IDX_Website")
                ->where("hp.date_created < (NOW() - INTERVAL 24 HOUR)")
                ->group_by("hp.user_id");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function get_date_listings_updated_48h() {

        $array =

        $this->db->select("hp.user_id,u.agent_id, d.domains,hp.date_created,max(hp.id) as 'hp.id'")
                ->from("homepage_properties hp")
                ->join("domains d", "d.agent=hp.user_id", "inner")
                ->join("users u", "u.id=hp.user_id", "inner")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id", "inner")
                ->join("agent_subscription ags", "ags.UserId=u.agent_id", "inner")
                ->where("uit.is_valid","1")
                ->where("ags.EventType","purchased")
                ->where("ags.ApplicationType","Agent_IDX_Website")
                ->where("hp.date_created < (NOW() - INTERVAL 48 HOUR)")
                ->group_by("hp.user_id");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function get_date_listings_updated_72h() {

        $array =

        $this->db->select("hp.user_id,u.agent_id, d.domains,hp.date_created,max(hp.id) as 'hp.id'")
                ->from("homepage_properties hp")
                ->join("domains d", "d.agent=hp.user_id", "inner")
                ->join("users u", "u.id=hp.user_id", "inner")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id", "inner")
                ->join("agent_subscription ags", "ags.UserId=u.agent_id", "inner")
                ->where("uit.is_valid","1")
                ->where("ags.EventType","purchased")
                ->where("ags.ApplicationType","Agent_IDX_Website")
                ->where("hp.date_created < (NOW() - INTERVAL 72 HOUR)")
                ->group_by("hp.user_id");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function get_onboarded_agent_12h() {

        $this->db->select("u.id,u.agent_id,uit.access_token,u.date_signed")
                ->from("users u")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id", "inner")
                ->where("uit.is_valid","1")
                ->where("u.date_signed > (NOW() - INTERVAL 12 HOUR)")
                ->order_by("u.id","desc");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function get_onboarded_agent_24h() {

        $this->db->select("u.id,u.agent_id,uit.access_token,u.date_signed")
                ->from("users u")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id", "inner")
                ->where("uit.is_valid","1")
                ->where("u.date_signed > (NOW() - INTERVAL 24 HOUR)");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

     public function get_onboarded_agent_18h() {

        $this->db->select("u.id,u.agent_id,uit.access_token,u.date_signed")
                ->from("users u")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id", "inner")
                ->where("uit.is_valid","1")
                ->where("u.date_signed > (NOW() - INTERVAL 18 HOUR)");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function get_all_agents_no_latlon() {

        $this->db->select("u.id,u.agent_id,uit.is_valid")
                ->from("users u")
                ->join("agent_subscription as","as.UserId = u.agent_id","left")
                ->join("users_idx_token uit","uit.agent_id = u.agent_id","left")
                ->where("as.EventType", "purchased")
                ->where("as.ApplicationType", "Agent_IDX_Website")
                ->where("u.latitude =", "")
                ->where("uit.is_valid", "1")
                ->group_by("u.id");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;
    }


    public function get_agents_no_latlon($limit="") {

        $limitQuery = $limit ? $limit : 10;

        $this->db->select("u.id,u.agent_id,uit.is_valid")
                ->from("users u")
                ->join("agent_subscription as","as.UserId = u.agent_id","left")
                ->join("users_idx_token uit","uit.agent_id = u.agent_id","left")
                ->where("as.EventType", "purchased")
                ->where("as.ApplicationType", "Agent_IDX_Website")
                ->where("u.latitude =", "")
                ->where("uit.is_valid", "1")
                ->group_by("u.id")
                ->limit($limitQuery);

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;
    }

    public function check_liondesk_account() {

        $this->db->select("*")
                ->from("liondesk")
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->limit(1);

        $query = $this->db->get()->row();

        return ($query) ? $query : FALSE;
    }

    public function delete_liondesk_account() {

      $this->db->where("agent_id", $this->session->userdata("agent_id"));
      $this->db->delete( "liondesk" );

      return ($this->db->affected_rows()>0) ? TRUE : FALSE;

    }

    public function reset_password() {

        $info["password"] = "$2y$10$2VAtH6N0ewVv7dlEJjEG9eVbf13MZ4zS5Alp1sx0Uc4bXGcwRMRWG";//test1234

        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $info);

      return ($this->db->affected_rows()>0) ? TRUE : FALSE;

    }

    public function check_password() {

        $this->db->select("password")
                ->from("users")
                ->where("id", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get()->row();

        $default_password = "$2y$10$2VAtH6N0ewVv7dlEJjEG9eVbf13MZ4zS5Alp1sx0Uc4bXGcwRMRWG";//test1234

        if(!empty($query)){

            return ($query->password === $default_password) ? TRUE :  FALSE;
        }

    }

}
