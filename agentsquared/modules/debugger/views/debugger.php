<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<!-- Full Width Column -->
<div class="loader"></div>
<div class="content-wrapper">

  <div class="page-title">
    <h3 class="heading-title">
      <span>Debugger</span>
    </h3>
  </div>

      <!-- Main content -->
      <section class="content">
        <?php
          $session = $this->session->flashdata('session');
          if(isset($session)) { ?>
            <div class="alert <?=($session['status']) ? 'alert-success' : 'alert-danger'?> alert-flash" role='alert'><?= $session["message"]; ?></div>
        <?php } ?>
        <div class="display-alert">
          <?php echo $this->session->flashdata('msg'); ?>
        </div>
        <?php $this->load->view('session/launch-view'); ?>

        <table class="table table-dark table-striped">
          <thead>
            <tr>

              <div class="alert alert-warning">
                <strong>Note:</strong> Run <strong>Update Token</strong> first before you proceed to other sync endpoint!
              </div>
              <th>Service</th>
              <th>Endpoint</th>
               <th>Total Count/Modified</th>
               <th>Data Stored</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Update Token DSL </td>
              <td>update-token-copies</td>
                <td><span>This will update all token copies in DSL</span></td>
               <td>DSL</td>
              <td><a href="debugger/update_token_dsl"><button type="button" class="btn btn-info">Update Token</button></a></td>
            </tr>
            <tr>
              <td>Active Listings </td>
              <td>GetMyListings(Active)</td>
              <td>DB Count: <span class="badge badge-primary"><?php echo $active_listing_count_db?></span> Last Modified DB: <span class="badge badge-primary"><?php echo $active_listing_created?></span>  DSL Count: <span class="badge badge-primary"><?php echo $active_listing_count_dsl?></span> </td>
              <td>MYSQL/DSL</td>
              <td><a href="debugger/sync_active_listings"><button type="button" class="btn btn-info">Sync Active Listings</button></a></td>
            </tr>
             <tr>
              <td>Sold Listings </td>
              <td>GetMyListings(Sold)</td>
              <td>Current Sold: <span class="badge badge-primary"><?php echo $sold_listing_new?></span> Current Sold Modification: <span class="badge badge-primary"><?php echo $current_modified_sold_listing_data?></span>  Previous Sold: <span class="badge badge-primary"><?php echo $sold_listing_old?></span> Previous Sold Modification: <span class="badge badge-primary"><?php echo $previous_created_sold_listing_data?></span> </td>
              <td>MYSQL/DSL</td>
              <td><a href="debugger/sync_sold_listings"><button type="button" class="btn btn-info">Sync Sold Listings</button></a></td>
            </tr>
            <tr>
              <td>Office Listings </td>
              <td>GetOfficeListings</td>
               <td>Database: <span class="badge badge-primary"><?php echo $office_listing_count?></span>  Last Modified: <span class="badge badge-primary"><?php echo $office_listing_created?></span></td>
              <td>MYSQL</td>
              <td><a href="debugger/sync_office_listings"><button type="button" class="btn btn-info">Sync Office Listings</button></a></td>
            </tr>
            <tr>
              <td>Nearby Listings </td>
              <td>GetListings(Nearby)</td>
               <td>Database: <span class="badge badge-primary"><?php echo $nearby_listing_count?></span>  Last Modified: <span class="badge badge-primary"><?php echo $nearby_listing_created?></span></td>
               <td>MYSQL</td>
              <td><a href="debugger/sync_nearby_listings"><button type="button" class="btn btn-info">Sync Nearby Listings</button></a></td>
            </tr>
            <tr>
              <td>New Listings </td>
              <td>GetListings(New)</td>
               <td>Database: <span class="badge badge-primary"><?php echo $new_listing_count?></span>  Last Modified: <span class="badge badge-primary"><?php echo $new_listing_created?></span></td>
               <td>MYSQL</td>
              <td><a href="debugger/sync_new_listings"><button type="button" class="btn btn-info">Sync New Listings</button></a></td>
            </tr>
            <tr>
              <td>Contacts</td>
              <td>GetContacts</td>
              <td>Contacts Database : <span class="badge badge-primary"><?php echo $contacts_count?></span></td>
              <td>MYSQL</td>
              <td><a href="debugger/sync_contacts"><button type="button" class="btn btn-info">Sync Contacts</button></a></td>
            </tr>
            <tr>
              <td>Saved Search </td>
              <td>sync-saved-searches</td>
              <td>Saved Search DSL: <span class="badge badge-primary"><?php echo $saved_search_count?></span></td>
              <td>DSL</td>
              <td><a href="debugger/sync_saved_searches"><button type="button" class="btn btn-info">Sync Saved Searches</button></a></td>
            </tr>
            <tr>
              <td>Account Info </td>
              <td>account-info</td>
               <td>Last Modified: <span class="badge badge-primary"><?php echo $account_info_modified?></span></td>
              <td>MYSQL</td>
              <td><a href="debugger/sync_account_info"><button type="button" class="btn btn-info">Sync Account Info</button></a></td>
            </tr>
             <tr>
              <td>System Info </td>
              <td>system-info</td>
                <td>Last Modified: <span class="badge badge-primary"><?php echo $system_info_modified?></span></td>
               <td>MYSQL</td>
              <td><a href="debugger/sync_system_info"><button type="button" class="btn btn-info">Sync System Info</button></a></td>
            </tr>
            <tr>
              <td>All Endpoint </td>
              <td>agent-listing-updater</td>
                <td>Last Modified: <span class="badge badge-primary"><?php echo $account_info_modified?></span></td>
               <td>MYSQL/DSL</td>
              <td><a href="debugger/sync_master"><button type="button" class="btn btn-info">Sync ALL ENDPOINT</button></a></td>
            </tr>
            </tr>
             <tr>
              <td>Scrape Active/Spw Url </td>
              <td>facebook url scraper</td>
                <td>This will scrape active and spw url for facebook sharing.</td>
               <td>Facebook API</td>
              <td><a href="debugger/facebook_scraper"><button type="button" class="btn btn-info">Scrape ACTIVE/SPW Url</button></a></td>
            </tr>
            <?php if($liondesk_account){?>
              <tr>
              <td>Delete LionDesk Account </td>
              <td>delete liondesk account</td>
                <td>This will delete liondesk account in the database.</td>
               <td>MYSQL</td>
              <td><a href="debugger/delete_liondesk_account"><button type="button" class="btn btn-info">Delete Account</button></a></td>
            </tr>
           <?php }?>

          <?php if(!$default_password){?>
            <tr>
              <td>Reset Password</td>
              <td>password reset</td>
                <td>This will reset to default password: <span class="badge badge-primary">test1234</span></td>
               <td>MYSQL</td>
              <td><a href="debugger/reset_password"><button type="button" class="btn btn-info">Reset Password</button></a></td>
            </tr>
          <?php }?>
           
          </tbody>
        </table>
        
    </section>
</div>
<?php $this->load->view('session/footer'); ?>
