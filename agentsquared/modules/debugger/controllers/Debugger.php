<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debugger extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Debugger_model");
		$this->load->model('idx_login/idx_model', 'idx_model');
		$this->load->model("agent_sites/Agent_sites_model");
		$this->load->model("dsl/dsl_model");
		$this->load->model('crm/leads_model', 'leads');
		$this->load->model('mysql_properties/properties_model', 'pm');
		$this->load->model('single_property_listings/Single_property_listings_model', 'single_property');

	}

	public function index() {

		$data['title'] = "Debugger";

	 	$data["branding"] = $this->Agent_sites_model->get_branding_infos();
	 	$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

		//saved search info
		$dsl = modules::load('dsl/dsl');
		$endpoint = "savedsearches/".$this->session->userdata("user_id")."";
		$saved_search = $dsl->post_endpoint($endpoint);
		$savedSearchData = json_decode($saved_search,true);
		$data["saved_search_count"] = isset($savedSearchData["count"]) ? $savedSearchData["count"] : 0;

		//contacts
		$contacts = $this->leads->get_contacts(TRUE);
		$data["contacts_count"] = isset($contacts) ? $contacts : 0;

		//Account info  
		$account_info = $this->get_account($this->session->userdata("user_id"), "account_info");
		$data['account_info_modified'] = isset($account_info->date_modified) ? $account_info->date_modified : "" ;

		//System Info
		$system_info = $this->get_account($this->session->userdata("user_id"), "system_info");
		$data['system_info_modified'] = isset($system_info->date_modified) ? $system_info->date_modified : "" ;

		//Active listings
		$active_listing_data = $this->get_properties($this->session->userdata("user_id"), "active");
		$data["active_listing_count_db"] = isset($active_listing_data["listings"]) ? count($active_listing_data["listings"]) : 0 ;
		$data["active_listing_created"] = isset($active_listing_data["date_created"]) ? $active_listing_data["date_created"] : "null";
	  	$mls_status = Modules::run('dsl/get_mls_statuses',$this->session->userdata("agent_id"));
	  	$endpoint = "properties/".$this->session->userdata("user_id")."/status/".$mls_status."?_limit=10";
	  	$active_listings = Modules::run('dsl/get_endpoint', $endpoint);
	  	$activeListingData = json_decode($active_listings);
	  	$data["active_listing_count_dsl"] = isset($activeListingData->total_count)? $activeListingData->total_count : 0;

		//Sold listings
		$current_sold_listing_data = $this->pm->sold_listings_data_db_count($this->session->userdata("user_id"),"new");
		$previous_sold_listing_data = $this->pm->sold_listings_data_db_count($this->session->userdata("user_id"),"old");
		$current_modified_sold_listing_data = $this->pm->checkLastModified($this->session->userdata("user_id"),"new");
		$previous_created_sold_listing_data = $this->pm->checkLastModified($this->session->userdata("user_id"),"old");

		$data["sold_listing_old"] = !empty($previous_sold_listing_data) ? $previous_sold_listing_data : 0 ;
		$data["sold_listing_new"] = !empty($current_sold_listing_data) ? $current_sold_listing_data : 0 ;
		$data["current_modified_sold_listing_data"] = !empty($current_modified_sold_listing_data) ? $current_modified_sold_listing_data : "null";
		$data["previous_created_sold_listing_data"] = !empty($previous_created_sold_listing_data) ? $previous_created_sold_listing_data : "null";

		//Office listings
		$office_listing_data = $this->get_properties($this->session->userdata("user_id"), "office");
		$data["office_listing_count"] = isset($office_listing_data["listings"]) ? count($office_listing_data["listings"]) : 0 ;
		$data["office_listing_created"] = isset($office_listing_data["date_created"]) ? $office_listing_data["date_created"] : "null";

		//New listings
		$new_listing_data = $this->get_properties($this->session->userdata("user_id"), "new");
		$data["new_listing_count"] = isset($new_listing_data["listings"]) ? count($new_listing_data["listings"]) : 0 ;
		$data["new_listing_created"] = isset($new_listing_data["date_created"]) ? $new_listing_data["date_created"] : "null" ;

		//Nearby listings
		$nearby_listing_data = $this->get_properties($this->session->userdata("user_id"), "nearby");
		$data["nearby_listing_count"] = isset($nearby_listing_data["listings"]) ? count($nearby_listing_data["listings"]) : 0 ;
		$data["nearby_listing_created"] = isset($nearby_listing_data["date_created"]) ? $nearby_listing_data["date_created"] : "null" ;

		$data["liondesk_account"] = $this->Debugger_model->check_liondesk_account();
		$data["default_password"] =  $this->Debugger_model->check_password();

		//printA($soldListingData);exit;

		$this->load->view('debugger', $data);
	}

	public function sync_active_listings() {

		$active_listings = Modules::run('mysql_properties/get_my_listings', $this->session->userdata("agent_id"));

		if(!empty($active_listings)) {
			$this->save_listings($this->session->userdata("user_id"), $active_listings, "active");
		} else{
			$this->delete_listings($this->session->userdata("user_id"), "active");
		}

		$dsl = modules::load('dsl/dsl');
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ""
		);
		$dsl->post_endpoint('sync-new-agent-listings', $access_token);

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated Active listings!</h4>');
		redirect("debugger","refresh");

	}

	public function sync_sold_listings() {

		Modules::run('mysql_properties/syncAgentSoldListings', $this->session->userdata("user_id"), $this->session->userdata("agent_id"),FALSE);

		// $sold_listings = Modules::run('mysql_properties/get_sold_listings', $this->session->userdata("agent_id"));

		// if(!empty($sold_listings)) {
		// 	$this->save_listings($this->session->userdata("user_id"), $sold_listings, "sold");
		// } else{
		// 	$this->delete_listings($this->session->userdata("user_id"), "sold");
		// }

		// $dsl = modules::load('dsl/dsl');
		// $token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		// $access_token = array(
		// 		'access_token' => $token->access_token,
		// 		'bearer_token' => ""
		// );
		// $dsl->post_endpoint('sync-new-agent-listings', $access_token);

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated Sold Listings!</h4>');
		redirect("debugger","refresh");
	}

	public function sync_office_listings() {

		$office_listings = Modules::run('mysql_properties/get_office_listings', $this->session->userdata("agent_id"));

		if(!empty($office_listings)) {
			$this->save_listings($this->session->userdata("user_id"), $office_listings, "office");
		} else{
			$this->delete_listings($this->session->userdata("user_id"), "office");
		}

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated Office Listings!</h4>');
		redirect("debugger","refresh");
	}

	public function sync_nearby_listings() {

		$nearby_listings = Modules::run('mysql_properties/get_nearby_listings', $this->session->userdata("user_id"),$this->session->userdata("agent_id"));

		if(!empty($nearby_listings)) {
			$this->save_listings($this->session->userdata("user_id"), $nearby_listings, "nearby");
		} else{
			$this->delete_listings($this->session->userdata("user_id"), "nearby");
		}

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated Nearby Listings!</h4>');
		redirect("debugger","refresh");
	}

	public function sync_new_listings() {

		$new_listings = Modules::run('mysql_properties/get_new_listings', $this->session->userdata("agent_id"));

		if(!empty($new_listings)) {
			$this->save_listings($this->session->userdata("user_id"), $new_listings, "new");
		} else{
			$this->delete_listings($this->session->userdata("user_id"), "new");
		}

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated New Listings!</h4>');
		redirect("debugger","refresh");
	}

	public function save_listings($user_id, $listings, $type) {

		$listings_data = $listings;
		for ($x = 0; $x <= count($listings_data); $x++) {
			$count = 0;
		   	foreach($listings_data as $listing) {
		   		$listing_id = ($type == "active" || $type == "sold") ? $listing->Id : $listing->StandardFields->ListingKey;
			   	$countType = ($type == "sold") ? "8" : "4";

			   	if($count < $countType) {

					$data = array(
						'user_id'		=> $user_id,
						'listing_id' 	=> $listing_id,
						'listing' 		=> json_encode($listing),
						'type' 			=> $type,
						'data_type' 	=> '1',
						'date_created' 	=> date('Y-m-d H:i:s')
					);

					$this->pm->insert_homepage_listings($data);
				} $count++;
			}
		} 
	}

	public function delete_listings($user_id,$type) {

		$data = array(
			'user_id'	=> $user_id,
			'type' 		=> $type,
		);

		$this->pm->delete_listings($data);
	}

	public function sync_account_info() {

		$this->load->library("idx_auth", $this->session->userdata("agent_id"));
		$account_info = $this->idx_auth->GetMyAccount();

		if(!empty($account_info[0])) {
			$this->compress_metas($this->session->userdata("user_id"), $account_info[0], "account_info");
		}

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated Account Info!</h4>');
		redirect("debugger","refresh");

	}

	public function sync_system_info() {

		$this->load->library("idx_auth", $this->session->userdata("agent_id"));
		$system_info = $this->idx_auth->GetSystemInfo();

		if(!empty($system_info[0])) {
			$this->compress_metas($this->session->userdata("user_id"), $system_info[0], "system_info");
		}

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated System Info!</h4>');
		redirect("debugger","refresh");

	}

	public function get_account($user_id =NULL, $type=NULL){

		if($type && $user_id) {
	   		$account_json = $this->Debugger_model->get_account_info($user_id, $type);
	   		if(!empty($account_json)) {
	   	
	   			return $account_json;
	   		}
	  	}
	}

	public function get_properties($user_id=NULL, $type=NULL) {

		if($type) {

			$listing_checker = $this->Debugger_model->check_listing($user_id, $type);

			if(!empty($listing_checker)){

				//get data from listing(TEXT) column
				$listing_json = $this->Debugger_model->get_property_new($user_id, $type);

				$listings_data = array();

				if(!empty($listing_json)){
					foreach($listing_json as $listing){
						$listings_data[] = json_decode($listing->listing);
						$date_created = $listing->date_created;
					}

					$data = array(
						"listings" => $listings_data,
						"date_created" => $date_created
					);

					return $data;	
				}

			} else {

				//get data from listings(BLOB) column
				$listings_json = $this->Debugger_model->get_property_old($user_id, $type);

				if(!empty($listings_json)) {
					$listings_data = gzdecode($listings_json->listings);
					$listings_data = json_decode($listings_data);

					$data = array(
						"listings" => $listings_data,
						"date_created" => $date_created
					);

					return $data;
				}
			}	
		}
	}

	public function compress_metas($user_id, $meta, $type) {

		$json = json_encode($meta);
		$json_meta = gzencode($json, 9);
		$data = array(
			'user_id' => $user_id,
			'account' => $json_meta,
			'type' => $type,
			'date_created' => date('Y-m-d H:i:s')
		);

		$this->pm->insert_account($data);
	}


	public function sync_saved_searches() {

		$dsl = modules::load('dsl/dsl');
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
				'access_token' => $token->access_token
		);
		$dsl->post_endpoint('sync-saved-searches', $access_token);
		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated Saved Search!</h4>');
		redirect("debugger","refresh");
	}

	public function sync_contacts() {
		
		$this->load->library("idx_auth", $this->session->userdata("agent_id"));
		$contact = $this->idx_auth->GetContacts(1);
		
		if(isset($contact['pagination']['TotalPages']) && !empty($contact['pagination']['TotalPages'])) {

			for($i=1; $i<=$contact['pagination']['TotalPages']; $i++) {
			//for($i=1; $i<=4; $i++) {

				$contactsave = $this->idx_auth->GetContacts($i);

				if(!empty($contactsave['results'])) {

					foreach($contactsave['results'] as $contacts) {
						if(!$this->leads->is_contacts_exists($contacts)) {
							$this->leads->save_contacts($contacts);
						}
					}
				}
			}
		}

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated Contacts!</h4>');
		redirect("debugger","refresh");
	}

	public function update_token_dsl() {

		$call_url= getenv('AGENT_SYNCER_URL').'update-token-copies';
		$headers = array('Content-Type: application/json');
		$body=array();
		$body_json = json_encode($body, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$result_info  = curl_exec($ch);

		if(!$result_info) {
			$result_info = curl_error($ch);
		}

		$dsl_token_data = json_decode($result_info,true);
		$status = $dsl_token_data["status"];

		if($status == "success"){
			$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Updated Token!</h4>');

		}else{
			$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-danger text-center">Failed to update token! Please try again in 20 mins.</h4>');
		}
		
		redirect("debugger","refresh");
	}

	public function sync_master() {

		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = $token->access_token;
		$call_url= getenv('AGENT_SYNCER_URL').'agent-listings-updater/'.$this->session->userdata("user_id").'/'.$this->session->userdata("agent_id").'?access_token='.$access_token;

		$headers = array('Content-Type: application/json');
		$body=array();
		$body_json = json_encode($body, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$result_info  = curl_exec($ch);

		if(!$result_info) {
			$result_info = curl_error($ch);
		}

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Running Sync Master!</h4>');
		redirect("debugger","refresh");
	}

	public function set_freemium(){

		$freemium = $this->Debugger_model->make_site_freemium($this->session->userdata("user_id"));
		echo "user_id : ".$freemium->subscriber_user_id."  |  plan_id : ".$freemium->plan_id."";
	}

	public function set_premium(){

		$premium = $this->Debugger_model->make_site_premium($this->session->userdata("user_id"));
		echo "user_id : ".$premium->subscriber_user_id."  |  plan_id : ".$premium->plan_id."";
	}

	public function facebook_scraper() {

		$spw_url = $this->scrape_spw_url();
		$active_url = $this->scrape_active_url();
		
		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Scraped Active and SPW Url!</h4>');
		redirect("debugger","refresh");
	}

	public function facebook_url_scraper( $url=""){

        $link = $url;
        //$access_token = getenv('FACEBOOK_APP_TOKEN');
        $access_token = "867016703396837|HUy0w10u4ijhj49ohLJwXA2RbzQ";
        $url = 'https://graph.facebook.com/v2.9/?scrape=true&id='.$link.'&access_token='.$access_token.'';

        $json = file_get_contents($url, false, stream_context_create(
            array (
                'http' => array(
                    'method'    => 'POST',
                    'header'    => 'Content-type: application/x-www-form-urlencoded',
                    'content'   =>  $url,
                    'ignore_errors' => TRUE
                )
            )
        ));
        $obj = json_decode($json, true);
        //printA($obj);exit;

        return TRUE;  
    }

    public function scrape_spw_url(){

        //scrape spw url before posting to facebook
        $param["mlsID"] = $this->session->userdata("agent_id");
        $spw_listings = $this->single_property->get_your_properties_listings(FALSE, $param);
        //printA($spw_listings);exit;

         //get custom domain url
        $domain_info = $this->Agent_sites_model->fetch_domain_info();
        if(!empty($domain_info)) {
            $base =  "https://www.";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $base_url = "https://www.agentsquared.com";
        }

        if(!empty($spw_listings)){

            foreach($spw_listings as $spw){

            	//get spw url in domains table
	            $listingid = $spw->ListingID;
	            $property_name = url_title($spw->unparsedaddr, '-', TRUE);
	            $property_uri = implode(
	              '~',
	              array(
	                  $property_name,
	                  $listingid,
	                  $this->session->userdata("user_id")
	              )
	            );

               $this->facebook_url_scraper($base_url."property-site/".$property_uri); 
            }
        }

        return TRUE;
    }

     public function scrape_active_url(){

        //scrape active properties before posting to facebook
        $domain_info = $this->Agent_sites_model->fetch_domain_info();
        if(!empty($domain_info)) {
            $base =  "https://www.";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $base_url = base_url();
        }

        $active_listings = Modules::run('single_property_listings/getallActiveProperties');

        if(!empty($active_listings)){

            foreach($active_listings as $active){

                $listing_id = $active->Id;

                 $url_rewrite = url_title("{$active->StandardFields->UnparsedFirstLineAddress} {$active->StandardFields->PostalCode}");

               	 $url = $base_url."property-details/".$url_rewrite."/".$listing_id."";

               if(!empty($url)){
                    $this->facebook_url_scraper($url);
               }
                
            }
        }

        return TRUE;
    }

	public function listings_updater_debugger_3h(){

		$listings_data = $this->Debugger_model->get_date_listings_updated_3h();
		printA($listings_data);exit;

	}

	public function listings_updater_debugger_24h(){

		$listings_data = $this->Debugger_model->get_date_listings_updated_24h();
		printA($listings_data);exit;

	}

	public function listings_updater_debugger_48h(){

		$listings_data = $this->Debugger_model->get_date_listings_updated_48h();
		printA($listings_data);exit;

	}

	public function listings_updater_debugger_72h(){

		$listings_data = $this->Debugger_model->get_date_listings_updated_72h();
		printA($listings_data);exit;

	}

	public function listings_updater_endpoint_3h(){

		$listings_data = $this->Debugger_model->get_date_listings_updated_3h();
		//printA($listings_data);exit;

		foreach($listings_data as $data){

			$slack_message = "{
		      	'text': 'Failed to Update site within 3 hours!',
				'attachments': [
					{
			          	'title': '".$data->domains."',
	          		  	'title_link': 'http://".$data->domains."',
			          	'text': 'User Id: ".$data->user_id."\n Date Created: ".$data->date_created."',
					}
			  	],
		   	}";
	
			$this->slack_webhook($slack_message);
		}	
	}

	public function listings_updater_endpoint_24h(){

		$listings_data = $this->Debugger_model->get_date_listings_updated_24h();
		//printA($listings_data);exit;

		foreach($listings_data as $data){

			$slack_message = "{
		      	'text': 'Failed to Update site within 24 hours!',
				'attachments': [
					{
			          	'title': '".$data->domains."',
	          		  	'title_link': 'http://".$data->domains."',
			          	'text': 'User Id: ".$data->user_id."\n Date Created: ".$data->date_created."',
					}
			  	],
		   	}";
	
			$this->slack_webhook($slack_message);
		}		
	}

	public function listings_updater_endpoint_48h(){

		$listings_data = $this->Debugger_model->get_date_listings_updated_48h();
		//printA($listings_data);exit;

		foreach($listings_data as $data){

			$slack_message = "{
		      	'text': 'Failed to Update site within 48 hours!',
				'attachments': [
					{
			          	'title': '".$data->domains."',
	          		  	'title_link': 'http://".$data->domains."',
			          	'text': 'User Id: ".$data->user_id."\n Date Created: ".$data->date_created."',
					}
			  	],
		   	}";
	
			$this->slack_webhook($slack_message);
		}	
	}

	public function listings_updater_endpoint_72h(){

		$listings_data = $this->Debugger_model->get_date_listings_updated_72h();
		//printA($listings_data);exit;

		foreach($listings_data as $data){

			$slack_message = "{
		      	'text': 'Failed to Update site within 72 hours!',
				'attachments': [
					{
			          	'title': '".$data->domains."',
	          		  	'title_link': 'http://".$data->domains."',
			          	'text': 'User Id: ".$data->user_id."\n Date Created: ".$data->date_created."',
					}
			  	],
		   	}";
	
			$this->slack_webhook($slack_message);
		}	
	}

	public function slack_webhook($payload = NULL) {

		$url = "https://hooks.slack.com/services/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";
		
		$ch = curl_init( $url );
		# Setup request to send json via POST.
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		# Send request.
		$result = curl_exec($ch);
		curl_close($ch);

	}

	 public function show_newly_onboarded_agents() {

	 	$agents12h = $this->Debugger_model->get_onboarded_agent_12h();
	 	$agents18h = $this->Debugger_model->get_onboarded_agent_18h();
	 	$agents24h = $this->Debugger_model->get_onboarded_agent_24h();

      	$agents = array(
      		'12hrs' => $agents12h,
      		'18hrs' =>$agents18h,
      		'24hrs' =>$agents24h
      	);

      	printA($agents);exit;
    }

    public function sync_sold_listings_onboarded_agent_12h (){

		$agents = $this->Debugger_model->get_onboarded_agent_12h();
		//printA($agents);exit;

		if(!empty($agents)){

			foreach($agents as $agent){

				$sold_listings = Modules::run('mysql_properties/get_sold_listings', $agent->agent_id);

				if(!empty($sold_listings)) {
					$this->save_listings($agent->id, $sold_listings, "sold");
				} else{
					$this->delete_listings($agent->id, "sold");
				}
			}

		}
	}

	public function sync_sold_listings_onboarded_agent_18h (){

		$agents = $this->Debugger_model->get_onboarded_agent_18h();
		//printA($agents);exit;

		if(!empty($agents)){

			foreach($agents as $agent){
			
				$sold_listings = Modules::run('mysql_properties/get_sold_listings', $agent->agent_id);

				if(!empty($sold_listings)) {
					$this->save_listings($agent->id, $sold_listings, "sold");
				} else{
					$this->delete_listings($agent->id, "sold");
				}

			}

		}

	}

	public function sync_sold_listings_onboarded_agent_24h (){

		$agents = $this->Debugger_model->get_onboarded_agent_24h();
		//printA($agents);exit;

		if(!empty($agents)){

			foreach($agents as $agent){

				$sold_listings = Modules::run('mysql_properties/get_sold_listings', $agent->agent_id);

				if(!empty($sold_listings)) {
					$this->save_listings($agent->id, $sold_listings, "sold");
				} else{
					$this->delete_listings($agent->id, "sold");
				}

			}

		}

	}

	public function sync_nearby_latlon(){

    $limit = isset($_GET['limit']) ? $_GET['limit'] : "" ;
    $debugLimit = isset($_GET['debuggerLimit']) ? $_GET['debuggerLimit'] : "" ;
    $debug = isset($_GET['debugger']) ? $_GET['debugger'] : "" ;

    $all_agents_no_latlon = $this->Debugger_model->get_all_agents_no_latlon();

    if($debug){
      printA($all_agents_no_latlon);exit;
    }

    $agents_latlon = $this->Debugger_model->get_agents_no_latlon($limit);

    if($debugLimit){
      printA($agents_latlon);exit;
    }

    if(!empty($agents_latlon)){

      foreach($agents_latlon as $agent){

        $account_info = $account_info = $this->get_account_address($agent->id, "account_info");
        //printA($account_info);exit;

        if(!empty($account_info->Addresses[0]->Address) && $account_info->Addresses[0]->Address != "********"){

          $address = $account_info->Addresses[0]->Address;
          $latlon = Modules::run('mysql_properties/getlatlon', $address);
          //printA($address);exit;

          if(!empty($latlon)){
            $this->pm->update_nearby_latlon($agent->agent_id,$latlon);
          }
        }
      }
    }
  }

	public function get_account_address($user_id =NULL, $type=NULL){

		if($type && $user_id) {
	   		$account_json = $this->Debugger_model->get_account_address($user_id, $type);
	   		if(!empty($account_json)) {

	   			$account_data = gzdecode($account_json->account);
	   			$account_data = json_decode($account_data);
	   			return $account_data;
	   		}
	  	}
	}

	public function delete_liondesk_account() {

		$this->Debugger_model->delete_liondesk_account();

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Delete LionDesk Account!</h4>');
		redirect("debugger","refresh");

	}

	public function reset_password(){

   		$this->Debugger_model->reset_password();

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Reset Password!</h4>');
		redirect("debugger","refresh");
	}

}
