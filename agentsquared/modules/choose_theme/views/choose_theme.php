<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?> 
<div class="content-wrapper">  
  <div class="page-title">
    <h3 class="heading-title">
      <span>Theme Selection</span>
    </h3>
    <p>Customize your site's look and feel</p>
  </div>       
  <div class="theme-loader"></div>
    <section class="content">
        <?php //if( !empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret")) ) : ?>
            <?php $this->load->view('session/launch-view'); ?>
        <?php //endif; ?>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="theme-block <?php if ($branding->theme == "agent-haven") { echo "selected active"; } ?>" data-price="Free" data-img="<?= base_url()?>assets/images/dashboard/theme/agent-haven.jpg" data-features="" data-theme="Agent Haven">
                            <p class="theme-name">Agent Haven</p>
                            <p class="theme-author">By AgentSquared Team</p>
                            <div class="theme-img" style="background-image: url('<?= base_url()?>assets/images/dashboard/theme/agent-haven.jpg')"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="theme-block <?php if ($branding->theme == "desert") { echo "selected active"; } ?>" data-price="Free with Premium user" data-img="<?= base_url()?>assets/images/dashboard/theme/desert.jpg" data-features="Advanced Homepage Layout Builder" data-theme="Desert">
                            <p class="theme-name">Desert</p>
                            <p class="theme-author">By AgentSquared Team</p>
                            <div class="theme-img" style="background-image: url('<?= base_url()?>assets/images/dashboard/theme/desert.jpg')"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="theme-block" data-price="Free" data-img="" data-features="Advanced " data-theme="Sapphire">
                            <p class="theme-name">Sapphire</p>
                            <p class="theme-author">By AgentSquared Team</p>
                            <div class="theme-soon">
                                <p>Coming <br> Soon</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="theme-block" data-price="9" data-img="" data-features="Advanced Homepage Layout Builder" data-theme="Khaldrick">
                            <p class="theme-name">Khaldrick</p>
                            <p class="theme-author">By AgentSquared Team</p>
                            <div class="theme-soon" >
                                <p>Coming <br> Soon</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="preview-section">
                    <div class="preview-details">
                        <input type="hidden" name="theme_name" value="" class="theme_name">
                        <input type="hidden" name="agent_subscription" value="<?php echo is_freemium()->plan_id; ?>" class="agent_subscription">
                        <p class="preview-theme-name">Theme: <span><?=str_replace("-", " ", $branding->theme); ?></span></p>
                        <p class="preview-info">Author: <span>AgentSquared Team</span></p>
                        <p class="preview-info">Author URL: <span class="site-address"><a href="https://www.agentsquared.com">https://www.agentsquared.com</a></span></p>
                        <p class="preview-info price">Price: <span></span></p>
                        <p class="preview-info features">Features: <span>Advanced Homepage Layout Builder</span></p>
                        <ul class="preview-action">
                            <li><a href="#" class="btn btn-light-green btn-preview" data-toggle="modal" data-target="#modalPreview">Preview</a></li>
                            <!-- <li><a href="#" class="btn btn-light-blue">Select</a></li> -->
                            <li class="action-active-select">
                                <a href="javascript:(void)" class="btn btn-light-blue-disable btn-current">Current</a>
                                <?php //if (!empty(is_freemium()) && is_freemium()->plan_id != 1): ?>
                                <a href="?modal_premium=true" class="btn btn-light-blue btn-premium" style="display:none;">Upgrade to Premium</a>
                                <?php //else: ?>
                                <a href="javascript:(void)" class="btn btn-light-blue btn-select" style="display:none;">Select</a>
                                <?php //endif ?>
                            </li>
                        </ul>
                    </div>
                    <div class="preview-img" ></div>
                </div>
            </div>
        </div>
    <!-- Modal fullscreen -->
    <div class="modal modal-fullscreen fade modal-demo" id="modalPreview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Agent Haven</h4>
          </div>
          <div class="modal-body">
            <div class="theme-preview theme-agenthaven" style="display:none;">
                <div class="owl-carousel-preview owl-theme">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <div class="item">
                            <img src="<?=base_url()?>assets/images/dashboard/theme/agent-haven.jpg" alt="">
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="theme-preview theme-desert" style="display:none;">
                <div class="owl-carousel-preview owl-theme">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <div class="item">
                            <img src="<?=base_url()?>assets/images/dashboard/theme/desert.jpg" alt="">
                        </div>
                    <?php } ?>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </section>
</div>
<!-- Modal -->
<?php $this->load->view('session/footer'); ?>
