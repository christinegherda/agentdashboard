<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Choose_theme_model extends CI_Model {

	public function __construct() {
        parent::__construct();

    }

    public function get_theme() {
    	$user_id = $this->session->userdata('user_id');
		$array = array('id' => $user_id);
	    $this->db->where($array);
	    $query = $this->db->get('users')->row();

	    if(isset($query)) {
		    $data[] = array(
		    	'id' => $query->id,
		    	'theme' => $query->theme
		    );
		    return $data;
		} else
			return false;
    }

    public function add_theme($arr) {
 
    	$array = array('id' => $arr['id']);
    	$this->db->where($array);
    	$data = array(
    		'theme' => $arr['theme']
    	);
	    $this->db->update('users', $data);
    }

    public function update_theme($theme) {
    	$id = $this->get_theme();
    	$array = array('id' => $id[0]['id']);
    	$this->db->where($array);
    	$data = array(
    		'theme' => $theme
    	);
	    $this->db->update('users', $data);
    }

    public function activated_theme_wizard($theme) {
        $id = $_SESSION['user_id'];
        $arr = array(
            'theme' => $theme
        );
        $this->db->where('id', $id);
        $this->db->update('users', $arr);
    }

    public function update_agent_theme( $data = array(), $user_id )
    {
        $theme["theme"] = $data['theme'];
        $this->db->where("id", $user_id);
        $this->db->update("users", $theme);

        return TRUE;
    }
}