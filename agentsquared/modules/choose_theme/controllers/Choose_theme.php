<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Choose_theme extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');
		// if(empty(config_item("idx_api_key"))) {
		// 	$this->db->select("*")->from("property_idx")->where("user_id", $this->session->userdata('user_id'));
		// 	$propert_idx = $this->db->get()->row();
		// 	$this->config->set_item('idx_api_key',$propert_idx->api_key);  
		// 	$this->config->set_item('idx_api_secret', $propert_idx->api_secret);
		// }
		$this->load->model('choose_theme_model', 'theme_model');
		$this->load->model("agent_sites/Agent_sites_model");
	}

	public function index() {

		$data['title'] = "Choose theme";
		$theme = $this->theme_model->get_theme();

		if($theme) {
			$data['id'] = $theme[0]['id'];
			$data['theme'] = $theme[0]['theme'];
		} else {
			$data['theme'] = null;
		}
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["js"] = array("choose-theme/choose-theme.js");
		$this->load->view('choose_theme', $data);
	}

	public function add_theme($user_id, $theme) {
		$arr = array(
			'id' => $user_id,
			'theme' => $theme
		);
		$this->theme_model->add_theme($arr);
	}

	public function activate_theme() {
		$theme = $this->input->post('theme');
		$this->theme_model->update_theme($theme);
		$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success text-center">'. ucfirst($theme) .' Theme Successfully Activated!</div>');
		redirect('choose_theme');
	}

	public function update_theme() {
		$data['theme'] = $this->input->post('theme_name');
		$user_id = $this->session->userdata('user_id');
		$theme_update = $this->theme_model->update_agent_theme($data, $user_id);
		echo json_encode($theme_update);
	}
}

?>