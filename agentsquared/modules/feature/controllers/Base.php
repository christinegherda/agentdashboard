<?php
/*
	* Feature Control (Freemium)
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Base extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('freemium_model', 'freemium');
	}

	public function index() {
		// $this->session->userdata('agent_id');
	}
	public function get_user_plan() {
		$this->output->enable_profiler(TRUE);
		$agent_id = $this->session->userdata('agent_id');
		
		$user_plan = $this->freemium->get_user_plan($agent_id);
	}
	public function get_plans() {
		// Fetch System Wide Plans
	}
	public function get_agent_plan() {
		// Fetch Current Agent Plan
	}
}

?>