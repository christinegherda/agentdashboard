<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Freemium_model extends CI_Model {
	public function __construct() {
        parent::__construct();
    }
    public function get_user_plan($agent_id = NULL) {
    	$query = false;

    	if($agent_id) {
    		$this->db->select("*");
	    	$this->db->from("subscription_view");
	    	$this->db->where("agent_id", $agent_id);
	    	$query = $this->db->get()->row();
    	}
    	return $query;
    }
}