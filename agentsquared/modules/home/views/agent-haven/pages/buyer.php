<div class="page-content">
    <section class="property-detail-content">
        <div class="container">
            <div class="row">
            	<!-- Sidebar Area -->
    			<?php $this->load->view($theme."/session/sidebar"); ?>
    			<div class="col-md-9 col-sm-9">
                     <div class="buyer-blurb">
                            <?php if(isset($page_data) AND !empty($page_data)){?>

                                <?php echo $page_data['content']; ?>

                            <?php }?>
                    </div>
                    <?php $message = $this->session->flashdata("message"); $success = $this->session->flashdata("success"); ?>
                    <?php if(isset($message)) : ?>
                        <div class="alert <?=(isset($success) AND $success) ? "alert-success" : "alert-danger"?>"> <?php echo $message; ?> </div>
                    <?php endif;?>
                    <div class="property-details">
                        <form action="<?php echo site_url('home/home/save_customer_buyer'); ?>" method="POST" class="">
                            <div class="contact-information">
                                <h4>Contact Information</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" name="buyer[fname]"  class="form-control" required>
                                        </div>    
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" name="buyer[lname]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" name="buyer[email]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phone</label>
                                            <input type="text" name="buyer[phone]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Mobile</label>
                                            <input type="text" name="buyer[mobile]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Fax</label>
                                            <input type="text" name="buyer[fax]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Address</label>
                                            <input type="text" name="buyer[address]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">City</label>
                                            <input type="text" name="buyer[city]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">State</label>
                                            <input type="text" name="buyer[state]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Zip Code</label>
                                            <input type="text" name="buyer[zip_code]" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="property-information clearfix">
                                <h4>Property Information</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">No. of Bedrooms</label>
                                            <select name="buyer[bedrooms]" id="" class="form-control" required>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">No. of Bathrooms</label>
                                            <select name="buyer[bathrooms]" id="" class="form-control" required>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Square Feet</label>
                                            <select name="buyer[square_feet]" id="" class="form-control" required>
                                                <option value="Less than 1000">< 1000</option>
                                                <option value="1000 - 2000">1000 - 2000</option>
                                                <option value="2000 - 3000">2000 - 3000</option>
                                                <option value="3000 - 5000">3000 - 5000</option>
                                                <option value="6000 - 7000">6000 - 7000</option>
                                                <option value="8000 - 9000">8000 - 9000</option>
                                                <option value="10000 - 11000">10000 - 11000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Contact You By</label>
                                            <select name="buyer[contact_by]" id="" class="form-control" required>
                                                <option value="email">Email</option>
                                                <option value="phone">Phone</option>
                                                <option value="mobile">Mobile</option>
                                                <option value="fax">Fax</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Price Range</label>
                                            <select name="buyer[price_range]" id="" class="form-control" required>
                                                <option value="Less than $200,000">Less than $200,000</option>
                                                <option value="$200,000 - $300,000">$200,000 - $300,000</option>
                                                <option value="$300,000 - $400,000">$300,000 - $400,000</option>
                                                <option value="$400,000 - $500,000">$400,000 - $500,000</option>
                                                <option value="$500,000 - $600,000">$500,000 - $600,000</option>
                                                <option value="$600,000 - $700,000">$600,000 - $700,000</option>
                                                <option value="$700,000 - $800,000">$700,000 - $800,000</option>
                                                <option value="$800,000 - $900,000">$800,000 - $900,000</option>
                                                <option value="$900,000 - $1,000,000">$900,000 - $1,000,000</option>
                                                <option value="$1,000,000 - $2,000,000">$1,000,000 - $2,000,000</option>
                                                <option value="$2,000,000 - $3,000,000">$2,000,000 - $3,000,000</option>
                                                <option value="$3,000,000 - $4,000,000">$3,000,000 - $4,000,000</option>
                                                <option value="$4,000,000 - $5,000,000">$4,000,000 - $5,000,000</option>
                                                <option value="More than $5,000,000">More than $5,000,000</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="moving-detail">
                                <h4>Moving Details</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">When do you want to move?</label>
                                            <select name="buyer[when_do_you_want_to_move]" id="" class="form-control" required>
                                                <option value="Less than 30 days">Less than 30 days</option>
                                                <option value="1 Month">1 Month</option>
                                                <option value="2 Months">2 Months</option>
                                                <option value="3 Monts">3 Monts</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">When did you start looking?</label>
                                            <select name="buyer[when_did_you_start_looking]" id="" class="form-control" required>
                                                <option value="1 Month">1 Month</option>
                                                <option value="2 Months">2 Months</option>
                                                <option value="3 Months">3 Months</option>
                                                <option value="4 Months">4 Months</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Where would you like to own?</label>
                                            <input name="buyer[wher_would_you_like_to_own]" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Are you currently with an agent?</label>
                                            <select name="buyer[are_you_currently_with_an_agent]" id="" class="form-control" required>
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Describe your dream home</label>
                                            <textarea name="buyer[describe_your_dream_home]" id="" cols="30" rows="10" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" required> I consent to receiving emails containing real estate related information from this site. I understand that I can unsubscribe at any time.
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-default submit-button">Submit Form</button>    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
    		</div>
		</div>
	</section>
</div>