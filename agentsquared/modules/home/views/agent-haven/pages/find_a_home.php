
        <?php
            $this->load->view($theme.'/pages/page_view/advance-search-form');  
        ?> 

        <?php $this->load->view($theme.'/pages/findahome_view/search-map');?>

    <section class="property-main-content">
        <div class="container">
            <div class="row">
                <?php if( isset($_GET) AND !empty($_GET)) : ?>
                <div class="sorting">
                    <div class="col-md-offset-3 col-md-10">                       
                        <div class="col-md-3 pull-right">
                            <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" class="btn btn-default submit-button follow-search"><i class="fa fa-undo"></i> Save Search </a>
                        </div>
                    </div>
                </div>
                <?php endif;?>
                <div class="col-md-3 col-sm-3">
                  
                    <?php 

                        $this->load->view($theme.'/pages/findahome_view/mortgage-calc');

                        $this->load->view($theme.'/pages/findahome_view/customer-question');
                    ?>

                </div>

                <?php $this->load->view($theme.'/pages/findahome_view/search-result'); ?>

            </div>
        </div>
    </section>
