        
        <section class=" <?=(isset($is_new_featured)) ? "featured-listing-area" : "new-listing-area"?>">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">

                        <?php if($is_new_featured){?>
                            <div class="trapezoid"></div>
                        <?php }?>

                        <h2 class="<?=(isset($is_new_featured)) ? "section-title" : ""?> other-listing-title text-center"><span><?php echo $new_title['option_title'];?></span></h2>

                         <?php if(isset($new_listings) AND !empty($new_listings)){?>
                            <p class="viewall">
                                <a href="<?php echo site_url('home/listings?Search=&min_price=&max_price=&bedroom=&bathroom=&listing_change_type%5B%5D=New+Listing&house_size=0&lot_size=0&house_age='); ?>" target="_blank">View All</a>    
                            </p>
                        <?php }?>

                    </div>
                    <div class="col-md-12">
                        <div class="featured-list">
                          <?php

                                $count = 0;

                               foreach($new_listings as $new_listing){

    
                                    if($count < 4) { ?>
                                <div class="col-md-3 col-sm-3 featured-list-item">
                                    <div class="property-image <?php if($new_listing->StandardFields->PropertyClass == 'Land' || $new_listing->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                     <?php if($is_capture_leads) { ?>
                                        <?php if(!isset($_SESSION['customer_id'])) { ?>
                                            <a href="#" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?=$new_listing->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                        <?php } else { ?>
                                            <?php if(!empty($token_checker)){?>
                                                <a href="<?= base_url();?>other-property-details/<?=$new_listing->StandardFields->ListingKey;?>">
                                            <?php }?>
                                        <?php }  if(isset($new_listing->Photos->Uri300)) { ?>
                                            <img src="<?=$new_listing->Photos->Uri300?>" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                            <?php }
                                            else { ?> 
                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                            <?php } ?>
                                    <?php } else { ?>
                                        <?php if(!empty($token_checker)){?>
                                            <a href="<?= base_url();?>other-property-details/<?=$new_listing->StandardFields->ListingKey;?>">
                                        <?php } ?>
                                        <?php if(isset($new_listing->Photos->Uri300)) { ?>
                                            <img src="<?=$new_listing->Photos->Uri300?>" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                        <?php }
                                        else { ?>
                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                        <?php } ?>
                                        </a>
                                    <?php } ?>
                                    </div>
                                    <div class="property-listing-status">
                                        <?php echo $new_listing->StandardFields->MlsStatus;?>
                                    </div>
                                    <div class="property-listing-price">
                                        <div class="property-listing-type">
                                            <?=$new_listing->StandardFields->PropertyClass;?>
                                        </div>
                                        $<?=number_format($new_listing->StandardFields->CurrentPrice);?>
                                    </div>
                                    <div class="property-quick-icons">
                                        <ul class="list-inline">

                                        <?php if(isset($new_listing->StandardFields->BedsTotal) && !empty($new_listing->StandardFields->BedsTotal)){
                                                if(($new_listing->StandardFields->BedsTotal != "********")){?>
                                                    <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                                               <?php } else{?>
                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                               <?php } ?>

                                         <?php  } else {?>
                                                <li><i class="fa fa-bed"></i> N/A</li>
                                        <?php }?>

                                        <?php if(isset($new_listing->StandardFields->BathsTotal) && !empty($new_listing->StandardFields->BathsTotal)){
                                                if(($new_listing->StandardFields->BathsTotal != "********")){?>
                                                    <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>
                                               <?php } else{?>
                                                    <li><i class="icon-toilet"></i> N/A</li>
                                               <?php } ?>

                                         <?php  } else {?>
                                               <li><i class="icon-toilet"></i> N/A</li>
                                        <?php }?>
                                        
                                        <?php
                                        if(!empty($new_listing->StandardFields->BuildingAreaTotal) && ($new_listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($new_listing->StandardFields->BuildingAreaTotal)) {?>

                                            <li class="lot-item"> <?=number_format($new_listing->StandardFields->BuildingAreaTotal)?> sqft </li>

                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeArea) && ($new_listing->StandardFields->LotSizeArea != "0")   && is_numeric($new_listing->StandardFields->LotSizeArea)) {
                                               
                                            if(!empty($new_listing->StandardFields->LotSizeUnits) && ($new_listing->StandardFields->LotSizeUnits) === "Acres"){?>

                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                            <?php } else {?>

                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeArea)?> </li>

                                            <?php }?>

                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeSquareFeet) && ($new_listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($new_listing->StandardFields->LotSizeSquareFeet)) {?>

                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeSquareFeet)?></li> 

                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeAcres) && ($new_listing->StandardFields->LotSizeAcres != "0")   && is_numeric($new_listing->StandardFields->LotSizeAcres)) {?>

                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeAcres,2 ,'.',',')?></li>
                                                
                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeDimensions) && ($new_listing->StandardFields->LotSizeDimensions != "0")   && ($new_listing->StandardFields->LotSizeDimensions != "********")) {?>

                                                        <li class="lot-item">lot size area: <?=$new_listing->StandardFields->LotSizeDimensions?></li>
                                        <?php } else {?>
                                                        <li class="lot-item">lot size area: N/A</li>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="property-listing-description">
                                        <p>
                                            <?php if(!empty($token_checker)){?>
                                                <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey; ?>">
                                            <?php }?>
                                                <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                            </a>
                                        </p>
                                        <p>
                                          <?php
                                                $mystring = $new_listing->StandardFields->City;
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);

                                            if($pos === false)
                                                echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                else
                                                echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                            ?>
                                        </p>    
                                    </div>
                                </div>

                        <?php  } $count++;

                             } 
                        ?> 
                        </div>

                    </div>
                </div>
            </div>
        </section>