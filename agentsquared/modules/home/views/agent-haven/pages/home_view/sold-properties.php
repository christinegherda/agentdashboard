<?php if (isset($sold_listings) && !empty($sold_listings)): ?>
         <div class="col-md-12 properties-sold">
            <h4>Recent Properties Sold</h4>
            <div class="sold-property-container"> 

            <?php foreach ($sold_listings as $key => $sold): ?>
                <?php if ($key == 7): ?>
                    <div class="other-listing-item last-item-sold">
                        <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                        <div class="sold-image">
                            <div class="last-item-viewall">
                                <a href="<?php echo base_url()?>home/sold_listings" target="_blank">View All</a>
                            </div>
                            <?php if($is_capture_leads) {
                                if(!isset($_SESSION['customer_id'])) { ?>
                                    <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<<?=$sold->StandardFields->ListingKey;?>" data-propertyType="sold_property">
                                <?php } else { ?>
                                    <a href="<?php echo base_url()?>home/sold_listings" target="_blank">
                                <?php } if(isset($sold->StandardFields->Photos[0]->Uri300)) { ?>
                                        <img src="<?=$sold->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                    <?php } else { ?>
                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                    <?php } ?>
                                    </a>
                            <?php } else { ?>
                                <a href="<?php echo base_url()?>home/sold_listings" target="_blank">
                                <?php if(isset($sold->StandardFields->Photos[0]->Uri300)) { ?>
                                    <img src="<?=$sold->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                <?php } ?>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="other-listing-item">
                        <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                        <div class="sold-image">
                            <?php if($is_capture_leads) {
                                if(!isset($_SESSION['customer_id'])) { ?>
                                    <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<<?=$sold->StandardFields->ListingKey;?>" data-propertyType="sold_property">
                                <?php } else { ?>
                                    <a href="<?= base_url();?>property-details/<?=$sold->StandardFields->ListingKey;?>">
                                <?php } if(isset($sold->StandardFields->Photos[0]->Uri300)) { ?>
                                        <img src="<?=$sold->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                    <?php } else { ?>
                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                    <?php } ?>
                                    </a>
                            <?php } else { ?>
                                <a href="<?= base_url();?>property-details/<?=$sold->StandardFields->ListingKey;?>">
                                <?php if(isset($sold->StandardFields->Photos[0]->Uri300)) { ?>
                                    <img src="<?=$sold->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                <?php } ?>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="sold-property-details">
                             <?php if($is_capture_leads) { ?>
                                <p>
                                <?php if(!isset($_SESSION['customer_id'])) { ?>
                                    <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?=$sold->StandardFields->ListingKey;?>" data-propertyType="sold_property">
                                <?php } else { ?> 
                                    <a href="<?= base_url();?>property-details/<?=$sold->StandardFields->ListingKey;?>">
                                <?php } ?>
                                   <?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>
                                </a>
                                </p>
                            <?php } else { ?>
                                <p><a href="<?= base_url();?>property-details/<?=$sold->StandardFields->ListingKey;?>"><?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?></a></p>
                            <?php } ?>
                            <p><i class="fa fa-map-marker"></i>
                                <?php
                                    $mystring = $sold->StandardFields->City;
                                    $findme   = '*';
                                    $pos = strpos($mystring, $findme);
                                    
                                    if($pos === false) 
                                        echo $sold->StandardFields->City . ", " . $sold->StandardFields->StateOrProvince . " " . $sold->StandardFields->PostalCode;
                                    else
                                        echo $sold->StandardFields->PostalCity . ", " . $sold->StandardFields->StateOrProvince . " " . $sold->StandardFields->PostalCode;
                                ?>
                            </p>
                            <p><i class="fa fa-usd"></i> <?=number_format($sold->StandardFields->CurrentPrice);?></p>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
            </div>   
        </div>
<?php endif ?>