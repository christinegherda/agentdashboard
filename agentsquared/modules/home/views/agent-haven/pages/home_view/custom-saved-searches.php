<?php if (!empty($saved_searches_selected)): ?>
  <div class="save-search-slider" style="margin-bottom: 20px;">
      <div class="container">
        <!-- Set up your HTML -->
        <h2 class="other-listing-title">Recommended Listings</h2>
        <div class="owl-carousel-saved-search">
            <?php $i = 0;
            foreach ($saved_searches_selected as $key => $saved): ?>
              <?php if ($key == 4): ?>
                <div class="saved-item viewall-list">
                  <div class="overlay">
                   <a href="<?php echo base_url()?>home/all_saved_searches" target="_blank">View All <br> Recommended Listings</a> 
                  </div>
                </div>
              <?php else: ?>
                  <div class="saved-item">
                    <div class="saved-caption">
                      <a href="<?php echo base_url()?>home/saved_searches/<?php echo $saved['Id'];?>" target="_blank">View All</a>
                    </div>
                    <p class="searched-title">
                      <i class="fa fa-tasks"></i> <br> <?=$saved['Name']?>
                    </p>
                  </div>
              <?php endif ?>
            <?php 
            if ($i++ == 4) break;
            endforeach ?>
        </div>
      </div>
  </div>
<?php endif ?>