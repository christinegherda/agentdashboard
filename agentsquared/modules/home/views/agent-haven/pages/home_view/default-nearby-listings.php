    <?php if (isset($nearby_listings) && !empty($nearby_listings)): ?>
        <div class="col-md-6 col-sm-12">
            <div class="nearby-listings-area">
                <h2 class="other-listing-title">Nearby Listings
                    <span class="viewall-newlisting">
                        <a href="<?php base_url()?>home/nearby_listings" target="_blank" data-original-title="" title="">View All</a>    
                    </span>
                </h2>
                <ul>
                    <?php foreach($nearby_listings as $nearby) : ?>
                        <li>
                            <div class="col-md-4 col-sm-4 no-padding-left">
                                <div class="nearby-image">
                                 <?php if($is_capture_leads) { ?>
                                        <?php if(!isset($_SESSION['customer_id'])) { ?>
                                            <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?=$nearby->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                        <?php } else { ?>
                                            <?php if(!empty($token_checker)){?>
                                                <a href="<?= base_url();?>other-property-details/<?=$nearby->StandardFields->ListingKey;?>">
                                            <?php }?>
                                        <?php } if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
                                                <img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                       <?php } elseif(isset($nearby->Photos->Uri300)) { ?>
                                            <img src="<?=$nearby->Photos->Uri300;?>" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                        <?php } else{?>
                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                          <?php  } ?>
                                            </a>
                                    <?php } else { ?>
                                        <?php if(!empty($token_checker)){?>
                                            <a href="<?= base_url();?>other-property-details/<?=$nearby->StandardFields->ListingKey;?>">
                                        <?php }?>
                                        <?php if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
                                            <img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                        <?php } elseif(isset($nearby->Photos->Uri300)) { ?>
                                            <img src="<?=$nearby->Photos->Uri300;?>" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                        <?php } else{?>
                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                          <?php  } ?>
                                        </a>
                                    <?php } ?>
                                </div>
                                <div class="property-listing-status">
                                    <?php echo $nearby->StandardFields->MlsStatus;?>
                                </div>
	                            <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="visible-xs-block property-listing-price">
		                            <div class="property-listing-type"><?= $nearby->StandardFields->PropertyClass ?></div>
		                            <span itemprop="priceCurrency" content="USD">$</span>
		                            <span itemprop="price" content="<?= $nearby->StandardFields->CurrentPrice ?>"><?=number_format($nearby->StandardFields->CurrentPrice);?></span>
	                            </div>
                            </div>
                            <div class="nearby-description clearfix">
                                <div class="col-md-8 col-sm-8">
                                    <h4>
                                         <?php if($is_capture_leads) { ?>
                                            <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                <a href="#" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?=$nearby->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                            <?php } else { ?>
                                                <?php if(!empty($token_checker)){?>
                                                    <a href="<?= base_url();?>other-property-details/<?=$nearby->StandardFields->ListingKey;?>">
                                                <?php }?>
                                            <?php } ?>
                                                    <b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                </a>
                                        <?php } else { ?>
                                            <?php if(!empty($token_checker)){?>
                                                <a href="<?= base_url();?>other-property-details/<?=$nearby->StandardFields->ListingKey;?>">
                                            <?php }?>
                                                <b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
                                            </a>
                                        <?php } ?>
                                   </h4>
	                                <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="visible-lg-block visible-md-block visible-sm-block property-listing-price">
		                                <div class="property-listing-type"><?= $nearby->StandardFields->PropertyClass ?></div>
		                                <span itemprop="priceCurrency" content="USD">$</span>
		                                <span itemprop="price" content="<?= $nearby->StandardFields->CurrentPrice ?>"><?=number_format($nearby->StandardFields->CurrentPrice);?></span>
	                                </div>
                                    <p>
                                        <?php
                                            $mystring = $nearby->StandardFields->City;
                                            $findme   = '*';
                                            $pos = strpos($mystring, $findme);

                                        if($pos === false)
                                            echo $nearby->StandardFields->City . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
                                            else
                                            echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
                                        ?>
                                    </p>
                                    <ul class="list-inline">

                                    <?php if(isset($nearby->StandardFields->BedsTotal) && !empty($nearby->StandardFields->BedsTotal)){
                                            if(($nearby->StandardFields->BedsTotal != "********")){?>
                                                <li><i class="fa fa-bed"></i> <?=$nearby->StandardFields->BedsTotal?> Bed</li>
                                           <?php } else{?>
                                                <li><i class="fa fa-bed"></i> N/A</li>
                                           <?php } ?>

                                     <?php  } else {?>
                                            <li><i class="fa fa-bed"></i> N/A</li>
                                    <?php }?>

                                    <?php if(isset($nearby->StandardFields->BathsTotal) && !empty($nearby->StandardFields->BathsTotal)){
                                            if(($nearby->StandardFields->BathsTotal != "********")){?>
                                                <li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsTotal?> Bath</li>
                                           <?php } else{?>
                                                <li><i class="icon-toilet"></i> N/A</li>
                                           <?php } ?>

                                     <?php  } else {?>
                                           <li><i class="icon-toilet"></i> N/A</li>
                                    <?php }?>

                                <?php
                                    if(!empty($nearby->StandardFields->BuildingAreaTotal) && ($nearby->StandardFields->BuildingAreaTotal != "0")   && is_numeric($nearby->StandardFields->BuildingAreaTotal)) {?>

                                    <li class="lot-item"><?=number_format($nearby->StandardFields->BuildingAreaTotal)?> sqft</li>

                                <?php } elseif(!empty($nearby->StandardFields->LotSizeArea) && ($nearby->StandardFields->LotSizeArea != "0")   && is_numeric($nearby->StandardFields->LotSizeArea)) {

                                       
                                        if(!empty($nearby->StandardFields->LotSizeUnits) && ($nearby->StandardFields->LotSizeUnits) === "Acres"){?>

                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                        <?php } else {?>

                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeArea)?> </li>

                                        <?php }?>

                                <?php } elseif(!empty($nearby->StandardFields->LotSizeSquareFeet) && ($nearby->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($nearby->StandardFields->LotSizeSquareFeet)) {?>

                                        <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeSquareFeet)?></li> 


                                 <?php } elseif(!empty($nearby->StandardFields->LotSizeAcres) && ($nearby->StandardFields->LotSizeAcres != "0")   && is_numeric($nearby->StandardFields->LotSizeAcres)) {?>

                                        <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeAcres,2 ,'.',',')?></li>

                                <?php } elseif(!empty($nearby->StandardFields->LotSizeDimensions) && ($nearby->StandardFields->LotSizeDimensions != "0")   && ($nearby->StandardFields->LotSizeDimensions != "********")) {?>

                                        <li class="lot-item">lot size area: <?=$nearby->StandardFields->LotSizeDimensions?></li>
                                <?php } else {?>
                                        <li class="lot-item">lot size area: N/A</li>
                                <?php } ?>
                                </ul>
                                </div>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    
                </ul>
            </div>
        </div> 
    <?php endif ?>