    
    <div class="col-md-4 col-sm-12 mobilepadding-0">
        <div class="mortage-calculator">
            <h3 class="text-center">CONTACT AGENT</h3>

            <div class="question-mess" ></div>  <br/>    
            <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
                <div class="form-group">
                    <input type="text" name="name" value="" class="form-control" placeholder="Name" required>
                </div>
                <div class="form-group">
                    <input type="email" name="email" value="" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <input type="phone" name="phone" value="" class="form-control" placeholder="Phone" required>
                </div>
                <div class="form-group">
                    <textarea name="message" id="" value="" cols="30" rows="10" class="form-control" placeholder="Message" required></textarea>
                </div>
                <button type="submit" class="btn btn-default btn-block submit-button submit-question-button" style="text-transform:uppercase;">Submit</button>
            </form>

        </div>
    </div>