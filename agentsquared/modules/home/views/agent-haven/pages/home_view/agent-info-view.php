
        <div class="col-md-3 col-sm-4 nopadding">
            <div class="agent-image">
               <?php if( isset($user_info->agent_photo) AND !empty($user_info->agent_photo)) { ?>
                     <img src="<?=AGENT_DASHBOARD_URL . 'assets/upload/photo/'.$user_info->agent_photo?>" class="img-thumbnail" width="300">
                <?php } else { ?>
                    <?php if (isset($account_info->Images[0]->Uri) AND !empty($account_info->Images[0]->Uri)){?>
                        <img src="<?=$account_info->Images[0]->Uri?>" class="img-thumbnail" width="300"> 
                     <?php } else { ?>
                        <img src="<?= base_url()?>/assets/images/no-profile-img.gif" alt="No Profile Image">
                     <?php } ?>  
                <?php } ?> 
            </div>
        </div>
        <div class="col-md-9 col-sm-8 nopadding">

          <!-- Get data from Active Listings -->   
                <?php

                if(isset($active_listings) AND !empty($active_listings)){

                        $icount = 1;

                        foreach($active_listings as $active_property){

                            if(isset($active_property) AND !empty($account_info->data)){

                                if($active_property->StandardFields->ListAgentEmail ==  $account_info->Emails[0]->Address || $active_property->StandardFields->ListOfficeName ==  $account_info->Office){

                                    if($icount == 1) {?>

                                    <h4 class="agent-name">
                                        
                                        <?php if (isset($user_info->first_name) AND !empty($user_info->first_name)) {?>
                                            <?php echo $user_info->first_name ?>
                                        <?php } else {?>
                                                <?php echo $active_property->StandardFields->ListAgentFirstName?>
                                        <?php }?>
                                         <?php if (isset($user_info->last_name) AND !empty($user_info->last_name)) {?>
                                            <?php echo $user_info->last_name ?>
                                        <?php } else {?>
                                                <?php echo $active_property->StandardFields->ListAgentLastName?>
                                        <?php }?>   
                                    </h4>

                                    <div class="about-agent">
                                        <?php if (isset($user_info->about_agent) AND !empty($user_info->about_agent)) { ?>

                                            <?php
                                                $img = $user_info->about_agent;
                                                $findImg   = 'data-filename';
                                                $aboutImg = strpos($img, $findImg);
                                            ?>
                                               
                                               <?php if($aboutImg !== false) { ?>

                                                    <?php echo $user_info->about_agent; ?>

                                                <?php } else { ?>  

                                                    <?php echo  $user_info->about_agent = (strlen($user_info->about_agent) > 600) ? substr($user_info->about_agent,0,600).'... <a href="./about">Read more</a>' : $user_info->about_agent; ?>

                                                <?php } ?>
                                            
                                       <?php } ?>
                                    </div>
                                    <ul class="agent-detail">

                                            <?php 
                                                
                                                if (isset($user_info->mobile) AND !empty($user_info->mobile)) {

                                                     echo "<li><i class='fa fa-2x fa-mobile'></i> Cell Number: ".$user_info->mobile."</li>";

                                                } elseif(isset($active_property->StandardFields->ListAgentCellPhone) && !empty($active_property->StandardFields->ListAgentCellPhone) && ($active_property->StandardFields->ListAgentCellPhone !== "********")) {

                                                    echo "<li><i class='fa fa-2x fa-mobile'></i> Cell Number: ".$active_property->StandardFields->ListAgentCellPhone."</li>";

                                                } elseif(isset($account_info->Phones)){
                                                    foreach($account_info->Phones as $phone){
                                                        if($phone->Name == "Mobile"){

                                                            echo "<li><i class='fa fa-2x fa-mobile'></i> Mobile Number: ".$phone->Number ."</li>";

                                                        }
                                                    }
                                                }

                                                if (isset($user_info->phone) AND !empty($user_info->phone)) {

                                                     echo "<li><i class='fa fa-phone'></i> Phone Number: ".$user_info->phone."</li>";

                                                } elseif(isset($active_property->StandardFields->ListAgentPreferredPhone) && !empty($active_property->StandardFields->ListAgentPreferredPhone) && ($active_property->StandardFields->ListAgentPreferredPhone !== "********")) {

                                                    echo "<li><i class='fa fa-phone'></i> Phone Number: ".$active_property->StandardFields->ListAgentPreferredPhone."</li>";
                                                } elseif(isset($account_info->Phones)){
                                                    foreach($account_info->Phones as $phone){
                                                        if($phone->Name == "Office"){

                                                            echo "<li><i class='fa fa-2x fa-mobile'></i> Phone Number: ".$phone->Number ."</li>";

                                                        }
                                                    }
                                                }

                                                if (isset($user_info->address) AND !empty($user_info->address)) {

                                                     echo "<li><i class='fa fa-map-marker'></i> Address: ".$user_info->address."</li>";

                                                } elseif(isset($account_info->Addresses[0]->Address) && !empty($account_info->Addresses[0]->Address) && ($account_info->Addresses[0]->Address !== "********")) {

                                                    echo "<li><i class='fa fa-map-marker'></i> Address: ".$account_info->Addresses[0]->Address."</li>";
                                                }

                                                if (isset($user_info->email) AND !empty($user_info->email)) {

                                                     echo "<li><i class='fa fa-envelope'></i> Email: ".$user_info->email."</li>";

                                                } elseif(isset($active_property->StandardFields->ListAgentEmail) && !empty($active_property->StandardFields->ListAgentEmail) && ($active_property->StandardFields->ListAgentEmail !== "********")) {

                                                    echo "<li><i class='fa fa-envelope'></i> Email: ".$active_property->StandardFields->ListAgentEmail."</li>";

                                                } elseif(isset($account_info->Addresses[0]->Address) && !empty($account_info->Addresses[0]->Address) && ($account_info->Addresses[0]->Address !== "********")) {

                                                    echo "<li><i class='fa fa-envelope'></i> Address: ".$account_info->Addresses[0]->Address."</li>";
                                                }

                                                if (isset($user_info->license_number) AND !empty($user_info->license_number)) {

                                                     echo "<li><i class='fa fa-certificate'></i> License Number: ".$user_info->license_number."</li>";

                                                } elseif(isset($active_property->StandardFields->ListAgentStateLicense) && !empty($active_property->StandardFields->ListAgentStateLicense) && ($active_property->StandardFields->ListAgentStateLicense !== "********")) {

                                                    echo "<li><i class='fa fa-certificate'></i> License Number: ".$active_property->StandardFields->ListAgentStateLicense."</li>";
                                                }

                                                if (isset($user_info->broker) AND !empty($user_info->broker)) {

                                                     echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$user_info->broker."</li>";

                                                } elseif(isset($active_property->StandardFields->ListOfficeName) && !empty($active_property->StandardFields->ListOfficeName) && ($active_property->StandardFields->ListOfficeName !== "********")) {

                                                    echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$active_property->StandardFields->ListOfficeName."</li>";
                                                    
                                                } elseif(isset($account_info->Office) && !empty($account_info->Office) && ($account_info->Office !== "********")) {

                                                    echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$account_info->Office."</li>";
                                                }

                                                if (isset($user_info->broker_number) AND !empty($user_info->broker_number)) {

                                                     echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$user_info->broker_number."</li>";

                                                } elseif(isset($active_property->StandardFields->ListOfficePhone) && !empty($active_property->StandardFields->ListOfficePhone) && ($active_property->StandardFields->ListOfficePhone !== "********")) {

                                                    echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$active_property->StandardFields->ListOfficePhone."</li>";

                                                } elseif(isset($account_info->Phones)){
                                                    foreach($account_info->Phones as $phone){
                                                        if($phone->Name == "Office"){

                                                            echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$phone->Number ."</li>";

                                                        }
                                                    }
                                                }

                                              if(isset($active_property->StandardFields->ListOfficeFax) && !empty($active_property->StandardFields->ListOfficeFax) && ($active_property->StandardFields->ListOfficeFax !== "********")) {

                                                    echo "<li><i class='fa fa-fax'></i> Fax Number: ".$active_property->StandardFields->ListOfficeFax."</li>";

                                                } elseif(isset($account_info->Phones)){
                                                    foreach($account_info->Phones as $phone){
                                                        if($phone->Name == "Fax"){

                                                            echo "<li><i class='fa fa-fax'></i> Fax Number: ".$phone->Number ."</li>";

                                                        }
                                                    }
                                                }

                                            ?>
                                    </ul>
                    
                                    <?php   }break;
                                }

                                    }  else  {

                                             $mcount = 1;

                                             if($mcount == 1) {?>

                                            <!-- Get data from My Account -->
                                             <h4 class="agent-name">
                                                
                                                <?php if (isset($user_info->first_name) AND !empty($user_info->first_name)) {?>
                                                    <?php echo $user_info->first_name ?>
                                                <?php } else {?>
                                                        <?php echo $account_info->FirstName?>
                                                <?php }?>
                                                 <?php if (isset($user_info->last_name) AND !empty($user_info->last_name)) {?>
                                                    <?php echo $user_info->last_name ?>
                                                <?php } else {?>
                                                        <?php echo  $account_info->LastName?>
                                                <?php }?>   
                                            </h4>
                                            <div class="about-agent">
                                                <?php if (isset($user_info->about_agent) AND !empty($user_info->about_agent)) { ?>

                                                    <?php
                                                        $img = $user_info->about_agent;
                                                        $findImg   = 'data-filename';
                                                        $aboutImg = strpos($img, $findImg);
                                                    ?>
                                                                   
                                                        <?php if($aboutImg !== false) { ?>

                                                            <?php echo $user_info->about_agent; ?>

                                                        <?php } else { ?>  

                                                        <?php echo  $user_info->about_agent = (strlen($user_info->about_agent) > 600) ? substr($user_info->about_agent,0,600).'... <a href="./about">Read more</a>' : $user_info->about_agent; ?>

                                                        <?php } ?>
                                                                
                                                <?php } ?>
                                            </div>
                                            <ul class="agent-detail">
                                                <?php

                                                        if (isset($user_info->mobile) AND !empty($user_info->mobile)) {

                                                            echo "<li><i class='fa fa-2x fa-mobile'></i> Cell Number: ".$user_info->mobile."</li>";

                                                        } elseif(isset($account_info->Phones)){
                                                            foreach($account_info->Phones as $phone){
                                                                if($phone->Name == "Mobile"){

                                                                    echo "<li><i class='fa fa-2x fa-mobile'></i> Cell Number: ".$phone->Number ."</li>";

                                                                }
                                                            }
                                                        }

                                                        if (isset($user_info->phone) AND !empty($user_info->phone)) {

                                                             echo "<li><i class='fa fa-phone'></i> Phone Number: ".$user_info->phone."</li>";

                                                        } elseif(isset($account_info->Phones[0]->Number) && !empty($account_info->Phones[0]->Number) && ($account_info->Phones[0]->Number !== "********")) {

                                                            echo "<li><i class='fa fa-phone'></i> Phone Number: ".$account_info->Phones[0]->Number."</li>";
                                                        }

                                                        if (isset($user_info->address) AND !empty($user_info->address)) {

                                                             echo "<li><i class='fa fa-map-marker'></i> Address: ".$user_info->address."</li>";

                                                        } elseif(isset($account_info->Addresses[0]->Address) && !empty($account_info->Addresses[0]->Address) && ($account_info->Addresses[0]->Address !== "********")) {

                                                            echo "<li><i class='fa fa-map-marker'></i> Address: ".$account_info->Addresses[0]->Address."</li>";
                                                        }

                                                        if (isset($user_info->email) AND !empty($user_info->email)) {

                                                             echo "<li><i class='fa fa-envelope'></i> Email: ".$user_info->email."</li>";

                                                        } elseif(isset($account_info->Addresses[0]->Address) && !empty($account_info->Addresses[0]->Address) && ($account_info->Addresses[0]->Address !== "********")) {

                                                            echo "<li><i class='fa fa-envelope'></i> Email: ".$account_info->Addresses[0]->Address."</li>";
                                                        }

                                                        if (isset($user_info->license_number) AND !empty($user_info->license_number)) {

                                                             echo "<li><i class='fa fa-certificate'></i> License Number: ".$branding->license_number."</li>";
                                                        }

                                                        if (isset($user_info->broker) AND !empty($user_info->broker)) {

                                                             echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$user_info->broker."</li>";

                                                        } elseif(isset($account_info->Office) && !empty($account_info->Office) && ($account_info->Office !== "********")) {

                                                            echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$account_info->Office."</li>";
                                                        }

                                                        if (isset($user_info->broker_number) AND !empty($user_info->broker_number)) {

                                                             echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$user_info->broker_number."</li>";

                                                        } elseif(isset($account_info->Phones)){
                                                            foreach($account_info->Phones as $phone){
                                                                if($phone->Name == "Office"){

                                                                    echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$phone->Number ."</li>";

                                                                }
                                                            }
                                                        }

                                                        if(isset($account_info->Phones)){
                                                            foreach($account_info->Phones as $phone){
                                                                if($phone->Name == "Fax"){

                                                                    echo "<li><i class='fa fa-fax'></i> Fax Number: ".$phone->Number ."</li>";

                                                                }
                                                            }
                                                        }
                                                    ?>
                                            </ul>

                                        <?php
                                        
                                    } break;

                                    }

                        }$icount++;
                    } else {?>

                         <!-- Get data from My Account -->
                         <h4 class="agent-name">
                            
                            <?php if (isset($user_info->first_name) AND !empty($user_info->first_name)) {?>
                                <?php echo $user_info->first_name ?>
                            <?php } else {?>
                                    <?php echo $account_info->FirstName?>
                            <?php }?>
                             <?php if (isset($user_info->last_name) AND !empty($user_info->last_name)) {?>
                                <?php echo $user_info->last_name ?>
                            <?php } else {?>
                                    <?php echo  $account_info->LastName?>
                            <?php }?>   
                        </h4>
                        <div class="about-agent">
                            <?php if (isset($user_info->about_agent) AND !empty($user_info->about_agent)) { ?>

                                <?php
                                    $img = $user_info->about_agent;
                                    $findImg   = 'data-filename';
                                    $aboutImg = strpos($img, $findImg);
                                ?>
                                               
                                    <?php if($aboutImg !== false) { ?>

                                        <?php echo $user_info->about_agent; ?>

                                    <?php } else { ?>  

                                    <?php echo  $user_info->about_agent = (strlen($user_info->about_agent) > 600) ? substr($user_info->about_agent,0,600).'... <a href="./about">Read more</a>' : $user_info->about_agent; ?>

                                    <?php } ?>
                                            
                            <?php } ?>
                        </div>
                        <ul class="agent-detail">
                            <?php
                                    if (isset($user_info->mobile) AND !empty($user_info->mobile)) {

                                        echo "<li><i class='fa fa-2x fa-mobile'></i> Cell Number: ".$user_info->mobile."</li>";

                                    } elseif(isset($account_info->Phones)){
                                        foreach($account_info->Phones as $phone){
                                            if($phone->Name == "Mobile"){

                                                echo "<li><i class='fa fa-2x fa-mobile'></i> Cell Number: ".$phone->Number ."</li>";

                                            }
                                        }
                                    }

                                    if (isset($user_info->phone) AND !empty($user_info->phone)) {

                                         echo "<li><i class='fa fa-phone'></i> Phone Number: ".$user_info->phone."</li>";

                                    } elseif(isset($account_info->Phones[0]->Number) && !empty($account_info->Phones[0]->Number) && ($account_info->Phones[0]->Number !== "********")) {

                                        echo "<li><i class='fa fa-phone'></i> Phone Number: ".$account_info->Phones[0]->Number."</li>";
                                    }

                                    if (isset($user_info->address) AND !empty($user_info->address)) {

                                         echo "<li><i class='fa fa-map-marker'></i> Address: ".$user_info->address."</li>";

                                    } elseif(isset($account_info->Addresses[0]->Address) && !empty($account_info->Addresses[0]->Address) && ($account_info->Addresses[0]->Address !== "********")) {

                                        echo "<li><i class='fa fa-map-marker'></i> Address: ".$account_info->Addresses[0]->Address."</li>";
                                    }

                                    if (isset($user_info->email) AND !empty($user_info->email)) {

                                         echo "<li><i class='fa fa-envelope'></i> Email: ".$user_info->email."</li>";

                                    } elseif(isset($account_info->Addresses[0]->Address) && !empty($account_info->Addresses[0]->Address) && ($account_info->Addresses[0]->Address !== "********")) {

                                        echo "<li><i class='fa fa-envelope'></i> Email: ".$account_info->Addresses[0]->Address."</li>";
                                    }

                                    if (isset($user_info->license_number) AND !empty($user_info->license_number)) {

                                         echo "<li><i class='fa fa-certificate'></i> License Number: ".$user_info->license_number."</li>";
                                    }

                                    if (isset($user_info->broker) AND !empty($user_info->broker)) {

                                         echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$user_info->broker."</li>";

                                    } elseif(isset($account_info->Office) && !empty($account_info->Office) && ($account_info->Office !== "********")) {

                                        echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$account_info->Office."</li>";
                                    }

                                    if (isset($user_info->broker_number) AND !empty($user_info->broker_number)) {

                                         echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$user_info->broker_number."</li>";

                                    } elseif(isset($account_info->Phones)){
                                        foreach($account_info->Phones as $phone){
                                            if($phone->Name == "Office"){

                                                echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$phone->Number ."</li>";

                                            }
                                        }
                                    }

                                    if(isset($account_info->Phones)){
                                        foreach($account_info->Phones as $phone){
                                            if($phone->Name == "Fax"){

                                                echo "<li><i class='fa fa-fax'></i> Fax Number: ".$phone->Number ."</li>";

                                            }
                                        }
                                    }
                                ?>
                        </ul>

                    <?php } ?>
          
    </div>
    
