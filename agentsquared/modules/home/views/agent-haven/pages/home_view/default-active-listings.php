                
        <section class="featured-listing-area">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">
                        <div class="trapezoid"></div>
                        <h2 class="text-center section-title">Featured Properties</h2>
                    </div>
                   <div class="col-md-12">
                        <div class="featured-list">
                         <?php
                        //Fetch Active Property on Property Listing Data
                        if(isset($active_listings) AND !empty($active_listings)) {
                                $itemCount = count($active_listings);
                                if($itemCount > 4) {
                                    $itemCount = 4;
                                }
                                $iCount = 0;
                                $items = 12 / $itemCount;
                                $totalListingCount = 0;
                                foreach($active_listings as $feature) {
                                    if(isset($feature->StandardFields->Photos[0]->Uri300)){
                                        if(!empty($feature->StandardFields->Photos[0]->Uri300)){
                                            if($iCount < $itemCount) {
                                                $totalListingCount++;
                                            }
                                            $iCount++;
                                        }
                                    }
                                }
                                $iCount = 0;
                                $items = 12 / $totalListingCount;

                                if(isset($active_listings) && !empty($active_listings)){

                                    $countFeatured = count($active_listings);

                                    if($countFeatured >= 4){?>
                                        <p class="featured-viewall"><a href="<?php echo base_url()?>find-a-home" target="_blank">View all</a></p>
                                    <?php }
                                }
                                                 
                                foreach($active_listings as $feature) {

                                    if($iCount < $itemCount) { ?>
                                         
                                       <div class="col-md-3 col-sm-3  featured-list-item item-<?=$itemCount?>-<?=$iCount?>-<?=$totalListingCount?>">
                                            <div class="property-image <?php if($feature->StandardFields->PropertyClass == 'Land' || $feature->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                            <?php 
                                                if($is_capture_leads) { 
                                                    if(!isset($_SESSION['customer_id'])) { ?>
                                                        <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?= $feature->StandardFields->ListingKey; ?>" data-propertyType="featured_property">
                                                    <?php } else { ?>
                                                    <?php if(!empty($token_checker)){?>
                                                        <a href="<?= base_url();?>property-details/<?= $feature->StandardFields->ListingKey; ?>">
                                                    <?php }?>
                                                    <?php } if(isset($feature->StandardFields->Photos[0]->Uri300)) { ?>
                                                            <img src="<?=$feature->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                    <?php } 
                                                    else if(isset($feature->Photos->Uri300)) { ?>
                                                            <img src="<?=$feature->Photos->Uri300?>" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                    <?php }
                                                    else { ?>
                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                    <?php } ?>
                                                        </a>
                                            <?php } else { ?>
                                                <?php if(!empty($token_checker)){?>
                                                    <a href="<?= base_url();?>property-details/<?= $feature->StandardFields->ListingKey; ?>">
                                                <?php }?>
                                                <?php if(isset($feature->StandardFields->Photos[0]->Uri300)) { ?>
                                                    <img src="<?=$feature->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                <?php } 
                                                else if(isset($feature->Photos->Uri300)) { ?>
                                                    <img src="<?=$feature->Photos->Uri300?>" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                <?php }
                                                else { ?>
                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                <?php } ?>
                                                </a>
                                            <?php } ?>
                                            </div>
                                            <div class="property-listing-status">
                                                <?php echo $feature->StandardFields->MlsStatus;?>
                                            </div>
                                            <div class="property-listing-price">
                                                <div class="property-listing-type">
                                                    <?=$feature->StandardFields->PropertyClass;?>
                                                </div>
                                                $<?=number_format($feature->StandardFields->CurrentPrice);?>
                                            </div>
                                            <div class="property-quick-icons">
                                                 <ul class="list-inline">
                                                    <?php if(isset($feature->StandardFields->BedsTotal) && !empty($feature->StandardFields->BedsTotal)){
                                                            if(($feature->StandardFields->BedsTotal != "********")){?>
                                                                <li><i class="fa fa-bed"></i> <?=$feature->StandardFields->BedsTotal?> Bed</li>
                                                           <?php } else{?>
                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                           <?php } ?>

                                                     <?php  } else {?>
                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                    <?php }?>

                                                    <?php if(isset($feature->StandardFields->BathsTotal) && !empty($feature->StandardFields->BathsTotal)){
                                                            if(($feature->StandardFields->BathsTotal != "********")){?>
                                                                <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsTotal?> Bath</li>
                                                           <?php } else{?>
                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                           <?php } ?>

                                                     <?php  } else {?>
                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                    <?php }?>

                                                     <?php
                                                        if(!empty($feature->StandardFields->BuildingAreaTotal) && ($feature->StandardFields->BuildingAreaTotal != "0")   && is_numeric($feature->StandardFields->BuildingAreaTotal)) {?>

                                                            <li class="lot-item"><?=number_format($feature->StandardFields->BuildingAreaTotal)?> sqft</li>

                                                    <?php } elseif(!empty($feature->StandardFields->LotSizeArea) && ($feature->StandardFields->LotSizeArea != "0")   && is_numeric($feature->StandardFields->LotSizeArea)) {

                                                           
                                                            if(!empty($feature->StandardFields->LotSizeUnits)  && ($feature->StandardFields->LotSizeUnits === "Acres") ){?>

                                                                <li class="lot-item">lot size area: <?=number_format($feature->StandardFields->LotSizeArea, 2, '.', ',')?></li>

                                                            <?php } else { ?>

                                                                <li class="lot-item">lot size area: <?=number_format($feature->StandardFields->LotSizeArea)?></li>

                                                            <?php } ?> 

                                                      <?php } elseif(!empty($feature->StandardFields->LotSizeSquareFeet) && ($feature->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($feature->StandardFields->LotSizeSquareFeet)) {?>

                                                            <li class="lot-item">lot size area: <?=number_format($feature->StandardFields->LotSizeSquareFeet)?>
                                                            </li> 

                                                     <?php } elseif(!empty($feature->StandardFields->LotSizeAcres) && ($feature->StandardFields->LotSizeAcres != "0")   && is_numeric($feature->StandardFields->LotSizeAcres)) {?>

                                                            <li class="lot-item">lot size area: <?=number_format($feature->StandardFields->LotSizeAcres,2 ,'.',',')?></li>

                                                    <?php } elseif(!empty($feature->StandardFields->LotSizeDimensions) && ($feature->StandardFields->LotSizeDimensions != "0")   && ($feature->StandardFields->LotSizeDimensions != "********")) {?>

                                                            <li class="lot-item">lot size area: <?=$feature->StandardFields->LotSizeDimensions?></li>
                                                    <?php } else {?>
                                                            <li class="lot-item">lot size area: N/A</li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                            <div class="property-listing-description">
                                                <p>
                                                 <?php if($is_capture_leads) {
                                                        if(!isset($_SESSION['customer_id'])) { ?> 
                                                            <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?=$feature->StandardFields->ListingKey;?>" data-propertyType="featured_property">
                                                    <?php } else { ?>
                                                            <a class="listing-link" href="<?= base_url();?>property-details/<?=$feature->StandardFields->ListingKey;?>">
                                                    <?php } ?>
                                                                <b><?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                            </a>
                                                <?php } else { ?>
                                                    <a class="listing-link" href="<?= base_url();?>property-details/<?=$feature->StandardFields->ListingKey;?>">
                                                        <b><?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                    </a>
                                                <?php } ?>
                                                </p>
                                                <p>
                                               <?php
                                                    $mystring = $feature->StandardFields->City;
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);

                                                    if($pos === false)
                                                        echo $feature->StandardFields->City . ", " . $feature->StandardFields->StateOrProvince . " " . $feature->StandardFields->PostalCode;
                                                    else
                                                        echo $feature->StandardFields->PostalCity . ", " . $feature->StandardFields->StateOrProvince . " " . $feature->StandardFields->PostalCode;
                                                ?>
                                                </p>    
                                            </div>
                                       </div>   
                             
                                <?php }
                           
                                $iCount++; 
                              }
                            //If Active Listing Count = 0, add 4 Office Listings
                            if($iCount == 0){
                                    if(isset($office_listings) AND !empty($office_listings)){
                                        $count1 = 0;
                                        foreach($office_listings as $office){
                                            if($count1++ >= 4) break; ?>

                                                <div class="col-md-3 col-sm-3  featured-list-item">
                                                        <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                            <?php if($is_capture_leads) { ?>
                                                                <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                    <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                <?php } 
                                                                        if(isset($office['Photos']['Uri300'])) { ?>
                                                                            <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                <?php   } else { ?>
                                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                <?php   } ?>
                                                                    </a>
                                                            <?php } else { ?>
                                                                <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                                    <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                <?php } else { ?> 
                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                <?php } ?>
                                                                </a>
                                                            <?php } ?> 
                                                        </div>
                                                        <div class="property-listing-status">
                                                             <?php echo $office['StandardFields']['MlsStatus'];?>
                                                        </div>
                                                        <div class="property-listing-price">
                                                            <div class="property-listing-type">
                                                                <?=$office['StandardFields']['PropertyClass'];?>
                                                            </div>
                                                            $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                        </div>
                                                        <div class="property-quick-icons">
                                                            <ul class="list-inline">
                                                               <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                                        if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                            <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                       <?php } else{?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                       <?php } ?>

                                                                 <?php  } else {?>
                                                                        <li><i class="fa fa-bed"></i> N/A</li>
                                                                <?php }?>

                                                                <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                        if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                            <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                       <?php } else{?>
                                                                            <li><i class="icon-toilet"></i> N/A</li>
                                                                       <?php } ?>

                                                                 <?php  } else {?>
                                                                       <li><i class="icon-toilet"></i> N/A</li>
                                                                <?php }?>

                                                               <?php
                                                                if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                                        <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                            <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                                   if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                                        <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                    <?php } else {?>

                                                                        <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                                    <?php }?>

                                                            <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                    <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                             <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                                    <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                                    
                                                             <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                    <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                            <?php } else {?>
                                                                    <li class="lot-item">lot size area: N/A</li>
                                                            <?php } ?>
                                                            </ul>
                                                        </div>
                                                        <div class="property-listing-description">
                                                            <p>
                                                            <?php if($is_capture_leads) { ?>
                                                                <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                    <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                <?php } ?>
                                                                        <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                    </a>
                                                            <?php } else { ?>
                                                                <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                </a>
                                                            <?php } ?>
                                                            </p>
                                                            <p>
                                                           <?php
                                                                $mystring = $office['StandardFields']['City'];
                                                                $findme   = '*';
                                                                $pos = strpos($mystring, $findme);

                                                                if($pos === false)
                                                                    echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                else
                                                                    echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                            ?>
                                                            </p>    
                                                        </div>
                                                </div> 
                                 
                                      <?php  }
                                        $count1++;
                                    } 
                                     
                            // if Active listing count = 1, add 3 Office Listings
                             } elseif($iCount == 1){

                                    if(!empty($office_listings[0]['StandardFields']['ListAgentId']) AND !empty($active_listings[0]->StandardFields->ListAgentId)){
                                                
                                        if($office_listings[0]['StandardFields']['ListAgentId'] == $active_listings[0]->StandardFields->ListAgentId){

                                                $count2 = 0;
                                                //add 3 Nearby Listings if Office listing Id is === Active listing Id
                                                if(isset($nearby_listings) AND !empty($nearby_listings)){

                                                foreach(array_slice($nearby_listings,1) as $nearby){

                                                    if($count2++ >= 3) break; ?>

                                                        <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($nearby->StandardFields->PropertyClass == 'Land' || $nearby->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                            <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?= $nearby->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                                                        <?php } else { ?>
                                                                            <a href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php } if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                    <img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else if(isset($nearby->Photos->Uri300)) { ?>
                                                                                    <img src="<?=$nearby->Photos->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else { ?>
                                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } ?>
                                                                            </a>
                                                                    <?php } else { ?>
                                                                        <a href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                    <img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else if(isset($nearby->Photos->Uri300)) { ?>
                                                                                    <img src="<?=$nearby->Photos->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else { ?>
                                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } ?>
                                                                        </a>
                                                                    <?php } ?>
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $nearby->StandardFields->MlsStatus;?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$nearby->StandardFields->PropertyClass;?>
                                                                </div>
                                                                $<?=number_format($nearby->StandardFields->CurrentPrice);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                    <?php if(isset($nearby->StandardFields->BedsTotal) && !empty($nearby->StandardFields->BedsTotal)){
                                                                        if(($nearby->StandardFields->BedsTotal != "********")){?>
                                                                            <li><i class="fa fa-bed"></i> <?=$nearby->StandardFields->BedsTotal?> Bed</li>
                                                                       <?php } else{?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                       <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($nearby->StandardFields->BathsTotal) && !empty($nearby->StandardFields->BathsTotal)){
                                                                            if(($nearby->StandardFields->BathsTotal != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsTotal?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>


                                                                   <?php
                                                                        if(!empty($nearby->StandardFields->BuildingAreaTotal) && ($nearby->StandardFields->BuildingAreaTotal != "0")   && is_numeric($nearby->StandardFields->BuildingAreaTotal)) {?>

                                                                        <li class="lot-item"><?=number_format($nearby->StandardFields->BuildingAreaTotal)?> sqft</li>

                                                                    <?php } elseif(!empty($nearby->StandardFields->LotSizeArea) && ($nearby->StandardFields->LotSizeArea != "0")   && is_numeric($nearby->StandardFields->LotSizeArea)) {

                                                                           
                                                                        if(!empty($nearby->StandardFields->LotSizeUnits) && ($nearby->StandardFields->LotSizeUnits) === "Acres"){?>

                                                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                                                        <?php } else {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeArea)?> </li>

                                                                        <?php }?>

                                                                    <?php } elseif(!empty($nearby->StandardFields->LotSizeSquareFeet) && ($nearby->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($nearby->StandardFields->LotSizeSquareFeet)) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeSquareFeet)?></li> 


                                                                     <?php } elseif(!empty($nearby->StandardFields->LotSizeAcres) && ($nearby->StandardFields->LotSizeAcres != "0")   && is_numeric($nearby->StandardFields->LotSizeAcres)) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeAcres,2 ,'.',',')?></li>

                                                                    <?php } elseif(!empty($nearby->StandardFields->LotSizeDimensions) && ($nearby->StandardFields->LotSizeDimensions != "0")   && ($nearby->StandardFields->LotSizeDimensions != "********")) {?>

                                                                            <li class="lot-item">lot size area: <?=$nearby->StandardFields->LotSizeDimensions?></li>
                                                                    <?php } else {?>
                                                                            <li class="lot-item">lot size area: N/A</li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>

                                                                 <?php if($is_capture_leads) { ?>
                                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                            <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?= $nearby->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                                                        <?php } else { ?>
                                                                            <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php } ?>
                                                                            <b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                            </a>
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                            <b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                        </a>
                                                                    <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $nearby->StandardFields->City;
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $nearby->StandardFields->City . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
                                                                    else
                                                                        echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div>          
                                         
                                                <?php }
                                            
                                                $count2++;
                                            }
                                        //add 3 Office listings if Office listing != Active listings
                                        } else {

                                            if(isset($office_listings) AND !empty($office_listings)){
                                                        $count2 = 0;
                                                    foreach($office_listings as $office){
                                                        if($count2++ >= 3) break; ?>

                                                            <div class="col-md-3 col-sm-3  featured-list-item">
                                                                <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                    <?php if($is_capture_leads) { ?>
                                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                            <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                        <?php } else { ?>
                                                                            <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                        <?php } 
                                                                                if(isset($office['Photos']['Uri300'])) { ?>
                                                                                    <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                        <?php   } else { ?>
                                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php   } ?>
                                                                            </a>
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                        <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                                            <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else { ?> 
                                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } ?>
                                                                        </a>
                                                                    <?php } ?> 
                                                                </div>
                                                                <div class="property-listing-status">
                                                                     <?php echo $office['StandardFields']['MlsStatus'];?>
                                                                </div>
                                                                <div class="property-listing-price">
                                                                    <div class="property-listing-type">
                                                                        <?=$office['StandardFields']['PropertyClass'];?>
                                                                    </div>
                                                                    $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                                </div>
                                                                <div class="property-quick-icons">
                                                                    <ul class="list-inline">
                                                                        <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                                                if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                                    <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                               <?php } else{?>
                                                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                                                               <?php } ?>

                                                                         <?php  } else {?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                        <?php }?>

                                                                        <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                                if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                                    <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                               <?php } else{?>
                                                                                    <li><i class="icon-toilet"></i> N/A</li>
                                                                               <?php } ?>

                                                                         <?php  } else {?>
                                                                               <li><i class="icon-toilet"></i> N/A</li>
                                                                        <?php }?>

                                                                        <?php
                                                                            if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                                                    <li class="lot-item"> <?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                                        <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                                               if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                                                    <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                                <?php } else {?>

                                                                                    <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                                                <?php }?>

                                                                        <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                                         <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                                                
                                                                         <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                                <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                                        <?php } else {?>
                                                                                <li class="lot-item">lot size area: N/A</li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                                <div class="property-listing-description">
                                                                    <p>
                                                                    <?php if($is_capture_leads) { ?>
                                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                            <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                        <?php } else { ?>
                                                                            <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                        <?php } ?>
                                                                                <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                            </a>
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                        </a>
                                                                    <?php } ?>
                                                                    </p>
                                                                    <p>
                                                                   <?php
                                                                        $mystring = $office['StandardFields']['City'];
                                                                        $findme   = '*';
                                                                        $pos = strpos($mystring, $findme);

                                                                        if($pos === false)
                                                                            echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                        else
                                                                            echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                    ?>
                                                                    </p>    
                                                                </div>
                                                        </div> 

                                                   <?php }
                                                        $count2++;
                                            }

                                        }
                                                
                                    //add 3 office listings if Nearby listings is empty
                                    } else { 

                                        if(isset($office_listings) AND !empty($office_listings)){
                                                $count2 = 0;
                                            foreach($office_listings as $office){
                                                if($count2++ >= 3) break;?>

                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } 
                                                                            if(isset($office['Photos']['Uri300'])) { ?>
                                                                                <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } else { ?>
                                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                                        <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php } else { ?> 
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php } ?>
                                                                    </a>
                                                                <?php } ?> 
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $office['StandardFields']['MlsStatus'];?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$office['StandardFields']['PropertyClass'];?>
                                                                </div>
                                                                $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                    <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                                            if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                           <?php } else{?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                            if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>
                                                                   <?php
                                                                        if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                                                <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                                           if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                            <?php } else {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                                            <?php }?>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                                            
                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                            <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                                    <?php } else {?>
                                                                            <li class="lot-item">lot size area: N/A</li>
                                                                <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } ?>
                                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                        <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $office['StandardFields']['City'];
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                    else
                                                                        echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div> 
                                    
                                               <?php }
                                                $count2++;
                                            } else {

                                                   //add 2 new listings
                                             if(isset($new_listings) AND !empty($new_listings)){
                                                $count2 = 0;
                                            foreach($new_listings as $new_listing){
                                                if($count2++ >= 3) break;?>

                                                 <?php
                                                    if(!empty($new_listing->StandardFields->Photos)){
                                                     $photo = $new_listing->StandardFields->Photos[0]->Uri300 ;
                                                    }
                                                    else if(!empty(isset($new_listing->Photos->Uri300))) {
                                                         $photo = $new_listing->Photos->Uri300 ;
                                                    }
                                                    $defaultPhoto = base_url('assets/images/image-not-available.jpg');
                                                ?>

                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($new_listing->StandardFields->PropertyClass == 'Land' || $new_listing->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $new_listing->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <?php if(!empty($token_checker)){?>
                                                                            <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                        <?php }?>
                                                                    <?php } 
                                                                            if(isset($new_listing->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                <img src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?= $new_listing->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } else if(isset($new_listing->Photos->Uri300)) { ?>
                                                                                <img src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?= $new_listing->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                                                    <?php   }
                                                                    else { ?>
                                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <?php if(!empty($token_checker)){?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                    <?php }?>
                                                                    <?php if(isset($new_listing->StandardFields->Photos[0]->Uri300) || isset($new_listing->Photos->Uri300)) { ?>
                                                                        <img src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?= $new_listing->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                                                    <?php } else { ?> 
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php } ?>
                                                                    </a>
                                                                <?php } ?> 
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $new_listing->StandardFields->MlsStatus;?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$new_listing->StandardFields->PropertyClass;?>
                                                                </div>
                                                                $<?=number_format($new_listing->StandardFields->CurrentPrice);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                    <?php if(isset($new_listing->StandardFields->BedsTotal) && !empty($new_listing->StandardFields->BedsTotal)){
                                                                            if(($new_listing->StandardFields->BedsTotal != "********")){?>
                                                                                <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                                                                           <?php } else{?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($new_listing->StandardFields->BathsTotal) && !empty($new_listing->StandardFields->BathsTotal)){
                                                                            if(($new_listing->StandardFields->BathsTotal != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php
                                                                        if(!empty($new_listing->StandardFields->BuildingAreaTotal) && ($new_listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($new_listing->StandardFields->BuildingAreaTotal)) {?>

                                                                            <li class="lot-item"><?=number_format($new_listing->StandardFields->BuildingAreaTotal)?> sqft </li>

                                                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeArea) && ($new_listing->StandardFields->LotSizeArea != "0")   && is_numeric($new_listing->StandardFields->LotSizeArea)) {
                                                                               
                                                                            if(!empty($new_listing->StandardFields->LotSizeUnits) && ($new_listing->StandardFields->LotSizeUnits) === "Acres"){?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                                                            <?php } else {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeArea)?> </li>

                                                                            <?php }?>

                                                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeSquareFeet) && ($new_listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($new_listing->StandardFields->LotSizeSquareFeet)) {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeSquareFeet)?></li> 

                                                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeAcres) && ($new_listing->StandardFields->LotSizeAcres != "0")   && is_numeric($new_listing->StandardFields->LotSizeAcres)) {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeAcres,2 ,'.',',')?></li>
                                                                                
                                                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeDimensions) && ($new_listing->StandardFields->LotSizeDimensions != "0")   && ($new_listing->StandardFields->LotSizeDimensions != "********")) {?>

                                                                                        <li class="lot-item">lot size area: <?=$new_listing->StandardFields->LotSizeDimensions?></li>
                                                                        <?php } else {?>
                                                                                        <li class="lot-item">lot size area: N/A</li>
                                                                        <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $new_listing->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                    <?php } ?>
                                                                            <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                        <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $new_listing->StandardFields->City;
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                                    else
                                                                        echo $new['StandardFields']['PostalCity'] . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div> 
                                           <?php }
                                                $count2++;

                                                    
                                                }
                                            }

                                            
                                    }
                                            
                                           
                            //add 2 Office Listings
                            } elseif($iCount == 2){

                                if(!empty($office_listings[0]['StandardFields']['ListAgentId']) AND !empty($active_listings[0]->StandardFields->ListAgentId)){
                                        if($office_listings[0]['StandardFields']['ListAgentId'] == $active_listings[0]->StandardFields->ListAgentId){

                                            $count3 = 0;
                                            //add 2 Nearby Listings if Office listing Id is === Active listing Id
                                            if(isset($nearby_listings) AND !empty($nearby_listings)){

                                                foreach(array_slice($nearby_listings,1) as $nearby){

                                                    if($count3++ >= 2) break;?>

                                                         <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($nearby->StandardFields->PropertyClass == 'Land' || $nearby->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                            <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?= $nearby->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                                                        <?php } else { ?>
                                                                            <a href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php } if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                    <img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } elseif(isset($nearby->Photos->Uri300)) { ?>
                                                                            <img src="<?=$nearby->Photos->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else { ?>
                                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } ?>
                                                                            </a>
                                                                    <?php } else { ?>
                                                                        <a href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                    <img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } elseif(isset($nearby->Photos->Uri300)) { ?>
                                                                            <img src="<?=$nearby->Photos->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else { ?>
                                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } ?>
                                                                        </a>
                                                                    <?php } ?>
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $nearby->StandardFields->MlsStatus;?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$nearby->StandardFields->PropertyClass;?>
                                                                </div>
                                                                $<?=number_format($nearby->StandardFields->CurrentPrice);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                    <?php if(isset($nearby->StandardFields->BedsTotal) && !empty($nearby->StandardFields->BedsTotal)){
                                                                            if(($nearby->StandardFields->BedsTotal != "********")){?>
                                                                                <li><i class="fa fa-bed"></i> <?=$nearby->StandardFields->BedsTotal?> Bed</li>
                                                                           <?php } else{?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($nearby->StandardFields->BathsTotal) && !empty($nearby->StandardFields->BathsTotal)){
                                                                            if(($nearby->StandardFields->BathsTotal != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsTotal?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>


                                                                   <?php
                                                                        if(!empty($nearby->StandardFields->BuildingAreaTotal) && ($nearby->StandardFields->BuildingAreaTotal != "0")   && is_numeric($nearby->StandardFields->BuildingAreaTotal)) {?>

                                                                        <li class="lot-item"><?=number_format($nearby->StandardFields->BuildingAreaTotal)?> sqft</li>

                                                                    <?php } elseif(!empty($nearby->StandardFields->LotSizeArea) && ($nearby->StandardFields->LotSizeArea != "0")   && is_numeric($nearby->StandardFields->LotSizeArea)) {

                                                                           
                                                                        if(!empty($nearby->StandardFields->LotSizeUnits) && ($nearby->StandardFields->LotSizeUnits) === "Acres"){?>

                                                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                                                        <?php } else {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeArea)?> </li>

                                                                        <?php }?>

                                                                    <?php } elseif(!empty($nearby->StandardFields->LotSizeSquareFeet) && ($nearby->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($nearby->StandardFields->LotSizeSquareFeet)) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeSquareFeet)?></li> 


                                                                     <?php } elseif(!empty($nearby->StandardFields->LotSizeAcres) && ($nearby->StandardFields->LotSizeAcres != "0")   && is_numeric($nearby->StandardFields->LotSizeAcres)) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeAcres,2 ,'.',',')?></li>

                                                                    <?php } elseif(!empty($nearby->StandardFields->LotSizeDimensions) && ($nearby->StandardFields->LotSizeDimensions != "0")   && ($nearby->StandardFields->LotSizeDimensions != "********")) {?>

                                                                            <li class="lot-item">lot size area: <?=$nearby->StandardFields->LotSizeDimensions?></li>
                                                                    <?php } else {?>
                                                                            <li class="lot-item">lot size area: N/A</li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>

                                                                 <?php if($is_capture_leads) { ?>
                                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                            <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?= $nearby->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                                                        <?php } else { ?>
                                                                            <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php } ?>
                                                                            <b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                            </a>
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                            <b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                        </a>
                                                                    <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $nearby->StandardFields->City;
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $nearby->StandardFields->City . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
                                                                    else
                                                                        echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div>          
                                                <?php }
                                            
                                                $count3++;
                                            }
                                        //add 2 Office Listings if Office Listings != Active listing
                                        } else {

                                                if(isset($office_listings) AND !empty($office_listings)){
                                                            $count3 = 0;
                                                        foreach($office_listings as $office){
                                                            if($count3++ >= 2) break;?>

                                                        <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } 
                                                                            if(isset($office['Photos']['Uri300'])) { ?>
                                                                                <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } else { ?>
                                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                                        <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php } else { ?> 
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php } ?>
                                                                    </a>
                                                                <?php } ?> 
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $office['StandardFields']['MlsStatus'];?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$office['StandardFields']['PropertyClass'];?>
                                                                </div>
                                                                $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                   
                                                                    <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                                            if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                           <?php } else{?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                            if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php
                                                                        if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                                                <li class="lot-item"> <?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                                           if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                            <?php } else {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                                            <?php }?>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                                            
                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                            <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                                    <?php } else {?>
                                                                            <li class="lot-item">lot size area: N/A</li>
                                                            <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } ?>
                                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                        <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $office['StandardFields']['City'];
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                    else
                                                                        echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div> 
                                                
                                                       <?php }
                                                            $count3++;
                                                }
                                        }
                                                
                                // add 2 Office Listings if Nearby Listings is Empty 
                                } else { 

                                        if(isset($office_listings) AND !empty($office_listings)){
                                                $count3 = 0;
                                            foreach($office_listings as $office){
                                                if($count3++ >= 2) break;?>

                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } 
                                                                            if(isset($office['Photos']['Uri300'])) { ?>
                                                                                <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } else { ?>
                                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                                        <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php } else { ?> 
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php } ?>
                                                                    </a>
                                                                <?php } ?> 
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $office['StandardFields']['MlsStatus'];?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$office['StandardFields']['PropertyClass'];?>
                                                                </div>
                                                                $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                    <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                                            if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                           <?php } else{?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                            if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>

                                                                   <?php
                                                                        if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                                                <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                                           if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                            <?php } else {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                                            <?php }?>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                                            
                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                            <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                                    <?php } else {?>
                                                                            <li class="lot-item">lot size area: N/A</li>
                                                            <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } ?>
                                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                        <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $office['StandardFields']['City'];
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                    else
                                                                        echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div> 
                                           <?php }
                                                $count3++;
                                        } else {

                                            //add new listings
                                             if(isset($new_listings) AND !empty($new_listings)){
                                                $count3 = 0;
                                            foreach($new_listings as $new_listing){
                                                if($count3++ >= 2) break;?>

                                                 <?php
                                                        if(!empty($new_listing->StandardFields->Photos)){
                                                         $photo = $new_listing->StandardFields->Photos[0]->Uri300 ;
                                                     }
                                                        $defaultPhoto = base_url('assets/images/image-not-available.jpg');
                                                    ?>

                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($new_listing->StandardFields->PropertyClass == 'Land' || $new_listing->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $new_listing->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                    <?php if(!empty($token_checker)){?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                    <?php }?>
                                                                    <?php } 
                                                                            if(isset($new_listing->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                <img src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?= $new_listing->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } else { ?>
                                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                <?php if(!empty($token_checker)){?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                <?php }?>
                                                                    <?php if(isset($new_listing->StandardFields->Photos[0]->Uri300)) { ?>
                                                                        <img src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?= $new_listing->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                                                    <?php } else { ?> 
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php } ?>
                                                                    </a>
                                                                <?php } ?> 
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $new_listing->StandardFields->MlsStatus;?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$new_listing->StandardFields->PropertyClass;?>
                                                                </div>
                                                                $<?=number_format($new_listing->StandardFields->CurrentPrice);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                     <?php if(isset($new_listing->StandardFields->BedsTotal) && !empty($new_listing->StandardFields->BedsTotal)){
                                                                            if(($new_listing->StandardFields->BedsTotal != "********")){?>
                                                                                <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                                                                           <?php } else{?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($new_listing->StandardFields->BathsTotal) && !empty($new_listing->StandardFields->BathsTotal)){
                                                                            if(($new_listing->StandardFields->BathsTotal != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php
                                                                        if(!empty($new_listing->StandardFields->BuildingAreaTotal) && ($new_listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($new_listing->StandardFields->BuildingAreaTotal)) {?>

                                                                            <li class="lot-item"><?=number_format($new_listing->StandardFields->BuildingAreaTotal)?> sqft </li>

                                                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeArea) && ($new_listing->StandardFields->LotSizeArea != "0")   && is_numeric($new_listing->StandardFields->LotSizeArea)) {
                                                                               
                                                                            if(!empty($new_listing->StandardFields->LotSizeUnits) && ($new_listing->StandardFields->LotSizeUnits) === "Acres"){?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                                                            <?php } else {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeArea)?> </li>

                                                                            <?php }?>

                                                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeSquareFeet) && ($new_listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($new_listing->StandardFields->LotSizeSquareFeet)) {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeSquareFeet)?></li> 

                                                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeAcres) && ($new_listing->StandardFields->LotSizeAcres != "0")   && is_numeric($new_listing->StandardFields->LotSizeAcres)) {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeAcres,2 ,'.',',')?></li>
                                                                                
                                                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeDimensions) && ($new_listing->StandardFields->LotSizeDimensions != "0")   && ($new_listing->StandardFields->LotSizeDimensions != "********")) {?>

                                                                                        <li class="lot-item">lot size area: <?=$new_listing->StandardFields->LotSizeDimensions?></li>
                                                                        <?php } else {?>
                                                                                        <li class="lot-item">lot size area: N/A</li>
                                                                        <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $new_listing->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                    <?php } ?>
                                                                            <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                        <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $new_listing->StandardFields->City;
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                                    else
                                                                        echo $new['StandardFields']['PostalCity'] . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div> 
                                           <?php }
                                                $count3++;

                                                    
                                                }
                                            }
                                }
                                                

                            // add 1 Office Listings
                            } elseif($iCount == 3){

                                if(!empty($office_listings[0]['StandardFields']['ListAgentId']) AND !empty($active_listings[0]->StandardFields->ListAgentId)){
                                        if($office_listings[0]['StandardFields']['ListAgentId'] == $active_listings[0]->StandardFields->ListAgentId){
 
                                                $count4 = 0;
                                                //add 1 Nearby Listings if Office listing Id is === Active listing Id
                                                if(isset($nearby_listings) AND !empty($nearby_listings)){

                                                    foreach(array_slice($nearby_listings,1) as $nearby) {

                                                        if($count4++ >= 1) break;?>

                                                              <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($nearby->StandardFields->PropertyClass == 'Land' || $nearby->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                            <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?= $nearby->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                                                        <?php } else { ?>
                                                                            <a href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php } if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                    <img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } elseif(isset($nearby->Photos->Uri300)) { ?>
                                                                            <img src="<?=$nearby->Photos->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else { ?>
                                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } ?>
                                                                            </a>
                                                                    <?php } else { ?>
                                                                        <a href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                    <img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } elseif(isset($nearby->Photos->Uri300)) { ?>
                                                                            <img src="<?=$nearby->Photos->Uri300;?>" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } else { ?>
                                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                        <?php } ?>
                                                                        </a>
                                                                    <?php } ?>
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $nearby->StandardFields->MlsStatus;?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$nearby->StandardFields->PropertyClass;?>
                                                                </div>
                                                                $<?=number_format($nearby->StandardFields->CurrentPrice);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                <?php if(isset($nearby->StandardFields->BedsTotal) && !empty($nearby->StandardFields->BedsTotal)){
                                                                        if(($nearby->StandardFields->BedsTotal != "********")){?>
                                                                            <li><i class="fa fa-bed"></i> <?=$nearby->StandardFields->BedsTotal?> Bed</li>
                                                                       <?php } else{?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                       <?php } ?>

                                                                 <?php  } else {?>
                                                                        <li><i class="fa fa-bed"></i> N/A</li>
                                                                <?php }?>

                                                                <?php if(isset($nearby->StandardFields->BathsTotal) && !empty($nearby->StandardFields->BathsTotal)){
                                                                        if(($nearby->StandardFields->BathsTotal != "********")){?>
                                                                            <li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsTotal?> Bath</li>
                                                                       <?php } else{?>
                                                                            <li><i class="icon-toilet"></i> N/A</li>
                                                                       <?php } ?>

                                                                 <?php  } else {?>
                                                                       <li><i class="icon-toilet"></i> N/A</li>
                                                                <?php }?>

                                                                <?php
                                                                    if(!empty($nearby->StandardFields->BuildingAreaTotal) && ($nearby->StandardFields->BuildingAreaTotal != "0")   && is_numeric($nearby->StandardFields->BuildingAreaTotal)) {?>

                                                                    <li class="lot-item"> <?=number_format($nearby->StandardFields->BuildingAreaTotal)?> sqft</li>

                                                                <?php } elseif(!empty($nearby->StandardFields->LotSizeArea) && ($nearby->StandardFields->LotSizeArea != "0")   && is_numeric($nearby->StandardFields->LotSizeArea)) {

                                                                       
                                                                    if(!empty($nearby->StandardFields->LotSizeUnits) && ($nearby->StandardFields->LotSizeUnits) === "Acres"){?>

                                                                        <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                                                    <?php } else {?>

                                                                        <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeArea)?> </li>

                                                                    <?php }?>

                                                                <?php } elseif(!empty($nearby->StandardFields->LotSizeSquareFeet) && ($nearby->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($nearby->StandardFields->LotSizeSquareFeet)) {?>

                                                                        <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeSquareFeet)?></li> 


                                                                 <?php } elseif(!empty($nearby->StandardFields->LotSizeAcres) && ($nearby->StandardFields->LotSizeAcres != "0")   && is_numeric($nearby->StandardFields->LotSizeAcres)) {?>

                                                                        <li class="lot-item">lot size area: <?=number_format($nearby->StandardFields->LotSizeAcres,2 ,'.',',')?></li>

                                                                <?php } elseif(!empty($nearby->StandardFields->LotSizeDimensions) && ($nearby->StandardFields->LotSizeDimensions != "0")   && ($nearby->StandardFields->LotSizeDimensions != "********")) {?>

                                                                        <li class="lot-item">lot size area: <?=$nearby->StandardFields->LotSizeDimensions?></li>
                                                                <?php } else {?>
                                                                        <li class="lot-item">lot size area: N/A</li>
                                                                <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>

                                                                 <?php if($is_capture_leads) { ?>
                                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                            <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?= $nearby->StandardFields->ListingKey;?>" data-propertyType="other_property">
                                                                        <?php } else { ?>
                                                                            <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                        <?php } ?>
                                                                            <b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                            </a>
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $nearby->StandardFields->ListingKey;?>">
                                                                            <b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                        </a>
                                                                    <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $nearby->StandardFields->City;
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $nearby->StandardFields->City . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
                                                                    else
                                                                        echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div>          
                                             
                                                   <?php }
                                            
                                                    $count4++;
                                                }
                                        //add  1 Office listings if Office Listing Id !=  Active Listing Id
                                        } else {
                                                        
                                            if(isset($office_listings) AND !empty($office_listings)){

                                                $count4 = 0;         
                                                foreach($office_listings as $office) {

                                                    if($count4++ >= 1) break; ?>

                                                        <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } 
                                                                            if(isset($office['Photos']['Uri300'])) { ?>
                                                                                <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } else { ?>
                                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                                        <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php } else { ?> 
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php } ?>
                                                                    </a>
                                                                <?php } ?> 
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $office['StandardFields']['MlsStatus'];?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$office['StandardFields']['PropertyClass'];?>
                                                                </div>
                                                                $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                   <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                                            if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                           <?php } else{?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                            if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php
                                                                        if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                                                <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                                           if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                            <?php } else {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                                            <?php }?>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                                            
                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                            <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                                    <?php } else {?>
                                                                            <li class="lot-item">lot size area: N/A</li>
                                                            <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } ?>
                                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                        <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $office['StandardFields']['City'];
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                    else
                                                                        echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div> 
                                                   
                                               <?php }
                                                    $count4++;
                                            }
                                        }

                                // add  1 Office listings if Nearby Listings is empty
                                } else { 

                                        if(isset($office_listings) AND !empty($office_listings)){
                                                            $count4 = 0;
                                                            
                                            foreach($office_listings as $office) {

                                                if($count4++ >= 1) break;?>

                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } 
                                                                            if(isset($office['Photos']['Uri300'])) { ?>
                                                                                <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } else { ?>
                                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                                        <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                                    <?php } else { ?> 
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php } ?>
                                                                    </a>
                                                                <?php } ?> 
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $office['StandardFields']['MlsStatus'];?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$office['StandardFields']['PropertyClass'];?>
                                                                </div>
                                                                $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                    <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                                        if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                            <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                       <?php } else{?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                       <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                            if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php
                                                                        if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                                                <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                                           if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                            <?php } else {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                                            <?php }?>

                                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                                            <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                                            
                                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                            <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                                    <?php } else {?>
                                                                            <li class="lot-item">lot size area: N/A</li>
                                                            <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                    <?php } ?>
                                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                                        <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $office['StandardFields']['City'];
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                    else
                                                                        echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div> 
                                               
                                     
                                           <?php }
                                                $count4++;
                                        } else {                    

                                        //add new listings
                                             if(isset($new_listings) AND !empty($new_listings)){
                                                $count4 = 0;
                                            foreach($new_listings as $new_listing){
                                                if($count4++ >= 1) break;?>

                                                 <?php
                                                        if(!empty($new_listing->StandardFields->Photos)){
                                                         $photo = $new_listing->StandardFields->Photos[0]->Uri300 ;
                                                     }
                                                        $defaultPhoto = base_url('assets/images/image-not-available.jpg');
                                                    ?>

                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                            <div class="property-image <?php if($new_listing->StandardFields->PropertyClass == 'Land' || $new_listing->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $new_listing->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                    <?php if(!empty($token_checker)){?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                    <?php }?>
                                                                    <?php } 
                                                                            if(isset($new_listing->StandardFields->Photos[0]->Uri300)) { ?>
                                                                                <img src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?= $new_listing->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } else { ?>
                                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php   } ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                <?php if(!empty($token_checker)){?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                <?php }?>
                                                                    <?php if(isset($new_listing->StandardFields->Photos[0]->Uri300)) { ?>
                                                                        <img src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?= $new_listing->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                                                                    <?php } else { ?> 
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                    <?php } ?>
                                                                    </a>
                                                                <?php } ?> 
                                                            </div>
                                                            <div class="property-listing-status">
                                                                 <?php echo $new_listing->StandardFields->MlsStatus;?>
                                                            </div>
                                                            <div class="property-listing-price">
                                                                <div class="property-listing-type">
                                                                    <?=$new_listing->StandardFields->PropertyClass;?>
                                                                </div>
                                                                $<?=number_format($new_listing->StandardFields->CurrentPrice);?>
                                                            </div>
                                                            <div class="property-quick-icons">
                                                                <ul class="list-inline">
                                                                    <?php if(isset($new_listing->StandardFields->BedsTotal) && !empty($new_listing->StandardFields->BedsTotal)){
                                                                            if(($new_listing->StandardFields->BedsTotal != "********")){?>
                                                                                <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                                                                           <?php } else{?>
                                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php if(isset($new_listing->StandardFields->BathsTotal) && !empty($new_listing->StandardFields->BathsTotal)){
                                                                            if(($new_listing->StandardFields->BathsTotal != "********")){?>
                                                                                <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>
                                                                           <?php } else{?>
                                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                                           <?php } ?>

                                                                     <?php  } else {?>
                                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                                    <?php }?>

                                                                    <?php
                                                                        if(!empty($new_listing->StandardFields->BuildingAreaTotal) && ($new_listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($new_listing->StandardFields->BuildingAreaTotal)) {?>

                                                                            <li class="lot-item"> <?=number_format($new_listing->StandardFields->BuildingAreaTotal)?> sqft </li>

                                                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeArea) && ($new_listing->StandardFields->LotSizeArea != "0")   && is_numeric($new_listing->StandardFields->LotSizeArea)) {
                                                                               
                                                                            if(!empty($new_listing->StandardFields->LotSizeUnits) && ($new_listing->StandardFields->LotSizeUnits) === "Acres"){?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                                                            <?php } else {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeArea)?> </li>

                                                                            <?php }?>

                                                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeSquareFeet) && ($new_listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($new_listing->StandardFields->LotSizeSquareFeet)) {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeSquareFeet)?></li> 

                                                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeAcres) && ($new_listing->StandardFields->LotSizeAcres != "0")   && is_numeric($new_listing->StandardFields->LotSizeAcres)) {?>

                                                                                <li class="lot-item">lot size area: <?=number_format($new_listing->StandardFields->LotSizeAcres,2 ,'.',',')?></li>
                                                                                
                                                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeDimensions) && ($new_listing->StandardFields->LotSizeDimensions != "0")   && ($new_listing->StandardFields->LotSizeDimensions != "********")) {?>

                                                                                        <li class="lot-item">lot size area: <?=$new_listing->StandardFields->LotSizeDimensions?></li>
                                                                        <?php } else {?>
                                                                                        <li class="lot-item">lot size area: N/A</li>
                                                                        <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="property-listing-description">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                                        <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $new_listing->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                    <?php } ?>
                                                                            <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                                                        <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                                <p>
                                                               <?php
                                                                    $mystring = $new_listing->StandardFields->City;
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                    if($pos === false)
                                                                        echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                                    else
                                                                        echo $new['StandardFields']['PostalCity'] . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                                ?>
                                                                </p>    
                                                            </div>
                                                        </div> 
                                           <?php }
                                                $count4++;

                                                    
                                                }
                                            }

                            }
                        }
                        //Add 4 office listings if No Active listings
                         } else {

                            if(isset($office_listings) AND !empty($office_listings)){
                                $count1 = 0;
                                foreach($office_listings as $office){
                                    if($count1++ >= 4) break; ?>

                                         <div class="col-md-3 col-sm-3  featured-list-item">
                                                <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                    <?php if($is_capture_leads) { ?>
                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                            <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                        <?php } else { ?>
                                                            <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                        <?php } 
                                                                if(isset($office['Photos']['Uri300'])) { ?>
                                                                    <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                        <?php   } else { ?>
                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                        <?php   } ?>
                                                            </a>
                                                    <?php } else { ?>
                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                        <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                            <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                                        <?php } else { ?> 
                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } ?>
                                                        </a>
                                                    <?php } ?> 
                                                </div>
                                                <div class="property-listing-status">
                                                     <?php echo $office['StandardFields']['MlsStatus'];?>
                                                </div>
                                                <div class="property-listing-price">
                                                    <div class="property-listing-type">
                                                        <?=$office['StandardFields']['PropertyClass'];?>
                                                    </div>
                                                    $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                </div>
                                                <div class="property-quick-icons">
                                                    <ul class="list-inline">

                                                    <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                            if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                           <?php } else{?>
                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                           <?php } ?>

                                                     <?php  } else {?>
                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                    <?php }?>

                                                    <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                            if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                           <?php } else{?>
                                                                <li><i class="icon-toilet"></i> N/A</li>
                                                           <?php } ?>

                                                     <?php  } else {?>
                                                           <li><i class="icon-toilet"></i> N/A</li>
                                                    <?php }?>

                                                       <?php
                                                            if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                                    <li class="lot-item"> <?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                        <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                               if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                                    <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                <?php } else {?>

                                                                    <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                                <?php }?>

                                                        <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                         <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                                
                                                         <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                        <?php } else {?>
                                                                <li class="lot-item">lot size area: N/A</li>
                                                <?php } ?>
                                                    </ul>
                                                </div>
                                                <div class="property-listing-description">
                                                    <p>
                                                    <?php if($is_capture_leads) { ?>
                                                        <?php if(!isset($_SESSION['customer_id'])) { ?> 
                                                            <a href="#" type="button" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                        <?php } else { ?>
                                                            <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                        <?php } ?>
                                                                <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                            </a>
                                                    <?php } else { ?>
                                                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                        </a>
                                                    <?php } ?>
                                                    </p>
                                                    <p>
                                                   <?php
                                                        $mystring = $office['StandardFields']['City'];
                                                        $findme   = '*';
                                                        $pos = strpos($mystring, $findme);

                                                        if($pos === false)
                                                            echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                        else
                                                            echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                    ?>
                                                    </p>    
                                                </div>
                                            </div> 
                         
                              <?php  }
                                $count1++;
                            } 

                          }?>
                        </div>
                    </div>
                </div>
            </div>
        </section>