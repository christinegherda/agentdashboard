       
        <section class="<?=(isset($is_office_featured)) ? "featured-listing-area" : "new-listing-area"?>">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">

                        <?php if($is_office_featured){?>
                            <div class="trapezoid"></div>
                        <?php }?>

                        <h2 class="<?=(isset($is_office_featured)) ? "section-title" : ""?> other-listing-title text-center"><?php echo $office_title['option_title'];?></h2>
                    </div>
                    <div class="col-md-12">
                        <div class="featured-list">
                          <?php

                                $countOffice = count($office_listings);

                                if($countOffice >= 4){?>
                                    <p class="featured-viewall"><a href="<?php echo base_url()?>home/office_listings" target="_blank">View all</a></p>
                                <?php }

                                $count = 0;


                               foreach( $office_listings as $office){
    
                                    if($count < 4) { ?>
                                <div class="col-md-3 col-sm-3 featured-list-item">
                                    <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                        <?php if($is_capture_leads) { ?>
                                            <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                            <?php } else { ?>
                                                <a href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey']; ?>">
                                            <?php } if(isset($office['Photos']['Uri300'])) { ?>
                                                        <img src="<?=$office['Photos']['Uri300']?>" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } else { ?>
                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } ?>
                                                </a>
                                        <?php } else { ?>
                                            <a href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey']; ?>">
                                            <?php if(isset($office['Photos']['Uri300'])) { ?>
                                                <img src="<?=$office['Photos']['Uri300']?>" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } else { ?>
                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } ?>
                                            </a>
                                        <?php } ?>
                                    </div>
                                    <div class="property-listing-status">
                                        <?php echo $office['StandardFields']['MlsStatus'];?>
                                    </div>
                                    <div class="property-listing-price">
                                        <div class="property-listing-type">
                                            <?=$office['StandardFields']['PropertyClass'];?>
                                        </div>
                                        $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                    </div>
                                    <div class="property-quick-icons">
                                        <ul class="list-inline">

                                            <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                    if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                        <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                   <?php } else{?>
                                                        <li><i class="fa fa-bed"></i> N/A</li>
                                                   <?php } ?>

                                             <?php  } else {?>
                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                            <?php }?>

                                            <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                    if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                        <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                   <?php } else{?>
                                                        <li><i class="icon-toilet"></i> N/A</li>
                                                   <?php } ?>

                                             <?php  } else {?>
                                                   <li><i class="icon-toilet"></i> N/A</li>
                                            <?php }?>

                                           <?php
                                            if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                    <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                        <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                               if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                    <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                <?php } else {?>

                                                    <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeArea'])?> </li>

                                                <?php }?>

                                        <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeSquareFeet'])?></li> 

                                         <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                <li class="lot-item">lot size area: <?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                                
                                         <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                <li class="lot-item">lot size area: <?=$office['StandardFields']['LotSizeDimensions']?></li>
                                        <?php } else {?>
                                                <li class="lot-item">lot size area: N/A</li>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="property-listing-description">
                                        <p>
                                        <?php if($is_capture_leads) { ?>
                                            <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                <a href="#" class="featured_listing listing-link" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $office['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                            <?php } else { ?>
                                                <a href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey']; ?>">
                                            <?php } ?>
                                                    <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                </a>
                                        <?php } else { ?>
                                            <a href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey']; ?>">
                                                <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                            </a>
                                        <?php } ?>
                                        </p>
                                        <p>
                                          <?php
                                                $mystring = $office['StandardFields']['City'];
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);

                                            if($pos === false)
                                                echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                else
                                                echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                            ?>
                                        </p>    
                                    </div>
                                </div>

                        <?php  } $count++;
                            } 
                        ?> 
                        </div>

                    </div>
                </div>
            </div>
        </section>