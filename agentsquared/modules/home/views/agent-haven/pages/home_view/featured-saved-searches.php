

        <section class="save-search-slider <?=(isset($is_saved_search_featured)) ? "featured-listing-area" : "new-listing-area"?>">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">

                        <?php if($is_saved_search_featured){?>
                            <div class="trapezoid"></div>
                        <?php }?>

                        <h2 class="<?=(isset($is_saved_search_featured)) ? "section-title" : ""?> other-listing-title text-center"><span><?php echo $saved_searches_featured[0]['Name'];?></span></h2>
                         <?php 
                            if(isset($saved_searches_featured) AND !empty($saved_searches_featured)){
                                if(count($saved_searches_featured[0]) > 4) {?>
                                <p class="viewall">
                                <?php if($is_capture_leads) { ?>
                                    <?php if(!isset($_SESSION['customer_id'])) { ?>
                                        <a href="#" type="button" data-toggle="modal" data-target="#modalLogin" data-search-url-holder="<?php echo base_url()?>home/saved_searches/<?php echo $saved_searches_featured[0]['Id'];?>">View All</a>
                                    <?php } else { ?>
                                        <a href="<?php echo base_url()?>home/saved_searches/<?php echo $saved_searches_featured[0]['Id'];?>"  target="_blank" >View All</a>
                                    <?php } ?>
                                <?php } else { ?>
                                    <a href="<?php echo base_url()?>home/saved_searches/<?php echo $saved_searches_featured[0]['Id'];?>"  target="_blank" >View All</a>
                                <?php } ?>
                                </p>

                        <?php   }

                            }?>
                    </div>
                    <div class="col-md-12">
                        <div class="featured-list">
                          <?php

                            $count = 0;
                            foreach($saved_searches_featured as $saved){
                                foreach($saved['results']['data'] as $saved_featured){
    
                                if($count < 4) { ?>
                                    <div class="col-md-3 col-sm-3 featured-list-item">
                                        <div class="property-image <?php if($saved_featured['StandardFields']['PropertyClass'] == 'Land' || $saved_featured['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                        <?php if($is_capture_leads) { ?>
                                            <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                <a data-toggle="modal" data-target="#modalLogin" href="javascript:;" class="featured_listing" data-listingId="<?php echo $saved_featured['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                            <?php } else { ?>
                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $saved_featured['StandardFields']['ListingKey']; ?>">
                                            <?php } if(isset($saved_featured['Photos']['Uri300'])) { ?>
                                                        <img src="<?=$saved_featured['Photos']['Uri300']?>" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } else { ?>
                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } ?>
                                                </a>
                                        <?php } else { ?>
                                            <a href="<?= base_url();?>other-property-details/<?= $saved_featured['StandardFields']['ListingKey']; ?>">
                                            <?php if(isset($saved_featured['Photos']['Uri300'])) { ?>
                                                <img src="<?=$saved_featured['Photos']['Uri300']?>" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } else { ?>
                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } ?>
                                            </a>
                                        <?php } ?>
                                        </div>
                                        <div class="property-listing-status">
                                            <?php echo $saved_featured['StandardFields']['MlsStatus'];?>
                                        </div>
                                        <div class="property-listing-price">
                                            <div class="property-listing-type">
                                                <?=$saved_featured['StandardFields']['PropertyClass'];?>
                                            </div>
                                            $<?=number_format($saved_featured['StandardFields']['CurrentPrice']);?>
                                        </div>
                                        <div class="property-quick-icons">
                                            <ul class="list-inline">
                                                <?php if (array_key_exists('BedsTotal', $saved_featured['StandardFields'])): ?>
                                                    <?php if ($saved_featured['StandardFields']['BedsTotal'] && is_numeric($saved_featured['StandardFields']['BedsTotal']) || strpos($saved_featured['StandardFields']['BedsTotal'], "*")): ?>
                                                        <li><i class="fa fa-bed"></i> <?=$saved_featured['StandardFields']['BedsTotal']?> Bed</li>
                                                    <?php elseif (empty($saved_featured['StandardFields']['BedsTotal'])): ?>
                                                        <li><i class="fa fa-bed"></i> N/A</li>
                                                    <?php else: ?>
                                                        <li><i class="fa fa-bed"></i> N/A</li>
                                                    <?php endif ?>
                                                <?php else: ?>
                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                                <?php endif ?>

                                                <?php if (array_key_exists('BathsTotal', $saved_featured['StandardFields'])): ?>
                                                    <?php if ($saved_featured['StandardFields']['BathsTotal'] && is_numeric($saved_featured['StandardFields']['BathsTotal']) ||  strpos($saved_featured['StandardFields']['BathsTotal'], "*")): ?>
                                                        <li><i class="icon-toilet"></i> <?=$saved_featured['StandardFields']['BathsTotal']?> Bath</li>
                                                    <?php elseif (empty($saved_featured['StandardFields']['BathsTotal'])): ?>
                                                        <li><i class="icon-toilet"></i> N/A</li>
                                                    <?php else: ?>
                                                        <li><i class="icon-toilet"></i> N/A</li>
                                                    <?php endif ?>
                                                <?php else: ?>
                                                    <li><i class="icon-toilet"></i> N/A</li>
                                                <?php endif ?>

                                           <?php
                                            if(!empty($saved_featured['StandardFields']['LotSizeArea']) && ($saved_featured['StandardFields']['LotSizeArea'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeArea'])) {

                                                if(!empty($saved_featured['StandardFields']['LotSizeUnits']) && ($saved_featured['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                                    <li class="lot-item">lot size area: <?=number_format($saved_featured['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                <?php } else {?>

                                                    <li class="lot-item">lot size area: <?=number_format($saved_featured['StandardFields']['LotSizeArea'])?> </li>

                                                <?php }?>

                                            <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeSquareFeet']) && ($saved_featured['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeSquareFeet'])) {?>

                                                    <li class="lot-item">lot size area: <?=number_format($saved_featured['StandardFields']['LotSizeSquareFeet'])?></li> 

                                             <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeAcres']) && ($saved_featured['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeAcres'])) {?>

                                                    <li class="lot-item">lot size area: <?=number_format($saved_featured['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                            <?php } else {?>
                                                    <li class="lot-item">lot size area: N/A</li>
                                            <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="property-listing-description">
                                            <p>
                                            <?php if($is_capture_leads) { ?>
                                                <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                    <a data-toggle="modal" data-target="#modalLogin" href="javascript:;" class="featured_listing" data-listingId="<?php echo $saved_featured['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                                <?php } else { ?>
                                                    <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $saved_featured['StandardFields']['ListingKey']; ?>">
                                                <?php } ?>
                                                        <b><?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                    </a>
                                            <?php } else { ?>
                                                <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $saved_featured['StandardFields']['ListingKey']; ?>">
                                                    <b><?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                </a>
                                            <?php } ?>
                                            </p>
                                            <p>
                                              <?php
                                                    $mystring = $saved_featured['StandardFields']['City'];
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);

                                                if($pos === false)
                                                    echo $saved_featured['StandardFields']['City'] . ", " . $saved_featured['StandardFields']['StateOrProvince'] . " " . $saved_featured['StandardFields']['PostalCode'];
                                                    else
                                                    echo $saved_featured['StandardFields']['PostalCity'] . ", " . $saved_featured['StandardFields']['StateOrProvince'] . " " . $saved_featured['StandardFields']['PostalCode'];
                                                ?>
                                            </p>    
                                        </div>
                                    </div>

                        <?php  } $count++;

                             } 
                         }
                        ?> 
                        </div>

                    </div>
                </div>
            </div>
        </section>