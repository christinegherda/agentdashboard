
<section class="page-content" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="customer-dashboard">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#properties" aria-controls="home" role="tab" data-toggle="tab">Favorites </a></li>
                        <li role="presentation"><a href="#searches" aria-controls="home" role="tab" data-toggle="tab">Searches </a></li>
                        <li role="presentation"><a href="#settings" aria-controls="home" role="tab" data-toggle="tab">Profile / Settings</a></li>
                    </ul>

                      <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="properties">
                            <h3>My Saved Favorites ( <?php echo isset($save_properties) ? count($save_properties) : 0; ?> )</h3>
                            <br>    
                            <table class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <td width="10%">Photo</td>
                                        <td>Activity</td>
                                        <td>Date Saved</td>
                                        <td>Address</td>
                                        <td>Details</td>
                                        <!-- <td>Ratings</td> -->
                                        <td align="center">Delete</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($token_checker)){
                                     if( !empty($save_properties) ){
                                        foreach ($save_properties as $property) {?>  
                                            <tr id="<?=$property->cpid?>" >
                                                <td> <a href="<?php echo site_url("other-property-details/".$property->property_id); ?>" target="_blank"> <img src="<?php echo $property->property_photos[0]["StandardFields"]["Photos"][0]["Uri300"]?>" width="100" height="70"> </a></td>
                                                <td><?php if($property->properties_details["MlsStatus"] == "New") : ?><span class="badge">New</span><?php endif;?> Listings </td>
                                                <td><?php echo date("m-d-Y", strtotime($property->date_created))?></td>
                                                <td><?php echo $property->properties_details["UnparsedAddress"]?></td>
                                                <td>$<?php echo number_format($property->properties_details["CurrentPrice"])?> <?php echo str_replace($property->properties_details["UnparsedFirstLineAddress"].',', "", $property->properties_details["UnparsedAddress"])?></td>
                                                <!-- <td><span class="glyphicon glyphicon-star" aria-hidden="true"></span> </td> -->
                                                <td align="center"><a href="<?php echo site_url('home/customer/delete_customer_property/'); ?>" data-id="<?=$property->cpid?>" data-property-id="<?=$property->property_id?>" class="delete-save-property" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                                            </tr>
                                         <?php } ?>
                                      <?php } else {?>
                                            <tr class="center"><td colspan="10"><i>No saved properties records!</i></td></tr>
                                      <?php } ?>

                                    <?php } else {?>
                                             <tr class="center"><td colspan="10"><i>Saved Properties not available!</i></td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="searches">
                            <h3>My Saved Searches ( <?php echo isset($save_searches) ? count($save_searches) : 0; ?> )</h3>
                            <br>
                            <table class="table table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <td width="20%">Searched Links</td>
                                        <td>Date Saved</td>
                                        <td>Details</td>
                                        <td>Email Settings</td>
                                        <td align="center">Delete</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if( !empty($save_searches) ) : 
                                        foreach ($save_searches as $searches) : ?>  
                                            <!-- Get Full Details -->
                                              <?php 
                                                        if(isset($searches->json_search) AND !empty($searches->json_search)) {

                                                            $full_details = json_decode($searches->json_search);

                                                             $search = ($full_details->Search) ? $full_details->Search.", " : "" ;
                                                            
                                                            if(!empty($full_details->bedroom) && !empty($full_details->bathroom)) {

                                                                $bed = ($full_details->bedroom != 1 ) ? "Beds" : "Bed";
                                                                $bath = ($full_details->bathroom != 1) ? "Baths" : "Bath";
                                                                $bedroom = $full_details->bedroom;
                                                                $bathroom = $full_details->bathroom;

                                                            } else {
                                                                $bed = "Bed";
                                                                $bath = "Bath";
                                                                $bedroom =  0;
                                                                $bathroom = 0 ; 
                                                            }

                                                            if(!empty($full_details->min_price) && !empty($full_details->max_price)){
                                                                $min_price = $full_details->min_price ;
                                                                $max_price = $full_details->max_price;

                                                            } else {
                                                                $min_price =  0 ;
                                                                $max_price =  0 ;
                                                            }
                                                            
                                                            //echo ucfirst($search) ."Price (".$min_price."-".$max_price."), ".$bedroom ." ".$bed.", ".$bathroom ." ".$bath;
                                                        } 
                                                ?>

                                            <tr id="search_<?=$searches->csid?>">
                                                <td> <a href="<?php echo base_url()."listings?".$searches->url;?>" target="_blank">Run this search >></a> </td>
                                                <td><?php echo date("m-d-Y", strtotime($searches->date_created))?></td>
                                                <td><?php echo ucfirst($search) ."Price (".$min_price."-".$max_price."), ".$bedroom ." ".$bed.", ".$bathroom ." ".$bath; ?>....</td>
                                                <td>On</td>
                                                <td  align="center"><a href="<?php echo site_url('home/customer/delete_customer_search/'); ?>" data-id="<?=$searches->csid?>" class="delete-save-searches" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <tr class="center"><td colspan="10"><i>no saved searches records!</i></td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="col-md-12"> 
                                <div class="alert-flash display-status alert alert-success" style="display:none;text-align:center;">Email Schedule Updated!</div>
                            </div>  
                            <h3>My Profile </h3>
                            <div class="alert alert-default alert-dismissible" role="alert">
                                <a href="<?php echo site_url("home/customer/edit_profile"); ?>" type="button" class="close" id="customer-edit-profile" ><span aria-hidden="true">Edit Profile</span></a>
                                Account/Email :  <?= (isset($profile->email)) ? $profile->email : $this->session->userdata("customer_email"); ; ?> (<a href="#" class="" id="customer-change-password">change password</a>)
                            </div>
                            <h3>Email Preferences </h3>
                            <div class="alert alert-default alert-dismissible" role="alert">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4>Email Listings Updates: <input type="checkbox" name="email_updates"></h4>
                                    </div>
                                    <div class="col-md-6">
                                        <form class="form-inline">
                                            <div class="form-group">
                                                <label for="Select Frequency">Select Frequency: </label>
                                                <select name="email_preference" class="form-control" style="width: 390px;">f
                                                    <option value="daily">Daily</option>
                                                    <option value="weekly">Weekly</option>
                                                    <option value="monthly">Monthly</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- Saved Listings &amp; Search Updates <input name="saved_search_updates" class="updates-email-settings" value="real_time"  type="radio" <?php echo (isset( $profile->email_settings ) and $profile->email_settings == "real_time") ? "checked" : "" ?>> Real Time <input name="saved_search_updates" class="updates-email-settings" value="daily" type="radio" <?php echo (isset( $profile->email_settings ) and $profile->email_settings == "daily") ? "checked" : "" ?>> Daily <input name="saved_search_updates" class="updates-email-settings" value="off" type="radio" <?php echo (isset( $profile->email_settings ) and $profile->email_settings == "off") ? "checked" : "" ?>> Off  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="change-pass-modal" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
            <div class="show-mess" ></div><br/>
            <form action="<?php echo site_url('home/customer/change_password'); ?>" method="POST" class="customer-change-pass" id="customer-change-pass" >
                <div class="form-group">
                    <label for="exampleInputEmail1">New Password</label>
                    <input type="password" name="new_password" class="form-control" id="new-password" placeholder="New Password">
                </div> 

                <div class="form-group">
                    <label for="exampleInputEmail1">Confirm Password</label>
                    <input type="password" name="cpassword" class="form-control" id="new-password" placeholder="Confirm Password">
                </div>                     
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
       </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="customer-profile-modal" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Profile</h4>
      </div>
      <div class="modal-body">
            <div class="show-mess" ></div><br/>
            <?php $profile_url = (isset($profile->id)) ? "home/customer/update_profile/".$profile->id : "home/customer/update_profile"; ?>
            <form action="<?php echo base_url().$profile_url; ?>" method="post" class="customer-profile" id="customer-profile" >
                <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text" name="profile[first_name]" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" class="form-control" id="fname" placeholder="First Name" required >
                </div> 

                <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text" name="profile[last_name]" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" class="form-control" id="lname" placeholder="Last Name" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Email Address</label>
                    <input type="email" name="profile[email]" value="<?= (isset($profile->email)) ? $profile->email : $this->session->userdata("customer_email"); ; ?>" class="form-control" id="email" placeholder="Email Address" required>
                </div>
                
                <div class="form-group">
                    <label for="exampleInputEmail1">Phone</label>
                    <input type="text" name="profile[phone]" value="<?= (isset($profile->phone)) ? $profile->phone : "" ; ?>" class="form-control" id="phone" placeholder="Phone Number" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Mobile</label>
                    <input type="text" name="profile[mobile]" value="<?= (isset($profile->mobile)) ? $profile->mobile : "" ; ?>" class="form-control" id="mobile" placeholder="Mobile Number" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">State</label>
                    <input type="text" name="profile[state]" value="<?= (isset($profile->state)) ? $profile->state : "" ; ?>" class="form-control" id="state" placeholder="State" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">City</label>
                    <input type="text" name="profile[city]" value="<?= (isset($profile->city)) ? $profile->city : "" ; ?>" class="form-control" id="city" placeholder="City" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <input type="text" name="profile[address]" value="<?= (isset($profile->address)) ? $profile->address : "" ; ?>" class="form-control" id="address" placeholder="Address" required>
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary edit-profile-btn" data-loading-text="Loading...">Submit</button>
      </div>
       </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->