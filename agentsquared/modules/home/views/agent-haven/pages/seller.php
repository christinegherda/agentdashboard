<div class="page-content">
    <section class="property-detail-content">
        <div class="container">
            <div class="row">
            	<!-- Sidebar Area -->
    			<?php $this->load->view($theme."/session/sidebar"); ?>

    			<div class="col-md-9 col-sm-9">
                     <div class="seller-blurb">
                        <?php if(isset($page_data) AND !empty($page_data)){?>

                            <?php echo $page_data['content']; ?>

                        <?php }?>
                    </div>

                    <?php $message = $this->session->flashdata("message"); $success = $this->session->flashdata("success"); ?>
                    <?php if(isset($message)) : ?>
                        <div class="alert <?=(isset($success) AND $success) ? "alert-success" : "alert-danger"?>"> <?php echo $message; ?> </div>
                    <?php endif;?>
                    <div class="property-details">
                        <form action="<?php echo site_url('home/home/save_customer_seller'); ?>" method="POST" class="">
                            <div class="property-information">
                                <h4>Home Information</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Address</label>
                                            <input type="text" name="seller[address]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Zip Code</label>
                                            <input type="text" name="seller[zip_code]" class="form-control" required>
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">No. of Bedrooms</label>
                                            <select name="seller[bedrooms]" id="" class="form-control" required>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">No. of Bathrooms</label>
                                            <select name="seller[bathrooms]" id="" class="form-control" required>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Square Feet</label>
                                            <select name="seller[square_feet]" id="" class="form-control" required>
                                                <option value="Less than 1000">< 1000</option>
                                                <option value="1000 - 2000">1000 - 2000</option>
                                                <option value="2000 - 3000">2000 - 3000</option>
                                                <option value="3000 - 4000">3000 - 4000</option>
                                                <option value="4000 - 5000">4000 - 5000</option>
                                                <option value="5000 - 6000">5000 - 6000</option>
                                                <option value="6000 - 7000">6000 - 7000</option>
                                                <option value="7000 - 8000">7000 - 8000</option>
                                                <option value="8000 - 9000">8000 - 9000</option>
                                                <option value="9000 - 10000">9000 - 10000</option>
                                                <option value="Greater than 10000">> 1000</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="contact-information clearfix">
                                <h4>Contact Information</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" name="seller[fname]"  class="form-control" required>
                                        </div>    
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" name="seller[lname]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" name="seller[email]" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phone</label>
                                            <input type="text" name="seller[phone]" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-default submit-button">Get my home's worth</button>    
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
    		</div>
		</div>
	</section>
</div>