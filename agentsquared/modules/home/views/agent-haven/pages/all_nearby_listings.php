

    <?php
        $this->load->view($theme.'/pages/page_view/advance-search-form');  
    ?> 

    <section class="property-map">
        <div id="map_canvas" style="border: 2px solid #3872ac;"></div>
        <style type="text/css">
            html,
            body,
            #map_canvas  {
                height: 600px;
                width: 100%;
                margin-top: 0px;
                padding: 0px;
            }
        </style>
        
       <script type="text/javascript">

                
                    function initialize() {
                                map = new google.maps.Map(
                                    document.getElementById("map_canvas"), {

                                      center: new google.maps.LatLng(37.387474,-122.05754339999999),
                                       zoom: 13,
                                       scrollwheel: false,
                                       mapTypeId: google.maps.MapTypeId.ROADMAP
                                     });

                                geocoder = new google.maps.Geocoder();

                            <?php 
                                if(isset($nearby_listings_data['data']) && $nearby_listings_data['data']) {
                                    foreach($nearby_listings_data['data'] as $nearby) {
                            ?>
                                    var l1 = "<?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']?>";
                                    var l2 = "<?php echo $nearby['StandardFields']['UnparsedAddress']?>";
                                    var l3 = base_url + "other-property-details/<?php echo $nearby['StandardFields']['ListingKey']?>";
                                    var l4 = "<?= (isset($nearby['Photos']['Uri300'])) ? $nearby['Photos']['Uri300'] : ""; ?>";

                                    var locations = [
                                        [l1, l2, l3, l4]
                                    ];

                                    geocodeAddress(locations);

                             <?php } } ?>

                     }

                            var geocoder;
                            var map;
                            var bounds = new google.maps.LatLngBounds();

                            function geocodeAddress(locations, i) {
                                var title = locations[0][0];
                                var address = locations[0][1];
                                var url = locations[0][2];
                                var img = locations[0][3];

                                geocoder.geocode({
                                  'address': locations[0][1]
                                },

                                function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        var marker = new google.maps.Marker({
                                              icon: 'http://maps.google.com/mapfiles/ms/icons/red.png',
                                              map: map,
                                              position: results[0].geometry.location,
                                              title: title,
                                              animation: google.maps.Animation.DROP,
                                              address: address,
                                              url: url
                                        })
                                        //infoWindow(marker, map, title, address, url, img);

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });

                                        google.maps.event.addListener(marker, 'click', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });
              

                                         google.maps.event.addListener(marker, 'mouseout', function() {
                                            iw.close();
                                        });

                                        bounds.extend(marker.getPosition());
                                        map.fitBounds(bounds);

                                    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) { 

                                        wait = true;

                                        setTimeout(function() {
                                            geocodeAddress(locations, i);
                                        }, 1000 );

                                       // setTimeout("wait = true", 10000);
                                        
                                    } else {
                                        console.log( "Geocode was not successful for the following reason: " + status );
                                        //alert("Geocode was not successful for the following reason: " + status);
                                    }
                                 
                                });
                            }
          

                    google.maps.event.addDomListener(window, "load", initialize);

                    function infoWindow(marker, map, title, address, url,img) {
                          google.maps.event.addListener(marker, 'mouseover', function() {
                            var html = "<div><h3>" + title + "</h3><p>" + address + "<br><img src="+img+" alt="+address+" width='200'  height='150'><br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                            iw = new google.maps.InfoWindow({
                              content: html,
                              maxWidth: 350
                            });
                            iw.open(map, marker);

                          });

                          google.maps.event.addListener(marker, 'mouseout', function() {
                                
                                new google.maps.InfoWindow.close(map, marker);
                            });

                    }

                    
            </script>        
    </section>

    <section class="property-main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">

                    <?php 

                        $this->load->view($theme.'/pages/findahome_view/mortgage-calc');

                        $this->load->view($theme.'/pages/findahome_view/customer-question');
                    ?>
                </div>
                <div class="col-md-9 col-sm-9">
                    <div class="search-listings">
                        <?php  if(!isset($nearby_listings_data['data']) || count($nearby_listings_data['data']) == 0 || empty($nearby_listings_data['data'])) { ?>
                                    <div class="nosold-property">
                                          <i class="fa fa-exclamation-triangle"></i>
                                            <p class="text-center">Nearby Listings not found!</p>
                                    </div>
                        <?php } else {?>

                            <?php if(isset($total_count) && isset($current_page) && isset($total_pages)) {?>
                                     <div class="col-md-12 col-sm-12">
                                     <?php 
                                        $offset = (isset($_GET['offset'])) ? $_GET['offset'] : "" ; 
                                    ?>

                                    <h4><?php echo !empty($total_count) ? number_format($total_count) :"" ?> Properties Found. <!-- Showing  <?php echo !empty($offset) ? number_format($offset) : "" ?> <?php echo !empty($offset) ? "of" : "" ?> <?php echo !empty($total_count) ? number_format($total_count) : ""; ?> --></h4>
                                    </div>
                                <?php } ?>

                               <?php $countF = 0; 
                                if(!empty($nearby_listings_data['data'])) {
                                    foreach($nearby_listings_data['data'] as $nearby) {
                                                $countF++;
                                    ?>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="search-listing-item">
                                                        <div class="search-property-image">
                                                            <?php if($is_capture_leads) { ?>
                                                                <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                                    <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>" data-propertyType="other_property">
                                                                <?php } else { ?>
                                                                    <a href="<?= base_url();?>other-property-details/<?= $nearby["StandardFields"]["ListingKey"]; ?>">
                                                                <?php } if(isset($nearby["Photos"]["Uri300"])) { ?>
                                                                            <img src="<?=$nearby["Photos"]["Uri300"]?>" alt="<?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;">
                                                                <?php } else { ?>
                                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;">
                                                                <?php } ?>
                                                                    </a> 
                                                            <?php } else { ?>
                                                                <a href="<?= base_url();?>other-property-details/<?= $nearby["StandardFields"]["ListingKey"]; ?>">
                                                                <?php if(isset($nearby["Photos"]["Uri300"])) { ?>
                                                                    <img src="<?=$nearby["Photos"]["Uri300"]?>" alt="<?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;">
                                                                <?php } else { ?>
                                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;">
                                                                <?php } ?>
                                                                </a>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="search-listing-type">
                                                            <?php
                                                                if(isset( $nearby["StandardFields"]["PropertyClass"])){?>
                                                                    <?php echo $nearby["StandardFields"]["PropertyClass"];?>
                                                            <?php  } ?> 
                                                        </div>
                                                        <div class="search-listing-title">
                                                            <div class="col-md-9 col-xs-9 padding-0">
                                                                <p>
                                                                <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                                        <a href="javascript:;" class="featured_listing" data-listingId="<?php echo $nearby["StandardFields"]["ListingKey"]; ?>" data-propertyType="other_property" data-propertyUrl="<?php echo base_url();?>home/check_view_property">
                                                                    <?php } else { ?>
                                                                        <a href="<?= base_url();?>other-property-details/<?= $nearby["StandardFields"]["ListingKey"]; ?>">
                                                                    <?php } echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a href="<?= base_url();?>other-property-details/<?= $nearby["StandardFields"]["ListingKey"]; ?>">
                                                                        <?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-3">
                                                                <p>
                                                                    <?php $property_saved = FALSE; if( isset( $_SESSION["save_properties"] ) ) : ?>
                                                                        <?php foreach( $_SESSION["save_properties"] as $key ) : ?>
                                                                            <?php if( $key['property_id'] == $nearby["StandardFields"]["ListingKey"] ) : $property_saved = TRUE; break; endif; ?>
                                                                        <?php endforeach; ?>
                                                                    <?php else : $property_saved = FALSE; endif;?>

                                                                    <a href="javascript:void(0)" data-pro-id="<?=$nearby["StandardFields"]["ListingKey"]?>" class="save-favorate" title="Save Property" >
                                                                        <span id="isSaveFavorate_<?= $nearby["StandardFields"]["ListingKey"]; ?>">
                                                                            <?php echo ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>" ;?>
                                                                        </span>
                                                                    </a>
                                                                </p>
                                                            </div>    <!-- <i class="fa fa-heart-o"></i> -->
                                                        </div>
                                                        
                                                        <p><i class="fa fa-map-marker"></i> 
                                                        <?php
                                                            $mystring = $nearby["StandardFields"]["City"];
                                                            $findme   = '*';
                                                            $pos = strpos($mystring, $findme);

                                                            if($pos === false)
                                                                echo $nearby["StandardFields"]["City"] . " " . $nearby["StandardFields"]["StateOrProvince"] . ", " . $nearby["StandardFields"]["PostalCode"];
                                                            else
                                                                echo $nearby["StandardFields"]["City"] . " " . $nearby["StandardFields"]["StateOrProvince"] . ", " . $nearby["StandardFields"]["PostalCode"];
                                                        ?>
                                                       </i></p>
                                                        <p class="search-property-location"><i class="fa fa-usd"></i> <?=number_format($nearby["StandardFields"]["CurrentPrice"]);?>
                                                        <ul class="list-inline search-property-specs">
                                                            <?php
                                                                    if( $nearby["StandardFields"]["BedsTotal"] && is_numeric($nearby["StandardFields"]["BedsTotal"])) {
                                                                ?>
                                                                <li><i class="fa fa-bed"></i> <?=$nearby["StandardFields"]["BedsTotal"]?> Bed</li>
                                                                <?php
                                                                    }
                                                                    if($nearby["StandardFields"]["BathsTotal"] && is_numeric($nearby["StandardFields"]["BathsTotal"])) {
                                                                ?>
                                                                <li><i class="icon-toilet"></i> <?=$nearby["StandardFields"]["BathsTotal"]?> Bath</li>
                                                                <?php
                                                                    }
                                                                ?>

                                                                <?php
                                                                    if(!empty($nearby['StandardFields']['BuildingAreaTotal']) && ($nearby['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($nearby['StandardFields']['BuildingAreaTotal'])) {?>

                                                                            <li><?=number_format($nearby['StandardFields']['BuildingAreaTotal'])?> sqft</li>

                                                                    <?php } elseif(!empty($nearby['StandardFields']['LotSizeArea']) && ($nearby['StandardFields']['LotSizeArea'] != "0")   && is_numeric($nearby['StandardFields']['LotSizeArea'])) {

                                                                        if(!empty($nearby['StandardFields']['LotSizeUnits']) && ($nearby['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                                                            <li>lot size area: <?=number_format($nearby['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                                        <?php } else {?>

                                                                            <li>lot size area: <?=number_format($nearby['StandardFields']['LotSizeArea'])?> </li>

                                                                        <?php }?>


                                                                    <?php } elseif(!empty($nearby['StandardFields']['LotSizeSquareFeet']) && ($nearby['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($nearby['StandardFields']['LotSizeSquareFeet'])) {?>

                                                                            <li>lot size area: <?=number_format($nearby['StandardFields']['LotSizeSquareFeet'])?></li> 

                                                                     <?php } elseif(!empty($nearby['StandardFields']['LotSizeAcres']) && ($nearby['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($nearby['StandardFields']['LotSizeAcres'])) {?>

                                                                            <li>lot size area: <?=number_format($nearby['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>

                                                                    <?php } elseif(!empty($nearby['StandardFields']['LotSizeDimensions']) && ($nearby['StandardFields']['LotSizeDimensions'] != "0")   && ($nearby['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                                        <li class="lot-item">lot size area: <?=$nearby['StandardFields']['LotSizeDimensions']?></li>
                                                                    <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                <?php
                                            }
                                        }?>
                        </div>

                         <?php
                            if(isset($total_count)) {
                                if($total_count > 25) { ?>
                                    <div class="pagination-area">
                                        <?php echo $pagination;?>
                                        <!-- <nav>
                                            <ul class="pagination">
                                                <?php
                                                    $x = 5;
                                                    $pagerStart = ((round($current_page)%$x === 0) ? round($current_page) : round(($current_page+$x/2)/$x)*$x) - $x;
                                                    $pagerEnd = (round($current_page)%$x === 0) ? round($current_page) : round(($current_page+$x/2)/$x)*$x;

                                                    if($current_page > $x) {
                                                        if(isset($_GET['page']) && $_GET['page']) {
                                                            $getStr = 'page='.($pagerStart);
                                                        } else {
                                                            $getStr = "page=".($pagerStart);
                                                        }
                                                        ?>
                                                        <li ><a href="<?php echo base_url().'home/nearby_listings/'.'?'.$getStr; ?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span> Prev</a></li>
                                                <?php
                                                    }

                                                    for($i = $pagerStart + 1; ($i <= $pagerEnd) && $pagerEnd <= $total_count; $i++) {
                                                        $getStr = "";
                                                        if(isset($_GET['page']) && $_GET['page']) {
                                                            $getStr =  "page=".$i;
                                                        } else {
                                                            $getStr = "page=".$i;
                                                        }
                                                        ?>
                                                        <li <?php if($i == $current_page) { echo 'class="active"'; } ?>>
                                                            <a href="<?php echo base_url().'home/nearby_listings/'.'?'.$getStr; ?>"><?=$i?></a>
                                                        </li>
                                                        <?php
                                                    }

                                                    if(($pagerEnd + 5 ) < $total_count ) {
                                                        if(isset($_GET['page']) && $_GET['page']) {
                                                            $getStr = 'page='.($pagerEnd+1);
                                                        } else {
                                                            $getStr = "page=".($pagerEnd+1);
                                                        }
                                                         ?>
                                                        <li ><a href="<?php echo base_url().'home/nearby_listings/'.'?'.$getStr; ?>" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a></li>
                                                <?php
                                                    }
                                                ?>
                                            </ul>
                                        </nav> -->
                                    </div>
                                <?php
                                }
                            }
                        ?>

                    <?php }?> 
                </div>
            </div>
        </div>
    </section>
