
    <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                   <!-- Sidebar Area -->
                    <?php $this->load->view($theme."/session/sidebar"); ?>
                    <div class="col-md-9 col-sm-9">
                        <div class="page-area">
                            <h2><?php echo ucwords($page_data["title"]); ?></h2>
                            <p><?php echo $page_data["content"]; ?></p> 
                        </div>
                        <?php 
                            if(isset($search_id) AND !empty($search_id)){
                                if(isset($saved_searches['data']) AND !empty($saved_searches['data'])) {
                                    $this->load->view('page_view/page-saved-search');
                                } else { ?>
                                    <div class="col-md-12 col-sm-6 bg-danger text-center" style="padding: 20px;">
                                        <h4>No Saved Search Property Listings Found!</h4>
                                    </div>
                        <?php   }
                            }
                        ?>
                    </div>
                    <?php //$this->load->view('page_view/page-info-contact-mobile-view'); ?>
                </div>
            </div>
        </section>
    </div>
   