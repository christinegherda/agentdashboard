
    <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                     <!-- Sidebar Area -->
                    <?php $this->load->view($theme."/session/sidebar"); ?>
                    <div class="col-md-9 col-sm-9">
                        <?php 
                            if(isset($saved_searches_selected[0]['data']) AND !empty($saved_searches_selected[0]['data'])) {?>
                                
                            <section class="saved-search-area save-search-all">
                                <div class="search-listings">
                                    <?php
                                        foreach($saved_searches_selected as $saved){?>
                                            <div class="col-md-4 col-sm-6">
                                                <div class="search-listing-item">
                                                    <div class="search-property-image">
                                                        <?php if(isset($saved['photo'])) { ?>
                                                                <img src="<?=$saved['photo']?>" alt="<?php echo $saved['data']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } else { ?>
                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved['data']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } ?>   
                                                    </div>
                                                    <div class="saved-search-name">
                                                       <p><?=$saved['Name'];?></p>
                                                       <a href="<?= base_url();?>/home/saved_searches/<?=$saved['Id'];?>" class="btn btn-blue">View Saved Search</a>
                                                    </div>
                                             
                                                </div>
                                            </div>
                                        <?php

                                        }?>
                                </div>
                            </section>
                            
                            <?php } else { ?>
                                <div class="col-md-12 col-sm-6 bg-danger text-center" style="padding: 20px;">
                                    <h4>No Saved Search Found!</h4>
                                </div>
                        <?php  
                            }  ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
   