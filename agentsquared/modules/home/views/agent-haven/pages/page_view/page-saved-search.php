
    <section class="saved-search-area">
        <div class="search-listings">
            <?php
                $count = 0;

                foreach($saved_searches['data'] as $saved){
                
                    if($count < 6) { ?>
                
                    <div class="col-md-4 col-sm-6">
                        <div class="search-listing-item">
                            <div class="search-property-image <?php if($saved['StandardFields']['PropertyClass'] == 'Land' || $saved['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                <?php if($is_capture_leads) { ?>
                                    <?php if(!isset($_SESSION['customer_id'])) { ?>
                                        <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $saved['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                    <?php } else { ?>
                                        <a href="<?= base_url();?>other-property-details/<?= $saved['StandardFields']['ListingKey']; ?>">
                                    <?php } if(isset($saved['Photos']['Uri300'])) { ?>
                                            <img src="<?=$saved['Photos']['Uri300']?>" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                    <?php } else { ?>
                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                    <?php } ?>
                                        </a>
                                <?php } else { ?>
                                    <?php if(isset($saved['Photos']['Uri300'])) { ?>
                                        <a href="<?= base_url();?>other-property-details/<?= $saved['StandardFields']['ListingKey']; ?>"><img src="<?=$saved['Photos']['Uri300']?>" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                    <?php } else { ?>
                                        <a href="<?= base_url();?>other-property-details/<?= $saved['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                    <?php } ?>
                                <?php } ?>    
                            </div>
                            <div class="property-listing-status">
                                <?php echo $saved['StandardFields']['MlsStatus'];?> 
                            </div>
                            <div class="search-listing-type">
                                <?=$saved['StandardFields']['PropertyClass'];?> 
                            </div>
                            <div class="search-listing-title">
                                <div class="col-md-12 col-xs-12">
                                    <p class="search-property-title">
                                     <?php if($is_capture_leads) { ?>
                                        <?php if(!isset($_SESSION['customer_id'])) { ?>
                                            <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $saved['StandardFields']['ListingKey']; ?>" data-propertyType="other_property">
                                        <?php } else { ?>
                                            <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $saved['StandardFields']['ListingKey']; ?>">
                                        <?php } echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>
                                            </a>
                                    <?php } else { ?>
                                        <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $saved['StandardFields']['ListingKey']; ?>">
                                            <?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>
                                        </a>
                                    <?php } ?>
                                    </p>
                                </div>
                            </div>
                            <p class="search-property-location"><i class="fa fa-map-marker"></i> 
                                <?php
                                    $mystring = $saved['StandardFields']['City'];
                                    $findme   = '*';
                                    $pos = strpos($mystring, $findme);

                                if($pos === false)
                                    echo $saved['StandardFields']['City'] . ", " . $saved['StandardFields']['StateOrProvince'] . " " . $saved['StandardFields']['PostalCode'];
                                    else
                                    echo $saved['StandardFields']['PostalCity'] . ", " . $saved['StandardFields']['StateOrProvince'] . " " . $saved['StandardFields']['PostalCode'];
                                ?>
                            </p>
                            <p><i class="fa fa-usd"></i> <?=number_format($saved['StandardFields']['CurrentPrice']);?></p>
                            <ul class="list-inline search-property-specs">
                                <?php
                                   if($saved['StandardFields']['BedsTotal'] && is_numeric($saved['StandardFields']['BedsTotal'])) {?>

                                    <li><i class="fa fa-bed"></i> <?=$saved['StandardFields']['BedsTotal']?> Bed</li>

                                <?php } elseif(empty($saved['StandardFields']['BedsTotal'])){?>

                                    <li><i class="fa fa-bed"></i> N/A</li>

                               <?php }?>

                               <?php
                                   if($saved['StandardFields']['BathsTotal'] && is_numeric($saved['StandardFields']['BathsTotal'])) {?>

                                    <li><i class="icon-toilet"></i> <?=$saved['StandardFields']['BathsTotal']?> Bath</li>

                                <?php } elseif(empty($saved['StandardFields']['BathsTotal'])){?>

                                    <li><i class="icon-toilet"></i> N/A</li>

                               <?php }?>

                             <?php
                            if(!empty($saved['StandardFields']['LotSizeArea']) && ($saved['StandardFields']['LotSizeArea'] != "0")   && is_numeric($saved['StandardFields']['LotSizeArea'])) {

                                if(!empty($saved['StandardFields']['LotSizeUnits']) && ($saved['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                    <li>lot size area: <?=number_format($saved['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                <?php } else {?>

                                    <li>lot size area: <?=number_format($saved['StandardFields']['LotSizeArea'])?> </li>

                                <?php }?>

                            <?php } elseif(!empty($saved['StandardFields']['LotSizeSquareFeet']) && ($saved['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($saved['StandardFields']['LotSizeSquareFeet'])) {?>

                                    <li>lot size area: <?=number_format($saved['StandardFields']['LotSizeSquareFeet'])?></li> 

                             <?php } elseif(!empty($saved['StandardFields']['LotSizeAcres']) && ($saved['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($saved['StandardFields']['LotSizeAcres'])) {?>

                                    <li>lot size area: <?=number_format($saved['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                            <?php }?>
                            </ul>
                        </div>
                    </div>

                <?php  } $count++; 

                }

                if($count < 1) { ?>
                    <div class="col-md-12 col-sm-6 bg-danger text-center" style="padding: 20px;">
                        <h4>No Property Listings!</h4>
                    </div>

                <?php }?>
        </div>

                <?php if(isset($saved_searches['data']) AND !empty($saved_searches['data'])){
                        if (count($saved_searches['data']) > 5){?>
                         <div class="col-md-12 text-center">
                            <div class="col-md-12 line-container">
                                    <p class="submit-line"></p>    
                            </div>
                            <a href="<?php echo base_url()?>home/saved_searches/<?php echo $search_id?>" target="_blank" class="btn btn-default submit-button">View All</a>    
                        </div>
                        
            <?php       }
                    } 
            ?>
    </section>