                
        <div class="filter-tab">
            <div class="property-agent-info">
                <div class="agent-image">
                    <?php if( isset($user_info->agent_photo) AND !empty($user_info->agent_photo)) { ?>

                        <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/photo/<?php echo $user_info->agent_photo ?>" class="img-thumbnail" width="400">
                    <?php } else { ?>

                         <?php if (isset($account_info->Images[0]->Uri) AND !empty($account_info->Images[0]->Uri)){?>
                            <img src="<?=$account_info->Images[0]->Uri?>" class="img-thumbnail" width="400"> 
                         <?php } else { ?>
                            <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/no-profile-img.gif" alt="No Profile Image">
                         <?php } ?>  

                    <?php } ?>  
                </div>
                 <h4 class="agent-name">
                    <?php echo isset($user_info->first_name) ? $user_info->first_name : $account_info->FirstName;?> 
                    <?php echo isset($user_info->last_name) ? $user_info->last_name : $account_info->LastName;?> 
            </h4>
           <p>

               <?php
                //Office Phone
                 if(isset($user_info->phone) AND !empty($user_info->phone)){?>

                    <i class="fa fa-phone-square"></i> Office: <?= $user_info->phone; ?><br/>

                 <?php }else{?>

                  <?php   if(isset($account_info->Phones)) {
                    foreach($account_info->Phones as $phone) {
                        if($phone->Type == 'Office'){
                            echo "<i class='fa fa-phone-square'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                        }
                        
                    }
                }

                 }
                ?>


                <?php
                    //Mobile phone
                 if(isset($user_info->mobile) AND !empty($user_info->mobile)){?>

                    <i class="fa fa-2x fa-mobile"></i> Mobile: <?= $user_info->mobile; ?><br/>

                 <?php }else{?>

                  <?php   
                      if(isset($account_info->Phones )) {
                        foreach($account_info->Phones as $phone) {
                            if($phone->Type == 'Mobile'){
                                echo "<i class='fa fa-2x fa-mobile'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                            }
                            
                        }
                    }
                 }
                ?>


                <?php
                    //FAX
                   if(isset($account_info->Phones )) {
                    foreach($account_info->Phones as $phone) {
                        if($phone->Type == 'Fax'){
                            echo "<i class='fa fa-fax'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                        }
                        
                    }
                }?>
               
           </p>
            <p><i class="fa fa-envelope"></i> : <a href="mailto:<?php echo isset($user_info->email) ? $user_info->email  : $account_info->Emails[0]->Address;?>">Email Me</a></p>
            </div>
        </div>
