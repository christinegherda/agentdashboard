

<div class="filter-tab">
    <div class="property-submit-question">
        <h4 class="text-center">Have a question?</h4>
        <hr>
        <div class="question-mess" ></div>  <br/>                          
        <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" name="name" value="" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="">Email</label>
                <input type="email" name="email" value="" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="">Message</label>
                <textarea name="message" id="" value="" cols="30" rows="5" class="form-control" required></textarea>
            </div>
            <button type="submit" class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
        </form>
    </div>
</div>