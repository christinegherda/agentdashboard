
    <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                         <div class="visible-xs filter-button-container">
                            <button class="btn btn-default submit-button filter-search-button">
                                Show Contact
                            </button>
                        </div>
                        <div class="contact-panel">
                          <?php 
                                $this->load->view($theme.'/pages/page_view/page-agent-info');

                                $this->load->view($theme.'/pages/page_view/page-contact-us');
                           ?> 
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 content-panel">
                        <?php $message = $this->session->flashdata("message");?>
                        <?php if(isset($message)) : ?>
                            <div class="alert alert-success flash-data" role="alert"><?php echo $message; ?></div>
                        <?php endif;?>
                        <h2>Testimonials</h2>
                        <div class="rating-summary">
                            <div class="average-rating" data-average-reviews="<?=$average_reviews['average'];?>"></div>
                            <p>Based on <?=$average_reviews['total']?> reviews.</p>
                            <button class="btn btn-default write-review-button">Write a review</button>
                            <p class="clearfix"></p>
                            <br>
                            <div class="review-area">
                                <form action="<?php echo base_url();?>home/add_testimonial" id="review_form">
                                    <p>Rate my service</p>
                                    <div class="new"></div>
                                    <br>
                                    <div class="form-group">
                                      <input type="hidden" name="user_id" value="<?=$this->session->userdata('user_id');?>">
                                      <input type="hidden" name="customer_id" value="<?=$this->session->userdata('customer_id');?>">
                                      <input type="hidden" name="rating" id="rating">
                                      <input type="text" name="subject" class="form-control" placeholder="Enter your subject here.">
                                    </div>
                                    <div class="form-group">
                                      <textarea name="message" id="testimonial-message" cols="10" rows="10" class="form-control" placeholder="Enter your testimonial here."></textarea>
                                    </div>
                                    <div class="form-group">
                                      <button type="submit" class="btn btn-default" id="submit_review">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-8 col-xs-7 no-padding-left">
                            <h4 class="text-left">Customer Reviews</h4>
                        </div>
                        <div class="col-md-4 col-xs-5 no-padding">
                            <form action="#" id="sort_reviews_form" method="GET">
                                <select name="reviews_order" id="reviews_order" class="form-control">
                                    <option>Sort By</option>
                                    <option value="highest">Highest</option>
                                    <option value="lowest">Lowest</option>
                                    <option value="newest">Newest</option>
                                    <option value="oldest">Oldest</option>
                                </select>
                            </form>
                        </div>
                        <p class="clearfix"></p>
                        <div class="customer-reviews">
                            <?php 
                                if($reviews) {
                                    foreach($reviews as $review) { ?>
                                        <ul>
                                            <li>
                                                <div class="ratefixed" data-rate="<?=$review->rating?>"></div>
                                                <p><strong><?=$review->subject;?></strong></p>
                                                <p>by: <?=$review->first_name." ".$review->last_name;?></p>
                                                <p><?=$review->message;?></p>
                                            </li>
                                        </ul>
                            <?php
                                    }
                                }
                            ?>
                        </div>
                        <ul class="pagination pagination-list">
                            <?php echo $pagination; ?>                     
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    