        <div class="sold-property-container">
        <?php 
            if(isset($nearby_listings['status']) && $nearby_listings['status'] == 200) {
                foreach($nearby_listings['data'] as $nearby) { ?>
                    <div class="other-listing-item">
                        <div class="other-related-items">
                        <?php if(isset( $nearby['Photos']['Uri300'])){?>
                             <a href="<?= base_url(); ?>other-property-details/<?=$nearby['StandardFields']['ListingKey'];?>">
                                <img src="<?php echo $nearby['Photos']['Uri300']; ?>" class="img-responsive">
                            </a>
                        <?php } else {?>
                            <a href="<?= base_url(); ?>other-property-details/<?=$nearby['StandardFields']['ListingKey'];?>">
                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?= $nearby['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                            </a>
                       <?php }?>
                        </div>
                        <div class="other-related-description">
                            <div class="nearby-listing-type">
                               <?php echo $nearby['StandardFields']['PropertyClass']; ?>
                            </div>
                            <p><a href="<?= base_url(); ?>other-property-details/<?=$nearby['StandardFields']['ListingKey'];?>"><?php echo $nearby['StandardFields']["UnparsedFirstLineAddress"]; ?></a></p>
                            <p><i class="fa fa-map-marker"></i><?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?></p>
                            <p><i class="fa fa-usd"></i><?php echo number_format($nearby['StandardFields']['CurrentPrice']); ?></p>
                        </div>
                    </div>
        <?php
                }
            } else { ?>
                <div class="alert alert-info text-center">No Nearby Listings Found!</div>
        <?php
            }
        ?>      
        </div>