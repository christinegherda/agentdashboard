            <?php if(isset($rooms['data']) && !empty($rooms['data'])) { ?>
                <table class="table table-responsive table-striped">
                    <thead>
                        <tr>
                        <?php
                            foreach($rooms['data'][0]['Fields'] as $room) { 
                                foreach($room as $key=>$val) { ?>
                                     <td><strong><?=$key;?></strong></td>
                        <?php
                                }
                            }
                        ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($rooms['data'] as $room) { ?>
                            <tr>
                                <?php
                                    foreach($room['Fields'] as $arr) { 
                                        foreach($arr as $key=>$val) { ?>
                                            <td><?=!empty($val) ? $val : "N/A";?></td>
                                <?php
                                        }
                                    }
                                ?>
                            </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            <?php } ?>