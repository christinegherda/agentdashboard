<?php
    if(!empty($property_standard_info["data"])){
    $detail = $property_standard_info["data"];
?>
<div class="property-title-wrapper">
    <h5 class="property-title"><?php echo $detail["UnparsedFirstLineAddress"]; ?></h5>
    <h5 class="property-address"><?php echo $detail["City"]; ?>, <?php echo $detail["StateOrProvince"]; ?> <?php echo $detail["PostalCode"]; ?> </h5>
    <h5 class="property-bedsbathstotal">
        <ul>
           <?php if(isset($detail["BedsTotal"]) && !empty($detail["BedsTotal"])){
                if(($detail["BedsTotal"] != "********")){?>
                    <li><i class="fa fa-bed"></i> <?=$detail["BedsTotal"]?> Bed</li>
               <?php } else{?>
                    <li><i class="fa fa-bed"></i> N/A</li>
               <?php } ?>

             <?php  } else {?>
                    <li><i class="fa fa-bed"></i> N/A</li>
            <?php }?>

            <?php if(isset($detail["BathsTotal"]) && !empty($detail["BathsTotal"])){
                    if(($detail["BathsTotal"] != "********")){?>
                        <li><i class="icon-toilet"></i> <?=$detail["BathsTotal"]?> Bath</li>
                   <?php } else{?>
                        <li><i class="icon-toilet"></i> N/A</li>
                   <?php } ?>

             <?php  } else {?>
               <li><i class="icon-toilet"></i> N/A</li>
            <?php }?>
            
            <?php
                if(!empty($detail["BuildingAreaTotal"]) && ($detail["BuildingAreaTotal"] != "0")   && is_numeric($detail["BuildingAreaTotal"])) {?>

                    <li class="lot-item"><?=number_format($detail["BuildingAreaTotal"])?> sqft</li>

            <?php } elseif(!empty($detail["LotSizeArea"]) && ($detail["LotSizeArea"] != "0")   && is_numeric($detail["LotSizeArea"])) {

                    if(!empty($detail["LotSizeUnits"])  && ($detail["LotSizeUnits"] === "Acres") ){?>

                        <li class="lot-item"> lot size area: <?=number_format($detail["LotSizeArea"], 2, '.', ',')?></li>

                    <?php } else { ?>

                        <li class="lot-item"> lot size area: <?=number_format($detail["LotSizeArea"])?></li>

                    <?php } ?> 

             <?php } elseif(!empty($detail["LotSizeSquareFeet"]) && ($detail["LotSizeSquareFeet"] != "0")   && is_numeric($detail["LotSizeSquareFeet"])) {?>

                    <li class="lot-item"> lot size area: <?=number_format($detail["LotSizeSquareFeet"])?>
                    </li> 


             <?php } elseif(!empty($detail["LotSizeAcres"]) && ($detail["LotSizeAcres"] != "0")   && is_numeric($detail["LotSizeAcres"])) {?>

                    <li class="lot-item"> lot size area: <?=number_format($detail["LotSizeAcres"],2 ,'.',',')?></li>

            <?php } elseif(!empty($detail["LotSizeDimensions"]) && ($detail["LotSizeDimensions"] != "0")   && ($detail["LotSizeDimensions"] != "********")) {?>

                    <li class="lot-item"> lot size area: <?=$detail["LotSizeDimensions"]?></li>
            <?php } else {?>
                    <li class="lot-item"> lot size area: N/A</li>
            <?php } ?>
        </ul>
    </h5>

    <small><a  href="https://www.google.com/maps/dir//<?php echo urlencode($detail["UnparsedAddress"]); ?>" target="_blank"  style="color: #3498db;">Get Directions <span class="get-directions"><img src="<?=base_url()."assets/img/home/get-direction.png"?>" width="15px" height="15px" alt="Google Maps Direction"></span></a></small>
    <h5 class="property-type"><?php echo $detail["PropertyClass"]; ?>-<span class="p-status"><?php echo $detail["MlsStatus"]; ?></span></h5>
    <h5 class="property-price"><?php echo '$ '.number_format($detail["CurrentPrice"]); ?></h5>
</div>

<div class="property-sold">
    <?php  if($detail["MlsStatus"] == 'Sold' || $detail["MlsStatus"] == 'Closed') { ?>
        <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/SOLD.png" class="img-responsive detail-sold-banner">
     <?php } else { ?>
        <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/SOLD.png" class="img-responsive detail-sold-banner" style="visibility: hidden;">
     <?php } ?>
</div>

<div class="property-detail-wrapper">
    <?php if(strpos($mlsLogo, 'http') !== false) { ?>
        <p class="idx-logo" style="background-image: url('<?php echo $mlsLogo; ?>');"><img src="<?php echo $mlsLogo; ?>" alt="IDX Logo" class="mlsLogo"/></p>
    <?php } else { ?>
        <h4><?php echo $mlsName; ?></h4>
    <?php } ?>
    
    <?php if(isset($detail["ListingId"]) AND strpos($detail["ListingId"],'*') === false){?>
        <p style="margin: 0 0 5px;"><strong>MLS&reg; #:</strong> <small><?php echo $detail["ListingId"]; ?></small></p>
    <?php }?>

    <?php if(isset($detail["ListOfficeName"]) AND strpos($detail["ListOfficeName"],'*') === false){?>
        <p><strong>Courtesy of:</strong> <small><?php echo $detail["ListOfficeName"]; ?></small></p>
    <?php }?>

     <ul class="listing-icons list-inline">
        <!-- Insert Virtual tour -->
        <li>
            <div id="has-vtour"></div>
        </li>
        <li>
            <?php  $property_saved = FALSE; if( isset( $_SESSION["save_properties"] ) ) : ?>
                <?php foreach( $_SESSION["save_properties"] as $key ) : ?>
                    <?php if( $key['property_id'] == $property_id ) : $property_saved = TRUE; break; endif; ?>
                <?php endforeach; ?>
            <?php else : $property_saved = FALSE; endif;?>
            
                <a href="<?php echo site_url('home/home/save_favorates_properties/'.$property_id); ?>" data-pro-id="<?=$property_id?>" class="save-favorate" data-title="Tooltip" data-trigger="hover" title="<?php echo ($property_saved) ? "Saved Property" : "Save Property" ;?>">
                    <span id="isSaveFavorate_<?=$property_id?>"><?php echo ($property_saved) ? "<i class='fa fa-2x fa-heart'></i>" : "<i class='fa fa-2x fa-heart-o'></i>" ;?></span>
                </a>
            
            <p id="isSavedFavorate_<?=$property_id?>"><?php echo ($property_saved) ? "<small>Saved</small>" : "<small>Save</small>" ;?></p>
        </li>
         <li>
            <?php if(isset($_SESSION["customer_id"])) :
                $href =  "<a href='#schedModal'  class='sched_modal' data-toggle='modal' data-title='Tooltip' data-trigger='hover' title='Schedule for Showing'><i class='fa fa-2x fa-calendar'></i></a>";
            else: 
                $href = "<a href='".base_url("signup?type=schedule")."' class='sched_modal' data-title='Tooltip' data-trigger='hover' title='Schedule for Showing' ><i class='fa fa-2x fa-calendar'></i></a>";
            endif;?>
            <p><?php echo $href; ?></p>
            <p><small>Showing</small></p>
        </li>
    </ul>
</div>
<?php }?>
