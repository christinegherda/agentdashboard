	<?php if(isset($virtual_tours[0]['Uri']) AND !empty($virtual_tours[0]['Uri'])){?>
            <p class="virtualTour"><a href="#tourModal" data-toggle="modal" data-title="Tooltip" data-trigger="hover" title="Virtual Tour"><i class="fa fa-2x fa-file-video-o"></i></a></p>
            <p><small>Tour</small></p>
    <?php } ?>

<?php 
    if(isset($virtual_tours) AND !empty($virtual_tours)) { 
        foreach($virtual_tours as $tour) {
            $vtour = (isset($tour['Name'])) ? $tour['Name'] : "";
        } 
    }  
?>
    <!-- Virtual Tour Modal -->
    <div id="tourModal" class="modal fade" data-backdrop="static">
        <div class="modal-dialog modal-virtual-tour">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-tour" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                    <h4><?php echo $vtour; ?></h4>
                </div>
                <div class="virtual-tours-modal text-center">
                <?php 
                    if(isset($virtual_tours) AND !empty($virtual_tours)) {
                        foreach($virtual_tours as $tour) { 
                            $virtual_tour = $tour['Uri'];
                            $findDropbox   = 'dropbox';
                            $virtualTourdropbox = strpos($virtual_tour, $findDropbox);
                            
                            //For the Dropbox Link for Virtual Tour 
                            if ($virtualTourdropbox !== false) { ?>
                                <div class="virtual-dropbox">
                                    <h2 class="alert alert-info"> Virtual Tour preview is not available!</h2><br>
                                    <p>Please click the link below for the preview</p>
                                    <p class="virtual-drop-link"><strong><a href="<?php echo $tour['Uri']; ?>" target="_blank">Virtual Tour</a></strong></p> 
                                </div> 

                <?php       } else { 
                                // For the Youtube video to work in IFRAME
                                $string     = $tour['Uri'];
                                $search     = '#(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*#x';
                                $replace    = 'http://www.youtube.com/embed/$2';
                                $url        = preg_replace($search,$replace,$string);

                                //Identify if there is a string "youtu.be"
                                $mystring = $tour['Uri'];
                                $findme   = 'youtube';
                                $pos = strstr($mystring, $findme);

                                if($pos !== false) { ?>
                                    <iframe class="virtualTours-iframe" width="100%" height="750px"  data-src="<?php echo $url;?>" src="about:blank" frameborder="0"></iframe>
                <?php           } else { ?>
                                    <iframe class="virtualTours-iframe" width="100%" height="750px" data-src="<?php echo $tour['Uri']; ?>" src="about:blank" frameborder="0"></iframe>
                <?php           }
                            }
                        }  
                    } 
                ?>
                </div>
            </div>
        </div>
    </div>