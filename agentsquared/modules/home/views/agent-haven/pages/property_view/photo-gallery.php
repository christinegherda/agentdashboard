<?php

if(!empty($photos)) {
    $photo = json_decode($photos);
    if(!isset($photo->response)) {
        $photoD = $photo->data;

        $photoData = $photoD[0]->StandardFields->Photos;

        if(!empty($photoData)) { ?>
            <div id="photosync1" class="owl-carousel photo-preview">
                <?php for ($i = 0; $i < count($photoData); $i++) { ?>
                    <div class="item"  style="background-image: url('<?php echo $photoData[$i]->Uri640; ?>');">
                        <img src="<?php echo $photoData[$i]->Uri640; ?>" alt="" class=""> 
                    </div>
                <?php } ?>
            </div>
            <div id="photosync2" class="owl-carousel owl-photo-thumbnail">
                <?php for ($i = 0; $i < count($photoData); $i++) { ?>
                    <div class="item"  style="background-image: url('<?php echo $photoData[$i]->Uri640; ?>');">
                        <img src="<?php echo $photoData[$i]->Uri640; ?>" alt="" class=""> 
                    </div>
                <?php } ?>
            </div>
    <?php
        } elseif(isset($photoData[0]->Uri640)) { ?>
            <div class="no-image-available">
                <img src="<?php echo $photoData[0]->Uri640; ?>" alt="" class="">
            </div>
    <?php
        } else { ?>
            <div class="no-image-available">
                <img src="<?=base_url()."assets/images/image-not-available.jpg"?>" alt="" class="">
            </div>
    <?php
        }
    } else { ?>
       <!-- <div id="photosync1" class="owl-carousel photo-preview">
            <div class="item"  style="background-image: url('<?=base_url()."assets/images/image-not-available.jpg"?>');">
                <img src="<?=base_url()."assets/images/image-not-available.jpg"?>" alt="" class="">
            </div>
        </div>
        <div id="photosync2" class="owl-carousel owl-photo-thumbnail">
            <div class="item"  style="background-image: url('<?=base_url()."assets/images/image-not-available.jpg"?>');">
                <img src="<?=base_url()."assets/images/image-not-available.jpg"?>" alt="" class="">
            </div>
        </div> -->
        <p class="text-center">The resource is not available at the current API key's service level.</p>
<?php
    }
}else{?>
    <p class="text-center">The resource is not available at the current API key's service level.</p>
<?php }?>
