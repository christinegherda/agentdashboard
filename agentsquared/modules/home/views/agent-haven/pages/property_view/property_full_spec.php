<div class="row">
<?php 
if(!empty($property_standard_info["data"])){ ?>
    <!-- Schedule for Showing Modal -->
    <div id="schedModal" class="modal fade" data-backdrop="static">
        <div class="modal-dialog modal-virtual-tour">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                    <h4>Schedule For Showing</h4>
                </div>
                <!-- <?php echo $property_standard_info["data"]["UnparsedFirstLineAddress"]; ?> -->
                <div class="description sched-modal-section">
                    <div id="resultInsert" class="alert alert-success" style="display:none"></div>
                    <form action="<?php echo base_url("home/home/save_schedule_for_showing"); ?>" method="POST" class="sched_showing" >
                        <input type="hidden" name="property_id" value="<?php echo $property_standard_info["property_id"]; ?>"/>
                        <input type="hidden" name="property_name" value="<?php echo $property_standard_info["data"]["UnparsedFirstLineAddress"]; ?>"/>
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <label>Date schedule</label>
                                <!-- <input type="text" data-provide="datepicker" name="date_scheduled" value="" class="form-control date_schedule" id="startdatepickermodal" placeholder="Date schedule" /><br /> -->
                                <input type="text" name="date_scheduled" value="" class="form-control" id="datepickerfromM" placeholder="Date schedule"> <br />
                            </div>
                        </div>
                        <textarea name="sched_for_showing" class="form-control" rows="10" > Checkout listings at <?php echo $property_standard_info["data"]["UnparsedFirstLineAddress"]; ?></textarea>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-default submit-button" id="sbt-showing" >Submit</button>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs detail-tabs" role="tablist">
    <li class="view-full-details active" role="presentation" class="active"><a href="#property-detail" aria-controls="home" role="tab" data-toggle="tab">Property Details</a></li>
    <li role="presentation"><a href="#location" role="tab" data-toggle="tab">Location <br>&nbsp;</a></li>
    <li class="view-full-spec" role="presentation"><a href="#fullspec" role="tab" data-toggle="tab">Full Specifications</a></li>
    <li role="presentation"><a href="#school" role="tab" data-toggle="tab">Nearby <br>&nbsp;</a></li>
</ul>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="getDirection">
        <div class="description property-detail-sections">
            <h4>Get Direction</h4>
            <div style='overflow:hidden;height:450px;width:100%;'>
                <input id="origin-input" class="controls" value="" type="text" placeholder="Enter an origin location">
                <input id="destination-input" value="<?php echo $property_standard_info["data"]["UnparsedFirstLineAddress"]; ?>" class="controls" type="text" placeholder="Enter a destination location">
                <div id="mode-selector" class="controls" style="display:none">
                    <input type="radio" name="type" id="changemode-walking" checked="checked">
                    <label for="changemode-walking">Walking</label>

                    <input type="radio" name="type" id="changemode-transit">
                    <label for="changemode-transit">Transit</label>

                    <input type="radio" name="type" id="changemode-driving">
                    <label for="changemode-driving">Driving</label>
                </div>

                <div id="map"></div>


                 <div id='gmap_directions' style='height:450px;width:100%;'></div>
                 <style>
                #gmap_directions img {
                         max-width: none!important;
                         background: none!important
                    }

                .controls {
                        margin-top: 10px;
                        border: 1px solid transparent;
                        border-radius: 2px 0 0 2px;
                        box-sizing: border-box;
                        -moz-box-sizing: border-box;
                        /*height: 32px;*/
                        outline: none;
                        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                      }

                      #origin-input,
                      #destination-input {
                        background-color: #fff;
                        font-family: Roboto;
                        font-size: 15px;
                        font-weight: 300;
                        margin-left: 12px;
                        padding: 0 11px 0 13px;
                        text-overflow: ellipsis;
                        width: 200px;
                      }

                      #origin-input:focus,
                      #destination-input:focus {
                        border-color: #4d90fe;
                      }

                      #mode-selector {
                        color: #fff;
                        background-color: #4d90fe;
                        margin-left: 12px;
                        padding: 5px 11px 0px 11px;
                      }

                      #mode-selector label {
                        font-family: Roboto;
                        font-size: 13px;
                        font-weight: 300;
                        padding: 5px;
                      }
                </style>
            </div>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane fade in active" id="property-detail">
        <div class="description property-detail-sections">
            <h4>Description</h4>
            <p><?php echo $property_standard_info['data']['PublicRemarks']; ?></p>
        </div>
        <div class="specifications property-detail-sections">
            <h4>Specifications</h4>
            <div class="row">
                <?php
                    /*for($i=0; $i<3; $i++) { ?>
                        <div class="col-md-4 col-sm-12">
                            <ul class="property-detail-list">
                            <?php if(!empty($property_full_spec['groups'])){?>
                                <br/><h4 class='panel-title'><strong><?php echo $property_full_spec['groups'][$i]['group']; ?></strong></h4><br/>
                                <?php 
                                    $count=0;
                                    foreach($property_full_spec['groups'][$i]['fields'] as $fields) { 
                                        if(!empty($fields['Value'])){
                                            if($fields['Label'] && $fields['Value']) {
                                                if($count<5) { ?>
                                                    <?php if( isset($fields['Label']) && isset($fields['Value']) ) { ?>
                                                        <li>
                                                            <label><?php echo $fields['Label']; ?>: </label>
                                                            <!--<span><?php echo (!empty($fields['Value'])) ? $fields['Value'] : ""; ?></span>-->                                                           
                                                            <span>
                                                                <?php if( !is_array($fields['Value']) ) { ?>
                                                                    <span><?php echo (!empty($fields['Value'])) ? $fields['Value'] : ""; ?></span>
                                                                <?php } else { ?>
                                                                    <?php foreach( $fields['Value'] as $keyO => $valO ) { ?>
                                                                        <span><?php echo (!empty($keyO)) ? $keyO : ""; ?></span>
                                                                        <span><?php echo (!empty($valO)) ? $valO : ""; ?></span>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </span>
                                                        </li>
                                                    <?php } ?>
                                    <?php
                                                }
                                                $count++;
                                            }
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                <?php
                    }*/
                ?>
                <?php 
                    if(!empty($property_full_spec['data'])) {
                        $counter=1;
                        foreach ($property_full_spec['data']['Main'] as $key => $value) { ?>
                            <?php if($counter <= 3) { ?>
                                <div class="col-md-4 col-sm-12">
                                <?php foreach ($value as $key2 => $value2) {
                                     $count=0;
                                     ?>
                                      
                                     <ul class="property-detail-list">
                                            <br/><h4 class='panel-title'><strong><?php echo $key2; ?></strong></h4><br/>
                                            <?php
                                                foreach ($value2 as $key3 => $value3) {
                                                    foreach ($value3 as $key4 => $value4) {
                                                        if($count<5) { ?>
                                                            <li>
                                                                <label><?php echo $key4; ?>: </label>
                                                                <span><?php echo $value4; ?></span>
                                                            </li>

                                                        <?php } ?>
                                                <?php $count++;
                                                    }
                                                }
                                            ?>
                                        </ul>  
                                <?php } $counter++ ?>  
                                 </div> 
                            <?php } ?>
                       <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="view-full" role="presentation"><a href="#fullspec" role="tab" data-toggle="tab">View Full Specifications</a></div>
    </div>

    <div role="tabpanel" class="tab-pane" id="location">
        <div class="location-info property-detail-sections">
            <h4>Map View</h4>
            <div style='overflow:hidden;height:450px;width:100%;'>
                <div id='gmap_canvas' style='height:450px;width:100%;'></div>
                <style>
                    #gmap_canvas img {
                        max-width: none!important;
                        background: none!important
                    }
                </style>
            </div>
            <div class="streetview">
                 <h4>Street View</h4>
                 <div style='overflow:hidden;height:450px;width:100%;'>
                     <div id='gmap_canvas_streetview' style='height:450px;width:100%;'></div>
                </div>
            </div>
        </div>
    </div>  

    <div role="tabpanel" class="tab-pane" id="school">
        <div class="location-info property-detail-sections">
            <h4>Nearby</h4>
            <div style='overflow:hidden;height:520px;width:100%;'>
                <ul class="radio-sample controls" id="mode-selector" >
                    <li>
                        <input type="radio" id="changemode-school" name="type" checked>
                        <label for="changemode-school"><i class="fa fa-university"></i><span> School </span></label>
                        <div class="check"></div>
                    </li>
                    <li>
                        <input type="radio" id="changemode-restaurants" name="type">
                        <label for="changemode-restaurants"><i class="fa fa-cutlery"></i><span> Restaurant </span></label>
                        <div class="check"><div class="inside"></div></div>
                    </li>
                    <li>
                        <input type="radio" id="changemode-store" name="type">
                        <label for="changemode-store"><i class="fa fa-shopping-cart"></i><span> Store </span></label>
                        <div class="check"><div class="inside"></div></div>
                    </li>
                </ul>
                <div id="school_map" style="height:450px;width:100%;"></div>
                <div id="restaurant_map" style="height:450px;width:100%;"></div>
                <div id="store_map" style="height:450px;width:100%;"></div>
                    <style>
                        #school_map,#restaurant_map,#store_map img {
                             max-width: none!important;
                             background: none!important
                        }

                        .controls {
                            margin-top: 10px;
                            border: 1px solid transparent;
                            border-radius: 2px 0 0 2px;
                            box-sizing: border-box;
                            -moz-box-sizing: border-box;
                            /*height: 45px;*/
                            outline: none;
                            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                        }

                        #mode-selector {
                            color: #fff;
                            background-color: #4d90fe;
                            margin-left: 0px;
                            padding: 5px;
                        }

                        #mode-selector label {
                            font-family: sans-serif;
                            font-size: 14px;
                            font-weight: 400;
                            z-index: 1;
                        }

                    </style>
                </div>
            <script type='text/javascript'>
              var school_map;
              var infowindow;

              function initSchoolMap() {

                var modes = "school";
                var school_location = {lat: <?php echo $property_standard_info["data"]["Latitude"]; ?>, lng: <?php echo $property_standard_info["data"]["Longitude"]; ?>};
                school_map = new google.maps.Map(document.getElementById('school_map'), {
                  center: school_location,
                  zoom: 5,
                  scrollwheel: false,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                marker =  new google.maps.Marker({
                    position: school_location,
                    map: school_map,
                    title: "<?php echo $property_standard_info['data']['UnparsedAddress']; ?>"
                });

                setupClickListener('changemode-school', "school", "school_location");
                setupClickListener('changemode-restaurants', "restaurant", "school_location");
                setupClickListener('changemode-store', "store", "school_location");

                infowindow = new google.maps.InfoWindow();
                var service = new google.maps.places.PlacesService(school_map);
                
                if( modes == "school")
                {
                    service.nearbySearch({
                      location: school_location,
                      radius: 1500,
                      type: modes
                    }, callback);
                }

                function setupClickListener(id, mode,loc) {
                   
                    
                    var radioButton = document.getElementById(id);

                    radioButton.addEventListener('click', function() { 
                        modes = mode;

                        if( mode == "restaurant" )
                        {
                            mode_restaurant( mode );
                        }

                        if( mode == "school" )
                        {
                            mode_school( mode );
                        }

                        if( mode == "store" )
                        {
                            mode_store( mode );
                        }

                    });

                }


              }

            function mode_school( modes, school_map )
            {
                document.getElementById('restaurant_map').style.display = 'none';
                document.getElementById('store_map').style.display = 'none';

                var sc_location = {lat: <?php echo $property_standard_info["data"]["Latitude"]; ?>, lng: <?php echo $property_standard_info["data"]["Longitude"]; ?>};
                
                document.getElementById('school_map').style.display = 'block';
                    school_map2 = new google.maps.Map(document.getElementById('school_map'), {
                        center: sc_location,
                            zoom: 12,
                            scrollwheel: false,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                    marker =  new google.maps.Marker({
                        position: sc_location,
                        map: school_map2,
                        title: "<?php echo $property_standard_info['data']['UnparsedAddress']; ?>"
                    });

                    infowindow = new google.maps.InfoWindow();

                    var service = new google.maps.places.PlacesService(school_map2);
                    
                    service.nearbySearch({
                        location: sc_location,
                        radius: 1500,
                        type: modes
                    }, callback_school);
              }

            function mode_store( modes )
            {
                document.getElementById('restaurant_map').style.display = 'none';
                document.getElementById('school_map').style.display = 'none';
                //marker.setVisible(false);
                var st_location = {lat: <?php echo $property_standard_info["data"]["Latitude"]; ?>, lng: <?php echo $property_standard_info["data"]["Longitude"]; ?>};
                    
                document.getElementById('store_map').style.display = 'block';
                store_map = new google.maps.Map(document.getElementById('store_map'), {
                    center: st_location,
                    zoom: 12,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                marker =  new google.maps.Marker({
                    position: st_location,
                    map: store_map,
                    title: "<?php echo $property_standard_info['data']['UnparsedAddress']; ?>"
                });

                infowindow = new google.maps.InfoWindow();

                var service = new google.maps.places.PlacesService(store_map);
                
                service.nearbySearch({
                    location: st_location,
                    radius: 1500,
                    type: modes
                },  callback_store);
            }

            function mode_restaurant( modes, school_map )
            {
                document.getElementById('school_map').style.display = 'none';
                document.getElementById('store_map').style.display = 'none';
                //marker.setVisible(false);
                var res_location = {lat: <?php echo $property_standard_info["data"]["Latitude"]; ?>, lng: <?php echo $property_standard_info["data"]["Longitude"]; ?>};
                
                document.getElementById('restaurant_map').style.display = 'block';
                restaurant_map = new google.maps.Map(document.getElementById('restaurant_map'), {
                          center: res_location,
                          zoom: 12,
                          scrollwheel: false,
                          mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                marker =  new google.maps.Marker({
                    position: res_location,
                    map: restaurant_map,
                    title: "<?php echo $property_standard_info['data']['UnparsedAddress']; ?>"
                });

                infowindow = new google.maps.InfoWindow();

                var service = new google.maps.places.PlacesService(restaurant_map);
                
                service.nearbySearch({
                    location: res_location,
                    radius: 1500,
                    type: modes
                }, callback_restaurant);
            }

            function set_places ( modes , service ,school_location)
            {
                service.nearbySearch({
                    location: school_location,
                    radius: 1500,
                    type: modes
                }, callback);
            }

            function callback(results, status) {

                var types = ["school","restaurant",'store'];

                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        //console.log( results[i].types[0]  );
                        school_createMarker(results[i], results[i].types[0]);                                                                                        
                    }
                }
            }

            function callback_restaurant(results, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        //console.log( results[i].types[0]  );
                        restaurant_createMarker(results[i], results[i].types[0]);                                                                                        
                    }
                }
            }

            function callback_store(results, status) {

                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        //console.log( results[i].types[0]  );
                        store_createMarker(results[i], results[i].types[0]);                                                                                        
                    }

                }
            }

            function callback_school(results, status) {

                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        //console.log( results[i].types[0]  );
                        clickschool_createMarker(results[i], results[i].types[0]);                                                                                        
                    }
                }
            }

            function school_createMarker(place , type) {
               
                //console.log(type);
                    switch( type ){
                        case "school" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
                             break;

                        case "restaurant" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                             break;

                        case "store" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                             break;

                        case "grocery_or_supermarket" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                             break;

                    }

                    //var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";


                if( type == "school" || type == "restaurant" || type == "store" || type == "grocery_or_supermarket" )
                {   
                    var marker = new google.maps.Marker({
                        map: school_map,
                        position: place.geometry.location,
                        icon: school_icon
                    });
                }
                                                     

                google.maps.event.addListener(marker, 'mouseover', function() {
                    infowindow.setContent(place.name);
                    infowindow.open(school_map, marker);
                }); 

                google.maps.event.addListener(marker, 'mouseout', function() {
                   infowindow.close();
                });

                var service = new google.maps.places.PlacesService(school_map);
                var request = {placeId: place.place_id};
                //console.log(request);
                service.getDetails(request, function(details, status) {
                  google.maps.event.addListener(marker, 'click', function() {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {

                        var html = "<div style='position:relative'><br>" + details.name + "<br>" + details.vicinity + "<br>" + (details.formatted_phone_number !=undefined ? details.formatted_phone_number : "") + "<br>" + (details.website != undefined ? details.website : "" ) + "<br>" + (details.url != undefined ? "<a href='" + details.url + "' target='_blank' >View on Google Maps</a>" :"") + "</div>";
                      iw = new google.maps.InfoWindow({
                         content: html
                      });
                        iw.open(school_map, marker);
                    }
                  });
                });

              }

              function clickschool_createMarker(place , type) {
               
                //console.log(type);
                    switch( type ){
                        case "school" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
                             break;

                        case "restaurant" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                             break;

                        case "store" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                             break;

                        case "grocery_or_supermarket" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                             break;

                    }


                if( type == "school" || type == "restaurant" || type == "store" || type == "grocery_or_supermarket" )
                {   
                    var marker = new google.maps.Marker({
                        map: school_map2,
                        position: place.geometry.location,
                        icon: school_icon
                    });
                }
                                                     

                google.maps.event.addListener(marker, 'mouseover', function() {
                    infowindow.setContent(place.name);
                    infowindow.open(school_map2, marker);
                }); 

                google.maps.event.addListener(marker, 'mouseout', function() {
                   infowindow.close();
                });

                var service = new google.maps.places.PlacesService(school_map2);
                var request = {placeId: place.place_id};
                service.getDetails(request, function(details, status) {
                  google.maps.event.addListener(marker, 'click', function() {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {

                        var html = "<div style='position:relative'><br>" + details.name + "<br>" + details.vicinity + "<br>" + (details.formatted_phone_number !=undefined ? details.formatted_phone_number : "") + "<br>" + (details.website != undefined ? details.website : "" ) + "<br>" + (details.url != undefined ? "<a href='" + details.url + "' target='_blank' >View on Google Maps</a>" :"") + "</div>";
                      iw = new google.maps.InfoWindow({
                         content: html
                      });
                        iw.open(school_map2, marker);
                    }
                  });
                });

              }

            function restaurant_createMarker(place , type) {
               
                //console.log(type);
                    switch( type ){
                        case "school" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
                             break;

                        case "restaurant" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                             break;

                        case "store" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                             break;

                        case "grocery_or_supermarket" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                             break;
                    }
                
                if( type == "school" || type == "restaurant" || type == "store" || type == "grocery_or_supermarket" )
                {   
                    var marker = new google.maps.Marker({
                        map: restaurant_map,
                        position: place.geometry.location,
                        icon: school_icon
                    });
                }                   

                google.maps.event.addListener(marker, 'mouseover', function() {
                    infowindow.setContent(place.name);
                    infowindow.open(restaurant_map, marker);
                }); 

                google.maps.event.addListener(marker, 'mouseout', function() {
                   infowindow.close();
                });

                var service = new google.maps.places.PlacesService(restaurant_map);
                var request = {placeId: place.place_id};
                //console.log(request);
                service.getDetails(request, function(details, status) {
                    google.maps.event.addListener(marker, 'click', function() {
                        if (status == google.maps.places.PlacesServiceStatus.OK) {

                            var html = "<div style='position:relative'><br>" + details.name + "<br>" + details.vicinity + "<br>" + (details.formatted_phone_number !=undefined ? details.formatted_phone_number : "") + "<br>" + (details.website != undefined ? details.website : "" ) + "<br>" + (details.url != undefined ? "<a href='" + details.url + "' target='_blank' >View on Google Maps</a>" :"") + "</div>";
                            iw = new google.maps.InfoWindow({
                                content: html
                            });
                            iw.open(restaurant_map, marker);
                        }
                    });
                });
            }

            function store_createMarker(place , type) {
               
                //console.log(type);
                    switch( type ){
                        case "school" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
                             break;

                        case "restaurant" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                             break;

                        case "store" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                             break;

                        case "grocery_or_supermarket" : 
                             var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                             break;

                    }

                
                if( type == "school" || type == "restaurant" || type == "store" || type == "grocery_or_supermarket" )
                {   
                    var marker = new google.maps.Marker({
                        map: store_map,
                        position: place.geometry.location,
                        icon: school_icon
                    });
                }
                                                     

                google.maps.event.addListener(marker, 'mouseover', function() {
                    infowindow.setContent(place.name);
                    infowindow.open(store_map, marker);
                }); 

                google.maps.event.addListener(marker, 'mouseout', function() {
                   infowindow.close();
                });

                var service = new google.maps.places.PlacesService(store_map);
                var request = {placeId: place.place_id};
                //console.log(request);
                service.getDetails(request, function(details, status) {
                  google.maps.event.addListener(marker, 'click', function() {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {

                        var html = "<div style='position:relative'><br>" + details.name + "<br>" + details.vicinity + "<br>" + (details.formatted_phone_number !=undefined ? details.formatted_phone_number : "") + "<br>" + (details.website != undefined ? details.website : "" ) + "<br>" + (details.url != undefined ? "<a href='" + details.url + "' target='_blank' >View on Google Maps</a>" :"") + "</div>";
                      iw = new google.maps.InfoWindow({
                         content: html
                      });
                        iw.open(store_map, marker);
                    }
                  });
                });
            }

            google.maps.event.addDomListener(window, 'load', initSchoolMap);

            </script>          
        </div>
    </div>
    <!-- <div role="tabpanel" class="tab-pane" id="fullspec">
        <div class="property-detail-sections">
            <ul class="property-detail-list">
                <div class="panel-group" id="accordion">
                <?php
                if(!empty($property_full_spec['groups'])) {
                    for($i=0; $i<count($property_full_spec['groups']); $i++) {
                        $count=0;
                        for($x=0; $x<count($property_full_spec['groups'][$i]['fields']); $x++) {
                            if(!empty($property_full_spec['groups'][$i]['fields'][$x]['Label'])) {
                                if(isset($property_full_spec['groups'][$i]['fields'][$x]['Value']) && !empty($property_full_spec['groups'][$i]['fields'][$x]['Value'])) {
                                    $count++;
                                }
                            }
                        }

                        if($count>0) { ?>
                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                <div class='panel-heading'>
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#<?= $i; ?>">
                                        <h4 class='panel-title'><?php echo $property_full_spec['groups'][$i]['group']; ?><i class="indicator glyphicon <?php if($i < 2) echo 'glyphicon-chevron-up'; else echo 'glyphicon-chevron-down'; ?> pull-right"></i></h4>
                                    </a>
                                </div>
                                <div id="<?=$i;?>" class="panel-collapse collapse<?php if($i < 2) echo ' in'; ?>">
                                    <div class="panel-body ">
                                        <ul class='list-group property-detail-list'>
                                        <?php
                                            foreach($property_full_spec['groups'][$i]['fields'] as $fields) { 
                                                if($fields['Label']) { ?>

                                                    <li class="list-group-item">
                                                            <strong><?php echo $fields['Label']; ?>: </strong>                                                   
                                                            <span>
                                                                <?php if( !is_array($fields['Value']) ) { ?>
                                                                    <span><?php echo (!empty($fields['Value'])) ? $fields['Value'] : ""; ?></span>
                                                                <?php } else { ?>
                                                                    <?php foreach( $fields['Value'] as $keyO => $valO ) { ?>
                                                                        <span><?php echo (!empty($keyO)) ? $keyO : ""; ?></span>
                                                                        <span><?php echo (!empty($valO)) ? $valO : ""; ?></span>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </span>
                                                        </li>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    }
                }
                ?>
                </div>
            </ul>
            <div class="col-md-12">
            <?php if(!empty($rooms['data'])) { ?>
                <table class="table table-responsive table-striped">
                    <thead>
                        <tr>
                        <?php
                            foreach($rooms['data'][0]['Fields'] as $room) { 
                                foreach($room as $key=>$val) { ?>
                                     <td><strong><?=$key;?></strong></td>
                        <?php
                                }
                            }
                        ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($rooms['data'] as $room) { ?>
                            <tr>
                                <?php
                                    foreach($room['Fields'] as $arr) { 
                                        foreach($arr as $key=>$val) { ?>
                                            <td><?=!empty($val) ? $val : "N/A";?></td>
                                <?php
                                        }
                                    }
                                ?>
                            </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <?php } ?>
        </div>
    </div> -->

    <div role="tabpanel" class="tab-pane" id="fullspec">
        <div class="property-detail-sections">
            <ul class="property-detail-list">
                <div class="panel-group" id="accordion">
                    <?php if(!empty($property_full_spec['data'])) { 
                            $count=0;
                            $i=0;
                           foreach ($property_full_spec['data']['Main'] as $key => $value) { 
                                foreach ($value as $key2 => $value2) {
                                    $count++;
                                    $i++;
                                        if($count>0) { ?>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class='panel-heading'>
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#<?= $i; ?>">
                                                        <h4 class='panel-title'><?php echo $key2; ?><i class="indicator glyphicon <?php if($i < 2) echo 'glyphicon-chevron-up'; else echo 'glyphicon-chevron-down'; ?> pull-right"></i></h4>
                                                    </a>
                                                </div>
                                                <div id="<?=$i;?>" class="panel-collapse collapse<?php if($i < 2) echo ' in'; ?>">
                                                    <div class="panel-body ">
                                                        <ul class='list-group property-detail-list'>
                                                            <?php 
                                                                foreach ($value2 as $key3 => $value3) {
                                                                    foreach ($value3 as $key4 => $value4) { ?>
                                                                    <li class="list-group-item">
                                                                        <strong><?php echo $key4; ?>: </strong>
                                                                        <span><?php echo (isset($value4) && !empty($value4)) ? $value4 : 1; ?></span>
                                                                    </li>
                                                            <?php  
                                                                    }
                                                                }
                                                            ?>    

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>       
                                <?php } ?>   
                           <?php } ?>

                    <?php } else { ?>

                    <?php } ?>
                </div>
            </ul>
            <div id="rooms"></div>
        </div>
    </div>


    <!-- Nearby Listings -->
    <div class="property-other-listings property-detail-sections">
        <h4>Nearby Listings</h4>
        <div id="nearby-listings">
            <div class="nearby-loader">
                <div class="preloader"></div>
            </div>
        </div>
    </div>
      <?php } ?>
</div>