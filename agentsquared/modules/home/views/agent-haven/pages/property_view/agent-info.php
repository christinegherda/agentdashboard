<div class="property-agent-info">
    <div class="agent-image">
       <?php if( isset($user_info->agent_photo) AND !empty($user_info->agent_photo)) { ?>
             <img src="<?=AGENT_DASHBOARD_URL . 'assets/upload/photo/'.$user_info->agent_photo?>" class="img-thumbnail" width="300">
        <?php } else { ?>
            <?php if (isset($account_info->Images[0]->Uri) AND !empty($account_info->Images[0]->Uri)){?>
                <img src="<?=$account_info->Images[0]->Uri?>" class="img-thumbnail" width="300"> 
             <?php } else { ?>
                <img src="<?= base_url()?>/assets/images/no-profile-img.gif" alt="No Profile Image">
             <?php } ?>  
        <?php } ?> 
    </div>
    <h4 class="agent-name">
    <?php if( isset($user_info->first_name) AND !empty($user_info->first_name)) { ?>
        <?=$user_info->first_name?>
    <?php } ?> <?php if (isset($user_info->last_name) AND !empty($user_info->last_name)) {?>
        <?php echo $user_info->last_name ?>
    <?php } ?>
    </h4>
    <p>
        <?php
            echo isset($user_info->phone) ? "<i class='fa fa-phone-square'></i> Office: ".$user_info->phone."<br>": "";
            echo isset($user_info->mobile) ? "<i class='fa fa-2x fa-mobile'></i> Mobile: ".$user_info->mobile."<br>": "";
            if(isset($account_info->Phones)){
                foreach($account_info->Phones as $phone){
                    if($phone->Name == "Fax"){
                        echo "<i class='fa fa-fax'></i> Fax: ".$phone->Number ."</br>";
                    }
                }
            }
        ?>
    </p>
    <p>
        <i class="fa fa-envelope"></i>: <a href="emailto: <?=$user_info->email?>">Email Me</a>
    </p>
</div>
