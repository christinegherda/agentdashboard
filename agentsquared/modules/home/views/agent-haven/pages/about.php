
      <div class="page-content">
          <section class="property-detail-content">
              <div class="container">
                  <div class="row">
                      <div class="col-md-3 col-sm-3 invisible-xs">

                         <?php
                              $this->load->view($theme.'/pages/page_view/page-agent-info'); 
                          ?>   

                      </div>
                      <div class="col-md-9 col-sm-9">
                          <div class="col-md-12 col-md-offset-0 property-details">
                              <?php if (isset($user_info->about_agent) AND !empty($user_info->about_agent)) {?>
                              <h3>About us  </h3>
                            
                              <p><?php echo $user_info->about_agent ?></p>
                                
                              <hr>
                              <?php }?>
                              <h3>Contact us  </h3>
                              <div class="question-mess" ></div>  <br/>
                              <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
                                <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="">Name</label>
                                          <input type="text" name="name" value="" class="form-control" required>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="">Email</label>
                                          <input type="email" name="email" value="" class="form-control" required>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                          <label for="">Message</label>
                                          <textarea name="message" id="" value="" cols="30" rows="5" class="form-control" required></textarea>
                                      </div>
                                    </div>
                                </div>
                                  <button type="submit" class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
                              </form>    
                          </div>
                      </div>

                    <?php

                        $this->load->view($theme.'/pages/page_view/page-info-contact-mobile-view');

                    ?>

                  </div>
              </div>
          </section>
    </div>
