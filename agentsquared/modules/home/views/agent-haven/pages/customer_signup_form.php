
<?php
    $this->load->view($theme."/session/header-signup");
?> 

<div id="fb-root"></div>
<script>

    (function(d, s, id) {

        var js, fjs = d.getElementsByTagName(s)[0];

        if(d.getElementById(id)) return;

        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=596551583817490";
        fjs.parentNode.insertBefore(js, fjs);

    } (document, 'script', 'facebook-jssdk'));

</script>

    <section class="sign-up-page clearfix">
        <div class="sign-up-strip clearfix">
            <div class="container">
                <div class="row">
                    <div class="left-sign-up">
                        <div class="col-md-6 col-sm-6">
                            <div class="welcoming-sign-up">
                                <h1>Welcome to</h1>
                                <a href="<?= base_url()?>home">
                                   <?php if (isset($user_info->logo) AND !empty($user_info->logo)) {?>

                                     <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/logo/<?php echo $user_info->logo;?>" alt="" width="200px" height="65px";>

                                    <?php } else { ?>
                                        
                                        <div class="logo-preset-customer"> <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/logo.png" alt="Agentsquared Signup" class="sign-up-logo"></div>
                                    <?php }?>
                                </a>
                            </div>
                            
                        </div>
                    </div>
                    <div class="right-sign-up">
                        <div class="col-md-6 col-sm-6">
                            <div class="sign-up-container clearfix">
                                <h2>Sign Up</h2>
                                <div class="col-md-12">                            
                                <?php $session = $this->session->flashdata('session');                                  
                                    if(isset($session)) { ?>
                                        <div class=" display-status alert alert-flash <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>  
                                <?php } ?>
                                </div>
                                <form  action="<?php echo base_url();?>signup" method="post" id="signupForm" class="sign_up_form">
                                    <?php if( isset($_GET["type"]) ) : ?>
                                        <input type="hidden" name="type" value="<?php echo $_GET["type"]; ?>" id="" class="form-control">
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <label for="email">First Name</label>
                                        <input type="text" name="fname" id="email" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Last Name</label>
                                        <input type="text" name="lname" id="email" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="email" name="email" id="email" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Phone Number</label>
                                        <input type="phone" name="phone" id="phone" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password</label>
                                        <input type="password" name="confirm_password" id="confirm_password" class="form-control required" required>
                                    </div>
                                    <p>
                                        By clicking Create Account, I agree to the  <a href="" data-toggle="modal" data-target="#modalSignupTerms">Terms of Service</a> and <a href="" data-toggle="modal" data-target="#modalSignupPrivacy">Privacy Policy</a>.
                                    </p>
                                    <!-- <div class="col-md-12 col-sm-12 no-padding-left">
                                        <button type="submit" name="submit" class="btn btn-success btn-create-account">Create Account</button> 
                                        <a href="javascript:void(0)" onclick ="customer_fb_login();" class="fb_btn" id="fb-btn" ><img style="width: 51%;" src="<?=base_url()?>assets/images/fb2.png"></a>       
                                    </div> -->
                                    <div class="col-md-12 col-sm-12 mobilepadding-0 text-center">
                                        <button type="submit" name="submit" class="btn btn-success btn-create-account">Create Account</button> 
                                        <!-- <a href="javascript:void(0)" onclick="customer_fb_login();" class="fb_btn" id="fb-btn" ><img style="width: 51%;" src="<?=base_url()?>assets/images/fb2.png"></a>    -->
                                        <a href="javascript:void(0)" onclick="customer_fb_login();" class="fb_btn" id="fb-btn">
                                            <span class="fa fa-facebook"></span><span class="fb-text">Login with facebook</span>
                                        </a>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="have-account text-center">
                                            <p>Already have an account?</p>
                                            <p><a href="login">Login</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 footer-brand">
            <p class="text-center text-powered">2016 All Rights Reserved. Developed by
                <a href="http://www.agentsquared.com/" target="_blank" class="brand-name" data-original-title="" title=""> <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/logo.png" alt="Agentsquared Signup" class="footer-logo"></a>
            </p>
        </div>
    </section>
     <?php

         $this->load->view($theme."/session/footer-signup");
   ?>
    
