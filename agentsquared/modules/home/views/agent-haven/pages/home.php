
  <?php 

    $this->load->view($theme.'/pages/home_view/slider-area'); 

    $this->load->view($theme.'/pages/home_view/search-view');

    if( !empty($active_listings) ) { 

      $this->load->view($theme.'/pages/home_view/default-active-listings');
    }

  ?>
    <section class="other-listing-area">
        <div class="container">
            <div class="row">

                 <?php if( !empty($new_listings) ) { 

                     $this->load->view($theme.'/pages/home_view/default-new-listings'); 

                  }?>

                  <?php if( !empty($nearby_listings) ) { 

                     $this->load->view($theme.'/pages/home_view/default-nearby-listings'); 

                  }?>
            </div>
        </div>
    </section>

    <section class="agent-info-area">
        <div class="container">
            <div class="row">
                
                <?php $this->load->view($theme.'/pages/home_view/contact-view'); ?>

                <div class="col-md-8 col-sm-12 nopadding-left">

                  <?php $this->load->view($theme.'/pages/home_view/agent-info-view'); ?> 

                   <?php if( !empty($sold_listings) ) { 

                      $this->load->view($theme.'/pages/home_view/sold-properties'); 

                   }?> 

                </div>
     
        </div>
    </section>

