
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="blog-detail-content">
                        <h2 class="blog-title"><?php echo ucwords($blog_post['title']); ?></h2>
                        <p>
                            <?php echo ucwords($blog_post['content']); ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="filter-tab">
                        <h4 class="text-center">Archives</h4>
                        <hr>
                        <ul>
                            <?php if( !empty($all_blog_archives) ) { 
                                        foreach ($all_blog_archives as $archive) :
                                            //printA($archive);exit;
                                ?>  
                                 <li style="margin-left:20px;list-style-type:circle;margin-top:10px"><?php echo $archive->year?> </li>
                                 <li style="margin-left:20px">- <a href="<?=base_url()?>home/archives/<?=date("Y-m", strtotime($archive->post_date))?>"><?php echo $archive->month_name?></a> <span class="badge"><?php echo $archive->total?></span></li>

                                <?php endforeach; ?>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="filter-tab">
                        <h4 class="text-center">Recent Blogs</h4>
                        <hr>

                        <ul class="recent-blogs clearfix">
                         <?php if( !empty($recent_posts) ) { 
                                        foreach ($recent_posts as $rp) :
                                ?> 

                                 <li>
                                    <div class="col-md-8 nopadding">
                                        <a href="<?=base_url()?>home/post/<?php echo (str_replace(" ", "-", $rp->slug))?>"><?php echo ucwords($rp->title); ?></a>    
                                    </div>
                                    <div class="col-md-4">
                                        <span class="pull-right"><?php echo date("F d, Y", strtotime($rp->post_date)); ?></span>    
                                    </div>
                                </li>
           
                             <?php endforeach; ?>
                                <?php } ?>
                        </ul>
                    </div>

                    <?php $this->load->view($theme.'/pages/page_view/page-agent-info'); ?>
                    
            </div>
        </div>
    </div>

   