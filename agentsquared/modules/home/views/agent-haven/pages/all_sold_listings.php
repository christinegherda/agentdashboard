
    <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                   <!-- Sidebar Area -->
                    <?php $this->load->view($theme."/session/sidebar"); ?>
                    <div class="col-md-9 col-sm-9">
                        <?php 
                            if(isset($sold_listings) AND !empty($sold_listings)) {?>

                                <section class="saved-search-area">
                                    <div class="search-listings">
                                        <?php
                                            foreach($sold_listings as $sold){?>
                                            
                                                <div class="col-md-4 col-sm-6">
                                                    <div style="min-height: 280px;" class="search-listing-item">
                                                     <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                                                        <div class="search-property-image">
                                                            <?php if($is_capture_leads) { ?>
                                                                <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                                    <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $sold->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                <?php } else { ?>
                                                                    <a href="<?= base_url();?>property-details/<?= $sold->StandardFields->ListingKey; ?>">
                                                                <?php } if(isset($sold->StandardFields->Photos[0]->Uri300)) { ?>
                                                                        <img src="<?=$sold->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                <?php } else { ?>
                                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                                <?php } ?>
                                                                    </a>
                                                            <?php } else { ?>
                                                                <?php if(isset($sold->StandardFields->Photos[0]->Uri300)) { ?>
                                                                    <a href="<?= base_url();?>property-details/<?= $sold->StandardFields->ListingKey; ?>"><img src="<?=$sold->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;"></a>
                                                                <?php } else { ?>
                                                                    <a href="<?= base_url();?>property-details/<?= $sold->StandardFields->ListingKey; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;"></a>
                                                                <?php } ?>
                                                            <?php } ?>    
                                                        </div>
                                                        <div class="search-listing-title">
                                                            <div class="col-md-12 col-xs-12">
                                                                <p class="search-property-title">
                                                                 <?php if($is_capture_leads) { ?>
                                                                    <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                                        <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $sold->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                    <?php } else { ?>
                                                                        <a class="listing-link" href="<?= base_url();?>property-details/<?= $sold->StandardFields->ListingKey; ?>">
                                                                    <?php } echo $sold->StandardFields->UnparsedFirstLineAddress; ?>
                                                                        </a>
                                                                <?php } else { ?>
                                                                    <a class="listing-link" href="<?= base_url();?>property-details/<?= $sold->StandardFields->ListingKey; ?>">
                                                                        <?php echo $sold->StandardFields->UnparsedFirstLineAddress; ?>
                                                                    </a>
                                                                <?php } ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <p><i class="fa fa-map-marker"></i>
                                                            <?php
                                                                $mystring = $sold->StandardFields->City;
                                                                $findme   = '*';
                                                                $pos = strpos($mystring, $findme);
                                                                
                                                                if($pos === false) 
                                                                    echo $sold->StandardFields->City . ", " . $sold->StandardFields->StateOrProvince . " " . $sold->StandardFields->PostalCode;
                                                                else
                                                                    echo $sold->StandardFields->PostalCity . ", " . $sold->StandardFields->StateOrProvince . " " . $sold->StandardFields->PostalCode;
                                                            ?>
                                                        </p>
                                                        
                                                        <p><i class="fa fa-usd"></i> <?=number_format($sold->StandardFields->CurrentPrice);?></p>
                                                    </div>
                                                </div>

                                            <?php  }
                                            }?>
                                    </div>
                                </section>

                                <?php
                                    if(isset($total_count)) {
                                        if($total_count > 25) { ?>
                                            <div class="pagination-area">
                                             <?php echo $pagination;?>
                                <?php }
                                }?>
                    </div>
                </div>
            </div>
        </section>
    </div>
   