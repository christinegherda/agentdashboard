	   
       <?php
            $this->load->view($theme.'/pages/page_view/advance-search-form');  
        ?> 
        <section class="property-map">
            <div id="map_canvas" style="border: 2px solid #3872ac;"></div>
            <style type="text/css">
                html,
                body,
                #map_canvas  {
                    height: 600px;
                    width: 100%;
                    margin-top: 0px;
                    padding: 0px;
                }
            </style>

            <script type="text/javascript">

                
                    function initialize() {
                                map = new google.maps.Map(
                                    document.getElementById("map_canvas"), {

                                      center: new google.maps.LatLng(37.387474,-122.05754339999999),
                                       zoom: 15,
                                       scrollwheel: false,
                                       mapTypeId: google.maps.MapTypeId.ROADMAP
                                     });

                                geocoder = new google.maps.Geocoder();

                            <?php 
                                if(isset($featured) && $featured) {
                                    foreach($featured as $feature) {
                            ?>
                                    //base_url()."home/other_property?listingId=".$property["Id"]
                                    var l1 = "<?php echo $feature['StandardFields']['UnparsedFirstLineAddress']?>";
                                    var l2 = "<?php echo $feature['StandardFields']['UnparsedAddress']?>";
                                    var l3 = base_url + "other-property-details/<?php echo $feature['StandardFields']['ListingKey']?>";
                                    var l4 = "<?= (isset($feature['Photos']['Uri300'])) ? $feature['Photos']['Uri300'] : ""; ?>";

                                    var locations = [
                                        [l1, l2, l3, l4]
                                    ];

                                    geocodeAddress(locations);

                             <?php } } ?>

                     }

                            var geocoder;
                            var map;
                            var bounds = new google.maps.LatLngBounds();

                            function geocodeAddress(locations, i) {
                                var title = locations[0][0];
                                var address = locations[0][1];
                                var url = locations[0][2];
                                var img = locations[0][3];

                                geocoder.geocode({
                                  'address': locations[0][1]
                                },

                                function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        var marker = new google.maps.Marker({
                                              icon: 'http://maps.google.com/mapfiles/ms/icons/red.png',
                                              map: map,
                                              position: results[0].geometry.location,
                                              title: title,
                                              animation: google.maps.Animation.DROP,
                                              address: address,
                                              url: url
                                        })
                                        //infoWindow(marker, map, title, address, url, img);

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });

                                        google.maps.event.addListener(marker, 'click', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });
              

                                         google.maps.event.addListener(marker, 'mouseout', function() {
                                            iw.close();
                                        });

                                        bounds.extend(marker.getPosition());
                                        map.fitBounds(bounds);

                                    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) { 

                                        wait = true;

                                        setTimeout(function() {
                                            geocodeAddress(locations, i);
                                        }, 1000 );

                                       // setTimeout("wait = true", 10000);
                                        
                                    } else {
                                        console.log( "Geocode was not successful for the following reason: " + status );
                                        //alert("Geocode was not successful for the following reason: " + status);
                                    }
                                 
                                });
                            }
          

                    google.maps.event.addDomListener(window, "load", initialize);

                    function infoWindow(marker, map, title, address, url,img) {
                          google.maps.event.addListener(marker, 'mouseover', function() {
                            var html = "<div><h3>" + title + "</h3><p>" + address + "<br><img src="+img+" alt="+address+" width='200'  height='150'><br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                            iw = new google.maps.InfoWindow({
                              content: html,
                              maxWidth: 350
                            });
                            iw.open(map, marker);

                          });

                          google.maps.event.addListener(marker, 'mouseout', function() {
                                
                                new google.maps.InfoWindow.close(map, marker);
                            });

                    }

                    
            </script>        
           
        </section>
	<section class="property-main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <?php 

                        $this->load->view($theme.'/pages/findahome_view/mortgage-calc');

                        $this->load->view($theme.'/pages/findahome_view/customer-question');
                    ?>
                </div>
                <div class="col-md-9 col-sm-9">
                	<div class="search-listings">
                        <?php
                        if(!isset($featured) || count($featured) == 0 || empty($featured)) { ?>
                            <div class="nosold-property">
                              <i class="fa fa-exclamation-triangle"></i>
                              Results not found. Please search again.
                            </div>
                        <?php
                        } else { ?>
                            <div class="col-md-12 col-sm-12">
                                <h4>
                                    You searched for: 
                                     <!-- <?php 
                                        foreach($_GET as $get) {
                                            if(is_array($get)) {
                                                foreach ($get as $g) {
                                                    echo ucfirst($g).", ";
                                                }
                                            } else {
                                                if($get == '') {
                                                    continue;
                                                } elseif($get == '0') {
                                                    continue;
                                                } else {
                                                    echo ucfirst($get).", ";
                                                }
                                            }
                                        }
                                    ?>  --> 

                                    <?php 
                                        if(isset($_GET['Search'])) {

                                            $search = ($_GET['Search']) ? $_GET['Search'].", " : "" ;
                                            
                                            if(!empty($_GET['bedroom']) && !empty($_GET['bathroom'])) {

                                                $bed = ($_GET['bedroom'] != 1 ) ? "Beds" : "Bed";
                                                $bath = ($_GET['bathroom'] != 1) ? "Baths" : "Bath";
                                                $bedroom = $_GET['bedroom'];
                                                $bathroom = $_GET['bathroom'];

                                            } else {
                                                $bed = "Bed";
                                                $bath = "Bath";
                                                $bedroom =  0;
                                                $bathroom = 0 ; 
                                            }

                                            if(!empty($_GET['min_price']) && !empty($_GET['max_price'])){
                                                $min_price = $_GET['min_price'] ;
                                                $max_price = $_GET['max_price'];

                                            } else {
                                                $min_price =  0 ;
                                                $max_price =  0 ;
                                            }
                                            
                                            echo ucfirst($search) ."Price (".$min_price."-".$max_price."), ".$bedroom ." ".$bed.", ".$bathroom ." ".$bath;
                                        } 
                                    ?>
                                </h4>
                                   <?php 
                                        $offset = (isset($_GET['offset'])) ? $_GET['offset'] : "" ; 
                                    ?>

                                    <h4><?php echo !empty($total_count) ? number_format($total_count) :"" ?> Properties Found. <!-- Showing  <?php echo !empty($offset) ? number_format($offset) : "" ?> <?php echo !empty($offset) ? "of" : "" ?> <?php echo !empty($total_count) ? number_format($total_count) : ""; ?> --></h4>
                                <br/>
                            </div>
                        <?php
                            $is_openhouse=false;
                            if(isset($_GET['listing_change_type'])) {
                                foreach($_GET['listing_change_type'] as $show_me) {
                                    $is_openhouse = ($show_me == "Open House") ? true : false;
                                }
                            }
                            if(!$is_openhouse) {
                                if(isset($_GET['Search'])) {
                                    $search_get = $_GET['Search'];
                                    $sg = strtolower($search_get);
                                    $is_openhouse = ($sg == 'open house') ? true : false;
                                }
                            }

                            foreach($featured as $feature) {
                                if(isset($feature['Photos']['Uri300'])) { ?>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="search-listing-item">
                                            <div class="search-property-image">
                                                <?php if($is_capture_leads) { ?>
                                                    <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                        <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']); ?>" data-propertyType="other_property">
                                                    <?php } else { ?>
                                                        <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>" title="">
                                                    <?php } ?>                    
                                                            <img src="<?= $feature['Photos']['Uri300'] ?>" alt="<?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>" class="img-responsive">
                                                        </a> 
                                                <?php } else { ?>
                                                    <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>" title="">
                                                        <img src="<?= $feature['Photos']['Uri300']?>" alt="<?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>" class="img-responsive">
                                                    </a>
                                                <?php } ?>
                                            </div>
                                            <div style="top:15px;" class="property-listing-status">
                                                <?php
                                                    if(isset( $feature['StandardFields']["MlsStatus"])){?>
                                                        <?php echo $feature['StandardFields']["MlsStatus"];?>
                                                <?php  } ?>
                                            </div>
                                            <div class="search-listing-type">
                                                <?php
                                                    if(isset( $feature['StandardFields']["PropertyClass"])){?>
                                                        <?php echo $feature['StandardFields']["PropertyClass"];?>
                                                <?php  } ?>
                                            </div>
                                            <div class="search-listing-title">
                                                <div class="col-md-9 col-sm-9 col-xs-9">
                                                    <p class="search-property-title">
                                                    <?php if($is_capture_leads) { ?>
                                                        <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                            <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']); ?>" data-propertyType="other_property">
                                                        <?php } else { ?>
                                                            <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>">
                                                        <?php } echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>
                                                            </a>
                                                    <?php } else { ?>
                                                        <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>">
                                                            <?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>
                                                        </a>
                                                    <?php } ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <p>
                                                        <?php  
                                                            $property_saved = FALSE; 
                                                            if(isset($_SESSION["save_properties"])) :
                                                                foreach($_SESSION["save_properties"] as $key) :
                                                                    if($key['property_id'] == $feature['StandardFields']['ListingKey']) : 
                                                                        $property_saved = TRUE; 
                                                                        break; 
                                                                    endif;
                                                                endforeach;
                                                            else : 
                                                                $property_saved = FALSE; 
                                                            endif;
                                                        ?>
                                                        <a href="javascript:void(0)" data-pro-id="<?=$feature['StandardFields']['ListingKey']?>" class="save-favorate" title="Save Property" >
                                                            <span id="isSaveFavorate_<?=$feature['StandardFields']['ListingKey']?>">
                                                                <?php echo ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>" ;?>
                                                            </span>
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                            <p><i class="fa fa-map-marker"></i><?php echo str_replace($feature['StandardFields']["UnparsedFirstLineAddress"].',', "", $feature['StandardFields']["UnparsedAddress"]); ?></p>
                                            <p class="search-property-location"><i class="fa fa-usd"></i> <?=number_format($feature['StandardFields']['CurrentPrice'])?></p>
                                            <ul class="list-inline search-property-specs">
                                            <?php
                                              
                                                if($feature['StandardFields']['BedsTotal'] && is_numeric($feature['StandardFields']['BedsTotal'])) { ?>
                                                    <li><i class="fa fa-bed"></i> <?=$feature['StandardFields']['BedsTotal']?> Bed</li>
                                            <?php }
                                                if($feature['StandardFields']['BathsTotal'] && is_numeric($feature['StandardFields']['BathsTotal'])) { ?>
                                                    <li><i class="icon-toilet"></i> <?=$feature['StandardFields']['BathsTotal']?> Bath</li>
                                            <?php }?>

                                           <?php
                                            if(!empty($feature['StandardFields']['BuildingAreaTotal']) && ($feature['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($feature['StandardFields']['BuildingAreaTotal'])) {?>

                                                    <li><?=number_format($feature['StandardFields']['BuildingAreaTotal'])?> sqft</li>

                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeArea']) && ($feature['StandardFields']['LotSizeArea'] != "0")   && is_numeric($feature['StandardFields']['LotSizeArea'])) {

                                                if(!empty($feature['StandardFields']['LotSizeUnits']) && ($feature['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                                    <li>lot size area: <?=number_format($feature['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                <?php } else {?>

                                                    <li>lot size area: <?=number_format($feature['StandardFields']['LotSizeArea'])?> </li>

                                                <?php }?>


                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeSquareFeet']) && ($feature['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($feature['StandardFields']['LotSizeSquareFeet'])) {?>

                                                    <li>lot size area: <?=number_format($feature['StandardFields']['LotSizeSquareFeet'])?></li> 

                                             <?php } elseif(!empty($feature['StandardFields']['LotSizeAcres']) && ($feature['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($feature['StandardFields']['LotSizeAcres'])) {?>

                                                    <li>lot size area: <?=number_format($feature['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>

                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeDimensions']) && ($feature['StandardFields']['LotSizeDimensions'] != "0")   && ($feature['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                <li class="lot-item">lot size area: <?=$feature['StandardFields']['LotSizeDimensions']?></li>
                                            <?php } ?>

                                            </ul>
                                            <?php 
                                                if($is_openhouse) { ?>
                                                    <style type="text/css">
                                                        .search-listing-item {
                                                            min-height: 410px;
                                                        }
                                                    </style>
                                            <?php
                                                    $pos = strpos(strtoupper($feature['StandardFields']['PublicRemarks']), 'OPEN HOUSE');
                                                    $ret = substr($feature['StandardFields']['PublicRemarks'], $pos, 60); ?>
                                                    <p style="color:red;"><?php echo $ret; ?>....
                                                        <?php if($is_capture_leads) { ?>
                                                            <a href="#" type="button" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']); ?>" data-propertyType="other_property">
                                                        <?php } else { ?>
                                                            <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>" target="_blank">
                                                        <?php } ?>
                                                            Read More</a>
                                                    </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php 
                                }
                            }
                        }
                    ?>
            </div>
            <?php
                if(isset($total_count)) {
                    if($total_count > 25) { ?>
                        <div class="pagination-area">
                        <?php echo $pagination;?>
                            <!-- <nav>
                                <ul class="pagination">
                                    <?php
                                        $x = 5;
                                        $pagerStart = ((round($current_page)%$x === 0) ? round($current_page) : round(($current_page+$x/2)/$x)*$x) - $x;
                                        $pagerEnd = (round($current_page)%$x === 0) ? round($current_page) : round(($current_page+$x/2)/$x)*$x;

                                        if($current_page > $x) {
                                            if(isset($_GET['page']) && $_GET['page']) {
                                                $getStr = str_replace('page='.$current_page,'page='.($pagerStart),http_build_query($_GET));
                                            } else {
                                                if($_GET) {
                                                    $getStr = http_build_query($_GET).'&page='.($pagerStart);
                                                } else {
                                                    $getStr = "page=".($pagerStart);
                                                }
                                            }
                                            ?>
                                            <li ><a href="<?php echo base_url().'listings?'.$getStr; ?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span> Prev</a></li>
                                    <?php
                                        }

                                        for($i = $pagerStart + 1; ($i <= $pagerEnd) && $pagerEnd <= $total_count; $i++) {
                                            $getStr = "";
                                            if(isset($_GET['page']) && $_GET['page']) {
                                                $getStr = str_replace('page='.$current_page,'page='.$i,http_build_query($_GET));
                                            } else {
                                                if($_GET) {
                                                    $getStr = http_build_query($_GET).'&page='.$i;
                                                } else {
                                                    $getStr = "page=".$i;
                                                }
                                            }
                                            ?>
                                            <li <?php if($i == $current_page) { echo 'class="active"'; } ?>>
                                                <a href="<?php echo base_url().'listings?'.$getStr; ?>"><?=$i?></a>
                                            </li>
                                            <?php
                                        }

                                        if(($pagerEnd + 5 ) < $total_count ) {
                                            if(isset($_GET['page']) && $_GET['page']) {
                                                $getStr = str_replace('page='.$current_page,'page='.($pagerEnd+1),http_build_query($_GET));
                                            } else {
                                                if($_GET) {
                                                    $getStr = http_build_query($_GET).'&page='.($pagerEnd+1);
                                                } else {
                                                    $getStr = "page=".($pagerEnd+1);
                                                }
                                            } ?>
                                            <li ><a href="<?php echo base_url().'listings?'.$getStr; ?>" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a></li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </nav> -->
                        </div>
                    <?php
                    }
                }
            ?>
        </div>
    </section>