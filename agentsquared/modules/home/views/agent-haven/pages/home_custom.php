
     <?php 

        $this->load->view($theme.'/pages/home_view/slider-area'); 

        $this->load->view($theme.'/pages/home_view/search-view');
    ?>

    <!--##### Featured Listing Section #####-->
 <?php

    //featured is active listing 
    if(isset($is_active_featured)){

         $this->load->view($theme.'/pages/home_view/custom-active-listings');

    //featured is new listing 
    } elseif(isset($is_new_featured)){
        if(isset($new_listings) && !empty($new_listings)){

            $this->load->view($theme.'/pages/home_view/custom-new-listings');
        }

    //featured is nearby listing 
    } elseif(isset($is_nearby_featured)){
        if(isset($nearby_listings) && !empty($nearby_listings)){

            $this->load->view($theme.'/pages/home_view/custom-nearby-listings');
        }

    //featured is office listing 
    } elseif(isset($is_office_featured)){
        if(isset($office_listings) && !empty($office_listings)){

             $this->load->view($theme.'/pages/home_view/custom-office-listings');
        }

    //featured is saved search
    } elseif(isset($is_saved_search_featured)){
        if(isset($saved_searches_featured[0]) && !empty($saved_searches_featured[0])){

            $this->load->view($theme.'/pages/home_view/featured-saved-searches');
        }
    }

    if(isset($is_active_selected) AND !empty($is_active_selected)){
        
        $this->load->view($theme.'/pages/home_view/custom-active-listings');
        
    }?>

    <?php
     //Office Listing Section 
    if(isset($is_office_selected)){
        if(isset($office_listings) AND !empty($office_listings)){

            $this->load->view($theme.'/pages/home_view/custom-office-listings');
        } 
    }?>

    <!--##### New Listing and Nearby Listing Section #####-->
    <?php if(isset($is_new_selected) && isset($is_nearby_selected)){?>
    <section class="other-listing-area">
        <div class="container">
            <div class="row">
                <?php
                    if(isset($new_listings) AND !empty($new_listings)){

                    $this->load->view($theme.'/pages/home_view/default-new-listings'); 

                 }?> 

                <?php
                    if(isset($nearby_listings) AND !empty($nearby_listings)){
                
                   $this->load->view($theme.'/pages/home_view/default-nearby-listings'); 

                 }?>

            </div>
        </div>
    </section>
    <?php } elseif(isset($is_new_selected)){

            //New Listing Section
            if(isset($new_listings) AND !empty($new_listings)){

                 $this->load->view($theme.'/pages/home_view/custom-new-listings');

            } 

        } elseif(isset($is_nearby_selected)){

            //Nearby Listing Section 
            if(isset($nearby_listings) AND !empty($nearby_listings)){

                $this->load->view($theme.'/pages/home_view/custom-nearby-listings');

            } 
        }?>

    <!--##### New Save Searched Section #####-->
    <?php
    if(isset($is_saved_search_selected) AND !empty($is_saved_search_selected)){

      if(isset($saved_searches_selected[0]) AND !empty($saved_searches_selected[0])){

           $this->load->view($theme.'/pages/home_view/custom-saved-searches');                        
                                
        }
    }?>

   <section class="agent-info-area">
        <div class="container">
            <div class="row">
                
                <?php $this->load->view($theme.'/pages/home_view/contact-view'); ?>

                <div class="col-md-8 col-sm-12 nopadding-left">

                  <?php $this->load->view($theme.'/pages/home_view/agent-info-view'); ?> 

                   <?php if( !empty($sold_listings) ) { 

                      $this->load->view($theme.'/pages/home_view/sold-properties'); 

                   }?> 

                </div>
     
        </div>
    </section>