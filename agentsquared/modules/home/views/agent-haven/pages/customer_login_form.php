<?php
    $url = (isset($_GET["property_id"]) || isset($_GET["url_link"])) ? "login?".http_build_query($_GET) : "login";
?> 

<?php
    $this->load->view($theme."/session/header-signup");
?>

    <section class="sign-in-page clearfix">
        <div class="sign-up-strip clearfix">
            <div class="container">
                <div class="row">
                        <div class="left-sign-up">
                            <div class="col-md-6 col-sm-6">
                                <div class="welcoming-sign-up">
                                    <h1>Welcome to</h1>
                                    <a href="<?= base_url()?>home">
                                       <?php if (isset($user_info->logo) AND !empty($user_info->logo)) {?>

                                         <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/logo/<?php echo $user_info->logo;?>" alt="" width="200px" height="65px";>

                                        <?php } else { ?>

                                        <div class="logo-preset-customer"> <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/logo.png" alt="Agentsquared Signup" class="sign-up-logo"></div>
                                        <?php }?>
                                    </a>
                                </div>           
                            </div>
                        </div>
                        <div class="right-sign-up">
                            <div class="col-md-6 col-sm-6">
                                <div class="sign-in-container clearfix">
                                    <h2>Login</h2>
                                    <?php $session = $this->session->flashdata('session');                                  
                                        if( isset($session) )
                                        {
                                    ?>
                                        <div class="alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo $session["message"]; ?></div>  
                                    <?php } ?>
                                    <form class="form-signin" action="<?=$url?>" method="post" id="signupForm">
                                        <?php 
                                            if(isset($_GET["property_id"])) : 
                                                $this->session->set_userdata('isSavePropertyId', $_GET["property_id"]); ?>
                                                <input type="hidden" name="isHavePropertyId" value="<?php echo (isset($_GET["property_id"])) ? $_GET["property_id"] : "" ;?>" class="isPropertyId" /> 
                                        <?php 
                                            endif;
                                            
                                            if(isset($_GET) AND empty($_GET["property_id"]) AND empty($_GET["listingId"])) : 
                                                $this->session->set_userdata('isSaveSearchPropertyUrl', http_build_query($_GET)); ?> 
                                                <input type="hidden" name="isHaveSearchPropertyUrl" value="<?php echo (isset($_GET)) ? http_build_query($_GET) : "" ;?>" class="isPropertyId" />
                                        <?php 
                                            endif; 
                                        ?>
                                        <div class="form-group">
                                            <label for="email">Username or Email</label>
                                             <input type="text" name="email" class="form-control" required> 
                                        </div>
                                        <div class="form-group mb-25px">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password" class="form-control" required>
                                        </div>
                                        <!-- <div class="col-md-12 col-sm-12  no-padding-right">
                                            <button class="btn btn-default btn-create-account" name="submit" type="submit">Sign In</button>
                                            <a href="javascript:void(0)" onclick="customer_fb_login();" class="fb_btn" id="fb-btn" ><img style="width: 51%;" src="<?=base_url()?>assets/images/fb2.png"></a>   
                                        </div> -->
                                        <div class="col-md-12 col-sm-12 mobilepadding-0 text-center">
                                            <button class="btn btn-default btn-create-account" name="submit" type="submit">Sign In</button>
                                            <!-- <a href="javascript:void(0)" onclick="customer_fb_login();" class="fb_btn" id="fb-btn" ><img style="width: 51%;" src="<?=base_url()?>assets/images/fb2.png"></a>    -->
                                            <a href="javascript:void(0)" onclick="customer_fb_login();" class="fb_btn" id="fb-btn">
                                                <span class="fa fa-facebook"></span><span class="fb-text">Login with facebook</span>
                                            </a>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="sign-in-account text-center">
                                                <p>Don't have an account?</p>
                                                <p><a href="signup">Create Account</a></p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> 
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 footer-brand">
            <p class="text-center text-powered">2016 All Rights Reserved. Developed by
                <a href="http://www.agentsquared.com/" target="_blank" class="brand-name" data-original-title="" title=""> <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/logo.png" alt="Agentsquared Signup" class="footer-logo"></a>
            </p>
        </div>
    </section>

   <?php

         $this->load->view($theme."/session/footer-signup");
   ?>

