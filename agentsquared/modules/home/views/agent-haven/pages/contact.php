<div class="page-content">
    <section class="property-detail-content">
        <div class="container">
            <div class="row">
            	<!-- Sidebar Area -->
    			<?php $this->load->view($theme."/session/sidebar"); ?>

				<div class="col-md-9 col-sm-9 agent-map">
				    <div id="map_canvas" class="col-md-12 col-sm-12"></div>
				    <style type="text/css">
				      #map_canvas {
				        height: 600px;
				        width: 100%;
				        margin-top: 0px;
				        padding: 0px;
				      }
				    </style>
				    <script type="text/javascript">
				      	var geocoder = new google.maps.Geocoder();
				      	var address = "<?php echo (isset($account_info->Addresses[0]->Address)) ? $account_info->Addresses[0]->Address : '' ;?>";
				      	

				     	function initialize() {

				        	geocoder = new google.maps.Geocoder();
				       	 	geocoder.geocode({ 'address': address }, function (results, status) {

				          	if(status == google.maps.GeocoderStatus.OK) {

				            	latitude = results[0].geometry.location.lat();
				            	longitude = results[0].geometry.location.lng();

					            var myOptions = {
					                zoom:12,
					                scrollwheel: false,
					                center:new google.maps.LatLng(latitude,longitude),
					                mapTypeId: google.maps.MapTypeId.ROADMAP
					            };

				            	map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

					            // position of marker
					            google.maps.event.addListenerOnce(map, 'idle', function() {
				              		this.getDiv().style.width = '100%';
				              		this.panBy(-(this.getDiv().offsetWidth/3),0);
				              		google.maps.event.trigger(this, 'resize');
				            	});

					            marker = new google.maps.Marker({
					                map: map,
					                icon: 'http://maps.google.com/mapfiles/ms/icons/red.png',
					                position: new google.maps.LatLng(latitude,longitude),
					                title: address,
					                address: address,
					            });

					            google.maps.event.addListener(marker, 'click', function() {
					                infowindow.open(map, marker);
					                map.setCenter(marker.getPosition()); 
					            });

				          	} else {
				            	alert("Geocode was not successful for the following reason: " + status);
				          	}

				        });

				      }

				      google.maps.event.addDomListener(window, 'load', initialize);

				    </script>
					<div class="col-md-12 col-md-offset-0 property-details contac-form-wrapper">
					 	<h3>CONTACT ME  </h3>
					  	<div class="question-mess" ></div> <br/>
						<form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
							<div class="form-group">
								<label for="">Name</label>
								<input type="text" name="name" value="" class="form-control" required>
							</div>
							<div class="form-group">
							   	<label for="">Email</label>
							    <input type="email" name="email" value="" class="form-control" required>
							</div>
							<div class="form-group">
							   	<label for="">Phone</label>
							    <input type="phone" name="phone" value="" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="">Message</label>
								<textarea name="message" id="" value="" cols="30" rows="5" class="form-control" required></textarea>
							</div>
							<button type="submit" class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
						</form>    
					</div>
				</div>
			</div>
		</div>
	</section>
</div>