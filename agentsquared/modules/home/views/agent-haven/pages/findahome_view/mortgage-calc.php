 
 <div class="filter-panel sidebar">
    <div class="filter-tab">
        <h4 class="text-center">Mortgage Calculator</h4>
        <hr>
        <form  method="post" id="mortgageCalculator">
            <div class="form-group">
                <label for="sale_price">Price of Home ($<span id="sale_price_label"></span>)</label>
                <input type="text" name="sale_price" id="sale_price" value="<?php echo $this->session->flashdata('mortgage_price_value'); ?>" class="form-control">
                <input type="hidden" name="calculator_url" value="<?php echo base_url();?>home/mortgage_calculator">
            </div>
            <div class="form-group">
                <label for="down_percent">Downpayment (<span id="down_percent_label"></span>%)</label>
                <input type="text" name="down_percent" id="down_percent" value="<?php echo $this->session->flashdata('mortgage_downpayment_value'); ?>" class="form-control">
            </div>
            <div class="form-group">
                <label for="mortgage_interest_percent">Annual Interest Rate (<span id="mortgage_interest_percent_label"></span>%)</label>
                <input type="text" name="mortgage_interest_percent" id="mortgage_interest_percent" value="<?php echo $this->session->flashdata('mortgage_interest_rate_value'); ?>" class="form-control">
            </div>
            <div class="form-group">
                <label for="year_term">Loan Term (<span id="year_term_label"></span>  years)</label>
                <input type="text" name="year_term" id="year_term" value="<?php echo $this->session->flashdata('mortgage_loan_terms_value'); ?>" class="form-control">
            </div>
            <div class="col-md-12 col-sm-12 nopadding">
                <button class="btn btn-default btn-block submit-button"  id="morg_calc_btn">Calculate <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i></button>
            </div>
            <div class="col-md-12 col-sm-12">
               <div id="mortgage-result">
                   <?php echo $this->session->flashdata('mortgage_monthly'); ?>
                   <!-- <?php echo $this->session->flashdata('mortgage_monthly_interest'); ?>
                   <?php echo $this->session->flashdata('mortgage_annual_interest'); ?>
                   <?php echo $this->session->flashdata('mortgage_downpayment'); ?>
                   <?php echo $this->session->flashdata('mortgage_amount_financed'); ?> -->
               </div>
            </div>
        </form>
    </div>
</div>