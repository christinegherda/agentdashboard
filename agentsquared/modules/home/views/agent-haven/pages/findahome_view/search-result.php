                <div class="col-md-9 col-sm-9">
                    <div class="search-listings">
                        <?php

                            if(!empty($active_listings)) {
                                
                                $countF = 0;

                                foreach($active_listings as $active) {

                                $countF++;
                        ?>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="search-listing-item">
                                                <div class="search-property-image">
                                                <?php if($is_capture_leads) { ?>
                                                    <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                        <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $active->StandardFields->ListingKey; ?>" data-propertyType="featured_property">
                                                    <?php } else { ?>
                                                        <a href="<?= base_url();?>property-details/<?= $active->StandardFields->ListingKey; ?>">
                                                    <?php } if(isset($active->StandardFields->Photos[0]->Uri300)) { ?>
                                                            <img src="<?=$active->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                    <?php } else { ?>
                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                    <?php } ?>
                                                        </a>
                                                <?php } else { ?>
                                                    <a href="<?= base_url();?>property-details/<?= $active->StandardFields->ListingKey; ?>">
                                                        <?php if(isset($active->StandardFields->Photos[0]->Uri300)) { ?>
                                                            <img src="<?=$active->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } else { ?>
                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } ?>
                                                    </a>
                                                <?php } ?>

                                                    <a href="<?= base_url();?>property-details/<?= $active->StandardFields->ListingKey; ?>">
                                                        <?php if(isset($active->StandardFields->Photos[0]->Uri300)) { ?>
                                                            <img src="<?=$active->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } else { ?>
                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } ?>
                                                    </a>
                                          
                                                </div>
                                                <div style="top:15px;" class="property-listing-status">
                                                   <?php
                                                        if(isset($active->StandardFields->MlsStatus)){?>
                                                            <?php echo $active->StandardFields->MlsStatus;?>
                                                    <?php  } ?> 
                                                </div>
                                                <div class="search-listing-type">
                                                    <?php
                                                        if(isset($active->StandardFields->PropertyClass)){?>
                                                            <?php echo $active->StandardFields->PropertyClass;?>
                                                    <?php  } ?> 
                                                </div>
                                                <div class="search-listing-title">
                                                    <div class="col-md-9 col-xs-9 padding-0">
                                                        <p class="search-property-title">
                                                             <?php if($is_capture_leads) { ?>
                                                                <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                                    <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $active->StandardFields->ListingKey; ?>" data-propertyType="featured_property">
                                                                <?php } else { ?>
                                                                    <a href="<?= base_url();?>property-details/<?= $active->StandardFields->ListingKey; ?>">
                                                                <?php } echo $active->StandardFields->UnparsedFirstLineAddress; ?>
                                                                    </a>
                                                            <?php } else { ?>
                                                                <a href="<?= base_url();?>property-details/<?= $active->StandardFields->ListingKey; ?>"><?php echo $active->StandardFields->UnparsedFirstLineAddress; ?></a>
                                                            <?php } ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-3">
                                                        <p>
                                                            <?php  $property_saved = FALSE; if( isset( $_SESSION["save_properties"] ) ) : ?>
                                                                <?php foreach( $_SESSION["save_properties"] as $key ) : ?>
                                                                    <?php if( $key['property_id'] == $active->StandardFields->ListingKey ) : $property_saved = TRUE;break; endif; ?>
                                                                <?php endforeach; ?>
                                                            <?php else : $property_saved = FALSE; endif;?>

                                                            <a href="javascript:void(0)" data-pro-id="<?=$active->StandardFields->ListingKey?>" class="save-favorate" title="Save Property" >
                                                                <span id="isSaveFavorate_<?= $active->StandardFields->ListingKey; ?>">
                                                                    <?php echo ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>" ;?>
                                                                </span>
                                                            </a>
                                                        </p>
                                                    </div>    <!-- <i class="fa fa-heart-o"></i> -->
                                                </div>
                                                
                                                <p><i class="fa fa-map-marker"></i> 
                                                <?php
                                                    $mystring = $active->StandardFields->City;
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);

                                                    if($pos === false)
                                                        echo $active->StandardFields->City . " " . $active->StandardFields->StateOrProvince . ", " . $active->StandardFields->PostalCode;
                                                    else
                                                        echo $active->StandardFields->PostalCity . " " . $active->StandardFields->StateOrProvince . ", " . $active->StandardFields->PostalCode;
                                                ?>
                                               </i></p>
                                                <p class="search-property-location"><i class="fa fa-usd"></i> <?=number_format($active->StandardFields->CurrentPrice);?>
                                                <ul class="list-inline search-property-specs">
                                                    <?php
                                                            if($active->StandardFields->BedsTotal && is_numeric($active->StandardFields->BedsTotal)) {
                                                        ?>
                                                        <li><i class="fa fa-bed"></i> <?=$active->StandardFields->BedsTotal?> Bed</li>
                                                        <?php
                                                            }
                                                            if($active->StandardFields->BathsTotal && is_numeric($active->StandardFields->BathsTotal)) {
                                                        ?>
                                                        <li><i class="icon-toilet"></i> <?=$active->StandardFields->BathsTotal?> Bath</li>
                                                        <?php
                                                            }
                                                        ?>

                                                        
                                                     <?php
                                                            if(!empty($active->StandardFields->BuildingAreaTotal) && ($active->StandardFields->BuildingAreaTotal != "0")   && is_numeric($active->StandardFields->BuildingAreaTotal)) {?>

                                                                <li class="lot-item"><?=number_format($active->StandardFields->BuildingAreaTotal)?> sqft </li>

                                                            <?php } elseif(!empty($active->StandardFields->LotSizeArea) && ($active->StandardFields->LotSizeArea != "0")   && is_numeric($active->StandardFields->LotSizeArea)) {
                                                                   
                                                                if(!empty($active->StandardFields->LotSizeUnits) && ($active->StandardFields->LotSizeUnits) === "Acres"){?>

                                                                    <li class="lot-item">lot size area: <?=number_format($active->StandardFields->LotSizeArea, 2, '.', ',' )?> </li>

                                                                <?php } else {?>

                                                                    <li class="lot-item">lot size area: <?=number_format($active->StandardFields->LotSizeArea)?> </li>

                                                                <?php }?>

                                                            <?php } elseif(!empty($active->StandardFields->LotSizeSquareFeet) && ($active->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($active->StandardFields->LotSizeSquareFeet)) {?>

                                                                    <li class="lot-item">lot size area: <?=number_format($active->StandardFields->LotSizeSquareFeet)?></li> 

                                                             <?php } elseif(!empty($active->StandardFields->LotSizeAcres) && ($active->StandardFields->LotSizeAcres != "0")   && is_numeric($active->StandardFields->LotSizeAcres)) {?>

                                                                    <li class="lot-item">lot size area: <?=number_format($active->StandardFields->LotSizeAcres,2 ,'.',',')?></li>
                                                                    
                                                             <?php } elseif(!empty($active->StandardFields->LotSizeDimensions) && ($active->StandardFields->LotSizeDimensions != "0")   && ($active->StandardFields->LotSizeDimensions != "********")) {?>

                                                                            <li class="lot-item">lot size area: <?=$active->StandardFields->LotSizeDimensions?></li>
                                                            <?php } else {?>
                                                                            <li class="lot-item">lot size area: N/A</li>
                                                            <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                            <?php
                                    }

                                } else {

                                     if(!empty($new_listings)) {

                                        foreach($new_listings as $new_listing) {?>

                                            <div class="col-md-4 col-sm-6">
                                                <div class="search-listing-item">
                                                    <div class="search-property-image">
                                                    <?php if($is_capture_leads) { ?>
                                                        <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                            <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $new_listing->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                        <?php } else { ?>
                                                            <a href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey; ?>">
                                                        <?php } if(isset($new_listing->Photos->Uri300)) { ?>
                                                                <img src="<?=$new_listing->Photos->Uri300?>" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } else { ?>
                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } ?>
                                                            </a>
                                                    <?php } else { ?>
                                                        <a href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey; ?>">
                                                        <?php if(isset($new_listing->Photos->Uri300)) { ?>
                                                            <img src="<?=$new_listing->Photos->Uri300?>" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } else { ?>
                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } ?>
                                                        </a>
                                                    <?php } ?>
                                                    </div>
                                                    <div style="top:15px;" class="property-listing-status">
                                                       <?php
                                                            if(isset($new_listing->StandardFields->MlsStatus)){?>
                                                                <?php echo $new_listing->StandardFields->MlsStatus;?>
                                                        <?php  } ?> 
                                                    </div>
                                                    <div class="search-listing-type">
                                                        <?php
                                                            if(isset($new_listing->StandardFields->PropertyClass)){?>
                                                                <?php echo $new_listing->StandardFields->PropertyClass;?>
                                                        <?php  } ?> 
                                                    </div>
                                                    <div class="search-listing-title">
                                                        <div class="col-md-9 col-xs-9 padding-0">
                                                            <p>
                                                            <?php if($is_capture_leads) { ?>
                                                                <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                                    <a href="#" class="featured_listing" data-toggle="modal" data-target="#modalLogin" data-listingId="<?php echo $new_listing->StandardFields->ListingKey; ?>" data-propertyType="other_property">
                                                                <?php } else { ?>
                                                                    <a href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey; ?>">
                                                                <?php } echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>
                                                                    </a>
                                                            <?php } else { ?>
                                                                <a href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey; ?>">
                                                                    <?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>
                                                                </a>
                                                            <?php } ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-3 col-xs-3">
                                                            <p>
                                                                <?php  $property_saved = FALSE; if( isset( $_SESSION["save_properties"] ) ) : ?>
                                                                    <?php foreach( $_SESSION["save_properties"] as $key ) : ?>
                                                                        <?php if( $key['property_id'] == $new_listing->StandardFields->ListingKey ) : $property_saved = TRUE;break; endif; ?>
                                                                    <?php endforeach; ?>
                                                                <?php else : $property_saved = FALSE; endif;?>

                                                                <a href="javascript:void(0)" data-pro-id="<?=$new_listing->StandardFields->ListingKey;?>" class="save-favorate" title="Save Property" >
                                                                    <span id="isSaveFavorate_<?= $new_listing->StandardFields->ListingKey; ?>">
                                                                        <?php echo ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>" ;?>
                                                                    </span>
                                                                </a>
                                                            </p>
                                                        </div>    <!-- <i class="fa fa-heart-o"></i> -->
                                                    </div>
                                                    
                                                    <p><i class="fa fa-map-marker"></i> 
                                                    <?php
                                                        $mystring = $new_listing->StandardFields->City;
                                                        $findme   = '*';
                                                        $pos = strpos($mystring, $findme);

                                                        if($pos === false)
                                                            echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                        else
                                                            echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                    ?>
                                                   </i></p>

                                                    <p class="search-property-location"><i class="fa fa-usd"></i>  $<?=number_format($new_listing->StandardFields->CurrentPrice);?>
                                                    <ul class="list-inline search-property-specs">
                                                        <?php
                                                                if($new_listing->StandardFields->BedsTotal && is_numeric($new_listing->StandardFields->BedsTotal)) {
                                                            ?>
                                                            <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                                                            <?php
                                                                }
                                                                if($new_listing->StandardFields->BathsTotal && is_numeric($new_listing->StandardFields->BathsTotal)) {
                                                            ?>
                                                            <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>
                                                            <?php
                                                                }
                                                            ?>
                                                    </ul>
                                                </div>
                                            </div>
                                <?php   }
                            }
                            }?>

                             <?php
                            if(isset($active_listings_total)) {
                                if($active_listings_total > 25) { ?>
                                    <div class="pagination-area">
                                        <?php echo $pagination;?>
                                    </div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                </div>