 
 <div class="filter-panel">
    <div class="filter-tab">
        <div class="property-submit-question">
            <h4 class="text-center">Have a question?</h4>
            <hr>
            <div class="question-mess" ></div>  <br/>      
            <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" value="" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" value="" name="email" class="form-control">
                </div>
                 <div class="form-group">
                    <label for="">Phone</label>
                    <input type="text" value="" name="phone" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Message</label>
                    <textarea id="" cols="30" rows="5" value="" name="message" class="form-control" required></textarea>
                </div>
                <button class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
            </form> 
        </div>
    </div>
</div>