<div class="col-md-3 col-sm-3 invisible-xs">
   <div class="filter-tab">
            <div class="property-agent-info">
                <div class="agent-image">
                    <?php if( isset($user_info->agent_photo) AND !empty($user_info->agent_photo)) { ?>

                        <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/photo/<?php echo $user_info->agent_photo ?>" class="img-thumbnail" width="400">
                    <?php } else { ?>

                         <?php if (isset($account_info->Images[0]->Uri) AND !empty($account_info->Images[0]->Uri)){?>
                            <img src="<?=$account_info->Images[0]->Uri?>" class="img-thumbnail" width="400"> 
                         <?php } else { ?>
                            <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/no-profile-img.gif" alt="No Profile Image">
                         <?php } ?>  

                    <?php } ?>  
                </div>
                 <h4 class="agent-name">
                    <?php echo isset($user_info->first_name) ? $user_info->first_name : $account_info->FirstName;?> 
                    <?php echo isset($user_info->last_name) ? $user_info->last_name : $account_info->LastName;?> 
            </h4>
           <p>

               <?php
                //Office Phone
                 if(isset($user_info->phone) AND !empty($user_info->phone)){?>

                    <i class="fa fa-phone-square"></i> Office: <?= $user_info->phone; ?><br/>

                 <?php }else{?>

                  <?php   if(isset($account_info->Phones)) {
                    foreach($account_info->Phones as $phone) {
                        if($phone->Type == 'Office'){
                            echo "<i class='fa fa-phone-square'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                        }
                        
                    }
                }

                 }
                ?>


                <?php
                    //Mobile phone
                 if(isset($user_info->mobile) AND !empty($user_info->mobile)){?>

                    <i class="fa fa-2x fa-mobile"></i> Mobile: <?= $user_info->mobile; ?><br/>

                 <?php }else{?>

                  <?php   
                      if(isset($account_info->Phones )) {
                        foreach($account_info->Phones as $phone) {
                            if($phone->Type == 'Mobile'){
                                echo "<i class='fa fa-2x fa-mobile'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                            }
                            
                        }
                    }
                 }
                ?>


                <?php
                    //FAX
                   if(isset($account_info->Phones )) {
                    foreach($account_info->Phones as $phone) {
                        if($phone->Type == 'Fax'){
                            echo "<i class='fa fa-fax'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                        }
                        
                    }
                }?>
               
           </p>
            <p><i class="fa fa-envelope"></i> : <a href="mailto:<?php echo isset($user_info->email) ? $user_info->email  : $account_info->Emails[0]->Address;?>">Email Me</a></p>
            </div>
        </div>
    <?php
        if($page == "property") { ?>
            <div class="filter-tab">
                <h4 class="text-center">Share this property to:</h4>
                <hr>
                 <!-- AddToAny BEGIN -->
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_linkedin"></a>
                    <a class="a2a_button_google_plus"></a>
                    <a class="a2a_button_email"></a>
                </div>
                <!-- AddToAny END -->
               <!--  <ul class="list-inline property-social-share">
                    <li><span class='st_facebook_large' displayText='Facebook'></span></li>
                    <li><span class='st_twitter_large' displayText='Tweet'></span></li>
                    <li><span class='st_linkedin_large' displayText='LinkedIn'></span></li>
                    <li><span class='st_googleplus_large' displayText='Google +'></span></li>
                    <li><span class='st_email_large' displayText='Email'></span></li>
                </ul> -->
            </div>
    <?php
        }

        if($page != "contact") { ?>
            <div class="filter-tab">
                <div class="property-submit-question">
                    <h4 class="text-center">Have a question?</h4>
                    <hr>
                    <div class="question-mess" ></div>  <br/>                          
                    <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" name="email" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="phone" name="phone" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Message</label>
                            <textarea name="message" id="" value="" cols="30" rows="5" class="form-control" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
                    </form>
                </div>
            </div>
    <?php 
        } else { ?>
            <div class="filter-tab">
                <h4>You can also contact me through these social media accounts</h4>
                <hr>
                 <?php
                    if(isset($socialmedia_link['facebook']) AND !empty($socialmedia_link['facebook'])) {
                        if(preg_match('/http|https/', $socialmedia_link['facebook'])) { 
                            $fblink = $socialmedia_link['facebook'];
                        } else {
                            $fblink = "http://".$socialmedia_link['facebook'];
                        }
                    } else {
                        $fblink = "https://www.facebook.com/AgentSquared/";
                    }

                    if(isset($socialmedia_link['twitter']) AND !empty($socialmedia_link['twitter'])) {
                        if(preg_match('/http|https/', $socialmedia_link['twitter'])) {
                            $twittlink = $socialmedia_link['twitter'];
                        } else {
                            $twittlink = "http://".$socialmedia_link['twitter'];
                        }
                    } else {
                        $twittlink = "https://twitter.com/agentsquared";
                    }

                    if(isset($socialmedia_link['googleplus']) AND !empty($socialmedia_link['googleplus'])) {
                        if(preg_match('/http|https/', $socialmedia_link['googleplus'])) {
                            $gplink = $socialmedia_link['googleplus'];
                        } else {
                            $gplink = "http://".$socialmedia_link['googleplus'];
                        }
                    } else {
                        $gplink = "https://plus.google.com/+Agentsquared";
                    }

                    if(isset($socialmedia_link['linkedin']) AND !empty($socialmedia_link['linkedin'])) {
                        if(preg_match('/http|https/', $socialmedia_link['linkedin'])) {
                            $lnlink = $socialmedia_link['linkedin'];
                        } else {
                           $lnlink = "http://".$socialmedia_link['linkedin'];
                        }
                    } else {
                        $lnlink = "https://www.linkedin.com/company/agentsquared";
                    }
                ?>
                <ul class="list-inline property-social-share">
                  <?php if($socialmedia_link['agent_email']) : ?><li><a href="mailto:<?php echo $socialmedia_link['agent_email']; ?>"><i class="fa fa-envelope"></i></a></li><?php endif; ?>
                  <li><a href="<?php echo $fblink; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="<?php echo $twittlink; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="<?php echo $gplink; ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                  <li><a href="<?php echo $lnlink; ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
    <?php
        } 

        if($page == "property") { ?>
            <div class="filter-tab">
                <div class="property-submit-question">
                    <h4 class="text-center">Mortgage Calculator</h4>
                    <hr>
                    <form  method="post" id="mortgageCalculator">
                        <div class="form-group">
                            <label for="sale_price">Price of Home ($<span id="sale_price_label"></span>)</label>
                            <input type="text" name="sale_price" id="sale_price" value="<?php echo $this->session->flashdata('mortgage_price_value'); ?>" class="form-control">
                            <input type="hidden" name="calculator_url" value="<?php echo base_url();?>home/mortgage_calculator">
                        </div>
                        <div class="form-group">
                            <label for="down_percent">Downpayment (<span id="down_percent_label"></span>%)</label>
                            <input type="text" name="down_percent" id="down_percent" value="<?php echo $this->session->flashdata('mortgage_downpayment_value'); ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="mortgage_interest_percent">Annual Interest Rate (<span id="mortgage_interest_percent_label"></span>%)</label>
                            <input type="text" name="mortgage_interest_percent" id="mortgage_interest_percent" value="<?php echo $this->session->flashdata('mortgage_interest_rate_value'); ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="year_term">Loan Term (<span id="year_term_label"></span>  years)</label>
                            <input type="text" name="year_term" id="year_term" value="<?php echo $this->session->flashdata('mortgage_loan_terms_value'); ?>" class="form-control">
                        </div>
                        <button class="btn btn-default btn-block submit-button" id="morg_calc_btn">Calculate <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i></button>
                        <div id="mortgage-result">
                               <?php echo $this->session->flashdata('mortgage_monthly'); ?>
                               <!-- <?php echo $this->session->flashdata('mortgage_monthly_interest'); ?>
                               <?php echo $this->session->flashdata('mortgage_annual_interest'); ?>
                               <?php echo $this->session->flashdata('mortgage_downpayment'); ?>
                               <?php echo $this->session->flashdata('mortgage_amount_financed'); ?> -->
                        </div>
                    </form>
                </div>
            </div>
    <?php
        }

    ?>

</div>
