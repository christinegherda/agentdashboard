<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared Signup'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon"  href="<?php if (isset($branding->favicon) AND !empty($branding->favicon)) {?><?php echo "/assets/upload/favicon/$branding->favicon"?><?php } else { ?><?php echo "/assets/images/default-favicon.png"?><?php } ?>"/>
    <link rel="apple-touch-icon" href="/assets/img/home/apple-touch-icon.png">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css"/> -->
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-theme.min.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/css/landing_page.css">
    <script src="/assets/js/agent-haven/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/fonts/fontello/css/fontello.css">
    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
</head>
<body class="sign-up-page">

