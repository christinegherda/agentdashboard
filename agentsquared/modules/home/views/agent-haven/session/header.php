<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <?php
        $google_verification = $this->config->item('google-verification');
        echo $google_verification;  
    ?>

    <?php 
        $description = isset($description) ? $description : "" ;
        $broker = isset($broker) ? $broker : "" ;
        $broker_number = isset($broker_number) ? $broker_number : "" ;
    ?>
    <title><?= isset($title) ? $title : "Home | Agentsquared"; ?></title>
    <meta name ="description" content="<?php echo $description .'... '.'Listed by:'.$broker .','.' '.'Phone:'.$broker_number ?>"/>
     
    <meta property="fb:app_id" content="867016703396837" />
    <meta property="og:url" content="<?php echo base_url()?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($title) ? $title : "" ?>" />
    <meta property="og:description" content="<?php echo isset($description) ? $description : "" ?>" />
    <meta property="og:image" content="<?php echo isset($image) ? $image : ""?>" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon"  href="<?php if (isset($user_info->favicon) AND !empty($user_info->favicon)) {?><?php echo AGENT_DASHBOARD_URL . "assets/upload/favicon/$user_info->favicon"?><?php } else { ?><?php echo AGENT_DASHBOARD_URL . "assets/images/default-favicon.png"?><?php } ?>"/>

    <!-- Stylesheets -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:400,300,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/agent-haven/home-compressed.css.php" />
    <script src="<?=base_url()?>assets/js/agent-haven/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
</head>
<body>

    <!-- Modal Pricing -->
    <div class="modal fade" id="modalPricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">You have <?=($this->config->item("trial")) ? 15 - $this->config->item("left") : "15"; ?> FREE trial day(s) left!</h4>
                    <?php if($this->config->item('left') <= 15) { ?>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php } ?>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="price-item">
                                <h4 class="price-title">Instant IDX Website</h4>
                                <div class="price-desc">
                                    <p><sup>$</sup><?php echo ($this->config->item('price')) ? $this->config->item('price') : "99"; ?></p>
                                    <small>per monthly</small>
                                </div>
                                <a href="javascript:void(0)" data-type="monthly" data-price="<?php echo $this->config->item('price'); ?>" id="purchase-idx-monthly" class="price-text purchase-idx">Purchase</a>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <p class="or-text">OR</p>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="price-item">
                                <h4 class="price-title">IDX Annual</h4>
                                <div class="price-desc">
                                    <p><sup>$</sup>990</p>
                                    <small>per year</small>
                                </div>
                                <a href="javascript:void(0)" data-type="annual" data-price="990" id="purchase-idx-yearly" class="price-text purchase-idx">Purchase</a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

      <!-- Modal Payment -->
      <div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-label" id="myModalLabel">Checkout</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <p class="modal-text">Review Your Order:</p>
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Product</th>
                          <th>Amount</th>
                          <th>Sub-Total</th>
                          <!-- <th></th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><span id="product-name"></span></td>
                          <td>$<span id="product-amount"></span></td>
                          <td>$<span class="product-sub-amount"></span></td>
                          <!-- <td><a href="#"><i class="fa fa-close"></i></a></td> -->
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-md-12 text-right">
                  <p class="text-total">Total:$<span class="product-sub-amount"></span></p>
                </div>
                <div class="col-md-12">
                  <div class="border-gray"></div>
                </div>
                <div class="col-md-6">              
                  <div class="row">
                    <div class="col-md-5">
                      <p class="modal-text">Payment Method:</p>
                    </div>
                    <div class="col-md-6">
                      <img src="<?= base_url()?>assets/images/dashboard/payment_method.png" class="cc-accepted">
                    </div>
                  </div>
                  <div class="cc-detail">
                    <h3>Credit Card Details:</h3> </br>
                    <form class="form-horizontal trial_stripe_sbt" id="trial_stripe_sbt" action="" method="POST">
                      <div class="col-md-12"><h3><div class="col-md-12 payment-errors label label-danger"></div></h3><br></div>
                      <div class="col-md-12"><h3><div class="col-md-12 payment-message label label-success"></div></h3><br></div>

                      <input type="hidden" name="product_amount" value="" class="product-mount" >
                      <input type="hidden" name="product_type" value="" class="product-type" >
                      <input type="hidden" name="property_id" value="" class="property_id" >

                      <div class="row">                   
                        <div class="col-md-12 row mb-15px">
                          <div class="form-group">
                            <label for="cardnumber" class="col-md-4">Card number:</label>
                            <div class="col-md-8 no-padding">
                              <input type="input" class="form-control" size="20" id="" data-stripe="number" placeholder="Card Number" required >
                            </div>
                          </div>
                        </div> 

                        <div class="col-md-12 row mb-15px">
                          <div class="form-group">
                            <label for="cardnumber" class="col-md-4">CVV code:</label>
                            <div class="col-md-8 no-padding">
                              <input type="input" class="form-control" size="20" id="" data-stripe="cvc" placeholder="CVV" required >
                            </div>
                          </div>
                        </div>    

                        <div class="col-md-12 row mb-15px">
                          <div class="form-group">
                            <label for="expirationdate" class="col-md-4">Expiration date:</label>
                            <div class="col-md-2 no-padding">
                              <input type="input" class="form-control" size="20" id="" data-stripe="exp-month" placeholder="MM" required >
                            </div>
                            <div class="col-md-2 no-padding">
                              <input type="input" class="form-control" size="20" id="" data-stripe="exp-year" placeholder="YYYY" required >
                            </div>                        
                          </div>
                        </div>

                      </div>
                    
                  </div>
                </div>
                <div class="col-md-6 text-center mclear">
                  <h1 class="payment-total">$<span class="product-sub-amount"></span></h1>
                  <p class="payment-total-note1">Total is inclusive of VAT</p>
                  <p class="text-left payment-total-note2">
                    By clicking on the 'Place Order' button at the end of the order process, you are consenting to be bound by our terms and conditions contained in these Terms and Conditions and appearing anywhere on AgentSquared website.
                  </p>
                  <!-- <a href="#" class="btn btn-border-green">Place Order</a> -->
                  <button type="submit" class="btn btn-border-green" id="trial_pay_now_btn"> Place Order</button>
                </div>
                </form>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <ul class="list-link">
                      <li><a href="#" target="_blank">Disclaimer</a></li>
                      <li><a href="#" target="_blank">Privacy Policy</a></li>
                      <li><a href="#" target="_blank">Terms &amp; Conditions</a></li>
                    </ul>
                  </div>
                  <div class="col-md-6 text-right">
                    <p class="powered-by">Powered By: <img src="<?= base_url()?>assets/images/dashboard/stripe-logo.png"></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <nav class="navbar navbar-fixed-top main-navbar home-navbar" role="navigation">
        <div class="container">
            <div class="col-md-12">
                <p class="text-right registration-nav">
                    <?php
                        if($this->session->userdata("customer_id")) { ?>
                            <span class="userlog">Welcome <?php echo ($this->session->userdata("customer_email")) ? $this->session->userdata("customer_email") : "User" ;?>!</span> 
                            <span class="nav-link">
                                <a href="<?php echo site_url("customer-dashboard"); ?>">Dashboard</a> | <a href="<?php echo site_url("home/logout"); ?>"> Logout</a>
                            </span>
                    <?php
                        } else { ?>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#modalLogin">Login</a> <!-- <span>|</span> <a href="<?=base_url();?>home/signup">Sign Up</a>   --> 
                    <?php 
                        }
                    ?>
                </p>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=base_url()?>home">
                <?php if(!empty($user_info->logo)){?>

                    <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/logo/<?php echo $user_info->logo;?>" alt="" width="200px" height="65px">

                <?php } else {?>

                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/agent-logo-2.jpg" alt="" width="200px" height="65px">
                <?php }?>
                </a>
                <p class="tagline"><?php echo !empty($user_info->tag_line) ? $user_info->tag_line : "Your home away from home";?></p>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <!-- Default menu Area -->  
                <?php if(isset($default_menu_data) && !empty($default_menu_data)){
                     echo $default_menu_data;
                }?>
            </div>
        </div>
    </nav>
    
    <!-- Custom menu Area -->
    <?php if(isset($custom_menu_data) && !empty($custom_menu_data)){?>
        <div id="custom-navbar" class="sub-navbar-menu hide-menu">
            <div class="container">
                <div class="subnavbar-menu-btn">
                    <a href="#">More Pages <i class="fa fa-plus"></i></a>   
                </div>

                 <?php 
                    echo $custom_menu_data;
                 ?>
                 
            </div>
        </div>
    <?php  }?>
