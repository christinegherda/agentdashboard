<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
     <?php
        $google_verification = $this->config->item('google-verification');
        echo $google_verification;  
    ?>

    <?php if($this->uri->segment(2) != "page" && $this->uri->segment(1) != "listings" && $this->uri->segment(1) != "property-details" && $this->uri->segment(1) != "other-property-details"){?>
    
        <title><?php echo isset($title)  ? $title : "Home | Agentsquared" ?></title>
        <meta name ="description" content="<?php echo isset($description) ? $description : ""   ?>"/>
    <?php } ?>

     <?php if($this->uri->segment(2) == "page"){?>
        <title><?php echo isset($page['title'])  ? $page['title'] : "Page Detail | Agentsquared" ?></title>
    <?php } ?>

     <?php if($this->uri->segment(1) == "listings"){?>
        <title><?php echo ucwords($_GET['Search'])." Real Estate | ". ucwords($_GET['Search']). " Home For Sale";?></title>
    <?php } ?>

    <?php if($this->uri->segment(1) == "property-details"){
        if(!empty($property_standard_info["data"])){
            $details = $property_standard_info["data"];
    ?>
            <title><?= isset($details["UnparsedAddress"]) ? $details["UnparsedAddress"]  :"";?> | MLS #<?= isset($details["ListingId"]) ? $details["ListingId"]  :"";?> | AgentSquared</title>
            <meta name ="description" content="View <?=isset($details["PhotosCount"]) ? $details["PhotosCount"] : "undefined";?> photos of this $<?=($details["CurrentPrice"] != "********") ? number_format($details["CurrentPrice"]) :"undefined";?>, <?=($details["BedsTotal"] != "********") ? $details["BedsTotal"] : "undefined";?> bed, <?=($details["BathsTotal"] != "********") ? $details["BathsTotal"] : "undefined";?> bath, <?=($details["LotSizeArea"] != "********") ? $details["LotSizeArea"] : "undefined";?> sqft <?=isset($details["PropertySubType"]) ? strtolower($details["PropertySubType"]) : "undefined";?> located at <?=isset($details["UnparsedAddress"]) ? $details["UnparsedAddress"] : "undefined";?> built in <?=($details["YearBuilt"] != "********") ? $details["YearBuilt"] : "undefined";?>. MLS # <?=isset($details["ListingId"]) ? $details["ListingId"] : "undefined";?>."/>
    <?php }
    }?>

    <?php if($this->uri->segment(1) == "other-property-details") {
        if(!empty($property_standard_info["data"])){
            $details = $property_standard_info["data"];
        ?>

        <title><?= isset($details['UnparsedAddress']) ? $details['UnparsedAddress'] : "";?> | MLS #<?= isset($details['ListingId']) ? $details['ListingId'] :"";?> | AgentSquared</title>
        <meta name ="description" content="View <?=isset($details['PhotosCount']) ? $details['PhotosCount'] : "undefined";?> photos of this $<?=($details['CurrentPrice'] != "********") ? number_format($details['CurrentPrice']) : "undefined";?>, <?=($details['BedsTotal'] != "********") ? $details['BedsTotal'] : "undefined";?> bed, <?=($details['BathsTotal'] != "********") ? $details['BathsTotal'] : "undefined";?> bath, <?=($details['LotSizeArea'] != "********") ? $details['LotSizeArea'] : "undefined";?> sqft <?=strtolower($details['PropertySubType'])?> located at <?=$details['UnparsedAddress']?> built in <?=($details['YearBuilt'] != "********") ? $details['YearBuilt'] : "undefined";?>. MLS # <?=isset($details['ListingId']) ? $details['ListingId'] : "undefined";?>."/>

     <?php }
     }?>

    <?php if($this->uri->segment(1) == "property-details") {
        if(!empty($property_standard_info["data"])){
            $details = $property_standard_info["data"];
    ?>

                <meta property="fb:app_id" content="867016703396837" />
                <meta property="og:url" content="<?php echo base_url()?>property-details/<?=$details["ListingKey"]?>" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="<?php echo isset($details["UnparsedFirstLineAddress"]) ? $details["UnparsedFirstLineAddress"] : "" ?>" />
                <meta property="og:description" content="<?php echo isset($details["PublicRemarks"]) ? str_limit($details["PublicRemarks"],197) : "" ?>" />
                <meta property="og:image" content="<?php echo isset($details["Photos"][0]["Uri1024"]) ? $details["Photos"][0]["Uri1024"] : ''?>" />
                
                <!-- Schema.org markup for Google+ -->
                <meta itemprop="name" content="<?php echo isset($details["UnparsedFirstLineAddress"]) ? $details["UnparsedFirstLineAddress"] : "" ?>" />
                <meta itemprop="description" content="<?php echo isset($details["PublicRemarks"]) ? str_limit($details["PublicRemarks"],197) : "" ?>" />
                <meta itemprop="image" content="<?php echo isset($details["Photos"][0]["Uri1024"]) ? $details["Photos"][0]["Uri1024"] : "" ?>">

                <!-- Twitter Card data -->
                <meta name="twitter:card" value="summary">
                <meta name="twitter:title" content="<?php echo isset($details["UnparsedFirstLineAddress"]) ? $details["UnparsedFirstLineAddress"] : "" ?>" />
                <meta name="twitter:description" content="<?php echo isset($details["PublicRemarks"]) ? str_limit($details["PublicRemarks"],197) : "" ?>" />
                <meta name="twitter:image" content="<?php echo isset($details["Photos"][0]["Uri1024"]) ? $details["Photos"][0]["Uri1024"] : ''?>">
                <meta name="twitter:url" content="<?php echo base_url()?>property-details/<?=$details["ListingKey"]?>" />
    <?php }
    } ?>

    <?php if($this->uri->segment(1) == "other-property-details") {
        if(!empty($property_standard_info["data"])){
            $details = $property_standard_info["data"];
    ?>

        <meta property="fb:app_id" content="867016703396837" />
        <meta property="og:url" content="<?php echo base_url()?>home/<?php echo $this->uri->segment(2);?>?<?php echo http_build_query($_GET);?>" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo isset($details['UnparsedFirstLineAddress']) ? $details['UnparsedFirstLineAddress'] : "" ?>" />
        <meta property="description" content="<?php echo isset($details['PublicRemarks']) ? str_limit($details['PublicRemarks'],197) : "" ?>" />
        <meta property="og:image" content="<?php echo isset($photos[0]['Uri1024']) ? $photos[0]['Uri1024'] : "" ?>" />
        
        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="<?php echo isset($details['UnparsedFirstLineAddress']) ? $details['UnparsedFirstLineAddress'] : "" ?>" />
        <meta itemprop="description" content="<?php echo isset($details['PublicRemarks']) ? str_limit($details['PublicRemarks'],197) : "" ?>" />
        <meta itemprop="image" content="<?php echo isset($photos[0]['Uri1024']) ? $photos[0]['Uri1024'] : "" ?>">

        <!-- Twitter Card data -->
       <meta name="twitter:card" value="summary">
        <meta name="twitter:title" content="<?php echo isset($details['UnparsedFirstLineAddress']) ? $details['UnparsedFirstLineAddress'] : "" ?>" />
        <meta name="twitter:description" content="<?php echo isset($details['PublicRemarks']) ? str_limit($details['PublicRemarks'],197) : "" ?>" />
        <meta name="twitter:image" content="<?php echo isset($photos[0]['Uri1024']) ? $photos[0]['Uri1024'] : "" ?>">
        <meta name="twitter:url" content="<?php echo base_url()?>home/<?php echo $this->uri->segment(2);?>?<?php echo http_build_query($_GET);?>">

    <?php }
    } ?>

    <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="icon" type="image/x-icon"  href="<?php if (isset($user_info->favicon) AND !empty($user_info->favicon)) {?><?php echo AGENT_DASHBOARD_URL . "assets/upload/favicon/$user_info->favicon"?><?php } else { ?><?php echo AGENT_DASHBOARD_URL . "assets/images/default-favicon.png"?><?php } ?>"/>

    <!-- Steylesheet -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,300,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/agent-haven/font.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/bootstrap/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/agent-haven/home-layout.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/fontello/css/fontello.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/agent-haven/home-vegas.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/agent-haven/selectize.bootstrap3.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/agent-haven/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/agent-haven/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/agent-haven/button.css" />
    <script src="<?=base_url()?>assets/js/agent-haven/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <?php 
        if(isset($css)) :
            foreach($css as $key => $val ) :  ?>
                <link rel="stylesheet" href="<?= base_url()?>assets/css/agent-haven/<?php echo $val?>" type="text/css">
    <?php   endforeach;
        endif; 
    ?>

    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>

     <?php if ($this->uri->segment(1) == "listings" || $this->uri->segment(2) == "listings" || $this->uri->segment(1) == "find-a-home" || $this->uri->segment(2) == "nearby_listings"): ?>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCrMzetdv25NdoUlfkVQuBVUcRw4_NGXcc&libraries=places&callback=initialize"></script>
    <?php elseif ($this->uri->segment(1) == "other-property-details" || $this->uri->segment(1) == "property-details" || $this->uri->segment(1) == "contact"): ?>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCrMzetdv25NdoUlfkVQuBVUcRw4_NGXcc&libraries=places"></script>
    <?php endif ?>

</head>
<body>

    <!-- Modal Pricing -->
    <!-- <div class="modal fade" id="modalPricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">You have <?=($this->config->item("trial")) ? 15 - $this->config->item("left") : "15"; ?> FREE trial day(s) left!</h4>
                    <?php if($this->config->item('left') <= 15) { ?>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php } ?>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="price-item">
                                <h4 class="price-title">Instant IDX Website</h4>
                                <div class="price-desc">
                                    <p><sup>$</sup><?php echo ($this->config->item('price')) ? $this->config->item('price') : "99"; ?></p>
                                    <small>per monthly</small>
                                </div>
                                <a href="javascript:void(0)" data-type="monthly" data-price="<?php echo $this->config->item('price'); ?>" id="purchase-idx-monthly" class="price-text purchase-idx">Purchase</a>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <p class="or-text">OR</p>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="price-item">
                                <h4 class="price-title">IDX Annual</h4>
                                <div class="price-desc">
                                    <p><sup>$</sup>990</p>
                                    <small>per year</small>
                                </div>
                                <a href="javascript:void(0)" data-type="annual" data-price="990" id="purchase-idx-yearly" class="price-text purchase-idx">Purchase</a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    
    <nav class="navbar navbar-fixed-top main-navbar page-navbar" role="navigation">
        <div class="container">
            <div class="col-md-12">
                <p class="text-right registration-nav">
                    <?php
                        if($this->session->userdata("customer_id")) { ?>
                            <span class="userlog">Welcome <?php echo ($this->session->userdata("customer_email")) ? $this->session->userdata("customer_email") : "User" ;?>!</span> 
                            <span class="nav-link">
                                <a href="<?php echo site_url("customer-dashboard"); ?>">Dashboard</a> | <a href="<?php echo site_url("logout"); ?>"> Logout</a>
                            </span>
                    <?php
                        } else { ?>
                            <a href="<?=base_url();?>home/login">Login</a> <span>|</span> <a href="<?=base_url();?>home/signup">Sign Up</a>    
                    <?php
                        }
                    ?>
                </p>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=base_url()?>home">
                    <?php if(!empty($user_info->logo)){?>

                        <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/logo/<?php echo $user_info->logo;?>" alt="" width="200px" height="65px">

                    <?php } else {?>

                        <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/agent-logo-2.jpg" alt="" width="200px" height="65px">
                    <?php }?>
                </a>
                 <p class="tagline"><?php echo !empty($user_info->tag_line) ? $user_info->tag_line : "Your home away from home";;?></p>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <!-- Default menu Area -->  
                <?php if(isset($default_menu_data) && !empty($default_menu_data)){
                     echo $default_menu_data;
                }?>
            </div>
        </div>
    </nav>
    
    <!-- Custom menu Area -->
    <?php if(isset($custom_menu_data) && !empty($custom_menu_data)){?>
        <div id="custom-navbar" class="sub-navbar-menu hide-menu">
            <div class="container">
                <div class="subnavbar-menu-btn">
                    <a href="#">More Pages <i class="fa fa-plus"></i></a>   
                </div>

                 <?php 
                    echo $custom_menu_data;
                 ?>
                 
            </div>
        </div>
    <?php  }?>
