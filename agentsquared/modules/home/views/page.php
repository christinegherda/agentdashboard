<?php 
    //Header Area
    if($page != "customer_login_form" && $page != "customer_signup_form") {
        $this->load->view($theme."/session/".$header);
    }
    
    // Content Area 
    $this->load->view($theme."/pages/".$page);

    // Footer Area
    if($page != "customer_login_form" && $page != "customer_signup_form") {
        $this->load->view($theme."/session/footer");
    }
?>
