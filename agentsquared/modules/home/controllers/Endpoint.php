<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Endpoint extends MX_Controller {


	function __construct() {

 		parent::__construct();

 		$userData = $this->session->userdata('user_id');

        if(empty($userData)) {
            $this->session->set_flashdata('flash_data', 'You don\'t have access!');
            redirect('login/auth/login', 'refresh');
        }

 		$this->load->model("home_model");
 	} 

	public function default_active_listings() { 

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = Modules::run('dsl/getNewListingsProxy');
		$data['active_listings'] = Modules::run('dsl/getAgentActivePropertyListings', 4);
		$data['office_listings'] = Modules::run('dsl/getOfficeListings');

		if(!empty($data['active_listings'][0]->StandardFields)){

				$lat = $data['active_listings'][0]->StandardFields->Latitude ;
				$lon = $data['active_listings'][0]->StandardFields->Longitude ;

			} elseif (!empty($data['new_listings'][0]->StandardFields)){

				$lat = $data['new_listings'][0]->StandardFields->Latitude ;
				$lon = $data['new_listings'][0]->StandardFields->Longitude ;

			} elseif(!empty($data['office_listings'][0]['StandardFields'])){

				$lat = $data['office_listings'][0]['StandardFields']['Latitude'];
				$lon = $data['office_listings'][0]['StandardFields']['Longitude'];

			} else {

				$lat = "";
				$lon = "";
			}

		$data['nearby_listings']  = Modules::run('dsl/getNearbyListingsProxy', array("lat" => $lat,  "lon" => $lon));

		$this->load->view($theme.'/pages/home_view/default-active-listings',$data);
	
	}

	public function custom_active_listings() { 

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;
		$data["active_title"] = $this->home_model->get_active_title();
		$data["is_active_featured"] = $this->home_model->get_active_featured();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = Modules::run('dsl/getNewListingsProxy');
		$data['active_listings'] = Modules::run('dsl/getAgentActivePropertyListings', 4);
		$data['office_listings'] = Modules::run('dsl/getOfficeListings');

		if(!empty($data['active_listings'][0]->StandardFields)){

				$lat = $data['active_listings'][0]->StandardFields->Latitude ;
				$lon = $data['active_listings'][0]->StandardFields->Longitude ;

			} elseif (!empty($data['new_listings'][0]->StandardFields)){

				$lat = $data['new_listings'][0]->StandardFields->Latitude ;
				$lon = $data['new_listings'][0]->StandardFields->Longitude ;

			} elseif(!empty($data['office_listings'][0]['StandardFields'])){

				$lat = $data['office_listings'][0]['StandardFields']['Latitude'];
				$lon = $data['office_listings'][0]['StandardFields']['Longitude'];

			} else {

				$lat = "";
				$lon = "";
			}

		$data['nearby_listings']  = Modules::run('dsl/getNearbyListingsProxy', array("lat" => $lat,  "lon" => $lon));

		$this->load->view($theme.'/pages/home_view/default-active-listings',$data);
	
	}

	public function default_nearby_listings() { 

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = Modules::run('dsl/getNewListingsProxy');
		$data['active_listings'] = Modules::run('dsl/getAgentActivePropertyListings', 4);
		$data['office_listings'] = Modules::run('dsl/getOfficeListings');

		if(!empty($data['active_listings'][0]->StandardFields)){

				$lat = $data['active_listings'][0]->StandardFields->Latitude ;
				$lon = $data['active_listings'][0]->StandardFields->Longitude ;

			} elseif (!empty($data['new_listings'][0]->StandardFields)){

				$lat = $data['new_listings'][0]->StandardFields->Latitude ;
				$lon = $data['new_listings'][0]->StandardFields->Longitude ;

			} elseif(!empty($data['office_listings'][0]['StandardFields'])){

				$lat = $data['office_listings'][0]['StandardFields']['Latitude'];
				$lon = $data['office_listings'][0]['StandardFields']['Longitude'];

			} else {

				$lat = "";
				$lon = "";
			}

		$data['nearby_listings'] = Modules::run('dsl/getNearbyListingsProxy', array("lat" => $lat,  "lon" => $lon));

		$this->load->view($theme.'/pages/home_view/default-nearby-listings',$data);
	}

	public function custom_nearby_listings() { 

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["nearby_title"] = $this->home_model->get_nearby_title();
		$data["is_nearby_featured"] = $this->home_model->get_nearby_featured();
		$data['new_listings'] = Modules::run('dsl/getNewListingsProxy');
		$data['active_listings'] = Modules::run('dsl/getAgentActivePropertyListings', 4);
		$data['office_listings'] = Modules::run('dsl/getOfficeListings');

		if(!empty($data['active_listings'][0]->StandardFields)){

				$lat = $data['active_listings'][0]->StandardFields->Latitude ;
				$lon = $data['active_listings'][0]->StandardFields->Longitude ;

			} elseif (!empty($data['new_listings'][0]->StandardFields)){

				$lat = $data['new_listings'][0]->StandardFields->Latitude ;
				$lon = $data['new_listings'][0]->StandardFields->Longitude ;

			} elseif(!empty($data['office_listings'][0]['StandardFields'])){

				$lat = $data['office_listings'][0]['StandardFields']['Latitude'];
				$lon = $data['office_listings'][0]['StandardFields']['Longitude'];

			} else {

				$lat = "";
				$lon = "";
			}

		$data['nearby_listings'] = Modules::run('dsl/getNearbyListingsProxy', array("lat" => $lat,  "lon" => $lon));

		$this->load->view($theme.'/pages/home_view/custom-nearby-listings',$data);
	}

	public function custom_office_listings() { 

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["is_office_featured"] = $this->home_model->get_office_featured();
		$data["office_title"] = $this->home_model->get_office_title();
		$data['office_listings'] = Modules::run('dsl/getOfficeListings');

		$this->load->view($theme.'/pages/home_view/custom-office-listings',$data);
	}

	public function default_new_listings() { 

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = Modules::run('dsl/getNewListingsProxy');

		$this->load->view($theme.'/pages/home_view/default-new-listings',$data);
	}

	public function custom_new_listings() { 

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;
		$data["is_new_featured"] = $this->home_model->get_new_featured();
		$data["new_title"] = $this->home_model->get_new_title();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = Modules::run('dsl/getNewListingsProxy');

		$this->load->view($theme.'/pages/home_view/custom-new-listings',$data);
	}

	public function featured_saved_search() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
 		$theme = $user_info->theme;

	 	$savedSearchFeatured = Modules::run('dsl/getFeaturedAgentSavedSearch');
	 	$data["is_saved_search_featured"] = $savedSearchFeatured;

	 	$dsl = modules::load('dsl/dsl');

		$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));

		$access_token = array(
			'access_token' => $token->access_token
		);

		if(!empty($data["token_checker"])) {
			if(!empty($savedSearchFeatured)){ 
				foreach($savedSearchFeatured as $savedFeatured) {

					$filterStr = "(".$savedFeatured['Filter'].")";
					$filter_url = rawurlencode($filterStr);
					$filter_string = substr($filter_url, 3, -3);

					$endpoint = "spark-listings?_limit=4&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
					$results = $dsl->post_endpoint($endpoint, $access_token);
					$savedFeatured['results'] = json_decode($results, true);
			    } 
		    }
		}
		if(!empty($savedSearchFeatured)){
			if(!empty($savedSearchFeatured[0]["results"]["data"])){
				$data["saved_searches_featured"] = $savedSearchFeatured;
			}
		}

		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$this->load->view($theme.'/pages/home_view/featured-saved-searches', $data);
	}

	public function selected_saved_search() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
 		$theme = $user_info->theme;

		$data["is_saved_search_selected"] = Modules::run('dsl/getSelectedAgentSavedSearch');
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$this->load->view($theme.'/pages/home_view/custom-saved-searches',$data);
	}

	public function sold_properties() {

 		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
 		$theme = $user_info->theme;
 		$data['sold_listings'] = Modules::run('dsl/getAgentSoldPropertyListings', 6);
	 	$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

 		$this->load->view($theme.'/pages/home_view/sold-properties', $data);
 	}
	
}

	