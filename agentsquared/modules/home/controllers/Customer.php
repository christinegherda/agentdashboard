<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Customer extends MX_Controller {

	function __construct() {

		parent::__construct();

		//$this->output->enable_profiler(TRUE);

		$this->load->library('facebook');
		
		$this->load->model("home_model");
		$this->load->model("customer_model", "customer");
	}

	public function customer_dashboard() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "customer_dashboard";
		$data["title"] = "Dashboard";

		$data["default_menu_data"] = Modules::run('menu/get_default_menu_data');
        $data["custom_menu_data"] = Modules::run('menu/get_custom_menu_data');

        $data["user_info"] = $user_info;
		$data["tag_line"] = ($user_info->tag_line) ? $user_info->tag_line : "Your home away from home";
		$data["banner_tagline"] = ($user_info->banner_tagline) ? $user_info->banner_tagline : "Everyone needs a place to call HOME";
		$data["system_info"] = Modules::run('home/getSystemInfo');
		$data["socialmedia_link"] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$save_properties = $this->customer->get_customer_properties();
		$savesearches = $this->customer->get_customer_searches();
		$savesearchesflex = $this->customer->getSaveSearchesFlex();

		$save_searches_flex = ( empty($savesearchesflex) ) ? array() : $savesearchesflex;
        $save_searches = array_merge($savesearches, $save_searches_flex);

		$profile = $this->customer->get_customer_profile();
		$send_email = $this->customer->get_contact_send_email();

		if(!empty($save_properties)) {
			foreach ($save_properties as $properties) {
				 $properties->properties_details = Modules::run('dsl/getListing', $properties->property_id);
				 $properties->property_photos = Modules::run('dsl/getListingPhotos', $properties->property_id);
			}
		}

		$data["save_properties"] = $save_properties;
		$data["save_searches"] = $save_searches;
		$data["profile"] = $profile;
		$data["send_email"] = $send_email;

		$data["token_checker"] = Modules::run('home/token_checker');

		$data["css"] = array("msgBoxLight.css");
		$data["js"] = array("jquery.msgBox.js", "customer.js");
		$data["cdn"] = array("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js");

		$this->load->view("page", $data);
	}

	public function change_password()
	{ 
		if($this->input->is_ajax_request())
		{
			if($_POST)
			{
				$this->form_validation->set_rules("new_password", "New Password", "required|trim|matches[cpassword]");
				$this->form_validation->set_rules("cpassword", "Confirm Password", "required|trim");

				if ($this->form_validation->run() == FALSE)
				{
					$response = array("success" => FALSE ,"errors" => "<span style='color:red'>".validation_errors()."</span>" );
				}
				else
				{
					if( $this->customer->change_password( $this->input->post("new_password") ) )
					{
						$response = array("success" => TRUE ,"message" => "<span style='color:green'>New Password has been successfully changed!</span>" );	
					}
					else
					{
						$response = array("success" => FALSE ,"errors" => "<span style='color:red'>".validation_errors()."</span>" );
					}
					
				}
			}
			exit( json_encode($response) );
		}
	}

	public function delete_customer_property( $id = NULL , $property_id = NULL)
	{ 
		if(empty($id))
		{
			return exit(json_encode(array("success" => FALSE)));
		}

		
		if( $this->customer->delete_customer_property( $id ) )
		{
			
			if( isset($_SESSION["save_properties"] ) )
			{ 
				foreach ($_SESSION["save_properties"] as $session_property ) {
					if( isset($session_property["property_id"]) AND $session_property["property_id"] == $property_id)
					{
						$this->session->unset_userdata( array( $session_property["property_id"] ) );
					}
				}

			} 

			$this->customer->set_new_save_property_session( $this->session->userdata("customer_id") );

			return exit(json_encode(array("success" => TRUE)));
		}

		return exit(json_encode(array("success" => FALSE)));
	}

	public function delete_customer_search( $property_id = NULL )
	{ 
		if( empty($property_id) )
		{
			return exit(json_encode(array("success" => FALSE)));
		}

		if( $this->customer->delete_customer_search( $property_id ) )
		{
			return exit(json_encode(array("success" => TRUE)));
		}

		return exit(json_encode(array("success" => FALSE)));
	}
	
	public function update_profile( $profile_id = NULL )
	{
		if( $this->input->is_ajax_request() )
		{
			if( $_POST )
			{
				if( $this->customer->update_contact_profile( $profile_id ) )
				{
					return exit(json_encode(array("success" => TRUE,"message" => "<span style='color:green'>Profile has been successfully updated!</span>" )));
				}
				else
				{
					return exit(json_encode(array("success" => FALSE, "message" => "<span style='color:red'>Failed to edit profile!</span>" )));
				}

			}

			return exit(json_encode(array("success" => FALSE, "message" => "<span style='color:red'>Failed to edit profile!</span>" )));
		}
		return exit(json_encode(array("success" => FALSE, "message" => "<span style='color:red'>Failed to edit profile!</span>" )));
	}

	public function update_email_preferences_settings()
	{
		if( $this->input->is_ajax_request() )
		{
			if( $this->customer->update_email_preferences_settings() )
			{
				return exit(json_encode(array("success" => TRUE )));
			}
			else
			{
				return exit(json_encode(array("success" => FALSE )));
			}
		}

		return exit(json_encode(array("success" => FALSE )));
	}	

	public function user_send_email()
	{
		$customer_id = $this->session->userdata("customer_id");

		if( empty($customer_id) ) return FALSE; 

		if( $_POST ){

			if( $email_id = $this->customer->save_email() )
			{ 
				//save to activtiy
				save_activities( $customer_id, $email_id, $_POST["message"], "send email", "customer", $this->session->userdata("contact_id"));
				$this->send_email_to_customer( );

				exit(json_encode(array("success" => TRUE)));
			}
			else
			{
				exit(json_encode(array("success" => FALSE)));
			}

		}
	}

	public function send_email_to_customer() 
	{
	 	$this->load->library("email");
		
		$agent_profile = $this->customer->getAgentInfo();
		$contact_profile = $this->customer->getContactInfo();

		$content = "Hi ".$agent_profile->agent_name.", <br><br>

			You have a new message from ".$contact_profile->contact_name."! <br><br>

			<a href='".base_url("/login")."' target = '_blank'> Read Message </a> <br><br>

			Regards, <br>

			Team AgentSquared
			";

	 	$subject ="New message from ".$contact_profile->contact_name ;

	 	return $this->email->send_email(
	 		'sales@agentsquared.com',
	 		'Agentsquared',
	 		$agent_profile->email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "rolbru12@gmail.com,jocereymondelo@gmail.com"
	 		)
	 	);
	}

	public function set_email_schedule() {
		if($this->input->post('schedule')) {
			$schedule = $this->input->post('schedule');

			$update = $this->customer->set_email_schedule(array('email_schedule'=>$schedule));

			if($update) {
				$response = array("success" => TRUE);
				exit(json_encode($response));
			}
		}

		$response = array("success" => FALSE);
		exit(json_encode($response));
	}

	public function check_subscription() {
		$switch = $this->customer->get_contact_subscription();
		exit(json_encode(array('is_subscribe_email' => $switch->is_subscribe_email, 'email_schedule' => $switch->email_schedule)));
	}

	public function set_email_subscription() {
		if($this->input->post('switch_post')) {
			$post = $this->input->post('switch_post');
			if($post == 'subscribe') {
				$data = array('is_subscribe_email'=> 1);
			} else {
				$data = array('is_subscribe_email'=> 0);
			}
			$this->customer->set_email_subscription($data);
		}
	}
}