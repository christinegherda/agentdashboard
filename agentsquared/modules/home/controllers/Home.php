<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {


	function __construct() {

 		parent::__construct();

 		$userData = $this->session->userdata('user_id');
 		//$this->output->enable_profiler(TRUE);

        if(empty($userData)) {
            $this->session->set_flashdata('flash_data', 'You don\'t have access!');
            redirect('login/auth/login', 'refresh');
        }

 		//$this->output->enable_profiler(TRUE);
 		$this->load->model("customer_model", "customer");
 		$this->load->model("home_model");
 		$this->load->model("Page_model","page");
 		$this->load->model("Blog_model","blog");
 		$this->load->library("facebook");
 		$this->load->library("idx_auth", $this->session->userdata("agent_id"));
 	} 
	 

	public function index() { 
		
		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header";

		$is_custom_home = $this->home_model->is_custom_home();
		$is_saved_search_selected = Modules::run('dsl/getSelectedAgentSavedSearch');

		//set home view
		if($is_custom_home != "" || isset($is_saved_search_selected[0])){
		 	$data["page"] = "home_custom";
		 } else {
			$data["page"] = "home";
		}

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
        $data['slider_photos'] = $this->home_model->getSliderPhotos();

        $data["user_info"] = $user_info;
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$data["token_checker"] = ($token) ? $this->idx_auth->checker() : TRUE;
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

      	$obj = Modules::load("mysql_properties/mysql_properties");

		$data['active_listings'] = $obj->get_properties($this->session->userdata("user_id"), "active");
		$data['new_listings'] = $obj->get_properties($this->session->userdata("user_id"), "new");
		$data['office_listings'] = $obj->get_office($this->session->userdata("user_id"), "office");
		$data['sold_listings'] = $obj->get_properties($this->session->userdata("user_id"), "sold");
		$data['nearby_listings'] = $obj->get_properties($this->session->userdata("user_id"), "nearby");
		$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");

		$data["active_title"] = $this->home_model->get_active_title();
		$data["new_title"] = $this->home_model->get_new_title();
		$data["nearby_title"] = $this->home_model->get_nearby_title();
		$data["office_title"] = $this->home_model->get_office_title();

		$data["is_active_selected"] = $this->home_model->get_active_selected();
		$data["is_nearby_selected"] = $this->home_model->get_nearby_selected();	
		$data["is_new_selected"] = $this->home_model->get_new_selected();
		$data["is_office_selected"] = $this->home_model->get_office_selected();
		$data["is_saved_search_selected"] = $is_saved_search_selected;
		$data["saved_searches_selected"] = $is_saved_search_selected;
		
		$data["is_active_featured"] = $this->home_model->get_active_featured();
		$data["is_nearby_featured"] = $this->home_model->get_nearby_featured();
		$data["is_new_featured"] = $this->home_model->get_new_featured();
		$data["is_office_featured"] = $this->home_model->get_office_featured();
		$data["is_saved_search_featured"] = (!empty($is_saved_search_featured)) ? $is_saved_search_featured : NULL; 

		$data["saved_searches_selected"] = $is_saved_search_selected;
			
		//Set dynamic Metas
		if(isset($data['account_info'])){
			$data["broker"] = isset($data['account_info']->Office) ? $data['account_info']->Office : "" ;
			$data["broker_number"] = isset($data['account_info']->Phones[0]->Number) ? $data['account_info']->Phones[0]->Number : "" ;
		}
		if(isset($data['active_listings']) && !empty($data['active_listings'])){
			foreach($data['active_listings'] as $active){

				if(isset($data['user_info']->banner_tagline) && !empty($data['user_info']->banner_tagline)){

					$data['title'] = $data['user_info']->first_name.' '.$data['user_info']->last_name.' | '.$data['user_info']->banner_tagline;

				} elseif(!empty($data['account_info']->Name)) {

					$data['title'] = $data['account_info']->Name.' | '.$active->StandardFields->UnparsedFirstLineAddress;

				} else {

					$data['title'] = 'AgentSquared | '.$active->StandardFields->UnparsedFirstLineAddress;
				}
				
				$data['description'] = str_limit($active->StandardFields->PublicRemarks,197);
				$data['image'] = !empty($active->StandardFields->Photos[0]->Uri1024) ? $active->StandardFields->Photos[0]->Uri1024 : "";
			}
		} elseif(isset($data['office_listings'])  && !empty($data['office_listings'])){

			if(isset($data['user_info']->banner_tagline) && !empty($data['user_info']->banner_tagline)){
				$data['title'] = $data['user_info']->first_name.' '.$data['user_info']->last_name.' | '.$data['user_info']->banner_tagline;
			} elseif(!empty($data['account_info']->Name)) {
				$data['title'] = $data['account_info']->Name.' | '.$data['office_listings'][0]['StandardFields']['UnparsedFirstLineAddress'];
			} else {
				$data['title'] = 'AgentSquared | '.$data['office_listings'][0]['StandardFields']['UnparsedFirstLineAddress'];
			}

			$data['description'] = str_limit($data['office_listings'][0]['StandardFields']['PublicRemarks'],197);
			$data['image'] = !empty($data['office_listings'][0]['Photos']['Uri300']) ? $data['office_listings'][0]['Photos']['Uri300'] : "";
		} elseif(isset($data['new_listings'])  && !empty($data['new_listings'])) {

				if(isset($data['user_info']->banner_tagline) && !empty($data['user_info']->banner_tagline)){
					$data['title'] = $data['user_info']->first_name.' '.$data['user_info']->last_name.' | '.$data['user_info']->banner_tagline;
				} elseif(!empty($data['account_info']->Name)) {
					$data['title'] = $data['account_info']->Name.' | '.$data['new_listings'][0]->StandardFields->UnparsedFirstLineAddress;
				} else {
					$data['title'] = 'AgentSquared | '.$data['new_listings'][0]->StandardFields->UnparsedFirstLineAddress;
				}
				
				$data['description'] = str_limit($data['new_listings'][0]->StandardFields->PublicRemarks,197);
				$data['image'] = !empty($data['new_listings'][0]->StandardFields->Photos[0]->Uri300) ? $data['new_listings'][0]->StandardFields->Photos[0]->Uri300 : "";
		}

	 	$data["js"] = array("listings.js");
	 	$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$this->load->view("page", $data);
	}

	function find_a_home() {

		$limit = 24;
		$this->load->library('pagination');
		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "find_a_home";
		$data["title"] = "Find A Home";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

        $data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data['new_listings'] = $obj->get_properties($this->session->userdata("user_id"), "new");

		if(isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		else {
			$page = 1;
		}
		
		if( isset($_GET['offset']) && !isset($_GET['page']) ) {
			$page = ($_GET['offset'] / ($limit - 1)) + 1;
			$page = intval ($page);
		}

		$dsl = modules::load('dsl/dsl');
		$endpoint = "properties/".$this->session->userdata("user_id")."/status/Active,Contingent,Pending?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto";

		$active_listings = $dsl->get_endpoint($endpoint);
		$activeListingData = json_decode($active_listings);
		$activeList = array();

		if( !empty($activeListingData)){
			if(property_exists($activeListingData, 'data') ) {
				foreach ($activeListingData->data as $active){
			 		$activeList[] = $active->standard_fields[0];
			 	}
			}

			$data['active_listings'] = $activeList;
			$data['active_listings_total'] = $activeListingData->total_count;
		}

		if(!empty($activeListingData)) {
			$data['total_pages'] = $activeListingData->total_pages;
			$data['current_page'] = $activeListingData->current_page;
			$data['total_count'] = $activeListingData->total_count;
			$data['page_size'] = $activeListingData->page_size;

			$param["limit"] = 24;
			$total = $activeListingData->total_count;
			$config["base_url"] = base_url().'/home/find_a_home';
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';
		
			$this->pagination->initialize($config);	
		}
		$data["pagination"] = $this->pagination->create_links();

		$data["js"] = array("listings.js");
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$this->load->view('page', $data);
	}

	public function page($slug = NULL) {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "page_view";
		$data["title"] = "Page";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

        $data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		
		$data["page_data"] = $this->page->get_page($slug);
		$data["search_id"] = $this->page->get_search_id($slug);
		$search_id = $data["search_id"]["saved_search_id"];
		$data["search_id"] = $search_id;
		$savedSearch = $this->idx_auth->getSavedSearch($search_id);

		if(!empty($savedSearch)){

			$dsl = modules::load('dsl/dsl');
			$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
			$access_token = array(
					'access_token' => $token->access_token
			);

			$filterStr = "(".$savedSearch[0]['Filter'].")";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$endpoint = "spark-listings?_limit=6&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
			$results = $dsl->post_endpoint($endpoint, $access_token);
			$savedSearches['results'] = json_decode($results, true);

			$data["saved_searches"] = $savedSearches['results'];
		}
		
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["js"] = array("listings.js");
	
		$this->load->view('page',$data);
	}
	public function about() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "about";
		$data["title"] = "About Us";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$data["user_info"] = $user_info;
		$data['account_info'] = Modules::run('dsl/getAgentAccountInfo');
		$data['system_info'] = Modules::run('dsl/getSystemInfo');

		$data["js"] = array("listings.js");

		$this->load->view("page", $data);
	}


	public function contact() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "contact";
		$data["title"] = "Contact";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$data["js"] = array("listings.js");

		$this->load->view("page", $data);
	}	

	public function buyer() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "buyer";
		$data["title"] = "Buyer";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data["page_data"] = $this->page->get_buyer_page();

		$data["js"] = array("listings.js");

		$this->load->view("page", $data);
	}

	public function seller() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "seller";
		$data["title"] = "Seller";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data["page_data"] = $this->page->get_seller_page();

		$data["js"] = array("listings.js");

		$this->load->view("page", $data);
	}

	function testimonials() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "testimonials";
		$data["title"] = "Testimonials";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$this->load->model('reviews/reviews_model', 'review');
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;

		if($this->input->get('reviews_order'))
			$data['reviews'] = Modules::run('reviews/reviews/get_sort_reviews', $param);
		else
			$data['reviews'] = Modules::run('reviews/reviews/fetch_approved_reviews', $param);

		$data['average_reviews'] = Modules::run('reviews/reviews/compute_average_review');
		$total = $this->review->count_approved_reviews();
		$this->load->library('pagination');
		$config["base_url"] = base_url().'home/testimonials';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		$data["js"] = array("jquery.rateyo.min.js");
		$data["css"] = array("jquery.rateyo.min.css");

		$this->load->view("page", $data);
	}

	function add_testimonial() {

		if($this->session->userdata('customer_id')) {
			if($this->input->post('subject') && $this->input->post('message')) {
				$data = array(
					'user_id' => $this->input->post('user_id'),
					'customer_id' => $this->input->post('customer_id'),
					'rating' => $this->input->post('rating'),
					'subject' => $this->input->post('subject'),
					'message' => $this->input->post('message'),
					'date_created' => date('Y-m-d H:i:s')
				);
				$insert = $this->home_model->save_testimonial($data);
				if($insert)
					$this->session->set_flashdata("message", "<p>Testimonial Successfully Submitted!</p>" );

				$response = ($insert) ? array("success"=>TRUE, "redirect"=>base_url().'testimonials') : array("success" => FALSE, "redirect"=>base_url().'testimonials');
				exit(json_encode($response));
			}
		} else {
			$response = array("success"=>TRUE, "redirect"=>base_url().'home/login');
			exit(json_encode($response));
		}
	}
	
	function blog() {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "blog";
		$data["title"] = "Blog";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$this->load->library('pagination');
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 5;
		$total = $this->blog->get_all_blogs( TRUE, $param );
		$data["blogs"] = $this->blog->get_all_blogs( FALSE, $param );
		$config["base_url"] = base_url().'/blog';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		$data["all_blog_archives"] = $this->blog->get_all_blog_archives();

		$this->load->view("page", $data);
	}

	function post($slug = NULL) {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "post";
		$data["title"] = "Blog Post";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$param = array();
		$data["blogs"] = $this->blog->get_all_blogs( FALSE, $param );
		$data["recent_posts"] = $this->blog->get_recent_post();
		$data["all_blog_archives"] = $this->blog->get_all_blog_archives();
		$data["blog_post"] = $this->blog->get_blog( $slug);

		$this->load->view("page",$data);
	}

	function archives($filter = NULL) {

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "archive";
		$data["title"] = "Archive Post";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$this->load->library('pagination');
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 5;
		$total = $this->blog->filter_archives($filter, TRUE, $param );
		$data["filter_archives"] = $this->blog->filter_archives($filter, FALSE, $param );
		$config["base_url"] = base_url().'/home/archives';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		$data["all_blog_archives"] = $this->blog->get_all_blog_archives();

		$this->load->view("page", $data);
	}

	function saved_searches( $search_id = NULL){

		$this->load->library('pagination');
		
		if( empty($search_id) ){

			redirect("/home", "refresh");
		}

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "saved_searches";
		$data["title"] = "Saved Search";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		if(isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		else {
			$page = 1;
		}
		
		$savedSearch = $this->idx_auth->getSavedSearch($search_id);

		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
			'access_token' => $token->access_token
		);

		$filterStr = "(".$savedSearch[0]['Filter'].")";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "spark-listings?_pagination=1&_page=".$page."&_limit=24&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$savedSearches['results'] = json_decode($results, true);

		$data['saved'] = $savedSearches['results'];
		//printA($data["saved"]);exit;

		if(!isset($data['saved']["response"])) {	
			$data['total_pages'] = $data['saved']['pagination']['TotalPages'];
			$data['current_page'] = $data['saved']['pagination']['CurrentPage'];
			$data['total_count'] = $data['saved']['pagination']['TotalRows'];
			$data['page_size'] = $data['saved']['pagination']['PageSize'];

			$param["limit"] = 24;
			$total = $data['saved']['pagination']['TotalRows'];
			$config["base_url"] = base_url().'/home/saved_searches/'.$search_id;
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';
		
			$this->pagination->initialize($config);	
		}

		$data["pagination"] = $this->pagination->create_links();

		$data['search_id'] = $search_id;		
		$data["saved_searches"] = $savedSearches['results'];
		$data['saved_search_name'] = $savedSearch[0]["Name"];
		
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$data["js"] = array("listings.js");

		$this->load->view("page", $data);
	}

	function all_saved_searches(){

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "all_saved_searches";
		$data["title"] = "Saved Search";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

        $dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
			'access_token' => $token->access_token
		);

		$savedSearchSelected = Modules::run('dsl/getSelectedAgentSavedSearch');
		if( !empty( $this->idx_auth->checker() ) ){
			if( !empty($savedSearchSelected) ){ 
				foreach($savedSearchSelected as &$savedSelected) {

					$filterStr = "(".$savedSelected['Filter'].")";
					$filter_url = rawurlencode($filterStr);
					$filter_string = substr($filter_url, 3, -3);

					$endpoint = "spark-listings?_limit=1&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
					$results = $dsl->post_endpoint($endpoint, $access_token);
					$data_results = json_decode($results, true);
					$savedSelected['data'] = (!empty($data_results["data"])) ? $data_results["data"][0]["StandardFields"] : "";
					$savedSelected['photo'] = (!empty($data_results["data"])) ? $data_results["data"][0]["Photos"]["Uri300"] : "";
	    		}    
		    }
		}

		$data["saved_searches_selected"] = $savedSearchSelected;
		//printA($data["saved_searches_selected"]);exit;

		$data["user_info"] = $user_info;
		$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$data["js"] = array("listings.js");

		$this->load->view("page", $data);
	}

	function save_favorates_properties( $property_id = NULL ) {

		if( $this->input->is_ajax_request() )
		{

			if(!isset($_SESSION["customer_id"]) )
			{
				$redirect = base_url()."home/login?property_id=".$property_id;
				$response = array( "success" => FALSE, "redirect" => $redirect);

				exit( json_encode($response) );
			}

			if( empty($property_id) ) exit(json_encode(array("success" => FALSE)));

			if( $this->customer->save_favorates_properties($property_id) ) {
				$response = array( "success" => TRUE);
			}
			else
			{
				$response = array("success" => FALSE);
			}

			exit( json_encode($response) );
		}
	}
	
	function save_search_properties() {

		if( $this->input->is_ajax_request() )
		{	
			$url= http_build_query($_GET);
			
			if(!isset($_SESSION["customer_id"]))
			{
				$redirect = base_url()."home/login?".$url;

				$response = array( "success" => FALSE, "redirect" => $redirect);

				exit( json_encode($response) );
			}

			if( empty($url) ) exit(json_encode(array("success" => FALSE)));

			if($this->customer->save_search_properties($url))
			{
				$response = array("success" => TRUE);
			}
			else
			{
				$response = array("success" => FALSE);
			}
			exit( json_encode($response) );
		}
	}

	function listings() {

		$this->load->library('pagination');

		$getData = http_build_query($this->input->get());

		$searchProperty = Modules::run('search/get_search_property', $getData);

		$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));

		if($token) {
			
			if(!empty($this->idx_auth->checker())){

				if(isset($searchProperty["data"]) && !empty($searchProperty["data"])) {
					$data["featured"] = $searchProperty["data"];

					if($searchProperty["pagination"]) {	
						$data["total_pages"] = $searchProperty["pagination"]["TotalPages"];
						$data["current_page"] = $searchProperty["pagination"]["CurrentPage"];
						$data["total_count"] = $searchProperty["pagination"]["TotalRows"];
						$data["page_size"] = $searchProperty["pagination"]["PageSize"];

						$param["limit"] = 24;
						$total = $searchProperty["pagination"]["TotalRows"];
						$config["base_url"] = base_url().'/home/listings?'.http_build_query($_GET);
						$config["total_rows"] = $total;
						$config["per_page"] = $param["limit"];
						$config['page_query_string'] = TRUE;
						$config['query_string_segment'] = 'offset';
					
						$this->pagination->initialize($config);
					}
				}
			}
			
		} else {
			$data["featured"] = $searchProperty["data"];

			if($searchProperty["pagination"]) {	
				$data["total_pages"] = $searchProperty["pagination"]["TotalPages"];
				$data["current_page"] = $searchProperty["pagination"]["CurrentPage"];
				$data["total_count"] = $searchProperty["pagination"]["TotalRows"];
				$data["page_size"] = $searchProperty["pagination"]["PageSize"];

				$param["limit"] = 24;
				$total = $searchProperty["pagination"]["TotalRows"];
				$config["base_url"] = base_url().'/home/listings?'.http_build_query($_GET);
				$config["total_rows"] = $total;
				$config["per_page"] = $param["limit"];
				$config['page_query_string'] = TRUE;
				$config['query_string_segment'] = 'offset';
			
				$this->pagination->initialize($config);
			}
		}

		$data["pagination"] = $this->pagination->create_links();

		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

 		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

 		$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "listings";
		$data["title"] = "Search Results";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

        $data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$data["js"] = array("listings.js");

		$this->load->view("page", $data);
	}


	function nearby_listings(){

		$this->load->library('pagination');

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "all_nearby_listings";
		$data["title"] = "Nearby Listings";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

        $data["user_info"] = $user_info;
        $data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$activeListings = $obj->get_properties($this->session->userdata("user_id"), "active");
		$newListings = $obj->get_properties($this->session->userdata("user_id"), "new");
		$officeListings = $obj->get_office($this->session->userdata("user_id"), "office");

		$data["js"] = array("listings.js");
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		if(isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		else {
			$page = 1;
		}

		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
			'access_token' => $token->access_token
		);
		$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Contingent' Or MlsStatus Eq 'Pending')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		if(!empty($activeListings)){

			//get latlon from first active listings
			$lat = (isset($activeListings[0]->StandardFields->Latitude) ? $activeListings[0]->StandardFields->Latitude : "" );
			$lon = (isset($activeListings[0]->StandardFields->Longitude) ? $activeListings[0]->StandardFields->Longitude : "" );

		} elseif(!empty($officeListings)) {

			//get latlon from office listings
			$lat = (isset($officeListings[0]['StandardFields']['Latitude']) ? $officeListings[0]['StandardFields']['Latitude'] : "" );
			$lon = (isset($officeListings[0]['StandardFields']['Longitude']) ? $officeListings[0]['StandardFields']['Longitude'] : "" );
		} elseif(!empty($newListings)) {
			
			$lat = (isset($newListings[0]->StandardFields->Latitude) ? $newListings[0]->StandardFields->Latitude : "" );
			$lon = (isset($newListings[0]->StandardFields->Longitude) ? $newListings[0]->StandardFields->Longitude : "" );

		}

		$endpoint = "spark-nearby-listings?_pagination=1&_page=".$page."&_limit=24&_lat=".$lat."&_lon=".$lon."&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$nearby_listings['results'] = json_decode($results, true);

		$data['nearby_listings_data'] = $nearby_listings['results'];
		//printA($data['nearby_listings_data'] );exit;

		if(!empty($data['nearby_listings_data']['data'])) {	
			$data['total_pages'] = $data['nearby_listings_data']['pagination']['TotalPages'];
			$data['current_page'] = $data['nearby_listings_data']['pagination']['CurrentPage'];
			$data['total_count'] = $data['nearby_listings_data']['pagination']['TotalRows'];
			$data['page_size'] = $data['nearby_listings_data']['pagination']['PageSize'];

			$param["limit"] = 24;
			$total = $data['nearby_listings_data']['pagination']['TotalRows'];
			$config["base_url"] = base_url().'/home/nearby_listings';
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';
		
			$this->pagination->initialize($config);
		}

		$data["pagination"] = $this->pagination->create_links();
		
		$this->load->view("page", $data);
	}

	function sold_listings(){

		$limit = 24;
		$this->load->library('pagination');

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "all_sold_listings";
		$data["title"] = "Sold Listings";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

        $data["user_info"] = $user_info;
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		if(isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		else {
			$page = 1;
		}
		
		if( isset($_GET['offset']) && !isset($_GET['page']) ) {
			$page = ($_GET['offset'] / ($limit - 1)) + 1;
			$page = intval ($page);
		}

		$dsl = modules::load('dsl/dsl');
		$endpoint = "properties/".$this->session->userdata("user_id")."/status/Sold,Closed?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto";

		$sold_listings = $dsl->get_endpoint($endpoint);
		$soldListingData = json_decode($sold_listings);
		$soldList = array();

		if( !empty($soldListingData)){
			if(property_exists($soldListingData, 'data') ) {
				foreach ($soldListingData->data as $sold){
			 		$soldList[] = $sold->standard_fields[0];
			 	}
			}

			$data['sold_listings'] = $soldList;
			$data['sold_listings_total'] = $soldListingData->total_count;
		}

		if(!empty($soldListingData)) {
			$data['total_pages'] = $soldListingData->total_pages;
			$data['current_page'] = $soldListingData->current_page;
			$data['total_count'] = $soldListingData->total_count;
			$data['page_size'] = $soldListingData->page_size;

			$param["limit"] = 24;
			$total = $soldListingData->total_count;
			$config["base_url"] = base_url().'/home/sold_listings';
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';
		
			$this->pagination->initialize($config);	
		}

		$data["pagination"] = $this->pagination->create_links();

		$data["js"] = array("listings.js");
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$this->load->view("page", $data);
	}

	function office_listings(){

		$this->load->library('pagination');

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "all_office_listings";
		$data["title"] = "Office Listings";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

        $data["user_info"] = $user_info;
        $data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$obj = Modules::load("mysql_properties/mysql_properties");
      	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		
		if(isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		else {
			$page = 1;
		}
		
		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
			'access_token' => $token->access_token
		);
		$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Contingent' Or MlsStatus Eq 'Pending')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "office/listings/?_pagination=1&_page=".$page."&_limit=24&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$office_listings['results'] = json_decode($results, true);

		$data['office_listings_data'] = $office_listings['results'];
		//printA($data['office_listings_data'] );exit;
		//
		if($data['office_listings_data']) {	
			$data['total_pages'] = $data['office_listings_data']['pagination']['TotalPages'];
			$data['current_page'] = $data['office_listings_data']['pagination']['CurrentPage'];
			$data['total_count'] = $data['office_listings_data']['pagination']['TotalRows'];
			$data['page_size'] = $data['office_listings_data']['pagination']['PageSize'];

			$param["limit"] = 24;
			$total = $data['office_listings_data']['pagination']['TotalRows'];
			$config["base_url"] = base_url().'/home/office_listings';
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';
		
			$this->pagination->initialize($config);	
		}

		$data["pagination"] = $this->pagination->create_links();

		$data["js"] = array("listings.js");
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		
		$this->load->view("page", $data);
	}

	/*public function login() {

		if(isset($_SESSION["customer_id"])){
			if($this->session->userdata("featuredPropertyUrl"))
		        redirect($this->session->userdata("featuredPropertyUrl"), "refresh");
		   	elseif($this->session->userdata("popularsearchurl_session"))
		        redirect($this->session->userdata("popularsearchurl_session")."?is_fb_login=TRUE", "refresh");
		    else
		    	redirect(base_url()."customer-dashboard");
		}

		if($_POST) {	

			$this->form_validation->set_rules("first_name", "First Name", "required|trim");
			$this->form_validation->set_rules("last_name", "Last Name", "required|trim");
			$this->form_validation->set_rules("phone_number", "Cell / Phone Number", "required|trim");
			$this->form_validation->set_rules("email", "Email", "required|trim");
			$this->form_validation->set_rules("password", "Password", "required|trim");

			if($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('session' , array('status' => FALSE,'message' => validation_errors() ));
			}
			else
			{
				if($this->customer->sign_in())
				{	
					if($this->input->post('listing_id') && $this->input->post('property_type')) {
						redirect(base_url()."home/".$_POST['property_type']."?listingId=".$_POST['listing_id']);
					} if($this->input->post('popular_search')) {
						redirect($this->input->post('popular_search'));
					} else {
						redirect(base_url()."customer-dashboard");
					}
				}
				else
				{
					$this->session->set_flashdata('session', array('status' => FALSE,'message' => 'Incorrect Login!'));
				}
			}
			unset($_POST);
		}

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$data["user_info"] = $user_info;
		$data["theme"] = $user_info->theme;
		$siteInfo = Modules::run('agent_sites/getInfo');
        $data['branding'] = $siteInfo['branding'];
		$data["page"] = "customer_login_form";
		$data['title'] = "Login";

		$this->load->view("page", $data);
	}*/


	public function login() {

		if($this->input->is_ajax_request()) {

			if($this->session->userdata('customer_id')) {
				if($this->session->userdata("featuredPropertyUrl"))
			        redirect($this->session->userdata("featuredPropertyUrl"), "refresh");
			   	elseif($this->session->userdata("popularsearchurl_session"))
			        redirect($this->session->userdata("popularsearchurl_session")."?is_fb_login=TRUE", "refresh");
			    else
			    	redirect(base_url()."customer-dashboard");
			}

			if($this->input->post()) {

				$this->form_validation->set_rules("email", "Email", "required|trim");
				$this->form_validation->set_rules("phone", "Phone", "required|trim");

				if($this->form_validation->run() == FALSE) {
					$response = array('success'=>FALSE, 'message'=>validation_errors());
				} else {
					if($this->customer->sign_in()) {	
						if($this->input->post('listing_id') && $this->input->post('property_type')) {
							//redirect(base_url()."home/".$_POST['property_type']."?listingId=".$_POST['listing_id']);
							//$response = array('success'=>FALSE, 'redirect'=>validation_errors());
						} if($this->input->post('popular_search')) {
							$response = array('success'=>TRUE, 'redirect'=>$this->input->post('popular_search'));
						} else {
							$response = array('success'=>TRUE, 'redirect'=>base_url()."customer-dashboard");
						}
					} else {
						$response = array('session', array('status'=>FALSE, 'message'=>'Incorrect Login!'));
					}
				}
				unset($_POST);
				exit(json_encode($response));
			}
		}
	}

	public function signup() {

		if($this->input->is_ajax_request()) {

			if($this->session->userdata('customer_id')) {
				redirect(base_url()."customer-dashboard");
			}

			if($this->input->post()) { 

				$this->form_validation->set_rules("fname", "First Name", "required|trim");
				$this->form_validation->set_rules("lname", "Last Name", "required|trim");
				$this->form_validation->set_rules("email", "Email", "required|trim");
				$this->form_validation->set_rules("phone", "Phone Number", "required|trim");

				if($this->form_validation->run() == FALSE) {
					$response = array('success'=>FALSE, 'message'=>validation_errors());
				} else {

					if(!$this->customer->is_email_exist()) {

						$save_return = $this->save_to_flexmls();

						if($save_return) {

							$password = $this->input->post("phone");
							$type = ($this->input->post('type')) ? $this->input->post('type') : 'signup_form';

							$contact_id = (isset($save_return[0]["Id"]) && !empty($save_return[0]["Id"])) ? $save_return[0]["Id"] :  generate_key("10");

							$data = array(
								'ip_address'	=> $_SERVER['REMOTE_ADDR'],
					            'first_name' 	=> $this->input->post("fname"),
					            'last_name' 	=> $this->input->post("lname"),
					            'email' 		=> $this->input->post("email"),
					            'phone' 		=> $this->input->post("phone"),
					            'password' 		=> password_hash($password, PASSWORD_DEFAULT),
					            'active' 		=> 1,
					            'type' 			=> $type,
					            'user_id' 		=> $this->session->userdata("user_id"),
					            'agent_id' 		=> $this->session->userdata("agent_id"),
					            'contact_id' 	=> $contact_id,
					            'created' 		=> date("Y-m-d H:m:s"),
					            'modified' 		=> date("Y-m-d H:m:s")
							);

							if($this->customer->sign_up($data)) {

								echo Modules::run('crm/leads/send_leads_notification_to_agent');
								echo Modules::run('crm/leads/send_login_credential_to_customer');
								$response = array('success'=>TRUE, 'redirect'=>base_url().'customer-dashboard');

							} else {
								$response = array('success'=>FALSE, 'message'=>'Incorrect Login!');
							}	

						} else {
							$response = array('success'=>FALSE, 'message'=>'Cannot connect to MLS!');
						}
					}
					else {
						$response = array('success'=>FALSE, 'message'=>'Email address is already exist!');
					}
				}
				exit(json_encode($response));
			}
		}
	}

	public function reset_phone_number() {

		if($this->input->is_ajax_request()) {

			if($this->input->post('email')) {

				$ret = $this->customer->isHaveRecord($this->input->post('email'));

				if($ret) {
					$phone = $ret->phone;
					$response = array('success'=>TRUE, 'message'=>'Hi! We just sent you an email with a link to reset your phone number.');
				} else {
					$response = array('success'=>FALSE, 'message'=>'You don\'t have a record yet!');
				}

			} else {
				$response = array('success'=>FALSE, 'message'=>'Please provide your email.');
			}	

			exit(json_encode($response));
		}

	}


	/*public function signup() {
		
		//printA($this->input->post());exit;
		if(isset($_SESSION["customer_id"])){
			redirect(base_url()."customer-dashboard");
		}

		if($_POST) { 

			$this->form_validation->set_rules("fname", "First Name", "required|trim");
			$this->form_validation->set_rules("lname", "Last Name", "required|trim");
			$this->form_validation->set_rules("email", "Email", "required|trim");
			$this->form_validation->set_rules("phone", "Phone Number", "required|trim");
			//$this->form_validation->set_rules("password", "Password", "required|trim|matches[confirm_password]");
			//$this->form_validation->set_rules("confirm_password", "Confirm Password", "required|trim");

			if($this->form_validation->run() == FALSE) {

				$this->session->set_flashdata('session' , array('status' => FALSE,'message' => validation_errors()));

			} else {

				if(!$this->customer->is_email_exist()) {
					$save_return = $this->save_to_flexmls();
					
					if(!empty($save_return)) {
						if($this->customer->sign_up($save_return[0]["Id"])) {
							echo Modules::run('crm/leads/send_leads_notification_to_agent');
							echo Modules::run('crm/leads/send_login_credential_to_customer');
							redirect(base_url()."customer-dashboard");
						}
						else {
							$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Incorrect Login!'));
						}	
					}
					$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Cannot connect to MLS!'));
				}
				else {
					$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Email address is already exist!'));
				}
			}
		}

		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$data["user_info"] = $user_info;
		$data["theme"] = $user_info->theme;
		$siteInfo = Modules::run('agent_sites/getInfo');
        $data['branding'] = $siteInfo['branding'];
		//$data["page"] = "customer_signup_form";
		$data['title'] = "Sign Up";

		//$this->load->view("page", $data);
	}*/

	public function logout() {

		$this->session->unset_userdata(array('customer_id', 'customer_email' ,'facebook_customer_login','save_properties','isSavePropertyId','isSaveSearchPropertyUrl','featuredPropertyUrl','popularsearchurl_session'));
		redirect(base_url()."home", 'refresh');
	}

	public function facebook_login() {
    
        $data['user'] = array();
      	
        if($this->session->userdata('facebook_customer_login') != true && !isset($_SESSION["customer_id"]))
        {
            // Check if user is logged in
            if($token = $this->facebook->is_authenticated() )
            {
	            // User logged in, get user details
	            $user = $this->facebook->request('get', '/me?fields=name,email');
	            $customer_info = $this->customer->is_registered($user);
	            
	            if(!$customer_info )
	            {

	            	//insert to flex
	            	$save_return = $this->save_fb_to_flexmls( $user );
	            	$contactId = ( isset( $save_return[0]["Id"] ) ) ? $save_return[0]["Id"] : false;
	            	//insert to database
	            	$this->customer->insert_fb_registration( $user , $contactId );

	            	//send email to agent
	            	echo Modules::run('crm/leads/send_leads_notification_to_agent_fb', $user);
	            	
	            	//Save favorite property to database
	            	if($this->session->userdata('isSavePropertyId'))
	            		$this->customer->save_favorates_properties($this->session->userdata('isSavePropertyId'));
	            	//Save followed search to database
	            	if($this->session->userdata('isSaveSearchPropertyUrl'))
	            		$this->customer->save_search_properties($this->session->userdata('isSaveSearchPropertyUrl'));

	            	$this->session->set_userdata('facebook_customer_login', true);

	            	redirect(base_url()."customer-dashboard?is_fb_login=TRUE", "refresh");
	            }
	            else
	            {
	            	//query to database
	            	$save_properties = $this->customer->get_save_properties($customer_info["id"]);
                
	                $c_session["save_properties"] = $save_properties;

	                $this->customer->set_customer_session( $c_session );

	            	$customer["customer_id"] = $customer_info["id"];
	            	$customer["contact_id"] = $customer_info["contact_id"];
		            $customer["customer_email"] = $customer_info["email"];

		            //set session
		            $this->customer->set_customer_session( $customer );
		            //Save favorite property to database
		            if($this->session->userdata('isSavePropertyId'))
	            		$this->customer->save_favorates_properties($this->session->userdata('isSavePropertyId'));
	            	//Save followed search to database
	            	if($this->session->userdata('isSaveSearchPropertyUrl'))
	            		$this->customer->save_search_properties($this->session->userdata('isSaveSearchPropertyUrl'));

		            $this->session->set_userdata('facebook_customer_login', true);
	            	
	            	redirect(base_url()."customer-dashboard?is_fb_login=TRUE", "refresh");
	            }
	        }
	    }

	    redirect(base_url()."customer-dashboard?is_fb_login=TRUE", "refresh");
    }

	private function save_to_flexmls() {

		$arr = array(
			'DisplayName' => $this->input->post("fname"). ' '.$this->input->post("lname"),
            'GivenName' => $this->input->post("fname"),
            'FamilyName' => $this->input->post("lname"),
            'PrimaryEmail' =>  $this->input->post("email"),
			'Notify' => 1
        );
		
		$result_contacts = $this->idx_auth->AddContact($arr);
		
		return $result_contacts;
	}

	private function save_fb_to_flexmls($infos = array()) {

		$name = explode(" ", $infos["name"] );

		$arr = array(
			'DisplayName' => $infos["name"],
            'GivenName' => (isset($name[0])) ? $name[0] : "",
            'FamilyName' => (isset($name[1])) ? $name[1] : "",
            'PrimaryEmail' =>  $infos["email"],
			'Notify' => 1
        );

		$result_contacts = $this->idx_auth->AddContact($arr);
		
		return $result_contacts;
	}

	function save_customer_buyer() {

		$this->form_validation->set_rules("buyer[fname]", "First Name", "required|trim");
		$this->form_validation->set_rules("buyer[lname]", "Last Name", "required|trim");
		$this->form_validation->set_rules("buyer[email]", "Email", "required|trim|valid_email");
		$this->form_validation->set_rules("buyer[phone]", "Phone", "required|trim");
		$this->form_validation->set_rules("buyer[mobile]", "MObile", "required|trim");
		$this->form_validation->set_rules("buyer[fax]", "Fax", "required|trim");
		$this->form_validation->set_rules("buyer[address]", "Address", "required|trim");
		$this->form_validation->set_rules("buyer[city]", "City", "required|trim");
		$this->form_validation->set_rules("buyer[state]", "State", "required|trim");
		$this->form_validation->set_rules("buyer[zip_code]", "Zip Code", "required|trim");
		$this->form_validation->set_rules("buyer[bedrooms]", "Bed Rooms", "required|trim");
		$this->form_validation->set_rules("buyer[bathrooms]", "Bath Rooms", "required|trim");
		$this->form_validation->set_rules("buyer[contact_by]", "Contact By", "required|trim");
		$this->form_validation->set_rules("buyer[price_range]", "Price Range", "required|trim");
		$this->form_validation->set_rules("buyer[when_do_you_want_to_move]", "When do you want to move", "required|trim");
		$this->form_validation->set_rules("buyer[when_did_you_start_looking]", "When did you start looking", "required|trim");
		$this->form_validation->set_rules("buyer[wher_would_you_like_to_own]", "where would you like to own", "required|trim");
		$this->form_validation->set_rules("buyer[are_you_currently_with_an_agent]", "Are ou currently with an agent", "required|trim");
		$this->form_validation->set_rules("buyer[describe_your_dream_home]", "Describe ou drean home", "required|trim");

		if( $this->form_validation->run() )
		{
			if($this->customer->save_customer_buyer())
			{
				$this->session->set_flashdata(array("success" => TRUE ,"message" => "<p>Data has been successfully added!</p>" ));	
				echo Modules::run('crm/leads/send_buyer_lead');
				echo Modules::run('crm/leads/respond_to_buyer');
			}
			else
			{
				$this->session->set_flashdata(array("success" => FALSE ,"message" => "<p>Failed to add data!</p>" ));
				$response = array("success" => FALSE ,"message" => "<p>Failed to add data!</p>" );	
			}
		}
		else
		{
			$this->session->set_flashdata(array("success" => FALSE ,"message" => "<p>".validation_errors()."<p>" ));
			$response = array("success" => FALSE ,"message" => "<p>".validation_errors()."<p>" );	
		}

		redirect( base_url()."buyer" );
	}
	
	function save_customer_seller() {

		$this->form_validation->set_rules("seller[fname]", "First Name", "required|trim");
		$this->form_validation->set_rules("seller[lname]", "Last Name", "required|trim");
		$this->form_validation->set_rules("seller[email]", "Email", "required|trim|valid_email");
		$this->form_validation->set_rules("seller[phone]", "Phone", "required|trim");
		$this->form_validation->set_rules("seller[address]", "Address", "required|trim");
		$this->form_validation->set_rules("seller[zip_code]", "Zip Code", "required|trim");
		$this->form_validation->set_rules("seller[bedrooms]", "Bed Rooms", "required|trim");
		$this->form_validation->set_rules("seller[bathrooms]", "Bath Rooms", "required|trim");

		if($this->form_validation->run()) {
			if($this->customer->save_customer_seller()) {
				$this->session->set_flashdata(array("success" => TRUE ,"message" => "<p>Data has been successfully added!</p>" ));	
				echo Modules::run('crm/leads/send_seller_lead');
				echo Modules::run('crm/leads/respond_to_seller');
			} else {
				$this->session->set_flashdata(array("success" => FALSE ,"message" => "<p>Failed to add data!</p>" ));
				$response = array("success" => FALSE ,"message" => "<p>Failed to add data!</p>" );	
			}
		} else {
			$this->session->set_flashdata(array("success" => FALSE ,"message" => "<p>".validation_errors()."<p>" ));
			$response = array("success" => FALSE ,"message" => "<p>".validation_errors()."<p>" );	
		}

		redirect( base_url()."seller" );
	}

	function mortgage_calculator() {

		require_once APPPATH .'libraries/CentralApps/MortgageCalculator/Calculator.php';

			if($_POST)
			{
				$calculator = new CentralApps\MortgageCalculator\Calculator();
				$calculator->setAmountBorrowed( $this->input->post('sale_price'));
				$calculator->setDownpayment($this->input->post('down_percent'));
				$calculator->setInterestRate($this->input->post('mortgage_interest_percent'));
				$calculator->setYears($this->input->post('year_term'));

				$monthly_payment = $calculator->calculateMonthlyPayment();
				$monthly_payment_interest = $calculator->calculateMonthlyInterest();
				$annual_payment_interest = $calculator->calculateAnnualInterest();
				$downpayment = $calculator->calculateDownpayment();
				$amount_financed = $calculator->calculateAmountFinanced();

				$price_value           = $calculator->get_price_value();
				$downpayment_value     = $calculator->get_downpayment_value();
				$interest_rate_value   = $calculator->get_interest_rate_value();
				$loan_terms_value      = $calculator->get_loan_terms_value();

				$response = array("monthly_payment" => $monthly_payment, );
				exit(json_encode($response));
			}
	}

	function customer_questions() { 

		if($this->input->is_ajax_request())
		{
			if($_POST)
			{
				$this->form_validation->set_rules("name", "Name", "required|trim");
				$this->form_validation->set_rules("phone", "Phone Number", "required|trim");
				$this->form_validation->set_rules("email", "Email", "required|trim|valid_email");
				$this->form_validation->set_rules("message", "Message", "required|trim");

				if ($this->form_validation->run() == FALSE)
				{
					$response = array("success" => FALSE ,"message" => "<span class='alert alert-danger' style='display:block;'>".validation_errors()."</span>" );
				}
				else
				{
					if($this->customer->save_customer_questions())
					{
						echo Modules::run('crm/leads/send_customer_question');
						echo Modules::run('crm/leads/respond_to_customer_question');
						$response = array("success" => TRUE ,"message" => "<span class='alert alert-success' style='display:block;'>Message has been Sent!</span>" );
					}
					else
					{
						$response = array("success" => FALSE ,"message" => "<span class='alert alert-danger' style='display:block;'>Failed to sent message!</span>" );
					}
				}

			}

			return exit(json_encode($response));
			
		}
	}
	
	function save_schedule_for_showing() {

		if( $this->input->is_ajax_request() )
		{	
			if( $this->customer->save_schedule_for_showing() )			
			{
				$this->sched_send_email_to_agent( $this->input->post() );
				$response = array("success" => TRUE, "message" => " Showing has been successfully saved!");
			}
			else
			{
				$response = array("success" => FALSE, "message" => " Failed to saved data!");
			}
			exit( json_encode($response) );
		}
	}

	public function sched_send_email_to_agent( $post = array() ) 
	{
	 	$this->load->library("email");
		
		$agent_profile = $this->customer->getAgentInfo();
		$contact_profile = $this->customer->getContactInfo();

		$content = "Hi ".$agent_profile->agent_name.", <br><br>

			You have a new schedule for showing! <br><br>

			Customer Name: ".$contact_profile->contact_name." <br><br>
			Customer Email Address: ".$contact_profile->email." <br><br>
			Property Name: ".$post["property_name"]." <br><br>
			Message: ".$post["sched_for_showing"]." <br><br>

			Regards, <br>

			Team AgentSquared
			";

	 	$subject ="New schedule for showing";

	 	return $this->email->send_email(
	 		'sales@agentsquared.com',
	 		'Agentsquared',
	 		$agent_profile->email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "rolbru12@gmail.com,joce@agentsquared.com"
	 		)
	 	);
	}
}
