<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends MX_Controller {

	function __construct() {

 		parent::__construct();

 		$userData = $this->session->userdata('user_id');

        if(empty($userData)) {
            $this->session->set_flashdata('flash_data', 'You don\'t have access!');
            redirect('login/auth/login', 'refresh');
        }
        
 		$this->load->model("customer_model", "customer");
 		$this->load->model("home_model");
 		$this->load->model("Page_model","page");

 	}

 	public function details($id = NULL) {

 		if($id) {

 			$dsl = modules::load('dsl/dsl');
 			$obj = Modules::load("mysql_properties/mysql_properties");
			$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
			$type = $this->home_model->getUserAcessType($this->session->userdata("agent_id"));

			if($type == 0) {
				$body = array('access_token' => $token->access_token);
			} else {
				$body = array('bearer_token' => $token->access_token);
			}

 			$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
 			$data["theme"] 	= $user_info->theme;
			$data["header"] = "header-page";
			$data["page"] 	= "property";
		
			$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
        	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
        	$SiteInfo = Modules::run('agent_sites/getInfo');
        	$data['branding'] = $SiteInfo['branding'];
			$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
			$data['mlsLogo'] = isset($data['system_info']->Configuration[0]->MlsLogos[0]->Uri) ? $data['system_info']->Configuration[0]->MlsLogos[0]->Uri : "" ;
			$data['mlsName'] = isset($data['system_info']->Mls) ? $data['system_info']->Mls : "";
			$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
			$data['property_id'] = $id;

			$data["user_info"] = $user_info;

 			$endpoints = array(
				'property_standard_info' => "property/".$id."/standard-fields/".$this->session->userdata('user_id'),
				'property_details' =>  "property/".$id."/custom-fields/".$this->session->userdata('user_id'),
			);

 			$standard_info = Modules::run('dsl/get_endpoint', $endpoints['property_standard_info']);
 			$data['property_standard_info'] = json_decode($standard_info,true);
 			$property_details = Modules::run('dsl/get_endpoint', $endpoints['property_details']);
 			$data['property_full_spec'] = json_decode($property_details, true);

 			$data["js"] = array("listings.js");

 			$this->load->view("page", $data);
 		}
 	}

 	public function other_property_details($id=NULL) {

 		if($id) {
 			
 			$dsl = modules::load('dsl/dsl');
 			$obj = Modules::load("mysql_properties/mysql_properties");
			$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
			$type = $this->home_model->getUserAcessType($this->session->userdata("agent_id"));

			if($type == 0) {
				$body = array('access_token' => $token->access_token);
			} else {
  				$body = array('bearer_token' => $token->access_token);
  			}

 			$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));

 			$data["theme"] 	= $user_info->theme;
			$data["header"] = "header-page";
			$data["page"] 	= "property";

			$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
        	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
        	$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
        	$SiteInfo = Modules::run('agent_sites/getInfo');
        	$data['branding'] = $SiteInfo['branding'];
			$data['system_info'] = $obj->get_account($this->session->userdata("user_id"), "system_info");
			$data['mlsLogo'] = isset($data['system_info']->Configuration[0]->MlsLogos[0]->Uri) ? $data['system_info']->Configuration[0]->MlsLogos[0]->Uri : "" ;
			$data['mlsName'] = isset($data['system_info']->Mls) ? $data['system_info']->Mls : "";
			$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
			$data['property_id'] = $id;

			$data["user_info"] = $user_info;

			$standard_info = $dsl->post_endpoint('spark-listing/'.$id.'/standard-fields', $body);
			$data['property_standard_info'] = json_decode($standard_info, true);
			$data['property_full_spec'] = $this->get_property_custom_field($id);

			$data["js"] = array("listings.js");
			
 			$this->load->view("page", $data);
 		}
 	}

 	public function get_property_custom_field($id=NULL) {

 		$ret=array();

 		if($id) {

			$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));

			if($token) {

	 			$url = "https://sparkapi.com/v1/listings/".$id."?_select=CustomFields&_expand=CustomFields";

				$headers = array(
					'Authorization:OAuth '.$token->access_token,
					'Accept:application/json',
					'X-SparkApi-User-Agent:MyApplication',
				);

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL, $url);

				$result = curl_exec($ch);
				$result_info = json_decode($result, true);

				if($result_info) {
					$ret["data"] = $result_info["D"]["Results"][0]["CustomFields"][0];
				}
			}
 		}

 		return $ret;
 	}

 	public function property_photos( $id= NULL) { 

 		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;

 		if($this->uri->segment(1) == "property-details"){

 			$endpoints = "property/".$id."/photos/".$this->session->userdata("user_id");
 			$data['photos'] = Modules::run('dsl/get_endpoint', $endpoints);

 		} else {

 			$dsl = modules::load('dsl/dsl');

 			$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
			
			$type = $this->home_model->getUserAcessType($this->session->userdata("agent_id"));
			
			if($type == 0) {
				$body = array(
					'access_token' => $token->access_token
				);
			}
			else {
				$body = array(
					'bearer_token' => $token->access_token
				);
			}

			$data['photos'] = $dsl->post_endpoint('spark-listing/'.$id.'/photos', $body);

 		}

		$this->load->view($theme.'/pages/property_view/photo-gallery',$data);
	
	}

	public function property_nearby( $id= NULL) { 

 		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;


 		if($this->uri->segment(1) == "property-details"){

 			$endpoints = "property/".$id."/standard-fields/".$this->session->userdata("user_id");

 			$standard_info = Modules::run('dsl/get_endpoint', $endpoints);
 			$property_standard_info = json_decode($standard_info,true);

 			$lat = ($property_standard_info["data"]["Latitude"]) ? $property_standard_info["data"]["Latitude"]  : "";
			$lon = ($property_standard_info["data"]["Longitude"]) ? $property_standard_info["data"]["Longitude"] :"";

			$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Pending' Or MlsStatus Eq 'Contingent')";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$nearby_listings = $dsl->post_endpoint('spark-nearby-listings?_expand=PrimaryPhoto&_filter=('.$filter_string.')&_lat='.$lat.'&_lon='.$lon.'', $body);
			
 			if($lat != "********" && $lon != "********"){
				$data['nearby_listings'] = json_decode($nearby_listings, true);
			} 

 		} else {

 			$dsl = modules::load('dsl/dsl');

 			$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
			
			$type = $this->home_model->getUserAcessType($this->session->userdata("agent_id"));
			
			if($type == 0) {
				$body = array(
					'access_token' => $token->access_token
				);
			}
			else {
				$body = array(
					'bearer_token' => $token->access_token
				);
			}

		$standard_info = $dsl->post_endpoint('spark-listing/'.$id.'/standard-fields', $body);
		$property_standard_info = json_decode($standard_info, true);

		if(!empty($property_standard_info["data"])){

				$lat = ($property_standard_info["data"]["Latitude"]) ? $property_standard_info["data"]["Latitude"]  : "";
				$lon = ($property_standard_info["data"]["Longitude"]) ? $property_standard_info["data"]["Longitude"] :"";
				
				$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Pending' Or MlsStatus Eq 'Contingent')";
				$filter_url = rawurlencode($filterStr);
				$filter_string = substr($filter_url, 3, -3);

				$nearby_listings = $dsl->post_endpoint('spark-nearby-listings?_expand=PrimaryPhoto&_filter=('.$filter_string.')&_lat='.$lat.'&_lon='.$lon.'', $body);

				if($lat != "********" && $lon != "********"){
					$data['nearby_listings'] = json_decode($nearby_listings, true);
				}
			} 

 		}

 		$data = isset($data) ? $data : "";

		$this->load->view($theme.'/pages/property_view/nearby-listings',$data);
	
	}

	public function property_virtual_tours( $id= NULL) { 

 		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;


 		if($this->uri->segment(1) == "property-details"){

 			$endpoints = "property/".$id."/virtual-tours/".$this->session->userdata("user_id");

 			$vt = Modules::run('dsl/get_endpoint', $endpoints);
 			$virtual_tours = json_decode($vt, true);
 			
 			$data['virtual_tours'] = "";
 			if( isset($virtual_tours['data'][0]['StandardFields']['VirtualTours']) ){
 				$data['virtual_tours'] = (!empty($virtual_tours['data'])) ? $virtual_tours['data'][0]['StandardFields']['VirtualTours'] : FALSE;
 			}

 			
 		} else {

 			$dsl = modules::load('dsl/dsl');

 			$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
			
			$type = $this->home_model->getUserAcessType($this->session->userdata("agent_id"));
			
			if($type == 0) {
				$body = array(
					'access_token' => $token->access_token
				);
			}
			else {
				$body = array(
					'bearer_token' => $token->access_token
				);
			}

 			$virtual_tours = $dsl->post_endpoint('spark-listing/'.$id.'/virtual-tours', $body);

 			$vt = json_decode($virtual_tours, true);
			if(!isset($vt['response'])){
				$data['virtual_tours'] = $vt['data'];
			}

 		}

 		$data = isset($data) ? $data : "";

		$this->load->view($theme.'/pages/property_view/virtual-tours',$data);
	}

	public function property_rooms( $id= NULL) { 

 		$user_info = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
		$theme = $user_info->theme;


		$dsl = modules::load('dsl/dsl');

		$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
		
		$type = $this->home_model->getUserAcessType($this->session->userdata("agent_id"));
		
		if($type == 0) {
			$body = array(
				'access_token' => $token->access_token
			);
		}
		else {
			$body = array(
				'bearer_token' => $token->access_token
			);
		}

			$rooms = $dsl->post_endpoint('spark-listing/'.$id.'/rooms', $body);
			$data['rooms'] = json_decode($rooms, true);

		$this->load->view($theme.'/pages/property_view/rooms',$data);
	
	}

}