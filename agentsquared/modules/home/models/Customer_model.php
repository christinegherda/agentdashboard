<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Customer_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function isHaveRecord($email=NULL) {
        $this->db->select("phone")
                ->from("contacts")
                ->where("email", $email)
                ->limit(1);

        $query = $this->db->get()->row();

        return (!empty($query)) ? $query : FALSE;
    }

    public function get_credits($domain = NULL) {
        // 
        $user = $this->db
                        ->select("agent")
                        ->from("domains")
                        ->like('domains',$domain, 'after')
                        ->where('type','agent-site')
                        ->get()->row_array();

        if(isset($user['agent'])) {
            #return $user['agent'];
            $idx = $this->db
                    ->select("*")
                    ->from("property_idx")
                    ->where('user_id', $user['agent'])
                    ->get()->row_array();
            return $idx;

        }
        else {
            return false;
        }
    }

    public function sign_up($data=array()) {

        if($data) {

            $this->db->insert('contacts', $data);

            $customer_id = $this->db->insert_id();

            $customer["customer_id"] = $customer_id;
            $customer["customer_email"] = $data['email'];
            $customer["contact_id"] = $data["contact_id"];

            //set session
            $this->set_customer_session($customer);

            return $customer_id;
        }

    }

    public function sign_in() {  

        $email = $this->input->post("email");
        $phone = $this->input->post("phone");

        $customer = $this->db->select("*")
                        ->from("contacts")
                        ->where("email", $email)
                        ->where("phone", $phone)
                        ->get()->row_array();

        if(!empty($customer)) {

            $c_session["customer_id"] = $customer["id"];
            $c_session["customer_email"] = $customer["email"];
            $c_session["contact_id"] = $customer["contact_id"];

            $save_properties = $this->get_save_properties($customer["id"]);
            
            $c_session["save_properties"] = $save_properties;

            $this->set_customer_session($c_session);
            
            if($this->input->post("isHavePropertyId")) {
                $this->save_favorates_properties($_POST["isHavePropertyId"]);
            }

            if($this->input->post('isHaveSearchPropertyUrl')) {
                $this->save_search_properties( $_POST["isHaveSearchPropertyUrl"] );
            }

            return TRUE;
        }

        return FALSE;
    }

    /*public function sign_up($ContactId = NULL)
    {
        if(empty($ContactId)) {
            $ContactId = generate_key("10");
        }

        $email = $this->input->post("email");
        $password = $this->input->post("password");

        $type = ( isset($_POST["type"]) AND !empty($_POST["type"]) ) ? $_POST["type"] : "signup_form";

        $arr = array(
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'first_name' => $this->input->post("fname"),
            'last_name' => $this->input->post("lname"),
            'email' => $email,
            'phone' => $this->input->post("phone"),
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'active' => 1,
            'type' => $type,
            'user_id' => $this->session->userdata("user_id"),
            'agent_id' => $this->session->userdata("agent_id"),
            'contact_id' => $ContactId,
            'created' => date("Y-m-d H:m:s"),
            'modified' => date("Y-m-d H:m:s")
        );
        
        if($this->db->insert('contacts', $arr)) {

            $customer_id = $this->db->insert_id();

            $customer["customer_id"] = $customer_id;
            $customer["customer_email"] = $email;
            $customer["contact_id"] = $arr["contact_id"];

            //set session
            $this->set_customer_session($customer);

            return $customer_id;
        }

       return FALSE;

    }*/

   /* public function sign_in()
    {   
        $email = $this->input->post("email");
        $phone = $this->input->post("phone");
        //$password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);

        $customer = $this->db->select("*")
                        ->from("contacts")
                        ->where("email", $email)
                        ->get()->row_array();

        if(!empty($customer)) {
            
            if( password_verify($this->input->post("password"), $customer["password"]) ) {

                $c_session["customer_id"] = $customer["id"];
                $c_session["customer_email"] = $customer["email"];
                $c_session["contact_id"] = $customer["contact_id"];

                $save_properties = $this->get_save_properties($customer["id"]);
                
                $c_session["save_properties"] = $save_properties;

                $this->set_customer_session( $c_session );
                
                if( isset( $_POST["isHavePropertyId"] ) )
                {
                    $this->save_favorates_properties( $_POST["isHavePropertyId"] );
                }

                if( isset( $_POST["isHaveSearchPropertyUrl"] ) AND !empty( $_POST["isHaveSearchPropertyUrl"] ) )
                {
                    $this->save_search_properties( $_POST["isHaveSearchPropertyUrl"] );
                }

                return TRUE;

            } else {

                return FALSE;
            }
            
        }

        return FALSE;
    }*/

    public function set_new_save_property_session( $customer_id = NULL )
    {
        $save_properties = $this->get_save_properties($customer_id);
        $c_session["save_properties"] = $save_properties;

        $this->set_customer_session( $c_session );

        return TRUE;
    }

    public function get_save_properties( $customer_id = NULL )
    {
        $property = $this->db->select("property_id")
                        ->from("customer_properties")
                        ->where("customer_id", $customer_id)
                        ->get()->result_array();

        return $property;
    } 

    public function set_customer_session( $customer = array() )
    {
        if( !empty( $customer ) )
        {
            $this->session->set_userdata( $customer );
        }

        return TRUE;
    }

    public function get_customer_properties()
    {
        $this->db->select('*')
                ->from('customer_properties')
                ->where("customer_id", $this->session->userdata("customer_id"))
                ->where("user_id", $this->session->userdata("user_id"))
                ->order_by("cpid");

        return $this->db->get()->result();
    }

    public function get_customer_searches()
    {
        $this->db->select('*')
                ->from('customer_searches')
                ->where("customer_id", $this->session->userdata("customer_id"))
                ->where("user_id", $this->session->userdata("user_id"))
                ->order_by("csid");

        return $this->db->get()->result();
    }


    public function getSaveSearchesFlex()
    {

        $this->db->select("id,json_saved_search_updated,img,date_modified")
                ->from("saved_searches")
                ->where("FIND_IN_SET('".$this->session->userdata("contact_id")."',ContactIds) != ",0);

        $query = $this->db->get(); 

        if( $query->num_rows() > 0 )
        {
            $data = $query->result();
            
            foreach ($data as &$key) {
                $key->json_decoded = json_decode($key->json_saved_search_updated);
            }


            return $data;
        }

        return FALSE;
    }

    public function change_password( $new_password = NULL )
    {
        $update["password"] = password_hash($new_password, PASSWORD_DEFAULT);
        $this->db->where( "id", $this->session->userdata("customer_id") );

        $this->db->update("contacts", $update);

        return ( $this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function save_favorates_properties( $property_id = NULL )
    {
        $this->db->select("*")
                ->from("customer_properties")
                ->where("property_id", $property_id)
                ->where("customer_id", $this->session->userdata("customer_id"))
                ->where("user_id", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get();

        if( $query->num_rows() > 0)
        {
            return TRUE;
        }

        $save["property_id"] = $property_id;
        $save["user_id"] = $this->session->userdata("user_id");
        $save["customer_id"] = $this->session->userdata("customer_id");
        $save["date_created"] = date("Y-m-d");
        $save["date_modified"] = date("Y-m-d");

        if( $this->db->insert("customer_properties", $save) )
        {
            $save_properties = $this->get_save_properties( $this->session->userdata("customer_id") );                
            $c_session["save_properties"] = $save_properties;
            $this->set_customer_session( $c_session );

            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function delete_customer_property( $id = NULL )
    {
        $this->db->where("cpid", $id);
        $this->db->delete("customer_properties");

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function save_search_properties( $url = "" , $details = "" )
    {
        $arr = array();
        $arr = $_GET;
        $json_search = json_encode($arr);

        $url2 = explode("=", $url);
       
        $save["url"] = $url;
        $save["user_id"] = $this->session->userdata("user_id");
        $save["customer_id"] = $this->session->userdata("customer_id");
        $save["details"] = (isset($url2[6]) AND !empty($url2[6])) ? $url2[6] : (isset($url2[1]) AND !empty($url2[1])) ? str_replace("&type", " ", $url2[1]) : "";
        $save["json_search"] = $json_search;
        $save["date_created"] = date("Y-m-d");
        $save["date_modified"] = date("Y-m-d");
        
        if( $this->db->insert("customer_searches", $save) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function delete_customer_search( $id = NULL )
    {
        $this->db->where("csid", $id);
        $this->db->delete("customer_searches");

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function customer_edit_profile( $profile_id = NULL )
    {
        $profile = $this->input->post("profile");
        $profile['created'] = date("Y-m-d");
        $profile['customer_id'] = $this->session->userdata("customer_id");
        
        if( $profile_id )
        {
            $this->db->where("id", $profile_id);
            $this->db->update("customer_profile", $profile);

            if( $this->db->affected_rows() > 0 )
            {
                //update email to user table
                $this->db->where("id",$this->session->userdata("customer_id"));
                $this->db->update("users", array("email" => $profile['email'], "first_name" => $profile['first_name'], "last_name" => $profile['last_name']) );
            
                return TRUE;
            }

            return FALSE ;
        }
        else
        {
            $this->db->insert("customer_profile", $profile);
            $pro_id = $this->db->insert_id();
            if( $pro_id )
            {
                $this->db->where("id",$this->session->userdata("customer_id"));
                $this->db->update("users", array("email" => $profile['email'], "first_name" => $profile['first_name'], "last_name" => $profile['last_name']) );
            }
      
            return ($pro_id) ? $pro_id : FALSE ;
        }
    }
    public function update_contact_profile( $profile_id = NULL )
    {
        $profile = $this->input->post("profile");
        $profile['modified'] = date("Y-m-d H:i:s");
        //printA( $profile ); exit;
        $this->db->where("id", $profile_id);
        $this->db->update("contacts", $profile);

        return ( $this->db->affected_rows() > 0 ) ? TRUE : FALSE;
    }

    public function get_customer_profile()
    {   
        $this->db->select("*")
                ->from("contacts c")
                ->where("c.id", $this->session->userdata("customer_id"))
                ->limit(1); 

        return $this->db->get()->row();
    }

    public function update_email_preferences_settings()
    {
        
        $this->db->where("customer_id", $this->session->userdata("customer_id"));
        $this->db->update("customer_profile", array("email_settings" => $this->input->post("settings")));

        return ( $this->db->affected_rows() ) ? TRUE : FALSE ;
    }

    public function save_customer_buyer()
    {
        $buyer = $this->input->post("buyer");
        $password = "buyer1234";

        $save = array(
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'first_name' => $buyer["fname"],
            'last_name' => $buyer["lname"],
            'email' => $buyer["email"],
            'password' => password_hash( $password, PASSWORD_DEFAULT ),
            'phone' => $buyer["phone"],
            'address' => $buyer["address"],
            'mobile' => $buyer["mobile"],
            'city' => $buyer["city"],
            'state' => $buyer["state"],
            'zipcode' => $buyer["zip_code"],
            'active' => 1,
            'type' => "buyer",
            'user_id' => $this->session->userdata("user_id"),
            'agent_id' => $this->session->userdata("agent_id"),
            'contact_id' => generate_key("10"),
            'created' => date("Y-m-d H:m:s"),
            'modified' => date("Y-m-d H:m:s")
        );

        if($this->db->insert("contacts", $save)){
            $id = $this->db->insert_id();
            $buyer["agent_id"] = $this->session->userdata("user_id");
            $buyer["date_created"] = date("Y-m-d H:i:s");
            $buyer["date_modified"] = date("Y-m-d H:i:s");
            $buyer["contact_id"] = $id;
            if($this->db->insert("customer_buyer", $buyer)) {
                return $this->db->insert_id();
            }
            return FALSE;
        }
        return FALSE;
    }

    public function save_customer_seller()
    {
        $seller = $this->input->post("seller");
        $password = "seller1234";

        $save = array(
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'first_name' => $seller["fname"],
            'last_name' => $seller["lname"],
            'email' => $seller["email"],
            'phone' => $seller["phone"],
            'password' => password_hash( $password, PASSWORD_DEFAULT ),
            'active' => 1,
            'type' => "seller",
            'user_id' => $this->session->userdata("user_id"),
            'agent_id' => $this->session->userdata("agent_id"),
            'contact_id' => generate_key("10"),
            'created' => date("Y-m-d H:m:s"),
            'modified' => date("Y-m-d H:m:s")
        );

        // save for contacts
        if($this->db->insert("contacts", $save)) {
            $id = $this->db->insert_id();
            $seller["agent_id"] = $this->session->userdata("agent_id");
            $seller["date_created"] = date("Y-m-d H:i:s");
            $seller["contact_id"] = $id;

            if($this->db->insert("customer_seller", $seller)) {
                return $this->db->insert_id();
            }
        }

        return FALSE;
    }

    public function save_customer_questions() {

        $password = "question1234";

        $contact = array(
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'first_name' => $this->input->post("name"),
            'email' => $this->input->post("email"),
            'phone' => $this->input->post("phone"),
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'active' => 1,
            'type' => "question",
            'user_id' => $this->session->userdata("user_id"),
            'agent_id' => $this->session->userdata("agent_id"),
            'contact_id' => generate_key("10"),
            'created' => date("Y-m-d H:m:s"),
            'modified' => date("Y-m-d H:m:s")
        );

        if($this->db->insert("contacts", $contact)) {

            $cid = $this->db->insert_id();

            $message["name"] = $this->input->post("name");
            $message["email"] = $this->input->post("email");
            $message["phone"] = $this->input->post("phone");
            $message["status"] = 0;
            $message["agent_id"] = $this->session->userdata("user_id");
            $message["date_created"] = date("Y-m-d H:i:s");

            if($this->db->insert("customer_questions", $message)) {

                $id = $this->db->insert_id();

                $save["mid"] = $id;
                $save["message"] = $this->input->post("message");
                $save["customer_id"] = $cid;
                $save["agent_id"] = $this->session->userdata("user_id");
                $save["type"] = 0;
                $save["created"] = date("Y-m-d H:i:s");

                if($this->db->insert("customer_messaging", $save)) {
                    return  $id;
                }
            }
        }
        return FALSE;
    }

    public function save_schedule_for_showing()
    {
        $schedule["message"] = $this->input->post("sched_for_showing");
        $schedule["property_id"] = $this->input->post("property_id");
        $schedule["customer_id"] = $this->session->userdata("customer_id");
        $schedule["customer_email"] = $this->session->userdata("customer_email");
        $schedule["created"] = date("Y-m-d H:m:s");
        $schedule["agent_id"] =$this->session->userdata("user_id");
        $schedule["type"] = "agent_site";
        $schedule["property_name"] = $this->input->post("property_name");
        $schedule["date_scheduled"] = $this->input->post("date_scheduled");

        if( $this->db->insert("customer_schedule_of_showing", $schedule) )
        {
            return $this->db->insert_id();
        }

        return FALSE;
    }

    public function is_email_exist()
    {
        $this->db->select("email")
                ->from("contacts")
                ->where("email", $this->input->post("email"))
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->limit(1);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? TRUE : FALSE;
    }

    public function is_registered( $infos = array() )
    { 
        $array = array(
            'social_id' => $infos["id"],
            'email'     => $infos["email"],
            'user_id'   => $this->session->userdata("user_id"),
            'agent_id'  => $this->session->userdata("agent_id"),
            'is_deleted' => 0
        );

        $this->db->select("*")
                ->from("contacts")
                ->where($array)
                ->limit(1);

        $query = $this->db->get();

        return ( $query->num_rows() > 0 ) ? $query->row_array() : FALSE;

    }

    public function insert_fb_registration( $infos = array() , $contactId = NULL )
    {
        $contactId = ( !empty($contactId) ) ? $contactId : generate_key("10"); 
        $name = explode(" ", $infos["name"] );

        $type = "facebook";

        $arr = array(
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'first_name' => (isset($name[0])) ? $name[0] : "",
            'last_name' => (isset($name[1])) ? $name[1] : "",
            'email' => $infos["email"],
            'active' => 1,
            'type' => $type,
            'user_id' => $this->session->userdata("user_id"),
            'agent_id' => $this->session->userdata("agent_id"),
            'contact_id' => $contactId,
            'created' => date("Y-m-d H:m:s"),
            'modified' => date("Y-m-d H:m:s"),
            'social_id' =>$infos["id"],
        );
        
        if(  $this->db->insert('contacts', $arr) )
        {
            $customer_id = $this->db->insert_id();
            
            $customer["customer_id"] = $customer_id;
            $customer["customer_email"] = $infos["email"];
            //set session

            $this->set_customer_session( $customer );

            return $customer_id;
        }
    }

    public function get_contact_send_email()
    {
        $this->db->select("customer_messaging.*, CONCAT(contacts.first_name ,' ', contacts.last_name) as contact_name, CONCAT(users.first_name ,' ', users.last_name) as agent_name ")
                ->from("customer_messaging")
                ->join("contacts", "customer_messaging.contact_id = contacts.contact_id", "left")
                ->join("users", "users.agent_id = customer_messaging.agent_id", "left")
                ->where("customer_messaging.customer_id", $this->session->userdata("customer_id"))
                ->where("customer_messaging.agent_id !=", 0);


        return $this->db->get()->result();    
    }

    public function save_email(){

        $save["customer_id"] =  $this->session->userdata("customer_id");
        $save["contact_id"] =  $this->session->userdata("contact_id");
        $save["agent_id"] =  $this->session->userdata("agent_id");
        $save["sent_by"] =  $this->session->userdata("customer_id");
        $save["created"] =  date("Y-m-d H:m:s");
        $save["type"] = 0;
        $save["message"] = $_POST["message"];

        $this->db->insert("customer_messaging", $save);

        return ( $this->db->insert_id() ) ? $this->db->insert_id() : FALSE;
    }

    public function getContactInfo()
    {
        $this->db->select("CONCAT(first_name ,' ', last_name) as contact_name,email")
                ->from("contacts")
                ->where("id", $this->session->userdata("customer_id"))
                ->limit(1);

        return $this->db->get()->row();
    }

    public function getAgentInfo()
    {
        $this->db->select("CONCAT(first_name ,' ', last_name) as agent_name,email")
                ->from("users")
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->limit(1);

        return $this->db->get()->row();
    }

    public function set_email_schedule($data=array()) {

        $ret = FALSE;

        if($data) {
            $this->db->where('id', $this->session->userdata('customer_id'));
            $this->db->update('contacts', $data);

            $ret = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        }
        
        return $ret;
    }

    public function get_contact_subscription() {

        $this->db->select('is_subscribe_email, email_schedule')
                ->from('contacts')
                ->where('id', $this->session->userdata('customer_id'));

        return $this->db->get()->row();
    }

    public function set_email_subscription($data=array()) {
        $this->db->where('id', $this->session->userdata('customer_id'));
        $this->db->update('contacts', $data);
    }
}