<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Blog_model extends CI_Model { 

	public function __construct()
    {
        parent::__construct();
    }
	// --------------------------------------

    public function get_all_blogs( $counter = FALSE,  $param = array() )
    {
        
        $this->db->select("*")
                ->from("blogs")
                ->where("status","published")
                ->where("author", $this->session->userdata('user_id'))
                ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function get_blog( $slug = NULL )
    {
        $array = array(
            'slug' => $slug,
            'author' => $this->session->userdata('user_id')
             );
        $this->db->select("*")
                ->from("blogs")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function filter_archives($filter = NULL, $counter = FALSE,  $param = array() )
    {
        $this->db->select("*")
                ->from("blogs")
                ->where("status" , "published")
                ->where("author" , $this->session->userdata('user_id'))
                ->where("post_date like '%".$filter."%'")
                ->order_by("id","desc");


        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }


        if( $counter )
        {
            return $this->db->count_all_results();
        }
        

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function get_recent_post()
    {

        $this->db->select("*")
                ->from("blogs")
                ->where("author", $this->session->userdata('user_id'))
                ->where("status","published")
                ->order_by("id", "desc")
                ->limit("3");

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function get_all_blog_archives()
    {

        $this->db->select("year(post_date) as `year`, monthname(post_date) as `month_name`, count(*) as `total`, post_date")
                ->from ("blogs")
                ->group_by("concat(year(post_date),month(post_date))") 
                ->order_by("post_date", "desc")
                ->where("status","published")
                ->where("author", $this->session->userdata('user_id'));

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

   
}