<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Home_model extends CI_Model { 

	public function __construct() {
        parent::__construct();
    }

    public function getUsersInfo($user_id = NULL) {
        $this->db->select("*")
                ->from("users")
                ->where("id", $user_id);

        return $this->db->get()->row();
    }
    public function getUserAcessType($agent_id = NULL) {
        /* Distinguising between Bearer and Auth Token users */
        $this->db->select("auth_type")
                ->from("users")
                ->where("agent_id", $agent_id);
        $ret = $this->db->get()->row();
        $type = $ret->auth_type;

        return $type;
    }
    public function getUserAccessToken( $agent_id = NULL ) {
        /* Distinguising between Bearer and Auth Token users */
        $this->db->select("auth_type")
                ->from("users")
                ->where("agent_id", $agent_id);
        $ret = $this->db->get()->row();
        $type = $ret->auth_type;

        if($type) {
             $this->db->select("bearer_token as access_token")
                ->from("user_bearer_token")
                ->where("agent_id", $agent_id);
        }
        else {
             $this->db->select("access_token")
                ->from("users_idx_token")
                ->where("agent_id", $agent_id);
        }
        
        return $this->db->get()->row();
    }

    public function getSliderPhotos() {
        $this->db->select("*")
                ->from("slider_photos")
                ->where("user_id", $this->session->userdata("user_id"));

        return $this->db->get()->result();
    }

    public function get_capture_leads() {

        $this->db->select('capture_leads, is_email_market')
                ->from('users')
                ->where('agent_id', $this->session->userdata("agent_id"));

        return $this->db->get()->row();
    }
    
     public function get_active_featured(){
        $array = array(
            'is_featured' => 1,
            'option_name' => 'active_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

     public function get_new_featured(){

        $array = array(
            'is_featured' => 1,
            'option_name' => 'new_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

     public function get_nearby_featured(){

        $array = array(
            'is_featured' => 1,
            'option_name' => 'nearby_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

     public function get_office_featured(){

        $array = array(
            'is_featured' => 1,
            'option_name' => 'office_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function get_active_title(){

        $array = array(
            'option_name' => 'active_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function get_nearby_title(){

        $array = array(
            'option_name' => 'nearby_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function get_new_title(){

        $array = array(
            'option_name' => 'new_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function get_office_title(){

        $array = array(
            'option_name' => 'office_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }
    
    public function get_active_selected(){

        $array = array(
            'option_value' => 1,
            'is_featured'  => 0,
            'option_name' => 'active_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

     public function get_new_selected(){

        $array = array(
            'option_value' => 1,
            'is_featured'  => 0,
            'option_name' => 'new_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

     public function get_nearby_selected(){

        $array = array(
            'option_value' => 1,
            'is_featured'  => 0,
            'option_name' => 'nearby_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

     public function get_office_selected(){

        $array = array(
            'option_value' => 1,
            'is_featured'  => 0,
            'option_name' => 'office_listings',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function is_custom_home(){
        $array = array(
            'option_value' => 1,
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function save_testimonial($data=array()) {
        $ret = FALSE;
        if($data) {
            $this->db->insert('testimonials', $data);
            $ret = TRUE;
        }
        return $ret;
    }


    public function save_whitelisted_ips($data = array()) {
        $ret = FALSE;
        if($data) {
            $this->db->insert('whitelisted_ips', $data);
            $ret = TRUE;
        }
        return $ret;
    }

    public function get_sendgrid_rule_id_by_ip($ip) {
        $this->db->select("rule_id")
            ->from("whitelisted_ips")
            ->where("ip_address", $ip);

         return $this->db->get()->row();
    }

    
}   