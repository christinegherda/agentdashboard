<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Page_model extends CI_Model { 

	public function __construct()
    {
        parent::__construct();
    }
	
    public function get_page($slug = NULL )
    {
        $array = array(
            'slug' => $slug,
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("pages")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function get_search_id($slug = NULL)
    {
        $array = array(
            'slug' => $slug,
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("saved_search_id")
                ->from("pages")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function get_seller_page()
    {
        $array = array(
            'slug' => 'seller',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("pages")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function get_buyer_page()
    {
        $array = array(
            'slug' => 'buyer',
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("*")
                ->from("pages")
                ->where($array);

        return $this->db->get()->row_array();
    }


   
}