<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['facebook_app_id']              = '867016703396837';
$config['facebook_app_secret']          = 'd08f2f4416bd0823e6b93dedba356613';
$config['facebook_login_type']          = 'web';
$config['customer_facebook_login_redirect_url']  = 'home/facebook_login';
$config['facebook_logout_redirect_url'] = 'home/deactivate_facebook';
$config['facebook_permissions']         = array('public_profile','email');