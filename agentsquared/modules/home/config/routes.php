<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['home'] = 'home';
$route['find-a-home'] = 'home/find_a_home';
$route['contact'] = 'home/contact';
$route['about'] = 'home/about';
$route['buyer'] = 'home/buyer';
$route['seller'] = 'home/seller';
$route['testimonials'] = 'home/testimonials';
$route['blog'] = 'home/blog';

$route['property'] = 'home/property';
$route['listings'] = 'home/listings';
$route['property-details/(:num)'] = 'home/property/details/$1';
$route['other-property-details/(:num)'] = 'home/property/other_property_details/$1';

$route['home/customer-dashboard'] = 'home/customer_dashboard';
$route['customer/customer-dashboard'] = 'home/customer/customer_dashboard';
$route['customer-dashboard'] = 'home/customer/customer_dashboard';
$route['login'] = 'home/login';
$route['signup'] = 'home/signup';
$route['logout'] = 'home/logout';
$route['customer/change_password'] = 'home/customer/change_password';
$route['customer/delete_customer_property/(:num)'] = 'home/customer/delete_customer_property/(:num)';
$route['recommended-search'] = 'home/recommended_search';
$route['custom_domain/domain'] = 'custom_domain/aws_domain/domain';