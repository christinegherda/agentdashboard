<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>
 <div class="content-wrapper"> 
	  <div class="page-title">
	    <h3 class="heading-title">
	      <span> Edit Media </span>
	    </h3>
	  </div>         
     <section class="content">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                	<form action="<?=base_url()?>media/edit_media?media_id=<?php echo $info["id"]?>" method="POST" accept-charset="utf-8">
                		<input type="hidden" name="media[file_type]" value="<?=$info['file_type']?>">
	                    <div class="col-md-12 mb-30px">
	                        <a href="<?php echo site_url("media"); ?>" class="btn btn-default btn-submit"><i class="fa fa-arrow-left"></i> Back to Media Library</a>
	                    </div>
	                    <div class="col-md-12">	                    	
                    		<?php 
                    			$session = $this->session->flashdata('session'); 	                    			
                    			if(!empty($session)) { ?>
                    				<div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
                					<div class="alert alert-info alert-dismissable">
									    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
									    <a href="<?php echo site_url("pages"); ?>" target="_blank"><strong>Click Here!</strong></a> to add media to your page.
									</div>	
                    		<?php } ?>
	                     </div>

	                    <div class="col-md-8">

	                    	<input type="text" name="media[title]" value='<?php echo (isset($info["title"])) ? $info["title"] : "" ?>' class="form-control" maxlength="30" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _">

		                    	 <?php if($info["is_freemium"] == "1"){

	                                $href = getenv('AWS_S3_ASSETS') . "uploads/file/".$info["file_name"];
	                            } else{
	                                
	                                $href = base_url()."assets/upload/file/".$info["file_name"];
	                            }?>

	                    	<?php if($info["file_type"] === "application/pdf"){?>

                            	<p class="media-icon"><img src='<?php echo base_url()?>assets/images/pdf-icon.png' alt='<?php echo $info["alt_text"]?>' width="80" height="80"></p><br>

                           <?php } elseif($info["file_type"] === "application/vnd.openxmlformats-officedocument.word" || $info["file_type"] === "application/msword" ) { ?>

                            	<p class="media-icon"><img src='<?php echo base_url()?>assets/images/doc-icon.png' alt='<?php echo $info["alt_text"]?>' width="80" height="80"></p><br>

                           <?php } elseif($info["file_type"] === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){?>

                           		<p class="media-icon"><img src='<?php echo base_url()?>assets/images/excel-icon.png' alt='<?php echo $info["alt_text"]?>' width="80" height="80"></p><br>

                           	<?php } elseif($info["file_type"] === "text/plain"){?>

                           		<p class="media-icon"><img src='<?php echo base_url()?>assets/images/txt-icon.png' alt='<?php echo $info["alt_text"]?>' width="80" height="80"></p><br>

                           	<?php } elseif($info["file_type"] === "text/csv"){?>

                           		<p class="media-icon"><img src='<?php echo base_url()?>assets/images/csv-icon.png' alt='<?php echo $info["alt_text"]?>' width="80" height="80"></p><br>

                           	<?php } else {?>

                           		<p class="media-icon"><img src='<?php echo $href?>' alt='<?php echo $info["alt_text"]?>' width="80" height="80"></p><br>

                           <?php }?>

                            <label for="media-link">Image Link</label>
							<input type="text" name="media[link]" value='<?php echo (isset($info["file_name"])) ? $href : "" ?>' class="form-control" disabled> <br>

	                        <p><label for="media-caption">Caption</label>
	                        <input type="text" name="media[caption]" value='<?php echo (isset($info["caption"])) ? $info["caption"] : "" ?>' class="form-control" maxlength="100" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>

							<p><label for="media-alt-text">Alternative Text</label>
	                    	<input type="text" name="media[alt_text]" value='<?php echo (isset($info["alt_text"])) ? $info["alt_text"] : "" ?>' class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>
	
	                        <p><label for="media-description">Description</label>
	                        <input type="text" name="media[description]" value='<?php echo (isset($info["description"])) ? $info["description"] : "" ?>' class="form-control" maxlength="200" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>

	                    </div>
	                    <div class="col-md-4">
	                        <div class="media-file-info">
                                <p class="media-file-uploaded"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Uploaded on:</span> <?php echo date("M d, Y @ g:i ", strtotime($info["post_date"])) ?></p>

                                <?php if($info["is_freemium"] == "1"){

	                                $href = getenv('AWS_S3_ASSETS') . "uploads/file/".$info["file_name"];
	                            } else{
	                                
	                                $href = base_url()."assets/upload/file/".$info["file_name"];
	                            }?>

                                <p class="media-file-url"><label for="attachment_url">File URL:</label>
								
                               	<span style="display:none" id="copyMediaUrl"><?php echo $href ?></span>
                                <span style="padding: 3px;margin: 5px;" class="btn btn-primary" onclick="copyToClipboard('media_url')">Copy Path</span>
                                <span style="display:none" class="copied-success copied-status text-success">Copied to clipboard!</span>
                                <span style="display:none" class="copied-failed copied-status text-danger">Failed to copy!</span>

                                <input type="text" readonly="readonly" name="media_url" id="media_url" value="<?php echo $href ?>"></p>
								 <p class="media-file-name"><span>File name:</span> <?php echo $info["file_name"]?></p>

								 <?php if($info["file_type"] === "application/pdf"){?>
								 	<p class="media-file-type"><span>File type:</span> PDF</p>

								 <?php } elseif($info["file_type"] === "application/vnd.openxmlformats-officedocument.word" || $info["file_type"] === "application/msword") {?>
								 	<p class="media-file-type"><span>File type:</span> DOCX</p>
								 	
								 <?php } elseif($info["file_type"] === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){?>

								 		<p class="media-file-type"><span>File type:</span> XLSX</p>

								 <?php } elseif($info["file_type"] === "text/plain"){?>

								 		<p class="media-file-type"><span>File type:</span> TXT</p>

								 <?php } elseif($info["file_type"] === "text/csv"){?>

								 		<p class="media-file-type"><span>File type:</span> CSV</p>
								 		
								 <?php } else {?>
									<p class="media-file-type"><span>File type:</span> <?php echo strtoupper($info["file_type"])?></p>
								 <?php }?>
								
								 <?php
									$_media = new Media ();
									$size = $_media -> size_as_kb($info["file_size"]);?>
									 <p class="media-file-size"><span>File size:</span> <?php echo $size;?></p>
								<?php ?>

								 <?php if($info["is_freemium"] !== "1" && $info["file_type"] !== "application/pdf" && $info["file_type"] !== "application/vnd.openxmlformats-officedocument.word"){?>
								 	 <p class="media-file-dimensions"><span>Dimensions:</span> <?php echo $info["image_width"]?> x <?php echo $info["image_height"]?></p>
								 <?php }?>

	                            <button class="btn btn-default btn-submit">Update</button> 
	                        </div>
	                    </div>
               		 </form>
                </div>
            </div>
        </div>
    </div>
   </section>
</div>
<?php $this->load->view('session/footer'); ?>
