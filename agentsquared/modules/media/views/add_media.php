<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>

    <div class="content-wrapper"> 
          <div class="page-title">
            <h3 class="heading-title">
              <span> Add Media </span>
            </h3>
          </div>         
         <section class="content">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mb-30px">
                            <a href="<?php echo site_url("media"); ?>" class="btn btn-default btn-submit"><i class="fa fa-arrow-left"></i> Back to Media Library</a>
                        </div>
                        <div class="col-md-12 media-upload">
                            <form id="upload" method="POST" action="<?= base_url()?>media/add_media" enctype="multipart/form-data">
                                <div id="drop" class="imageupload">Drop files anywhere to upload<br>or<br><a>Select Files</a><br><br><br>
                                    <input class="hide-choose-files" type="file" name="userfile" multiple="true" />
                                </div>
                                <ul>
                                    <!-- The file uploads will be shown here -->
                                </ul>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-default btn-save btn-submit pull-right" ><i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Upload</button>
                                </div>
                            </form>
                        <p class="max-upload">Maximum upload file size: <span>10 MB</span></p>
                        <p class="allowed-type">Allowed file type: <span>jpg</span> | <span>jpeg</span> | <span>png</span> | <span>gif</span> | <span>pdf</span> | <span>doc</span>| <span>docx</span>| <span>xls</span>| <span>xlsx</span>| <span>csv</span> | <span>txt</span></p>
                     </div>
                    </div>
                </div>
            </div>
       </section>
    </div>

 <?php $this->load->view('session/footer'); ?>