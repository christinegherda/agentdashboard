<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>

    <div class="content-wrapper">   
      <div class="page-title">
        <h3 class="heading-title">
          <span> Media Library </span>
        </h3>
      </div>      
        <section class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-12 mb-30px">

                    <a href="#" class="btn-show-upload-aws btn btn-default btn-submit mb-30px"><i class="fa fa-plus"></i> Add Media</a>

                     <div style="display:none" class="show-upload-aws mb-30px">
                            <form id="upload-slider-photo-aws" method="POST" action="<?php echo base_url()?>media/add_media" enctype="multipart/form-data">

                                <div style="margin-bottom: 5px" class="input-group imageupload">
                                    <input type="file" name="userfile" class="filestyle" data-buttonBefore="true" data-icon="false">
                                </div>

                                <small>Max. upload file size: <strong>10 MB</strong></small> | <small>Allowed file type: <strong>jpg</strong> | <strong>jpeg</strong> | <strong>png</strong> | <strong>gif</strong> | <strong>pdf</strong> | <strong>doc</strong> | <strong>docx</strong> | <strong>xls</strong> | <strong>xlsx</strong> | <strong>csv</strong> | <strong>txt</strong></small>

                                <div style="padding-top:20px" class="box-footer">
                                    <button type="submit" class="btn btn-default btn-save upload-slider-photo btn-submit"> <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Upload</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="page-content media-grid-view">
                        <div class="col-md-12 mb-30px" >
                            <div class="row">
                                <div class="col-lg-4 col-md-8 col-sm-9 mode">
                                    <div class="view-switch">
                                        <a href="<?php echo base_url()?>media?mode=list" class="view-list" id="view-switch-list"><i class="fa fa-2x fa-list" aria-hidden="true"></i></a>
                                        <a href="<?php echo base_url()?>media?mode=grid" class="view-grid <?php echo ($_GET['mode'] === "grid") ? "current" : "" ?>" id="view-switch-grid"><i class="fa fa-2x fa-th" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <form action="" method="POST" id="bulk-action-grid">
                                             <select name="bulk_action" id="bulk-select-grid" class="form-control">
                                                <option>Bulk Actions</option>
                                                <option value="delete">Delete Permanently</option>
                                             </select>
                                         </form>
                                    </div>
                                    <div class="btn-group">
                                        <button type="submit" form="bulk-action-grid" class="btn btn-default btn-add-featured">Apply</button>
                                    </div>
                           
                                    <div class="btn-group">
                                        <form action="" method="POST" id="filter-date-grid">
                                             <select name="filter_month" id="select-pages" class="form-control">
                                                <option>All Dates</option>
                                                <?php if( !empty($group_published_media) ) { 
                                                    foreach($group_published_media as $media){?>

                                                        <option value="?filter_date=<?php echo date("Y-m", strtotime($media->post_date)); ?>"> <?php echo date("F Y", strtotime($media->post_date)); ?></option>
                                                <?php 
                                     
                                                 }

                                                    } 
                                                ?>         
                                            </select>
                                        </form>   
                                    </div>
                                    <div class="btn-group">
                                        <button type="submit" form="filter-date-grid" class="btn btn-default btn-add-featured">Filter</button>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-3">
                                    <form action="" class="form-horizontal" method="get" >
                                        <div class="input-group">
                                          <input type="hidden" name="mode" value="<?php echo (isset($_GET['mode'])) ? $_GET['mode'] : "" ;?>" class="form-control">
                                            <input type="text" name="keywords" value="<?php echo (isset($_GET['keywords'])) ? $_GET['keywords'] : "" ;?>" class="form-control" placeholder="Search for..." pattern="[a-zA-Z0-9-_.,$\s]+">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-submit" type="submit">Search</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    <?php if(!empty($published_media)) { ?>

                                <div class="col-md-12">
                                  <ul class="list-item">

                                    <?php foreach ($published_media as $media){?> 

                                        <li class="list-item-grid">
                                          <div class="media-grid-actions">
                                             <a href="#" class="btn btn-selected">
                                               <span>Select</span>
                                               <input class="grid-checkbox" type="checkbox" name="bulk_delete[]" value="<?php echo $media->id ?>" form="bulk-action-grid">
                                             </a>

                                              <?php if($media->is_freemium == "1"){

                                                    $href = getenv('AWS_S3_ASSETS') . "uploads/file/".$media->file_name;
                                                } else{
                                                    
                                                    $href = base_url()."assets/upload/file/".$media->file_name;
                                                }?>
                                             <div class="btn-action-2">
                                               <a href="<?php echo site_url('media/edit_media?media_id='.$media->id); ?>" class="btn btn-edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                <a href="<?php echo $href ?>"  class="btn btn-view" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                             </div>
                                            </div>  
                                               
                                            <?php if($media->file_type === "application/pdf" ){?>

                                                    <div class="item-grid">
                                                         <a href="<?php echo site_url('media/edit_media?media_id='.$media->id); ?>"><img src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?php echo $media->alt_text?>"></a>
                                                    </div>

                                               <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.word" || $media->file_type === "application/msword") { ?>

                                                     <div class="item-grid">
                                                           <a href="<?php echo site_url('media/edit_media?media_id='.$media->id); ?>"><img src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?php echo $media->alt_text?>"></a>
                                                      </div>
                                                
                                               <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){?>

                                                     <div class="item-grid">
                                                       <a href="<?php echo site_url('media/edit_media?media_id='.$media->id); ?>"><img src="<?php echo base_url()?>assets/images/excel-icon.png" alt="<?php echo $media->alt_text?>"></a>
                                                    </div>

                                               <?php } elseif($media->file_type === "text/plain"){?>
                                                    <div class="item-grid">
                                                       <a href="<?php echo site_url('media/edit_media?media_id='.$media->id); ?>"><img src="<?php echo base_url()?>assets/images/txt-icon.png" alt="<?php echo $media->alt_text?>"></a>
                                                    </div>

                                                <?php } elseif($media->file_type === "text/csv"){?>
                                                    <div class="item-grid">
                                                       <a href="<?php echo site_url('media/edit_media?media_id='.$media->id); ?>"><img src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?php echo $media->alt_text?>"></a>
                                                    </div>
                                                <?php } else{?>

                                                     <?php if($media->is_freemium == "1"){

                                                            $src = getenv('AWS_S3_ASSETS') . "uploads/file/".$media->file_name;
                                                      } else{

                                                           $src = base_url()."assets/upload/file/".$media->file_name;
                                                      }?>

                                                   <div class="item-grid">
                                                           <a href="<?php echo site_url('media/edit_media?media_id='.$media->id); ?>"><img src="<?php echo $src ?>" alt="<?php echo $media->alt_text?>"></a>
                                                  </div>

                                               <?php }?>  
                                          
                                        </li>

                                  <?php }?>

                                    </ul>
                                </div>
                             <?php  }  else { ?>

                              <div class="no-media-uploaded">

                                 <p class="no-media">No <strong><?php echo isset($_GET["keywords"]) ?  ucwords($_GET["keywords"]) : ""?></strong> media found!<br><br>

                                   <?php if(isset($_GET["keywords"])){?>
                                            <a href="<?php echo site_url("media?mode=grid"); ?>" class="btn btn-default btn-submit">Back to Media Library</a>

                                     <?php } ?>
                                </p>
                              </div>

                            <?php } ?>

                        <div class="col-md-12">
                            <?php if( $pagination ) : ?>
                                <div class="pagination-area pull-right">
                                    <nav>
                                        <ul class="pagination">
                                            <?php echo $pagination; ?>
                                        </ul>
                                    </nav>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $this->load->view('session/footer'); ?>
