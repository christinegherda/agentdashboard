<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>

    <div class="content-wrapper">
      <div class="page-title">
        <h3 class="heading-title">
          <span> Media Library </span>
        </h3>
      </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                <a href="#" class="btn-show-upload-aws btn btn-default btn-submit mb-30px" "><i class="fa fa-plus"></i> Add Media</a>
                    
                   <div style="display:none" class="show-upload-aws mb-30px">
                          <form id="upload-slider-photo-aws" method="POST" action="<?php echo base_url()?>media/add_media" enctype="multipart/form-data">

                              <div style="margin-bottom: 5px" class="input-group imageupload">
                                  <input type="file" name="userfile" class="filestyle" data-buttonBefore="true" data-icon="false">
                              </div>

                              <small>Max. upload file size: <strong>10 MB</strong></small> | <small>Allowed file type: <strong>jpg</strong> | <strong>jpeg</strong> | <strong>png</strong> | <strong>gif</strong> | <strong>pdf</strong> | <strong>doc</strong> | <strong>docx</strong> | <strong>xls</strong> | <strong>xlsx</strong> | <strong>csv</strong> | <strong>txt</strong> </small>

                              <div style="padding-top:20px" class="box-footer">
                                  <button type="submit" class="btn btn-default btn-save upload-slider-photo btn-submit"> <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Upload</button>
                              </div>
                          </form>
                      </div>
                    <div class="page-content media-list-view">

                        <div class="col-md-12 mb-30px" >
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-9">
                                      <div class="view-switch">
                                        <a href="<?php echo base_url()?>media?mode=list" class="view-list current" id="view-switch-list"><i class="fa fa-2x fa-list" aria-hidden="true"></i></a>
                                        <a href="<?php echo base_url()?>media?mode=grid" class="view-grid" id="view-switch-grid"><i class="fa fa-2x fa-th" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <form action="" method="POST" id="bulk-action-list">
                                             <select name="bulk_action" id="bulk-select-list" class="form-control">
                                                <option>Bulk Actions</option>
                                                <option value="delete">Delete Permanently</option>
                                             </select>
                                         </form>
                                    </div>
                                    <div class="btn-group">
                                        <button type="submit" form="bulk-action-list" class="btn btn-default btn-add-featured">Apply</button>
                                    </div>

                                    <div class="btn-group">
                                        <form action="" method="POST" id="filter-date-list">
                                             <select name="filter_month" id="select-pages" class="form-control mobile-select">
                                                <option>All Dates</option>
                                                <?php if( !empty($group_published_media) ) {
                                                    foreach($group_published_media as $media){?>

                                                        <option value="?filter_date=<?php echo date("Y-m", strtotime($media->post_date)); ?>"> <?php echo date("F Y", strtotime($media->post_date)); ?></option>
                                                <?php

                                                 }

                                                    }
                                                ?>
                                            </select>
                                        </form>
                                    </div>
                                    <div class="btn-group">
                                        <button type="submit" form="filter-date-list" class="btn btn-default btn-add-featured">Filter</button>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-lg-offset-3 col-md-4 col-md-offset-3 col-sm-5">
                                    <form action="" class="form-horizontal" method="get" >
                                        <div class="input-group">
                                            <input type="text" name="keywords" value="<?php echo (isset($_GET['keywords'])) ? $_GET['keywords'] : "" ;?>" class="form-control" placeholder="Search for..." pattern="[a-zA-Z0-9-_.,$\s]+">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-submit" type="submit">Search</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                          <?php 
                            $session = $this->session->flashdata('session');
                            
                            if(isset($session)) { ?>

                             <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>

                                <?php if($session["status"]){?>

                                 <div class="alert alert-info alert-dismissable">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                      <a href="<?php echo site_url("pages"); ?>" target="_blank"><strong>Click Here!</strong></a> to add media to your page.
                                  </div>
                              <?php }
                             
                            } ?>
                       </div>

                        <div class="col-md-12">
                          <div class="table-responsive">
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th class="text-center"><label><?php if(!empty($published_media)) { ?><input type="checkbox" id="checkAll"/><?php } ?></label></th>
                                  <th class="text-center">Icon</th>
                                  <th>Title</th>
                                  <th class="text-center">Date</th>
                                </tr>
                              </thead>
                              <tbody>
                                  <?php if(!empty($published_media)) {

                                            foreach ($published_media as $media){?>

                                             <tr>
                                              <td width="1%">
                                                <input type="checkbox" name="bulk_delete[]" value="<?php echo $media->id ?>" form="bulk-action-list">
                                              </td>
                                              <td width="5%">
                                                <?php if($media->file_type === "application/pdf" ){?>

                                                <p class="media-icon"><img src="<?php echo base_url()?>assets/images/pdf-icon.png" alt='<?php echo $media->alt_text?>' width="100%" height="100%"></p>

                                               <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.word" || $media->file_type === "application/msword") { ?>

                                                 <p class="media-icon"><img src="<?php echo base_url()?>assets/images/doc-icon.png" alt='<?php echo $media->alt_text?>' width="100%" height="100%"></p>

                                               <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){?>

                                                      <p class="media-icon"><img src="<?php echo base_url()?>assets/images/excel-icon.png" alt='<?php echo $media->alt_text?>' width="100%" height="100%"></p>

                                                <?php } elseif($media->file_type === "text/plain"){?>

                                                      <p class="media-icon"><img src="<?php echo base_url()?>assets/images/txt-icon.png" alt='<?php echo $media->alt_text?>' width="100%" height="100%"></p>

                                                <?php } elseif($media->file_type === "text/csv"){?>

                                                      <p class="media-icon"><img src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?php echo $media->alt_text?>" width="100%" height="100%"></p>
                                                      
                                                <?php } else {?>

                                                <?php

                                                    $src = ($media->is_freemium == "1") ? getenv('AWS_S3_ASSETS') . "uploads/file/".$media->file_name : base_url()."assets/upload/file/".$media->file_name;
                                                ?>

                                                <p class="media-icon"><img src="<?php echo $src?>" alt='<?php echo $media->alt_text?>' width="100%" height="100%"></p>

                                               <?php }?>

                                              </td>
                                                  
                                        <td width="85%">
                                          <div class="media-hover-actions">
                                            <p class="media-title"><a href="<?=base_url()?>media/edit_media?media_id=<?php echo $media->id?>"><?php echo htmlentities($media->title); ?></a></p>
                                            <p class="media-slug"><small><?php echo $media->file_name?></small></p>
                                            <p class="media-list-action">
                                              <?php
                                                $href = ($media->is_freemium == "1") ? getenv('AWS_S3_ASSETS') . "uploads/file/".$media->file_name : base_url()."assets/upload/file/".$media->file_name;
                                              ?>
                                              <a href="<?php echo site_url('media/edit_media?media_id='.$media->id); ?>">Edit</a> <span>|</span>  <a href="#" class="media-delete" data-toggle="modal" data-target="#modal-<?=$media->id?>">Delete Permanently</a> <span>| </span><a href="<?=$href?>" target="_blank">View</a>
                                              </p>
                                              <!-- Modal -->
                                            <div class="modal fade" id ="modal-<?=$media->id?>" tabindex ="-1" role ="dialog"
                                               aria-labelledby="myModalLabel" aria-hidden ="true">
                                               <div class="modal-dialog">
                                                  <div class="modal-content">
                                                     <div class="modal-header">
                                                        <button type ="button" class ="close" data-dismiss ="modal" aria-hidden ="true">
                                                              &times;
                                                        </button>
                                                        <h4 class="modal-title" id ="myModalLabel">
                                                           Delete Media
                                                        </h4>
                                                     </div>
                                                     <div class ="modal-body text-center">
                                                        Are you really want to delete permanently <strong><?php echo $media->title?></strong>?
                                                     </div>
                                                     <div class= "modal-footer">
                                                     <a class="btn btn-default btn-submit" href="<?php echo site_url('media/delete_media?media_name='.$media->file_name.'&media_id='.$media->id);?>">Delete</a>

                                                     </div>
                                                  </div><!-- /.modal-content -->
                                               </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->
                                          </div>
                                        </td>
                                        <td width="10%">
                                            <p class="text-center"><?php echo date("Y/m/d", strtotime($media->post_date)); ?></p>
                                        </td>
                                      </tr>
                              <?php 
                                    }
                                  }else{?>
                                     <tr>
                                      <td colspan="4">
                                        <p class="no-media">No <strong><?php echo isset($_GET["keywords"]) ?  ucwords($_GET["keywords"]) : ""?></strong> media found!<br><br>
                                          <?php if(isset($_GET["keywords"])) { ?>
                                                  <a href="<?php echo site_url("media"); ?>" class="btn btn-default btn-submit">Back to Media Library</a>
                                          <?php }?>

                                        </p>
                                      </td>
                                    </tr>
                                <?php  } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <?php if( $pagination ) : ?>
                            <div class="pagination-area pull-right">
                              <nav>
                                <ul class="pagination">
                                    <?php echo $pagination; ?>
                                </ul>
                              </nav>
                            </div>
                          <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $this->load->view('session/footer'); ?>
