<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends MX_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model("Media_model","media");
		$this->load->model("agent_sites/Agent_sites_model", "domain");

		// Load Aws_s3 Library
  		$this->load->library('s3');
  		$this->config->load('s3');

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');
	}

	public function index(){

		$data["title"] = "Manage Media";

		$this->load->library('pagination');

		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;

		if(isset($_GET['mode'])){

			if($_GET['mode'] === "grid"){

				$param["limit"] = 30;
				$pagination_url = base_url().'/media?mode=grid';

			} else {

				$param["limit"] = 10;
				$pagination_url = base_url().'/media';
			}

		} else {

			$param["limit"] = 10;
			$pagination_url = base_url().'/media';
		}

		$total = $this->media->get_all_media( TRUE, $param );
		$data["media"] = $this->media->get_all_media( FALSE, $param );
		$data["published_media"] = $this->media->get_all_published_media( FALSE, $param );
		$data["group_published_media"] = $this->media->get_group_published_media();

		$config["base_url"] = $pagination_url;
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("media/media.js");
		$data["css"] = array("dashboard/media.css");

		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");

		if(isset($_GET['mode'])){

			if($_GET['mode'] === "list"){

				$view_mode = "media_list";

			} elseif($_GET['mode'] === "grid"){

				$view_mode = "media_grid";

			} else {

				$view_mode = "media_list";
			}

		} else {

			$view_mode = "media_list";
		}

		$this->load->view($view_mode, $data);
	}

	public function filter_grid(){

		$data["title"] = "Filter Media";
		$this->load->library('pagination');
		$param = array();
		$param["filter"] = isset($_GET["filter_date"])? $_GET["filter_date"] : "";
		$param["pagination_filter"] = isset($_GET["filter_date"])? "filter_date=".$_GET["filter_date"] : "";
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 100;
		$total = $this->media->get_all_media( TRUE, $param );
		$data["media"] = $this->media->get_all_media( FALSE, $param );
		$data["published_media"] = $this->media->get_all_published_media( FALSE, $param );
		$data["group_published_media"] = $this->media->get_group_published_media();
		$data["filter_media"] = $this->media->filter_date(FALSE,$param);

		$config["base_url"] = base_url().'/media/filter_grid?'.$param["pagination_filter"].$param["keywords"];
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("media/media.js");
		$data["css"] = array("dashboard/media.css");

		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();

		$this->load->view('filter_media_grid', $data);
	}

	public function filter_list(){

		$data["title"] = "Filter Media";
		$this->load->library('pagination');
		$param = array();
		$param["filter"] = isset($_GET["filter_date"])? $_GET["filter_date"] : "";
		$param["pagination_filter"] = isset($_GET["filter_date"])? "filter_date=".$_GET["filter_date"] : "";
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$total = $this->media->get_all_media( TRUE, $param );
		$data["media"] = $this->media->get_all_media( FALSE, $param );
		$data["published_media"] = $this->media->get_all_published_media( FALSE, $param );
		$data["group_published_media"] = $this->media->get_group_published_media();
		$data["filter_media"] = $this->media->filter_date(FALSE,$param);

		$config["base_url"] = base_url().'/media/filter_list?'.$param["pagination_filter"].$param["keywords"];
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("media/media.js");
		$data["css"] = array("dashboard/media.css");

		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();

		$this->load->view('filter_media_list', $data);
	}

	public function edit_media(){

		$media_ids = $this->get_all_agent_media_id();

        if (isset($_GET['media_id']) && is_numeric($_GET['media_id'])){

            if(in_array($_GET['media_id'], $media_ids,true)){

               $media_id = $_GET['media_id'];

				$data["title"] = "Edit Media";

				if($this->input->post()) {

					$edit = $this->media->edit_media($media_id);

					$this->session->set_flashdata('session' , array('status'=>$edit["success"], 'message'=>$edit['message']));
					redirect("media/edit_media?media_id=".$media_id, "refresh");

				}

				$data["info"] = $this->media->get_media($media_id);

				$data["js"] = array("media/media.js");
				$data["css"] = array("dashboard/media.css");

				$data['myClass'] = $this;

				$siteInfo = Modules::run('agent_sites/getInfo');
				$data['branding'] = $siteInfo['branding'];
				$data["domain_info"] = $this->domain->fetch_domain_info();
				$data["is_reserved_domain"] = $this->domain->is_reserved_domain();

				$this->load->view('edit_media', $data);

            } else{

                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to edit this media!'));
                redirect("media", "refresh");
            }

        } else{
            
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to edit media!'));
            redirect("media", "refresh");
        }
	}

	public function add_media() {

		if($_FILES) {

			if(!empty($_FILES["userfile"]["name"])) {

				if($_FILES["userfile"]["error"] == 0 && $_FILES["userfile"]["size"] < "10485760" ){

					if($_FILES["userfile"]["type"] == "image/jpeg" || $_FILES["userfile"]["type"] == "image/png" || $_FILES["userfile"]["type"] == "image/gif" || $_FILES["userfile"]["type"] == "image/x-ms-bmp" || $_FILES["userfile"]["type"] == "image/bmp" || $_FILES["userfile"]["type"] == "text/plain" || $_FILES["userfile"]["type"] == "text/csv" || $_FILES["userfile"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $_FILES["userfile"]["type"] == "application/pdf" || $_FILES["userfile"]["type"] == "application/vnd.openxmlformats-officedocument.word" || $_FILES["userfile"]["type"] == "application/msword"){ 

						//generate random-id
						$userfile = uniqid().'-'.($_FILES['userfile']['name']);
						$file = $_FILES['userfile']['tmp_name'];
						$bucket_name = $this->config->item('bucket_name');
						$file_name = "uploads/file/".$userfile;
						$acl = "public-read";

						$data = array(
							"title" => $userfile,
							"file_name" => $userfile,
							"file_type" => $_FILES['userfile']['type'],
							"file_size" => $_FILES['userfile']['size']
						);

						if(!empty($file)){

							//upload userfile to AWS S3
				 	 		$this->s3->putObjectFile($file,$bucket_name,$file_name,$acl);
						}

						if(!empty($userfile)){

							//save to database
							$this->media->add_media_aws($data);
						}

							$status = TRUE;
							$message = "Media has been successfully added!";


					} else {

						$status = FALSE;
						$message = "The filetype you are attempting to upload is not allowed!";
					}


				} else {

					$status = FALSE;
					$message = "The uploaded file exceeds the maximum upload filesize!";
				}

			} else {

				$status = FALSE;
				$message = "You did not select a file to upload!";
			}

			$this->session->set_flashdata('session' , array('status' => $status,'message' => $message));

		} 
		
		redirect("media", "refresh");
	}

	public function delete_media(){

		$media_ids = $this->get_all_agent_media_id();

        if (isset($_GET['media_id']) && is_numeric($_GET['media_id'])){

            if(in_array($_GET['media_id'], $media_ids,true)){

               //delete slider photos in amazon S3
				$uri = "uploads/file/".$_GET["media_name"];
				$this->s3->deleteObject($this->config->item("bucket_name"),$uri);


				if( $this->media->delete_media($_GET["media_name"],$_GET["media_id"]) ){

					$this->session->set_flashdata('message', 'Media has been successfully deleted!');

				} else {

					$this->session->set_flashdata('message', 'Failed to removed media');
				}

            } else{
            	
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to delete this media!'));
                redirect("media", "refresh");
            }

        } else{
            
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to delete media!'));
            redirect("media", "refresh");
        }

		redirect("media", "refresh");
	}

	public function bulk_delete_grid(){

		if(isset($_POST['bulk_delete']) AND !empty($_POST['bulk_delete'])){

			if( $this->media->bulk_delete() ){

					$this->session->set_flashdata('message', 'Selected media has been successfully deleted!');

				} else {

					$this->session->set_flashdata('message', 'Failed to delete selected media!');
				}
		}

		redirect("media?mode=grid", "refresh");
	}

	public function bulk_delete_list(){

		if(isset($_POST['bulk_delete']) AND !empty($_POST['bulk_delete'])){

			if( $this->media->bulk_delete() ){

					$this->session->set_flashdata('message', 'Selected media has been successfully deleted!');

				} else {

					$this->session->set_flashdata('message', 'Failed to delete selected media!');
				}
		}

		redirect("media", "refresh");
	}

	 public function size_as_kb($size) {

	 	//if(!empty(is_freemium())){

	 		if($size < 1024) {

			$size_b = round($size,0);

			return "{$size_b} B";

			} elseif($size < 1048576) {

				$size_kb = round($size/1024,0);
				return "{$size_kb} KB";

			} else {

				$size_mb = round($size/1048576, 1);
				return "{$size_mb} MB";
			}

	 	// } else{

	 	// 	if($size < 1024) {

			// $size_kb = round($size,0);

			// return "{$size_kb} KB";

			// } elseif($size < 1048576) {

			// 	$size_mb = round($size/1024,0);
			// 	return "{$size_mb} MB";

			// } else {

			// 	$size_gb = round($size/1048576, 1);
			// 	return "{$size_gb} GB";
			// }
	 	// }

		
	}
	public function get_all_agent_media_id(){

		$agent_media_id = $this->media->get_all_media_id();
        $media_id = array();

        foreach($agent_media_id as $am_id){
            $media_id[] = $am_id->id;
        }

        return $media_id;

	}

}
