<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Media_model extends CI_Model { 

	public function __construct(){

        parent::__construct();
    }

    public function get_all_media( $counter = FALSE,  $param = array()){

        $array = array(
            'author' => $this->session->userdata('user_id'),
        );
        $this->db->select("*")
                ->from("media")
                ->where($array)
                ->order_by("id","desc");

        if( isset($_GET["keywords"]) ){

            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter ){

            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) ){ 

            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) ){

            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 ){

            return $query->result();
        }

        return FALSE;

    }

     public function get_all_media_id(){

        $array = array(
            'author' => $this->session->userdata('user_id'),
        );
        $this->db->select("id")
                ->from("media")
                ->where($array);
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 ){

            return $query->result();
        }

        return FALSE;
    }

    public function get_group_published_media(){

        $array = array(
            'author' => $this->session->userdata('user_id'),
        );
        $year = 
        $this->db->select("post_date")
                ->from("media")
                ->where($array)
                ->group_by ("YEAR(post_date),MONTH(post_date)")
                ->order_by("id","desc");
        
        $query = $this->db->get();


        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function get_all_published_media( $counter = FALSE,  $param = array()){

        $array = array(
            'author' => $this->session->userdata('user_id'),
        );
        $this->db->select("*")
                ->from("media")
                ->where($array)
                ->order_by("id","desc");

        if( isset($_GET["keywords"])){

            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter ){

            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) ){

            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) ){

            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 ){

            return $query->result();

        }

        return FALSE;
    }

    public function get_all_published_media_new($params = array()){

        $array = array(
            'author'   => $this->session->userdata('user_id'),
        );

        $this->db->select('*')
            ->from('media')
            ->where($array)
            ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

            $this->db->limit($params['limit'],$params['start']);
        }

        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

            $this->db->limit($params['limit']);
        }
        
         $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function filter_date($counter = FALSE, $param = array() ){

        $this->db->select("*")
                ->from("media")
                ->where("author" , $this->session->userdata('user_id'))
                ->where("post_date like '%".$param["filter"]."%'")
                ->order_by("id","desc");


                  if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();
        }

        return FALSE;
    }

    public function add_media( $datas = array()){

        $media["title"] = $datas['raw_name'];
        $media["file_name"] = $datas['file_name'];

        if(!empty($datas['image_type'])){
            $media["file_type"] = $datas['image_type'];
        } else {
          $media["file_type"] = $datas['file_type'];
        }

        if(!empty($datas['image_width'])){
            $media["image_width"] = $datas['image_width'];
        } 

        if(!empty($datas['image_height'])){
            $media["image_height"] = $datas['image_height'];
        } 
        $media["file_size"] = $datas['file_size'];
        $media["author"] = $this->session->userdata("user_id");
        $media["post_date"] = date("Y-m-d h:m:s");

        $this->db->insert("media", $media);

        return TRUE;
    }

    public function add_media_aws( $datas = array()){

        $media["title"] = $datas['title'];
        $media["file_name"] = $datas['file_name'];
        $media["file_type"] = $datas['file_type'];
        $media["file_size"] = $datas['file_size'];
        $media["is_freemium"] = "1";

        if(!empty($datas['image_width'])){
            $media["image_width"] = $datas['image_width'];
        } 
        if(!empty($datas['image_height'])){
            $media["image_height"] = $datas['image_height'];
        } 
        $media["author"] = $this->session->userdata("user_id");
        $media["post_date"] = date("Y-m-d h:m:s");

        $this->db->insert("media", $media);

        return TRUE;
    }


    public function insert_media( $datas = array()){

        $media["title"] = $datas['raw_name'];
        $media["file_name"] = $datas['file_name'];

        if(!empty($datas['image_type'])){
            $media["file_type"] = $datas['image_type'];
        } else {
          $media["file_type"] = $datas['file_type'];
        }

        if(!empty($datas['image_width'])){
            $media["image_width"] = $datas['image_width'];
        } 

        if(!empty($datas['image_height'])){
            $media["image_height"] = $datas['image_height'];
        } 
        $media["file_size"] = $datas['file_size'];
        $media["author"] = $this->session->userdata("user_id");
        $media["post_date"] = date("Y-m-d h:m:s");

        $this->db->insert("media", $media);

        return $this->db->insert_id();
    }
    
    public function insert_media_aws( $datas = array()){

        $media["title"] = $datas['file_name'];
        $media["file_name"] = $datas['file_name'];
        $media["file_type"] = $datas['file_type'];
        $media["file_size"] = $datas['file_size'];
        $media["is_freemium"] = "1";

        if(!empty($datas['image_width'])){
            $media["image_width"] = $datas['image_width'];
        } 

        if(!empty($datas['image_height'])){
            $media["image_height"] = $datas['image_height'];
        } 
        $media["author"] = $this->session->userdata("user_id");
        $media["post_date"] = date("Y-m-d h:m:s");

        $this->db->insert("media", $media);

        return $this->db->insert_id();
    }

    public function edit_media($media_id=NULL, $datas=array()) {

        $ret = array("success"=>FALSE, "message"=>"Failed to update media!");

        if($media_id) {

            $media = $this->input->post("media");

            $this->db->select("id")
                    ->from("media")
                    ->where("id !=", $media_id)
                    ->where("title", $media["title"])
                    ->where("file_type", $media["file_type"])
                    ->where("author", $this->session->userdata("user_id"))
                    ->limit(1);

            $query = $this->db->get();

            if($query->num_rows()>0) {

                $ret["success"] = FALSE;
                $ret["message"] = "Error: Duplicate media title!";

            } else {

                $media["post_modified"] = date("Y-m-d H:i:s");
                
                $this->db->where("id", $media_id);
                $this->db->update("media", $media);

                $ret["success"] = ($this->db->affected_rows()>0) ? TRUE : FALSE;
                $ret["message"] = ($this->db->affected_rows()>0) ? "Media successfully updated!" : "Failed to update media!";

            }

        }

        return $ret;
    }
    
    public function delete_media($media_name = NULL, $media_id = NULL ){

        $array = array(
            'id' => $media_id,
            'file_name' => $media_name,
            'author' => $this->session->userdata("user_id")
        );
       $this->db->where($array);

       if( $this->db->delete( "media" ) ){

            return TRUE;

        } else {

            return FALSE;
        }

    }

    public function bulk_delete(){

       if(gettype($_POST['bulk_delete'])=="array"){
            foreach($_POST['bulk_delete'] as $val){
             $media_id = $val;
             
              $array = array(
                'id' => $media_id,
                'author' => $this->session->userdata('user_id'),
            );

           $this->db->where($array)
                ->delete('media');
            }
        }
    }

    public function get_media($media_id = 0){
        $this->db->select("*")
          ->from("media")
          ->where("id" , $media_id);

        return $this->db->get()->row_array();
    }
}


 