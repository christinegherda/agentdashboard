<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
/*
  - Failsafe functions
  - Returns null if nothing is supplied
*/

class Freemium_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }
  // Freemium CRUD
  public function create_freemium ($plan_id = NULL) { // recieved selected plan
    $user_id = $this->session->userdata('user_id');
    $ret = NULL;
    if($user_id && $plan_id) { // User and Plan should exist
      // Check if user has a freemium plan
      $freemium = $this->read_freemium();
      if(count($freemium) < 1) { // If user has no freemium, create a plan
        // Check if plan exists
        $plan = $this->read_freemium_plan($plan_id);
        if(count($plan)) {
          // Plan Exists, else it will return false
          // Subscribe user to plan
          $data = array(
            'plan_id'               => $plan->id,
            'subscriber_user_id'    => $user_id
          );

          $this->db->insert('freemium', $data);
          $ret = $this->db->insert_id(); // Returns the id of the inserted freemium
        }
      }
    }
    return $ret;
  }
  public function read_freemium ($user_id = NULL) {
    $user_id = $this->session->userdata('user_id');
    $ret = NULL;
    if($user_id) {
      $ret = $this->db->get_where('freemium', array('subscriber_user_id' => $user_id))->row();
    }
    return $ret;
  }
  public function update_freemium ($user_id = NULL) {
    $ret = NULL;
    if($user_id) {
      $ret = true; // Change This
    }
    return $ret;
  }
  public function delete_freemium ($user_id = NULL) {
    $ret = NULL;
    if($user_id) {
      $ret = true; // Change This
    }
    return $ret;
  }

  // Freemium Plan

  public function read_freemium_plan ($plan_id = NULL) {
    $ret = NULL;
    if($plan_id) {
      $ret = $this->db->get_where('freemium_plan', array('id' => $plan_id))->row();
    }
    return $ret;
  }

  // Freemium Feature Plan Group

  public function read_freemium_plan_feature_group ($plan_id = NULL) {
    $ret = NULL;
    if($plan_id) {
      $ret = $this->db->get_where('freemium_plan_feature_group', array('plan_id' => $plan_id))->result(); // true; // Change This
    }
    else {
      $ret = $this->db->get('freemium_plan_feature_group')->result(); // true; // Change This
    }
    return $ret;
  }

  // Freemium view
  public function read_freemium_view($plan_id = NULL) {
    $ret = NULL;
    if($plan_id) {
      $ret = $this->db->get_where('freemium_view', array('plan_id' => $plan_id))->result();
    } /*else {
      $ret = $this->db->get('freemium_view')->result();
    }*/
    return $ret;
  }

  // Freemium features view
  public function read_freemium_features_view() {
    return $this->db->get('freemium_features_view')->result();
  }
}
