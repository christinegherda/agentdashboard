<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
	Flagging
	* Agent Site
		- Custom Domain /custom_domain
		- Customize Homepage /agent_sites/infos
		- Media /media
		- Pages /pages
		- Menu /menu
		- Customize Homepage Search /customize_homepage
		- Reviews /reviews
		- Saved Searchs /agent_sites/saved_searches
	* CRM
		- Leads /crm/leads
		- Scheduled Showings /crm/spw_schedules
		- Listtrac /listtrac
	* Single Property Listings /single_property_listings
	* Marketing
	 - Activate Social Media /agent_social_media/activate
	 - Social Media Links /agent_social_media/links
*/
class Base extends MX_Controller {
	function __construct() {
 		parent::__construct();
    $this->load->model("freemium_model", "freemium");
  }
  public function index() {
		echo "Freemium Development<br>";

		// echo $this->session->userdata("user_id");

  }

	/*
		Freemium Function Check
	*/
	public function fetch_details() {

		$freemium_view = false;
		
		if($this->session->userdata('user_id')) {

			$freemium = $this->freemium->read_freemium(); //Fetch agent freemium data

			if(!empty($freemium)) {
				$plan = $this->freemium->read_freemium_plan($freemium->plan_id);
				$freemium_view = $this->freemium->read_freemium_view($freemium->plan_id);
			}

		}

		return $freemium_view;
	}

	/* Fetch all Details */
	public function fetch_all_details() {
		$freemium_view = $this->freemium->read_freemium_features_view();
		return $freemium_view;
	}

	/*
		Freemium Dashboard Control for Super Admin
		First Step
			- Create A freemium Plan
			- Assign Group Modules and assign it to plan
	*/
	public function create_freemium_plan() {
		$user_id = $this->input->post('user_id');
		if($user_id) {
			$data = array(
				'name' => $this->input->post('name'),
				'price' => $this->input->post('price'),
				'description' => $this->input->post('description')
			);
			if($this->freemium->create_freemium_plan($data)) {
				header("location: ".base_url()."base/create_freemium_plan?create=1");
			}
		}
		$this->load->view("create_plan");
	}

	public function isFreemiumUser() {
		
		$ret = NULL;

		if($this->session->userdata('user_id')) {
			$ret = $this->freemium->read_freemium();
			//$freemium = $this->freemium->read_freemium();
			//$ret = (!empty($freemium)) ? TRUE : FALSE;
		}

		return $ret;
	}

	public function create_freemium_feature_group() {

	}
	public function create_freemium_features() {

	}
}
