<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Demo extends MX_Controller {

 
	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		// if(empty(config_item("idx_api_key"))) {
		// 	$this->db->select("*")->from("property_idx")->where("user_id", $this->session->userdata('user_id'));
		// 	$propert_idx = $this->db->get()->row();
		// 	//printA(); exit
		// 	if($propert_idx) {
		// 		$this->config->set_item('idx_api_key',$propert_idx->api_key);  
		// 		$this->config->set_item('idx_api_secret', $propert_idx->api_secret);
		// 	}
		// }

		if(empty(config_item("is_paid"))) {
			$this->db->select("rid")->from("receipt")->where("agent_id", $this->session->userdata('user_id'));
			$receipt = $this->db->get()->row(); 

			if($receipt) {
				$this->config->set_item('is_paid', TRUE);  
			} else {
				$this->config->set_item('is_paid', FALSE);  
			}
		}

		$this->load->model("agent_sites/Agent_sites_model");
		$this->message_start_delimiter = $this->config->item('message_start_delimiter', 'ion_auth');
	}


	function index() {   
		
		$data['title'] = "Home";
		
		$this->load->view('preview/preview', $data);
		
	}

}

		