<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Agent_haven extends MX_Controller {

 
	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		if(empty(config_item("is_paid"))) {
			$this->db->select("rid")->from("receipt")->where("agent_id", $this->session->userdata('user_id'));
			$receipt = $this->db->get()->row(); 

			if($receipt) {
				$this->config->set_item('is_paid', TRUE);  
			} else {
				$this->config->set_item('is_paid', FALSE);  
			}
		}

		$this->load->model("agent_sites/Agent_sites_model");
		$this->message_start_delimiter = $this->config->item('message_start_delimiter', 'ion_auth');
	}


	function index() {   
		
		$data['title'] = "Home";
		
		$this->load->view('agent-haven/home_view', $data);
		
	}

	function listings() {   
		
		$data['title'] = "Listings";
		
		$this->load->view('agent-haven/listings_view', $data);
		
	}

	function find() {   
		
		$data['title'] = "Find";
		
		$this->load->view('agent-haven/find_a_home_view', $data);
		
	}

	function featured_property() {   
		
		$data['title'] = "Featured Property";
		
		$this->load->view('agent-haven/property_detail_view', $data);
		
	}

	function sold_property() {   
		
		$data['title'] = "Sold Property";
		
		$this->load->view('agent-haven/property_sold_detail_view', $data);
		
	}

	function other_property() {   
		
		$data['title'] = "Other Property";
		
		$this->load->view('agent-haven/other_property_detail_view', $data);
		
	}

	function about() {   
		
		$data['title'] = "About";
		
		$this->load->view('agent-haven/about_view', $data);
		
	}

	function buyer() {   
		
		$data['title'] = "Buyer";
		
		$this->load->view('agent-haven/buyer_view', $data);
		
	}

	function seller() {   
		
		$data['title'] = "Seller";
		
		$this->load->view('agent-haven/seller_view', $data);
		
	}

	function blog() {   
		
		$data['title'] = "Blog";
		
		$this->load->view('agent-haven/blog_view', $data);
		
	}

	function contact() {   
		
		$data['title'] = "Contact";
		
		$this->load->view('agent-haven/contact_view', $data);
		
	}
	function signup() {   
		
		$data['title'] = "Sign Up";
		
		$this->load->view('agent-haven/signup_view', $data);
		
	}
	function login() {   
		
		$data['title'] = "Log in";
		
		$this->load->view('agent-haven/login_view', $data);
		
	}
	function customer_dashboard() {   
		
		$data['title'] = "Customer Dashboard";
		
		$this->load->view('agent-haven/customer_dashboard_view', $data);
		
	}
	function mortgage_calculator() {   
		
		
	}

}

		