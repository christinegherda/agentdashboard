<?php $this->load->view('/session/header-page'); ?>
<section class="advance-search-area">
    <form method="get" action="http://bradleyfielding.com/listings" class="advance-search-form">
        <div class="visible-xs">
            <!-- <button class="btn btn-default submit-button btn-show-filter">Show Filters</button> -->
            <a href="javascript:void(0);" class="btn btn-default submit-button btn-show-filter" data-original-title="" title=""><i class="fa fa-search"></i> Show Filters</a>
        </div>
        <nav class="navbar  navbar-search">
            <div class="container">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="navbar-form navbar-left nav-search-box" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control input-search" placeholder="Search for..." name="Search">
                        </div>
                    </div>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-original-title="" title=""><i class="fa fa-usd" aria-hidden="true"></i> <span class="search-label">Price</span> (<span id="price_label">Any</span>)<span class="caret"></span></a>
                            <div class="dropdown-menu">
                                <div class="col-md-12">
                                    <label>From</label>
                                    <input type="text" class="form-control" name="min_price" id="min_price">
                                    <label>To</label>
                                    <input type="text" class="form-control" name="max_price" id="max_price">
                                    <a href="" style="width:100%;display: block;text-align: center;padding: 5px;" data-original-title="" title=""><small>Dismiss</small></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-original-title="" title="">
                                <i class="fa fa-bed" aria-hidden="true"></i> <span class="search-label">Beds</span> (<span id="bedroom_label">Any</span>)<span class="caret"></span>
                            </a>
                            <input type="hidden" name="bedroom" id="bedroom">
                            <ul class="dropdown-menu">
                                <li><a href="" onclick="setFieldValue('bedroom', 'Any')" data-original-title="" title="">Any</a></li>
                                <li><a href="" onclick="setFieldValue('bedroom', '1')" data-original-title="" title="">1+</a></li>
                                <li><a href="" onclick="setFieldValue('bedroom', '2')" data-original-title="" title="">2+</a></li>
                                <li><a href="" onclick="setFieldValue('bedroom', '3')" data-original-title="" title="">3+</a></li>
                                <li><a href="" onclick="setFieldValue('bedroom', '4')" data-original-title="" title="">4+</a></li>
                                <li><a href="" onclick="setFieldValue('bedroom', '5')" data-original-title="" title="">5+</a></li>
                                <li><a href="" onclick="setFieldValue('bedroom', '6')" data-original-title="" title="">6+</a></li>
                                <li><a href="" onclick="setFieldValue('bedroom', '7')" data-original-title="" title="">7+</a></li>
                                <li><a href="" onclick="setFieldValue('bedroom', '8')" data-original-title="" title="">8+</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-original-title="" title="">
                                <i class="icon-toilet" aria-hidden="true"></i> <span class="search-label">Baths</span> (<span id="bathroom_label">Any</span>)<span class="caret"></span>
                            </a>
                            <input type="hidden" name="bathroom" id="bathroom">
                            <ul class="dropdown-menu">
                                <li><a href="" onclick="setFieldValue('bathroom', 'Any')" data-original-title="" title="">Any</a></li>
                                <li><a href="" onclick="setFieldValue('bathroom', '1')" data-original-title="" title="">1+</a></li>
                                <li><a href="" onclick="setFieldValue('bathroom', '2')" data-original-title="" title="">2+</a></li>
                                <li><a href="" onclick="setFieldValue('bathroom', '3')" data-original-title="" title="">3+</a></li>
                                <li><a href="" onclick="setFieldValue('bathroom', '4')" data-original-title="" title="">4+</a></li>
                                <li><a href="" onclick="setFieldValue('bathroom', '5')" data-original-title="" title="">5+</a></li>
                                <li><a href="" onclick="setFieldValue('bathroom', '6')" data-original-title="" title="">6+</a></li>
                                <li><a href="" onclick="setFieldValue('bathroom', '7')" data-original-title="" title="">7+</a></li>
                                <li><a href="" onclick="setFieldValue('bathroom', '8')" data-original-title="" title="">8+</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav more-filters">
                        <li class="dropdown">
                            <a href="javascript:void(0);" id="moreFiltersToggle" class="moreFiltersToggle" data-original-title="" title=""> More Filters <span class="search-label">(Show)</span></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav submit-filters">
                        <li class="dropdown">
                            <button type="submit" class="submit-button submit-general"> <i class="fa fa-search"></i> <span class="search-label">Search</span> </button>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right show-map-area">
                        <li class="dropdown">
                            <a href="#" class="view-map-button" data-original-title="" title=""><i class="fa fa-map"></i> <span class="show-map-label">Show map</span> </a>
                        </li>
                        <li>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="container" id="more-filters-area" style="display:none;">
            <div class="row more-filter">
                <div class="container">
                    <!--  <div class="col-md-12">
                            <div class="alert alert-danger">
                                <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> This section is still under development. Click <a href="" class="moreFiltersToggle">here</a> to hide this area</p>
                            </div>
                        </div> -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group" id="property_type_container">
                            <h3>Property Types</h3>
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input name="property_type[]" value="Residential" type="checkbox" id="residential"> Residential </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="property_type[]" value="MultiFamily" type="checkbox" id="multifamily"> MultiFamily </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="property_type[]" value="Land" type="checkbox" id="land"> Land </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="property_type[]" value="Commercial" type="checkbox" id="commercial"> Commercial </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="property_type[]" value="Rental" type="checkbox" id="rental"> Rental </label>
                            </div>
                        </div>
                        <div class="form-group" id="property_sub_type_container" style="display:none;">
                            <h3>Property Sub Types</h3>
                            <hr>
                            <div id="residential_types" style="display:none;">
                                <div class="checkbox">
                                    <label>
                                        <input name="residential_type[]" value="Single Family" type="checkbox"> Single Family Detached </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="residential_type[]" value="Condo" type="checkbox"> Condo/Co-op </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="residential_type[]" value="Townhouse" type="checkbox"> Townhouse </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="residential_type[]" value="Manufactured" type="checkbox"> Mobile/Manufactured </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="residential_type[]" value="Villa" type="checkbox"> Villa </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="residential_type[]" value="Hotel" type="checkbox"> Hotel </label>
                                </div>
                            </div>
                            <div id="multifamily_types" style="display:none;">
                                <div class="checkbox">
                                    <label>
                                        <input name="multifamily_type[]" value="Condo" type="checkbox"> Condo/Coop </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="multifamily_type[]" value="Townhouse" type="checkbox"> Townhouse </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="multifamily_type[]" value="Plex" type="checkbox"> Duplex/Triplex/Quadplex </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="multifamily_type[]" value="House Plus Units" type="checkbox"> House Plus Units </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="multifamily_type[]" value="6 Units" type="checkbox"> 6 Units </label>
                                </div>
                            </div>
                            <div id="land_types" style="display:none;">
                                <div class="checkbox">
                                    <label>
                                        <input name="land_type[]" value="Sale" type="checkbox"> Sale </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="land_type[]" value="Farm" type="checkbox"> Farm </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="land_type[]" value="Ranch" type="checkbox"> Ranch </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="land_type[]" value="Combo" type="checkbox"> Combo </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="land_type[]" value="Industrial" type="checkbox"> Industrial </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="land_type[]" value="Industrial" type="checkbox"> Agricultural </label>
                                </div>
                            </div>
                            <div id="commercial_types" style="display:none;">
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Office" type="checkbox"> Office </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Showroom" type="checkbox"> Showroom </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Warehouse" type="checkbox"> Warehouse </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Building" type="checkbox"> Building </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Multi-Use" type="checkbox"> Multi-Use </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Developed Land" type="checkbox"> Developed Land </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Mobile Home" type="checkbox"> Mobile Home </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Industrial" type="checkbox"> Commercial Industrial </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Apartment" type="checkbox"> Apartment </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Hotel" type="checkbox"> Hotel </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Motel" type="checkbox"> Motel </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="commercial_type[]" value="Hospitality" type="checkbox"> Hospitality </label>
                                </div>
                            </div>
                            <div id="rental_types" style="display:none;">
                                <div class="checkbox">
                                    <label>
                                        <input name="rental_type[]" value="Single Family" type="checkbox"> Single Family Detached </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="rental_type[]" value="Condo" type="checkbox"> Condo/Coop </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="rental_type[]" value="Townhouse" type="checkbox"> Townhouse </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="rental_type[]" value="Plex" type="checkbox"> Duplex/Triplex/Quadplex </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="rental_type[]" value="Apartment" type="checkbox"> Apartment </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="rental_type[]" value="Hotel" type="checkbox"> Hotel </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="rental_type[]" value="Villa" type="checkbox"> Villa </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <h3>Show Me</h3>
                        <hr>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="listing_change_type[]" value="New Listing" type="checkbox"> New Listings</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="listing_change_type[]" value="New Construction" type="checkbox"> New Construction</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="listing_change_type[]" value="Open House" type="checkbox"> Open Houses</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="listing_change_type[]" value="Price Reduced" type="checkbox"> Price Reduced</label>
                            </div>
                        </div>
                        <h3>Recently Sold</h3>
                        <hr>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="recently_sold[]" value="Residential" type="checkbox"> Residential</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="recently_sold[]" value="Rental" type="checkbox"> Rental</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="recently_sold[]" value="Commercial" type="checkbox"> Commercial</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="recently_sold[]" value="Land" type="checkbox"> Land</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <h3>Features</h3>
                        <hr>
                        <div class="form-group">
                            <select name="house_size" class="form-control">
                                <option value="0">Any House Size</option>
                                <option value="600">600+ sq ft</option>
                                <option value="800">800+ sq ft</option>
                                <option value="1000">1000+ sq ft</option>
                                <option value="1200">1200+ sq ft</option>
                                <option value="1400">1400+ sq ft</option>
                                <option value="1600">1600+ sq ft</option>
                                <option value="1800">1800+ sq ft</option>
                                <option value="2000">2000+ sq ft</option>
                                <option value="2250">2250+ sq ft</option>
                                <option value="2500">2500+ sq ft</option>
                                <option value="2750">2750+ sq ft</option>
                                <option value="3000">3000+ sq ft</option>
                                <option value="3500">3500+ sq ft</option>
                                <option value="4000">4000+ sq ft</option>
                                <option value="5000">5000+ sq ft</option>
                                <option value="6000">6000+ sq ft</option>
                                <option value="7000">7000+ sq ft</option>
                                <option value="8000">8000+ sq ft</option>
                                <option value="9000">9000+ sq ft</option>
                                <option value="10000">10000+ sq ft</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="lot_size" class="form-control">
                                <option value="0">Any Lot Size</option>
                                <option value="21780">1/2+ acre</option>
                                <option value="43560">1+ acre</option>
                                <option value="87120">2+ acres</option>
                                <option value="217800">5+ acres</option>
                                <option value="435600">10+ acre</option>
                                <option value="871200">20+ acre</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="house_age" class="form-control">
                                <option>Any House Age</option>
                                <option value="fiveyears">0-5 Years</option>
                                <option value="tenyears">0-10 Years</option>
                                <option value="fifthteenyears">0-15 Years</option>
                                <option value="twentyyears">0-20 Years</option>
                                <option value="fiftyyears">0-50 Years</option>
                                <option value="fiftyyearsplus">51+ Years</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="features[]" value="garage"> 2+ Car Garage</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="features[]" value="pool"> Swimming Pool</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="features[]" value="view"> View</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="features[]" value="waterfront"> Waterfront</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="features[]" value="Cul-De-Sac"> Cul-De-Sac</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="features[]" value="golf"> Golf Course View</label>
                            </div>
                            <div><a href="javacript:;" data-toggle="modal" data-target="#features_modal" data-original-title="" title="">View all features</a></div>
                            <style>
                            .modal-backdrop.in {
                                display: none;
                            }
                            
                            .modal-open .modal {
                                overflow: hidden;
                            }
                            
                            .show-me-text {
                                color: inherit !important;
                            }
                            </style>
                            <div class="modal fade" id="features_modal" style="margin-top:198px; color:#000000; width:95%;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                            <h4 class="modal-title">Features</h4>
                                        </div>
                                        <div class="modal-body" style="color:#000000;">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h4>Inside</h4>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="hardwood"> Hardwood Floors</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="fireplace"> Fireplace</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="energy"> Energy Efficient</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="disable"> Disability Features</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h4>Heating/Cooling</h4>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="cool"> Central Air</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="heat"> Central Heat</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="air"> Forced Air</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h4>Community</h4>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="recreation"> Recreation Facilities</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="security"> Comm Security Features</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="pool"> Comm Swimming Pool(s)</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="boat"> Comm Boat Facilities</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="clubhouse"> Comm Clubhouse(s)</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="horse"> Comm Horse Facilities</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="tennis"> Comm Tennis Court(s)</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="park"> Comm Park(s)</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="golf"> Comm Golf</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="senior"> Senior Comm</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="spa"> Comm Spa/Hot Tub(s)</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h4>Outdoor Amenities</h4>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="pool"> Swimming Pool</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="boat"> RV/Boat Parking</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="spa"> Spa/Hot Tub</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="horse"> Horse Facilities</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="tennis"> Tennis Courts</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h4>Rooms</h4>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="dining"> Dining</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="laundry"> Laundry</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="family"> Family</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="basement"> Basement</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="office"> Den/Office</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="games"> Games</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h4>Lot</h4>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="corner lot"> Corner Lot</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="cul-de-sac"> Cul-De-Sac</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="waterfront"> WaterFront</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="water view"> Water View</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="golf"> Golf Course Lot/Frontage</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h4>Parking</h4>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="1 Garage"> 1 Garage</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="2 Garage"> 2 Garage</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="3 Garage"> 3 Garage</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Carport"> Carport</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h4>Style</h4>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="story"> 1 Story</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="stories"> 2 Story</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Bungalow"> Bungalow</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Historical"> Historical</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Pueblo"> Pueblo</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Patio Home"> Patio Home</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Revival"> Revival</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Southwestern"> Southwestern</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Spanish"> Spanish</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Traditional"> Traditional</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Victorian"> Victorian</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Contemporary"> Contemporary</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Contemporary"> Solar</label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label class="show-me-text">
                                                                <input type="checkbox" name="features[]" value="Split-Level"> Victorian</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal">Apply</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-default submit-button btn-lg">
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="visible-xs">
        <a href="#" class="view-map-button submit-button" data-original-title="" title=""><i class="fa fa-map"></i> <span class="search-label">Show map</span> </a>
        <button class="btn btn-default submit-button filter-search-button">
            <i class="fa fa-filter"></i> More Options
        </button>
    </div>
</section>

    <section class="property-map">
    <script src='https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCrMzetdv25NdoUlfkVQuBVUcRw4_NGXcc'></script>
    <div style='overflow:hidden;height:600px;width:100%;'>
        <div id='gmap_canvas' style='height:600px;width:100%;'></div>
        <div><small><a href="http://www.embedgooglemaps.com">Generate your Google map here, quick and easy!</a></small></div>
        <div><small><a href="https://binaireoptieservaringen.nl/">Meer informatie over binaire opties in Nederland vind je hier!</a></small></div>
        <style>
        #gmap_canvas img {
            max-width: none!important;
            background: none!important
        }
        </style>
    </div>
    <script type='text/javascript'>
    function init_map() {
        var myOptions = {
            zoom: 10,
            center: new google.maps.LatLng(26.419752, -80.11542889999998),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
        marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(26.419752, -80.11542889999998)
        });

        var styles = 
        [
            {
                "featureType": "landscape",
                "stylers": [
                    {
                        "hue": "#FFBB00"
                    },
                    {
                        "saturation": 43.400000000000006
                    },
                    {
                        "lightness": 37.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "stylers": [
                    {
                        "hue": "#FFC200"
                    },
                    {
                        "saturation": -61.8
                    },
                    {
                        "lightness": 45.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "stylers": [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 51.19999999999999
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.local",
                "stylers": [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 52
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "hue": "#0078FF"
                    },
                    {
                        "saturation": -13.200000000000003
                    },
                    {
                        "lightness": 2.4000000000000057
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "hue": "#00FF6A"
                    },
                    {
                        "saturation": -1.0989010989011234
                    },
                    {
                        "lightness": 11.200000000000017
                    },
                    {
                        "gamma": 1
                    }
                ]
            }
        ];

        map.setOptions({styles: styles});

        infowindow = new google.maps.InfoWindow({
            content: '<strong>My Property</strong><br>Bocaire<br>'
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });
        infowindow.open(map, marker);
    }
    google.maps.event.addDomListener(window, 'load', init_map);
    </script>



    </section>

    <section class="property-main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="filter-panel">
                        <div class="filter-tab clearfix">
                            <h4 class="text-center">Mortgage Calculator</h4>
                            <hr>
                            <form  method="post" id="mortgageCalculator">
                                <div class="form-group">
                                    <label for="sale_price">Price of Home ($<span id="sale_price_label"></span>)</label>
                                    <input type="text" name="sale_price" id="sale_price" value="<?php echo $this->session->flashdata('mortgage_price_value'); ?>" class="form-control">
                                    <input type="hidden" name="calculator_url" value="<?php echo base_url();?>home/mortgage_calculator">
                                </div>
                                <div class="form-group">
                                    <label for="down_percent">Downpayment (<span id="down_percent_label"></span>%)</label>
                                    <input type="text" name="down_percent" id="down_percent" value="<?php echo $this->session->flashdata('mortgage_downpayment_value'); ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="mortgage_interest_percent">Annual Interest Rate (<span id="mortgage_interest_percent_label"></span>%)</label>
                                    <input type="text" name="mortgage_interest_percent" id="mortgage_interest_percent" value="<?php echo $this->session->flashdata('mortgage_interest_rate_value'); ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="year_term">Loan Term (<span id="year_term_label"></span>  years)</label>
                                    <input type="text" name="year_term" id="year_term" value="<?php echo $this->session->flashdata('mortgage_loan_terms_value'); ?>" class="form-control">
                                </div>
                                <div class="col-md-12 col-sm-12 nopadding">
                                    <button class="btn btn-default btn-block submit-button" id="morg_calc_btn">Calculate <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i></button>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                   <div id="mortgage-result">
                                       <?php echo $this->session->flashdata('mortgage_monthly'); ?>
                                       <!-- <?php echo $this->session->flashdata('mortgage_monthly_interest'); ?>
                                       <?php echo $this->session->flashdata('mortgage_annual_interest'); ?>
                                       <?php echo $this->session->flashdata('mortgage_downpayment'); ?>
                                       <?php echo $this->session->flashdata('mortgage_amount_financed'); ?> -->
                                   </div>
                                </div>
                            </form>
                        </div>
                        <div class="filter-tab">
                            <div class="property-submit-question">
                                <h4 class="text-center">Have a question?</h4>
                                <hr>
                                <form action="">
                                    <div class="form-group">
                                        <label for="">Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                    <button class="btn btn-default btn-block submit-button">Submit</button>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="search-listings">
                        <div class="col-md-4 col-sm-6">
                            <div class="search-listing-item">
                                <div class="search-property-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/test-property-img3.jpg" alt="" class="img-responsive">    
                                </div>
                                <div class="search-listing-title">
                                    <div class="col-md-9 col-xs-9 padding-0">
                                        <p class="search-property-title"><a href="<?=base_url()?>demo/agent_haven/featured_property">257 NE 14th Street, Ocean Avenue </a></p>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <p><a href=""><span><i class="fa fa-heart"></i></a></span></p>
                                    </div>    
                                </div>
                                
                                <p class="search-property-location"><i class="fa fa-map-marker"></i> : 257 NE 14th Street, Ocean Avenue</p>
                                <p><i class="fa fa-usd"></i> : 1,000,000</p>
                                <ul class="list-inline search-property-specs">
                                    <li><i class="fa fa-home"></i> 500000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                </ul>
                            </div>
                        </div>
                    
                        <div class="col-md-4 col-sm-6">
                            <div class="search-listing-item">
                                <div class="search-property-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="search-listing-title">
                                    <div class="col-md-9 col-xs-9 padding-0">
                                        <p class="search-property-title"><a href="<?=base_url()?>demo/agent_haven/featured_property">Marriot Hotel</a></p>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <p><a href=""><span><i class="fa fa-heart-o"></i></a></span></p>
                                    </div>    
                                </div>
                                
                                <p class="search-property-location"><i class="fa fa-map-marker"></i> : Cebu Business Park</p>
                                <p><i class="fa fa-usd"></i> : 1,000,000</p>
                                <ul class="list-inline search-property-specs">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                </ul>
                            </div>
                        </div>
                    
                    
                        <div class="col-md-4 col-sm-6">
                            <div class="search-listing-item">
                                <div class="search-property-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="search-listing-title">
                                    <div class="col-md-9 col-xs-9 padding-0">
                                        <p><a href="<?=base_url()?>demo/agent_haven/featured_property">Marriot Hotel</a></p>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <p><a href=""><span><i class="fa fa-heart-o"></i></a></span></p>
                                    </div>    
                                </div>
                                
                                <p class="search-property-location"><i class="fa fa-map-marker"></i> : Cebu Business Park</p>
                                <p><i class="fa fa-usd"></i> : 1,000,000</p>
                                <ul class="list-inline search-property-specs">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                </ul>
                            </div>
                        </div>
                    
                        <div class="col-md-4 col-sm-6">
                            <div class="search-listing-item">
                                <div class="search-property-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="search-listing-title">
                                    <div class="col-md-9 col-xs-9 padding-0">
                                        <p><a href="<?=base_url()?>demo/agent_haven/featured_property">Marriot Hotel</a></p>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <p><a href=""><span><i class="fa fa-heart"></i></a></span></p>
                                    </div>    
                                </div>
                                
                                <p class="search-property-location"><i class="fa fa-map-marker"></i> : Cebu Business Park</p>
                                <p><i class="fa fa-usd"></i> : 1,000,000</p>
                                <ul class="list-inline search-property-specs">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="search-listing-item">
                                <div class="search-property-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="search-listing-title">
                                    <div class="col-md-9 col-xs-9 padding-0">
                                        <p><a href="<?=base_url()?>demo/agent_haven/featured_property">Marriot Hotel</a></p>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <p><a href=""><span><i class="fa fa-heart-o"></i></a></span></p>
                                    </div>    
                                </div>
                                
                                <p class="search-property-location"><i class="fa fa-map-marker"></i> : Cebu Business Park</p>
                                <p><i class="fa fa-usd"></i> : 1,000,000</p>
                                <ul class="list-inline search-property-specs">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="search-listing-item">
                                <div class="search-property-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="search-listing-title">
                                    <div class="col-md-9 col-xs-9 padding-0">
                                        <p><a href="<?=base_url()?>demo/agent_haven/featured_property">Marriot Hotel</a></p>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <p><a href=""><span><i class="fa fa-heart-o"></i></a></span></p>
                                    </div>    
                                </div>
                                
                                <p class="search-property-location"><i class="fa fa-map-marker"></i> : Cebu Business Park</p>
                                <p><i class="fa fa-usd"></i> : 1,000,000</p>
                                <ul class="list-inline search-property-specs">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="search-listing-item">
                                <div class="search-property-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="search-listing-title">
                                    <div class="col-md-9 col-xs-9 padding-0">
                                        <p><a href="<?=base_url()?>demo/agent_haven/featured_property">Marriot Hotel</a></p>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <p><a href=""><span><i class="fa fa-heart-o"></i></a></span></p>
                                    </div>    
                                </div>
                                
                                <p class="search-property-location"><i class="fa fa-map-marker"></i> : Cebu Business Park</p>
                                <p><i class="fa fa-usd"></i> : 1,000,000</p>
                                <ul class="list-inline search-property-specs">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="pagination-area">
                        <nav>
                            <ul class="pagination">
                                <li>
                                    <a href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li>
                                    <a href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('/session/footer'); ?>
