<?php
    $this->load->view('session/header-page');
?>
        <section class="advance-search-area">
            <form method="get" action="<?php echo base_url(); ?>listings" class="advance-search-form">
                <div class="visible-xs">
                        <!-- <button class="btn btn-default submit-button btn-show-filter">Show Filters</button> -->
                        <a href="javascript:void(0);" class="btn btn-default submit-button btn-show-filter"><i class="fa fa-search"></i> Show Filters</a>
                </div>
                <nav class="navbar  navbar-search">
                    <div class="container">
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="navbar-form navbar-left nav-search-box" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control input-search" placeholder="Search for..." name="Search" <?php
                                            if(isset($_GET['Search']) && $_GET['Search']) {
                                                echo "value='".$_GET['Search']."'";
                                            }
                                        ?>>
                                </div>
                            </div>
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-usd" aria-hidden="true"></i> <span class="search-label">Price</span> (<span id="price_label"><?php
                                        if(isset($_GET['min_price']) && $_GET['min_price']) {
                                            if(isset($_GET['max_price']) && $_GET['max_price']) {
                                                echo number_format($_GET['min_price'])." - ".number_format($_GET['max_price']);
                                            }
                                            else {
                                                echo number_format($_GET['min_price']);
                                            }
                                        }
                                        if(isset($_GET['max_price']) && $_GET['max_price'] && !(isset($_GET['min_price']) || $_GET['min_price'])) { 
                                            echo number_format($_GET['max_price']);
                                        }
                                        if(
                                            !(isset($_GET['min_price']) && $_GET['min_price']) && !(isset($_GET['max_price']) && $_GET['max_price'])
                                        ) {
                                            echo "Any";
                                        }

                                    ?></span>)<span class="caret"></span></a>
                                    <div class="dropdown-menu">
                                        <div class="col-md-12">
                                            <label>From</label>
                                            <input type="text" class="form-control" name="min_price" id="min_price" <?php if(isset($_GET['min_price']) && $_GET['min_price']) { echo "value='".$_GET['min_price']."'"; } ?>>
                                            
                                            <label>To</label>
                                            <input type="text" class="form-control" name="max_price" id="max_price" <?php if(isset($_GET['max_price']) && $_GET['max_price']) { echo "value='".$_GET['max_price']."'"; } ?>>
                                            <a href="" style="width:100%;display: block;text-align: center;padding: 5px;"><small>Dismiss</small></a>                                            
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bed" aria-hidden="true"></i>  <span class="search-label">Beds</span> (<span id="bedroom_label"><?php if(isset($_GET['bedroom']) && $_GET['bedroom'] ) { echo $_GET['bedroom']."+"; } else { echo "Any";} ?></span>)<span class="caret"></span>
                                    </a>
                                    <input type="hidden" name="bedroom" id="bedroom" <?php if(isset($_GET['bedroom']) && $_GET['bedroom'] ) { echo "value='".$_GET['bedroom']."'"; } ?>>
                                    <ul class="dropdown-menu">
                                        <li><a href="" onclick="setFieldValue('bedroom', 'Any')">Any</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '1')">1+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '2')">2+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '3')">3+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '4')">4+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '5')">5+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '6')">6+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '7')">7+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '8')">8+</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-toilet" aria-hidden="true"></i> <span class="search-label">Baths</span> (<span id="bathroom_label"><?php if(isset($_GET['bathroom']) && $_GET['bathroom'] ) { echo $_GET['bathroom']."+"; } else { echo "Any";} ?></span>)<span class="caret"></span>
                                    </a>
                                    <input type="hidden" name="bathroom" id="bathroom" <?php if(isset($_GET['bathroom']) && $_GET['bathroom'] ) { echo "value='".$_GET['bathroom']."'"; } ?>>
                                    <ul class="dropdown-menu">
                                        <li><a href="" onclick="setFieldValue('bathroom', 'Any')">Any</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '1')">1+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '2')">2+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '3')">3+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '4')">4+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '5')">5+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '6')">6+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '7')">7+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '8')">8+</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav more-filters">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" id="moreFiltersToggle" class="moreFiltersToggle"></i> More Filters <span class="search-label">(Show)</span></span></a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav submit-filters">
                                <li class="dropdown">
                                    <button type="submit"  class="submit-button submit-general"> <i class="fa fa-search"></i> <span class="search-label">Search</span> </button>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav pull-right show-map-area">
                                <li class="dropdown">
                                    <a href="#" class="view-map-button"><i class="fa fa-map"></i> <span class="show-map-label">Show map</span> </a>
                                </li>
                                <li>
                                    <?php if( isset($_GET) AND !empty($_GET)) : ?>
                                        <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" id="moreFiltersToggle" class="follow-search"><i class="fa fa-undo"></i> <span class="search-label">Follow</span> </a>
                                    <?php endif;?>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div> 
                </nav>
                <div class="container" id="more-filters-area" style="display:none;">
                    <div class="row more-filter">
                        <div class="container">
                       <!--  <div class="col-md-12">
                            <div class="alert alert-danger">
                                <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> This section is still under development. Click <a href="" class="moreFiltersToggle">here</a> to hide this area</p>
                            </div>
                        </div> -->
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group" id="property_type_container">
                                <h3>Property Types</h3>
                                <hr>
                                <div class="checkbox"><label><input name="property_type[]" value="Residential" type="checkbox" id="residential"> Residential </label></div>
                                <div class="checkbox"><label><input name="property_type[]" value="MultiFamily" type="checkbox" id="multifamily"> MultiFamily </label></div>
                                <div class="checkbox"><label><input name="property_type[]" value="Land" type="checkbox" id="land"> Land </label></div>
                                <div class="checkbox"><label><input name="property_type[]" value="Commercial" type="checkbox" id="commercial"> Commercial </label></div>
                                <div class="checkbox"><label><input name="property_type[]" value="Rental" type="checkbox" id="rental"> Rental </label></div>
                            </div>
                            <div class="form-group" id="property_sub_type_container" style="display:none;">
                                <h3>Property Sub Types</h3>
                                <hr>
                                <div id="residential_types" style="display:none;">
                                    <div class="checkbox"><label><input name="residential_type[]" value="Single Family" type="checkbox"> Single Family Detached </label></div>
                                    <div class="checkbox"><label><input name="residential_type[]" value="Condo" type="checkbox"> Condo/Co-op </label></div>
                                    <div class="checkbox"><label><input name="residential_type[]" value="Townhouse" type="checkbox"> Townhouse </label></div>
                                    <div class="checkbox"><label><input name="residential_type[]" value="Manufactured" type="checkbox"> Mobile/Manufactured </label></div>
                                </div>
                                <div id="multifamily_types" style="display:none;">
                                    <div class="checkbox"><label><input name="multifamily_type[]" value="Condo"  type="checkbox"> Condo/Coop </label></div>
                                    <div class="checkbox"><label><input name="multifamily_type[]" value="Townhouse" type="checkbox"> Townhouse </label></div>
                                    <div class="checkbox"><label><input name="multifamily_type[]" value="Plex"  type="checkbox"> Duplex/Triplex/Quadplex </label></div>
                                    <div class="checkbox"><label><input name="multifamily_type[]" value="House Plus Units" type="checkbox"> House Plus Units </label></div>
                                    <div class="checkbox"><label><input name="multifamily_type[]" value="6 Units" type="checkbox"> 6 Units </label></div>
                                </div>
                                <div id="land_types" style="display:none;">
                                    <div class="checkbox"><label><input name="land_type[]" value="Single Family" type="checkbox"> Sale </label></div>
                                    <div class="checkbox"><label><input name="land_type[]" value="Condo"  type="checkbox"> Farm </label></div>
                                    <div class="checkbox"><label><input name="land_type[]" value="Mobile Home" type="checkbox"> Ranch </label></div>
                                    <div class="checkbox"><label><input name="land_type[]" value="Farm" type="checkbox"> Combo </label></div>
                                </div>
                                <div id="commercial_types" style="display:none;">
                                    <div class="checkbox"><label><input name="commercial_type[]" value="Office" type="checkbox"> Office </label></div>
                                    <div class="checkbox"><label><input name="commercial_type[]" value="Showroom"  type="checkbox"> Showroom </label></div>
                                    <div class="checkbox"><label><input name="commercial_type[]" value="Warehouse" type="checkbox"> Warehouse </label></div>
                                    <div class="checkbox"><label><input name="commercial_type[]" value="Building" type="checkbox"> Building </label></div>
                                    <div class="checkbox"><label><input name="commercial_type[]" value="Multi-Use" type="checkbox"> Multi-Use </label></div>
                                    <div class="checkbox"><label><input name="commercial_type[]" value="Developed Land" type="checkbox"> Developed Land </label></div>
                                    <div class="checkbox"><label><input name="commercial_type[]" value="Mobile Home" type="checkbox"> Mobile Home </label></div>
                                    <div class="checkbox"><label><input name="commercial_type[]" value="Other" type="checkbox"> Other </label></div>
                                </div>
                                <div id="rental_types" style="display:none;">
                                    <div class="checkbox"><label><input name="rental_type[]" value="Single Family" type="checkbox"> Single Family Detached </label></div>
                                    <div class="checkbox"><label><input name="rental_type[]" value="Condo"  type="checkbox"> Condo/Coop </label></div>
                                    <div class="checkbox"><label><input name="rental_type[]" value="Townhouse"  type="checkbox"> Townhouse </label></div>
                                    <div class="checkbox"><label><input name="rental_type[]" value="Plex"  type="checkbox"> Duplex/Triplex/Quadplex </label></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h3>Show Me</h3>
                            <hr>
                            <div class="form-group">
                                <div class="checkbox"><label><input name="listing_change_type[]" value="New Listing" type="checkbox"> New Listings</label></div>
                                <div class="checkbox"><label><input name="listing_change_type[]" value="New Construction" type="checkbox"> New Construction</label></div>
                                <div class="checkbox"><label><input name="listing_change_type[]" value="Open House" type="checkbox"> Open Houses</label></div>
                                <div class="checkbox"><label><input name="listing_change_type[]" value="Price Reduced" type="checkbox"> Price Reduced</label></div>
                            </div>

                            <h3>Recently Sold</h3>
                            <hr>
                            <div class="form-group">
                                <div class="checkbox"><label><input name="recently_sold[]" value="Residential" type="checkbox" <?php  if(isset($_GET['recently_sold']) && $_GET['recently_sold'] =='Residential')  echo "checked ='checked'";?> > Residential</label></div>
                                <div class="checkbox"><label><input name="recently_sold[]" value="Rental" type="checkbox" <?php if(isset($_GET['recently_sold']) && $_GET['recently_sold']=='Rental') echo "checked ='checked'";?>> Rental</label></div>
                                <div class="checkbox"><label><input name="recently_sold[]" value="Commercial" type="checkbox"  <?php  if(isset($_GET['recently_sold']) && $_GET['recently_sold'] =='Commercial') echo "checked ='checked'";?>> Commercial</label></div>
                                <div class="checkbox"><label><input name="recently_sold[]" value="Land" type="checkbox"  <?php if(isset($_GET['recently_sold']) && $_GET['recently_sold'] =='Land') echo "checked ='checked'";?>> Land</label></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h3>Features</h3>
                            <hr>
                            <div class="form-group">
                                <select name="house_size" class="form-control">
                                    <option value="0">Any House Size</option>
                                    <?php $counter = 600; for($i=0; $i<20; $i++) { ?>
                                        <option value="<?php echo $counter; ?>"><?php echo $counter."+ sq ft"; ?></option>
                                    <?php if($counter < 2000) $counter += 200; elseif($counter < 3000) $counter += 250; elseif($counter < 4000) $counter += 500; else $counter += 1000; } ?>
                                </select> 
                            </div>
                            <div class="form-group">
                                <select name="lot_size"  class="form-control">
                                    <option value="0">Any Lot Size</option>
                                    <option value="21780">1/2+ acre</option>
                                    <option value="43560">1+ acre</option>
                                    <option value="87120">2+ acres</option>
                                    <option value="217800">5+ acres</option>
                                    <option value="435600">10+ acre</option>
                                    <option value="871200">20+ acre</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="house_age" class="form-control">
                                    <option>Any House Age</option>
                                    <option value="fiveyears">0-5 Years</option>
                                    <option value="tenyears">0-10 Years</option>
                                    <option value="fifthteenyears">0-15 Years</option>
                                    <option value="twentyyears">0-20 Years</option>
                                    <option value="fiftyyears">0-50 Years</option>
                                    <option value="fiftyyearsplus">51+ Years</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="garage"> 2+ Car Garage</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="pool"> Swimming Pool</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="view"> View</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="waterfront"> Waterfront</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="Cul-De-Sac"> Cul-De-Sac</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="golf"> Golf Course View</label></div>
                                <div><a href="javacript:;" data-toggle="modal" data-target="#features_modal">View all features</a></div>
                                <style>
                                    .modal-backdrop.in {
                                        display: none;
                                    }
                                    .modal-open .modal {
                                        overflow-x: hidden;
                                        overflow-y: hidden;
                                    }
                                    .show-me-text {
                                        color: inherit !important;
                                    }
                                </style>
                                <div class="modal fade" id="features_modal" style="margin-top:198px; color:#000000; width:95%;">
                                    <div class="modal-dialog" style="width:56%;">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Features</h4>
                                            </div>
                                            <div class="modal-body" style="color:#000000;">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h4>Inside</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="hardwood"> Hardwood Floors</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="fireplace"> Fireplace</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="energy"> Energy Efficient</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="disable"> Disability Features</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h4>Heating/Cooling</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="cool"> Central Air</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="heat"> Central Heat</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="air"> Forced Air</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h4>Community</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="recreation"> Recreation Facilities</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="security"> Comm Security Features</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="pool"> Comm Swimming Pool(s)</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="boat" > Comm Boat Facilities</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="clubhouse"> Comm Clubhouse(s)</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="horse" > Comm Horse Facilities</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="tennis"> Comm Tennis Court(s)</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="park"> Comm Park(s)</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="golf"> Comm Golf</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="senior"> Senior Comm</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="spa"> Comm Spa/Hot Tub(s)</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h4>Outdoor Amenities</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="pool"> Swimming Pool</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="boat"> RV/Boat Parking</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="spa"> Spa/Hot Tub</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="horse"> Horse Facilities</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="tennis"> Tennis Courts</label></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h4>Rooms</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="dining"> Dining</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="laundry"> Laundry</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="family"> Family</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="basement"> Basement</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="office"> Den/Office</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="games"> Games</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h4>Lot</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="corner lot"> Corner Lot</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="cul-de-sac"> Cul-De-Sac</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="waterfront"> WaterFront</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="water view"> Water View</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="golf"> Golf Course Lot/Frontage</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h4>Parking</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='2'> Garage 1+</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='3'> Garage 2+</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='spa'> Spa/Hot Tub</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="4"> Garage 3+</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="carport"> Carport</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h4>Stories</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='story'> Single Story</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='stories'> Multi</label></div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-success submit-button" data-dismiss="modal">Apply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-default submit-button btn-lg">
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    </div>
                </div>
            </form>
            <div class="visible-xs">
                <a href="#" class="view-map-button submit-button"><i class="fa fa-map"></i> <span class="search-label">Show map</span> </a>
                <button class="btn btn-default submit-button filter-search-button">
                     <i class="fa fa-filter"></i> More Options
                 </button>
                <?php if( isset($_GET) AND !empty($_GET)) : ?>
                    <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" id="moreFiltersToggle" class="follow-search submit-button"><i class="fa fa-undo"></i> <span class="search-label">Follow</span>  </a>
                <?php endif;?>
            </div>
        </section>

        <section class="property-map">
        <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script> -->
        <div id="map_canvas" style="border: 2px solid #3872ac;"></div>
        <style type="text/css">
            html,
            body,
            #map_canvas  {
                height: 600px;
                width: 100%;
                margin-top: 0px;
                padding: 0px;
            }
        </style>
        
       <script type="text/javascript">

                
                    function initialize() {
                                map = new google.maps.Map(
                                    document.getElementById("map_canvas"), {

                                      center: new google.maps.LatLng(37.387474,-122.05754339999999),
                                       zoom: 13,
                                       scrollwheel: false,
                                       mapTypeId: google.maps.MapTypeId.ROADMAP
                                     });

                                geocoder = new google.maps.Geocoder();

                            <?php 
                                if(isset($saved_searches) && $saved_searches) {
                                    foreach($saved_searches as $saved) {
                            ?>
                                    //base_url()."home/other_property?listingId=".$property["Id"]
                                    var l1 = "<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']?>";
                                    var l2 = "<?php echo $saved['StandardFields']['UnparsedAddress']?>";
                                    var l3 = base_url + "home/other_property?listingId=<?php echo $saved['Id']?>";
                                    var l4 = "<?= (isset($saved['StandardFields']['Photos'][0]['Uri300'])) ? $saved['StandardFields']['Photos'][0]['Uri300'] : ""; ?>";

                                    var locations = [
                                        [l1, l2, l3, l4]
                                    ];

                                    geocodeAddress(locations);

                             <?php } } ?>

                     }

                            var geocoder;
                            var map;
                            var bounds = new google.maps.LatLngBounds();

                            function geocodeAddress(locations, i) {
                                var title = locations[0][0];
                                var address = locations[0][1];
                                var url = locations[0][2];
                                var img = locations[0][3];

                                geocoder.geocode({
                                  'address': locations[0][1]
                                },

                                function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        var marker = new google.maps.Marker({
                                              icon: 'http://maps.google.com/mapfiles/ms/icons/red.png',
                                              map: map,
                                              position: results[0].geometry.location,
                                              title: title,
                                              animation: google.maps.Animation.DROP,
                                              address: address,
                                              url: url
                                        })
                                        //infoWindow(marker, map, title, address, url, img);

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });

                                        google.maps.event.addListener(marker, 'click', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });
              

                                         google.maps.event.addListener(marker, 'mouseout', function() {
                                            iw.close();
                                        });

                                        bounds.extend(marker.getPosition());
                                        map.fitBounds(bounds);

                                    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) { 

                                        wait = true;

                                        setTimeout(function() {
                                            geocodeAddress(locations, i);
                                        }, 1000 );

                                       // setTimeout("wait = true", 10000);
                                        
                                    } else {
                                        console.log( "Geocode was not successful for the following reason: " + status );
                                        //alert("Geocode was not successful for the following reason: " + status);
                                    }
                                 
                                });
                            }
          

                    google.maps.event.addDomListener(window, "load", initialize);

                    function infoWindow(marker, map, title, address, url,img) {
                          google.maps.event.addListener(marker, 'mouseover', function() {
                            var html = "<div><h3>" + title + "</h3><p>" + address + "<br><img src="+img+" alt="+address+" width='200'  height='150'><br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                            iw = new google.maps.InfoWindow({
                              content: html,
                              maxWidth: 350
                            });
                            iw.open(map, marker);

                          });

                          google.maps.event.addListener(marker, 'mouseout', function() {
                                
                                new google.maps.InfoWindow.close(map, marker);
                            });

                    }

                    
            </script>        
    </section>

    <section class="property-main-content">
        <div class="container">
            <div class="row">
                <div class="sorting">
                    <div class="col-md-offset-0 col-md-12 col-sm-12">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <!-- <div class="col-md-4 col-sm-6 col-xs-4">
                                <p>Sort by:</p>    
                            </div>
                            <div class="col-md-8 col-sm-6 col-xs-8">
                                <select name="" id="">
                                    <option value="">Price</option>
                                    <option value="">Bedrooms</option>
                                    <option value="">Bathrooms</option>
                                    <option value="">Square feet</option>
                                    <option value="">Newest</option>
                                </select>    
                            </div> -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <!-- <div class="col-md-3 col-sm-6 col-xs-4">
                                <p>Display:</p>    
                            </div>
                            <div class="col-md-9 col-sm-6 col-xs-8">
                                <select name="" id="">
                                    <option value="9">9</option>
                                    <option value="18">18</option>
                                    <option value="24">24</option>
                                </select>    
                            </div> -->
                        </div>
                        <!-- <div class="col-md-4 col-sm-3 col-xs-6">
                            <button class="btn btn-default submit-button view-map-button"><i class="fa fa-map"></i> Show map</button>
                        </div> -->

                    </div>
                </div>
                <?php if( isset($_GET) AND !empty($_GET)) : ?>
                <div class="sorting">
                    <div class="col-md-offset-3 col-md-10">                       
                        <div class="col-md-3 pull-right">
                            <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" class="btn btn-default submit-button follow-search"><i class="fa fa-undo"></i> Save Search </a>
                        </div>
                    </div>
                </div>
                <?php endif;?>
                <div class="col-md-3 col-sm-3">
                    <div class="filter-panel sidebar">
                        <div class="filter-tab">
                            <h4 class="text-center">Mortgage Calculator</h4>
                            <hr>
                            <form  method="post" id="mortgageCalculator">
                                <div class="form-group">
                                    <label for="sale_price">Price of Home ($<span id="sale_price_label"></span>)</label>
                                    <input type="text" name="sale_price" id="sale_price" value="<?php echo $this->session->flashdata('mortgage_price_value'); ?>" class="form-control">
                                    <input type="hidden" name="calculator_url" value="<?php echo base_url();?>home/mortgage_calculator">
                                </div>
                                <div class="form-group">
                                    <label for="down_percent">Downpayment (<span id="down_percent_label"></span>%)</label>
                                    <input type="text" name="down_percent" id="down_percent" value="<?php echo $this->session->flashdata('mortgage_downpayment_value'); ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="mortgage_interest_percent">Annual Interest Rate (<span id="mortgage_interest_percent_label"></span>%)</label>
                                    <input type="text" name="mortgage_interest_percent" id="mortgage_interest_percent" value="<?php echo $this->session->flashdata('mortgage_interest_rate_value'); ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="year_term">Loan Term (<span id="year_term_label"></span>  years)</label>
                                    <input type="text" name="year_term" id="year_term" value="<?php echo $this->session->flashdata('mortgage_loan_terms_value'); ?>" class="form-control">
                                </div>
                                <div class="col-md-12 col-sm-12 nopadding">
                                    <button class="btn btn-default btn-block submit-button"  id="morg_calc_btn">Calculate <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i></button>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                   <div id="mortgage-result">
                                       <?php echo $this->session->flashdata('mortgage_monthly'); ?>
                                       <!-- <?php echo $this->session->flashdata('mortgage_monthly_interest'); ?>
                                       <?php echo $this->session->flashdata('mortgage_annual_interest'); ?>
                                       <?php echo $this->session->flashdata('mortgage_downpayment'); ?>
                                       <?php echo $this->session->flashdata('mortgage_amount_financed'); ?> -->
                                   </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="filter-panel">
                        <div class="filter-tab">
                            <div class="property-submit-question">
                                <h4 class="text-center">Have a question?</h4>
                                <div class="question-mess" ></div>  <br/>      
                                <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions">
                                    <div class="form-group">
                                        <label for="">Name</label>
                                        <input type="text" value="" name="name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="text" value="" name="email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea id="" cols="30" rows="5" value="" name="message" class="form-control" required></textarea>
                                    </div>
                                    <button class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9">
                    <div class="search-listings">
                        <?php
                        if(!isset($properties)) {
                            ?>
                        <!-- <div class="alert alert-warning" role="alert">
                            <h1>Results not found</h1>
                        </div> -->
                        <div class="nosold-property">
                          <i class="fa fa-exclamation-triangle"> </i>
                          Results not found
                        </div>
                            <?php
                        }
                        else {
                            // echo "<pre>";
                            // var_dump($json_property);
                            // echo "</pre>";
                            $countF = 0; 
                            if($saved_searches) {
                                foreach($saved_searches as $saved) {
                                            $countF++;
                                ?>
                                            <div class="col-md-4 col-sm-6">
                                                <div class="search-listing-item">
                                                     <div class="search-property-image">
                                                        <a href="<?= base_url();?>home/other_property?listingId=<?= $saved["StandardFields"]["ListingKey"]; ?>">
                                                             <?php if(isset($saved["StandardFields"]["Photos"][0]["Uri300"])) { ?>
                                                            <img src="<?=$saved["StandardFields"]["Photos"][0]["Uri300"]?>" alt="<?php echo $saved["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;">
                                                            
                                                         <?php } else { ?>

                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;">

                                                         <?php } ?>
                                                        </a>    
                                                    </div>
                                                    <div class="search-listing-type">
                                                        <?php
                                                            if(isset( $saved["StandardFields"]["PropertyClass"])){?>
                                                                <?php echo $saved["StandardFields"]["PropertyClass"];?>
                                                        <?php  } ?> 
                                                    </div>
                                                    <div class="search-listing-title">
                                                        <div class="col-md-9 col-xs-9 padding-0">
                                                            <p class="search-property-title">
                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $saved["StandardFields"]["ListingKey"]; ?>">
                                                                    <?php echo $saved["StandardFields"]["UnparsedFirstLineAddress"]; ?>
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-3 col-xs-3">
                                                            <p>
                                                                <?php  $property_saved = FALSE; if( isset( $_SESSION["save_properties"] ) ) : ?>
                                                                    <?php foreach( $_SESSION["save_properties"] as $key ) : ?>
                                                                        <?php if( $key['property_id'] == $saved["StandardFields"]["ListingKey"] ) : $property_saved = TRUE; break; endif; ?>
                                                                    <?php endforeach; ?>
                                                                <?php else : $property_saved = FALSE; endif;?>

                                                                <a href="javascript:void(0)" data-pro-id="<?=$saved["StandardFields"]["ListingKey"]?>" class="save-favorate" title="Save Property" >
                                                                    <span id="isSaveFavorate_<?= $saved["StandardFields"]["ListingKey"]; ?>">
                                                                        <?php echo ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>" ;?>
                                                                    </span>
                                                                </a>
                                                            </p>
                                                        </div>    <!-- <i class="fa fa-heart-o"></i> -->
                                                    </div>
                                                    
                                                    <p><i class="fa fa-map-marker"></i> 
                                                    <?php
                                                        $mystring = $saved["StandardFields"]["City"];
                                                        $findme   = '*';
                                                        $pos = strpos($mystring, $findme);

                                                        if($pos === false)
                                                            echo $saved["StandardFields"]["City"] . " " . $saved["StandardFields"]["StateOrProvince"] . ", " . $saved["StandardFields"]["PostalCode"];
                                                        else
                                                            echo $saved["StandardFields"]["City"] . " " . $saved["StandardFields"]["StateOrProvince"] . ", " . $saved["StandardFields"]["PostalCode"];
                                                    ?>
                                                   </i></p>
                                                    <p class="search-property-location"><i class="fa fa-usd"></i> <?=number_format($saved["StandardFields"]["CurrentPrice"]);?>
                                                    <ul class="list-inline search-property-specs">
                                                        <?php
                                                                if( $saved["StandardFields"]["BedsTotal"] && is_numeric($saved["StandardFields"]["BedsTotal"])) {
                                                            ?>
                                                            <li><i class="fa fa-bed"></i> <?=$saved["StandardFields"]["BedsTotal"]?> Bed</li>
                                                            <?php
                                                                }
                                                                if($saved["StandardFields"]["BathsTotal"] && is_numeric($saved["StandardFields"]["BathsTotal"])) {
                                                            ?>
                                                            <li><i class="icon-toilet"></i> <?=$saved["StandardFields"]["BathsTotal"]?> Bath</li>
                                                            <?php
                                                                }
                                                            ?>
                                                    </ul>
                                                </div>
                                            </div>
                            <?php
                                        }
                                    }
                            if($countF < 1) { ?>
                                <div class="col-md-12 col-sm-6 bg-danger" style="padding: 20px;">
                                    <h4>No Property Listings!</h4>
                                </div>
                        <?php }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
        $this->load->view('session/footer');
    ?>
