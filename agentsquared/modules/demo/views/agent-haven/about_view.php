<?php $this->load->view('/session/header-page');  ?>

      <div class="page-content">
          <section class="property-detail-content">
              <div class="container">
                  <div class="row">
                      <div class="col-md-3 col-sm-3">
                          <div class="filter-tab">
                              <div class="property-agent-info">
                                <h4 class="text-center">Agent</h4>
                                <hr>
                                <div class="agent-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/john.jpg" alt="" class="img-thumbnail">  
                                </div>
                                <h4 class="agent-name">
                                    John Doe
                                </h4>
                                <p>
                                    <i class="fa fa-phone-square"></i> Office: 888-587-7767 x 1
                                    <br>
                                    <i class="fa fa-2x fa-mobile"></i> Mobile: 561-239-2929
                                    <br>
                                    <i class="fa fa-fax"></i> Fax : 855-297-7507
                                    <br>
                                </p>
                                <p><i class="fa fa-envelope"></i> : <a href="">Email Me</a></p>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-9 col-sm-9">
                          <div class="col-md-12 col-md-offset-0 property-details">
                              <h3>About us  </h3>

                                       <p>
                                       Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                       </p>
                                
                              <hr>
                              <h3>Contact us  </h3>
                              <div class="question-mess" ></div>  <br/>
                              <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
                                <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="">Name</label>
                                          <input type="text" name="name" value="" class="form-control" required>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="">Email</label>
                                          <input type="email" name="email" value="" class="form-control" required>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                          <label for="">Message</label>
                                          <textarea name="message" id="" value="" cols="30" rows="5" class="form-control" required></textarea>
                                      </div>
                                    </div>
                                </div>
                                  <button type="submit" class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
                              </form>    
                          </div>
                      </div>
                  </div>
              </div>
          </section>
    </div>

<?php $this->load->view('/session/footer'); ?>