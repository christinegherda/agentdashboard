<?php $this->load->view('/session/header-page'); ?>

    <div class="page-content">
    <section class="property-detail-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="filter-tab">
                        <div class="property-agent-info">
                          <h4 class="text-center">Agent</h4>
                          <hr>
                          <div class="agent-image">
                              <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/john.jpg" alt="" class="img-thumbnail">  
                          </div>
                          <h4 class="agent-name">
                              John Doe
                          </h4>
                          <p>
                              <i class="fa fa-phone-square"></i> Office: 888-587-7767 x 1
                              <br>
                              <i class="fa fa-2x fa-mobile"></i> Mobile: 561-239-2929
                              <br>
                              <i class="fa fa-fax"></i> Fax : 855-297-7507
                              <br>
                          </p>
                          <p><i class="fa fa-envelope"></i> : <a href="">Email Me</a></p>
                      </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5">
                    <div class="col-md-12 col-md-offset-0">
                        <h4>CONTACT ME  </h4>
                        <form action="">
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Message</label>
                                <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <button class="btn btn-default btn-block submit-button">Submit</button>
                        </form>    
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="filter-tab">
                        <h4>You can also contact me through these social media accounts</h4>
                        <hr>
                        <ul class="list-inline property-social-share">
                            <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>

<?php $this->load->view('/session/footer'); ?>