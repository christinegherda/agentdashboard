    
    <?php
        $this->load->view($theme.'/session/header-page');
    ?>

     <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-area">
                        
                            <h2><?php echo ucwords($page['title']); ?></h2>

                            <p>
                               <?php echo ucwords($page['content']); ?> 
                            </p> 
                            
                        </div>
                    </div>
                </div>
            </div>
  
    <?php

    if(isset($search_id) AND !empty($search_id)){

        if(isset($saved_searches) AND !empty($saved_searches)){?>

        <section class="saved-search-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <?php
                                $count = 0;

                               foreach($saved_searches as $saved){
    
                                    if($count < 8) { ?>
                                <div style="margin-bottom: 30px;" class="col-md-3 col-sm-4">
                                    <div class="property-image <?php if($saved['StandardFields']['PropertyClass'] == 'Land' || $saved['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                        <?php if(isset($saved['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $saved['StandardFields']['ListingKey']; ?>"><img src="<?=$saved['StandardFields']['Photos'][0]['Uri300']?>" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                        <?php } else { ?>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $saved['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                        <?php } ?>
                                    </div>
                                   <div class="property-listing-price">
                                        <div class="property-listing-type">
                                            <?=$saved['StandardFields']['PropertyClass'];?>
                                        </div>
                                        $<?=number_format($saved['StandardFields']['CurrentPrice']);?>
                                    </div>
                                    <div class="property-quick-icons <?php if($saved['StandardFields']['PropertyClass'] == 'Land' || $saved['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                        <ul class="list-inline">
                                            <?php
                                               if($saved['StandardFields']['BedsTotal'] && is_numeric($saved['StandardFields']['BedsTotal'])) {
                                            ?>
                                            <li><i class="fa fa-bed"></i> <?=$saved['StandardFields']['BedsTotal']?> Bed</li>
                                            <?php
                                                }
                                                if($saved['StandardFields']['BathsTotal'] && is_numeric($saved['StandardFields']['BathsTotal'])) {
                                            ?>
                                             <li><i class="icon-toilet"></i> <?=$saved['StandardFields']['BathsTotal']?> Bath</li>
                                            <?php
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="property-listing-description">
                                        <h5>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $saved['StandardFields']['ListingKey']; ?>">
                                                <?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>
                                            </a>
                                        </h5>
                                        <p><i>
                                          <?php
                                                $mystring = $saved['StandardFields']['City'];
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);

                                            if($pos === false)
                                                echo $saved['StandardFields']['City'] . ", " . $saved['StandardFields']['StateOrProvince'] . " " . $saved['StandardFields']['PostalCode'];
                                                else
                                                echo $saved['StandardFields']['PostalCity'] . ", " . $saved['StandardFields']['StateOrProvince'] . " " . $saved['StandardFields']['PostalCode'];
                                            ?>
                                        </i></p>    
                                    </div>
                                </div>

                                <?php  } $count++; 

                                }

                                if($count < 1) { ?>
                                    <div class="col-md-12 col-sm-6 bg-danger text-center" style="padding: 20px;">
                                        <h4>No Property Listings!</h4>
                                    </div>

                                <?php }  

                        } ?>

                        <?php if(isset($saved_searches) AND !empty($saved_searches)){
                                if (count($saved_searches) > 8){?>
                                 <div class="col-md-12 text-center">
                                    <div class="col-md-12 line-container">
                                            <p class="submit-line"></p>    
                                    </div>
                                    <a href="<?php echo base_url()?>home/popular_searches/<?php echo $search_id?>" target="_blank" class="btn btn-default submit-button">View All</a>    
                                </div>
                                
                    <?php       }
                            }
                        }
                    ?>

                    </div>
                </div>
            </div>
        </section>


        </div>

    <?php
        $this->load->view($theme.'/session/footer');
    ?>