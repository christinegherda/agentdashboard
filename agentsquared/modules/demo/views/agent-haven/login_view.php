<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/assets/css/landing_page.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<style type="text/css">
    .fb_btn{
        /*background-image: "../assets/images/no-profile-img.gif"; */
        background-image: url("/assets/images/fb2.png");
        background-repeat: no-repeat;
        color: #000;
    }
</style>
<body>
    <section class="sign-in-page clearfix">
        <div class="sign-up-strip clearfix">
            <div class="container">
                <div class="row">
                        <div class="left-sign-up">
                            <div class="col-md-6 col-sm-6">
                                <div class="welcoming-sign-up">
                                    <h1>Welcome to</h1>
                                    <a href="/home">
                                    <div class="logo-preset-customer"> <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/logo.png" alt="Agentsquared Signup" class="sign-up-logo"></div>
                                    </a>
                                </div>           
                            </div>
                        </div>
                        <div class="right-sign-up">
                            <div class="col-md-6 col-sm-6">
                                <div class="sign-in-container clearfix">
                                    <h2>Login</h2>
                                    <form class="form-signin" action="" method="post" id="signupForm">
                                        <div class="form-group">
                                            <label for="email">Username or Email</label>
                                             <input type="text" name="email" class="form-control" required> 
                                        </div>
                                        <div class="form-group mb-25px">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password" class="form-control" required>
                                        </div>
                                        <div class="col-md-12 col-sm-12  no-padding-right">
                                            <button class="btn btn-default btn-create-account" name="submit" type="submit">Sign In</button>
                                            <a href="javascript:void(0)" onclick ="customer_fb_login();" class="fb_btn" id="fb-btn" ><img style="width: 51%;" src="/assets/images/fb2.png"></a>   
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="sign-in-account text-center">
                                                <p>Don't have an account?</p>
                                                <p><a href="signup">Create Account</a></p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> 
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 footer-brand">
            <p class="text-center text-powered">2016 All Rights Reserved. Developed by
            <a href="http://www.agentsquared.com/" target="_blank" class="brand-name" data-original-title="" title=""> <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/logo.png" alt="Agentsquared Signup" class="footer-logo"></a>
            </p>

        </div>

    </section>
</body>
</html>
