<?php
    $this->load->view($theme.'/session/header-page');
?>
    <section class="property-main-content">
        <div class="container">
            <div class="row">
                <div class="nearby-listing-area">
                    <div class="col-md-3 col-sm-3">
                        <div class="filter-panel sidebar">
                            <div class="filter-tab">
                                <h4 class="text-center">Mortgage Calculator</h4>
                                <hr>
                                <form  method="post" id="mortgageCalculator">
                                    <div class="form-group">
                                        <label for="sale_price">Price of Home ($<span id="sale_price_label"></span>)</label>
                                        <input type="text" name="sale_price" id="sale_price" value="<?php echo $this->session->flashdata('mortgage_price_value'); ?>" class="form-control">
                                        <input type="hidden" name="calculator_url" value="<?php echo base_url();?>home/mortgage_calculator">
                                    </div>
                                    <div class="form-group">
                                        <label for="down_percent">Downpayment (<span id="down_percent_label"></span>%)</label>
                                        <input type="text" name="down_percent" id="down_percent" value="<?php echo $this->session->flashdata('mortgage_downpayment_value'); ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="mortgage_interest_percent">Annual Interest Rate (<span id="mortgage_interest_percent_label"></span>%)</label>
                                        <input type="text" name="mortgage_interest_percent" id="mortgage_interest_percent" value="<?php echo $this->session->flashdata('mortgage_interest_rate_value'); ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="year_term">Loan Term (<span id="year_term_label"></span>  years)</label>
                                        <input type="text" name="year_term" id="year_term" value="<?php echo $this->session->flashdata('mortgage_loan_terms_value'); ?>" class="form-control">
                                    </div>
                                    <div class="col-md-12 col-sm-12 nopadding">
                                        <button class="btn btn-default btn-block submit-button"  id="morg_calc_btn">Calculate <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i></button>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                       <div id="mortgage-result">
                                           <?php echo $this->session->flashdata('mortgage_monthly'); ?>
                                           <!-- <?php echo $this->session->flashdata('mortgage_monthly_interest'); ?>
                                           <?php echo $this->session->flashdata('mortgage_annual_interest'); ?>
                                           <?php echo $this->session->flashdata('mortgage_downpayment'); ?>
                                           <?php echo $this->session->flashdata('mortgage_amount_financed'); ?> -->
                                       </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="filter-panel">
                            <div class="filter-tab">
                                <div class="property-submit-question">
                                    <h4 class="text-center">Have a question?</h4>
                                    <hr>
                                    <div class="question-mess" ></div>  <br/>      
                                    <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions">
                                        <div class="form-group">
                                            <label for="">Name</label>
                                            <input type="text" value="" name="name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="text" value="" name="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Message</label>
                                            <textarea id="" cols="30" rows="5" value="" name="message" class="form-control" required></textarea>
                                        </div>
                                        <button class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9">
                        <div class="search-listings">
                            <?php
                                $countF = 0; 
                                if(isset($nearby_listings) AND !empty($nearby_listings)) {
                                    foreach($nearby_listings as $nearby) {
                                                $countF++;
                                    ?>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="search-listing-item">
                                                         <div class="search-property-image">
                                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby["StandardFields"]["ListingKey"]; ?>">
                                                                 <?php if(isset($nearby["StandardFields"]["Photos"][0]["Uri300"])) { ?>
                                                                <img src="<?=$nearby["StandardFields"]["Photos"][0]["Uri300"]?>" alt="<?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;">
                                                                
                                                             <?php } else { ?>

                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;">

                                                             <?php } ?>
                                                            </a>    
                                                        </div>
                                                        <div class="search-listing-type">
                                                            <?php
                                                                if(isset( $nearby["StandardFields"]["PropertyClass"])){?>
                                                                    <?php echo $nearby["StandardFields"]["PropertyClass"];?>
                                                            <?php  } ?> 
                                                        </div>
                                                        <div class="search-listing-title">
                                                            <div class="col-md-9 col-xs-9 padding-0">
                                                                <p class="search-property-title">
                                                                    <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby["StandardFields"]["ListingKey"]; ?>">
                                                                        <?php echo $nearby["StandardFields"]["UnparsedFirstLineAddress"]; ?>
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-3">
                                                                <p>
                                                                    <?php  $property_saved = FALSE; if( isset( $_SESSION["save_properties"] ) ) : ?>
                                                                        <?php foreach( $_SESSION["save_properties"] as $key ) : ?>
                                                                            <?php if( $key['property_id'] == $nearby["StandardFields"]["ListingKey"] ) : $property_saved = TRUE; break; endif; ?>
                                                                        <?php endforeach; ?>
                                                                    <?php else : $property_saved = FALSE; endif;?>

                                                                    <a href="javascript:void(0)" data-pro-id="<?=$nearby["StandardFields"]["ListingKey"]?>" class="save-favorate" title="Save Property" >
                                                                        <span id="isSaveFavorate_<?= $nearby["StandardFields"]["ListingKey"]; ?>">
                                                                            <?php echo ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>" ;?>
                                                                        </span>
                                                                    </a>
                                                                </p>
                                                            </div>    <!-- <i class="fa fa-heart-o"></i> -->
                                                        </div>
                                                        
                                                        <p><i class="fa fa-map-marker"></i> 
                                                        <?php
                                                            $mystring = $nearby["StandardFields"]["City"];
                                                            $findme   = '*';
                                                            $pos = strpos($mystring, $findme);

                                                            if($pos === false)
                                                                echo $nearby["StandardFields"]["City"] . " " . $nearby["StandardFields"]["StateOrProvince"] . ", " . $nearby["StandardFields"]["PostalCode"];
                                                            else
                                                                echo $nearby["StandardFields"]["City"] . " " . $nearby["StandardFields"]["StateOrProvince"] . ", " . $nearby["StandardFields"]["PostalCode"];
                                                        ?>
                                                       </i></p>
                                                        <p class="search-property-location"><i class="fa fa-usd"></i> <?=number_format($nearby["StandardFields"]["CurrentPrice"]);?>
                                                        <ul class="list-inline search-property-specs">
                                                            <?php
                                                                    if( $nearby["StandardFields"]["BedsTotal"] && is_numeric($nearby["StandardFields"]["BedsTotal"])) {
                                                                ?>
                                                                <li><i class="fa fa-bed"></i> <?=$nearby["StandardFields"]["BedsTotal"]?> Bed</li>
                                                                <?php
                                                                    }
                                                                    if($nearby["StandardFields"]["BathsTotal"] && is_numeric($nearby["StandardFields"]["BathsTotal"])) {
                                                                ?>
                                                                <li><i class="icon-toilet"></i> <?=$nearby["StandardFields"]["BathsTotal"]?> Bath</li>
                                                                <?php
                                                                    }
                                                                ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                        <?php
                                            }
                                        }
                                    ?>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </section>

    <?php
        $this->load->view('session/footer');
    ?>
