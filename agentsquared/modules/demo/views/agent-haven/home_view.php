<?php $this->load->view('/session/header'); ?>

    <section class="slider-area">
    </section>
    <section class="filters">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="filter-property">
                        <div class="filter-tabs">
                            <h1>Everybody needs a place called home</h1>
                        </div>
                        <form action="agent_haven/listings">
                            <div class="input-group">
                                <input type="text" class="form-control home-search" placeholder="State, City, Zip, MLS ID">
                                <span class="input-group-btn">
                                    <button class="btn btn-default submit-button" type="button">Search</button>
                                </span>
                            </div>
                        </form>
                        
                        <div class="help-info">
                            <div class="arrow-up"></div>
                            <p>Examples:</p>
                            <ul>
                                <li>
                                    <div class="col-md-4">
                                        <p>City, State</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>Las Vegas, NV</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-4">
                                        <p>Zip Code</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>90210</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-4">
                                        <p>Address</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>910 Hamilton ave, Campbell, Ca</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-4">
                                        <p>County</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>Santa Clara County, Ca</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-4">
                                        <p>MLSID</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>#12345</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="featured-listing-area">
        <div class="container">
            <div class="row">
                <div class="featured-title-container">
                    <div class="trapezoid"></div>
                    <h2 class="text-center section-title">Featured Properties</h2>
                    <p class="viewall">
                        <a href="agent_haven/listings" target="_blank" data-original-title="" title="">View All</a>    
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="featured-list">
                        <div class="col-md-3 col-sm-6 featured-list-item">
                            <div class="property-image">
                                <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/house1.jpg" alt="" class="img-responsive"></a>
                                <div class="property-listing-description">
                                    <p><a href="" class="listing-link"><b>The Property</b></a></p>
                                    <p>4 Privet Drive</p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-price">
                                <div class="property-listing-type">
                                    Residential
                                </div>
                                $1,000,000
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                    <li><i class="icon-toilet"></i> 2 Bath</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 featured-list-item">
                            <div class="property-image">
                                <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property2.jpg" alt="" class="img-responsive"></a>
                                <div class="property-listing-description">
                                    <p><a href="" class="listing-link"><b>The Property</b></a></p>
                                    <p>4 Privet Drive</p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-price">
                                <div class="property-listing-type">
                                    Residential
                                </div>
                                $1,000,000
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                    <li><i class="icon-toilet"></i> 2 Bath</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 featured-list-item">
                            <div class="property-image">
                                <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive"></a>
                                <div class="property-listing-description">
                                    <p><a href="" class="listing-link"><b>The Property</b></a></p>
                                    <p>4 Privet Drive</p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-price">
                                <div class="property-listing-type">
                                    Residential
                                </div>
                                $1,000,000
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                    <li><i class="icon-toilet"></i> 2 Bath</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 featured-list-item">
                            <div class="property-image">
                                <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/house2.jpg" alt="" class="img-responsive"></a>
                                <div class="property-listing-description">
                                    <p><a href="" class="listing-link"><b>The Property</b></a></p>
                                    <p>4 Privet Drive</p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-price">
                                <div class="property-listing-type">
                                    Residential
                                </div>
                                $1,000,000
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                    <li><i class="icon-toilet"></i> 2 Bath</li>
                                </ul>
                            </div>
                        </div>
                    </div>  
                </div>
                
            </div>
        </div>
    </section>
    
    <section class="other-listing-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="new-listings-area">
                        <h2 class="other-listing-title">New Listings
                            <span class="viewall-newlisting">
                                <a href="agent_haven/listings" target="_blank" data-original-title="" title="">View All</a>    
                            </span>
                        </h2>
                        <div class="col-md-6 col-sm-6 featured-list-item">
                            <div class="property-image">
                                <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/house1.jpg" alt="" class="img-responsive"></a>
                                <div class="property-listing-description">
                                    <p><a href="" class="listing-link"><b>The Property</b></a></p>
                                    <p>4 Privet Drive</p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-price">
                                <div class="property-listing-type">
                                    Residential
                                </div>
                                $1,000,000
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                    <li><i class="icon-toilet"></i> 2 Bath</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 featured-list-item">
                            <div class="property-image">
                                <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/house2.jpg" alt="" class="img-responsive"></a>
                                <div class="property-listing-description">
                                    <p><a href="" class="listing-link"><b>The Property</b></a></p>
                                    <p>4 Privet Drive</p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-price">
                                <div class="property-listing-type">
                                    Residential
                                </div>
                                $1,000,000
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                    <li><i class="icon-toilet"></i> 2 Bath</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 featured-list-item">
                            <div class="property-image">
                                <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive"></a>
                                <div class="property-listing-description">
                                    <p><a href="" class="listing-link"><b>The Property</b></a></p>
                                    <p>4 Privet Drive</p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-price">
                                <div class="property-listing-type">
                                    Residential
                                </div>
                                $1,000,000
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                    <li><i class="icon-toilet"></i> 2 Bath</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 featured-list-item">
                            <div class="property-image">
                                <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property2.jpg" alt="" class="img-responsive"></a>
                                <div class="property-listing-description">
                                    <p><a href="" class="listing-link"><b>The Property</b></a></p>
                                    <p>4 Privet Drive</p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="property-listing-price">
                                <div class="property-listing-type">
                                    Residential
                                </div>
                                $1,000,000
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-home"></i> 5.000 m</li>
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                    <li><i class="icon-toilet"></i> 2 Bath</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="nearby-listings-area">
                        <h2 class="other-listing-title">Nearby Listings
                            <span class="viewall-newlisting">
                                <a href="agent_haven/listings" target="_blank" data-original-title="" title="">View All</a>    
                            </span>
                        </h2>
                        <ul>
                            <li>
                                <div class="col-md-4 col-sm-4 no-padding-left">
                                    <div class="nearby-image">
                                        <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property16.jpg" alt="" class="img-responsive"></a>    
                                    </div>
                                </div>
                                <div class="nearby-description clearfix">
                                    <div class="col-md-8 col-sm-8">
                                        <h4><a href="">526 Flanders K</a></h4>
                                        <p>Delray Beach, FL 33484</p>
                                        <ul class="list-inline">
                                            <li><i class="fa fa-bed"></i> 2 Bed</li>
                                            <li><i class="icon-toilet"></i> 2 Bath</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-4 col-sm-4 no-padding-left">
                                    <div class="nearby-image">
                                        <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property17.jpg" alt="" class="img-responsive"></a>    
                                    </div>
                                </div>
                                <div class="nearby-description clearfix">
                                    <div class="col-md-8 col-sm-8">
                                        <h4><a href="">526 Flanders K</a></h4>
                                        <p>Delray Beach, FL 33484</p>
                                        <ul class="list-inline">
                                            <li><i class="fa fa-bed"></i> 2 Bed</li>
                                            <li><i class="icon-toilet"></i> 2 Bath</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-4 col-sm-4 no-padding-left">
                                    <div class="nearby-image">
                                        <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property18.jpg" alt="" class="img-responsive"></a>    
                                    </div>
                                </div>
                                <div class="nearby-description clearfix">
                                    <div class="col-md-8 col-sm-8">
                                        <h4><a href="">526 Flanders K</a></h4>
                                        <p>Delray Beach, FL 33484</p>
                                        <ul class="list-inline">
                                            <li><i class="fa fa-bed"></i> 2 Bed</li>
                                            <li><i class="icon-toilet"></i> 2 Bath</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-4 col-sm-4 no-padding-left">
                                    <div class="nearby-image">
                                        <a href="<?=base_url()?>demo/agent_haven/featured_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property19.jpg" alt="" class="img-responsive"></a>    
                                    </div>
                                </div>
                                <div class="nearby-description clearfix">
                                    <div class="col-md-8 col-sm-8">
                                        <h4><a href="">526 Flanders K</a></h4>
                                        <p>Delray Beach, FL 33484</p>
                                        <ul class="list-inline">
                                            <li><i class="fa fa-bed"></i> 2 Bed</li>
                                            <li><i class="icon-toilet"></i> 2 Bath</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="save-search-slider" style="margin-top:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="save-search-grid" class="owl-carousel owl-theme">
                  <div class="item" style="background: linear-gradient( rgba(125, 125, 125, 0.45), rgba(123, 130, 130, 0.63) ), url('<?=base_url()?>assets/images/dashboard/demo/agent-haven/property2.jpg');">
                      <div class="col-md-6 col-sm-5 col-xs-12 padding-0">
                          <div class="row">
                              <div class="col-md-12">
                                 
                                   <div class="save-search-desc">
                                      <h1 class="save-search-name">Beach</h1>
                                        <a href="agent_haven/listings" class="btn btn-view" target="_blank" >View All</a> 
                                   </div> 
                              </div>
                          </div>    
                      </div>
                      <div class="col-md-6 col-sm-7 col-xs-12 padding-0"> 
                          <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                            <div class="save-property-image" >
                                    <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property1.jpg" alt="" class="img-responsive" style="width:100%;"></a>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="save-property-price">
                                <p> Residential</p>
                                <p> $300,000</p>
                            </div>
                            <div class="save-property-title">
                                <p class="save-property-name">
                                   Lorem ipsum dolor
                                </p>
                                <p>
                                    Palm Beach Garden, FL 33410
                                </p>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                     <li><i class="icon-toilet"></i> 3 Bath</li>
                                </ul>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                            <div class="save-property-image" >
                                    <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property2.jpg" alt="" class="img-responsive" style="width:100%;"></a>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="save-property-price">
                                <p> Residential</p>
                                <p> $300,000</p>
                            </div>
                            <div class="save-property-title">
                                <p class="save-property-name">
                                   The Property
                                </p>
                                <p>
                                    Palm Beach Garden, FL 33410
                                </p>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                     <li><i class="icon-toilet"></i> 3 Bath</li>
                                </ul>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                            <div class="save-property-image" >
                                    <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property3.jpg" alt="" class="img-responsive" style="width:100%;"></a>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="save-property-price">
                                <p> Residential</p>
                                <p> $300,000</p>
                            </div>
                            <div class="save-property-title">
                                <p class="save-property-name">
                                   The Property
                                </p>
                                <p>
                                    Palm Beach Garden, FL 33410
                                </p>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                     <li><i class="icon-toilet"></i> 3 Bath</li>
                                </ul>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                            <div class="save-property-image" >
                                    <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property4.jpg" alt="" class="img-responsive" style="width:100%;"></a>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="save-property-price">
                                <p> Residential</p>
                                <p> $300,000</p>
                            </div>
                            <div class="save-property-title">
                                <p class="save-property-name">
                                   The Property
                                </p>
                                <p>
                                    Palm Beach Garden, FL 33410
                                </p>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                     <li><i class="icon-toilet"></i> 3 Bath</li>
                                </ul>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div class="item" style="background: linear-gradient( rgba(125, 125, 125, 0.45), rgba(123, 130, 130, 0.63) ), url('<?=base_url()?>assets/images/dashboard/demo/agent-haven/property2.jpg');">
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                          <div class="row">
                              <div class="col-md-12">
                                 
                                   <div class="save-search-desc">
                                      <h1 class="save-search-name"> West Palm Golf Homes</h1>
                                        <a href="agent_haven/listings" class="btn btn-view" target="_blank" >View All</a> 
                                   </div> 
                              </div>
                          </div>    
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-0"> 
                          <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                            <div class="save-property-image" >
                                    <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property5.png" alt="" class="img-responsive" style="width:100%;"></a>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="save-property-price">
                                <p> Residential</p>
                                <p> $300,000</p>
                            </div>
                            <div class="save-property-title">
                                <p class="save-property-name">
                                   The Property
                                </p>
                                <p>
                                    Palm Beach Garden, FL 33410
                                </p>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                     <li><i class="icon-toilet"></i> 3 Bath</li>
                                </ul>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                            <div class="save-property-image" >
                                    <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property9.png" alt="" class="img-responsive" style="width:100%;"></a>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="save-property-price">
                                <p> Residential</p>
                                <p> $300,000</p>
                            </div>
                            <div class="save-property-title">
                                <p class="save-property-name">
                                   The Property
                                </p>
                                <p>
                                    Palm Beach Garden, FL 33410
                                </p>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                     <li><i class="icon-toilet"></i> 3 Bath</li>
                                </ul>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                            <div class="save-property-image" >
                                    <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property7.png" alt="" class="img-responsive" style="width:100%;"></a>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="save-property-price">
                                <p> Residential</p>
                                <p> $300,000</p>
                            </div>
                            <div class="save-property-title">
                                <p class="save-property-name">
                                   The Property
                                </p>
                                <p>
                                    Palm Beach Garden, FL 33410
                                </p>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                     <li><i class="icon-toilet"></i> 3 Bath</li>
                                </ul>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                            <div class="save-property-image" >
                                    <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property8.png" alt="" class="img-responsive" style="width:100%;"></a>
                            </div>
                            <div class="property-listing-status">
                                    Active                                
                            </div>
                            <div class="save-property-price">
                                <p> Residential</p>
                                <p> $300,000</p>
                            </div>
                            <div class="save-property-title">
                                <p class="save-property-name">
                                   The Property
                                </p>
                                <p>
                                    Palm Beach Garden, FL 33410
                                </p>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <li><i class="fa fa-bed"></i> 2 Bed</li>
                                     <li><i class="icon-toilet"></i> 3 Bath</li>
                                </ul>
                            </div>
                          </div>
                      </div>
                  </div>
                </div>    
                </div>
                
            </div>
        </div>
    </div>

    <section class="agent-info-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 mobilepadding-0">
                    <div class="mortage-calculator">
                        <h3 class="text-center">CONTACT AGENT</h3>

                        <div class="question-mess" ></div>  <br/>    
                        <form action="" method="POST" class="customer_questions" id="customer_questions" >
                            <div class="form-group">
                                <input type="text" name="name" value="" class="form-control" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" value="" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="" value="" cols="30" rows="10" class="form-control" placeholder="Message" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-default btn-block submit-button submit-question-button" style="text-transform:uppercase;">Submit</button>
                        </form>

                    </div>
                </div>

                <div class="col-md-8 col-sm-12 nopadding-left">
                    <div class="col-md-3 col-sm-4 nopadding">
                        <div class="agent-image">
                            <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/john.jpg" class="img-thumbnail" width="300">
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8 nopadding">

                <!-- Get data from Featured Listings -->   

                    <h4 class="agent-name">
                        John Doe
                    </h4>
                    <div class="about-agent">
                        Pellentesque nisi tortor, congue eu consectetur ac, molestie in eros. Nullam ac suscipit est. Vestibulum pellentesque tincidunt magna non euismod. Maecenas suscipit nec augue vitae laoreet. Proin purus eros, tincidunt vel nulla quis, vulputate posuere ex.
                    </div>
                    <strong>
                        <ul class="agent-detail">
                            <li><i class="fa fa-phone"></i> Phone Number: 202-555-0156 </li>
                            <li><i class="fa fa-2x fa-mobile"></i> Mobile Number: 07700 900492  </li>
                            <li><i class="fa fa-map-marker"></i> Address: 888 Prospect St #200, La Jolla, CA 92037</li>
                            <li><i class="fa fa-envelope"></i> Email: johndoe@gmail.com</li>
                            <li><i class="fa fa-university"></i> Brokerage Name: Agent Squared</li>
                            <li><i class="fa fa-fax"></i> Fax Number: 202-555-0141</li>                                    
                        </ul>                     
                    </strong>
                     <div class="col-md-12 properties-sold">
                        <div class="sold-property-container">                                
                            <div class="other-listing-item">
                                <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                                <div class="sold-image">
                                        <a href="agent_haven/sold_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property10.jpg" alt="" class="img-responsive"></a>
                                </div>
                                <div class="sold-property-details">
                                    <p><a href="agent_haven/sold_property">The Property</a></p>
                                    <p><i class="fa fa-map-marker"></i>
                                       4 Private Drive
                                    </p>
                                    <p><i class="fa fa-usd"></i> 100,000</p>
                                </div>
                            </div>
                            <div class="other-listing-item">
                                <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                                <div class="sold-image">
                                        <a href="agent_haven/sold_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property11.jpg" alt="" class="img-responsive"></a>
                                </div>
                                <div class="sold-property-details">
                                    <p><a href="agent_haven/sold_property">The Property</a></p>
                                    <p><i class="fa fa-map-marker"></i>
                                       4 Private Drive
                                    </p>
                                    <p><i class="fa fa-usd"></i> 100,000</p>
                                </div>
                            </div>
                            <div class="other-listing-item">
                                <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                                <div class="sold-image">
                                        <a href="agent_haven/sold_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property12.jpg" alt="" class="img-responsive"></a>
                                </div>
                                <div class="sold-property-details">
                                    <p><a href="agent_haven/sold_property">The Property</a></p>
                                    <p><i class="fa fa-map-marker"></i>
                                       4 Private Drive
                                    </p>
                                    <p><i class="fa fa-usd"></i> 100,000</p>
                                </div>
                            </div>
                            <div class="other-listing-item">
                                <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                                <div class="sold-image">
                                        <a href="agent_haven/sold_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property13.jpg" alt="" class="img-responsive"></a>
                                </div>
                                <div class="sold-property-details">
                                    <p><a href="agent_haven/sold_property">The Property</a></p>
                                    <p><i class="fa fa-map-marker"></i>
                                       4 Private Drive
                                    </p>
                                    <p><i class="fa fa-usd"></i> 100,000</p>
                                </div>
                            </div>
                            <div class="other-listing-item">
                                <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                                <div class="sold-image">
                                        <a href="agent_haven/sold_property"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property14.jpg" alt="" class="img-responsive"></a>
                                </div>
                                <div class="sold-property-details">
                                    <p><a href="agent_haven/sold_property">The Property</a></p>
                                    <p><i class="fa fa-map-marker"></i>
                                       4 Private Drive
                                    </p>
                                    <p><i class="fa fa-usd"></i> 100,000</p>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('/session/footer'); ?>