<?php
    $this->load->view($theme.'/session/header-page');
?>

    <section class="page-content" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="customer-dashboard">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#properties" aria-controls="home" role="tab" data-toggle="tab" class="fa fa-heart"> <span>Favorites</span></a></li>
                            <li role="presentation"><a href="#searches" aria-controls="home" role="tab" data-toggle="tab" class="fa fa-search"> <span>Searches</span></a></li>
                            <li role="presentation"><a href="#settings" aria-controls="home" role="tab" data-toggle="tab" class="fa fa-gear"> <span>Profile / Email</span></a></li>
                        </ul>

                          <!-- Tab panes -->
                        <div class="tab-content" style="margin-top:20px">
                            <div role="tabpanel" class="tab-pane active" id="properties">
                                <h3>My Saved Favorites ( <?php echo count($save_properties)?> )</h3>
                                <br>    
                                <table class="table table-hover table-responsive">
                                    <thead>
                                        <tr>
                                            <td width="10%">Photo</td>
                                            <td>Activity</td>
                                            <td>Date Saved</td>
                                            <td>Address</td>
                                            <td>Details</td>
                                            <!-- <td>Ratings</td> -->
                                            <td align="center">Delete</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if( !empty($save_properties) ) : 
                                            foreach ($save_properties as $property) : ?>  
                                                <tr id="<?=$property->cpid?>" >
                                                    <!-- <td> <img src="<?php echo base_url()?>assets/img/home/property1.jpg" width="100"> </td> -->
                                                    <td> <a href="<?php echo site_url("home/other_property?listingId=".$property->property_id); ?>" target="_blank"> <img src="<?php echo $property->property_photos[0]["Uri1024"]?>" width="100" height="70"> </a></td>
                                                    <td><?php if($property->properties_details["StandardFields"]["MlsStatus"] == "New") : ?><span class="badge">New</span><?php endif;?> Listings </td>
                                                    <td><?php echo date("m-d-Y", strtotime($property->date_created))?></td>
                                                    <td><?php echo $property->properties_details["StandardFields"]["UnparsedAddress"]?></td>
                                                    <td>$<?php echo number_format($property->properties_details["StandardFields"]["CurrentPrice"]);?> <?php echo str_replace($property->properties_details["StandardFields"]["UnparsedFirstLineAddress"].',', "", $property->properties_details["StandardFields"]["UnparsedAddress"])?></td>
                                                    <!-- <td><span class="glyphicon glyphicon-star" aria-hidden="true"></span> </td> -->
                                                    <td align="center"><a href="<?php echo site_url('home/customer/delete_customer_property/'); ?>" data-id="<?=$property->cpid?>" data-property-id="<?=$property->property_id?>" class="delete-save-property" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                                                </tr>
                                             <?php endforeach; ?>
                                        <?php else : ?>
                                            <tr class="center"><td colspan="10"><i>no save propertes records!</i></td></tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="searches">
                                <h3>My Saved Searches ( <?php echo count($save_searches); ?> )</h3>
                                <br>
                                <table class="table table-hover table-responsive">
                                    <thead>
                                        <tr>
                                            <td width="20%">Searched Links</td>
                                            <td>Date Saved</td>
                                            <td>Full Details</td>
                                            <!-- <td>Email Settings</td> -->
                                            <td align="center">Delete</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if( !empty($save_searches) ) : 
                                            foreach ($save_searches as $searches) : ?>  
                                                <?php if( !isset($searches->json_decoded) ) { ?>
                                                  <tr>
                                                    <td> <a href="<?php echo base_url()."home/listings?".$searches->url;?>" target="_blank">Run this search >></a> </td>
                                                    <td><?=substr($searches->url,0,50);?>...</td>
                                                    <td><?= date("Y-m-d", strtotime($searches->date_created)); ?></td>
                                                    <td  align="center"><a href="<?php echo site_url('home/customer/delete_customer_search/'); ?>" data-id="<?=$searches->csid?>" class="delete-save-searches" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                                                  </tr>
                                                <?php }else{ ?>
                                                  <tr>
                                                    <td> <a href="<?php echo base_url()."home/popular_searches/".$searches->json_decoded->Id;?>" target="_blank">Run this search >></a> </td>
                                                    <td><?=substr($searches->json_decoded->Name,0,60);?></td>
                                                    <td><?= date("Y-m-d", strtotime($searches->date_modified)); ?></td>
                                                    <td  align="center"></td>
                                                  </tr>
                                                <?php } ?>
                                                <!-- <tr id="search_<?=$searches->csid?>">
                                                    <td> <a href="<?php echo base_url()."listings?".$searches->url;?>" target="_blank">Run this search >></a> </td>
                                                    <td><?php echo date("m-d-Y", strtotime($searches->date_created))?></td>
                                                    <td><a href="javascript:void(0)" ><?php echo substr($searches->url,0,60); ?>....</a></td>
                                                    <td  align="center"><a href="<?php echo site_url('home/customer/delete_customer_search/'); ?>" data-id="<?=$searches->csid?>" class="delete-save-searches" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                                                </tr> -->
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <tr class="center"><td colspan="10"><i>no save searches records!</i></td></tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="settings">
                                <h3>My Profile </h3>
                                Account/Email :  <?= (isset($profile->email)) ? $profile->email : $this->session->userdata("customer_email"); ; ?> (<a href="#" class="" id="customer-change-password">change password</a>)
                                <a href="<?php echo site_url("home/customer/edit_profile"); ?>" type="button" id="customer-edit-profile"  class="btn btn-edit"> <span class="fa fa-edit" aria-hidden="true"> <span aria-hidden="true">Edit Profile</span></a>
                                <br />
                                <br />
                                <!-- <h3>Alerts And Email Preferences </h3>
                                <div class="alert alert-default alert-dismissible" role="alert">
                                    Saved Listings & Search Updates <input name="saved_search_updates" class="updates-email-settings" value="real_time"  type="radio" <?php echo (isset( $profile->email_settings ) and $profile->email_settings == "real_time") ? "checked" : "" ?>> Real Time <input name="saved_search_updates" class="updates-email-settings" value="daily" type="radio" <?php echo (isset( $profile->email_settings ) and $profile->email_settings == "daily") ? "checked" : "" ?>> Daily <input name="saved_search_updates" class="updates-email-settings" value="off" type="radio" <?php echo (isset( $profile->email_settings ) and $profile->email_settings == "off") ? "checked" : "" ?>> Off  
                                </div> -->
                                <br>                                
                                <h3>Send Email</h3>
                                <br>
                                <table class="table table-hover table-responsive">
                                    <thead>
                                        <tr>
                                            <td width="50%">Message</td>
                                            <td>Date</td>
                                            <td>Sen By</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if( !empty($send_email)) : ?>
                                          <?php foreach( $send_email as $message ) : ?>
                                            <tr>
                                              <td> <a href="javascript:void(0)" data-message="<?php echo $message->message; ?>" class="email-view" title="view all"> <?php echo substr($message->message ,0,60); ?> </a></td>
                                              <td> <?= date( "Y-m-d" ,strtotime($message->created));?></td>
                                              <td> <?= ($message->type == 1) ? $message->agent_name : $message->contact_name; ?></td>
                                            </tr>
                                          <?php endforeach; ?>

                                        <?php else : ?>
                                          <tr class="center"> <td colspan="8"> <i> No records found!</i> </td> </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>                                
                                <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#SendEmail" >Send Email</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<div class="modal fade bs-example-modal-lg" id="SentEmailView" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Sent Email Content</h4>
      </div>
      <div class="modal-body">
        <textarea name="message" class="form-control" rows="15" id="sent-email-content"></textarea>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bs-example-modal-lg" id="SendEmail" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Send Email</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url("home/customer/user_send_email"); ?>" method="POST" id="sbt-send-email" class="form-horizontal">
            <textarea name="message" class="form-control" rows="15"></textarea>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-send" >Send</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="change-pass-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Change Password</h4>
          </div>
          <div class="modal-body">
                <div class="show-mess" ></div><br/>
                <form action="<?php echo site_url('customer/change_password'); ?>" method="post" class="customer-change-pass" id="customer-change-pass" >
                    <div class="form-group">
                        <label for="exampleInputEmail1">New Password</label>
                        <input type="password" name="new_password" class="form-control" id="new-password" placeholder="New Password">
                    </div> 

                    <div class="form-group">
                        <label for="exampleInputEmail1">Confirm Password</label>
                        <input type="password" name="cpassword" class="form-control" id="new-password" placeholder="Confirm Password">
                    </div>                     
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-default submit-button">Submit</button>
          </div>
           </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="customer-profile-modal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Profile</h4>
          </div>
          <div class="modal-body">
                <div class="show-mess" ></div><br/>
                <?php $profile_url = (isset($profile->id)) ? "home/customer/update_profile/".$profile->id : "home/customer/update_profile"; ?>
                <form action="<?php echo base_url().$profile_url; ?>" method="post" class="customer-profile" id="customer-profile" >
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" name="profile[first_name]" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" class="form-control" id="fname" placeholder="First Name" required >
                    </div> 

                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="text" name="profile[last_name]" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" class="form-control" id="lname" placeholder="Last Name" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Address</label>
                        <input type="email" name="profile[email]" value="<?= (isset($profile->email)) ? $profile->email : $this->session->userdata("customer_email"); ; ?>" class="form-control" id="email" placeholder="Email Address" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone</label>
                        <input type="text" name="profile[phone]" value="<?= (isset($profile->phone)) ? $profile->phone : "" ; ?>" class="form-control" id="phone" placeholder="Phone Number" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Mobile</label>
                        <input type="text" name="profile[mobile]" value="<?= (isset($profile->mobile)) ? $profile->mobile : "" ; ?>" class="form-control" id="mobile" placeholder="Mobile Number" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">State</label>
                        <input type="text" name="profile[state]" value="<?= (isset($profile->state)) ? $profile->state : "" ; ?>" class="form-control" id="state" placeholder="State" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">City</label>
                        <input type="text" name="profile[city]" value="<?= (isset($profile->city)) ? $profile->city : "" ; ?>" class="form-control" id="city" placeholder="City" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input type="text" name="profile[address]" value="<?= (isset($profile->address)) ? $profile->address : "" ; ?>" class="form-control" id="address" placeholder="Address" required>
                    </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-default submit-button edit-profile-btn" data-loading-text="Loading...">Submit</button>
          </div>
           </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

    <?php
        $this->load->view($theme.'/session/footer');
    ?>

<script type="text/javascript">
    
    var sbt_new_password = function ( e )
    {   
        e.preventDefault();

        var url = $(this).attr("action");
        
        $.ajax({
            url : url,
            type : "post",
            data : $(this).serialize(),
            dataType: "json",
            success: function( data){
                console.log(data);
                if(data.success)
                {
                    $(".show-mess").html(data.message);
                }
                else
                {
                    $(".show-mess").html(data.errors);
                }
            }
        });
    };

    var delete_save_property = function (e)
    {
        e.preventDefault();
        var $this = $(this);
        var link = $this.attr("href");
        var property_id = $this.data("id");
        var mls_id = $this.data("property-id");
        var action = link+"/"+property_id+"/"+mls_id;
        //alert(property_id)
        $.msgBox({
            title: "Are You Sure",
            content: "Would you like to remove this property?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "Cancel"}],
            success: function (result) {
                if (result == "Yes") {
                    $.ajax({
                        url : action,
                        type : "post",
                        dataType: "json",
                        beforeSend: function(){
                            $this.text("Loading...");
                        },
                        success: function( data){
                            console.log(data);
                            if(data.success)
                            {
                                $( "#"+property_id ).fadeOut( "slow", function() {
                                    $.msgBox({
                                        title:"Success",
                                        content:"Property has been removed!",
                                        type:"info"
                                    });
                                 });
                            }
                            else
                            {
                                $.msgBox({
                                    title: "Error",
                                    content: "Failed to Delete property!",
                                    type: "error",
                                    buttons: [{ value: "Ok" }]                                
                                });
                            }
                        }
                    });
                }
            }
        });

    };

    var delete_save_searches = function (e)
    {
        e.preventDefault();
        var $this = $(this);
        var link = $this.attr("href");
        var property_id = $this.data("id");
        var action = link+"/"+property_id;
        //alert(property_id)
        $.msgBox({
            title: "Are You Sure",
            content: "Would you like to remove this property search?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "Cancel"}],
            success: function (result) {
                if (result == "Yes") {
                    $.ajax({
                        url : action,
                        type : "post",
                        dataType: "json",
                        beforeSend: function(){
                            $this.text("Loading...");
                        },
                        success: function( data){
                            console.log(data);
                            if(data.success)
                            {
                                $( "#search_"+property_id ).fadeOut( "slow", function() {
                                    $.msgBox({
                                        title:"Success",
                                        content:"Record has been removed!",
                                        type:"info"
                                    });
                                 });
                            }
                            else
                            {
                                $.msgBox({
                                    title: "Error",
                                    content: "Failed to Delete record!",
                                    type: "error",
                                    buttons: [{ value: "Ok" }]                                
                                });
                            }
                        }
                    });
                }
            }
        });

    }

    var change_password_modal = function( e ){
        e.preventDefault();
        
        $('#change-pass-modal').modal('show');

        $("#customer-change-pass").on("submit", sbt_new_password);
    };

    var sbt_edit_profile = function ( e )
    {   
        e.preventDefault();

        var url = $(this).attr("action");
        $(".edit-profile-btn").prop('disabled',true);
        $(".edit-profile-btn").text('Loading..')
        $.ajax({
            url : url,
            type : "post",
            data : $(this).serialize(),
            dataType: "json",
            success: function( data){
                console.log(data);
                
                if(data.success)
                {
                    //$(".show-mess").html(data.message);
                    $.msgBox({
                        title:"Success",
                        content: data.message,
                        type:"info"
                    });

                    location.reload();
                }
                else
                {
                    //$(".show-mess").html(data.errors);
                    $.msgBox({
                        title: "Error",
                        content: data.message,
                        type: "error",
                        buttons: [{ value: "Ok" }]                                
                    });
                }
                $(".edit-profile-btn").prop('disabled',false);
                $(".edit-profile-btn").text('Submit')
            }
        });
    };

    var edit_profile = function( e )
    {
        e.preventDefault();

        $('#customer-profile-modal').modal('show');
        $("#customer-profile").on("submit", sbt_edit_profile);
    };

    var update_email_settings = function(e)
    {
        var val = $(this).val();
        var url = base_url+"home/customer/update_email_preferences_settings";
        $.ajax({
            url : url,
            type : "post",
            data : ( {"settings" : val }),
            dataType: "json",
            success: function( data){
                console.log(data);
                if(data.success)
                {
                    $.msgBox({
                        title:"Success",
                        content: "Alerts and Email Preferences has been successfully updated",
                        type:"info",
                        buttons: [{ value: "Ok" }]
                    });
                }
                else
                {
                    $.msgBox({
                        title: "Error",
                        content: "Failed to update Alerts and Email Preferences!",
                        type: "error",
                        buttons: [{ value: "Ok" }]                                
                    });
                }
            }
        });

    }

    $("#customer-change-password").on("click", change_password_modal);
    $(".delete-save-property").on("click", delete_save_property);
    $(".delete-save-searches").on("click", delete_save_searches);

    $("#customer-edit-profile").on( "click", edit_profile);
    $(".updates-email-settings").on("click", update_email_settings);
</script>