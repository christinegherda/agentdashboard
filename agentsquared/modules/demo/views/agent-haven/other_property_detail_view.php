    <?php
        $this->load->view($theme.'/session/header');
    ?>

    <section class="property-detail-header">
        <!-- Display Cover Photo -->
        <?php 
            if($photos) {
        ?>
            <img src="<?= $photos[0]['Uri1024'] ?>" alt="<?php echo $objStandard['UnparsedFirstLineAddress']; ?>" class="img-responsive">

        <?php } else { ?>

        <img src="<?=base_url()."assets/img/home/no-image-listing.jpg"?>" alt="<?php echo $objStandard['UnparsedFirstLineAddress']; ?>" class="img-responsive">

        <?php } ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="property-view">
                        <div class="property-header-text">
                            <h2><?php echo $objStandard['UnparsedFirstLineAddress']; ?></h2>
                            <p><?php echo $objStandard['City']; ?>, <?php echo $objStandard['StateOrProvince']; ?> <?php echo $objStandard['PostalCode']; ?></p>
                            <h2 class="property-header-price"><?php echo '$ '.number_format($objStandard['CurrentPrice']); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="property-detail-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="filter-tab">
                        <div class="property-agent-info">
                            <h4 class="text-center">Agent</h4>
                            <hr>

                            <div class="agent-image">
                                    <?php if( isset($branding->agent_photo) AND !empty($branding->agent_photo)) { ?>
                                        <img src="<?php AGENT_DASHBOARD_URL ?>assets/upload/photo/<?php echo $branding->agent_photo;?>" class="img-thumbnail" width="400">
                                    <?php } else { ?>

                                         <?php if (isset($my_account["Images"][0]["Uri"]) AND !empty($my_account["Images"][0]["Uri"])){?>
                                            <img src="<?=$my_account["Images"][0]["Uri"]?>" class="img-thumbnail" width="400"> 
                                         <?php } else { ?>
                                         <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/no-profile-img.gif" alt="No Profile Image">
                                         <?php } ?>  

                                    <?php } ?>  
                            </div>
                             <h4 class="agent-name">
                                    <?php echo $my_account['Name']?>
                            </h4>
                          <p>

                               <?php
                                //Office Phone
                                 if(isset($branding->phone) AND !empty($branding->phone)){?>

                                    <i class="fa fa-phone-square"></i> Office: <?= $branding->phone; ?><br/>

                                 <?php }else{?>

                                  <?php   if(isset($my_account['Phones'] )) {
                                    foreach($my_account['Phones'] as $phone) {
                                        if($phone['Type'] == 'Office'){
                                            echo "<i class='fa fa-phone-square'></i> ".$phone['Type']." : ".$phone['Number'].'<br>';
                                        }
                                        
                                    }
                                }

                                 }
                                ?>


                                <?php
                                    //Mobile phone
                                 if(isset($branding->mobile) AND !empty($branding->mobile)){?>

                                    <i class="fa fa-2x fa-mobile"></i> Mobile: <?= $branding->mobile; ?><br/>

                                 <?php }else{?>

                                  <?php   
                                      if(isset($my_account['Phones'] )) {
                                        foreach($my_account['Phones'] as $phone) {
                                            if($phone['Type'] == 'Mobile'){
                                                echo "<i class='fa fa-2x fa-mobile'></i> ".$phone['Type']." : ".$phone['Number'].'<br>';
                                            }
                                            
                                        }
                                    }
                                 }
                                ?>
                                

                                <?php
                                    //FAX
                                   if(isset($my_account['Phones'] )) {
                                    foreach($my_account['Phones'] as $phone) {
                                        if($phone['Type'] == 'Fax'){
                                            echo "<i class='fa fa-fax'></i> ".$phone['Type']." : ".$phone['Number'].'<br>';
                                        }
                                        
                                    }
                                }?>
                               
                           </p>
                            <p>
                                <?php
                                if(isset($my_account['Emails'] )) {
                                    foreach($my_account['Emails'] as $email) {
                                ?>
                                <i class="fa fa-envelope"></i>: <a href="mailto:<?php echo $email['Address'];?>">Email Me</a>
                                <?php
                                    }
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                    <!-- <div class="filter-tab">
                        <?php  $property_saved = FALSE; if( isset( $_SESSION["save_properties"] ) ) : ?>
                            <?php foreach( $_SESSION["save_properties"] as $key ) : ?>
                                <?php if( $key['property_id'] == $property_id ) : $property_saved = TRUE; break; endif; ?>
                            <?php endforeach; ?>
                        <?php else : $property_saved = FALSE; endif;?>
                        <h4 class="text-center">
                            <a href="<?php echo site_url('home/home/save_favorates_properties/'.$property_id); ?>" data-pro-id="<?=$property_id?>" class="save-favorate" title="Save Property">
                                <span id="isSaveFavorate_<?=$property_id?>" >
                                <?php echo ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>" ;?> 
                                </span> Add to Favorites
                            </a>
                        </h4>    
                    </div> -->
                    <div class="filter-tab">
                        <h4 class="text-center">Share this property to:</h4>
                        <hr>
                        <ul class="list-inline property-social-share">
                            <li><span class='st_facebook_large' displayText='Facebook'></span></li>
                            <li><span class='st_twitter_large' displayText='Tweet'></span></li>
                            <li><span class='st_linkedin_large' displayText='LinkedIn'></span></li>
                            <li><span class='st_googleplus_large' displayText='Google +'></span></li>
                            <li><span class='st_email_large' displayText='Email'></span></li>
                        </ul>
                    </div>
                    <div class="filter-tab">
                        <div class="property-submit-question">
                            <h4 class="text-center">Have a question?</h4>
                            <hr>
                            <div class="question-mess" ></div>  <br/>                          
                            <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" name="name" value="" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" name="email" value="" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Message</label>
                                    <textarea name="message" id="" value="" cols="30" rows="5" class="form-control" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="filter-tab">
                        <div class="property-submit-question">
                            <h4 class="text-center">Mortgage Calculator</h4>
                            <hr>
                            <form  method="post" id="mortgageCalculator">
                                <div class="form-group">
                                    <label for="sale_price">Price of Home ($<span id="sale_price_label"></span>)</label>
                                    <input type="text" name="sale_price" id="sale_price" value="<?php echo $this->session->flashdata('mortgage_price_value'); ?>" class="form-control">
                                    <input type="hidden" name="calculator_url" value="<?php echo base_url();?>home/mortgage_calculator">
                                </div>
                                <div class="form-group">
                                    <label for="down_percent">Downpayment (<span id="down_percent_label"></span>%)</label>
                                    <input type="text" name="down_percent" id="down_percent" value="<?php echo $this->session->flashdata('mortgage_downpayment_value'); ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="mortgage_interest_percent">Annual Interest Rate (<span id="mortgage_interest_percent_label"></span>%)</label>
                                    <input type="text" name="mortgage_interest_percent" id="mortgage_interest_percent" value="<?php echo $this->session->flashdata('mortgage_interest_rate_value'); ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="year_term">Loan Term (<span id="year_term_label"></span>  years)</label>
                                    <input type="text" name="year_term" id="year_term" value="<?php echo $this->session->flashdata('mortgage_loan_terms_value'); ?>" class="form-control">
                                </div>
                                <button class="btn btn-default btn-block submit-button" id="morg_calc_btn">Calculate <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i></button>
                                <div id="mortgage-result">
                                       <?php echo $this->session->flashdata('mortgage_monthly'); ?>
                                       <!-- <?php echo $this->session->flashdata('mortgage_monthly_interest'); ?>
                                       <?php echo $this->session->flashdata('mortgage_annual_interest'); ?>
                                       <?php echo $this->session->flashdata('mortgage_downpayment'); ?>
                                       <?php echo $this->session->flashdata('mortgage_amount_financed'); ?> -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-9 col-sm-8">
                    <div class="property-details">
                        <div class="row">
                            <div class="col-md-9 col-sm-7 col-xs-12">
                                <h5 class="property-title"><?php echo $objStandard['UnparsedFirstLineAddress']; ?> <span class="get-directions"><a class="bottom" title="" data-placement="bottom" data-toggle="tooltip" href="https://www.google.com/maps/dir//<?php echo urlencode($objStandard['UnparsedAddress']); ?>" data-original-title="Get Directions" target="_blank"><img src="<?=base_url()."assets/img/home/get-direction.png"?>" width="15px" height="15px" alt="Google Maps Direction"></a></span></h5>
                                <h5 class="property-address"><?php echo $objStandard['City']; ?>, <?php echo $objStandard['StateOrProvince']; ?> <?php echo $objStandard['PostalCode']; ?> </h5>
                                <h5 class="property-type"><?php echo $objStandard['PropertyClass']; ?></h5>
                                <h5 class="property-price"><?php echo '$ '.number_format($objStandard['CurrentPrice']); ?></h5>
                            </div>
                            <div class="col-md-3 col-sm-5 col-xs-12 text-right">

                                <?php if(strpos($mlsLogo, 'http') !== false) { ?>
                                    <p><img src="<?php echo $mlsLogo; ?>" alt="IDX Logo" style="width:50%; margin-left:88px; display:block;" class="mlsLogo"/></p>
                                <?php } else { ?>
                                    <h4><?php echo $mlsName; ?></h4>
                                <?php } ?>

                                <?php if(isset($objStandard['ListingId']) AND strpos($objStandard['ListingId'],'*') === false){?>
                                    <p style="margin: 0 0 5px;"><strong>MLS&reg; #:</strong> <small><?php echo $objStandard['ListingId']; ?></small></p>
                                <?php }?>

                               <?php if(isset($objStandard['ListOfficeName']) AND strpos($objStandard['ListOfficeName'],'*') === false ){?>
                                    <p><strong>Courtesy of:</strong> <small><?php echo $objStandard['ListOfficeName']; ?></small></p>
                                <?php }?>
                                <ul class="listing-icons">
                                    <li>
                                     <?php if(isset($virtual_tours) AND !empty($virtual_tours)){?>
                                        <p class="virtualTour"><a href="#tourModal" data-toggle="modal" data-title="Tooltip" data-trigger="hover" title="Virtual Tour"><i class="fa fa-2x fa-file-video-o"></i></a></p>
                                        <p><small>Tour</small></p>
                                    <?php } ?>
                                        
                                    </li>
                                    <li>
                                        <?php  $property_saved = FALSE; if( isset( $_SESSION["save_properties"] ) ) : ?>
                                            <?php foreach( $_SESSION["save_properties"] as $key ) : ?>
                                                <?php if( $key['property_id'] == $property_id ) : $property_saved = TRUE; break; endif; ?>
                                            <?php endforeach; ?>
                                        <?php else : $property_saved = FALSE; endif;?>
                                        <p>
                                            <a href="<?php echo site_url('home/home/save_favorates_properties/'.$property_id); ?>" data-pro-id="<?=$property_id?>" class="save-favorate" data-title="Tooltip" data-trigger="hover" title="<?php echo ($property_saved) ? "Saved Property" : "Save Property" ;?>">
                                                <span id="isSaveFavorate_<?=$property_id?>"><?php echo ($property_saved) ? "<i class='fa fa-2x fa-heart'></i>" : "<i class='fa fa-2x fa-heart-o'></i>" ;?></span>
                                            </a>
                                        </p>
                                        <p id="isSavedFavorate_<?=$property_id?>"><?php echo ($property_saved) ? "<small>Saved</small>" : "<small>Save</small>" ;?></p>
                                    </li>
                                     <li>
                                        <?php if(isset($_SESSION["customer_id"])) :
                                            $href =  "<a href='#schedModal' data-toggle='modal' data-title='Tooltip' data-trigger='hover' title='Schedule for Showing'><i class='fa fa-2x fa-calendar'></i></a>";
                                        else: 
                                            $href = "<a href='".base_url("login")."' data-title='Tooltip' data-trigger='hover' title='Schedule for Showing' ><i class='fa fa-2x fa-calendar'></i></a>";
                                        endif;?>
                                        <p><?php echo $href; ?></p>
                                        <p><small>Showing</small></p>
                                    </li>
                                </ul>
                            </div>

                            <!-- Virtual Tour Modal -->
                                <div id="tourModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-virtual-tour">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close close-tour" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                            </div>
                                             <div class="virtual-tours-modal text-center">
                                                 <?php if (isset($virtual_tours) AND !empty($virtual_tours)) { 

                                                    foreach($virtual_tours as $tour) { 
                                                    
                                                        $virtual_tour = $tour['Uri'];
                                                        $findDropbox   = 'dropbox';
                                                        $virtualTourdropbox = strpos($virtual_tour, $findDropbox);
                                                   ?>

                                                        <!-- For the Dropbox Link for Virtual Tour -->   
                                                        <?php
                                                        if ($virtualTourdropbox !== false) { ?>
                                                                <div class="virtual-dropbox">
                                                                    <h2 class="alert alert-info"> Virtual Tour preview is not available!</h2><br>
                                                                    <p>Please click the link below for the preview</p>
                                                                    <p class="virtual-drop-link"><strong><a href="<?php echo $tour['Uri']; ?>" target="_blank">Virtual Tour</a></strong></p> 
                                                                </div> 

                                                        <?php } else { ?>  
                                                                
                                                                 <h4> <?php echo $tour['Name']; ?></h4>

                                                            <?php 
                                                                    // For the Youtube video to work in IFRAME
                                                                $string     = $tour['Uri'];
                                                                $search     = '#(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*#x';
                                                                $replace    = 'http://www.youtube.com/embed/$2';
                                                                $url        = preg_replace($search,$replace,$string);
                                                            ?>

                                                                 <?php
                                                                     //Identify if there is a string "youtu.be"
                                                                    $mystring = $tour['Uri'];
                                                                    $findme   = 'youtube';
                                                                    $pos = strstr($mystring, $findme);

                                                                    if ($pos !== false) {?>

                                                                        <iframe class="virtualTours-iframe" width="100%" height="750px"  data-src="<?php echo $url;?>" src="about:blank" frameborder="0"></iframe>

                                                                       
                                                                    <?php } else { ?>

                                                                             <iframe class="virtualTours-iframe" width="100%" height="750px" data-src="<?php echo $tour['Uri']; ?>" src="about:blank" frameborder="0"></iframe>
                                                                       
                                                                   <?php } ?>


                                                            <?php } ?>

                                                    <?php } ?>

                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Schedule for Showing Modal -->
                                <div id="schedModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-virtual-tour">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                <h4>Schedule For Showing</h4>
                                            </div>
                                            <div class="description sched-modal-section">
                                                    <div id="resultInsert" class="alert alert-success" style="display:none"></div>

                                                    <form action="<?php echo base_url("home/home/save_schedule_for_showing"); ?>" method="POST" class="sched_showing" >
                                                        <input type="hidden" name="property_id" value="<?php echo $property_id?>" />
                                                        <input type="hidden" name="property_name" value="<?php echo $objStandard['UnparsedFirstLineAddress']; ?>" />
                                                        <div class="row">
                                                            <div class="col-md-6 col-xs-6">
                                                                <label>Date schedule</label><input type="text" data-provide="datepicker" name="date_scheduled" value="" class="form-control date_schedule" id="startdatepickermodal" placeholder="Date schedule" /><br />
                                                            </div>
                                                        </div>
                                                        <textarea name="sched_for_showing" rows="10" class="form-control"> Checkout listings at <?php echo $objStandard['UnparsedFirstLineAddress'];?></textarea>
                                                      <div class="modal-footer">
                                                        <button type="submit" class="btn btn-default submit-button" id="sbt-showing" >Submit</button>
                                                      </div>
                                                        

                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="view-full-details active" role="presentation" class="active"><a href="#property-detail" aria-controls="home" role="tab" data-toggle="tab">Property Details</a></li>
                            <li role="presentation"><a href="#location" role="tab" data-toggle="tab">Location</a></li>
                            <li class="view-full-spec" role="presentation"><a href="#fullspec" role="tab" data-toggle="tab">Full Specifications</a></li>
                            <li role="presentation"><a href="#school" role="tab" data-toggle="tab">Nearby</a></li>
                            <!-- <?php if(isset($_SESSION["customer_id"])) :
                                $href =  "<a href='#schedforshow' role='tab' data-toggle='tab'>Schedule For Showing</a>";
                            else: 
                                $href = "<a href='".base_url("login")."' >Schedule For Showing</a>";
                            endif;?>
                                <li role="presentation"><?php echo $href; ?></li> -->
                        </ul>

                        

                        <div class="tab-content">

                           <!--  <div role="tabpanel" class="tab-pane" id="schedforshow">
                                <div class="description property-detail-sections">
                                    <h4>Schedule For Showing</h4>
                                    <div id="resultInsert" class="label label-success" style="display:none"></div>

                                    <form action="<?php echo base_url("home/home/save_schedule_for_showing"); ?>" method="POST" class="sched_showing" >
                                        <input type="hidden" name="property_id" value="<?php echo $property_id?>" />
                                        <input type="hidden" name="property_name" value="<?php echo $objStandard['UnparsedFirstLineAddress']; ?>" />
                                        <label>Date schedule</label><input type="text" data-provide="datepicker" name="date_scheduled" value="" class="form-control date_schedule" id="startdatepicker" style="width: 350px;" placeholder="date schedule" /><br />
                                        <textarea name="sched_for_showing" row="60" style="width: 850px; height: 200px;"> Checkout listings at <?php echo $objStandard['UnparsedFirstLineAddress'];?></textarea>
                                        <button type="submit" class="btn btn-info pull-right" id="sbt-showing" >Submit</button>
                                    </form>
                                </div>

                                <br /><br /><br />
                            </div> -->

                            <div role="tabpanel" class="tab-pane fade in active" id="property-detail">
                                <div class="description property-detail-sections">
                                    <h4>Description</h4>
                                    <p>
                                        <?php echo $objStandard['PublicRemarks']; ?>
                                    </p>
                                </div>

                                <div class="photo-gallery property-detail-sections">
                                    <?php 
                                        if(!empty($photos)) :
                                            for($i = 0; $i < count($photos); $i++) { ?>    
                                                <div class="col-md-2 col-xs-4 col-sm-4">
                                                    <div class="photo-gallery-image">
                                                        <a href="<?php echo $photos[$i]['Uri640']; ?>" data-gallery="">
                                                            <img src="<?php echo $photos[$i]['Uri640']; ?>" alt="" class="img-responsive img-thumbnail"> 
                                                            <?php if($i == 7) { ?> 
                                                                    <div class="see-all">
                                                                        <p>See all <?php echo count($photos); ?> photos</p>
                                                                    </div>    
                                                            <?php } ?>
                                                        </a>    
                                                    </div>
                                                </div>
                                    <?php   
                                            }
                                        endif;
                                    ?>
                                </div>
                                <div class="specifications property-detail-sections">
                                    <h4>Specifications</h4>
                                    <div class="col-md-4 col-sm-12">
                                        
                                        <ul class="property-detail-list">
                                            <?php 
                                                $count = 0;
                                                foreach($ar2 as $GroupedFields) {
                                                    if($count < 1) {
                                            ?>
                                                   <br/><h4 class='panel-title'><strong><?= $GroupedFields[0]; ?></strong></h4><br/>
                                                        <?php
                                                            $i= 0;
                                                        ?> 
                                                        <?php foreach($GroupedFields[1] as $gf) {
                                                            if($i++ >= 5) break;
                                                                if($gf[1]['Domain'] == "CustomFields") {
                                                                    foreach($objCustom[0]['Main'] as $main) {
                                                                        foreach($main as $kMain=>$vMain) {
                                                                            if($GroupedFields[0] == $kMain) {
                                                                                foreach($vMain as $vmKey => $vmVal) {
                                                                                    if(isset($vmVal[$gf[6]['Label']])) { ?>
                                                                                        <li>
                                                                                            <label><?=$gf[6]['Label'];?>:</label>
                                                                                             <?php if($gf[6]['Label'] == 'ListPrice' || $gf[6]['Label'] == 'CurrentPrice' || $gf[6]['Label'] == 'List Price/SqFt' || $gf[6]['Label'] == 'List Price/Acre' || $gf[6]['Label'] == 'Rental Price/SqFt') {?>
                                                                                                        <span>$<?=number_format((float)$vmVal[$gf[6]['Label']],2);?></span>
                                                                                                    <?php } else  {?>
                                                                                                        <span><?=$vmVal[$gf[6]['Label']];?></span>
                                                                                             <?php } ?>
                                                                                        </li>
                                                                                    <?php }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else if($gf[1]['Domain'] == "StandardFields") {
                                                                    if(isset($objStandard[$gf[0]['Field']])) { ?>
                                                                        <li>
                                                                            <label><?=$gf[0]['Field'];?>:</label>
                                                                            <?php 
                                                                                if(is_array($objStandard[$gf[0]['Field']])) { 
                                                                                    foreach ($objStandard[$gf[0]['Field']] as $key => $value) { ?>
                                                                                        <?php if($gf[0]['Field'] == 'ListPrice' || $gf[0]['Field'] == 'CurrentPrice' || $gf[0]['Field'] == 'List Price/SqFt' || $gf[0]['Field'] == 'List Price/Acre' || $gf[0]['Field'] == 'Rental Price/SqFt') {?>
                                                                                                        <span>$<?=number_format((float)$key,2);?></span>
                                                                                                    <?php } else  {?>
                                                                                                        <span><?=$key;?></span>
                                                                                        <?php } ?>
                                                                            <?php   }
                                                                                } else {
                                                                            ?>
                                                                                        <?php if($gf[0]['Field'] == 'ListPrice' || $gf[0]['Field'] == 'CurrentPrice' || $gf[0]['Field'] == 'List Price/SqFt' || $gf[0]['Field'] == 'List Price/Acre' || $gf[0]['Field'] == 'Rental Price/SqFt') {?>
                                                                                                        <span>$<?=number_format((float)$objStandard[$gf[0]['Field']],2);?></span>
                                                                                                    <?php } else  {?>
                                                                                                        <span><?=$objStandard[$gf[0]['Field']];?></span>
                                                                                        <?php } ?>
                                                                            <?php } ?>
                                                                        </li>
                                                                <?php }
                                                                }
                                                            } 
                                                    } else break;
                                                    $count++;?>
                                            <?php 
                                                } 
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <ul class="property-detail-list">
                                            <?php 
                                                $count = 0;
                                                foreach($ar2 as $GroupedFields) {

                                                         if($count == 1) {
                                            ?>
                                                    <br/><h4 class='panel-title'><strong><?= $GroupedFields[0]; ?></strong></h4><br/>
                                                      <?php
                                                            $i= 0;
                                                       ?> 
                                                        <?php foreach($GroupedFields[1] as $gf) {
                                                             if($i++ >= 5) break;
                                                                if($gf[1]['Domain'] == "CustomFields") {
                                                                    foreach($objCustom[0]['Main'] as $main) {
                                                                        foreach($main as $kMain=>$vMain) {
                                                                            if($GroupedFields[0] == $kMain) {
                                                                                foreach($vMain as $vmKey => $vmVal) {
                                                                                    if(isset($vmVal[$gf[6]['Label']])) { ?>
                                                                                        <li>
                                                                                            <label><?=$gf[6]['Label'];?>:</label>

                                                                                            <?php if($gf[6]['Label'] == 'ListPrice' || $gf[6]['Label'] == 'CurrentPrice' || $gf[6]['Label'] == 'List Price/SqFt' || $gf[6]['Label'] == 'List Price/Acre' || $gf[6]['Label'] == 'Rental Price/SqFt') {?>
                                                                                                        <span>$<?=number_format((float)$vmVal[$gf[6]['Label']],2);?></span>
                                                                                                    <?php } else  {?>
                                                                                                        <span><?=$vmVal[$gf[6]['Label']];?></span>
                                                                                             <?php } ?>

                                                                                        </li>
                                                                                    <?php }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else if($gf[1]['Domain'] == "StandardFields") {
                                                                    if(isset($objStandard[$gf[0]['Field']])) { ?>
                                                                        <li>
                                                                            <label><?=$gf[0]['Field'];?>:</label>
                                                                            <?php 
                                                                                if(is_array($objStandard[$gf[0]['Field']])) { 
                                                                                    foreach ($objStandard[$gf[0]['Field']] as $key => $value) { ?>
                                                                                       <?php if($gf[0]['Field'] == 'ListPrice' || $gf[0]['Field'] == 'CurrentPrice' || $gf[0]['Field'] == 'List Price/SqFt' || $gf[0]['Field'] == 'List Price/Acre' || $gf[0]['Field'] == 'Rental Price/SqFt') {?>
                                                                                                        <span>$<?=number_format((float)$key,2);?></span>
                                                                                                    <?php } else  {?>
                                                                                                        <span><?=$key;?></span>
                                                                                        <?php } ?>
                                                                            <?php   }
                                                                                } else {
                                                                            ?>
                                                                                        <?php if($gf[0]['Field'] == 'ListPrice' || $gf[0]['Field'] == 'CurrentPrice' || $gf[0]['Field'] == 'List Price/SqFt' || $gf[0]['Field'] == 'List Price/Acre' || $gf[0]['Field'] == 'Rental Price/SqFt') {?>
                                                                                                        <span>$<?=number_format((float)$objStandard[$gf[0]['Field']],2);?></span>
                                                                                                    <?php } else  {?>
                                                                                                        <span><?=$objStandard[$gf[0]['Field']];?></span>
                                                                                        <?php } ?>
                                                                            <?php } ?>
                                                                        </li>
                                                                <?php }
                                                                }

                                                            } 
                                                    }
                                                    $count++;?>
                                            <?php 
                                                } 
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <ul class="property-detail-list">
                                                <?php 
                                                    $count = 0;
                                                    foreach($ar2 as $GroupedFields) {
                                                             if($count == 2) {
                                                ?>
                                                        <br/><h4 class='panel-title'><strong><?= $GroupedFields[0]; ?></strong></h4><br/>
                                                          <?php
                                                                $i= 0;
                                                           ?> 
                                                            <?php foreach($GroupedFields[1] as $gf) {
                                                                 if($i++ >= 5) break;
                                                                    if($gf[1]['Domain'] == "CustomFields") {
                                                                        foreach($objCustom[0]['Main'] as $main) {
                                                                            foreach($main as $kMain=>$vMain) {
                                                                                if($GroupedFields[0] == $kMain) {
                                                                                    foreach($vMain as $vmKey => $vmVal) {
                                                                                        if(isset($vmVal[$gf[6]['Label']])) { ?>
                                                                                            <li>

                                                                                                <label><?=$gf[6]['Label'];?>:</label>

                                                                                                <?php if($gf[6]['Label'] == 'Lot SqFt' || $gf[6]['Label'] == 'LotSizeSquareFeet' || $gf[6]['Label'] == 'Fin SqFt Above' || $gf[6]['Label'] == 'Fin SqFt Basement' || $gf[6]['Label'] == 'BuildingAreaTotal'){?>
                                                                                                    <span><?=number_format((float)$vmVal[$gf[6]['Label']],1);?></span>
                                                                                                <?php } else {?>
                                                                                                     <span><?=$vmVal[$gf[6]['Label']];?></span>
                                                                                                <?php }?>
                                                                                                
                                                                                            </li>
                                                                                        <?php }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else if($gf[1]['Domain'] == "StandardFields") {
                                                                        if(isset($objStandard[$gf[0]['Field']])) { ?>
                                                                            <li>
                                                                                <label><?=$gf[0]['Field'];?>:</label>
                                                                                <?php 
                                                                                    if(is_array($objStandard[$gf[0]['Field']])) { 
                                                                                        foreach ($objStandard[$gf[0]['Field']] as $key => $value) { ?>
                                                                                            <?php if($gf[0]['Field'] == 'Lot SqFt' || $gf[0]['Field'] == 'LotSizeSquareFeet' || $gf[0]['Field'] == 'Fin SqFt Basement' || $gf[0]['Field'] == 'Fin SqFt Above' || $gf[0]['Field'] == 'BuildingAreaTotal' ){?>
                                                                                                    <span>$<?=number_format((float)$key,1);?></span>
                                                                                                <?php } else {?>
                                                                                                    <span><?= $key;?></span>
                                                                                                <?php }?>

                                                                                            
                                                                                <?php   }
                                                                                    } else {
                                                                                ?>

                                                                                <?php if($gf[0]['Field'] == 'Lot SqFt' || $gf[0]['Field'] == 'LotSizeSquareFeet' || $gf[0]['Field'] == 'Fin SqFt Basement' || $gf[0]['Field'] == 'Fin SqFt Above' || $gf[0]['Field'] == 'BuildingAreaTotal' ){?>
                                                                                    <span><?= number_format((float)$objStandard[$gf[0]['Field']],1); ?></span>
                                                                                <?php } else {?>
                                                                                    <span><?= $objStandard[$gf[0]['Field']]; ?></span>
                                                                                <?php }?>
 
                                                                                <?php } ?>
                                                                            </li>
                                                                    <?php }
                                                                    }

                                                                } 
                                                        } 
                                                        $count++;?>
                                                <?php 
                                                    } 
                                                ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="view-full" role="presentation"><a href="#fullspec" role="tab" data-toggle="tab">View Full Specifications</a></div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="location">
                                <div class="location-info property-detail-sections">
                                    <h4>Map View</h4>
                                    <div style='overflow:hidden;height:450px;width:100%;'>
                                         <div id='gmap_canvas' style='height:450px;width:100%;'></div>
                                         <style>
                                         #gmap_canvas img {
                                             max-width: none!important;
                                             background: none!important
                                        }
                                        </style>
                                    </div>
                                    <script type='text/javascript'>
                                        function init_map() {
                                             // var infowindow;
                                            var school_location = {lat: <?php echo $objStandard['Latitude'];?>, lng: <?php echo $objStandard['Longitude'];?>};
                                            var geocoder = new google.maps.Geocoder();
                                            geocoder.geocode({ 'address': '<?php echo $objStandard['UnparsedAddress']; ?>' }, function (results, status) {
                                                if (status == google.maps.GeocoderStatus.OK) {
                                                    console.log("Call Okay");
                                                    latitude = results[0].geometry.location.lat();
                                                    longitude = results[0].geometry.location.lng();
                                                    var myOptions = {
                                                        zoom:12,
                                                        scrollwheel: false,
                                                        center:new google.maps.LatLng(latitude,longitude),
                                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                                    };
                                                    map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);

                                                    var service = new google.maps.places.PlacesService(school_map);
                                                   

                                                    marker = new google.maps.Marker({
                                                        map: map,
                                                        icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png",
                                                        position: new google.maps.LatLng(<?php echo $objStandard['Latitude'];?>,<?php echo $objStandard['Longitude'];?>
                                                    )});
                                                    
                                                    google.maps.event.addListener(marker, 'click', function() {
                                                        infowindow.open(map, marker);
                                                        map.setCenter(marker.getPosition()); 
                                                    });
                                                } 
                                            });
                                            
                                              
                                        }
                                        google.maps.event.addDomListener(window, 'load', init_map);
                                    </script>
                                    <div class="streetview">
                                         <h4>Street View</h4>
                                         <div style='overflow:hidden;height:450px;width:100%;'>
                                             <div id='gmap_canvas_streetview' style='height:450px;width:100%;'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div role="tabpanel" class="tab-pane" id="school">
                                <div class="location-info property-detail-sections">
                                     <h4>Nearby</h4>
                                        <ul style="padding:10px 0;">
                                            <?php 
                                                foreach($ar2 as $GroupedFields) {
                                                    foreach($GroupedFields[1] as $gf) {
                                                        if($gf[1]['Domain'] == "CustomFields") {
                                                            foreach($objCustom[0]['Main'] as $main) {
                                                                foreach($main as $kMain=>$vMain) {
                                                                    if($GroupedFields[0] == $kMain) {
                                                                        foreach($vMain as $vmKey => $vmVal) {
                                                                            if(isset($vmVal[$gf[6]['Label']])) { 
                                                                                if(strpos($gf[6]['Label'], 'School')) {
                                                                            ?>
                                                                                <li><strong><?= $gf[6]['Label']; ?></strong><?= ' : '.$vmVal[$gf[6]['Label']]; ?></li>
                                                                        <?php 
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else if($gf[1]['Domain'] == "StandardFields") {
                                                            if(isset($objStandard[$gf[0]['Field']])) { 
                                                                if(strpos($gf[0]['Field'], 'School')) {
                                                        ?>
                                                                <li><strong><?= $gf[0]['Field']; ?></strong><?= ' : '.$objStandard[$gf[0]['Field']]; ?></li>
                                                        <?php 
                                                                }
                                                            }
                                                        }
                                                    } 
                                                }
                                            ?>
                                        </ul>
                                      <div style='overflow:hidden;height:520px;width:100%;'>
                                        <ul class="radio-sample controls" id="mode-selector" >
                                          <li>
                                            <input type="radio" id="changemode-school" name="type" checked="checked">
                                            <label for="changemode-school"><i class="fa fa-university"></i><span> School </span></label>
                                            
                                            <div class="check"></div>
                                          </li>
                                          
                                          <li>
                                            <input type="radio" id="changemode-restaurants" name="type">
                                            <label for="changemode-restaurants"><i class="fa fa-cutlery"></i><span> Restaurant </span></label>
                                            
                                            <div class="check"><div class="inside"></div></div>
                                          </li>
                                          
                                          <li>
                                            <input type="radio" id="changemode-store" name="type">
                                            <label for="changemode-store"><i class="fa fa-shopping-cart"></i><span> Store </span></label>
                                            
                                            <div class="check"><div class="inside"></div></div>
                                          </li>
                                        </ul>
                                         <!-- <div id="mode-selector" class="controls">
                                            <input type="radio" name="type" id="changemode-school" checked="checked">
                                            <label for="changemode-school">School</label>

                                            <input type="radio" name="type" id="changemode-restaurants">
                                            <label for="changemode-restaurants">Restaurant</label>

                                            <input type="radio" name="type" id="changemode-store">
                                            <label for="changemode-store">Store</label>
                                        </div> -->

                                         <div id="school_map" style="height:450px;width:100%;"></div>
                                         <div id="restaurant_map" style="height:450px;width:100%;"></div>
                                         <div id="store_map" style="height:450px;width:100%;"></div>
                                             <style>
                                                #school_map,#restaurant_map,#store_map img {
                                                     max-width: none!important;
                                                     background: none!important
                                                }

                                                .controls {
                                                    margin-top: 10px;
                                                    border: 1px solid transparent;
                                                    border-radius: 2px 0 0 2px;
                                                    box-sizing: border-box;
                                                    -moz-box-sizing: border-box;
                                                    height: 32px;
                                                    outline: none;
                                                    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                                                  }

                                                #mode-selector {
                                                    color: #fff;
                                                    background-color: #4d90fe;
                                                    margin-left: 0px;
                                                    padding: 0;
                                                  }

                                                  #mode-selector label {
                                                    font-family: sans-serif;
                                                    font-size: 14px;
                                                    font-weight: 400;
                                                    color:#3C3B3E;
                                                    padding: 5px;
                                                  }

                                            </style>
                                        </div>
                                    <script type='text/javascript'>
                                      var school_map;
                                      var infowindow;

                                      function initSchoolMap() {

                                        var modes = "school";
                                        


                                        var school_location = {lat: <?php echo $objStandard['Latitude'];?>, lng: <?php echo $objStandard['Longitude'];?>};
                                        school_map = new google.maps.Map(document.getElementById('school_map'), {
                                          center: school_location,
                                          zoom: 13,
                                          scrollwheel: false,
                                          mapTypeId: google.maps.MapTypeId.ROADMAP
                                        });

                                        marker =  new google.maps.Marker({
                                            position: school_location,
                                            map: school_map,
                                            title: '<?php echo $objStandard['UnparsedAddress']; ?>'
                                        });

                                        setupClickListener('changemode-school', "school", "school_location");
                                        setupClickListener('changemode-restaurants', "restaurant", "school_location");
                                        setupClickListener('changemode-store', "store", "school_location");

                                        infowindow = new google.maps.InfoWindow();
                                        var service = new google.maps.places.PlacesService(school_map);
                                        
                                        if( modes == "school")
                                        {
                                            service.nearbySearch({
                                              location: school_location,
                                              radius: 1500,
                                              type: modes
                                            }, callback);
                                        }

                                        function setupClickListener(id, mode,loc) {
                                           
                                            
                                            var radioButton = document.getElementById(id);

                                            radioButton.addEventListener('click', function() { 
                                                modes = mode;

                                                if( mode == "restaurant" )
                                                {
                                                    mode_restaurant( mode );
                                                }

                                                if( mode == "school" )
                                                {
                                                    mode_school( mode );
                                                }

                                                if( mode == "store" )
                                                {
                                                    mode_store( mode );
                                                }
                                                // var service = new google.maps.places.PlacesService(school_map); 
                                                //set_places( modes , service, school_location);
                                            });

                                        }

                                        

                                      }

                                      function mode_school( modes, school_map )
                                      {
                                            document.getElementById('restaurant_map').style.display = 'none';
                                            document.getElementById('store_map').style.display = 'none';
                                            //marker.setVisible(false);
                                            var sc_location = {lat: <?php echo $objStandard['Latitude'];?>, lng: <?php echo $objStandard['Longitude'];?>};
                                            
                                            document.getElementById('school_map').style.display = 'block';
                                            school_map2 = new google.maps.Map(document.getElementById('school_map'), {
                                                      center: sc_location,
                                                      zoom: 12,
                                                      scrollwheel: false,
                                                      mapTypeId: google.maps.MapTypeId.ROADMAP
                                                    });

                                            marker =  new google.maps.Marker({
                                                position: sc_location,
                                                map: school_map2,
                                                title: '<?php echo $objStandard['UnparsedAddress']; ?>'
                                            });

                                            infowindow = new google.maps.InfoWindow();

                                            var service = new google.maps.places.PlacesService(school_map2);
                                            
                                            service.nearbySearch({
                                                  location: sc_location,
                                                  radius: 1500,
                                                  type: modes
                                                }, callback_school);
                                      }

                                      function mode_store( modes )
                                      {
                                            document.getElementById('restaurant_map').style.display = 'none';
                                            document.getElementById('school_map').style.display = 'none';
                                            //marker.setVisible(false);
                                            var st_location = {lat: <?php echo $objStandard['Latitude'];?>, lng: <?php echo $objStandard['Longitude'];?>};
                                            
                                            document.getElementById('store_map').style.display = 'block';
                                            store_map = new google.maps.Map(document.getElementById('store_map'), {
                                                      center: st_location,
                                                      zoom: 12,
                                                      scrollwheel: false,
                                                      mapTypeId: google.maps.MapTypeId.ROADMAP
                                                    });

                                            marker =  new google.maps.Marker({
                                                position: st_location,
                                                map: store_map,
                                                title: '<?php echo $objStandard['UnparsedAddress']; ?>'
                                            });

                                            infowindow = new google.maps.InfoWindow();

                                            var service = new google.maps.places.PlacesService(store_map);
                                            
                                            service.nearbySearch({
                                                  location: st_location,
                                                  radius: 1500,
                                                  type: modes
                                                }, callback_store);
                                      }

                                        function mode_restaurant( modes, school_map )
                                        {
                                            document.getElementById('school_map').style.display = 'none';
                                            document.getElementById('store_map').style.display = 'none';
                                            //marker.setVisible(false);
                                            var res_location = {lat: <?php echo $objStandard['Latitude'];?>, lng: <?php echo $objStandard['Longitude'];?>};
                                            
                                            document.getElementById('restaurant_map').style.display = 'block';
                                            restaurant_map = new google.maps.Map(document.getElementById('restaurant_map'), {
                                                      center: res_location,
                                                      zoom: 12,
                                                      scrollwheel: false,
                                                      mapTypeId: google.maps.MapTypeId.ROADMAP
                                                    });

                                            marker =  new google.maps.Marker({
                                                position: res_location,
                                                map: restaurant_map,
                                                title: '<?php echo $objStandard['UnparsedAddress']; ?>'
                                            });

                                            infowindow = new google.maps.InfoWindow();

                                            var service = new google.maps.places.PlacesService(restaurant_map);
                                            
                                            service.nearbySearch({
                                                  location: res_location,
                                                  radius: 1500,
                                                  type: modes
                                                }, callback_restaurant);
                                        }

                                      function set_places ( modes , service ,school_location)
                                      {
                                        //var service = new google.maps.places.PlacesService(school_map);
                                            service.nearbySearch({
                                                location: school_location,
                                                radius: 1500,
                                                type: modes
                                            }, callback);
                                      }

                                      function callback(results, status) {

                                        var types = ["school","restaurant",'store'];

                                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                                            for (var i = 0; i < results.length; i++) {
                                                //console.log( results[i].types[0]  );
                                                school_createMarker(results[i], results[i].types[0]);                                                                                        
                                            }

                                        }
                                      }

                                      function callback_restaurant(results, status) {

                                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                                            for (var i = 0; i < results.length; i++) {
                                                //console.log( results[i].types[0]  );
                                                restaurant_createMarker(results[i], results[i].types[0]);                                                                                        
                                            }

                                        }
                                      }

                                      function callback_store(results, status) {

                                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                                            for (var i = 0; i < results.length; i++) {
                                                //console.log( results[i].types[0]  );
                                                store_createMarker(results[i], results[i].types[0]);                                                                                        
                                            }

                                        }
                                      }

                                       function callback_school(results, status) {

                                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                                            for (var i = 0; i < results.length; i++) {
                                                //console.log( results[i].types[0]  );
                                                clickschool_createMarker(results[i], results[i].types[0]);                                                                                        
                                            }

                                        }
                                      }

                                      function school_createMarker(place , type) {
                                       
                                        console.log(type);
                                            switch( type ){
                                                case "school" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
                                                     break;

                                                case "restaurant" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                                                     break;

                                                case "store" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                                                     break;

                                                case "grocery_or_supermarket" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                                                     break;

                                            }

                                            //var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";


                                        if( type == "school" || type == "restaurant" || type == "store" || type == "grocery_or_supermarket" )
                                        {   
                                            var marker = new google.maps.Marker({
                                                map: school_map,
                                                position: place.geometry.location,
                                                icon: school_icon
                                            });
                                        }
                                                                             

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            infowindow.setContent(place.name);
                                            infowindow.open(school_map, marker);
                                        }); 

                                        google.maps.event.addListener(marker, 'mouseout', function() {
                                           infowindow.close();
                                        });

                                        var service = new google.maps.places.PlacesService(school_map);
                                        var request = {placeId: place.place_id};
                                        //console.log(request);
                                        service.getDetails(request, function(details, status) {
                                          google.maps.event.addListener(marker, 'click', function() {
                                            if (status == google.maps.places.PlacesServiceStatus.OK) {

                                                var html = "<div style='position:relative'><br>" + details.name + "<br>" + details.vicinity + "<br>" + (details.formatted_phone_number !=undefined ? details.formatted_phone_number : "") + "<br>" + (details.website != undefined ? details.website : "" ) + "<br>" + (details.url != undefined ? "<a href='" + details.url + "' target='_blank' >View on Google Maps</a>" :"") + "</div>";
                                              iw = new google.maps.InfoWindow({
                                                 content: html
                                              });
                                                iw.open(school_map, marker);
                                            }
                                          });
                                        });

                                      }

                                      function clickschool_createMarker(place , type) {
                                       
                                        console.log(type);
                                            switch( type ){
                                                case "school" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
                                                     break;

                                                case "restaurant" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                                                     break;

                                                case "store" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                                                     break;

                                                case "grocery_or_supermarket" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                                                     break;

                                            }

                                            //var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";


                                        if( type == "school" || type == "restaurant" || type == "store" || type == "grocery_or_supermarket" )
                                        {   
                                            var marker = new google.maps.Marker({
                                                map: school_map2,
                                                position: place.geometry.location,
                                                icon: school_icon
                                            });
                                        }
                                                                             

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            infowindow.setContent(place.name);
                                            infowindow.open(school_map2, marker);
                                        }); 

                                        google.maps.event.addListener(marker, 'mouseout', function() {
                                           infowindow.close();
                                        });

                                        var service = new google.maps.places.PlacesService(school_map2);
                                        var request = {placeId: place.place_id};
                                        //console.log(request);
                                        service.getDetails(request, function(details, status) {
                                          google.maps.event.addListener(marker, 'click', function() {
                                            if (status == google.maps.places.PlacesServiceStatus.OK) {

                                                var html = "<div style='position:relative'><br>" + details.name + "<br>" + details.vicinity + "<br>" + (details.formatted_phone_number !=undefined ? details.formatted_phone_number : "") + "<br>" + (details.website != undefined ? details.website : "" ) + "<br>" + (details.url != undefined ? "<a href='" + details.url + "' target='_blank' >View on Google Maps</a>" :"") + "</div>";
                                              iw = new google.maps.InfoWindow({
                                                 content: html
                                              });
                                                iw.open(school_map2, marker);
                                            }
                                          });
                                        });

                                      }

                                    function restaurant_createMarker(place , type) {
                                       
                                        console.log(type);
                                            switch( type ){
                                                case "school" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
                                                     break;

                                                case "restaurant" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                                                     break;

                                                case "store" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                                                     break;

                                                case "grocery_or_supermarket" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                                                     break;

                                            }

                                            //var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                                        
                                        if( type == "school" || type == "restaurant" || type == "store" || type == "grocery_or_supermarket" )
                                        {   
                                            var marker = new google.maps.Marker({
                                                map: restaurant_map,
                                                position: place.geometry.location,
                                                icon: school_icon
                                            });
                                        }
                                                                             

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            infowindow.setContent(place.name);
                                            infowindow.open(restaurant_map, marker);
                                        }); 

                                        google.maps.event.addListener(marker, 'mouseout', function() {
                                           infowindow.close();
                                        });

                                        var service = new google.maps.places.PlacesService(restaurant_map);
                                        var request = {placeId: place.place_id};
                                        //console.log(request);
                                        service.getDetails(request, function(details, status) {
                                          google.maps.event.addListener(marker, 'click', function() {
                                            if (status == google.maps.places.PlacesServiceStatus.OK) {

                                                var html = "<div style='position:relative'><br>" + details.name + "<br>" + details.vicinity + "<br>" + (details.formatted_phone_number !=undefined ? details.formatted_phone_number : "") + "<br>" + (details.website != undefined ? details.website : "" ) + "<br>" + (details.url != undefined ? "<a href='" + details.url + "' target='_blank' >View on Google Maps</a>" :"") + "</div>";
                                              iw = new google.maps.InfoWindow({
                                                 content: html
                                              });
                                                iw.open(restaurant_map, marker);
                                            }
                                          });
                                        });

                                    }

                                    function store_createMarker(place , type) {
                                       
                                        console.log(type);
                                            switch( type ){
                                                case "school" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
                                                     break;

                                                case "restaurant" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                                                     break;

                                                case "store" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                                                     break;

                                                case "grocery_or_supermarket" : 
                                                     var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                                                     break;

                                            }

                                            //var school_icon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
                                        
                                        if( type == "school" || type == "restaurant" || type == "store" || type == "grocery_or_supermarket" )
                                        {   
                                            var marker = new google.maps.Marker({
                                                map: store_map,
                                                position: place.geometry.location,
                                                icon: school_icon
                                            });
                                        }
                                                                             

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            infowindow.setContent(place.name);
                                            infowindow.open(store_map, marker);
                                        }); 

                                        google.maps.event.addListener(marker, 'mouseout', function() {
                                           infowindow.close();
                                        });

                                        var service = new google.maps.places.PlacesService(store_map);
                                        var request = {placeId: place.place_id};
                                        //console.log(request);
                                        service.getDetails(request, function(details, status) {
                                          google.maps.event.addListener(marker, 'click', function() {
                                            if (status == google.maps.places.PlacesServiceStatus.OK) {

                                                var html = "<div style='position:relative'><br>" + details.name + "<br>" + details.vicinity + "<br>" + (details.formatted_phone_number !=undefined ? details.formatted_phone_number : "") + "<br>" + (details.website != undefined ? details.website : "" ) + "<br>" + (details.url != undefined ? "<a href='" + details.url + "' target='_blank' >View on Google Maps</a>" :"") + "</div>";
                                              iw = new google.maps.InfoWindow({
                                                 content: html
                                              });
                                                iw.open(store_map, marker);
                                            }
                                          });
                                        });

                                    }

                                      google.maps.event.addDomListener(window, 'load', initSchoolMap);

                                    </script>
                                   
                                </div>
                            </div>
                            <?php //printA($ar2); exit;//echo "<pre>"; printA($ar2)." ".var_dump($ar2); echo "</pre>"?>
                            <div role="tabpanel" class="tab-pane" id="fullspec">
                                <div class="property-detail-sections">
                                    <div class="panel-group" id="accordion">
                                         <?php 
                                            $count = 0;
                                            $itemCount = 0;
                                            foreach($ar2 as $GroupedFields) { 
                                                //var_dump($GroupedFields[0]);
                                                foreach($objCustom[0]['Main'] as $main) {
                                                    if(isset($main[$GroupedFields[0]])) {
                                                        //var_dump($main[$GroupedFields[0]]); ?>
                                                        <div class="panel panel-default" style="border-radius: 0px !important;">
                                                            <div class='panel-heading'>
                                                                <?php $id = "group".key($GroupedFields).$itemCount;#strtr($GroupedFields[0], array("." => "", "," => "", "/" => "", "&" => "", "'" => "", "." => "", "/" => "", "$" => "", " " => "")); ?>
                                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#<?= $id; ?>"><h4 class='panel-title'><?= $GroupedFields[0]; ?><i class="indicator glyphicon <?php if($count < 2) echo 'glyphicon-chevron-down'; else echo 'glyphicon-chevron-up'; ?> pull-right"></i></h4></a>
                                                            </div>
                                                            <div id="<?= $id; ?>" class="panel-collapse collapse<?php if($count < 2) echo ' in'; ?>">
                                                                <div class="panel-body ">
                                                                    <ul class='list-group property-detail-list'>
                                                                    <?php 
                                                                        foreach($GroupedFields[1] as $gf) {
                                                                            if($gf[1]['Domain'] == "CustomFields") { // CustomFields & StandardFields
                                                                                foreach($objCustom[0]['Main'] as $main) {
                                                                                    foreach($main as $kMain=>$vMain) {
                                                                                        if($GroupedFields[0] == $kMain) {
                                                                                            foreach($vMain as $vmKey => $vmVal) {
                                                                                                if(isset($vmVal[$gf[6]['Label']])) { ?>
                                                                                                    <li class="list-group-item">
                                                                                                        <strong><?= $gf[6]['Label']; ?>:</strong>
                                                                                                            <?php if($gf[6]['Label'] == 'ListPrice' || $gf[6]['Label'] == 'CurrentPrice' || $gf[6]['Label'] == 'List Price/SqFt' || $gf[6]['Label'] == 'List Price/Acre' || $gf[6]['Label'] == 'Rental Price/SqFt') {?>
                                                                                                                    <span>$<?=number_format((float)$vmVal[$gf[6]['Label']],2);?></span>
                                                                                                                <?php } elseif($gf[6]['Label'] == 'Lot SqFt' || $gf[6]['Label'] == 'LotSizeArea' || $gf[6]['Label'] == 'LotSizeSquareFeet' || $gf[6]['Label'] == 'Fin SqFt Basement' || $gf[6]['Label'] == 'Fin SqFt Above' || $gf[6]['Label'] == 'BuildingAreaTotal' ){?>
                                                                                                                     <span><?=number_format((float)$vmVal[$gf[6]['Label']],1);?></span>
                                                                                                                <?php } else {?>
                                                                                                                    <span><?=$vmVal[$gf[6]['Label']];?></span>
                                                                                                            <?php } ?>
                                                                                                    </li>
                                                                                            <?php }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            else if($gf[1]['Domain'] == "StandardFields") {
                                                                                if(isset($objStandard[$gf[0]['Field']])) { ?>
                                                                                    <li class="list-group-item"><strong><?= $gf[0]['Field'];?>:</strong>
                                                                                        <?php 
                                                                                            if(is_array($objStandard[$gf[0]['Field']])) {
                                                                                                foreach ($objStandard[$gf[0]['Field']] as $key => $value) {?>

                                                                                                     <?php if($gf[0]['Field'] == 'ListPrice' || $gf[0]['Field'] == 'CurrentPrice' || $gf[0]['Field']== 'List Price/SqFt' || $gf[0]['Field']== 'List Price/Acre' || $gf[0]['Field']== 'Rental Price/SqFt') {?>
                                                                                                                    <span>$<?=number_format((float)$key,2);?></span>
                                                                                                                <?php } elseif($gf[0]['Field'] == 'Lot SqFt' || $gf[0]['Field'] == 'LotSizeSquareFeet' || $gf[0]['Field'] == 'Fin SqFt Basement' || $gf[0]['Field'] == 'Fin SqFt Above' || $gf[0]['Field'] == 'BuildingAreaTotal' ){?>
                                                                                                                     <span><?=number_format((float)$key,1);?></span>
                                                                                                                <?php } else  {?>
                                                                                                                    <span><?=$key;?></span>
                                                                                                    <?php } ?>
                                                                                           <?php     }
                                                                                            } else {?>

                                                                                                <?php if($gf[0]['Field'] == 'ListPrice' || $gf[0]['Field'] == 'CurrentPrice' || $gf[0]['Field']== 'List Price/SqFt' || $gf[0]['Field']== 'List Price/Acre' || $gf[0]['Field']== 'Rental Price/SqFt') {?>
                                                                                                                    <span>$<?=number_format((float)$objStandard[$gf[0]['Field']],2);?></span>
                                                                                                                <?php } elseif($gf[0]['Field'] == 'Lot SqFt' || $gf[0]['Field'] == 'LotSizeArea' || $gf[0]['Field'] == 'LotSizeSquareFeet' || $gf[0]['Field'] == 'Fin SqFt Basement' || $gf[0]['Field'] == 'Fin SqFt Above' || $gf[0]['Field'] == 'BuildingAreaTotal' ){?>
                                                                                                                    <span><?=number_format((float)$objStandard[$gf[0]['Field']],1);?></span>
                                                                                                                <?php } else  {?>
                                                                                                                    <span><?=$objStandard[$gf[0]['Field']];?></span>
                                                                                                <?php } ?>
                                                                                                
                                                                                          <?php  }
                                                                                        ?>
                                                                                    </li>
                                                                            <?php }
                                                                            }
                                                                        } 
                                                                    ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php 
                                                    }
                                                }
                                                $count++; $itemCount++;
                                            }
                                        ?>
                                    </div>
                                    <?php if (!empty($rooms)) { ?>
                                    <table class="table table-responsive table-striped">
                                        <thead>
                                            <tr>
                                                <td><strong>Room Name</strong></td><td><strong>Room Level</strong></td><td><strong>Dimensions</strong></td><td><strong>Room Remarks</strong></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($rooms as $key) { ?>
                                                <tr>

                                                    <td><?php if(isset( $key['Fields'][0]['Room Name'])) echo $key['Fields'][0]['Room Name']; ?></td>
                                                    <td><?php if(isset($key['Fields'][1]['Room Level'])) { echo $key['Fields'][1]['Room Level']; } else { echo "N/A"; }?></td>
                                                    <td><?php if(isset($key['Fields'][2]['Dimensions'])) echo $key['Fields'][2]['Dimensions']; elseif(isset($key['Fields'][1]['Length']) && isset($key['Fields'][2]['Width'])) echo $key['Fields'][1]['Length'] .' X '. $key['Fields'][2]['Width']; else echo "N/A"?></td>
                                                     <td><?php if(isset($key['Fields'][3]['Room Remarks'])) { echo $key['Fields'][3]['Room Remarks']; } else { echo "N/A"; }?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <?php } ?>
                                </div>                                  
                            </div>
                        </div>

                        <!-- Other Listings -->
                        <!-- <div class="property-other-listings property-detail-sections">
                            <h4>Other Related Listings</h4>
                            <?php
                                foreach($other as $feature) {
                            ?>
                                <div class="col-md-3">
                                    <div class="other-listing-item">
                                        <div class="other-related-items">
                                            <?php
                                            if($feature['StandardFields']['Photos']) {
                                                ?>
                                                <a href="<?=base_url('home/other_property?listingId=');?><?php echo str_replace(" ",  "-", $feature['Id']);?>"><img src="<?= $feature['StandardFields']['Photos'][0]['Uri300'] ?>" alt="<?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>" class="img-responsive"></a>
                                                <?php
                                            }
                                            else {
                                            ?>
                                                <a href="<?=base_url('home/other_property?listingId=');?><?php echo str_replace(" ",  "-", $feature['Id']);?>"><img src="<?=base_url()."assets/img/home/no-image-listing.png"?>" alt="<?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>" class="img-responsive"></a>
                                                <?php
                                                }
                                            ?>
                                        </div>
                                        <div class="other-related-description">
                                            <p><a href="<?=base_url('home/other_property?listingId=');?><?php echo str_replace(" ",  "-", $feature['Id']);?>"><?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?></a></p>
                                            <p><i class="fa fa-map-marker"></i><?php echo str_replace($feature['StandardFields']["UnparsedFirstLineAddress"].',', "", $feature['StandardFields']["UnparsedAddress"]); ?></p>
                                            <p><i class="fa fa-usd"></i> <?php echo number_format($feature['StandardFields']['CurrentPrice'])?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>
                        </div> -->

                        <!-- Nearby Listings -->
                        <div class="property-other-listings property-detail-sections">
                            <h4>Nearby Listings</h4>

                            <?php
                                $nearbyCount = count($nearby_listings);

                             if($nearbyCount >1){ ?>

                                <div class="sold-property-container">
                                    <?php
                                        $i=1;
                                        foreach(array_slice($nearby_listings,1) as $nearby){
                                            if ($i > 1) {
                                    ?>
                                            <div class="other-listing-item">
                                                <div class="other-related-items">
                                                    <?php
                                                    if($nearby['StandardFields']['Photos']) {
                                                        ?>
                                                        <a href="<?=base_url('home/other_property?listingId=');?><?php echo str_replace(" ",  "-", $nearby['Id']);?>"><img src="<?= $nearby['StandardFields']['Photos'][0]['Uri300'] ?>" alt="<?php echo $nearby['StandardFields']["UnparsedFirstLineAddress"]; ?>" class="img-responsive"></a>
                                                        <?php
                                                    }
                                                    else {
                                                    ?>
                                                        <a href="<?=base_url('home/other_property?listingId=');?><?php echo str_replace(" ",  "-", $nearby['Id']);?>"><img src="<?=base_url()."assets/img/home/no-image-listing.png"?>" alt="<?php echo $nearby['StandardFields']["UnparsedFirstLineAddress"]; ?>" class="img-responsive"></a>
                                                        <?php
                                                        }
                                                    ?>
                                                </div>
                                                <div class="other-related-description">
                                                    <div class="nearby-listing-type">
                                                       <?php
                                                            if(isset($nearby['StandardFields']['PropertyClass'])){?>
                                                                <?php echo $nearby['StandardFields']['PropertyClass'];?>
                                                        <?php  } ?> 
                                                    </div>
                                                    <p><a href="<?=base_url('home/other_property?listingId=');?><?php echo str_replace(" ",  "-", $nearby['Id']);?>"><?php echo $nearby['StandardFields']["UnparsedFirstLineAddress"]; ?></a></p>
                                                    <p><i class="fa fa-map-marker"></i><?php echo str_replace($nearby['StandardFields']["UnparsedFirstLineAddress"].',', "", $nearby['StandardFields']["UnparsedAddress"]); ?></p>
                                                    <p><i class="fa fa-usd"></i> <?php echo number_format($nearby['StandardFields']['CurrentPrice'])?></p>
                                                </div>
                                            </div>
                                    <?php
                                        } $i++;
                                    }
                                }else{ ?>

                                    <div class="alert alert-info text-center">No Nearby Listings Found!</div>
                            
                              <?php  } ?>
                                
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
        $this->load->view($theme.'/session/footer');
    ?>
