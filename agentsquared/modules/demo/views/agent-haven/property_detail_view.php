<?php $this->load->view('/session/header');?>

    <section class="property-detail-header">
        <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property3.jpg" alt="" class="img-responsive">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="property-header-text">
                        <h2>My Property</h2>
                        <p>Beverly Hills 2010</p>
                        <h3>$1,000,000</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="property-detail-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="visible-xs">
                        <button class="btn btn-default submit-button contact-agent-button">
                            Contact Agent
                        </button>
                        <div class="filter-tab contact-agent">
                            <div class="property-submit-question">
                                <h4 class="text-center">Have a question?</h4>
                                <hr>
                                <form action="">
                                    <div class="form-group">
                                        <label for="">Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                    <button class="btn btn-default btn-block submit-button">Submit</button>
                                </form> 
                            </div>
                        </div>
                        <div class="floating-panel">
                            <ul class="property-social-share">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>                   
                    </div>
                    <div class="contact-panel">
                        <div class="filter-tab">
                            <div class="property-agent-info">
                                <h4 class="text-center">Agent</h4>
                                <hr>
                                <div class="agent-image">
                                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/john.jpg" alt="" class="img-thumbnail">    
                                </div>
                                <h4 class="agent-name">John Doe</h4>
                                <p>
                                    <i class="fa fa-phone-square"></i> Office: 888-587-7767 x 1
                                    <br>
                                    <i class="fa fa-2x fa-mobile"></i> Mobile: 561-239-2929
                                    <br>
                                    <i class="fa fa-fax"></i> Fax : 855-297-7507
                                    <br>
                                </p>
                                <p><i class="fa fa-envelope"></i> : <a href="">Email Me</a></p>
                            </div>
                        </div>

                        <div class="filter-tab">
                            <h4 class="text-center">Share this property to:</h4>
                            <hr>
                            
                            <ul class="list-inline property-social-share">
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="filter-tab">
                            <div class="property-submit-question">
                                <h4 class="text-center">Have a question?</h4>
                                <hr>
                                <form action="">
                                    <div class="form-group">
                                        <label for="">Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                    <button class="btn btn-default btn-block submit-button">Submit</button>
                                </form> 
                            </div>
                        </div>
                        <div class="filter-panel">
                            <div class="filter-tab clearfix">
                                <h4 class="text-center">Mortgage Calculator</h4>
                                <hr>
                                <form  method="post" id="mortgageCalculator">
                                    <div class="form-group">
                                        <label for="sale_price">Price of Home ($<span id="sale_price_label"></span>)</label>
                                        <input type="text" name="sale_price" id="sale_price" value="<?php echo $this->session->flashdata('mortgage_price_value'); ?>" class="form-control">
                                        <input type="hidden" name="calculator_url" value="<?php echo base_url();?>home/mortgage_calculator">
                                    </div>
                                    <div class="form-group">
                                        <label for="down_percent">Downpayment (<span id="down_percent_label"></span>%)</label>
                                        <input type="text" name="down_percent" id="down_percent" value="<?php echo $this->session->flashdata('mortgage_downpayment_value'); ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="mortgage_interest_percent">Annual Interest Rate (<span id="mortgage_interest_percent_label"></span>%)</label>
                                        <input type="text" name="mortgage_interest_percent" id="mortgage_interest_percent" value="<?php echo $this->session->flashdata('mortgage_interest_rate_value'); ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="year_term">Loan Term (<span id="year_term_label"></span>  years)</label>
                                        <input type="text" name="year_term" id="year_term" value="<?php echo $this->session->flashdata('mortgage_loan_terms_value'); ?>" class="form-control">
                                    </div>
                                    <div class="col-md-12 col-sm-12 nopadding">
                                        <button class="btn btn-default btn-block submit-button" id="morg_calc_btn">Calculate <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i></button>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                       <div id="mortgage-result">
                                           <?php echo $this->session->flashdata('mortgage_monthly'); ?>
                                           <!-- <?php echo $this->session->flashdata('mortgage_monthly_interest'); ?>
                                           <?php echo $this->session->flashdata('mortgage_annual_interest'); ?>
                                           <?php echo $this->session->flashdata('mortgage_downpayment'); ?>
                                           <?php echo $this->session->flashdata('mortgage_amount_financed'); ?> -->
                                       </div>
                                    </div>
                                </form>
                            </div>
                            <div class="filter-tab">
                                <div class="property-submit-question">
                                    <h4 class="text-center">Have a question?</h4>
                                    <hr>
                                    <form action="">
                                        <div class="form-group">
                                            <label for="">Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Message</label>
                                            <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                        <button class="btn btn-default btn-block submit-button">Submit</button>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="property-details">
                        <div class="row">
                            <div class="col-md-9 col-sm-7 col-xs-12">
                                <h5 class="property-title">
                                    My Property
                                    <span class="get-directions"><a class="bottom" title="" data-placement="bottom" data-toggle="tooltip" href="https://www.google.com.ph/maps/search/888+Prospect+St+%23200,+La+Jolla,+CA+92037/@32.8466556,-117.2781672,17z/data=!3m1!4b1" data-original-title="Get Directions" target="_blank"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/get-direction.png" width="15px" height="15px" alt="Google Maps Direction"></a></span>
                                </h5>
                                <h5 class="property-address">Beverly Hills 2010</h5>
                                <h5 class="property-type">Residential</h5>
                                <h5 class="property-price">$1,000,000</h5>
                            </div>
                            <div class="col-md-3 col-sm-5 col-xs-12 text-right">
                                <h4>RAPB/JTHS MLS</h4>
                                <p style="margin: 0 0 5px;"><strong>MLS&reg; #:</strong> <small>RX-10246002</small></p>
                                <p><strong>Courtesy of:</strong> <small>US Property and Investment</small></p>

                                <ul class="listing-icons list-inline">
                                    <li>
                                     <?php if(isset($virtual_tours) AND !empty($virtual_tours)){?>
                                        <p class="virtualTour"><a href="#tourModal" data-toggle="modal" data-title="Tooltip" data-trigger="hover" title="Virtual Tour"><i class="fa fa-2x fa-file-video-o"></i></a></p>
                                        <p><small>Tour</small></p>
                                    <?php } ?>
                                        
                                    </li>
                                    <li>
                                        <a href="#" data-pro-id="" class="save-favorate" data-title="Tooltip" data-trigger="hover" title="Save Property">
                                            <span> <i class='fa fa-2x fa-heart'></i> </span>
                                        </a>
                                         <p> <small>Save</small> </p>
                                    </li>
                                     <li>
                                        <p>
                                            <a href='#schedModal'  class='sched_modal' data-toggle='modal' data-title='Tooltip' data-trigger='hover' title='Schedule for Showing'><i class='fa fa-2x fa-calendar'></i></a>
                                        </p>
                                        <p><small>Showing</small></p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#property-detail" aria-controls="home" role="tab" data-toggle="tab">Property Details</a></li>
                            <li role="presentation"><a href="#location" role="tab" data-toggle="tab">Location</a></li>
                            <li role="presentation"><a href="#fullspec" role="tab" data-toggle="tab">Full Specifications</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="property-detail">
                                <div class="description property-detail-sections">
                                    <h4>Description</h4>
                                    <p>
                                        Pellentesque nisi tortor, congue eu consectetur ac, molestie in eros. Nullam ac suscipit est. Vestibulum pellentesque tincidunt magna non euismod. Maecenas suscipit nec augue vitae laoreet. Proin purus eros, tincidunt vel nulla quis, vulputate posuere ex. In hendrerit suscipit purus non dictum. In consectetur, ipsum eget egestas tempor, neque enim aliquet risus, vel imperdiet elit nibh eu nisi. Phasellus nulla justo, tristique eget vestibulum quis, cursus quis felis. Donec ac lacus volutpat, convallis leo at, ornare massa. Etiam ac placerat odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec orci nibh, mollis non nunc eget, faucibus condimentum ipsum. Phasellus quis lorem eu turpis dictum consectetur. Integer tempor magna vel nulla sagittis ultricies. 
                                    </p>
                                </div>
                                <div class="photo-gallery property-detail-sections">
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/test-property-img3.jpg" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/test-property-img3.jpg" alt="" class="img-responsive img-thumbnail">  
                                            </a>    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property2.jpg" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property2.jpg" alt="" class="img-responsive img-thumbnail">    
                                            </a>    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property3.jpg" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property3.jpg" alt="" class="img-responsive img-thumbnail">    
                                            </a>    
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property4.jpg" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property4.jpg" alt="" class="img-responsive img-thumbnail">    
                                            </a>    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property5.png" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property5.png" alt="" class="img-responsive img-thumbnail">    
                                            </a>    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property7.png" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property7.png" alt="" class="img-responsive img-thumbnail">    
                                            </a>    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property8.png" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property8.png" alt="" class="img-responsive img-thumbnail">    
                                            </a>    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property9.png" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property9.png" alt="" class="img-responsive img-thumbnail"> 
                                                <div class="see-all">
                                                    <p>See all 10 photos</p>
                                                </div> 
                                            </a>    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/bg01.jpg" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/bg01.jpg" alt="" class="img-responsive img-thumbnail">    
                                            </a>    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <div class="photo-gallery-image">
                                            <a href="<?=base_url()?>assets/images/dashboard/demo/agent-haven/bg02.jpg" data-gallery="">
                                                <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/bg02.jpg" alt="" class="img-responsive img-thumbnail">    
                                            </a>    
                                        </div>
                                    </div>
                                </div>
                                <div class="specifications property-detail-sections">
                                    <h4>Specifications</h4>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <ul class="property-detail-list">
                                                <br/>
                                                <h4 class='panel-title'><strong>Contract Information</strong></h4>
                                                <br/>
                                                <li><label>Auction</label>: <span>No</span> </li>
                                                <li><label>ListPrice</label>: <span>$325,000.00</span> </li>
                                                <li><label>CurrentPrice</label>: <span>$325,000.00</span> </li>
                                                <li><label>List Price/SqFt</label>: <span>$96.24</span> </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <ul class="property-detail-list">
                                                <br/>
                                                <h4 class='panel-title'><strong>General Property Description</strong></h4>
                                                <br/>
                                                <li><label>PropertySubType</label>: <span>Single Family Detached</span> </li>
                                                <li><label>Governing Bodies</label>: <span>HOA</span> </li>
                                                <li><label>BedsTotal</label>: <span>4</span> </li>
                                                <li><label>BathsFull</label>: <span>5</span> </li>
                                                <li><label>BathsHalf</label>: <span>1</span> </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <ul class="property-detail-list">
                                                <br/>
                                                <h4 class='panel-title'><strong>Location, Tax & Legal</strong></h4>
                                                <br/>
                                                <li><label>MLSAreaMajor</label>: <span>4380</span> </li>
                                                <li><label>Geo Area</label>: <span>PB02</span> </li>
                                                <li><label>CountyOrParish</label>: <span>Palm Beach</span> </li>
                                                <li><label>ParcelNumber</label>: <span>00424636100090170</span> </li>
                                                <li><label>StreetNumber</label>: <span>4253</span> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="view-full" role="presentation"><a href="#fullspec" role="tab" data-toggle="tab">View Full Specifications</a></div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="location">
                                <div class="location-info property-detail-sections">
                                    <h4>Location</h4>
                                    <script src='https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCrMzetdv25NdoUlfkVQuBVUcRw4_NGXcc'></script>
                                    <div style='overflow:hidden;height:450px;width:100%;'>
                                        <div id='gmap_canvas' style='height:450px;width:100%;'></div>
                                        <div><small><a href="http://www.embedgooglemaps.com">Generate your Google map here, quick and easy!</a></small></div>
                                        <div><small><a href="https://binaireoptieservaringen.nl/">Meer informatie over binaire opties in Nederland vind je hier!</a></small></div>
                                        <style>
                                        #gmap_canvas img {
                                            max-width: none!important;
                                            background: none!important
                                        }
                                        </style>
                                    </div>
                                    <script type='text/javascript'>
                                    function init_map() {
                                        var myOptions = {
                                            zoom: 10,
                                            center: new google.maps.LatLng(26.419752, -80.11542889999998),
                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                        };
                                        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                                        marker = new google.maps.Marker({
                                            map: map,
                                            position: new google.maps.LatLng(26.419752, -80.11542889999998)
                                        });

                                        var styles = 
                                        [
                                            {
                                                "featureType": "landscape",
                                                "stylers": [
                                                    {
                                                        "hue": "#FFBB00"
                                                    },
                                                    {
                                                        "saturation": 43.400000000000006
                                                    },
                                                    {
                                                        "lightness": 37.599999999999994
                                                    },
                                                    {
                                                        "gamma": 1
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "road.highway",
                                                "stylers": [
                                                    {
                                                        "hue": "#FFC200"
                                                    },
                                                    {
                                                        "saturation": -61.8
                                                    },
                                                    {
                                                        "lightness": 45.599999999999994
                                                    },
                                                    {
                                                        "gamma": 1
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "road.arterial",
                                                "stylers": [
                                                    {
                                                        "hue": "#FF0300"
                                                    },
                                                    {
                                                        "saturation": -100
                                                    },
                                                    {
                                                        "lightness": 51.19999999999999
                                                    },
                                                    {
                                                        "gamma": 1
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "road.local",
                                                "stylers": [
                                                    {
                                                        "hue": "#FF0300"
                                                    },
                                                    {
                                                        "saturation": -100
                                                    },
                                                    {
                                                        "lightness": 52
                                                    },
                                                    {
                                                        "gamma": 1
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "water",
                                                "stylers": [
                                                    {
                                                        "hue": "#0078FF"
                                                    },
                                                    {
                                                        "saturation": -13.200000000000003
                                                    },
                                                    {
                                                        "lightness": 2.4000000000000057
                                                    },
                                                    {
                                                        "gamma": 1
                                                    }
                                                ]
                                            },
                                            {
                                                "featureType": "poi",
                                                "stylers": [
                                                    {
                                                        "hue": "#00FF6A"
                                                    },
                                                    {
                                                        "saturation": -1.0989010989011234
                                                    },
                                                    {
                                                        "lightness": 11.200000000000017
                                                    },
                                                    {
                                                        "gamma": 1
                                                    }
                                                ]
                                            }
                                        ];

                                        map.setOptions({styles: styles});

                                        // infowindow = new google.maps.InfoWindow({
                                        //     content: '<strong>My Property</strong><br>Bocaire<br>'
                                        // });
                                        google.maps.event.addListener(marker, 'click', function() {
                                            infowindow.open(map, marker);
                                        });
                                        infowindow.open(map, marker);
                                    }
                                    google.maps.event.addDomListener(window, 'load', init_map);
                                    </script>

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="fullspec">
                                <div class="property-detail-sections">
                                    <ul class="property-detail-list">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group00" data-original-title="" title=""><h4 class="panel-title">Contract Information<i class="indicator glyphicon glyphicon-chevron-down pull-right"></i></h4></a>
                                                </div>
                                                <div id="group00" class="panel-collapse collapse in">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Date Available:</strong>
                                                                <span>2015-12-01</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>ListPrice:</strong>
                                                                <span>$3,500.00</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Payment Frequency:</strong>
                                                                <span>Monthly</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>CurrentPrice:</strong>
                                                                <span>$3,500.00</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Rental Price/SqFt:</strong>
                                                                <span>$3.10</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Application Fee:</strong>
                                                                <span>100</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Damage Deposit:</strong>
                                                                <span>2000</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>For Sale:</strong>
                                                                <span>No</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group01" data-original-title="" title=""><h4 class="panel-title">General Property Description<i class="indicator glyphicon glyphicon-chevron-down pull-right"></i></h4></a>
                                                </div>
                                                <div id="group01" class="panel-collapse collapse in">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item"><strong>PropertySubType:</strong>
                                                                <span>Condo/Coop</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>BedsTotal:</strong>
                                                                <span>2</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>BathsFull:</strong>
                                                                <span>2</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>BathsHalf:</strong>
                                                                <span>0</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>BathsTotal:</strong>
                                                                <span>2</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>YearBuilt:</strong>
                                                                <span>2006</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>SqFt - Total:</strong>
                                                                <span>1256</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>LivingArea:</strong>
                                                                <span>1130</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>SqFt Source:</strong>
                                                                <span>Floor Plan</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>GarageSpaces:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Pets Allowed:</strong>
                                                                <span>Restricted</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Unit Floor #:</strong>
                                                                <span>3</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>StoriesTotal:</strong>
                                                                <span>15</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Governing Bodies:</strong>
                                                                <span>Condo</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Total Units in Bldg:</strong>
                                                                <span>130</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Min Days for Lease:</strong>
                                                                <span>120</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Ttl Units in Complex:</strong>
                                                                <span>349</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Waterfront:</strong>
                                                                <span>Yes</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Private Pool:</strong>
                                                                <span>No</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Front Exp:</strong>
                                                                <span>S</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>SubdivisionName:</strong>
                                                                <span>Marina Village at Boynton Beach Condominium</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Development Name:</strong>
                                                                <span>Marina Village</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Legal Desc:</strong>
                                                                <span>Marina Village at Boynton Beach Condominium Bldg 2 Unit 304</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>MLS #:</strong>
                                                                <span>Floor Plan</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Directions:</strong>
                                                                <span>Entrance East of Federal Hwy on Ocean Avenue, one block South of Boynton Beach Blvd.</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group02" data-original-title="" title=""><h4 class="panel-title">Location, Tax &amp; Legal<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group02" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item"><strong>MLSAreaMajor:</strong>
                                                                <span>4210</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Geo Area:</strong>
                                                                <span>PB12</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>CountyOrParish:</strong>
                                                                <span>Palm Beach</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>ParcelNumber:</strong>
                                                                <span>08434527620020304</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>StreetNumber:</strong>
                                                                <span>625</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>StreetName:</strong>
                                                                <span>Casa Loma</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>StreetSuffix:</strong>
                                                                <span>Boulevard</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>UnitNumber:</strong>
                                                                <span>304</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>City:</strong>
                                                                <span>Boynton Beach</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>StateOrProvince:</strong>
                                                                <span>FL</span>
                                                            </li>
                                                            <li class="list-group-item"><strong>PostalCode:</strong>
                                                                <span>33435</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>HOPA:</strong>
                                                                <span>No Hopa</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group04" data-original-title="" title=""><h4 class="panel-title">Status Change Info<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group04" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item"><strong>MlsStatus:</strong>
                                                                <span>Active</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Rented Price/SqFt:</strong>
                                                                <span>0</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group06" data-original-title="" title=""><h4 class="panel-title">Rental Info<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group06" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>1st Month Deposit:</strong>
                                                                <span>3850</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Last Month Deposit:</strong>
                                                                <span>3850</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Pet Fee:</strong>
                                                                <span>500</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Furn Seasonal Rent:</strong>
                                                                <span>3850</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>January:</strong>
                                                                <span>Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>February:</strong>
                                                                <span>Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>March:</strong>
                                                                <span>Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>April:</strong>
                                                                <span>Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>May:</strong>
                                                                <span>Off Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>June:</strong>
                                                                <span>Off Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>July:</strong>
                                                                <span>Off Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>August:</strong>
                                                                <span>Off Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>September:</strong>
                                                                <span>Off Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>October:</strong>
                                                                <span>Not Available</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>November:</strong>
                                                                <span>Off Seasonal</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>December:</strong>
                                                                <span>Seasonal</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group08" data-original-title="" title=""><h4 class="panel-title">Master Bedroom/Bath<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group08" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Combo Tub/Shower:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group09" data-original-title="" title=""><h4 class="panel-title">Rooms<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group09" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Great:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Laundry-Util/Closet:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group010" data-original-title="" title=""><h4 class="panel-title">Equip/Appl Included<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group010" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Auto Garage Open:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Dishwasher:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Disposal:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Dryer:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Fire Alarm:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Ice Maker:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Microwave:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Refrigerator:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Smoke Detector:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Water Heater - Elec:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group011" data-original-title="" title=""><h4 class="panel-title">Unit Desc<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group011" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Interior Hallway:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Lobby:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group012" data-original-title="" title=""><h4 class="panel-title">Tenant Pays<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group012" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Cable:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Electric:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group013" data-original-title="" title=""><h4 class="panel-title">Furnished<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group013" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Furnished:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group014" data-original-title="" title=""><h4 class="panel-title">Flooring<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group014" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Carpet:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Ceramic Tile:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group016" data-original-title="" title=""><h4 class="panel-title">Interior Features<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group016" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Built-in Shelves:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Closet Cabinets:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Foyer:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Pantry:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Roman Tub:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Split Bedroom:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Walk-in Closet:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group017" data-original-title="" title=""><h4 class="panel-title">Miscellaneous<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group017" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Assigned Parking:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Building Security:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Central A/C:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Community Pool:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Covered Parking:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Tenant Approval:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Washer / Dryer:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group018" data-original-title="" title=""><h4 class="panel-title">Parking<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group018" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Assigned:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Covered:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Garage - Building:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Guest:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Vehicle Restrictions:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group019" data-original-title="" title=""><h4 class="panel-title">Restrict<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group019" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Commercial Vehicles Prohibited:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Tenant Approval:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Up to 2 Pets:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>21 lb to 30 lb Pet:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group020" data-original-title="" title=""><h4 class="panel-title">Security<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group020" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Entry Card:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Entry Phone:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Lobby:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Security Patrol:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>TV Camera:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group021" data-original-title="" title=""><h4 class="panel-title">Subdiv. Amenities<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group021" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Bike Storage:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Business Center:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Exercise Room:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Lobby:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Pool:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Sauna:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Sidewalks:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Spa-Hot Tub:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Street Lights:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Trash Chute:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group022" data-original-title="" title=""><h4 class="panel-title">Exterior Features<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group022" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Open Balcony:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group023" data-original-title="" title=""><h4 class="panel-title">View<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group023" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Intracoastal:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Marina:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group024" data-original-title="" title=""><h4 class="panel-title">Boat Services<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group024" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Fuel:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Marina:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group027" data-original-title="" title=""><h4 class="panel-title">Heating<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group027" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Central Individual:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Electric:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group028" data-original-title="" title=""><h4 class="panel-title">Cooling<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group028" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Central Individual:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Electric:</strong>
                                                                <span>1</span>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <strong>Paddle Fans:</strong>
                                                                <span>1</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class="panel-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group032" data-original-title="" title=""><h4 class="panel-title">Storm Protection<i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4></a>
                                                </div>
                                                <div id="group032" class="panel-collapse collapse">
                                                    <div class="panel-body ">
                                                        <ul class="list-group property-detail-list">
                                                            <li class="list-group-item">
                                                                <strong>Impact Glass:</strong>
                                                                <span>Complete</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-responsive table-striped">
                                            <thead>
                                                <tr>
                                                    <td><strong>Room Name</strong></td>
                                                    <td><strong>Room Level</strong></td>
                                                    <td><strong>Dimensions</strong></td>
                                                    <td><strong>Room Remarks</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Living Room</td>
                                                    <td>N/A</td>
                                                    <td>20 X 13</td>
                                                    <td>N/A</td>
                                                </tr>
                                                <tr>
                                                    <td>Dining Area</td>
                                                    <td>N/A</td>
                                                    <td>20 X 13</td>
                                                    <td>N/A</td>
                                                </tr>
                                                <tr>
                                                    <td>Kitchen</td>
                                                    <td>N/A</td>
                                                    <td>9 X 8</td>
                                                    <td>N/A</td>
                                                </tr>
                                                <tr>
                                                    <td>Master Bedroom</td>
                                                    <td>N/A</td>
                                                    <td>14 X 12</td>
                                                    <td>N/A</td>
                                                </tr>
                                                <tr>
                                                    <td>Bedroom 2</td>
                                                    <td>N/A</td>
                                                    <td>14 X 12</td>
                                                    <td>N/A</td>
                                                </tr>
                                                <tr>
                                                    <td>Patio/Balcony</td>
                                                    <td>N/A</td>
                                                    <td>24 X 5</td>
                                                    <td>N/A</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ul>

                                </div>                                  
                            </div>
                        </div>

                        <div class="property-other-listings property-detail-sections">
                            <h4>Nearby Listings</h4>
                            <div class="sold-property-container">                                
                                <div class="other-listing-item">
                                    <div class="sold-image">
                                            <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property10.jpg" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="other-related-description">
                                    <div class="nearby-listing-type">Residential</div>
                                        <p><a href="#">The Property</a></p>
                                        <p><i class="fa fa-map-marker"></i>
                                           4 Private Drive
                                        </p>
                                        <p><i class="fa fa-usd"></i> 100,000</p>
                                    </div>
                                </div>
                                <div class="other-listing-item">
                                    <div class="sold-image">
                                            <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property11.jpg" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="other-related-description">
                                    <div class="nearby-listing-type">Residential</div>
                                        <p><a href="#">The Property</a></p>
                                        <p><i class="fa fa-map-marker"></i>
                                           4 Private Drive
                                        </p>
                                        <p><i class="fa fa-usd"></i> 100,000</p>
                                    </div>
                                </div>
                                <div class="other-listing-item">
                                    <div class="sold-image">
                                            <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property12.jpg" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="other-related-description">
                                    <div class="nearby-listing-type">Residential</div>
                                        <p><a href="#">The Property</a></p>
                                        <p><i class="fa fa-map-marker"></i>
                                           4 Private Drive
                                        </p>
                                        <p><i class="fa fa-usd"></i> 100,000</p>
                                    </div>
                                </div>
                                <div class="other-listing-item">
                                    <div class="sold-image">
                                            <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property13.jpg" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="other-related-description">
                                    <div class="nearby-listing-type">Residential</div>
                                        <p><a href="#">The Property</a></p>
                                        <p><i class="fa fa-map-marker"></i>
                                           4 Private Drive
                                        </p>
                                        <p><i class="fa fa-usd"></i> 100,000</p>
                                    </div>
                                </div>
                                <div class="other-listing-item">
                                    <div class="sold-image">
                                            <a href="#"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/property14.jpg" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="other-related-description">
                                    <div class="nearby-listing-type">Residential</div>
                                        <p><a href="#">The Property</a></p>
                                        <p><i class="fa fa-map-marker"></i>
                                           4 Private Drive
                                        </p>
                                        <p><i class="fa fa-usd"></i> 100,000</p>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('/session/footer'); ?>
