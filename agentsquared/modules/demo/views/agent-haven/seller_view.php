<?php $this->load->view('/session/header-page');  ?>

    <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <div class="visible-xs filter-button-container">
                            <button class="btn btn-default submit-button filter-search-button">
                                Show Contact
                            </button>
                        </div>
                        <div class="contact-panel">
                            <div class="filter-tab">
                                <div class="property-agent-info">
                                    <h4 class="text-center">Agent</h4>
                                    <hr>
                                    <div class="agent-image">
                                        <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/john.jpg" alt="" class="img-thumbnail">    
                                    </div>
                                    <h4 class="agent-name">John Doe</h4>
                                    <p>
                                        <i class="fa fa-phone-square"></i> Office: 888-587-7767 x 1
                                        <br>
                                        <i class="fa fa-2x fa-mobile"></i> Mobile: 561-239-2929
                                        <br>
                                        <i class="fa fa-fax"></i> Fax : 855-297-7507
                                        <br>
                                    </p>
                                    <p><i class="fa fa-envelope"></i> : <a href="">Email Me</a></p>
                                </div>
                            </div>

                            <div class="filter-tab">
                                <div class="property-submit-question">
                                    <h4 class="text-center">Have a question?</h4>
                                    <hr>
                                    <form action="">
                                        <div class="form-group">
                                            <label for="">Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Message</label>
                                            <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                        <button class="btn btn-default btn-block submit-button">Submit</button>
                                    </form> 
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 content-panel">
                        <div class="buyer-blurb">
                            <h3>Whats My Home Worth?</h3>
                            <p>
                                Receive A Custom Report Showing What Your Home Is Worth, Including Comparable Homes Recently Sold Or Are Currently On The Market.
                            </p>
                            <p>
                               To Find Out What Your Home Is Worth, Fill Out The Form Below:
                            </p>
                        </div>
                        <div class="property-details">
                            <form action="#" method="POST" class="">
                                <div class="property-information">
                                    <h4>Home Information</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Address</label>
                                                <input type="text" name="seller[address]" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Zip Code</label>
                                                <input type="text" name="seller[zip_code]" class="form-control" required="">
                                            </div>
                                        </div> 
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">No. of Bedrooms</label>
                                                <select name="seller[bedrooms]" id="" class="form-control" required="">
                                                     <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">No. of Bathrooms</label>
                                                <select name="seller[bathrooms]" id="" class="form-control" required="">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Square Feet</label>
                                                <select name="seller[square_feet]" id="" class="form-control" required="">
                                                    <option value="Less than 1000">&lt; 1000</option>
                                                    <option value="1000 - 2000">1000 - 2000</option>
                                                    <option value="2000 - 3000">2000 - 3000</option>
                                                    <option value="3000 - 4000">3000 - 4000</option>
                                                    <option value="4000 - 5000">4000 - 5000</option>
                                                    <option value="5000 - 6000">5000 - 6000</option>
                                                    <option value="6000 - 7000">6000 - 7000</option>
                                                    <option value="7000 - 8000">7000 - 8000</option>
                                                    <option value="8000 - 9000">8000 - 9000</option>
                                                    <option value="9000 - 10000">9000 - 10000</option>
                                                    <option value="Greater than 10000">&gt; 1000</option>

                                                </select>
                                            </div>
                                        </div>
                                       <!--  <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Type of Property</label>
                                                <select name="seller[property_type]" id="" class="form-control" required>
                                                    <option value="house">House</option>
                                                    <option value="condo">Condo</option>
                                                    <option value="land">Land</option>
                                                    <option value="townhouse">Townhouse</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Price Range</label>
                                                <select name="seller[price_range]" id="" class="form-control" required>
                                                    <option value="Less than $300,000">Less than $300,000</option>
                                                    <option value="$300,000 - $400,000">$300,000 - $400,000</option>
                                                    <option value="$400,000 - $500,000">$400,000 - $500,000</option>
                                                    <option value="$600,000 - $700,000">$600,000 - $700,000</option>
                                                </select>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>

                                <div class="contact-information clearfix">
                                    <h4>Contact Information</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">First Name</label>
                                                <input type="text" name="seller[fname]" class="form-control" required="">
                                            </div>    
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Last Name</label>
                                                <input type="text" name="seller[lname]" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Email</label>
                                                <input type="email" name="seller[email]" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Phone</label>
                                                <input type="text" name="seller[phone]" class="form-control" required="">
                                            </div>
                                        </div>
                                       <!--  <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Mobile</label>
                                                <input type="text" name="seller[mobile]" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Fax</label>
                                                <input type="text" name="seller[fax]" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Address</label>
                                                <input type="text" name="seller[address]" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">City</label>
                                                <input type="text" name="seller[city]" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">State</label>
                                                <input type="text" name="seller[state]" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Zip Code</label>
                                                <input type="text" name="seller[zip_code]" class="form-control" required>
                                            </div>
                                        </div> --> 
                                    </div>
                                </div>

                                <!-- <div class="moving-detail">
                                    <h4>Additional Information</h4>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">When do you plan to sell?</label>
                                            <select name="seller[when_do_you_plan_to_sell]" id="" class="form-control" required>
                                                <option value="Less than 30 days">Less than 30 days</option>
                                                <option value="1 month">1 Month</option>
                                                <option value="2 months">2 Months</option>
                                                <option value="3 months">3 Months</option>
                                                <option value="4 months">4 Months</option>
                                                <option value="5 months">5 Months</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Comments</label>
                                            <textarea name="seller[comments]" id="" cols="30" rows="10" class="form-control" required></textarea>
                                        </div>
                                        <p>Please list the additional amenities of your house.</p>
                                    </div>
                                </div> -->

                               <!--  <div class="col-md-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" required> I consent to receiving emails containing real estate related information from this site. I understand that I can unsubscribe at any time.
                                        </label>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-default submit-button">Get my home's worth</button>    
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php $this->load->view('/session/footer');  ?>