<?php
    $this->load->view($theme.'/session/header');
?>

    <section class="slider-area">        
        <div class="overlay"></div>
    </section>

    <section class="filters">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo base_url(); ?>home/listings" method="get" class="filter-property">
                        <div class="filter-tabs">
                            <h1><?php echo ($branding->banner_tagline) ? $branding->banner_tagline : "Everyone Needs a place to Call Home";?></h1>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control home-search" name="Search" placeholder="Address, City, Zip, MLS ID, Listing ID" required>
                            <span class="input-group-btn">
                                <button class="btn btn-default submit-button">Search</button>
                            </span>
                        </div>
                        <div class="help-info">
                            <div class="arrow-up"></div>
                            <p>Examples:</p>
                            <ul>
                                <?php
                                    if(isset($new_listings)) { ?>
                                        <li>
                                            <div class="col-md-4">
                                                <p>City, State</p>
                                            </div>
                                            <div class="col-md-8">
                                                <p>
                                                    <?php 
                                                        echo isset($new_listings[0]['StandardFields']['City']) ? $new_listings[0]['StandardFields']['City'] . ", ": "" ;
                                                        echo isset($new_listings[0]['StandardFields']['StateOrProvince']) ? $new_listings[0]['StandardFields']['StateOrProvince'] : "" ; 
                                                    ?>
                                                </p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-md-4">
                                                <p>Zip Code</p>
                                            </div>
                                            <div class="col-md-8">
                                                <p><?php echo isset($new_listings[0]['StandardFields']['PostalCode']) ? $new_listings[0]['StandardFields']['PostalCode'] : "" ; ?></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-md-4">
                                                <p>Address</p>
                                            </div>
                                            <div class="col-md-8">
                                                <p><?php echo isset($new_listings[0]['StandardFields']['UnparsedAddress']) ? $new_listings[0]['StandardFields']['UnparsedAddress'] : "" ; ?></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-md-4">
                                                <p>County</p>
                                            </div>
                                            <div class="col-md-8">
                                                <p><?php echo isset($new_listings[0]['StandardFields']['CountyOrParish']) ? $new_listings[0]['StandardFields']['CountyOrParish'] : "" ; ?></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-md-4">
                                                <p>Street Address</p>
                                            </div>
                                            <div class="col-md-8">
                                                <p><?php echo isset($new_listings[0]['StandardFields']['UnparsedFirstLineAddress']) ? $new_listings[0]['StandardFields']['UnparsedFirstLineAddress'] : "" ; ?></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-md-4">
                                                <p>Listing ID</p>
                                            </div>
                                            <div class="col-md-8">
                                                <p><?php echo isset($new_listings[0]['StandardFields']['ListingId']) ? $new_listings[0]['StandardFields']['ListingId'] : "" ; ?></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-md-4">
                                                <p>MLS ID</p>
                                            </div>
                                            <div class="col-md-8">
                                                <p><?php echo isset($new_listings[0]['StandardFields']['MlsId']) ? $new_listings[0]['StandardFields']['MlsId'] : "" ; ?></p>
                                            </div>
                                        </li>
                                <?php    
                                    }
                                ?>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--##### Featured Listing Section #####-->
 <?php 
    if(isset($is_featured_selected) AND !empty($is_featured_selected)){
        $countF = 0;
        if(isset($json_property) AND !empty($json_property)) {
            foreach($json_property as $feature) {
                if(isset($feature->MlsStatus) && !empty($feature->MlsStatus)) {
                    if( $feature->MlsStatus == "Active" || $feature->MlsStatus == "Contingent" || $feature->MlsStatus == "Pending" ) {
                        $countF++;
                    }
                }
                
            }
            if($countF > 0) { ?>
                <section class="featured-listing-area">
                    <div class="container">
                        <div class="row">
                            <div class="featured-title-container">
                                <div class="trapezoid"></div>
                                <h2 class="text-center section-title"><?php echo $featured_title['option_title'];?></h2>
                            </div>
                            <div class="col-md-12">
                                <div class="featured-list">

                                <?php
                                    //Fetch Active Property on Property Listing Data
                                    if(isset($json_property) AND !empty($json_property)) {
                                        $itemCount = count($json_property);
                                        if($itemCount > 4) {
                                            $itemCount = 4;
                                        }
                                        $iCount = 0;
                                        $items = 12 / $itemCount;
                                        $totalListingCount = 0;
                                        foreach($json_property as $feature) {
                                            if(isset($feature->MlsStatus) && !empty($feature->MlsStatus)) {
                                                if( $feature->MlsStatus == "Active" || $feature->MlsStatus == "Contingent" || $feature->MlsStatus == "Pending" ){
                                                    if($iCount < $itemCount) {
                                                        $totalListingCount++;
                                                    }
                                                    $iCount++;
                                                }
                                            }
                                        }
                                        $iCount = 0;
                                        $items = 12 / $totalListingCount;
                                        foreach($json_property as $feature) {
                                            if(isset($feature->MlsStatus) && !empty($feature->MlsStatus)) {
                                            if( $feature->MlsStatus == "Active" || $feature->MlsStatus == "Contingent" || $feature->MlsStatus == "Pending" ){
                                                if($iCount < $itemCount) {
                                ?>
                                                     <div class="col-md-3 col-sm-3  featured-list-item item-<?=$itemCount?>-<?=$iCount?>-<?=$totalListingCount?>">
                                                        <div class="property-image <?php if($feature->PropertyClass == 'Land' || $feature->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                          <?php if(isset($feature->Photos[0]->Uri300)) { ?>
                                                                 <a href="<?= base_url();?>home/featured_property?listingId=<?= $feature->ListingKey; ?>"><img src="<?=$feature->Photos[0]->Uri300?>" alt="<?php echo $feature->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;"></a>
                                                             <?php } else { ?>
                                                                 <a href="<?= base_url();?>home/featured_property?listingId=<?= $feature->ListingKey; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $feature->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;"></a>
                                                             <?php } ?>
                                                        </div>
                                                        <div class="property-listing-price">
                                                            <div class="property-listing-type">
                                                                <?=$feature->PropertyClass;?>
                                                            </div>
                                                            $<?=number_format($feature->CurrentPrice);?>
                                                        </div>
                                                        <div class="property-quick-icons <?php if($feature->PropertyClass == 'Land' || $feature->PropertyClass == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                            <ul class="list-inline">
                                                                <?php
                                                                    if($feature->BedsTotal && is_numeric($feature->BedsTotal)) {
                                                                ?>
                                                                <li><i class="fa fa-bed"></i> <?=$feature->BedsTotal?> Bed</li>
                                                                <?php
                                                                    }
                                                                    if($feature->BathsTotal && is_numeric($feature->BathsTotal)) {
                                                                ?>
                                                                <li><i class="icon-toilet"></i> <?=$feature->BathsTotal?> Bath</li>
                                                                <?php
                                                                    }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                        <div class="property-listing-description">
                                                            <p>
                                                                <a class="listing-link" href="<?= base_url();?>home/featured_property?listingId=<?= $feature->ListingKey; ?>">
                                                                    <b><?php echo $feature->UnparsedFirstLineAddress; ?></b>
                                                                </a>
                                                            </p>
                                                            <p>
                                                           <?php
                                                                $mystring = $feature->City;
                                                                $findme   = '*';
                                                                $pos = strpos($mystring, $findme);

                                                                if($pos === false)
                                                                    echo $feature->City . ", " . $feature->StateOrProvince . " " . $feature->PostalCode;
                                                                else
                                                                    echo $feature->PostalCity . ", " . $feature->StateOrProvince . " " . $feature->PostalCode;
                                                            ?>
                                                            </p>    
                                                        </div>
                                                    </div>
                                    <?php 
                                                }
                                                $iCount++;

                                               ?>

                                    <?php           
                                                }
                                            }
                                        }

                                                //add 4 Office Listings
                                                if($iCount == 0){
                                                    if(isset($office_listings) AND !empty($office_listings)){
                                                        $count1 = 0;
                                                        foreach($office_listings as $office){
                                                            //printA($office);exit;
                                                            if($count1++ >= 4) break; ?>

                                                            <div class="col-md-3 col-sm-3  featured-list-item">
                                                                <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                  <?php if(isset($office['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                                        <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey'];?>"><img src="
                                                                        <?=$office['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;"></a>
                                                                     <?php } else { ?>
                                                                         <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                                     <?php } ?>
                                                                </div>
                                                                <div class="property-listing-price">
                                                                    <div class="property-listing-type">
                                                                        <?=$office['StandardFields']['PropertyClass'];?>
                                                                    </div>
                                                                    $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                                </div>
                                                                <div class="property-quick-icons <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                                    <ul class="list-inline">
                                                                        <?php
                                                                            if($office['StandardFields']['BedsTotal'] && is_numeric($office['StandardFields']['BedsTotal'])) {
                                                                        ?>
                                                                        <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                        <?php
                                                                            }
                                                                            if($office['StandardFields']['BathsTotal'] && is_numeric($office['StandardFields']['BathsTotal'])) {
                                                                        ?>
                                                                        <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                    </ul>
                                                                </div>
                                                                <div class="property-listing-description">
                                                                    <p>
                                                                        <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>">
                                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                        </a>
                                                                    </p>
                                                                    <p>
                                                                   <?php
                                                                        $mystring = $office['StandardFields']['City'];
                                                                        $findme   = '*';
                                                                        $pos = strpos($mystring, $findme);

                                                                        if($pos === false)
                                                                            echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                        else
                                                                            echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                    ?>
                                                                    </p>    
                                                                </div>
                                                            </div>           
                                                <?php  
                                                        }
                                                        $count1++;
                                                    } 
                                                ?>
                                             
                                                <!--add 3 Office Listings-->
                                                <?php } elseif($iCount == 1){

                                                     //if(!empty($office_listings[0]['StandardFields']['ListAgentId']) AND !empty($json_property[1]->ListAgentId)){

                                                         if($office_listings[0]['StandardFields']['ListAgentId'] === $json_property[1]->ListAgentId){

                                                                $count2 = 0;

                                                                if(isset($nearby_listings) AND !empty($nearby_listings)){

                                                                foreach(array_slice($nearby_listings,1) as $nearby){
                                                                    //printA($nearby);exit;

                                                                    if($count2++ >= 3) break; ?>

                                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                                        <div class="property-image <?php if($nearby['StandardFields']['PropertyClass'] == 'Land' || $nearby['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                          <?php if(isset($nearby['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey'];?>"><img src="
                                                                                <?=$nearby['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $nearby['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } else { ?>
                                                                                 <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } ?>
                                                                        </div>
                                                                        <div class="property-listing-price">
                                                                            <div class="property-listing-type">
                                                                                <?=$nearby['StandardFields']['PropertyClass'];?>
                                                                            </div>
                                                                            $<?=number_format($nearby['StandardFields']['CurrentPrice']);?>
                                                                        </div>
                                                                        <div class="property-quick-icons <?php if($nearby['StandardFields']['PropertyClass'] == 'Land' || $nearby['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                                            <ul class="list-inline">
                                                                                <?php
                                                                                    if($nearby['StandardFields']['BedsTotal'] && is_numeric($nearby['StandardFields']['BedsTotal'])) {
                                                                                ?>
                                                                                <li><i class="fa fa-bed"></i> <?=$nearby['StandardFields']['BedsTotal']?> Bed</li>
                                                                                <?php
                                                                                    }
                                                                                    if($nearby['StandardFields']['BathsTotal'] && is_numeric($nearby['StandardFields']['BathsTotal'])) {
                                                                                ?>
                                                                                <li><i class="icon-toilet"></i> <?=$nearby['StandardFields']['BathsTotal']?> Bath</li>
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="property-listing-description">
                                                                            <p>
                                                                                <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>">
                                                                                    <b><?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                           <?php
                                                                                $mystring = $nearby['StandardFields']['City'];
                                                                                $findme   = '*';
                                                                                $pos = strpos($mystring, $findme);

                                                                                if($pos === false)
                                                                                    echo $nearby['StandardFields']['City'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                                                else
                                                                                    echo $nearby['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                                            ?>
                                                                            </p>    
                                                                        </div>
                                                                    </div>       
                                                        <?php 
                                                                }
                                                            
                                                                $count2++;
                                                            }
                                                        //}
                                                        ?>

                                                        <?php  } else { ?>

                                                           <?php if(isset($office_listings) AND !empty($office_listings)){
                                                                    $count2 = 0;
                                                                foreach($office_listings as $office){

                                                                    //printA($office);exit;
                                                                    if($count2++ >= 3) break; ?>

                                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                                        <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                          <?php if(isset($office['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey'];?>"><img src="
                                                                                <?=$office['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } else { ?>
                                                                                 <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } ?>
                                                                        </div>
                                                                        <div class="property-listing-price">
                                                                            <div class="property-listing-type">
                                                                                <?=$office['StandardFields']['PropertyClass'];?>
                                                                            </div>
                                                                            $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                                        </div>
                                                                        <div class="property-quick-icons <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                                            <ul class="list-inline">
                                                                                <?php
                                                                                    if($office['StandardFields']['BedsTotal'] && is_numeric($office['StandardFields']['BedsTotal'])) {
                                                                                ?>
                                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                                <?php
                                                                                    }
                                                                                    if($office['StandardFields']['BathsTotal'] && is_numeric($office['StandardFields']['BathsTotal'])) {
                                                                                ?>
                                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="property-listing-description">
                                                                            <p>
                                                                                <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>">
                                                                                    <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                           <?php
                                                                                $mystring = $office['StandardFields']['City'];
                                                                                $findme   = '*';
                                                                                $pos = strpos($mystring, $findme);

                                                                                if($pos === false)
                                                                                    echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                                else
                                                                                    echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                            ?>
                                                                            </p>    
                                                                        </div>
                                                                    </div>       
                                                        <?php 
                                                                    }
                                                                    $count2++;
                                                                }
                                                            }
                                                        ?>
                                                   
                                                <!--add 2 Office Listings-->
                                                <?php } elseif($iCount == 2){

                                                     //if(!empty($office_listings[0]['StandardFields']['ListAgentId']) AND !empty($json_property[1]->ListAgentId)){

                                                         if($office_listings[0]['StandardFields']['ListAgentId'] === $json_property[1]->ListAgentId){

                                                                $count3 = 0;

                                                                if(isset($nearby_listings) AND !empty($nearby_listings)){

                                                                foreach(array_slice($nearby_listings,1) as $nearby){
                                                                    //printA($nearby);exit;

                                                                    if($count3++ >= 2) break; ?>

                                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                                        <div class="property-image <?php if($nearby['StandardFields']['PropertyClass'] == 'Land' || $nearby['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                          <?php if(isset($nearby['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey'];?>"><img src="
                                                                                <?=$nearby['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $nearby['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } else { ?>
                                                                                 <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } ?>
                                                                        </div>
                                                                        <div class="property-listing-price">
                                                                            <div class="property-listing-type">
                                                                                <?=$nearby['StandardFields']['PropertyClass'];?>
                                                                            </div>
                                                                            $<?=number_format($nearby['StandardFields']['CurrentPrice']);?>
                                                                        </div>
                                                                        <div class="property-quick-icons <?php if($nearby['StandardFields']['PropertyClass'] == 'Land' || $nearby['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                                            <ul class="list-inline">
                                                                                <?php
                                                                                    if($nearby['StandardFields']['BedsTotal'] && is_numeric($nearby['StandardFields']['BedsTotal'])) {
                                                                                ?>
                                                                                <li><i class="fa fa-bed"></i> <?=$nearby['StandardFields']['BedsTotal']?> Bed</li>
                                                                                <?php
                                                                                    }
                                                                                    if($nearby['StandardFields']['BathsTotal'] && is_numeric($nearby['StandardFields']['BathsTotal'])) {
                                                                                ?>
                                                                                <li><i class="icon-toilet"></i> <?=$nearby['StandardFields']['BathsTotal']?> Bath</li>
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="property-listing-description">
                                                                            <p>
                                                                                <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>">
                                                                                    <b><?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                           <?php
                                                                                $mystring = $nearby['StandardFields']['City'];
                                                                                $findme   = '*';
                                                                                $pos = strpos($mystring, $findme);

                                                                                if($pos === false)
                                                                                    echo $nearby['StandardFields']['City'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                                                else
                                                                                    echo $nearby['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                                            ?>
                                                                            </p>    
                                                                        </div>
                                                                    </div>       
                                                        <?php 
                                                                }
                                                            
                                                                $count3++;
                                                            }
                                                       // }
                                                        ?>

                                                        <?php  } else { ?>

                                                           <?php if(isset($office_listings) AND !empty($office_listings)){
                                                                    $count3 = 0;
                                                                foreach($office_listings as $office){

                                                                    //printA($office);exit;
                                                                    if($count3++ >= 2) break; ?>

                                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                                        <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                          <?php if(isset($office['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey'];?>"><img src="
                                                                                <?=$office['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } else { ?>
                                                                                 <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } ?>
                                                                        </div>
                                                                        <div class="property-listing-price">
                                                                            <div class="property-listing-type">
                                                                                <?=$office['StandardFields']['PropertyClass'];?>
                                                                            </div>
                                                                            $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                                        </div>
                                                                        <div class="property-quick-icons <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                                            <ul class="list-inline">
                                                                                <?php
                                                                                    if($office['StandardFields']['BedsTotal'] && is_numeric($office['StandardFields']['BedsTotal'])) {
                                                                                ?>
                                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                                <?php
                                                                                    }
                                                                                    if($office['StandardFields']['BathsTotal'] && is_numeric($office['StandardFields']['BathsTotal'])) {
                                                                                ?>
                                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="property-listing-description">
                                                                            <p>
                                                                                <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>">
                                                                                   <b> <?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                           <?php
                                                                                $mystring = $office['StandardFields']['City'];
                                                                                $findme   = '*';
                                                                                $pos = strpos($mystring, $findme);

                                                                                if($pos === false)
                                                                                    echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                                else
                                                                                    echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                            ?>
                                                                            </p>    
                                                                        </div>
                                                                    </div>       
                                                        <?php 
                                                                    }
                                                                    $count3++;
                                                                }
                                                            }
                                                        ?>

                                                <!--add 1 Office Listings-->
                                                <?php } elseif($iCount == 3){

                                                     //if(!empty($office_listings[0]['StandardFields']['ListAgentId']) AND !empty($json_property[1]->ListAgentId)){

                                                            if($office_listings[1]['StandardFields']['ListAgentId'] === $json_property[1]->ListAgentId){
                                                            
                                                                
                                                                $count4 = 0;

                                                                if(isset($nearby_listings) AND !empty($nearby_listings)){

                                                                foreach(array_slice($nearby_listings,1) as $nearby){
                                                                    //printA($nearby);exit;

                                                                    if($count4++ >= 1) break; ?>

                                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                                        <div class="property-image <?php if($nearby['StandardFields']['PropertyClass'] == 'Land' || $nearby['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                          <?php if(isset($nearby['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey'];?>"><img src="
                                                                                <?=$nearby['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $nearby['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } else { ?>
                                                                                 <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } ?>
                                                                        </div>
                                                                        <div class="property-listing-price">
                                                                            <div class="property-listing-type">
                                                                                <?=$nearby['StandardFields']['PropertyClass'];?>
                                                                            </div>
                                                                            $<?=number_format($nearby['StandardFields']['CurrentPrice']);?>
                                                                        </div>
                                                                        <div class="property-quick-icons <?php if($nearby['StandardFields']['PropertyClass'] == 'Land' || $nearby['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                                            <ul class="list-inline">
                                                                                <?php
                                                                                    if($nearby['StandardFields']['BedsTotal'] && is_numeric($nearby['StandardFields']['BedsTotal'])) {
                                                                                ?>
                                                                                <li><i class="fa fa-bed"></i> <?=$nearby['StandardFields']['BedsTotal']?> Bed</li>
                                                                                <?php
                                                                                    }
                                                                                    if($nearby['StandardFields']['BathsTotal'] && is_numeric($nearby['StandardFields']['BathsTotal'])) {
                                                                                ?>
                                                                                <li><i class="icon-toilet"></i> <?=$nearby['StandardFields']['BathsTotal']?> Bath</li>
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="property-listing-description">
                                                                            <p>
                                                                                <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>">
                                                                                    <b><?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                           <?php
                                                                                $mystring = $nearby['StandardFields']['City'];
                                                                                $findme   = '*';
                                                                                $pos = strpos($mystring, $findme);

                                                                                if($pos === false)
                                                                                    echo $nearby['StandardFields']['City'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                                                else
                                                                                    echo $nearby['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                                            ?>
                                                                            </p>    
                                                                        </div>
                                                                    </div>       
                                                        <?php 
                                                                }
                                                            
                                                                $count4++;
                                                            }
                                                        //}
                                                        ?>

                                                        <?php  } else { ?>

                                                           <?php if(isset($office_listings) AND !empty($office_listings)){
                                                                    $count4 = 0;
                                                                    
                                                                foreach($office_listings as $office){
                                                                    //printA($office);exit;

                                                                    if($count4++ >= 1) break; ?>

                                                                    <div class="col-md-3 col-sm-3  featured-list-item">
                                                                        <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                                          <?php if(isset($office['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey'];?>"><img src="
                                                                                <?=$office['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } else { ?>
                                                                                 <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                                             <?php } ?>
                                                                        </div>
                                                                        <div class="property-listing-price">
                                                                            <div class="property-listing-type">
                                                                                <?=$office['StandardFields']['PropertyClass'];?>
                                                                            </div>
                                                                            $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                                        </div>
                                                                        <div class="property-quick-icons <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                                            <ul class="list-inline">
                                                                                <?php
                                                                                    if($office['StandardFields']['BedsTotal'] && is_numeric($office['StandardFields']['BedsTotal'])) {
                                                                                ?>
                                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                                                <?php
                                                                                    }
                                                                                    if($office['StandardFields']['BathsTotal'] && is_numeric($office['StandardFields']['BathsTotal'])) {
                                                                                ?>
                                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="property-listing-description">
                                                                            <p>
                                                                                <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>">
                                                                                    <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                           <?php
                                                                                $mystring = $office['StandardFields']['City'];
                                                                                $findme   = '*';
                                                                                $pos = strpos($mystring, $findme);

                                                                                if($pos === false)
                                                                                    echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                                else
                                                                                    echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                            ?>
                                                                            </p>    
                                                                        </div>
                                                                    </div>       
                                                        <?php 
                                                                    }
                                                                    $count4++;
                                                                }
                                                            }
                                                        ?>

                                                <?php } else {?>

                                                    <!--Do Nothing-->

                                                <?php }?>   
                                <?php    
                                        }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                        
    <?php   }
        }
    }
    ?>

    <?php
     //Office Listing Section 
    if(isset($is_office_selected)){
        if(isset($office_listings) AND !empty($office_listings)){?>

        <section class="new-listing-area">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">
                        <h2 class="other-listing-title text-center"><?php echo $office_title['option_title'];?></h2>
                    </div>
                    <div class="col-md-12">
                        <div class="featured-list">
                          <?php

                                $count = 0;

                               foreach( $office_listings as $office){
    
                                    if($count < 4) { ?>
                                <div class="col-md-3 col-sm-3 featured-list-item">
                                    <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                        <?php if(isset($office['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>"><img src="<?=$office['StandardFields']['Photos'][0]['Uri300']?>" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                        <?php } else { ?>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                        <?php } ?>
                                    </div>
                                   <div class="property-listing-price">
                                        <div class="property-listing-type">
                                            <?=$office['StandardFields']['PropertyClass'];?>
                                        </div>
                                        $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                    </div>
                                    <div class="property-quick-icons <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                        <ul class="list-inline">
                                            <?php
                                               if($office['StandardFields']['BedsTotal'] && is_numeric($office['StandardFields']['BedsTotal'])) {
                                            ?>
                                            <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                            <?php
                                                }
                                                if($office['StandardFields']['BathsTotal'] && is_numeric($office['StandardFields']['BathsTotal'])) {
                                            ?>
                                             <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                            <?php
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="property-listing-description">
                                        <p>
                                            <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $office['StandardFields']['ListingKey']; ?>">
                                                <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                            </a>
                                        </p>
                                        <p>
                                          <?php
                                                $mystring = $office['StandardFields']['City'];
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);

                                            if($pos === false)
                                                echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                else
                                                echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                            ?>
                                        </p>    
                                    </div>
                                </div>

                        <?php  } $count++;
                            } 
                        ?> 
                        </div>

                    </div>
                </div>
            </div>
    <?php  } 
        }
    ?>

    <!--##### New Listing and Nearby Listing Section #####-->
    <?php if(isset($is_new_selected) && isset($is_nearby_selected)){?>
    <section class="other-listing-area">
        <div class="container">
            <div class="row">
                <?php
                    if(isset($new_listings) AND !empty($new_listings)){?>
                    <div class="col-md-6">
                        <div class="new-listings-area">
                            <h2 class="other-listing-title"><?php echo $new_title['option_title'];?>
                                <?php if(isset($new_listings) AND !empty($new_listings)){?>
                                     <span class="viewall-newlisting">
                                        <a href="<?php echo site_url('home/listings?Search=&listing_change_type%5B%5D=New+Listing'); ?>" target="_blank">View All</a>    
                                    </span>
                                <?php }?>
                            </h2>
                            
                            <?php 
                               $count = 0;

                               foreach($new_listings as $new_listing){

                                    if($new_listing['StandardFields']['MajorChangeType'] == 'New Listing'){

                                        if($count < 4) { ?>
                                            <div class="col-md-6 featured-list-item">
                                                <div class="property-image <?php if($new_listing['StandardFields']['PropertyClass'] == 'Land' || $new_listing['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                     <?php if(isset($new_listing['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                        <a href="<?= base_url();?>home/other_property?listingId=<?= $new_listing['StandardFields']['ListingKey']; ?>"><img src="<?=$new_listing['StandardFields']['Photos'][0]['Uri300']?>" alt="<?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                    <?php } else { ?>
                                                        <a href="<?= base_url();?>home/other_property?listingId=<?= $new_listing['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                    <?php } ?>">
                                                    <div class="property-listing-description">
                                                        <p>
                                                            <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $new_listing['StandardFields']['ListingKey']; ?>">
                                                                <b><?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                            </a>
                                                        </p>
                                                        <p>
                                                          <?php
                                                                $mystring = $new_listing['StandardFields']['City'];
                                                                $findme   = '*';
                                                                $pos = strpos($mystring, $findme);

                                                            if($pos === false)
                                                                echo $new_listing['StandardFields']['City'] . ", " . $new_listing['StandardFields']['StateOrProvince'] . " " . $new_listing['StandardFields']['PostalCode'];
                                                                else
                                                                echo $new_listing['StandardFields']['PostalCity'] . ", " . $new_listing['StandardFields']['StateOrProvince'] . " " . $new_listing['StandardFields']['PostalCode'];
                                                            ?>
                                                        </p>    
                                                    </div>
                                                </div>
                                                <div class="property-listing-price">
                                                    <div class="property-listing-type">
                                                       <?=$new_listing['StandardFields']['PropertyClass'];?>
                                                    </div>
                                                    $<?=number_format($new_listing['StandardFields']['CurrentPrice']);?>
                                                </div>
                                                <div class="property-quick-icons <?php if($new_listing['StandardFields']['PropertyClass'] == 'Land' || $new_listing['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                    <ul class="list-inline">
                                                       <?php
                                                           if($new_listing['StandardFields']['BedsTotal'] && is_numeric($new_listing['StandardFields']['BedsTotal'])) {
                                                        ?>
                                                        <li><i class="fa fa-bed"></i> <?=$new_listing['StandardFields']['BedsTotal']?> Bed</li>
                                                        <?php
                                                            }
                                                            if($new_listing['StandardFields']['BathsTotal'] && is_numeric($new_listing['StandardFields']['BathsTotal'])) {
                                                        ?>
                                                         <li><i class="icon-toilet"></i> <?=$new_listing['StandardFields']['BathsTotal']?> Bath</li>
                                                        <?php
                                                            }
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div> 
                                        <?php  } $count++;  
                                    } 
                                } ?>
                        </div>
                    </div>
            <?php } ?> 

                <?php
                   if(isset($nearby_listings) AND !empty($nearby_listings)){?>
                    <div class="col-md-6">
                        <div class="nearby-listings-area">
                            <h2 class="other-listing-title"><?php echo $nearby_title['option_title'];?></h2>
                            <ul>
                            <?php 

                                $count = 0;
                                    foreach(array_slice($nearby_listings,1) as $nearby){
                                        if($count < 4) { ?>
                                            <li>
                                                <div class="col-md-4  col-sm-6 no-padding-left">
                                                    <div class="nearby-image">
                                                       <?php if(isset($nearby['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey'];?>"><img src="
                                                                <?=$nearby['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $nearby['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;"></a>
                                                        <?php } else { ?>
                                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby['StandardFields']['UnparsedFirstLineAddress']; ?> class="img-responsive" style="width:100%;"></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="nearby-description clearfix">
                                                    <div class="col-md-8 col-sm-6">
                                                        <h4>
                                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>">
                                                                <b><?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                            </a>
                                                        </h4>
                                                        <p>
                                                          <?php
                                                                $mystring = $nearby['StandardFields']['City'];
                                                                $findme   = '*';
                                                                $pos = strpos($mystring, $findme);

                                                            if($pos === false)
                                                                echo $nearby['StandardFields']['City'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                                else
                                                                echo $nearby['StandardFields']['PostalCity'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                            ?>
                                                        </p>    
                                                        <ul class="list-inline">
                                                            <?php
                                                                if($nearby['StandardFields']['BedsTotal'] && is_numeric($nearby['StandardFields']['BedsTotal'])) {
                                                            ?>
                                                                <li><i class="fa fa-bed"></i> <?=$nearby['StandardFields']['BedsTotal']?> Bed</li>
                                                            <?php
                                                            }
                                                                if($nearby['StandardFields']['BathsTotal'] && is_numeric($nearby['StandardFields']['BathsTotal'])) {
                                                            ?>
                                                                <li><i class="icon-toilet"></i> <?=$nearby['StandardFields']['BathsTotal']?> Bath</li>
                                                            <?php
                                                                }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } $count++; 
                                    }?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </section>
    <?php } elseif(isset($is_new_selected)){

        //New Listing Section #
        if(isset($new_listings) AND !empty($new_listings)){?>

        <section class="new-listing-area">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">
                        <h2 class="other-listing-title text-center"><?php echo $new_title['option_title'];?></h2>
                         <?php if(isset($new_listings) AND !empty($new_listings)){?>
                            <p class="viewall">
                                <a href="<?php echo site_url('home/listings?Search=&listing_change_type%5B%5D=New+Listing'); ?>" target="_blank">View All</a>    
                            </p>
                        <?php }?>
                    </div>
                    <div class="col-md-12">
                        <div class="featured-list">
                          <?php

                                $count = 0;

                               foreach($new_listings as $new_listing){

                                if($new_listing['StandardFields']['MajorChangeType'] == 'New Listing'){
    
                                    if($count < 4) { ?>
                                <div class="col-md-3 col-sm-3 featured-list-item">
                                    <div class="property-image <?php if($new_listing['StandardFields']['PropertyClass'] == 'Land' || $new_listing['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                        <?php if(isset($new_listing['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $new_listing['StandardFields']['ListingKey']; ?>"><img src="<?=$new_listing['StandardFields']['Photos'][0]['Uri300']?>" alt="<?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                        <?php } else { ?>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $new_listing['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                        <?php } ?>
                                    </div>
                                   <div class="property-listing-price">
                                        <div class="property-listing-type">
                                            <?=$new_listing['StandardFields']['PropertyClass'];?>
                                        </div>
                                        $<?=number_format($new_listing['StandardFields']['CurrentPrice']);?>
                                    </div>
                                    <div class="property-quick-icons <?php if($new_listing['StandardFields']['PropertyClass'] == 'Land' || $new_listing['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                        <ul class="list-inline">
                                            <?php
                                               if($new_listing['StandardFields']['BedsTotal'] && is_numeric($new_listing['StandardFields']['BedsTotal'])) {
                                            ?>
                                            <li><i class="fa fa-bed"></i> <?=$new_listing['StandardFields']['BedsTotal']?> Bed</li>
                                            <?php
                                                }
                                                if($new_listing['StandardFields']['BathsTotal'] && is_numeric($new_listing['StandardFields']['BathsTotal'])) {
                                            ?>
                                             <li><i class="icon-toilet"></i> <?=$new_listing['StandardFields']['BathsTotal']?> Bath</li>
                                            <?php
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="property-listing-description">
                                        <p>
                                            <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $new_listing['StandardFields']['ListingKey']; ?>">
                                                <b><?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                            </a>
                                        </p>
                                        <p>
                                          <?php
                                                $mystring = $new_listing['StandardFields']['City'];
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);

                                            if($pos === false)
                                                echo $new_listing['StandardFields']['City'] . ", " . $new_listing['StandardFields']['StateOrProvince'] . " " . $new_listing['StandardFields']['PostalCode'];
                                                else
                                                echo $new_listing['StandardFields']['PostalCity'] . ", " . $new_listing['StandardFields']['StateOrProvince'] . " " . $new_listing['StandardFields']['PostalCode'];
                                            ?>
                                        </p>    
                                    </div>
                                </div>

                        <?php  } $count++;

                             } 

                            } 
                        ?> 
                        </div>

                    </div>
                </div>
            </div>
    <?php  } 

        } elseif(isset($is_nearby_selected)){

        //Nearby Listing Section 
        if(isset($nearby_listings) AND !empty($nearby_listings)){?>

        <section class="new-listing-area">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">
                        <h2 class="other-listing-title text-center"><?php echo $nearby_title['option_title'];?></h2>
                    </div>
                    <div class="col-md-12">
                        <div class="featured-list">
                          <?php

                                $count = 0;

                               foreach(array_slice($nearby_listings,1) as $nearby){
    
                                    if($count < 4) { ?>
                                <div class="col-md-3 col-sm-3 featured-list-item">
                                    <div class="property-image <?php if($nearby['StandardFields']['PropertyClass'] == 'Land' || $nearby['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                        <?php if(isset($nearby['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>"><img src="<?=$nearby['StandardFields']['Photos'][0]['Uri300']?>" alt="<?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                        <?php } else { ?>
                                            <a href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                        <?php } ?>
                                    </div>
                                   <div class="property-listing-price">
                                        <div class="property-listing-type">
                                            <?=$nearby['StandardFields']['PropertyClass'];?>
                                        </div>
                                        $<?=number_format($nearby['StandardFields']['CurrentPrice']);?>
                                    </div>
                                    <div class="property-quick-icons <?php if($nearby['StandardFields']['PropertyClass'] == 'Land' || $nearby['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                        <ul class="list-inline">
                                            <?php
                                               if($nearby['StandardFields']['BedsTotal'] && is_numeric($nearby['StandardFields']['BedsTotal'])) {
                                            ?>
                                            <li><i class="fa fa-bed"></i> <?=$nearby['StandardFields']['BedsTotal']?> Bed</li>
                                            <?php
                                                }
                                                if($nearby['StandardFields']['BathsTotal'] && is_numeric($nearby['StandardFields']['BathsTotal'])) {
                                            ?>
                                             <li><i class="icon-toilet"></i> <?=$nearby['StandardFields']['BathsTotal']?> Bath</li>
                                            <?php
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="property-listing-description">
                                        <p>
                                            <a class="listing-link" href="<?= base_url();?>home/other_property?listingId=<?= $nearby['StandardFields']['ListingKey']; ?>">
                                                <b><?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                            </a>
                                        </p>
                                        <p>
                                          <?php
                                                $mystring = $nearby['StandardFields']['City'];
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);

                                            if($pos === false)
                                                echo $nearby['StandardFields']['City'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                else
                                                echo $nearby['StandardFields']['PostalCity'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                            ?>
                                        </p>    
                                    </div>
                                </div>

                        <?php  } $count++;
                            } 
                        ?> 
                        </div>

                    </div>
                </div>
            </div>
    <?php  } 
        } 
    ?>


    <!--##### New Save Searched Section #####-->
    <?php

    if(isset($is_saved_search_selected) AND !empty($is_saved_search_selected)){

      if(isset($custom_saved_searches) AND !empty($custom_saved_searches)){?>


            <div class="save-search-slider" style="margin-top:50px;">
                <div class="container">
                    <div class="row">
                        <div id="save-search-grid" class="owl-carousel owl-theme">

                        <?php
                            $ncount   = 0;
                            $scount   = 0;
                            $imgcount = 0;

                            foreach($custom_saved_searches as $savedsearch){
                                    $savesearchcount = count($savedsearch);
                            ?>
                            <?php if($savesearchcount): 

                                if ($savesearchcount == 1) {
                                    $x = 12;
                                    $class = "one-property";
                                } elseif ($savesearchcount == 3) {
                                    $x = 6;
                                    $class = "three-property";
                                } else {
                                    $x = 6;
                                    $class = "";
                                }
                            ?>
                          <div class="item <?php echo $class; ?>" style="background: linear-gradient( rgba(125, 125, 125, 0.45), rgba(123, 130, 130, 0.63) ), url(<?php echo $search_img[$imgcount++] ;?>);">
                              <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                                  <div class="row">
                                      <div class="col-md-12">
                                         
                                           <div class="save-search-desc">
                                              <h1 class="save-search-name"><?php echo $search_name[$ncount++] ;?></h1>

                                              <?php if(isset($saved) AND !empty($saved)){
                                                   if (count($saved) > 4){  ?>
                                                         
                                                    <a href="<?php echo base_url()?>home/popular_searches/<?php echo $search_id[$scount++];?>" class="btn btn-view" target="_blank" >View All</a>   
                                            <?php }
                                                    }?>
                                           </div> 
                                      </div>
                                  </div>    
                              </div>
                              <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                                    <?php
                                        if(isset($savedsearch) AND !empty($savedsearch)){
                                            $count = 0;
                                           foreach($savedsearch as $saved){
                                            //printA($savedsearch);exit;
                                                if($count < 4) { ?> 
                                                    <?php if ($class == "three-property") { ?>  
                                                        <?php if ($count == 2) { ?>  
                                                        <div class="col-md-6 col-sm-6 col-xs-12 padding-0"></div>
                                                        <?php }?>
                                                    <?php }?>
                                                      <div class="col-md-<?php echo $x; ?> col-sm-6 col-xs-12 padding-0">
                                                        <div class="save-property-image" <?php if($saved['StandardFields']['PropertyClass'] == 'Land' || $saved['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">

                                                           <?php if(isset($saved['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $saved['StandardFields']['ListingKey']; ?>"><img src="<?=$saved['StandardFields']['Photos'][0]['Uri300']?>" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                            <?php } else { ?>
                                                                <a href="<?= base_url();?>home/other_property?listingId=<?= $saved['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                                            <?php } ?>

                                                        </div>
                                                        <div class="save-property-price">
                                                            <p> <?=$saved['StandardFields']['PropertyClass'];?></p>
                                                            <p> $<?=number_format($saved['StandardFields']['CurrentPrice']);?></p>
                                                        </div>
                                                        <div class="save-property-title">
                                                            <p class="save-property-name">
                                                                <?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>
                                                            </p>
                                                            <p>
                                                                 <?php
                                                                    $mystring = $saved['StandardFields']['City'];
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);

                                                                if($pos === false)
                                                                    echo $saved['StandardFields']['City'] . ", " . $saved['StandardFields']['StateOrProvince'] . " " . $saved['StandardFields']['PostalCode'];
                                                                    else
                                                                    echo $saved['StandardFields']['PostalCity'] . ", " . $saved['StandardFields']['StateOrProvince'] . " " . $saved['StandardFields']['PostalCode'];
                                                                ?>
                                                            </p>
                                                        </div>
                                                        <div class="property-quick-icons <?php if($saved['StandardFields']['PropertyClass'] == 'Land' || $saved['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-quick-icons-land";} ?>">
                                                            <ul class="list-inline">
                                                                <?php
                                                                   if($saved['StandardFields']['BedsTotal'] && is_numeric($saved['StandardFields']['BedsTotal'])) {
                                                                ?>
                                                                <li><i class="fa fa-bed"></i> <?=$saved['StandardFields']['BedsTotal']?> Bed</li>
                                                                <?php
                                                                    }
                                                                    if($saved['StandardFields']['BathsTotal'] && is_numeric($saved['StandardFields']['BathsTotal'])) {
                                                                ?>
                                                                 <li><i class="icon-toilet"></i> <?=$saved['StandardFields']['BathsTotal']?> Bath</li>
                                                                <?php
                                                                    }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                      </div>

                                                <?php      } $count++; 

                                                    }
                                                }
                                            ?>
                              </div>
                          </div>
                            <?php endif; ?>


                        <?php 
                            }   
                        ?>
                        </div>
                    </div>
                </div>
            </div>

     <?php                          
                                
        }
    } 

    ?>

    <section class="agent-info-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 mobilepadding-0">
                    <div class="mortage-calculator">
                        <h3 class="text-center">CONTACT AGENT</h3>

                        <div class="question-mess" ></div>  <br/>    
                        <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
                            <div class="form-group">
                                <input type="text" name="name" value="" class="form-control" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" value="" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="" value="" cols="30" rows="10" class="form-control" placeholder="Message" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-default btn-block submit-button submit-question-button" style="text-transform:uppercase;">Submit</button>
                        </form>

                    </div>
                </div>

                <div class="col-md-8 col-sm-12 nopadding-left">
                    <div class="col-md-3 col-sm-4 nopadding">
                        <div class="agent-image">
                           <?php if( isset($branding->agent_photo) AND !empty($branding->agent_photo)) { ?>
                                 <img src="<?=AGENT_DASHBOARD_URL . 'assets/upload/photo/'.$branding->agent_photo?>" class="img-thumbnail" width="300">
                            <?php } else { ?>
                                <?php if (isset($my_account["Images"][0]["Uri"]) AND !empty($my_account["Images"][0]["Uri"])){?>
                                    <img src="<?=$my_account["Images"][0]["Uri"]?>" class="img-thumbnail" width="300"> 
                                 <?php } else { ?>
                                    <img src="<?= base_url()?>/assets/images/no-profile-img.gif" alt="No Profile Image">
                                 <?php } ?>  
                            <?php } ?> 
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8 nopadding">

                    <!-- Get data from Featured Listings -->   
                <?php

                if(isset($json_property) AND !empty($json_property)){

                        $icount = 1;

                        foreach($json_property as $active_property){

                            if(isset($active_property) AND !empty($my_account)){

                                if($active_property->ListAgentEmail ==  $my_account["Emails"][0]["Address"] || $active_property->ListOfficeName ==  $my_account["Office"]){

                                    if($icount == 1) {?>

                                    <h4 class="agent-name">
                                        
                                        <?php if (isset($branding->first_name) AND !empty($branding->first_name)) {?>
                                            <?php echo $branding->first_name ?>
                                        <?php } else {?>
                                                <?php echo $active_property->ListAgentFirstName?>
                                        <?php }?>
                                         <?php if (isset($branding->last_name) AND !empty($branding->last_name)) {?>
                                            <?php echo $branding->last_name ?>
                                        <?php } else {?>
                                                <?php echo $active_property->ListAgentLastName?>
                                        <?php }?>   
                                    </h4>

                                    <div class="about-agent">
                                        <?php if (isset($branding->about_agent) AND !empty($branding->about_agent)) { ?>

                                            <?php
                                                $img = $branding->about_agent;
                                                $findImg   = 'data-filename';
                                                $aboutImg = strpos($img, $findImg);
                                            ?>
                                               
                                               <?php if($aboutImg !== false) { ?>

                                                    <?php echo $branding->about_agent; ?>

                                                <?php } else { ?>  

                                                    <?php echo  $branding->about_agent = (strlen($branding->about_agent) > 300) ? substr($branding->about_agent,0,300).'... <a href="./about">Read more</a>' : $branding->about_agent; ?>

                                                <?php } ?>
                                            
                                       <?php } ?>
                                    </div>
                                    <ul class="agent-detail">

                                            <?php 
                                                if (isset($branding->phone) AND !empty($branding->phone)) {

                                                     echo "<li><i class='fa fa-phone'></i> Phone Number: ".$branding->phone."</li>";

                                                } elseif(isset($active_property->ListAgentPreferredPhone) && !empty($active_property->ListAgentPreferredPhone) && ($active_property->ListAgentPreferredPhone !== "********")) {

                                                    echo "<li><i class='fa fa-phone'></i> Phone Number: ".$active_property->ListAgentPreferredPhone."</li>";
                                                } elseif(isset($my_account["Phones"])){
                                                    foreach($my_account["Phones"] as $phone){
                                                        if($phone["Name"] == "Office"){

                                                            echo "<li><i class='fa fa-2x fa-mobile'></i> Phone Number: ".$phone["Number"] ."</li>";

                                                        }
                                                    }
                                                }

                                                if (isset($branding->mobile) AND !empty($branding->mobile)) {

                                                     echo "<li><i class='fa fa-2x fa-mobile'></i> Mobile Number: ".$branding->mobile."</li>";

                                                } elseif(isset($active_property->ListAgentCellPhone) && !empty($active_property->ListAgentCellPhone) && ($active_property->ListAgentCellPhone !== "********")) {

                                                    echo "<li><i class='fa fa-2x fa-mobile'></i> Mobile Number: ".$active_property->ListAgentCellPhone."</li>";

                                                } elseif(isset($my_account["Phones"])){
                                                    foreach($my_account["Phones"] as $phone){
                                                        if($phone["Name"] == "Mobile"){

                                                            echo "<li><i class='fa fa-2x fa-mobile'></i> Mobile Number: ".$phone["Number"] ."</li>";

                                                        }
                                                    }
                                                }

                                                 if (isset($branding->address) AND !empty($branding->address)) {

                                                     echo "<li><i class='fa fa-map-marker'></i> Address: ".$branding->address."</li>";

                                                } elseif(isset($my_account["Addresses"][0]["Address"]) && !empty($my_account["Addresses"][0]["Address"]) && ($my_account["Addresses"][0]["Address"] !== "********")) {

                                                    echo "<li><i class='fa fa-map-marker'></i> Address: ".$my_account["Addresses"][0]["Address"]."</li>";
                                                }

                                                if (isset($branding->email) AND !empty($branding->email)) {

                                                     echo "<li><i class='fa fa-envelope'></i> Email: ".$branding->email."</li>";

                                                } elseif(isset($active_property->ListAgentEmail) && !empty($active_property->ListAgentEmail) && ($active_property->ListAgentEmail !== "********")) {

                                                    echo "<li><i class='fa fa-envelope'></i> Email: ".$active_property->ListAgentEmail."</li>";

                                                } elseif(isset($my_account["Addresses"][0]["Address"]) && !empty($my_account["Addresses"][0]["Address"]) && ($my_account["Addresses"][0]["Address"] !== "********")) {

                                                    echo "<li><i class='fa fa-envelope'></i> Address: ".$my_account["Addresses"][0]["Address"]."</li>";
                                                }

                                                if (isset($branding->license_number) AND !empty($branding->license_number)) {

                                                     echo "<li><i class='fa fa-certificate'></i> License Number: ".$branding->license_number."</li>";

                                                } elseif(isset($active_property->ListAgentStateLicense) && !empty($active_property->ListAgentStateLicense) && ($active_property->ListAgentStateLicense !== "********")) {

                                                    echo "<li><i class='fa fa-certificate'></i> License Number: ".$active_property->ListAgentStateLicense."</li>";
                                                }

                                                if (isset($branding->broker) AND !empty($branding->broker)) {

                                                     echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$branding->broker."</li>";

                                                } elseif(isset($active_property->ListOfficeName) && !empty($active_property->ListOfficeName) && ($active_property->ListOfficeName !== "********")) {

                                                    echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$active_property->ListOfficeName."</li>";
                                                    
                                                } elseif(isset($my_account["Office"]) && !empty($my_account["Office"]) && ($my_account["Office"] !== "********")) {

                                                    echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$my_account["Office"]."</li>";
                                                }

                                                if (isset($branding->broker_number) AND !empty($branding->broker_number)) {

                                                     echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$branding->broker_number."</li>";

                                                } elseif(isset($active_property->ListOfficePhone) && !empty($active_property->ListOfficePhone) && ($active_property->ListOfficePhone !== "********")) {

                                                    echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$active_property->ListOfficePhone."</li>";

                                                } elseif(isset($my_account["Phones"])){
                                                    foreach($my_account["Phones"] as $phone){
                                                        if($phone["Name"] == "Office"){

                                                            echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$phone["Number"] ."</li>";

                                                        }
                                                    }
                                                }

                                              if(isset($active_property->ListOfficeFax) && !empty($active_property->ListOfficeFax) && ($active_property->ListOfficeFax !== "********")) {

                                                    echo "<li><i class='fa fa-fax'></i> Fax Number: ".$active_property->ListOfficeFax."</li>";

                                                } elseif(isset($my_account["Phones"])){
                                                    foreach($my_account["Phones"] as $phone){
                                                        if($phone["Name"] == "Fax"){

                                                            echo "<li><i class='fa fa-fax'></i> Fax Number: ".$phone["Number"] ."</li>";

                                                        }
                                                    }
                                                }

                                            ?>
                                    </ul>
                    
                                    <?php   }break;
                                        }
                                    }

                        }$icount++;
                    } else {?>

                         <!-- Get data from My Account -->
                         <h4 class="agent-name">
                            
                            <?php if (isset($branding->first_name) AND !empty($branding->first_name)) {?>
                                <?php echo $branding->first_name ?>
                            <?php } else {?>
                                    <?php echo $my_account["FirstName"]?>
                            <?php }?>
                             <?php if (isset($branding->last_name) AND !empty($branding->last_name)) {?>
                                <?php echo $branding->last_name ?>
                            <?php } else {?>
                                    <?php echo  $my_account["LastName"]?>
                            <?php }?>   
                        </h4>
                       <div class="about-agent">
                            <?php if (isset($branding->about_agent) AND !empty($branding->about_agent)) { ?>

                                <?php
                                    $img = $branding->about_agent;
                                    $findImg   = 'data-filename';
                                    $aboutImg = strpos($img, $findImg);
                                ?>
                                               
                                    <?php if($aboutImg !== false) { ?>

                                        <?php echo $branding->about_agent; ?>

                                    <?php } else { ?>  

                                    <?php echo  $branding->about_agent = (strlen($branding->about_agent) > 300) ? substr($branding->about_agent,0,300).'... <a href="./about">Read more</a>' : $branding->about_agent; ?>

                                    <?php } ?>
                                            
                            <?php } ?>
                        </div>
                        <ul class="agent-detail">
                            <?php
                                    if (isset($branding->phone) AND !empty($branding->phone)) {

                                         echo "<li><i class='fa fa-phone'></i> Phone Number: ".$branding->phone."</li>";

                                    } elseif(isset($my_account["Phones"][0]["Number"]) && !empty($my_account["Phones"][0]["Number"]) && ($my_account["Phones"][0]["Number"] !== "********")) {

                                        echo "<li><i class='fa fa-phone'></i> Phone Number: ".$my_account["Phones"][0]["Number"]."</li>";
                                    }

                                    if (isset($branding->mobile) AND !empty($branding->mobile)) {

                                        echo "<li><i class='fa fa-2x fa-mobile'></i> Mobile Number: ".$branding->mobile."</li>";

                                    } elseif(isset($my_account["Phones"])){
                                        foreach($my_account["Phones"] as $phone){
                                            if($phone["Name"] == "Mobile"){

                                                echo "<li><i class='fa fa-2x fa-mobile'></i> Mobile Number: ".$phone["Number"] ."</li>";

                                            }
                                        }
                                    }

                                    if (isset($branding->address) AND !empty($branding->address)) {

                                         echo "<li><i class='fa fa-map-marker'></i> Address: ".$branding->address."</li>";

                                    } elseif(isset($my_account["Addresses"][0]["Address"]) && !empty($my_account["Addresses"][0]["Address"]) && ($my_account["Addresses"][0]["Address"] !== "********")) {

                                        echo "<li><i class='fa fa-map-marker'></i> Address: ".$my_account["Addresses"][0]["Address"]."</li>";
                                    }

                                    if (isset($branding->email) AND !empty($branding->email)) {

                                         echo "<li><i class='fa fa-envelope'></i> Email: ".$branding->email."</li>";

                                    } elseif(isset($my_account["Emails"][0]["Address"]) && !empty($my_account["Emails"][0]["Address"]) && ($my_account["Emails"][0]["Address"] !== "********")) {

                                        echo "<li><i class='fa fa-envelope'></i> Email: ".$my_account["Emails"][0]["Address"]."</li>";
                                    }

                                    if (isset($branding->license_number) AND !empty($branding->license_number)) {

                                         echo "<li><i class='fa fa-certificate'></i> License Number: ".$branding->license_number."</li>";
                                    }

                                    if (isset($branding->broker) AND !empty($branding->broker)) {

                                         echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$branding->broker."</li>";

                                    } elseif(isset($my_account["Office"]) && !empty($my_account["Office"]) && ($my_account["Office"] !== "********")) {

                                        echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$my_account["Office"]."</li>";
                                    }

                                    if (isset($branding->broker_number) AND !empty($branding->broker_number)) {

                                         echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$branding->broker_number."</li>";

                                    } elseif(isset($my_account["Phones"])){
                                        foreach($my_account["Phones"] as $phone){
                                            if($phone["Name"] == "Office"){

                                                echo "<li><i class='fa fa-phone-square'></i> Brokerage Number: ".$phone["Number"] ."</li>";

                                            }
                                        }
                                    }

                                    if(isset($my_account["Phones"])){
                                        foreach($my_account["Phones"] as $phone){
                                            if($phone["Name"] == "Fax"){

                                                echo "<li><i class='fa fa-fax'></i> Fax Number: ".$phone["Number"] ."</li>";

                                            }
                                        }
                                    }
                                ?>
                        </ul>

                <?php } ?>
                    </div>
                    
                     <div class="col-md-12 properties-sold">

                         <?php 
                         $count =1;
                          if( isset($json_property) AND !empty($json_property)) { ?>
                             <?php foreach($json_property as $property) { ?>
                                <?php 
                                if(isset($property->MlsStatus) && !empty($property->MlsStatus)) {
                                    if( $property->MlsStatus == "Closed" || $property->MlsStatus == "Sold") { ?>

                                  <?php if($count == 1){
                                    echo "<h4>Recent Properties Sold</h4> ";
                                  } break;
                                  ?>
                                        
                        <?php } }
                                }
                                $count++;
                                
                            }
                        ?>


                            <div class="sold-property-container">                                

                                <!-- Fetch Sold Property From Property Listing Data -->
                                <?php if( isset($json_property) AND !empty($json_property)) { ?>
                                    <?php foreach($json_property as $property) { ?>
                                        <?php 
                                        if(isset($property->MlsStatus) && !empty($property->MlsStatus)) {
                                            if( $property->MlsStatus == "Closed" || $property->MlsStatus == "Sold") { ?>
                                            <div class="other-listing-item">
                                                <div class="sold-banner"><img src="<?= base_url().'assets/img/home/sold.png'?>"></div>
                                                <div class="sold-image">
                                                     <?php if(isset($property->Photos[0]->Uri300)) { ?>
                                                        <a href="<?= base_url();?>home/sold_property?listingId=<?= $property->ListingKey; ?>"><img src="<?=$property->Photos[0]->Uri300?>" alt="<?php echo $property->UnparsedFirstLineAddress; ?>" class="img-responsive"></a>
                                                        
                                                     <?php } else { ?>

                                                        <a href="<?= base_url();?>home/sold_property?listingId=<?= $property->ListingKey; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $property->UnparsedFirstLineAddress; ?>" class="img-responsive"></a>

                                                     <?php } ?>
                                                </div>
                                                <div class="sold-property-details">
                                                    <p><a href="<?= base_url();?>home/sold_property?listingId=<?= $property->ListingKey; ?>"><?php echo $property->UnparsedFirstLineAddress; ?></a></p>
                                                    <p><i class="fa fa-map-marker"></i>
                                                       <?php
                                                            $mystring = $property->City;
                                                            $findme   = '*';
                                                            $pos = strpos($mystring, $findme);
                                                            
                                                            if($pos === false) 
                                                                echo $property->City . ", " . $property->StateOrProvince . " " . $property->PostalCode;
                                                            else
                                                                echo $property->PostalCity . ", " . $property->StateOrProvince . " " . $property->PostalCode;
                                                        ?>
                                                    </p>
                                                    <p><i class="fa fa-usd"></i> <?=number_format($property->CurrentPrice)?></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php }
                                        } ?> 
                                <?php } ?> 
                            </div>   
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="agent-listing-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="agent-description">
                        <h4>JOHANNES KRAUSER</h4>
                        <p>The Google Fonts API will generate the necessary browser-specific CSS to use the fonts. All you need to do is add the font name to your CSS styles.</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    
    <?php
        $this->load->view($theme.'/session/footer');
    ?>
