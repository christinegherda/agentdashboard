<?php $this->load->view('/session/header-page');  ?>

    <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <div class="visible-xs filter-button-container">
                            <button class="btn btn-default submit-button filter-search-button">
                                Show Contact
                            </button>
                        </div>
                        <div class="contact-panel">
                            <div class="filter-tab">
                                <div class="property-agent-info">
                                    <h4 class="text-center">Agent</h4>
                                    <hr>
                                    <div class="agent-image">
                                        <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/john.jpg" alt="" class="img-thumbnail">    
                                    </div>
                                    <h4 class="agent-name">John Doe</h4>
                                    <p>
                                        <i class="fa fa-phone-square"></i> Office: 888-587-7767 x 1
                                        <br>
                                        <i class="fa fa-2x fa-mobile"></i> Mobile: 561-239-2929
                                        <br>
                                        <i class="fa fa-fax"></i> Fax : 855-297-7507
                                        <br>
                                    </p>
                                    <p><i class="fa fa-envelope"></i> : <a href="">Email Me</a></p>
                                </div>
                            </div>

                            <div class="filter-tab">
                                <div class="property-submit-question">
                                    <h4 class="text-center">Have a question?</h4>
                                    <hr>
                                    <form action="">
                                        <div class="form-group">
                                            <label for="">Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Message</label>
                                            <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        </div>
                                        <button class="btn btn-default btn-block submit-button">Submit</button>
                                    </form> 
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 content-panel">
                        <div class="buyer-blurb">
                            <h3>Buying Your Dream Home</h3>
                            <p>
                                The two key questions you want to ask yourself when finding the property that is right for you are 'where' and 'what'. Where do you want to live and what type of property do you want to live in? The old real estate saying - "location, location, location" - is as important today as it has ever been. Not only is it more enjoyable to live in a nice area but it will enhance the value of your home when the time comes to sell. 
                            </p>
                            <p>
                                Fill out the form below and one of our qualified agents will be in contact to help you get started on your home search:
                            </p>
                        </div>
                        <div class="property-details">
                            <form action="" class="">
                                <div class="contact-information">
                                    <h4>Contact Information</h4>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control">
                                        </div>    
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phone</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Mobile</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Fax</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Address</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">City</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">State</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Zip Code</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="property-information clearfix">
                                    <h4>Property Information</h4>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">No. of Bedrooms</label>
                                            <select name="" id="" class="form-control">
                                                <option value="">1</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">No. of Bathrooms</label>
                                            <select name="" id="" class="form-control">
                                                <option value="">1</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Square Feet</label>
                                            <select name="" id="" class="form-control">
                                                <option value="">< 1000</option>
                                                <option value="">1000 - 2000</option>
                                                <option value="">2000 - 3000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Contact You By</label>
                                            <select name="" id="" class="form-control">
                                                <option value="">Email</option>
                                                <option value="">Phone</option>
                                                <option value="">Mobile</option>
                                                <option value="">Fax</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Price Range</label>
                                            <select name="" id="" class="form-control">
                                                <option value="">Less than $300,000</option>
                                                <option value="">$300,000 - $400,000</option>
                                                <option value="">$400,000 - $500,000</option>
                                                <option value="">$600,000 - $700,000</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="moving-detail">
                                    <h4>Moving Details</h4>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">When do you want to move?</label>
                                            <select name="" id="" class="form-control">
                                                <option value="">Less than 30 days</option>
                                                <option value="">1 Month</option>
                                                <option value="">2 Months</option>
                                                <option value="">3 Monts</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">When did you start looking?</label>
                                            <select name="" id="" class="form-control">
                                                <option value="">1 Month</option>
                                                <option value="">2 Months</option>
                                                <option value="">3 Months</option>
                                                <option value="">4 Months</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Where would you like to own?</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Are you currently with an agent?</label>
                                            <select name="" id="" class="form-control">
                                                <option value="">Yes</option>
                                                <option value="">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Describe your dream home</label>
                                            <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> I consent to receiving emails containing real estate related information from this site. I understand that I can unsubscribe at any time.
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-default submit-button">Submit Form</button>    
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php $this->load->view('/session/footer');  ?>