<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/assets/css/landing_page.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>

        <section class="sign-up-page clearfix">
            <div class="sign-up-strip clearfix">
                <div class="container">
                    <div class="row">
                            <div class="left-sign-up">
                                <div class="col-md-6 col-sm-6">
                                    <div class="welcoming-sign-up">
                                        <h1>Welcome to</h1>
                                        <div class="logo-preset-customer"> <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/logo.png" alt="Agentsquared Signup" class="sign-up-logo"></div>
                                        </a>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="right-sign-up">
                                <div class="col-md-6 col-sm-6">
                                    <div class="sign-up-container clearfix">
                                        <h2>Sign Up</h2>
                                        <form  action="" method="post" id="signupForm" class="sign_up_form">
                                            <div class="form-group">
                                                <label for="email">First Name</label>
                                                <input type="text" name="fname" id="email" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Last Name</label>
                                                <input type="text" name="lname" id="email" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email Address</label>
                                                <input type="email" name="email" id="email" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" name="password" id="password" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="confirm_password">Confirm Password</label>
                                                <input type="password" name="confirm_password" id="confirm_password" class="form-control required" required>
                                            </div>
                                            <p>
                                                By clicking Create Account, I agree to the  <a href="" data-toggle="modal" data-target="#modalSignupTerms">Terms of Service</a> and <a href="" data-toggle="modal" data-target="#modalSignupPrivacy">Privacy Policy</a>.
                                            </p>
                                            <div class="col-md-12 col-sm-12 no-padding-left">
                                                <button type="submit" name="submit" class="btn btn-success btn-create-account">Create Account</button> 
                                                <a href="javascript:void(0)" onclick ="customer_fb_login();" class="fb_btn" id="fb-btn" ><img style="width: 51%;" src="/assets/images/fb2.png"></a>       
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="have-account text-center">
                                                    <p>Already have an account?</p>
                                                    <p><a href="login">Login</a></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 footer-brand">
                <p class="text-center text-powered">2016 All Rights Reserved. Developed by
                <a href="http://www.agentsquared.com/" target="_blank" class="brand-name" data-original-title="" title=""> <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/logo.png" alt="Agentsquared Signup" class="footer-logo"></a>
                </p>

            </div>
        </section>

        <!-- terms of use -->
        <div class="modal fade legal-modal" id="modalSignupTerms" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content theme-modal">
                    <div class="modal-header">
                        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        <h2 style="color:#fff" class="text-center">Terms of Use</h2>
                    </div>
                    <div class="modal-body clearfix">
                        <p>
                         PLEASE READ THESE TERMS AND CONDITIONS OF USE ("SITE TERMS") CAREFULLY. YOUR USE OF THE SITE IS CONDITIONED UPON YOUR ACCEPTANCE OF THESE SITE TERMS WITHOUT MODIFICATION. 
                         BY ACCESSING OR USING THIS WEB SITE, YOU AGREE TO BE BOUND BY THESE SITE TERMS AND ALL TERMS INCORPORATED BY REFERENCE. 
                         IF YOU DO NOT AGREE TO THESE SITE TERMS, DO NOT USE THIS WEB SITE.
                        <p>
                         The Web site you are accessing (the "Site") is hosted by AgentSquared.com on behalf of one of its real estate industry customers (the "Company"). 
                         AgentSquared and Company are independent contractors and are not partners or otherwise engaged in any joint venture in delivering the Site. 
                         Notwithstanding that fact, these Site Terms apply to both Agent Squared and Company and as such, any reference to "we", "us" or "our" herein will apply 
                         equally to both Agent Squared and Company.
                         These Site Terms apply exclusively to your access to, and use of, the Site so please read them carefully. 
                         These Site Terms do not alter in any way the terms or conditions of any other agreement you may have with Agent Squared, Company, or 
                         their respective subsidiaries or affiliates, for services, products or otherwise. We reserve the right to change or modify any of the Site 
                         Terms and the Site, at any time. If we decide to change our Site Terms, we will post a new version on the Site and update the date. 
                         Any changes or modifications will be effective immediately upon posting of the revisions on the Site, and you waive any right you may have to receive specific notice of such changes or modifications. 
                         Your use of the Site following the posting of changes or modifications to the Site Terms will constitute your acceptance of the revised Site Terms. 
                         Therefore, you should frequently review the Site Terms and applicable policies from time-to-time to understand the terms and conditions that apply to your use of the Site. 
                         If you do not agree to the amended terms, you must immediately stop using the Site.
                        </p>

                        <h4>Privacy</h4>
                        <p>
                         We believe strongly in providing you notice of how we collect and use your data, including personally identifying information, 
                         collected from the Sites. We have adopted a Privacy Statement to which you should refer to fully understand how we collect and use 
                         personally identifying information.
                        </p>

                        <h4>Consent to Communications</h4>
                        <p>
                         By filling out any forms on the Site or making any inquiries through the Site you acknowledge that we have an established business relationship and you expressly consent to being contacted us, 
                         whether by phone, mobile phone, email, mail or otherwise.
                        </p>

                        <h4>Property Listing Data</h4>                   
                        <p>
                         Any real estate listing data provided to you in connection with the Site is not intended to be a representation of the complete Multiple Listing Service data for any of our MLS sources. 
                         We are not liable for and do not guarantee the accuracy of any listing data or other data or information found on the Site, and all such information should be independently verified. The 
                         information provided in connection with the Site is for the personal, non-commercial use of consumers and may not be 
                         used for any purpose other than to identify prospective properties consumers may be interested in purchasing. 
                         Some properties which appear for sale on this website may no longer be available because they are under contract, have sold or are no longer being offered for sale.
                        </p>

                        <h4>Use of Site; Limited License</h4>
                        <p>
                         You are granted a limited, non-sublicensable license to access and use the Site and all content, data, information and materials included in the Site (the "Site Materials") 
                         solely for your personal use, subject to the terms and conditions set forth in these Site Terms. You may not use the Site or any Site Materials for commercial purposes. You agree that you will 
                         not modify, copy, distribute, resell, transmit, display, perform, reproduce, publish, license, create derivative works from, frame in another Web page, use on any other web site or service any of the Site Materials. 
                         You will not use the Site or any of the Site Materials other than for its intended purpose or in any way that is unlawful, or harms Agent Squared, Company and/or their suppliers.
                        </p>
                        <p>
                         Without prejudice to the foregoing, you may not engage in the practices of "screen scraping," "database scraping," "data mining" or any other activity with the purpose of obtaining lists of users or other information 
                         from the Site or that uses web "bots" or similar data gathering or extraction methods. You may not obtain or attempt to obtain any materials or information through any means not 
                         intentionally made available or provided for through the Site.
                        </p>
                        <p>
                         Any use of the Site or the Site Materials other than as specifically authorized herein is strictly prohibited and will terminate the license granted herein. Such unauthorized use may also violate applicable laws including without limitation copyright and 
                         trademark laws and applicable communications regulations and statutes. Unless explicitly stated herein, nothing in these Site Terms shall be construed as conferring any license to 
                         intellectual property rights, whether by estoppel, implication or otherwise. This license is revocable by us at any time.
                        </p>

                        <h4>Responsibility for Your Conduct and User Content</h4>
                        <p>
                         You are solely liable for your conduct and for any information, text, photos, content, materials or messages that you upload, post or transmit to the Site (collectively the "User Content"). 
                         You may not provide false or misleading information to the Site or submit information under false pretenses.
                        </p>
                        <p>
                         Without limiting anything else in these Site Terms, we may immediately terminate your access to and use of any of the Sites if you violate any of the foregoing or 
                         if you provide false or misleading information or submit information under false pretenses.
                        </p>

                        <h4>Account Security</h4>
                        <p>
                         Each person that is provided with a password and user ID to use the Site must agree to abide by these Site Terms and is responsible for all activity under such user ID. 
                         You are responsible for maintaining the confidentiality and security of any password connected with your account.
                        </p>

                        <h4>Submissions</h4>
                        <p>
                         You agree that any materials, including but not limited to questions, comments, suggestions, ideas, plans, notes, drawings, original or creative materials or other information regarding Company or the Site, provided by you in the form of e-mail or 
                         submissions to us, or postings on the Sites, are non-confidential (subject to our Privacy Policy). We will own exclusive rights, including all intellectual property 
                         rights, and will be entitled to the unrestricted use of these materials for any purpose, commercial or otherwise, without acknowledgment or compensation to you.
                        </p>

                        <h4>Hyperlinks</h4>
                        <p>
                         We make no claim or representation regarding, and accept no responsibility for, the quality, content, nature or reliability of third-party Web sites accessible by hyperlink from the Site, or Web sites linking to the Site. 
                         Such sites are not under our control and we are not responsible for the content of any linked site or any link contained in a linked site, or any review, changes or updates to such sites. We provide these links to you only as a convenience, and the inclusion of any 
                         link does not imply affiliation, endorsement or adoption by us of any site or any information contained therein. When you leave the Site, you should be aware that our terms and policies no longer govern. 
                         You should review the applicable terms and policies, including privacy and data gathering practices, of any site to which you navigate from the Site.
                        </p>

                        <h4>Third Party Products and Services; Third-Party Content</h4>
                        <p>
                         We may run advertisements and promotions from third parties on the Site or may otherwise provide information about or links or referrals to third-party products or services on the Site. Your business dealings or correspondence with, or participation in promotions of, 
                         such third parties, and any terms, conditions, warranties or representations associated with such dealings or promotions are solely between you and such third party. We are not responsible or liable for any loss or damage of any sort incurred as the result of any 
                         such dealings or promotions or from any third party products or services, and you use such third party products and services at your own risk. We do not sponsor, endorse, recommend or approve any such third party who advertises their goods or services through the Site. 
                         You should investigate and use your independent judgment regarding the merits, quality and reputation of any individual, entity or information that you find on or through the Site. We do not represent or warrant that any third party who places advertisements on the Site is 
                         licensed, qualified, reputable or capable of performing any such service.
                        </p>
                        <p>
                         We do not make any assurance as to the timeliness or accuracy of any third-party content or information provided on or linked to from the Site ("Third Party Content"). We do not monitor or have any control over any Third Party Content, and do not endorse or adopt any 
                         Third Party Content and can make no guarantee as to its accuracy or completeness. We will not update or review any Third Party Content, and if you choose to use such Third Party Content you do so at your own risk.
                        </p>

                        <h4>Disclaimer</h4>
                        <p>
                         THE SITE AND THE SITE MATERIALS (INCLUDING ALL THIRD PARTY CONTENT), AND ALL LINKS, INFORMATION, MATERIALS, EVALUATIONS, RECOMMENDATIONS, SERVICES 
                         AND PRODUCTS PROVIDED ON OR THROUGH THE SITES ARE PROVIDED ON AN "AS IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. YOU EXPRESSLY 
                         AGREE THAT USE OF THE SITE AND THE MATERIALS IS AT YOUR SOLE RISK. COMPANY AND AGENT SQUARED AND THEIR RESPECTIVE SUPPLIERS DISCLAIM ALL WARRANTIES, 
                         EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT 
                         AS TO THE SITE, THE SITE MATERIALS, LINKS, INFORMATION, MATERIALS, SERVICES AND PRODUCTS AVAILABLE ON OR THROUGH ON THE SITE.
                         NEITHER COMPANY NOR AGENT SQUARED REPRESENTS OR WARRANTS THAT THE MATERIALS ARE ACCURATE, COMPLETE, RELIABLE, CURRENT, OR ERROR-FREE; OR IS 
                         RESPONSIBLE FOR TYPOGRAPHICAL ERRORS OR OMISSIONS RELATING TO PRICING, TEXT, OR PHOTOGRAPHY. WHILE WE ATTEMPT TO ENSURE YOUR ACCESS AND USE OF THE 
                         SITES IS SAFE, NEITHER COMPANY NOR AGENT SQUARED REPRESENTS OR WARRANTS THAT THE SITES OR ITS SERVER(S) ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. 
                         AGENT SQUARED IS MERELY A SERVICE PROVIDER TO COMPANY, AND IN NO EVENT SHALL AGENT SQUARED BE LIABLE TO USER FOR ANY OF THE PRODUCTS, SERVICES, CONTENT 
                         OR INFORMATION PROVIDED THROUGH THE SITE OR OTHERWISE PROVIDED BY OR ON BEHALF OF COMPANY, AND AGENT SQUARED MAKES NO REPRESENTATION OR WARRANTY WITH 
                         RESPECT THERETO.
                        </p>

                        <h4>Limitation of Liability</h4>
                        <p>
                         IN NO EVENT WILL COMPANY OR AGENT SQUARED OR ANY OF THEIR RESPECTIVE SUPPLIERS BE LIABLE FOR ANY DIRECT, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, 
                         OR ANY OTHER DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO LOSS OF USE, LOSS OF PROFITS, OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT, TORT 
                         (INCLUDING BUT NOT LIMITED TO NEGLIGENCE), OR OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF THE SITE OR THE SITE MATERIALS.
                        </p>

                        <h4>Termination; Refusal to Provide Services.</h4>
                        <p>
                         We may terminate or suspend your access to the Site at any time, with or without cause, and with or without notice. Upon such termination or suspension, 
                         your right to use the Site will immediately cease. Furthermore, we reserve the right not to respond to any requests for information for any reason, 
                         or no reason.
                        </p>

                        <h4>Applicable Law and Venue</h4>
                        <p>
                         These terms and conditions are governed by and construed in accordance with the laws of the State of California, applicable to agreements made and 
                         entirely to be performed within the State of California, without resort to its conflict of law provisions. You agree that any action at law or in equity 
                         arising out of or relating to these terms and conditions can be filed only in state or federal court located in San Diego, California, and you hereby 
                         irrevocably and unconditionally consent and submit to the exclusive jurisdiction of such courts over any suit, action or proceeding arising out of these 
                         terms and conditions.
                        </p>

                        <h4>General Terms</h4>
                        <p>
                         If any part of the Site Terms is determined to be invalid or unenforceable pursuant to applicable law, then the invalid or unenforceable provision will 
                         be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the Site Terms 
                         will continue in effect. We may assign this Agreement, in whole or in part, at any time with or without notice to you. You may not assign this Agreement, 
                         or assign, transfer or sublicense your rights, if any, in the Sites. Except as expressly stated herein, the Site Terms constitute the entire agreement 
                         between you and us with respect to the Sites.
                        </p>

                        <h4>Copyright and Trademark</h4>
                        <p>
                         Unless otherwise indicated in the Site, the Site Materials and the selection and arrangement thereof are the proprietary property of Agent Squared, 
                         Company or their suppliers and are protected by U.S. and international copyright laws. The Company name, and any Company products and services slogans 
                         or logos referenced herein are either trademarks or registered trademarks of Company in the United States and/or other countries. The names of actual 
                         third party companies and products mentioned in the Site may be the trademarks of their respective owners. Any rights not expressly granted herein are 
                         reserved.
                        </p>
                        <p>
                         The parties have required that the Site Terms and all documents relating thereto be drawn up in English.  
                        </p>
                    </div>
                </div>
            </div>   
        </div>

         <!-- privacy policy modal -->
        <div class="modal fade legal-modal" id="modalSignupPrivacy" tabindex="-1" role="dialog">
           <div class="modal-dialog modal-lg">
              <div class="modal-content theme-modal">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                   <h2 style="color:#fff" class="text-center">Privacy Policy</h2>
                 </div>
                 <div class="modal-body clearfix">
                    <p>
                       The website you are accessing (the "Site") is hosted by Agent Squared on behalf of one of its real estate industry customers (the "Company"). 
                       Agent Squared and Company are independent contractors and are not partners or otherwise engaged in any joint venture in delivering the Site. 
                       Notwithstanding that fact, this Privacy Policy ("Policy") applies to both Agent Squared and Company and as such, any reference to "we", "us" or "our" 
                       herein will apply equally to both Agent Squared and Company. This Policy explains how personal information is collected, used, and disclosed with 
                       respect to your use of the Site, so you can make an informed decision about using the Site.
                    </p>

                    <p>
                       We reserve the right to change this Policy at any time. If we make any material changes to the Policy, we will post a new statement on the Site 
                       and update the effective date set forth above. Therefore, we encourage you to check the effective date of the Policy whenever you visit the Site to 
                       see if it has been updated since your last visit.
                    </p>

                    <h4>INFORMATION WE COLLECT</h4>
                    <ul>
                       <li>
                          <b>Personal Information.</b> As used herein, the term "personal information" means information that specifically identifies an individual 
                          (such as a name, address, telephone number, mobile number, or e-mail address,), and information about that individual's location or activities or 
                          demographic information when such information is directly linked to personally identifiable information. Personal information does not include "aggregate" 
                          information, which is data we collect about the use of the Site or about a group or category of products, services or users, from which individual identities 
                          or other personal information has been removed. This Policy in no way restricts or limits our collection and use of aggregate information.
                       </li>
                       <li>
                          <b>Collection.</b> Personal information may be collected in a number of ways when you visit the Site. We may collect the personal information that you voluntarily 
                          provide to us. For example, when you fill out a form, send us an email, make a request, complete a survey, register for certain services on the Site, or 
                          participate in certain activities on the Site, we may ask you to provide personal information such as your e-mail address, name, home or work address or 
                          telephone number. We may also collect demographic information, such as your ZIP code, age, gender, and preferences. Information we collect may be combined with 
                          information we obtained through other services we offer. We may collect information about your visit to the Site, including the pages you view, the links you 
                          click and other actions taken in connection with the Site and services. We may also collect certain standard information that your browser sends to every web 
                          site you visit, such as your IP address, browser type and language, access times and referring Web site addresses. If we provide you with various services related 
                          to home selling and home buying (e.g., estimates of current market value of homes, moving costs, mortgage or finance costs, current home listings, neighborhood 
                          information, etc.), we may collect additional information related to such services.
                       </li>
                       <li>
                          <b>Cookies and Web Beacons.</b> We may automatically collect certain information through the use of "cookies" or web beacons. Cookies are small pieces of information 
                          or files that are stored on a user's computer by the Web server through a user's browser to enable the site to recognize users who have previously visited them 
                          and retain certain information such as customer preferences and history. We may use cookies to identify users, customize their experience on our site or to 
                          serve relevant ads. If we combine cookies with or link them to any of the personal information, we would treat this information as personal information. 
                          We do not use Web beacons, or similar technology on the Site. However, we may permit third parties to place cookies or Web beacons on the Site to monitor 
                          the effectiveness of advertising and for other lawful purposes. A Web beacon, also known as a "Web bug" or a "clear gif" is a small, graphic image on a Web page, 
                          Web-based document or in an e-mail message that is designed to allow the site owner or a third party to monitor who is visiting a site and user activity on the site. 
                          We do not use (or permit third parties to use) Web beacons to collect personally identifying information about you. If you wish to block, erase, or be warned of 
                          cookies, please refer to your browser instructions to learn about these functions. However, if a browser is set to not accept cookies or if a user rejects a cookie, 
                          some portions of the Site may not function properly. For example, you may not be able to sign in and may not be able to access certain Site features or services.
                       </li>
                       <li>
                          <b>Third Party Cookies and Web Beacons; Use of Third Party Ad Networks.</b> We may also use third parties to serve ads on the Site.These third parties may place 
                          cookies, Web beacons or other devices on your computer to collect nonpersonal information, and information provided by these devices may be used, among other things, 
                          to deliver advertising targeted to your interests and to better understand the usage and visitation of the Site and the other sites tracked by these third parties. 
                          For example, we may allow other companies, called third-party ad servers or ad networks, to display advertisements on the Site. Some of these ad networks may place a 
                          persistent cookie on your computer in order to recognize your computer each time they send you an online advertisement. In this way, ad networks may compile information 
                          about where you, or others who are using your computer, saw their advertisements and determine which ads are clicked on. This information allows an ad network to 
                          deliver targeted advertisements that they believe will be of most interest to you. We do not have access to the cookies that may be placed by the third-party ad 
                          servers or ad networks. This Policy does not apply to, and we are not responsible for, cookies in third party ads, and we encourage you to check the privacy policies 
                          of advertisers and/or ad services to learn about their use of cookies and other technology.
                       </li>
                    </ul>

                    <h4>USE OF PERSONAL INFORMATION</h4>
                    <p>
                       We use personal information internally to serve users of the Site, to enhance and extend our relationship with Site users and to facilitate the use of, and 
                       administration and operation of, the Site. For example, we may use personal information you provide to deliver services to you, to process your requests or 
                       transactions, to provide you with information you request, to personalize content, features and advertising on the Site, to anticipate and resolve problems with 
                       the Site, to respond to customer support inquiries, to inform you of other events, promotions, products or services offered by third parties that we think will 
                       be of interest to you, to send you relevant survey invitations related to the Site and for our own internal operations, and for any purpose for which such 
                       information was collected. We may, from time to time, contact you on behalf of external business partners about a particular offering that may be of interest to you. 
                       In those cases, your personal information (e-mail, name, address, telephone number) is not transferred to the third party unless you have consented to such transfer.
                    </p>

                    <h4>DISCLOSURE OF PERSONAL INFORMATION</h4>
                    <p>
                       Except as set forth in this Policy, we do not disclose, sell, rent, loan, trade or lease any personal information collected on the Site to any third party. 
                       We may provide aggregate information to third parties without restriction. We will disclose personal information in the following circumstances:
                    </p>
                    <ul>
                       <li>
                          <b>Consent.</b> We may share personal information with third parties when you give us your express or implied consent to do so, such as when you fill out a form 
                          requesting certain information from a third party.
                       </li>
                       <li>
                          <b>Internal Analysis and Operations; Service Providers.</b> We will use and disclose information about you for our own internal statistical, design or operational 
                          purposes, such as to estimate our audience size, measure aggregate traffic patterns, and understand demographic, user interest, purchasing and other trends among 
                          our users. We may outsource these tasks and disclose personal information about you to third parties, provided the use and disclosure of your personal information 
                          by these third parties is restricted to performance of such tasks. We may also share personal information with other third-party vendors, consultants, and service 
                          providers ("Service Providers") who are engaged by or working with us in connection with the operation of the Site or our business In some cases, the Service Provider 
                          may be directly collecting the information from you on our behalf. We require our third party vendors to keep such information confidential and to only use the 
                          information for the purpose it was provided. However, we are not responsible for the actions of Service Providers or other third parties, nor are we responsible 
                          for any additional information you provide directly to these Service Providers or other third parties.
                       </li>
                       <li>
                          <b>Protection of Company and Others:</b> We may disclose personal information when we believe it is appropriate to comply with the law (e.g., a lawful subpoena, 
                          warrant or court order); to enforce or apply this Policy or our other policies or agreements; to initiate, render, bill, and collect for amounts owed to us; 
                          to respond to claims, to protect our or our users' rights, property or safety; or to protect us, our users or the public from fraudulent, abusive, harmful 
                          or unlawful activity or use of the Site; or if we reasonably believe that an emergency involving immediate danger of death or serious physical injury to any 
                          person requires disclosure of communications or justifies disclosure of records without delay.
                       </li>
                       <li>
                          <b>Business Transfers.</b> In addition, information about users of the Site, including personally identifiable information provided by users, may be disclosed 
                          or transferred as part of, or during negotiations regarding, any merger, acquisition, debt financing, sale of company assets, as well as in the event of an 
                          insolvency, bankruptcy or receivership in which personally identifiable information could be transferred to third parties as one of the business assets of 
                          the company.
                       </li>
                       <li>
                          <b>Co-Branded Services.</b> Some services available through the Site may be co-branded and offered in conjunction with other companies. If you register for or 
                          use such services, then those other companies may also receive any information collected in conjunction with the co-branded services.
                       </li>
                    </ul>

                    <h4>LINKS</h4>
                    <p>
                       Please be aware that we may provide links to third party Web sites from the Site as a service to its users and we are not responsible for the content or 
                       information collection practices of those sites. We have no ability to control the privacy and data collection practices of such sites and the privacy 
                       policies of such sites may differ from this Policy. Therefore, we encourage you to review and understand the privacy policies of such sites before providing 
                       them with any information.
                    </p>

                    <h4>SECURITY</h4>
                    <p>
                       We use commercially reasonable measures to store and maintain personally identifying information, to protect it from loss, misuse, alternation or destruction 
                       by any unauthorized party while it is under our control.
                    </p>

                    <h4>CHILDREN</h4>
                    <p>
                       We do not intend, and the Site is not designed, to collect personal information from children under the age of 13. If you are under 13 years old, you should 
                       not provide information on the Site.
                    </p>

                    <h4>CHOICE AND ACCESS</h4>
                    <p>
                       You may unsubscribe at any time from receiving non-service related communications from us. Site users have the following options for accessing, changing and 
                       deleting personal information previously provided, or opting out of receiving communications from Company or Agent Squared:
                    </p>
                    <ul>
                       <li>email us at support@agentsquared.com; or</li>
                       <li>call us at the following telephone number: 800-901-4428</li>
                    </ul>

                    <span>QUESTIONS AND CONCERNS</span><br>
                    <span>If you have any questions about this Policy, the practices applicable to the Site, or your dealings with the Site, please contact us at the following address:</span>
                    <br>
                    <span>Agent Squared</span>
                    <br/>
                    <span>1155 Camino Del Mar, #103</span>
                    <br/>
                    <span>Del Mar, CA 92014</span>
                 </div>
              </div>
           </div>
        </div> 


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/assets/js/home/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script src="/assets/js/home/main.js"></script>


    <script>
        var customer_fb_url = <?php echo json_encode($this->facebook->customer_login_url());?>;

    </script>

</body>

</html>
