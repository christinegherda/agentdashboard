<!doctype html>
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Arillo</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/demo/agent-haven/main.css">
    <script src="<?=base_url()?>assets/js/dashboard/demo/agent-haven/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>

<body class="preview-body">
    <div class="container">
        <div class="row">
            <div id="preview-nav">
                <div class="col-md-4">
                    <div class="back-to-theme">
                        <button onclick="location.href='<?=base_url()?>choose_theme'" class="btn btn-default">
                            <i class="fa fa-angle-left" aria-hidden="true"></i> &nbsp;Back to theme selection
                        </button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="size-container text-center">
                        <ul class="list-inline">
                            <li id="desktop-screen" class="active"><a href="javascript:void(0);"><i class="fa fa-desktop" aria-hidden="true"></i></a></li>
                            <li id="tablet-portrait"><a href="javascript:void(0);"><i class="fa fa-tablet" aria-hidden="true"></i></a></li>
                            <li id="tablet-landscape"><a href="javascript:void(0);"><i class="fa fa-tablet" aria-hidden="true"></i></a></li>
                            <li id="phone-portrait"><a href="javascript:void(0);"><i class="fa fa-mobile" aria-hidden="true"></i></a></li>
                            <li id="phone-landscape"><a href="javascript:void(0);"><i class="fa fa-mobile" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-2 no-padding-right">
                    <div class="theme-bg">
                        <h2 class="text-right">Agent Haven</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="iframe-section">
        <div id="iframe-switcher" class="desktop">
            <div class="iframe-wrapper">
                <iframe src="<?=base_url()?>/demo/agent_haven" frameborder="0" style="overflow:hidden;height:100%;width:102.5%;" height="100%" width="100%" seamless="seamless"></iframe>
            </div>
        </div>
    </section>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script type="text/javascript" src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/dashboard/demo/agent-haven/main.js"></script>
</body>
</html>