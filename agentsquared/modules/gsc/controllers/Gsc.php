<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gsc extends MX_Controller {

  public function __construct(){
    parent::__construct();

  } // end of __construct()

  public function index(){
    $data = array();

    $this->load->view('gsc', $data);
  }
}