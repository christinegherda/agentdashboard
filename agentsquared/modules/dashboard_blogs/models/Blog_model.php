<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Blog_model extends CI_Model { 

	public function __construct()
    {
        parent::__construct();
    }
	// --------------------------------------

    public function get_all_blogs( $counter = FALSE,  $param = array() )
    {
        
         $this->db->select("*")
                ->from("blogs")
                ->where("author", $this->session->userdata('user_id'))
                ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function get_group_published_blogs( )
    {
        $array = array(
            'author' => $this->session->userdata('user_id'),
            'status' => 'published'
        );
        $year = 
        $this->db->select("post_date")
                ->from("blogs")
                ->where($array)
                ->group_by ("YEAR(post_date),MONTH(post_date)")
                ->order_by("id","desc");
        
        $query = $this->db->get();


        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function get_group_trashed_blogs( )
    {
        $array = array(
            'author' => $this->session->userdata('user_id'),
            'status' => 'trash'
        );
        $year = 
        $this->db->select("post_date")
                ->from("blogs")
                ->where($array)
                ->group_by ("YEAR(post_date),MONTH(post_date)")
                ->order_by("id","desc");
        
        $query = $this->db->get();


        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

     public function get_all_published_blogs( $counter = FALSE,  $param = array() )
    {
        $array = array(
            'author' => $this->session->userdata('user_id'),
            'status' => 'published'
        );
        $this->db->select("*")
                ->from("blogs")
                ->where($array)
                ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function get_all_trashed_blogs( $counter = FALSE,  $param = array() )
    {
        $array = array(
            'author' => $this->session->userdata('user_id'),
            'status' => 'trash'
        );
        $this->db->select("*")
                ->from("blogs")
                ->where($array)
                ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function filter_date($filter = NULL )
    {
        $this->db->select("*")
                ->from("blogs")
                ->where("status" , "published")
                ->where("author" , $this->session->userdata('user_id'))
                ->where("post_date like '%".$filter."%'")
                ->order_by("id","desc");
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function filter_trash($filter = NULL )
    {
        $this->db->select("*")
                ->from("blogs")
                ->where("status" , "trash")
                ->where("author" , $this->session->userdata('user_id'))
                ->where("post_date like '%".$filter."%'")
                ->order_by("id","desc");
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function add()
    {
        $post = $this->input->post("blog");
        $post["content"] = html_entity_decode($post["content"]);
        $post["post_date"] = date("Y-m-d H:i:s");
        $post["post_modified"] = date("Y-m-d H:i:s");
        $post["status"] = "published";
        $post["slug"] = str_replace(" ", "-", clean_url($post["title"]));
        $post["author"] = $this->session->userdata('user_id'); //uncomment if session userid is activated

        if( $this->db->insert("blogs", $post) )
        {
            return $this->db->insert_id();
        }
        else
        {
            return FALSE;
        }

    }	

    public function edit( $blog_id = NULL, $datas = array()  )
    {
        $blog = $this->input->post("blog");
        $blog["slug"] = str_replace(" ", "-", clean_url($blog["title"]));
        $blog["post_modified"] = date("Y-m-d H:i:s");

        if( $this->db->update("blogs", $blog, array("id" => $blog_id) ))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function delete( $blog_id = NULL )
    {
        $this->db->where("id", $blog_id);

        if( $this->db->delete( "blogs" ) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }

    public function bulk_delete()
    {   
       if(gettype($_POST['bulk_delete'])=="array"){
            foreach($_POST['bulk_delete'] as $val){
             $menu_id = $val;
             
              $array = array(
                'id' => $menu_id,
                'author' => $this->session->userdata('user_id')
            );

           $this->db->where($array)
                ->delete('blogs');
            }
        }
    }

    public function move_trash( $blog_id = NULL )
    {
         $this->db->set('status', "trash")
            ->where("id", $blog_id)
            ->update('blogs'); 
       

    }

    public function restore( $blog_id = NULL )
    {
         $this->db->set('status', "published")
            ->where("id", $blog_id)
            ->update('blogs'); 

    }

    public function bulk_restore()
    {   
       if(gettype($_POST['bulk_restore'])=="array"){
            foreach($_POST['bulk_restore'] as $val){
             $menu_id = $val;
             
              $array = array(
                'id' => $menu_id,
                'author' => $this->session->userdata('user_id'),
            );

           $this->db->set('status', "published")
                ->where($array)
                ->update('blogs');
            }
        }
    }

    public function get_blog( $blog_id = 0 )
    {
        $this->db->select("*")
                ->from("blogs")
                ->where("id" , $blog_id);

        return $this->db->get()->row_array();
    }

    public function bulk_trash()
    {   
       if(gettype($_POST['bulk_trash'])=="array"){
            foreach($_POST['bulk_trash'] as $val){
             $menu_id = $val;
             
              $array = array(
                'id' => $menu_id,
                'author' => $this->session->userdata('user_id')
            );

           $this->db->set('status', "trash")
                ->where($array)
                ->update('blogs');
            }
        }
    }
}