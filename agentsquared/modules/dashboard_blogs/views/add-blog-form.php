<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>
 	<div class="content-wrapper">
 	 <div class="page-title">
	    <h3 class="heading-title">
	      <span> Add New Blog </span>
	    </h3>
	  </div>             
      <section class="content">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                	<?php echo form_open('dashboard_blogs/add', array("class" => "new-page-sbt", "id" => "new-page-sbt" ) ); ?>
	                    <div class="col-md-12">
	                        <a href="<?php echo site_url("dashboard_blogs"); ?>" class="btn btn-default btn-submit"><i class="fa fa-arrow-left"></i> Back to blog list</a>
	                    </div>
	                    <br><br>
	                    <div class="col-md-8">
	                        <input type="text" class="blog-title-area form-control " name="blog[title]" placeholder="Enter title here">
	                        <br>
	                        <textarea name="blog[content]" class="form-control summernote"></textarea>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="page-panel">
	                            <div class="page-actions">
	                                <h4>Publish</h4>
	                                <h5><i class="fa fa-key" aria-hidden="true"></i> Status: <strong>Draft</strong> <!--<a href="javascript:void(0);" class="toggle-option text-underline">Edit</a>--></h5>
	                                <div class="options" style="display:none">
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="statusRadios" id="" value="Published" class="radio-input">
	                                            Published
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="statusRadios" id="" value="Draft" class="radio-input">
	                                            Draft
	                                        </label>
	                                    </div>
	                                   <button class="btn btn-submit">Save</button>
	                                   <a href="#" class="toggle-option text-underline">Cancel</a>
	                                </div>    
	                            </div>
	                            
	                            <div class="page-actions">
	                                 <h5><i class="fa fa-eye" aria-hidden="true"></i> Visiblity: <strong>Public</strong> <!--<a href="javascript:void(0);" class="toggle-option text-underline">Edit</a>--></h5>
	                                <div class="options" style="display:none">
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="visibilityRadios" id="" value="Public">
	                                            Public
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="visibilityRadios" id="" value="Private">
	                                            Private
	                                        </label>
	                                    </div>
	                                    <button class="btn btn-submit">Save</button>
	                                   <a href="#" class="toggle-option text-underline">Cancel</a>
	                                </div>
	                                <h5><i class="fa fa-calendar" aria-hidden="true"></i> Publish <strong>immediately</strong></h5>
	                            </div>
	                            <br>
	                            <button class="btn btn-default btn-submit blog-publish">Publish</button>
	                        </div>
	                    </div>
               		</form>
                </div>
            </div>
        </div>
    </div>
   </section>
 </div>
<?php $this->load->view('session/footer'); ?>
<script>
    $(document).ready(function(){

        $('.summernote').summernote({
        	height: 400,

        	popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['custom', ['imageAttributes', 'imageShape']],
                ['remove', ['removeMedia']]
            ],
        },
        
		});

    });
</script>