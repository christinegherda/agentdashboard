<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>
    <div class="content-wrapper">
        <div class="page-title">
        <h3 class="heading-title">
          <span> Trashed Blogs</span>
        </h3>
        </div>           
        <section class="content">
            <div class="container-fluid">
                <?php if( !empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret")) AND !config_item("is_paid") ) : ?>
                    <?php $this->load->view('session/launch-view'); ?>
                <?php endif; ?>
                <div class="row">
                     <div class="col-md-12">
                        <a href="<?php echo site_url("dashboard_blogs/add"); ?>" class="btn btn-default btn-submit"><i class="fa fa-plus"></i> Add New Blog</a>
                    </div>
                    <br><br>
                    <div class="page-content">
                     <?php if( !empty($blogs) ) { ?>
                        <div class="col-md-12 add-page">
                            <ul class="sub-header">
                                <?php if (isset($published_blogs) AND !empty($published_blogs)){?>
                                    <li class="all"><a href="<?php echo base_url()?>dashboard_blogs">All <span class="count">(<?= count($published_blogs)?>)</span></a>  |</li>
                                    <li class="publish"><a href="<?php echo base_url()?>dashboard_blogs">Published <span class="count">(<?= count($published_blogs)?>)</span></a> </li>
                                <?php } else {?> 
                                     <li class="all"><a href="<?php echo base_url()?>dashboard_blogs">All <span class="count">(0)</span></a></li>
                                <?php }?>    
                                <?php if (isset($trashed_blogs) AND !empty($trashed_blogs)){?>
                                     <li class="trash"><a href="<?php echo base_url()?>dashboard_blogs/trash" class="current">| Trash <span class="count">(<?= count($trashed_blogs)?>)</span></a></li>
                                <?php }?>
                               
                            </ul>
                        </div>
                        <div class="col-md-12 mb-30px">
                            <div class="col-md-4">
                                <div class="btn-group">
                                    <form action="" method="POST" id="bulk-trash">
                                         <select name="bulk_action" id="bulk-select" class="form-control">
                                            <option value="">Bulk Actions</option>
                                            <option value="restore">Restore</option>
                                             <option value="delete">Delete Permanently</option>
                                         </select>
                                     </form>
                                </div>

                                <div class="btn-group">
                                    <button type="submit" form="bulk-trash" class="btn btn-default btn-add-featured">Apply</button>
                                </div>
                        
                                <div class="btn-group">
                                    <form action="" method="POST" id="filter-trash">
                                         <select name="filter_month" id="select-trash" class="form-control">
                                            <option>All Dates</option>
                                            <?php if( !empty($group_trashed_blogs) ) { 
                                                foreach($group_trashed_blogs as $blog){?>

                                                    <option value="<?php echo date("Y-m", strtotime($blog->post_date)); ?>"> <?php echo date("F Y", strtotime($blog->post_date)); ?></option>

                                            <?php  }

                                                } 
                                            ?>         
                                        </select>
                                    </form>   
                                </div>
                                 <div class="btn-group">
                                    <button type="submit" form="filter-trash" class="btn btn-default btn-add-featured">Filter</button>
                                </div>

                            </div>
                            <div class="col-md-4">
                                
                            </div>
                            <div class="col-md-4">
                                <form action="" class="form-horizontal" method="get" >
                                    <div class="input-group">
                                        <input type="text" name="keywords" value="<?php echo (isset($_GET['keywords'])) ? $_GET['keywords'] : "" ;?>" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-submit" type="submit">Search</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>

                    <?php }?> 
                    
                        <div class="col-md-12">
                             <table class="table table-striped table-responsive">
                                  <thead>
                                    <tr>
                                      <th><label><input type="checkbox" id="checkAll"/></label></th>
                                      <th>Title</th>
                                      <th class="text-center">Date</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?php if( !empty($blogs) ) { ?>
                                        <?php if( !empty($trashed_blogs) ) { 
                                                foreach ($trashed_blogs as $blog) :
                                        ?> 
                                             <tr>
                                              <td width="1%">
                                                <input class="select-action" type="checkbox" name="bulk_restore[]" value="<?php echo $blog->id ?>" form="bulk-trash">
                                              </td>
                                              <td width="90%">
                                                 <?php echo ucwords($blog->title); ?>
                                                    <br>
                                                    <a href="<?php echo site_url('dashboard_blogs/restore/'.$blog->id); ?>">Restore</a> <span>|</span> <a href="<?php echo site_url('dashboard_blogs/delete/'.$blog->id); ?>">Delete Permanently</a>
                                                 
                                              </td>
                                              <td width="10%">
                                                  <p class="pull-right"><?php echo date("F d, Y", strtotime($blog->post_date)); ?> <br> <?php echo ($blog->status) ? ucfirst($blog->status) : "Draft"; ?></p>
                                              </td>
                                            </tr> 

                                                <?php endforeach; ?>

                                        <?php } ?>

                                      <?php }  else { ?>

                                        <tr>
                                            <td colspan="3">
                                                No blog created yet!
                                            </td>
                                        </tr>
                                      <?php } ?>
                                  </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <?php if( $pagination ) : ?>
                                <div class="pagination-area pull-right">
                                    <nav>
                                        <ul class="pagination">
                                            <?php echo $pagination; ?>
                                        </ul>
                                    </nav>
                                </div>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $this->load->view('session/footer'); ?>

