<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>
 	<div class="content-wrapper">
 	<div class="page-title">
	    <h3 class="heading-title">
	      <span> Edit Blog </span>
	    </h3>
	  </div>               
      <section class="content">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                	<?php echo form_open('', array("class" => "new-page-sbt", "id" => "new-page-sbt" ) ); ?>
	                    <div class="col-md-12">
	                        <a href="<?php echo site_url("dashboard_blogs"); ?>" class="btn btn-default btn-submit"><i class="fa fa-arrow-left"></i> Back to blog list</a>
	                    </div>
	                    <br>
	                    <br>
	                    <div class="col-md-12">	                    	
	                    		<?php $session = $this->session->flashdata('session'); 	                    			
	                    			if( isset($session) )
	                    			{
	                    		?>
	                    			<div class="alert alert-flash <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>	
	                    		<?php } ?>
	                    	
	                     </div>
	                   
	                    <div class="col-md-8">
	                        <input type="text" name="blog[title]" value="<?php echo (isset($info["title"])) ? $info["title"] : "" ?>" class="form-control">
	                        <br>
	                        <textarea name="blog[content]" class="form-control summernote"><?php echo (isset($info["content"])) ? $info["content"] : "" ?></textarea>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="page-panel">
	                            <div class="page-actions">
	                                <h4>Publish</h4>
	                                	<h5><i class="fa fa-key" aria-hidden="true"></i> Status: <strong>Published</strong> <!--<a href="javascript:void(0);" class="toggle-option text-underline">Edit</a>--></h5>
	                                <div class="options" style="display:none">
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="statusRadios" id="" value="Published" class="radio-input">
	                                            Published
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="statusRadios" id="" value="Draft" class="radio-input">
	                                            Draft
	                                        </label>
	                                    </div>
	                                   <button class="btn btn-submit">Save</button>
	                                   <a href="#" class="toggle-option text-underline">Cancel</a>
	                                </div>    
	                            </div>
	                            
	                            <div class="page-actions">
	                               <h5><i class="fa fa-eye" aria-hidden="true"></i> Visiblity: <strong>Public</strong> <!--<a href="javascript:void(0);" class="toggle-option text-underline">Edit</a>--></h5>
	                                <div class="options" style="display:none">
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="visibilityRadios" id="" value="Public">
	                                            Public
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="visibilityRadios" id="" value="Private">
	                                            Private
	                                        </label>
	                                    </div>
	                                   <button class="btn btn-submit">Save</button>
	                                   <a href="#" class="toggle-option text-underline">Cancel</a>
	                                </div>
	                            </div>
	                             <h5><i class="fa fa-calendar" aria-hidden="true"></i> Published on: <strong><?php echo date("F d, Y", strtotime($info["post_date"])) ?></strong></h5>
	                            <br>
	                            <button class="btn btn-default btn-submit">Publish</button>
	                           <a href="<?=base_url()?>home/post/<?php echo clean_url($info["slug"])?>" type="button" class="btn btn-default btn-submit" target="_blank">Preview</a>
	                        </div>
	                    </div>
               		</form>
                </div>
            </div>
        </div>
    </div>
   </section>
 </div>
<?php $this->load->view('session/footer'); ?>
<script>
    $(document).ready(function(){

        $('.summernote').summernote({
        	height: 400,

        	popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['custom', ['imageAttributes', 'imageShape']],
                ['remove', ['removeMedia']]
            ],
        },
        
		});

    });
</script>