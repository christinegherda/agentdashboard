<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dashboard_blogs extends MX_Controller {
 	
 	public function __construct()
	{
		parent::__construct();
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->load->model("Blog_model","blog");
		$this->load->model("agent_sites/Agent_sites_model", "domain");

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

	}

	public function index()
	{
		
		$data["title"] = "Manage Blogs";

		$this->load->library('pagination');

		//get blogs
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$total = $this->blog->get_all_blogs( TRUE, $param );
		$data["blogs"] = $this->blog->get_all_blogs( FALSE, $param );
		$data["published_blogs"] = $this->blog->get_all_published_blogs( FALSE, $param );
		$data["group_published_blogs"] = $this->blog->get_group_published_blogs();
		$data["trashed_blogs"] = $this->blog->get_all_trashed_blogs( FALSE, $param );
		$data["group_trashed_blogs"] = $this->blog->get_group_trashed_blogs();
		
		$config["base_url"] = base_url().'/dashboard_blogs';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","blogs/blogs.js");
		$data["css"] = array("plugins/msgBoxLight.css");
		
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		//printA( $data["blogs"] ); exit;

		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();

		$this->load->view('dashboard_blogs', $data);

	}

	public function trash(){
		$data["title"] = "Trash Blogs";

		$this->load->library('pagination');

		//get pages
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$total = $this->blog->get_all_blogs( TRUE, $param );
		$data["blogs"] = $this->blog->get_all_blogs( FALSE, $param );
		$data["published_blogs"] = $this->blog->get_all_published_blogs( FALSE, $param );
		$data["group_published_blogs"] = $this->blog->get_group_published_blogs();
		$data["trashed_blogs"] = $this->blog->get_all_trashed_blogs( FALSE, $param );
		$data["group_trashed_blogs"] = $this->blog->get_group_trashed_blogs();
		
		$config["base_url"] = base_url().'/dashboard_blogs/trash';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","blogs/blogs.js");
		$data["css"] = array("plugins/msgBoxLight.css");
		
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		//printA( $data["published_blogs"] ); exit;

		$this->load->view('trash_blogs', $data);
	}

	public function filter($filter = NULL){
		$data["title"] = "Filter Blogs";

		$this->load->library('pagination');

		//get blogs
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$total = $this->blog->get_all_blogs( TRUE, $param );
		$data["blogs"] = $this->blog->get_all_blogs( FALSE, $param );
		$data["published_blogs"] = $this->blog->get_all_published_blogs( FALSE, $param );
		$data["group_published_blogs"] = $this->blog->get_group_published_blogs();
		$data["trashed_blogs"] = $this->blog->get_all_trashed_blogs( FALSE, $param );
		$data["group_trashed_blogs"] = $this->blog->get_group_trashed_blogs();
		$data["filter_blogs"] = $this->blog->filter_date($filter);

		
		$config["base_url"] = base_url().'/dashboard_blogs/trash';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","blogs/blogs.js");
		$data["css"] = array("plugins/msgBoxLight.css");
		
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		//printA( $data["filter_blogs"] ); exit;

		$this->load->view('filter_blogs', $data);
	}

	public function filter_trash($filter = NULL){
		$data["title"] = "Filter Trash";

		$this->load->library('pagination');

		//get pages
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$total = $this->blog->get_all_blogs( TRUE, $param );
		$data["blogs"] = $this->blog->get_all_blogs( FALSE, $param );
		$data["published_blogs"] = $this->blog->get_all_published_blogs( FALSE, $param );
		$data["group_published_blogs"] = $this->blog->get_group_published_blogs();
		$data["trashed_blogs"] = $this->blog->get_all_trashed_blogs( FALSE, $param );
		$data["group_trashed_blogs"] = $this->blog->get_group_trashed_blogs();
		$data["filter_trash"] = $this->blog->filter_trash($filter);

		$config["base_url"] = base_url().'/dashboard_blogs/trash';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","blogs/blogs.js");
		$data["css"] = array("plugins/msgBoxLight.css");
		
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		//printA( $data["filter_trash"] ); exit;

		$this->load->view('filter_trash', $data);
	}

	public function add()
	{	
		$data["title"] = "Add New Blog";

		if( $_POST ) 
		{
			$this->form_validation->set_rules('blog[title]', 'Title', 'required|trim');
			$this->form_validation->set_rules('blog[content]', 'Content', 'required|trim');

			if ($this->form_validation->run() == true)
			{
				if( $id = $this->blog->add() )
				{
					$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Blog has been successfully added!'));

					redirect("dashboard_blogs/edit/".$id, "refresh");
				}
				else
				{
					$this->session->set_flashdata('session' , array('status' => FALSE,'message'=>'Failed to add blog!'));
				}
			}
			else
			{
				$this->session->set_flashdata('session' , array('status' => FALSE,'message'=> validation_errors() ));
			}

		}

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","blogs/blogs.js","summernote-image-attributes.js");
		$data["css"] = array("plugins/msgBoxLight.css");

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();

		$this->load->view('add-blog-form', $data);
	}

	public function edit( $blog_id = NULL )
	{
		$data["title"] = "Edit Blog";

		if( $_POST )
		{
			$this->form_validation->set_rules('blog[title]', 'Title', 'required|trim');
			$this->form_validation->set_rules('blog[content]', 'Content', 'required|trim');

			if ($this->form_validation->run() == true)
			{ 
				if( $id = $this->blog->edit( $blog_id ) )
				{
					$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Blog has been successfully updated!'));

					redirect("dashboard_blogs/edit/".$blog_id, "refresh");
				}
				else
				{
					$this->session->set_flashdata('session' , array('status' => FALSE,'message'=>'Failed to edit the blog!'));
				}
			}
			else
			{
				$this->session->set_flashdata('session' , array('status' => FALSE,'message'=> validation_errors() ));
			}

		}

		$data["info"] = $this->blog->get_blog( $blog_id );

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","blogs/blogs.js","summernote-image-attributes.js");
		$data["css"] = array("plugins/msgBoxLight.css");

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		
		$this->load->view('edit-blog-form', $data);
	}

	public function delete( $blog_id = NULL )
	{
		if( empty($blog_id) ) return FALSE;

		if( $this->blog->delete( $blog_id ) )
		{
			$this->session->set_flashdata('message', 'Blog has been successfully removed!');			
		}
		else
		{
			$this->session->set_flashdata('message', 'Failed to removed blog');
		}

		redirect("dashboard_blogs", "refresh");
	}

	public function bulk_delete()
	{
		if(isset($_POST['bulk_delete']) AND !empty($_POST['bulk_delete'])){

			if( $this->blog->bulk_delete() )
				{
					$this->session->set_flashdata('message', 'All blogs has been successfully deleted!');
				}
				else
				{
					$this->session->set_flashdata('message', 'Failed to delete all blogs!');
				}
		}	

		redirect("dashboard_blogs", "refresh");
	}

	public function move_trash( $blog_id = NULL )
	{
		if( empty($blog_id) ) return FALSE;

		if( $this->blog->move_trash( $blog_id ) )
		{
			$this->session->set_flashdata('message', 'Blog has been successfully move to trash!');			
		}
		else
		{
			$this->session->set_flashdata('message', 'Failed to move to trash');
		}

		redirect("dashboard_blogs", "refresh");
	}

	public function restore( $blog_id = NULL )
	{
		if( empty($blog_id) ) return FALSE;

		if( $this->blog->restore( $blog_id ) )
		{
			$this->session->set_flashdata('message', 'Page has been successfully restored!');			
		}
		else
		{
			$this->session->set_flashdata('message', 'Failed to restore page!');
		}

		redirect("dashboard_blogs", "refresh");
	}

	public function bulk_restore()
	{
		 if(isset($_POST['bulk_restore']) AND !empty($_POST['bulk_restore'])){

			if( $this->blog->bulk_restore() )
				{
					$this->session->set_flashdata('message', 'All blogs has been successfully restored');
				}
				else
				{
					$this->session->set_flashdata('message', 'Failed to restore pages!');
				}
		 }	

		redirect("dashboard_blogs", "refresh");
	}

	public function bulk_trash()
	{
		if(isset($_POST['bulk_trash']) AND !empty($_POST['bulk_trash'])){

			if( $this->blog->bulk_trash() )
				{
					$this->session->set_flashdata('message', 'All blogs has been successfully move to trash!');
				}
				else
				{
					$this->session->set_flashdata('message', 'Failed to move to trash!');
				}
		}	

		redirect("dashboard_blogs/trash", "refresh");
	}

}