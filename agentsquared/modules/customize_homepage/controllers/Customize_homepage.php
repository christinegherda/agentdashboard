<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customize_homepage extends MX_Controller {

	function __construct() {
		parent::__construct();

		$this->load->library('Ajax_pagination');
		$this->load->library('Sold_ajax_pagination');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model("Customize_homepage_model","custom");
		$this->load->model("agent_sites/Agent_sites_model","saved_search");
		$this->load->model("dsl/dsl_model");
		$this->load->library("idx_auth", $this->session->userdata("agent_id"));
	}

	function index() {

		$data['title'] = "Customize Homepage";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
  	
  	$this->load->model('choose_theme/choose_theme_model', 'theme_model');
  	$theme = $this->theme_model->get_theme();

  	if($theme !== NULL && !empty($theme) && isset($theme[0]['theme']) && $theme[0]['theme'] !== 'agent-haven' ){

  		$this->load->model('client_theme_model', 'client_theme');
  		$this->client_theme->createTable();

  		$client_theme = $this->client_theme->get($this->session->userdata('agent_id'));

  		$homepage_layout = '[]';
  		$settings = '{}';
  		if($client_theme){
  			$homepage_layout = $client_theme->layout;
  			$settings = $client_theme->settings;
  		}

  		$theme_config = $_SERVER['DOCUMENT_ROOT'] . '/assets/theme-config/'.$theme[0]['theme'].'/.config';

  		if(file_exists($theme_config)){

	  		$json_config = file_get_contents($theme_config);
	  		$json_decoded = json_decode($json_config, true);

	  		$data['theme_config'] = (json_last_error() > 0)? false: $json_decoded;

	  		add_jsvar('theme_config', $json_config, null, true);
	  		add_jsvar('homepage_layout', $homepage_layout, null, true);
	  		add_jsvar('layout_settings', $settings, null, true);
	  		add_jsvar('min_rows', 1);
	  		add_jsvar('max_rows', 3);
	  		add_jsvar('min_columns', 1);
	  		add_jsvar('max_columns', 3);
	  		add_jsvar('widgets', $this->getWidgets(), 'json', true);
	  		add_jsvar('agent_id', $this->session->userdata('agent_id'), 'string', true);

	  		enqueue_style('custom-layout', base_url() . 'assets/css/dashboard/custom-layout.css');

	  		enqueue_script('custom-layout', base_url() . 'assets/js/dashboard/custom-layout.js');

	  	}else{
	  		$data['theme_config'] = false;
	  	}

	  	$data['theme'] = $theme[0]['theme'];

	  	$this->load->view('customize_homepage', $data);
  	}else{

  			$is_featured_option = $this->custom->get_featured_options();
			$data["home_options"] = $this->custom->get_all_options();
			$data["saved_search_title"] = $this->custom->get_saved_search_title();
			$data["sold_listings_title"] = $this->custom->get_sold_listings_title();

			$featured_saved_search_dsl = Modules::run('dsl/getFeaturedAgentSavedSearch');
			$featured_saved_search_mysql = $this->custom->get_featured_saved_search();
			$selected_saved_search_dsl = Modules::run('dsl/getSelectedAgentSavedSearch');
			$selected_saved_search_mysql = $this->custom->get_selected_saved_search();

			//check if dsl featured savedsearch status is failed
			if(isset($featured_saved_search_dsl['status']) && $featured_saved_search_dsl['status'] == "failed"){

				$data["featured_saved_search"] = $featured_saved_search_mysql;

				//save mysql featured savedsearch data to mongo
				if(!empty($featured_saved_search_mysql)){
					$this->addFeaturedSavedSearchDsl($featured_saved_search_mysql['Id']);
				}

			}elseif(!empty($featured_saved_search_dsl)){
				$data["featured_saved_search"] = $featured_saved_search_dsl[0];

				//save dsl featured savedsearch data to mysql
				if(empty($featured_saved_search_mysql)){
					$this->addFeaturedSavedSearchMysql($featured_saved_search_dsl['Id']);
				}
			}else{
				$data["featured_saved_search"] = $featured_saved_search_mysql;

				//save mysql featured savedsearch data to mongo
				if(!empty($featured_saved_search_mysql)){
					$this->addFeaturedSavedSearchDsl($featured_saved_search_mysql['Id']);
				}
			}

			//check if dsl selected savedsearch status is failed
			if(isset($selected_saved_search_dsl['status']) && $selected_saved_search_dsl['status'] == "failed"){

				$data["selected_saved_search"] = $selected_saved_search_mysql;

				//save mysql selected savedsearch data to mongo
				if(!empty($selected_saved_search_mysql)){
					$selectedId = array();
						foreach($selected_saved_search_mysql as $selected){
							$selectedId[] = $selected['Id'];
						}
					$this->addSelectedSavedSearchDsl($selectedId);
				}

			}elseif(!empty($selected_saved_search_dsl)){

				$data["selected_saved_search"] = $selected_saved_search_dsl;

				//save dsl selected savedsearch data to mysql
				if(empty($selected_saved_search_mysql)){
					$selectedId = array();
						foreach($selected_saved_search_dsl as $selected){
							$selectedId[] = $selected['Id'];
						}
					$this->addSelectedSavedSearchMysql($selectedId);
				}
			}else{

				$data["selected_saved_search"] = $selected_saved_search_mysql;

				//save mysql selected savedsearch data to mongo
				if(!empty($selected_saved_search_mysql)){
					$selectedId = array();
						foreach($selected_saved_search_mysql as $selected){
							$selectedId[] = $selected['Id'];
						}
					$this->addSelectedSavedSearchDsl($selectedId);
				}

			}

			$obj = Modules::load("mysql_properties/mysql_properties");
			$data['active_listings'] = $obj->get_properties($this->session->userdata("user_id"), "active");
			$data['office_listings'] = $obj->get_properties($this->session->userdata("user_id"), "office");
			$data['nearby_listings'] = $obj->get_properties($this->session->userdata("user_id"), "nearby");

			$dsl = modules::load('dsl/dsl');
			$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
			$access_token = array(
				'access_token' => $token->access_token
			);
			//subtract 1 day from today's date
			$lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
			$today =  gmdate("Y-m-d\TH:i:s\Z");
			//printA($lastday);exit;

			$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold') And (OnMarketDate Bt ".$lastday.",".$today.")";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$endpoint = "spark-listings?_pagination=1&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
			$results = $dsl->post_endpoint($endpoint, $access_token);
			$new_listings['results'] = json_decode($results, true);
			$new_listings_data = $new_listings['results'];
			$data['new_listings'] = isset($new_listings_data["data"] ) ? $new_listings_data["data"]  : "";
			//printA($data['new_listings']);exit;


			//Hide FEATURED options if empty
			if(!empty($is_featured_option)){
				if($is_featured_option[0]->option_name == "active_listings"){
					$data["is_featured_option"] = !empty($data['active_listings']) ? $is_featured_option : "";

				}elseif($is_featured_option[0]->option_name == "office_listings"){
					$data["is_featured_option"] = !empty($data['office_listings']) ? $is_featured_option : "";

				}elseif($is_featured_option[0]->option_name == "new_listings"){
					$data["is_featured_option"] = !empty($data['new_listings']) ? $is_featured_option : "";

				}elseif($is_featured_option[0]->option_name == "nearby_listings"){
					$data["is_featured_option"] = !empty($data['nearby_listings']) ? $is_featured_option : "";

				}
			}

			//Hide SELECTED options if empty
			$filter = array();
			$filter[] =  empty($data['active_listings'])? "active_listings" :"";
			$filter[] =  empty($data['office_listings'])? "office_listings" :"";
			$filter[] =  empty($data['new_listings'])? "new_listings" :"";
			$filter[] =  empty($data['nearby_listings'])? "nearby_listings" :"";

			$filter_data = array_filter($filter);
			$data["is_selected_option"] = $this->custom->get_selected_options($filter_data);

			$data["is_reserved_domain"] = $this->saved_search->is_reserved_domain();
			$data["domain_info"] = $this->saved_search->fetch_domain_info();
			$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");

			//DSL Savedsearch Count
			$endpoint = "savedsearches/".$this->session->userdata("user_id")."";
			$savedSearch_dsl = $dsl->post_endpoint($endpoint);
			$savedSearchData = json_decode($savedSearch_dsl,true);
			$savedSearch_dsl_count = isset($savedSearchData["count"]) ? $savedSearchData["count"] : 0;

			//SPARK savedsearch count
			$savedSearch_spark = Modules::run('dsl/getAllAgentSavedSearchFromSpark');
			$savedSearch_spark_count = isset($savedSearch_spark["count"]) ? $savedSearch_spark["count"] : 0;

			//Sync Savedsearch if savedsearch dsl and spark does not match
			if($savedSearch_dsl_count != $savedSearch_spark_count){
				$dsl->post_endpoint('sync-saved-searches', $access_token);
			}

			//savedsearch area
		  	$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 1;
			$param["limit"] = 20;
			$savedsearches_data = Modules::run('dsl/getAgentSavedSearch', array("page" => $param["offset"],  "limit" => $param["limit"]) );
			if(!empty($savedsearches_data["data"])){		
	 		
		 		$total = $savedsearches_data["count"];
		        $search_config['target']      = '#saved-search';
		        $search_config["base_url"] = base_url().'/customize_homepage/ajaxPaginationData';
		        $search_config['total_rows']  = $total;
		        $search_config['per_page']    = $param["limit"];
		        $search_config['page_query_string'] = TRUE;
				$search_config['query_string_segment'] = 'offset';
			    $this->ajax_pagination->initialize($search_config);
			    $data["ajax_pagination"] = $this->ajax_pagination->create_links();
		      	$data['savedsearches'] = $savedsearches_data["data"];
	 		}
	 		//sold-listings area
	 		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
			$param["limit"] = 20;
	 		$soldListings_data = $this->custom->get_sold_listings(FALSE,$param);
	 		$data['selected_sold_listings'] = $this->custom->get_selected_sold_listings();
			if(!empty($soldListings_data)){
	 				$total = $this->custom->get_sold_listings(TRUE);
		        $sold_config['target']      = '#sold-listings';
		        $sold_config["base_url"] = base_url().'/customize_homepage/soldListingsPagination';
		        $sold_config['total_rows']  = $total;
		        $sold_config['per_page']    = $param["limit"];
		        $sold_config['page_query_string'] = TRUE;
						$sold_config['query_string_segment'] = 'offset';
		        $this->sold_ajax_pagination->initialize($sold_config);
		        $data["sold_ajax_pagination"] = $this->sold_ajax_pagination->create_links();
		    	$data['soldListings'] = $soldListings_data;
		 	}
	 		$data["js"] = array("main.js");

	 		$this->load->view('customize_homepage_view', $data);
	  	}
	}
	// layout builder dependency
	private function getWidgets(){

		return array(
			array(
				'id' => 'horizontal-carousel',
				'name' => 'Horizontal Carousel',
				'description' => 'Horizontal carousel slider of selected properties.',
				'columns' => array(1),
				'image' => base_url('assets/images/widgets/horizontal-carousel.svg'),
				'icon' => base_url('assets/images/widgets/horizontal-carousel-icon.jpg'),
				'fields' => array(
					array(
						'label' 	=> 'Data Feed',
						'element' => 'select',
						'id' 			=> 'data-feed',
						'value' 	=> 'new',
						'options' => array(
							array( 'key' => 'new', 		'value' => 'New Listings' ),
							array( 'key' => 'active', 'value' => 'Active Listings' ),
							array( 'key' => 'nearby', 'value' => 'Nearby Listings' ),
							array( 'key' => 'office', 'value' => 'Office Listings' ),
							array( 'key' => 'sold', 	'value' => 'Sold Listings' ),
						),
					),
					array(
						'label'		=> 'Data Count',
						'element'	=> 'input',
						'type'		=> 'number',
						'id'			=> 'data-count',
						'value'		=> 4,
						'min'			=> 1,
						'max'			=> 8,
					),
				),
			),
			array(
				'id' => 'savedsearches-carousel',
				'name' => 'SavedSearches Carousel',
				'description' => 'Horizontal carousel slider of your preferred savedsearches. Maximum of 12.',
				'columns' => array(1),
				'image' => base_url('assets/images/widgets/horizontal-carousel.svg'),
				'icon' => base_url('assets/images/widgets/horizontal-carousel-icon.jpg'),
				'fields' => array(
					array(
						'label'		=> 'Select Savedsearches',
						'element'	=> 'multiselect',
						'id'			=> 'data-json',
						'options'	=> $this->getSavedSearches(),
						'value'		=> '{}',
						'max'			=> 12,
						'dynamic'	=> true,
						'feed'		=> 'savedsearches'
					),
				),
			),
			array(
				'id' => 'single-row-cards',
				'name' => 'Single Row Cards',
				'description' => 'Non-slider view of properties',
				'columns' => array(1),
				'image' => base_url('assets/images/widgets/single-row-cards.svg'),
				'icon' => base_url('assets/images/widgets/single-row-cards-icon.jpg'),
				'fields' => array(
					array(
						'label' 	=> 'Data Feed',
						'element' => 'select',
						'id' 			=> 'data-feed',
						'value' 	=> 'new',
						'options' => array(
							array( 'key' => 'new', 		'value' => 'New Listings' ),
							array( 'key' => 'active', 'value' => 'Active Listings' ),
							array( 'key' => 'nearby', 'value' => 'Nearby Listings' ),
							array( 'key' => 'office', 'value' => 'Office Listings' ),
							array( 'key' => 'sold', 	'value' => 'Sold Listings' ),
						),
					),
					array(
						'label'		=> 'Data Count',
						'element'	=> 'input',
						'type'		=> 'number',
						'id'			=> 'data-count',
						'value'		=> 4,
						'min'			=> 1,
						'max'			=> 8,
					),
				),
			),
			array(
				'id' => 'property-list-view',
				'name' => 'List View',
				'description' => 'List view of property listings.',
				'columns' => array(2,3),
				'image' => base_url('assets/images/widgets/property-list-view.svg'),
				'icon' => base_url('assets/images/widgets/property-list-view.jpg'),
				'fields' => array(
					array(
						'label' 	=> 'Data Feed',
						'element' => 'select',
						'id' 			=> 'data-feed',
						'value' 	=> 'new',
						'options' => array(
							array( 'key' => 'new', 		'value' => 'New Listings' ),
							array( 'key' => 'active', 'value' => 'Active Listings' ),
							array( 'key' => 'nearby', 'value' => 'Nearby Listings' ),
							array( 'key' => 'office', 'value' => 'Office Listings' ),
							array( 'key' => 'sold', 	'value' => 'Sold Listings' ),
						),
					),
				),
			),
			array(
				'id' => 'property-grid-view',
				'name' => 'Grid View',
				'description' => 'Grid view of property listings.',
				'columns' => array(2,3),
				'image' => base_url('assets/images/widgets/property-grid-view.svg'),
				'icon' => base_url('assets/images/widgets/property-grid-view.jpg'),
				'fields' => array(
					array(
						'label' 	=> 'Data Feed',
						'element' => 'select',
						'id' 			=> 'data-feed',
						'value' 	=> 'new',
						'options' => array(
							array( 'key' => 'new', 		'value' => 'New Listings' ),
							array( 'key' => 'active', 'value' => 'Active Listings' ),
							array( 'key' => 'nearby', 'value' => 'Nearby Listings' ),
							array( 'key' => 'office', 'value' => 'Office Listings' ),
							array( 'key' => 'sold', 	'value' => 'Sold Listings' ),
						),
					),
				),
			),
		);
	}

	public function ajaxPaginationData(){
	    $page = $this->input->post('page');
	    if(!$page){
	        $offset = 0;
	    }else{
	        $offset = $page;
	    }

	    $param["limit"] = 20;
		$savedsearches_data = Modules::run('dsl/getAgentSavedSearch', array("page" => $offset,  "limit" => $param["limit"]) );

		if(!empty($savedsearches_data["data"])){

			$total = $savedsearches_data["count"];
      $search_config['target']      = '#saved-search';
      $search_config["base_url"] = base_url().'/customize_homepage/ajaxPaginationData';
      $search_config['total_rows']  = $total;
      $search_config['per_page']    = $param["limit"];
      $search_config['page_query_string'] = TRUE;
			$search_config['query_string_segment'] = 'offset';
      $this->ajax_pagination->initialize($search_config);

      $data["ajax_pagination"] = $this->ajax_pagination->create_links();
      $data['savedsearches'] = $savedsearches_data["data"];
    	}

    	$dsl_featured_saved_search = Modules::run('dsl/getFeaturedAgentSavedSearch');
		$dsl_selected_saved_search = Modules::run('dsl/getSelectedAgentSavedSearch');

		//check if dsl featured savedsearch is not empty
		if(!empty($dsl_featured_saved_search)){
			$data["featured_saved_search"] = $dsl_featured_saved_search[0] ;
		}else{
			$data["featured_saved_search"] = $this->custom->get_featured_saved_search();
		}

		//check if dsl selected savedsearch is not empty
		if(!empty($dsl_selected_saved_search)){
			$data["selected_saved_search"] = $dsl_selected_saved_search ;
		}else{
			$data["selected_saved_search"] = $this->custom->get_selected_saved_search();
		}
        
        $this->load->view('customize_homepage_pagination_view', $data);
    }

   public function soldListingsPagination(){
	    $page = $this->input->post('page');
	    if(!$page){
	        $param["offset"] = 0;
	    }else{
	        $param["offset"] = $page;
	    }

	    $param["limit"] = 20;
	    $soldListings_data = $this->custom->get_sold_listings(FALSE, $param);
	    $data['selected_sold_listings'] = $this->custom->get_selected_sold_listings();
			if(!empty($soldListings_data)){		
 				$total = $this->custom->get_sold_listings(TRUE);
        $sold_config['target']      = '#sold-listings';
        $sold_config["base_url"] = base_url().'/customize_homepage/soldListingsPagination';
        $sold_config['total_rows']  = $total;
        $sold_config['per_page']    = $param["limit"];
        $sold_config['page_query_string'] = TRUE;
				$sold_config['query_string_segment'] = 'offset';
        $this->sold_ajax_pagination->initialize($sold_config);
        $data["sold_ajax_pagination"] = $this->sold_ajax_pagination->create_links();
    	  $data['soldListings'] = $soldListings_data;
 		  }

        $this->load->view('customize_homepage_sold_pagination_view', $data);
    }

	public function add_options(){

		if($this->input->is_ajax_request()) {

			if(!empty($this->input->post("set_featured"))){

				$this->custom->add_options();

				if($this->custom->set_featured_options()){
					$status = "success";
          			$msg = "<div class='alert-flash alert alert-success' role='alert'>Homepage option has been successfully added!</div>";
				}else{
					$status = "error";
          			$msg = "<div class='alert-flash alert alert-danger' role='alert'>Select checkbox to add homepage option!</div>";
				}

			} else {

				if(!empty($this->input->post("add_option"))){

					if($this->custom->add_options()){
						$status = "success";
              			$msg = "<div class='alert-flash alert alert-success' role='alert'>Homepage option has been successfully added!</div>";
					}else{
						$status = "error";
              			$msg = "<div class='alert-flash alert alert-danger' role='alert'>Select checkbox to add homepage option!</div>";
					}

				} else{

					 $status = "error";
             		 $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to add homepage option!</div>";

				}
			}

			echo json_encode(array('status' => $status, 'msg' => $msg));exit;

		}
	}

	public function edit_option_title(){

		if($this->input->is_ajax_request()) {

			$featured_name = $this->input->post("option_title");
			$featured_id = $this->input->post("option_id");

			if($this->custom->update_option_title( $this->input->post("option_id"))){

				  $status = "success";
	              $msg = "<div class='alert-flash alert alert-success' role='alert'>Homepage option has been successfully updated!</div>";

			} else {

				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to update homepage option!</div>";
			}
			
			echo json_encode(array('status' => $status, 'msg' => $msg, 'featured_name' => $featured_name, 'featured_id' =>$featured_id));exit;

		}

	}

	public function remove_option(){

		if($this->input->is_ajax_request()) {

			if($this->custom->remove_option( $this->input->get("option_id") )){

				  $status = "success";
	              $msg = "<div class='alert-flash alert alert-success' role='alert'>Homepage option has been successfully removed!</div>";

			} else {

				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to remove homepage option!</div>";
			}
			
			echo json_encode(array('status' => $status, 'msg' => $msg));exit;

		}

	}

	public function remove_featured(){

		if($this->input->is_ajax_request()) {

			if($this->custom->remove_featured( $this->input->get("option_id") )){

				  $status = "success";
	              $msg = "<div class='alert-flash alert alert-success' role='alert'>Featured option has been successfully removed!</div>";

			} else {

				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to remove Featured option!</div>";
			}
			
			echo json_encode(array('status' => $status, 'msg' => $msg));exit;

		}

	}

	public function update_saved_search_title(){

		if($this->input->is_ajax_request()) {

			if( $this->custom->update_saved_search_title() ){

				  $status = "success";
	              $msg = "<div class='alert-flash alert alert-success' role='alert'>Saved Search Title has been successfully updated!</div>";

			} else {

				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to update Saved Search Title!</div>";
			}
			
		echo json_encode(array('status' => $status, 'msg' => $msg));exit;

		}
	}

	public function update_sold_listings_title(){

		if($this->input->is_ajax_request()) {

			if( $this->custom->update_sold_listings_title() ){

				  $status = "success";
	              $msg = "<div class='alert-flash alert alert-success' role='alert'>Sold Listings Title has been successfully updated!</div>";

			} else {

				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to update Sold Listings Title!</div>";
			}
			
		echo json_encode(array('status' => $status, 'msg' => $msg));exit;

		}
	}
	// layout builder dependency
	public function update_builder(){
		if($this->input->is_ajax_request()) {
			// this is where I left off. save json data to database.
			$this->load->model('client_theme_model', 'client_theme');

			header('Content-Type: application/json');

			if(!isset($_POST['layout']) || !isset($_POST['theme'])){
				echo json_encode(array(
					'success' => false,
					'message' => 'Missing either layout_builder or theme parameter'
				));
				exit();
			}

			$data['layout'] = $_POST['layout'];
			$data['theme'] = $_POST['theme'];
			$data['settings'] = $_POST['settings'];
			$data['agent_id'] = $this->session->userdata('agent_id');

			if($this->client_theme->update($this->session->userdata('agent_id'), $data)){
				echo json_encode(array(
					'success' => true,
					'message' => 'Successfully updated agent\'s theme settings'
				));
			}else{
				echo json_encode(array(
					'success' => false,
					'message' => 'Failed to update agent\'s theme settings'
				));
			}
		}
	}
	// layout builder dependency
	public function getSavedSearches($page = 1, $limit = 20){
		
		//$endpoint = "_pagination=1&_orderby=-ModificationTimestamp&_expand=ReferencingQuickSearchIds";
		//$response = Modules::run('dsl/getSparkProxy', 'savedsearches', $endpoint);
		if($this->input->is_ajax_request()){

			$page = isset($_POST['page'])? $_POST['page'] : $page;
			$limit = isset($_POST['limit'])? $_POST['limit'] : $limit;
			$agent_id = isset($_POST['agent_id'])? $_POST['agent_id'] : $this->session->userdata('agent_id');

			$filter = '';
			if(isset($_POST['filter'])){

				$filter_string = rawurlencode( $_POST['filter'] );

				$filter = "&_filter=Name%20Eq%20contains('".$filter_string."')";
			}

			$endpoint = "_limit={$limit}&_page={$page}{$filter}&_pagination=1&_orderby=-ModificationTimestamp&_expand=ReferencingQuickSearchIds";
			$response = Modules::run('dsl/getSparkProxyComplete', 'savedsearches', $endpoint, false, $agent_id);
			// printa(array( // for debugging purposes #POSTMAN
			// 	'_POST' => $_POST,
			// 	'filter_string' => $filter_string,
			// 	'endpoint' => $endpoint,
			// 	'response' => $response
			// )); exit();
			header('Content-Type: application/json');

			if(!empty($response->Results) || $response !== null){
				echo json_encode($response);
			}else{
				echo '{ Success: false }';
			}
		}else{
			$endpoint = "_limit={$limit}&_page={$page}&_pagination=1&_orderby=-ModificationTimestamp&_expand=ReferencingQuickSearchIds";
			$response = Modules::run('dsl/getSparkProxyComplete', 'savedsearches', $endpoint);

			if(!empty($response->Results) || $response !== null){
				return $response;
			}
			return array();
		}
	}

	public function addSavedSearchOption(){

		if($this->input->is_ajax_request())
		{
		    if ( isset($_POST['featured_search_id']) )
            {
                $savedSearch  = $this->addFeaturedSavedSearchMysql();

                if ( $savedSearch['status'] !== 'error' )
                {
                    $savedSearch  = $this->addFeaturedSavedSearchDsl();
                }
            }


			if ( isset($_POST['add_saved_search']) )
            {
                $savedSearch = $this->addSelectedSavedSearchMysql();

                if ( $savedSearch['status'] !== 'error' )
                {
                    $savedSearch = $this->addSelectedSavedSearchDsl();
                }
            }

			echo json_encode(array('status' => $savedSearch['status'], 'msg' => $savedSearch['message']));
			exit;
		}
	}

	public function addSoldListingsOption($selectedId = array()){

		if($this->input->is_ajax_request()) {
			$selectedSoldListingsId = array();
			$selected_id = !empty($selectedId) ? $selectedId : $this->input->post('add_sold_listings');

			if(!empty($selected_id)){

				foreach($selected_id as $sold_id){

					$selectedSoldListingsData = $this->custom->get_sold_listings_data($sold_id);

					if(!empty($selectedSoldListingsData)){
						foreach($selectedSoldListingsData as $selected){
							$this->custom->add_selected_sold_listings($selected);
						}
					}
		    }
          $status = "success";
    			$msg = "<div class='alert-flash alert alert-success' role='alert'>Sold Listings has been successfully added!</div>";
	    } else {
	    	  $status = "error";
	     		$msg = "<div class='alert-flash alert alert-danger' role='alert'>Select Sold Listings to featured on homepage!</div>";
	    }
			
			echo json_encode(array('status' => $status, 'msg' => $msg));exit;
		}
	}

    /**
    * @param array $selectedId
    * @return array
    */
    public function addSelectedSavedSearchMysqlOld( $selectedId = array())
    {
        $selected_id =  !empty($selectedId) ? $selectedId : $this->input->post('add_saved_search');

        if(!empty($selected_id))
        {
        	$savedSearches = Modules::run('dsl/getAllAgentSavedSearchFromSpark');
     	 	$dsl_savesearchId = array();

			if(!empty($savedSearches))
			{

				$savedSearchpages = $savedSearches['pages'];
				$page = $savedSearchpages + 1;

				for($i=1; $i<=$page; $i++) 
				{

					$results = $this->idx_auth->getSavedSearches($i);

					foreach($results['results'] as $result)
					{
						$dsl_savesearchId[] = $result['Id'];
					}
				}
			}

            //compare savedsearch id of $_POST to dsl
            $update_selected = array_intersect($selected_id, $dsl_savesearchId);

            if( count($update_selected) > 0 )
            {
                foreach($update_selected as $search_id)
                {
                    $selectedSavedSearchData = $this->getSavedSearchData($search_id);

                    if(!empty($selectedSavedSearchData))
                    {
                        foreach($selectedSavedSearchData as $selected)
                        {
                            $this->custom->add_selected_saved_search($search_id, $selected['Name'], $selected['Filter']);
                        }
                    }
                }

                $status = "success";
                $msg = "<div class='alert-flash alert alert-success' role='alert'>Saved Search has been successfully added!</div>";

            } else {

                $status = "error";
                $msg = "<div class='alert-flash alert alert-danger' role='alert'>Cannot add this saved search at this moment! Please try again later!</div>";
            }

            return array( 'status' => $status, 'message' => $msg );
        }
    }

    /**
    * @param array $selectedId
    * @return array
    */
    public function addSelectedSavedSearchMysql( $selectedId = array())
    {

        $selected_id =  !empty($selectedId) ? $selectedId : $this->input->post('add_saved_search');

        if(!empty($selected_id))
        {
	        foreach($selected_id as $search_id)
	        {
	            $selectedSavedSearchData = $this->getSavedSearchData($search_id);

	            if(!empty($selectedSavedSearchData))
	            {
	                foreach($selectedSavedSearchData as $selected)
	                {
	                    $this->custom->add_selected_saved_search($search_id, $selected['Name'], $selected['Filter']);
	                }
	            }
	        }

	        $status = "success";
	        $msg = "<div class='alert-flash alert alert-success' role='alert'>Saved Search has been successfully added!</div>";
            
        } else {

	        $status = "error";
	        $msg = "<div class='alert-flash alert alert-danger' role='alert'>Select Savedsearch to saved!</div>";
           
        }
        return array( 'status' => $status, 'message' => $msg );
    }

	public function addSelectedSavedSearchDslOld( $selectedId = array() )
    {
		$dsl = modules::load('dsl/dsl');
		$sdsl_savesearchId = array();
		$selected_id = !empty($selectedId) ? $selectedId : $this->input->post('add_saved_search');

		$savedSearches = Modules::run('dsl/getAllAgentSavedSearchFromSpark');
    	$dsl_savesearchId = array();

		if(!empty($savedSearches))
		{
			$savedSearchpages = $savedSearches['pages'];
			$page = $savedSearchpages + 1;

			for($i=1; $i<=$page; $i++) 
			{

				$results = $this->idx_auth->getSavedSearches($i);

				foreach($results['results'] as $result)
				{
					$dsl_savesearchId[] = $result['Id'];
				}
			}
		}

		if(!empty($selected_id))
		{
			//comapare savedsearch id of $_POST to dsl
			$update_selected = array_intersect($selected_id, $dsl_savesearchId);

			if(!empty($update_selected) )
			{
				foreach($update_selected as $search_id)
				{
                    $addSelected = array(
                        'isSelected' => 1
                    );

					$dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$search_id.'/update', $addSelected);
	            }

	            $status = "success";
      			$msg = "<div class='alert-flash alert alert-success' role='alert'>Saved Search has been successfully added!</div>";

			} else {

				$status = "error";
     			$msg = "<div class='alert-flash alert alert-danger' role='alert'>Cannot add this savedsearch at this moment! Please try again later!</div>";
			}

			return array( 'status' => $status, 'message' => $msg );
        } 
	}

	public function addSelectedSavedSearchDsl( $selectedId = array() )
    {
		$dsl = modules::load('dsl/dsl');
		$sdsl_savesearchId = array();
		$selected_id = !empty($selectedId) ? $selectedId : $this->input->post('add_saved_search');

		if(!empty($selected_id) )
		{
			foreach($selected_id as $search_id)
			{
                $addSelected = array(
                    'isSelected' => 1
                );

				$dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$search_id.'/update', $addSelected);
            }

            $status = "success";
  			$msg = "<div class='alert-flash alert alert-success' role='alert'>Saved Search has been successfully added!</div>";

		} else {

			$status = "error";
 			$msg = "<div class='alert-flash alert alert-danger' role='alert'>Select Savedsearch to saved!</div>";
		}

		return array( 'status' => $status, 'message' => $msg );
	}

	public function addFeaturedSavedSearchMysql($search_id = null){

		$dsl = modules::load('dsl/dsl');
		$saved_search_id = !empty($search_id) ? $search_id : $this->input->post('featured_search_id');

		if(!empty($saved_search_id)){
			$featuredSavedSearch = $this->getSavedSearchData($saved_search_id);

			if(!empty($featuredSavedSearch)){

			  //add featured saved search data to saved_search_options	
			  $this->custom->add_featured_saved_search($featuredSavedSearch[0]);

			  //add featured saved search listings data to featured_saved_search_listings
			  $token = $this->custom->getUserAccessToken($this->session->userdata("agent_id"));
		      $access_token = array(
		          'access_token' => $token->access_token
		      );

		      $filterStr = "(".$featuredSavedSearch[0]['Filter'].")";
		      $filter_url = rawurlencode($filterStr);
		      $filter_string = substr($filter_url, 3, -3);

		      $endpoint = "spark-listings?_pagination=1&_page=1&_limit=6&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=-OnMarketDate";
		      $results = $dsl->post_endpoint($endpoint, $access_token);
		      $featured_saved_search_results = json_decode($results);

		      if(isset($featured_saved_search_results->data) && !empty($featured_saved_search_results->data)){

					$json = json_encode($featured_saved_search_results->data);
					$json_properties = gzencode($json, 9);

					$data = array(
						'user_id' => $this->session->userdata("user_id"),
						'saved_search_id' => $featuredSavedSearch[0]['Id'],
						'listings' => $json_properties,
						'date_created' => date('Y-m-d H:i:s')
					);
					
					$this->custom->insert_featured_saved_search_listings($data);

					  $status = "success";
          			  $msg = "<div class='alert-flash alert alert-success' role='alert'>Saved Search has been successfully added!</div>";

				} else{

					$data = array(
						'user_id' => $this->session->userdata("user_id"),
						'saved_search_id' => $featuredSavedSearch[0]['Id'],
						'listings' => '',
						'date_created' => date('Y-m-d H:i:s')
					);
					
					$this->custom->insert_featured_saved_search_listings($data);

					$status = "error";
         			$msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to add saved search. Saved search is empty!</div>";

				}

			} else {

				$status = "error";
         		$msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to add saved search!</div>";
			}

			return array( 'status' => $status, 'message' => $msg );	
		}	
	}

	public function addFeaturedSavedSearchDsl($search_id = null){

		$search_id = !empty($search_id) ? $search_id  : $this->input->post("featured_search_id");

		if(!empty($search_id)){

			$dsl = modules::load('dsl/dsl');

			//remove old featured saved search
			$old_saved_search_featured = Modules::run('dsl/getFeaturedAgentSavedSearch');
			if(!empty($old_saved_search_featured)){

				$old_featured_id = $old_saved_search_featured[0]['Id'];

				$removeFeatured = array( 'isFeatured' => 0 );

				$dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$old_featured_id.'/update', $removeFeatured);
			}

			//remove db featured property
			$this->custom->removeFeaturedProperty();
			
			//add new featured saved search
			$addFeatured = array( 'isFeatured' => 1 );

			$dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$search_id.'/update', $addFeatured);

			//saved featured saved search listings in database
			$dsl_featured_saved_search = Modules::run('dsl/getFeaturedAgentSavedSearch');

			//check if dsl featured savedsearch is not empty
			if(!empty($dsl_featured_saved_search)){
				$featured_saved_search = $dsl_featured_saved_search[0] ;
			}else{
				$featured_saved_search = $this->custom->get_featured_saved_search();
			}

			if(!empty($featured_saved_search)){

		      $token = $this->custom->getUserAccessToken($this->session->userdata("agent_id"));
		      $access_token = array(
		          'access_token' => $token->access_token
		      );

		      $filterStr = "(".$featured_saved_search['Filter'].")";
		      $filter_url = rawurlencode($filterStr);
		      $filter_string = substr($filter_url, 3, -3);

		      $endpoint = "spark-listings?_pagination=1&_page=1&_limit=6&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=-OnMarketDate";
		      $results = $dsl->post_endpoint($endpoint, $access_token);
		      $featured_saved_search_results = json_decode($results);

		      if(isset($featured_saved_search_results->data) && !empty($featured_saved_search_results->data)){

					$json = json_encode($featured_saved_search_results->data);
					$json_properties = gzencode($json, 9);

					$data = array(
						'user_id' => $this->session->userdata("user_id"),
						'saved_search_id' => $featured_saved_search['Id'],
						'listings' => $json_properties,
						'date_created' => date('Y-m-d H:i:s')
					);
					
					$this->custom->insert_featured_saved_search_listings($data);

					  $status = "success";
          			  $msg = "<div class='alert-flash alert alert-success' role='alert'>Featured Saved search has been successfully added!</div>";

				} else{

					$data = array(
						'user_id' => $this->session->userdata("user_id"),
						'saved_search_id' => $featured_saved_search['Id'],
						'listings' => '',
						'date_created' => date('Y-m-d H:i:s')
					);
					
					$this->custom->insert_featured_saved_search_listings($data);

					if(!empty($featured_saved_search)){

						$featured_saved_search_id = $featured_saved_search['Id'];

						$removeFeatured = array( 'isFeatured' => 0 );

						$dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$featured_saved_search_id.'/update', $removeFeatured);
					}

					$status = "error";
         			$msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to add featured saved search. Saved search is empty!</div>";

				}

				return array( 'status' => $status, 'message' => $msg );	
		    }
		} 
	}

	public function getSavedSearchData($saved_search_id = NULL){

		$this->load->library("idx_auth", $this->session->userdata("agent_id"));
		return $this->idx_auth->getSavedSearch($saved_search_id);
	}

	public function removedFeaturedSavedSearch(){

		if($this->input->is_ajax_request()) {

			$savedsearch_id = $this->input->get("search_id");

			//remove featured saved search mysql
			$removeFeaturedSavedSearchMysql = $this->custom->remove_featured_saved_search($savedsearch_id);

			//remove featured saved search dsl
			$removeFeatured = array( 'isFeatured' => 0 );
			$dsl = modules::load('dsl/dsl'); 
			$removeFeaturedSavedSearchDsl = $dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$savedsearch_id.'/update', $removeFeatured);

			if($removeFeaturedSavedSearchDsl && $removeFeaturedSavedSearchMysql ){
				$status = "success";
	            $msg = "<div class='alert-flash alert alert-success' role='alert'>Featured Saved search has been successfully removed!</div>";
			}else{
				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to remove featured saved search!</div>";
			}
	
			echo json_encode(array('status' => $status, 'msg' => $msg));exit;
		}
	}

	public function removedSelectedSavedSearch(){

		if($this->input->is_ajax_request()) {

			$savedsearch_id = $this->input->get("search_id");

			//remove selected saved search mysql
			$removeSelectedSavedSearchMysql = $this->custom->remove_selected_saved_search($savedsearch_id);

			//remove selected saved search dsl
			$removeSelected = array( 'isSelected' => 0 );
			$dsl = modules::load('dsl/dsl'); 
			$removeSelectedSavedSearchDsl = $dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$savedsearch_id.'/update', $removeSelected);

			if($removeSelectedSavedSearchDsl && $removeSelectedSavedSearchMysql ){
				$status = "success";
	            $msg = "<div class='alert-flash alert alert-success' role='alert'>Selected Saved search has been successfully removed!</div>";
			}else{
				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to remove selected saved search!</div>";
			}
	
			echo json_encode(array('status' => $status, 'msg' => $msg));exit;
		}
	}

	public function removedSelectedSoldListings(){

		if($this->input->is_ajax_request()) {

			$soldListings_id = $this->input->get("sold_id");

			//remove selected ssold listings
			$removeSelectedSoldListings = $this->custom->remove_selected_sold_listings($soldListings_id);


			if($removeSelectedSoldListings){
				$status = "success";
	      $msg = "<div class='alert-flash alert alert-success' role='alert'>Selected Sold Listings has been successfully removed!</div>";
			}else{
				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to remove selected sold listings!</div>";
			}
	
			echo json_encode(array('status' => $status, 'msg' => $msg));exit;
		}
	}

	public function editSearchName(){

		if($this->input->is_ajax_request()) {

			$savedsearch_name = $this->input->post("search_name");
			$savedsearch_id = $this->input->get("search_id");

			//edit search name mysql
			$editSearchNameMysql = $this->custom->edit_saved_search_name($savedsearch_id,$savedsearch_name);

			//edit search name dsl
			$updateName = array( 'Name' => $savedsearch_name );

			$dsl = modules::load('dsl/dsl'); 
			$editSearchNameDsl = $dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$savedsearch_id.'/update', $updateName);

			if($editSearchNameDsl && $editSearchNameMysql ){
				$status = "success";
	            $msg = "<div class='alert-flash alert alert-success' role='alert'>Saved search name has been successfully updated!</div>";
			}else{
				$status = "error";
	            $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to update saved search name!</div>";
			}

			echo json_encode(array('status' => $status, 'msg' => $msg, 'savedsearch_name' => $savedsearch_name, 'savedsearch_id' =>$savedsearch_id));exit;
		}
	}

	public function editSoldName(){

		if($this->input->is_ajax_request()) {

			$soldListings_name = $this->input->post("sold_name");
			$sold_id = $this->input->get("sold_id");

			//edit search name mysql
			$editSoldName = $this->custom->edit_saved_search_name($sold_id,$soldListings_name);

			if($editSoldName){
				$status = "success";
	            $msg = "<div class='alert-flash alert alert-success' role='alert'>Sold Listings name has been successfully updated!</div>";
			}else{
				$status = "error";
	      $msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to update sold listings name!</div>";
			}

			echo json_encode(array('status' => $status, 'msg' => $msg, 'soldListings_name' => $soldListings_name, 'sold_id' =>$sold_id));exit;
		}
	}

	public function syncAgentSoldListings(){

		Modules::run('mysql_properties/syncAgentSoldListings', $this->session->userdata("user_id"), $this->session->userdata("agent_id"),FALSE);

		$this->session->set_flashdata('msg', '<h4 class="alert alert-flash alert-success text-center">Successfully Synced Sold Listings!</h4>');
		redirect("customize_homepage","refresh");
	}
}

?>
