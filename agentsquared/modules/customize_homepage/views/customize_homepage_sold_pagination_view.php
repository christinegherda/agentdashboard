<div role="tabpanel" class="tab-pane" id="sold-listings">
    <form id="add_soldListings_option" action="<?= base_url()?>customize_homepage/addSoldListingsOption" method="POST" class="form-horizontal add-menu-form">
         <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Check the box next to the sold listings you would like to displayed on your homepage.</p>
        <ul class="menu-list">
        <?php if( isset($soldListings) && !empty($soldListings) ) { 
                    foreach ($soldListings as $sold) :          
        ?>  
            <li>
                <label class="checkbox-inline col-md-8 col-xs-6">
                    <input type="checkbox" name="add_sold_listings[]" class="selected-soldListings-<?php echo $sold['listing_id'];?>" value="<?php echo $sold["listing_id"];?>"
                    
                        <?php if(!empty($selected_sold_listings)){ 

                            foreach($selected_sold_listings as $selected){
                                  echo ( $selected['listing_id'] == $sold['listing_id']) ? 'checked="checked" disabled': "";
                              }
                        }?>>

                        <?php echo ucwords(str_replace('"', '', $sold["UnparseAddress"]))?>
                </label>
            </li>

            <?php endforeach; ?>

            <div class="col-md-12">
                <?php if( isset($sold_ajax_pagination) ) : ?>
                    <div class="pagination-area pull-right">
                        <nav>
                            <ul class="pagination">
                                <?php echo $sold_ajax_pagination; ?>
                            </ul>
                        </nav>
                    </div>
                    <div class="pull-left">
                       <p style="font-size: 13px;">Note: Hit <strong>Save Options</strong> before you proceed to next page to save your selected sold listings.</p>
                    </div>
                <?php endif; ?>
                
            </div>

            <?php } else { ?>

                <p class="no-page-created text-center">
                    <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i>No Sold Listings will display on Homepage!
                </p>

            <?php }?>

        </ul>
             <?php if( isset($soldListings) && !empty($soldListings) ) { ?>
                <div class="clearfix">
                    <button type="submit" class="btn btn-default btn-submit btn-remove-attr-soldListings"><i style="display: none;" class="save-loading-add-soldListings fa fa-spinner fa-spin"></i> Save Options</button>
                </div>
            <?php }?>
    </form>
</div><!--#sold-listings -->



