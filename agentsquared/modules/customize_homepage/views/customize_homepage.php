<?php
    $this->load->view('session/header-custom');
    $this->load->view('session/top-nav');
?>
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span>Customize Homepage</span>
    </h3>
  </div>
  <section class="content">
    <?php if(isset($theme_config) && !empty($theme_config) && $theme_config){?>
      <div class="disclaimer alert alert-info">
        Suggestion: We recommend to show less than or equal to 4 widgets only. This is to prevent your site from getting slow response rate. The more you add elements on your homepage, the slower it loads.
      </div>
      <div class="controls">
        Layout Builder Canvas <span role="button" class="toggler-container"><input type="checkbox" name="hide-others" id="hide-others-toggler"/> <label for="hide-others">Hide Other ELements</label></span> <button id="btn-save-layout" class="btn btn-darkblue">Save</button><button id="btn-reset-layout" class="btn btn-grey">Reset to Default</button>
      </div>
      <div class="page-representation">
        <?php if(isset($theme_config['layout']) && isset($theme_config['layout']['homepage'])){?>
          <?php foreach($theme_config['layout']['homepage'] as $section){?>
            <?php if($section['name'] == 'layout-builder'){?>
              <div id="layout-builder" class="layout-builder">
                <button id="add-row"><i class="fa fa-plus"></i> Add Row</button>
              </div>
            <?php }else{ ?>
              <div id="<?= $section['name']?>">
                <img src="<?php echo str_replace('[base_url]', base_url(), $section['image']);?>" alt="<?= $section['name']?>" title="<?= $section['name']?>"/>
              </div>
            <?php }?>
          <?php }?>
        <?php }else{ ?>
        <div id="layout-builder" class="layout-builder">
          <button id="add-row">Add Row</button>
        </div>
        <?php }?>
      </div>
    <?php }else{?>
      <div class="alert alert-danger">Missing or invalid theme config file. Please contact support.</div>
    <?php }?>
  </section>
</div>
<?php $this->load->view('session/footer-custom'); ?>