<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>
    <div class="content-wrapper">
      <div class="page-title">
        <div class="col-md-7 col-sm-6">
            <h3 class="heading-title">
              <span>Customize Homepage</span>
            </h3>
        </div>
        <div class="col-md-5 col-sm-6 col-md-3 pull-right">
            <div class="otherpage video-box video-collapse">
                <div class="row">
                  <div class="col-md-5 col-sm-5 col-xs-5">
                    <h1>Need Help?</h1>
                    <p>See how by watching this short video.</p>
                    <div class="checkbox">
                        <input type="checkbox" id="hide" name="hide" value="hide">
                        <label for="hide">Minimize this tip.</label>
                    </div>
                  </div>
                  <div class="col-md-1">
                    <div class="text-center">
                      <span class="fa fa-long-arrow-right"></span>
                    </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-7">
                    <div class="tour-thumbnail tour-video">
                       <img src="<?php echo base_url()."assets/images/dashboard/v_customizehomepage.png"; ?>" alt="Google Analytics" title="Google Analytics Guide" style="max-width: 100%;"/>
                     </div>
                  </div>
                </div>
            </div>
        </div>
      </div>
        <section class="content">
            <?php $this->load->view('session/launch-view'); ?>
            <div class="container-fluid customize-homepage">
                <div class="row">
                    <div class="col-md-4">
                        <h4> Choose which listings you would like to display on your homepage </h4><br>
                          <div class="flash-data">
                            <?php $session = $this->session->flashdata('session');
                                if( isset($session) ){?>

                                    <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
                            <?php } ?>
                            </div>
                             <div class="show-notif"></div>
                            <div class="home-options">
                                 <div class="panel-group" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab">
                                            <h4 class="panel-title">
                                                <a href="javascript:void(0);">Homepage</a>
                                                <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse collapse in moreDetails" role="tabpanel">
                                            <div class="panel-body savesearch-tab">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#options" aria-controls="profile" role="tab" data-toggle="tab">Options</a></li>
                                                    <li role="presentation"><a href="#saved-search" aria-controls="home" role="tab" data-toggle="tab">Saved Search</a></li>
                                                    <li role="presentation"><a href="#sold-listings" aria-controls="sold-listings" role="tab" data-toggle="tab">Sold Listings</a></li>
                                                </ul>

                                                <div class="tab-content clearfix">
                                                    <div role="tabpanel" class="tab-pane active" id="options">
                                                     <form id="add_homepage_option" action="<?= base_url()?>customize_homepage/add_options" method="POST" class="form-horizontal add-menu-form">
                                                        <ul class="menu-list">
                                                            <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Check the box next to the listings you would like to be displayed on your homepage. Set featured to have this listing category show up first.</p>
                                                           <?php if( isset($home_options) && !empty($home_options) ) {
                                                                    foreach ($home_options as $option) :
                                                            ?>

                                                                <li>
                                                                  <?php
                                                                    if($option->option_name == "active_listings" && empty($active_listings) || $option->option_name == "office_listings" && empty($office_listings) || $option->option_name == "new_listings" && empty($new_listings) || $option->option_name == "nearby_listings" && empty($nearby_listings) ){?>

                                                                        <label style="display:none" class="checkbox-inline col-md-8 col-xs-6">
                                                                            <input type="checkbox" name="add_option[]" class="selected-<?php echo $option->id;?>" value="<?php echo $option->id;?>" <?php echo ( $option->option_value == "1" && $option->is_featured != "1" ) ? 'checked="checked"': ""; ?>  > <?php echo ucwords($option->option_title);?>
                                                                        </label>

                                                                        <label style="display:none" class="radio-inline col-md-4 col-xs-6">
                                                                            <input class="radio" type="radio" name="set_featured[]" class="featured-<?php echo $option->id;?>" value="<?php echo $option->id;?>" <?php echo ( $option->is_featured == "1" ) ? 'checked="checked"': ""; ?> />Set as Featured
                                                                        </label>

                                                                  <?php  } else { ?>

                                                                        <label class="checkbox-inline col-md-8 col-xs-7">
                                                                            <input type="checkbox" name="add_option[]" class="selected-<?php echo $option->id;?>" value="<?php echo $option->id;?>" <?php echo ( $option->option_value == "1" || $option->is_featured == "1" ) ? 'checked="checked" ': ""; ?>  <?php echo ( $option->option_value == "1" || $option->is_featured == "1" ) ? "disabled": ""; ?> > <span class="selected-<?php echo $option->id;?>"><?php echo ucwords($option->option_title);?></span>
                                                                        </label>

                                                                        <label class="radio-inline col-md-4 col-xs-5 paddingmobile-0">
                                                                            <input class="radio featured-<?php echo $option->id;?>" type="radio" name="set_featured[]" value="<?php echo $option->id;?>" <?php echo ( $option->is_featured == "1" ) ? 'checked="checked"': ""; ?> />Set as Featured
                                                                        </label>

                                                                    <?php  }?>

                                                                </li>

                                                            <?php endforeach; ?>
                                                            <?php } else { ?>

                                                                <p class="no-page-created text-center">
                                                                    <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No Featured Properties will display on Homepage!
                                                                </p>

                                                            <?php }?>
                                                        </ul>

                                                            <?php if(isset($home_options) && !empty($home_options)){ ?>
                                                            <div class="clearfix default-save-options">
                                                                <button type="submit" class="btn btn-default btn-submit btn-remove-attr-addoption"><i style="display: none;" class="save-loading-add-option fa fa-spinner fa-spin"></i> Save options</button>
                                                            </div>
                                                            <?php }?>
                                                        </form>
                                                    </div><!--#options -->
                                                    <div role="tabpanel" class="tab-pane" id="saved-search">
                                                     <form id="add_savedSearch_option" action="<?= base_url()?>customize_homepage/addSavedSearchOption" method="POST" class="form-horizontal add-menu-form">
                                                        <ul class="menu-list">
                                                            <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Check the box next to the saved searches you would like to displayed on your homepage.</p>
                                                        <?php if( isset($savedsearches) && !empty($savedsearches) ) {
                                                                    foreach ($savedsearches as $saved) :
                                                        ?>
                                                            <li>
                                                                <label class="checkbox-inline col-md-8 col-xs-7">
                                                                <input type="checkbox" name="add_saved_search[]" class="selected-savedSearch-<?php echo $saved['Id'];?>" value="<?php echo $saved['Id'];?>" 

                                                                    <?php if(!empty($selected_saved_search)){ 

                                                                        foreach($selected_saved_search as $selected){
                                                                              echo ( $selected['Id'] == $saved['Id']) ? 'checked="checked" disabled': "";
                                                                          }
                                                                      }?>

                                                                    > <?php echo ucwords($saved['Name']);?>
                                                                </label>

                                                              <label class="radio-inline col-md-4 col-xs-5 paddingmobile-0">
                                                                    <input class="radio featured-savedSearch-<?php echo $saved['Id'];?>" type="radio" name="featured_search_id" value="<?php echo $saved['Id'];?>" 

                                                                    <?php if(isset($featured_saved_search['Id'])){ 
                                                                        echo ( $featured_saved_search['Id'] == $saved['Id']) ? 'checked="checked"': "";
                                                                    }?>

                                                                     /> Set as Featured
                                                                </label>
                                                            </li>

                                                            <?php endforeach; ?>

                                                             <div class="col-md-12">
                                                                <?php if( isset($ajax_pagination) ) : ?>
                                                                    <div class="pagination-area pull-right">
                                                                        <nav>
                                                                            <ul class="pagination">
                                                                                <?php echo $ajax_pagination; ?>
                                                                            </ul>
                                                                        </nav>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                       <p style="font-size: 13px;">Note: Hit <strong>Save Options</strong> before you proceed to next page to save your selected saved search.</p>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>

                                                            <?php } else { ?>

                                                                <div class="no-page-created text-center">
                                                                    <p class="text-center">No Saved Search found!</p>
                                                                    <p class="text-center">Click here to Sync Saved Searches <a href="<?=base_url()?>agent_sites/saved_searches">Sync Saved Searches</a></p>
                                                                </div>

                                                            <?php }?>

                                                        </ul>
                                                             <?php if( isset($savedsearches) && !empty($savedsearches) ) { ?>
                                                                <div class="clearfix">
                                                                     <button type="submit" class="btn btn-default btn-submit btn-remove-attr-addsavedsearch"><i style="display: none;" class="save-loading-add-savedsearch fa fa-spinner fa-spin"></i> Save Options</button>
                                                                </div>
                                                            <?php }?>
                                                        </form>
                                                    </div><!--#saved search -->
                                                    <div role="tabpanel" class="tab-pane" id="sold-listings">
                                                     <form id="add_soldListings_option" action="<?= base_url()?>customize_homepage/addSoldListingsOption" method="POST" class="form-horizontal add-menu-form">
                                                         <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Check the box next to the sold listings you would like to displayed on your homepage.</p>
                                                        <ul class="menu-list">
                                                           
                                                        <?php if( isset($soldListings) && !empty($soldListings) ) {
                                                                    foreach ($soldListings as $sold) :
                                                        ?>
                                                            <li>
                                                                <label class="checkbox-inline col-md-8 col-xs-7">
                                                                <input type="checkbox" name="add_sold_listings[]" class="selected-soldListings-<?php echo $sold['listing_id'];?>" value="<?php echo $sold['listing_id'];?>" 

                                                                <?php if(!empty($selected_sold_listings)){ 

                                                                    foreach($selected_sold_listings as $selected){
                                                                          echo ( $selected['listing_id'] == $sold['listing_id']) ? 'checked="checked" disabled': "";
                                                                      }
                                                                }?>>

                                                                <?php echo ucwords(str_replace('"', '', $sold["UnparseAddress"]))?>
                                                                </label>
                                                            </li>

                                                            <?php endforeach; ?>

                                                             <div class="col-md-12">
                                                                <?php if( isset($sold_ajax_pagination) ) : ?>
                                                                    <div class="pagination-area pull-right">
                                                                        <nav>
                                                                            <ul class="pagination">
                                                                                <?php echo $sold_ajax_pagination; ?>
                                                                            </ul>
                                                                        </nav>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                       <p style="font-size: 13px;">Note: Hit <strong>Save Options</strong> before you proceed to next page to save your selected sold listings.</p>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>

                                                            <?php } else { ?>

                                                                <div class="no-page-created text-center">
                                                                    <p class="text-center">No Sold Listings found!</p>
                                                                    <p class="text-center">Click here to Sync Sold Listings <a href="<?=base_url()?>customize_homepage/syncAgentSoldListings">Sync Sold Listings</a></p>
                                                                </div>

                                                            <?php }?>

                                                        </ul>
                                                             <?php if( isset($soldListings) && !empty($soldListings) ) { ?>
                                                                <div class="clearfix">
                                                                     <button type="submit" class="btn btn-default btn-submit btn-remove-attr-addsoldListings"><i style="display: none;" class="save-loading-add-soldListings fa fa-spinner fa-spin"></i> Save Options</button>
                                                                </div>
                                                            <?php }?>
                                                        </form>
                                                    </div><!--#sold-listings -->
                                                </div><!--.tab-content -->
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-offset-0 col-md-8">
                             <div class="panel-group" id="options-accordion">

                             <!-- Featured Options -->
                            <?php if(isset($is_featured_option) && !empty($is_featured_option)){?>
                                <div class="featured-option-info">
                                     <h3>Featured Property</h3>
                                    <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Select the green arrow next to the listing to rename or remove from your homepage.</p>
                                </div>

                                <?php foreach($is_featured_option as $featured){?> 

                                    <div id="option-item-<?php echo $featured->id?>" role="tablist" class="save-search-accordion">
                                        <div id="option-item-<?php echo $featured->id?>" class="card">
                                            <div class="card-header" role="tab">
                                              <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" href="#featured<?php echo $featured->id?>" aria-expanded="false" aria-controls="option<?php echo $featured->id?>">
                                                   <div class="featured-option-name-<?php echo $featured->id?>"><?php echo ucwords($featured->option_title)?></div>
                                                  <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span>
                                                </a>
                                              </h5>
                                            </div>
                                            <div id="featured<?php echo $featured->id?>" class="collapse" role="tabpanel" aria-labelledby="#featured<?php echo $featured->id?>" data-parent="#accordion">
                                              <div class="card-body">
                                              <form  id="homepage-option" data-id="<?php echo $featured->id?>" action="<?=base_url()?>customize_homepage/edit_option_title?option_title=<?php echo $featured->id?>" method="POST">
                                                    <div class="row form-group">
                                                        <div class="col-md-4">
                                                            <label>Navigation Label</label>
                                                        </div>
                                                         <div class="col-md-8 navigation-label">
                                                             <input type="hidden" name="option_id" value="<?php echo $featured->id?>">
                                                             <input type="text" name="option_title" id="saved-<?php echo $featured->id?>" class="form-control" value="<?php echo $featured->option_title?>" maxlength="50" pattern="[a-zA-Z0-9-_./$\s]+" required>
                                                         </div>
                                                        <div class="col-md-12">
                                                            <span class="remove-menu" id="remove_menu_1"><a class="remove-homepage-option" data-id="<?php echo $featured->id?>" href="<?=base_url()?>customize_homepage/remove_featured?option_id=<?php echo $featured->id?>">Remove</a></span> <span class="menu-separator"> | </span><span class="cancel-menu"> <a role="button" data-toggle="collapse" data-parent="#options-accordion" href="#featured<?php echo $featured->id?>">Cancel</a></span>
                                                        </div>
                                                        <div class="col-md-12">
                                                             <button style="margin-top:5px" type="submit" class="btn btn-default pull-right btn-submit btn-remove-attr-homeoption btn-savesearch-name"><i style="display: none;" class="save-loading-option-title fa fa-spinner fa-spin"></i> Save</button>
                                                        </div>
                                                    </div>
                                                    
                                                </form>
                                              </div>
                                            </div>
                                          </div>
                                    </div>
                            <?php } 
                             
                            }?>

                            <!-- Saved Search Featured Options -->
                            <?php if(!empty($featured_saved_search)){?>

                                <div class="featured-option-info">
                                    <h3>Featured Property</h3>
                                     <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Select the green arrow next to the Saved Search to rename or remove from your homepage.</p>
                                </div>

                                    <div id="featuredsaved-item-<?php echo $featured_saved_search['Id']?>" role="tablist" class="save-search-accordion">
                                      <div id="option-item-<?php echo $featured_saved_search['Id']?>" class="card">
                                        <div class="card-header" role="tab">
                                          <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#savedFeatured<?php echo $featured_saved_search['Id']?>" aria-expanded="false" aria-controls="option<?php echo $featured_saved_search['Id']?>">
                                               <div class="featured-option-name-<?php echo $featured_saved_search['Id']?>"><?php echo ucwords($featured_saved_search['Name'])?></div>
                                              <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span>
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="savedFeatured<?php echo $featured_saved_search['Id']?>" class="collapse" role="tabpanel" aria-labelledby="#savedFeatured<?php echo $featured_saved_search['Id']?>" data-parent="#accordion">
                                          <div class="card-body">
                                            <form  id="saved-search-name" data-id="<?php echo $featured_saved_search['Id']?>" action="<?=base_url()?>customize_homepage/editSearchName?search_id=<?php echo $featured_saved_search['Id']?>" method="POST">
                                                <div class="row form-group">
                                                    <div class="col-md-4">
                                                        <label>Navigation Label</label>
                                                    </div>
                                                     <div class="col-md-8 navigation-label">
                                                         <input type="text" name="search_name" id="search-name-<?php echo $featured_saved_search['Id']?>" class="form-control" value="<?php echo $featured_saved_search['Name']?>" maxlength="50" pattern="[a-zA-Z0-9-_./$\s]+" required>
                                                     </div>
                                                    <div class="col-md-12">
                                                        <span class="remove-menu" id="remove_menu_1"><a class="remove-featured-savedSearch" data-id="<?php echo $featured_saved_search['Id']?>" href="<?=base_url()?>customize_homepage/removedFeaturedSavedSearch?search_id=<?php echo $featured_saved_search['Id']?>">Remove</a></span> <span class="menu-separator"> | </span><span class="cancel-menu"> <a role="button" data-toggle="collapse" data-parent="#options-accordion" href="#savedFeatured<?php echo $featured_saved_search['Id']?>">Cancel</a></span>
                                                    </div>
                                                    <div class="col-md-12">
                                                         <button style="margin-top:5px" type="submit" class="btn btn-default pull-right btn-submit btn-remove-attr btn-savesearch-name"><i style="display: none;" class="save-loading-option-title fa fa-spinner fa-spin"></i> Save</button>
                                                    </div>
                                                </div>
                                                
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                <?php  
                            }?>

                            <!-- Homepage Options -->
                            <?php if(isset($is_selected_option) && !empty($is_selected_option)){ ?>

                                    <div class="homepage-option-info">
                                     <h3>Homepage Area</h3>
                                     <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Select the green arrow next to the listing to rename or remove from your homepage.</p>
                                    </div>
                                    <div id="accordion" role="tablist" class="save-search-accordion">
                                    <?php foreach($is_selected_option as $selected){?>    
                                      <div id="option-item-<?php echo $selected->id?>" class="card">
                                        <div class="card-header" role="tab">
                                          <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#option<?php echo $selected->id?>" aria-expanded="false" aria-controls="option<?php echo $selected->id?>">
                                               <div class="featured-option-name-<?php echo $selected->id?>"><?php echo ucwords($selected->option_title)?></div>
                                              <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span>
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="option<?php echo $selected->id?>" class="collapse" role="tabpanel" aria-labelledby="#option<?php echo $selected->id?>" data-parent="#accordion">
                                          <div class="card-body">
                                            <form  id="homepage-option" action="<?=base_url()?>customize_homepage/edit_option_title?option_id=<?php echo $selected->id?>" method="POST">
                                                <div class="row form-group">
                                                    <div class="col-md-4">
                                                        <label>Navigation Label</label>
                                                    </div>
                                                     <div class="col-md-8 navigation-label">
                                                         <input type="hidden" name="option_id" value="<?php echo $selected->id?>">
                                                         <input type="text" name="option_title" id="saved-<?php echo $selected->id?>" class="form-control" value="<?php echo $selected->option_title?>" maxlength="50" pattern="[a-zA-Z0-9-_./$\s]+" required>
                                                     </div>
                                                    <div class="col-md-12">
                                                        <span class="remove-menu" id="remove_menu_1"><a class="remove-homepage-option" data-id="<?php echo $selected->id?>" href="<?=base_url()?>customize_homepage/remove_option?option_id=<?php echo $selected->id?>">Remove</a></span> <span class="menu-separator"> | </span><span class="cancel-menu"> <a role="button" data-toggle="collapse" data-parent="#options-accordion" href="#option<?php echo $selected->id?>">Cancel</a></span>
                                                    </div>
                                                    <div class="col-md-12">
                                                         <button style="margin-top:5px" type="submit" class="btn btn-default pull-right btn-submit btn-remove-attr-homeoption btn-savesearch-name"><i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Save</button>
                                                    </div>
                                                </div>
                                                
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    <?php } ?>
                                    </div>
                            <?php } ?>
                                
                            </div>
                        </div><!-- End of Homepage area--->
                        <div class="col-md-offset-0 col-md-8">
                            <!-- Saved Search Options -->
                            <?php if(!empty($selected_saved_search)){?>

                             <h3>Saved Search Area</h3>
                            <p class="info-text"> <i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Saved Searches are created inside your mls and imported to your AgentSquared dashboard. Use saved searches to customize your website and showcase real estate that you specialise in.</p>

                                <div class="home-options">
                                     <div class="panel-group" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab">
                                                <h4 class="panel-title">
                                                    <form id="saved-search-title" action="<?=base_url()?>customize_homepage/update_saved_search_title" method="POST">
                                                        <a href="javascript:void(0);">Saved Search Name</a>
                                                        <input type="text" name="saved_search_title" value="<?= isset($saved_search_title["option_title"]) ? $saved_search_title["option_title"] : "Recommended Listings"?>" class="form-saved-name" maxlength="50" pattern="[a-zA-Z0-9-_./$\s]+" required>
                                                        <div class="pull-right">
                                                        <?php
                                                          if($this->config->item('disallowUser')) { ?>
                                                            <a href="?modal_premium=true" class="btn btn-default btn-submit" data-toggle="modal" data-target="#modalPremium">Update Title</a>
                                                        <?php
                                                          } else { ?>
                                                            <button type="submit" class="btn btn-default btn-submit btn-remove-attr"><i style="display: none;" class="save-loading-update-title fa fa-spinner fa-spin"></i> Update Title</button>
                                                        <?php
                                                          }
                                                        ?>
                                                        </div>
                                                    </form>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse collapse in moreDetails save-search-content" role="tabpanel">
                                                <div id="accordion" role="tablist" class="save-search-accordion">

                                                    <?php foreach($selected_saved_search as $saved_search){?>  

                                                      <div id="saved-item-<?php echo $saved_search['Id']?>" class="card">
                                                        <div class="card-header" role="tab" id="saved-item-<?php echo $saved_search['Id']?>">
                                                          <h5 class="mb-0">
                                                            <a class="collapsed" data-toggle="collapse" href="#saved<?php echo $saved_search['Id']?>" aria-expanded="false" aria-controls="saved<?php echo $saved_search['Id']?>">
                                                              <div class="featured-savedsearch-name-<?php echo $saved_search['Id']?>"><?php echo ucwords($saved_search['Name'])?></div>
                                                              <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span>
                                                            </a>
                                                          </h5>
                                                        </div>
                                                        <div id="saved<?php echo $saved_search['Id']?>" class="collapse" role="tabpanel" aria-labelledby="saved-item-<?php echo $saved_search['Id']?>" data-parent="#accordion">
                                                          <div class="card-body">
                                                            <form  id="saved-search-name" action="<?=base_url()?>customize_homepage/editSearchName?search_id=<?php echo $saved_search['Id']?>" method="POST">
                                                              <div class="row form-group">
                                                                <div class="col-md-4">
                                                                    <label>Navigation Label</label>
                                                                </div>
                                                                 <div class="col-md-8 navigation-label">
                                                                    <input type="hidden" name="search_id" value="<?php echo $saved_search['Id']?>">
                                                                     <input type="text" name="search_name" id="search-name-<?php echo $saved_search['Id']?>" class="form-control" value="<?php echo $saved_search['Name']?>" maxlength="50" pattern="[a-zA-Z0-9-_./$\s]+" required>
                                                                 </div>
                                                                <div class="col-md-12">
                                                                  <span class="remove-menu" id="remove_menu_1">
                                                                  <?php
                                                                    if($this->config->item('disallowUser')) { ?>
                                                                      <a href="?modal_premium=true" data-toggle="modal" data-target="#modalPremium">
                                                                  <?php
                                                                    } else { ?>
                                                                    <a class="remove-saved-search" data-id="<?php echo $saved_search['Id']?>" href="<?=base_url()?>customize_homepage/removedSelectedSavedSearch?search_id=<?php echo $saved_search['Id']?>">
                                                                    <?php } ?>
                                                                    Remove
                                                                    </a>
                                                                  </span>
                                                                  <span class="menu-separator"> | </span>
                                                                  <span class="cancel-menu">
                                                                  <?php
                                                                    if($this->config->item('disallowUser')) { ?>
                                                                      <a href="?modal_premium=true" data-toggle="modal" data-target="#modalPremium">
                                                                  <?php
                                                                    } else { ?>
                                                                    <a role="button" data-toggle="collapse" data-parent="#options-accordion" href="#saved<?php echo $saved_search['Id']?>">
                                                                  <?php } ?>
                                                                  Cancel
                                                                    </a>
                                                                  </span>
                                                                </div>
                                                                <div class="col-md-12">
                                                                <?php
                                                                  if($this->config->item('disallowUser')) { ?>
                                                                  <a href="?modal_premium=true" style="margin-top:5px" class="btn btn-default pull-right btn-submit" data-toggle="modal" data-target="#modalPremium">
                                                                <?php
                                                                  } else { ?>
                                                                  <button style="margin-top:5px" type="submit" class="btn btn-default pull-right btn-submit btn-remove-attr btn-savesearch-name">
                                                                    <i style="display: none;" class="save-loading-savedsearch-title fa fa-spinner fa-spin"></i>
                                                                <?php } ?>
                                                                    Save
                                                                  </button>
                                                                </div>
                                                              </div>
                                                            </form>
                                                          </div>
                                                        </div>
                                                    </div>

                                                <?php } ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                        <?php } 
                                 ?>
                    </div><!-- end of saved-search-area-->

                    <div class="col-md-offset-0 col-md-8">
                            <!-- Saved Search Options -->
                            <?php if(!empty($selected_sold_listings)){?>

                             <h3>Sold Listings Area</h3>
                            <p class="info-text"> <i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Sold Listings are listings inside your mls and imported to your AgentSquared dashboard. Select sold listings to feature on your homepage.</p>

                                <div class="home-options">
                                     <div class="panel-group" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab">
                                                <h4 class="panel-title">
                                                    <form id="sold-listings-title" action="<?=base_url()?>customize_homepage/update_sold_listings_title" method="POST">
                                                        <a href="javascript:void(0);">Sold Listings Name</a>
                                                        <input type="text" name="sold_listings_title" value="<?= isset($sold_listings_title["option_title"]) ? $sold_listings_title["option_title"] : "Recent Sold Properties"?>" class="form-saved-name" maxlength="50" pattern="[a-zA-Z0-9-_./$\s]+" required>
                                                        <div class="pull-right">
                                                        <?php
                                                          if($this->config->item('disallowUser')) { ?>
                                                            <a href="?modal_premium=true" class="btn btn-default btn-submit" data-toggle="modal" data-target="#modalPremium">Update Title</a>
                                                        <?php
                                                          } else { ?>
                                                            <button type="submit" class="btn btn-default btn-submit btn-remove-attr"><i style="display: none;" class="save-loading-update-title fa fa-spinner fa-spin"></i> Update Title</button>
                                                        <?php
                                                          }
                                                        ?>
                                                        </div>
                                                    </form>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse collapse in moreDetails save-search-content" role="tabpanel">
                                                <div id="accordion" role="tablist" class="save-search-accordion">

                                                    <?php foreach($selected_sold_listings as $sold){?>  

                                                      <div id="sold-item-<?php echo $sold['listing_id']?>" class="card">
                                                        <div class="card-header" role="tab" id="sold-item-<?php echo $sold['listing_id']?>">
                                                          <h5 class="mb-0">
                                                            <a class="collapsed" data-toggle="collapse" href="#sold<?php echo $sold['listing_id']?>" aria-expanded="false" aria-controls="sold<?php echo $sold['listing_id']?>">
                                                              <div class="selected-soldListings-name-<?php echo $sold['listing_id']?>"><?php echo ucwords(str_replace('"', '', $sold["UnparseAddress"]))?></div>
                                                              <span class="pull-right"><i class="indicator fa fa-caret-down pointer"></i></span>
                                                            </a>
                                                          </h5>
                                                        </div>
                                                        <div id="sold<?php echo $sold['listing_id']?>" class="collapse" role="tabpanel" aria-labelledby="sold-item-<?php echo $sold['listing_id']?>" data-parent="#accordion">
                                                          <div class="card-body">
                                                            <form  id="sold-listings-name" action="<?=base_url()?>customize_homepage/editSoldName?sold_id=<?php echo $sold['listing_id']?>" method="POST">
                                                              <div class="row form-group">
                                                                <div class="col-md-4">
                                                                    <label>Navigation Label</label>
                                                                </div>
                                                                <!--  <div class="col-md-8 navigation-label">
                                                                    <input type="hidden" name="sold_id" value="<?php echo $sold['listing_id']?>">
                                                                     <input type="text" name="sold_name" id="sold-name-<?php echo $sold['listing_id']?>" class="form-control" value="<?php echo ucwords(str_replace('"', '', $sold["UnparseAddress"]))?>" maxlength="50" pattern="[a-zA-Z0-9-_./$\s]+" required>
                                                                 </div> -->
                                                                <div class="col-md-12">
                                                                  <span class="remove-menu" id="remove_menu_1">
                                                                  <?php
                                                                    if($this->config->item('disallowUser')) { ?>
                                                                      <a href="?modal_premium=true" data-toggle="modal" data-target="#modalPremium">
                                                                  <?php
                                                                    } else { ?>
                                                                    <a class="remove-sold-listings" data-id="<?php echo $sold['listing_id']?>" href="<?=base_url()?>customize_homepage/removedSelectedSoldListings?sold_id=<?php echo $sold['listing_id']?>">
                                                                    <?php } ?>
                                                                    Remove
                                                                    </a>
                                                                  </span>
                                                                  <span class="menu-separator"> | </span>
                                                                  <span class="cancel-menu">
                                                                  <?php
                                                                    if($this->config->item('disallowUser')) { ?>
                                                                      <a href="?modal_premium=true" data-toggle="modal" data-target="#modalPremium">
                                                                  <?php
                                                                    } else { ?>
                                                                    <a role="button" data-toggle="collapse" data-parent="#options-accordion" href="#sold<?php echo $sold['listing_id']?>">
                                                                  <?php } ?>
                                                                  Cancel
                                                                    </a>
                                                                  </span>
                                                                </div>
                                                                <div class="col-md-12">
                                                                <?php
                                                                  if($this->config->item('disallowUser')) { ?>
                                                                  <a href="?modal_premium=true" style="margin-top:5px" class="btn btn-default pull-right btn-submit" data-toggle="modal" data-target="#modalPremium">
                                                                <?php
                                                                  } else { ?>
                                                                 <!--  <button style="margin-top:5px" type="submit" class="btn btn-default pull-right btn-submit btn-remove-attr btn-soldListings-name">
                                                                    <i style="display: none;" class="save-loading-soldListings-title fa fa-spinner fa-spin"></i>
                                                                <?php } ?>
                                                                    Save
                                                                  </button> -->
                                                                </div>
                                                              </div>
                                                            </form>
                                                          </div>
                                                        </div>
                                                    </div>

                                                <?php } ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                        <?php } 
                                 ?>

                    </div><!--End of Sold Listings Area-->
        </section>
    </div>

    <?php $this->load->view('session/footer'); ?>
