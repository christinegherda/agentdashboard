
 <div role="tabpanel" class="tab-pane" id="saved-search">
    <form id="add_savedSearch_option" action="<?= base_url()?>customize_homepage/addSavedSearchOption" method="POST" class="form-horizontal add-menu-form">
        <ul class="menu-list">
            <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Check the box next to the saved searches you would like to displayed on your homepage.</p>
        <?php if( isset($savedsearches) && !empty($savedsearches) ) { 
                    foreach ($savedsearches as $saved) :          
        ?>  
            <li>
                <label class="checkbox-inline col-md-8 col-xs-6">
                    <input type="checkbox" name="add_saved_search[]" class="selected-savedSearch-<?php echo $saved['Id'];?>" value="<?php echo $saved["Id"];?>"
                    
                        <?php if(!empty($selected_saved_search)){ 

                            foreach($selected_saved_search as $selected){
                                  echo ( $selected['Id'] == $saved['Id']) ? 'checked="checked" disabled': "";
                              }
                          }?>

                     > <?php echo ucwords($saved['Name']);?>
                </label>

               <label class="radio-inline col-md-4 col-xs-6">
                    <input class="radio featured-savedSearch-<?php echo $saved['Id'];?>" type="radio" name="featured_search_id" value="<?php echo $saved["Id"];?>" 
                        <?php if(isset($featured_saved_search['Id'])){ 
                            echo ( $featured_saved_search['Id'] == $saved['Id']) ? 'checked="checked"': "";
                        }?> 
                    />Set as Featured
                </label>
            </li>

            <?php endforeach; ?>

            <div class="col-md-12">
                <?php if( isset($ajax_pagination) ) : ?>
                    <div class="pagination-area pull-right">
                        <nav>
                            <ul class="pagination">
                                <?php echo $ajax_pagination; ?>
                            </ul>
                        </nav>
                    </div>
                    <div class="pull-left">
                       <p style="font-size: 13px;">Note: Hit <strong>Save Options</strong> before you proceed to next page to save your selected saved search.</p>
                    </div>
                <?php endif; ?>
                
            </div>

            <?php } else { ?>

                <p class="no-page-created text-center">
                    <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i>No Saved Searches will display on Homepage!
                </p>

            <?php }?>

        </ul>
             <?php if( isset($savedsearches) && !empty($savedsearches) ) { ?>
                <div class="clearfix">
                    <button type="submit" class="btn btn-default btn-submit btn-remove-attr-savedsearch"><i style="display: none;" class="save-loading-add-savedsearch fa fa-spinner fa-spin"></i> Save Options</button>
                </div>
            <?php }?>
    </form>
</div><!--#saved search -->


