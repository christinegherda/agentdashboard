<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Customize_homepage_model extends CI_Model { 

	public function __construct()
    {
        parent::__construct();
    }

    public function getUserAccessToken( $agent_id = NULL ) {
        /* Distinguising between Bearer and Auth Token users */
        $this->db->select("auth_type")
                ->from("users")
                ->where("agent_id", $agent_id);
        $ret = $this->db->get()->row();
        $type = $ret->auth_type;

        if($type) {
             $this->db->select("bearer_token as access_token")
                ->from("user_bearer_token")
                ->where("agent_id", $agent_id);
        }
        else {
             $this->db->select("access_token")
                ->from("users_idx_token")
                ->where("agent_id", $agent_id);
        }
        
        return $this->db->get()->row();
    }

    

    public function insert_featured_saved_search_listings($data = array()) {

        if($data) {

            $this->db->where('user_id', $data['user_id']);

            $query = $this->db->get("featured_saved_search_listings");

            if($query->num_rows() > 0) {

                $arr = array(
                    'saved_search_id' => $data['saved_search_id'],
                    'listings' => $data['listings'],
                    'date_modified' => date('Y-m-d H:i:s')
                );

                $this->db->where('user_id', $data['user_id']);
                $this->db->update('featured_saved_search_listings', $arr);

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;

            } else {

                $this->db->insert("featured_saved_search_listings", $data);

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;
            }
        }

    }

    public function get_all_options()
    {

        $this->db->select("*")
                ->from("home_options")
                ->where("agent_id", $this->session->userdata('agent_id'))
                ->where("option_name !=", "saved_search_title")
                ->where("option_name !=", "sold_listings_title");
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();
        }

        return FALSE;
    }

    public function add_options()
    {
        if(!empty($_POST['add_option'])){
            if(gettype($_POST['add_option'])=="array"){
                foreach($_POST['add_option'] as $val){
                 $option_id = $val;

                   $data = array(
                        'option_value' => "1"
                    );

                    $this->db->where('id', $option_id);
                    $this->db->where('agent_id', $this->session->userdata('agent_id'));
                    $this->db->update('home_options', $data);
                }

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;
            }
        }
    }


    public function set_featured_options()
    {
        if(!empty($_POST['set_featured'])){
            if(gettype($_POST['set_featured'])=="array"){
                foreach($_POST['set_featured'] as $val){
                    
                    $option_id = $val;

                    $this->db->where('user_id', $this->session->userdata('user_id'));
                    $this->db->where('saved_search_type', 'featured');
                    $query = $this->db->get("saved_search_options");
                    $oldSSfeatured = $query->row_array();

                    $this->db->where('saved_search_id', $oldSSfeatured['saved_search_id']);
                    $this->db->where('user_id', $this->session->userdata('user_id'));
                    $this->db->delete('saved_search_options');

            
                    $removeFeatured = array(
                        'is_featured' => "0"
                    );

                    $this->db->where('agent_id', $this->session->userdata('agent_id'));
                    $this->db->update('home_options', $removeFeatured);


                    $setFeatured = array(
                        'is_featured' => "1"
                    );

                    $this->db->where('id', $option_id);
                    $this->db->where('agent_id', $this->session->userdata('agent_id'));
                    $this->db->update('home_options', $setFeatured);

                }

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;
            }
        }

    }

    public function remove_option( $option_id= NULL )
    {

       $this->db->set('option_value', 0)
            ->where("id", $option_id)
            ->update('home_options');

        return TRUE;
    }

     public function remove_featured( $option_id= NULL )
    {

       $this->db->set('is_featured', 0)
            ->where("id", $option_id)
            ->update('home_options');

        return TRUE;
    }

    public function get_selected_options($filter_data = NULL)
    {

        $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'option_value' => 1,
            'is_featured'  => 0,
            );

        $filter_data = !empty($filter_data)? $filter_data :  "";

        $this->db->select("*")
                ->from("home_options")
                ->where_not_in("option_name",$filter_data)
                ->where($array);

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

     public function get_featured_options()
    {

        $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'is_featured' => 1,
            );
        $this->db->select("*")
                ->from("home_options")
                ->where($array);

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function update_option_title( $option_id = NULL )
    {

        $data = array(
               'option_title' => $this->input->post("option_title")
            );

        $this->db->where('id', $option_id);
        $this->db->update('home_options', $data); 

        return ( $this->db->affected_rows() ) ? TRUE : FALSE ;

    }

    public function insert_active_option($agent_id) {

        $active_option = array(
           'agent_id'       => $agent_id,
           'option_name'    => 'active_listings',
           'option_title'   => 'Active Listings',
           'option_value'   => 0
           
        );

        $this->db->insert('home_options', $active_option);
    }

     public function insert_new_option($agent_id) {

        $new_option = array(
           'agent_id'       => $agent_id,
           'option_name'    => 'new_listings',
           'option_title'   => 'New Listings',
           'option_value'   => 0
           
        );

        $this->db->insert('home_options', $new_option);
    }

     public function insert_nearby_option($agent_id) {

        $nearby_option = array(
           'agent_id'       => $agent_id,
           'option_name'    => 'nearby_listings',
           'option_title'   => 'Nearby Listings',
           'option_value'   => 0
           
        );

        $this->db->insert('home_options', $nearby_option);
    }

    public function insert_office_option($agent_id) {

        $office_option = array(
           'agent_id'       => $agent_id,
           'option_name'    => 'office_listings',
           'option_title'   => 'Office Listings',
           'option_value'   => 0
           
        );

        $this->db->insert('home_options', $office_option);
    }
    
    public function removeFeaturedProperty(){

        $array = array(
                'agent_id' => $this->session->userdata('agent_id')
        );

        $this->db->set('is_featured', 0)
                 ->where($array)
                 ->update('home_options');
    }

    public function update_saved_search_title(){

            $savedSearchTitle = $this->db->select("*")
            ->from("home_options")
            ->where("agent_id", $this->session->userdata('agent_id'))
            ->where("option_name", "saved_search_title")
            ->get()->row_array();

            if(!empty($savedSearchTitle) ) {

                if($savedSearchTitle['agent_id'] == $this->session->userdata('agent_id')){

                    $update["option_title"] = $this->input->post("saved_search_title");

                    $array = array(

                        "agent_id" => $this->session->userdata('agent_id'),
                        "option_name" => "saved_search_title"
                    );

                    $this->db->where( $array );

                    if( $this->db->update("home_options", $update) ) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                } 

            } else {

                $save["agent_id"] = $this->session->userdata('agent_id');
                $save["option_name"] = "saved_search_title";
                $save["option_title"] = $this->input->post("saved_search_title");
                
                if( $this->db->insert("home_options", $save) ) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
    }

    public function update_sold_listings_title(){

            $soldListingsTitle = $this->db->select("*")
            ->from("home_options")
            ->where("agent_id", $this->session->userdata('agent_id'))
            ->where("option_name", "sold_listings_title")
            ->get()->row_array();

            if(!empty($soldListingsTitle) ) {

                if($soldListingsTitle['agent_id'] == $this->session->userdata('agent_id')){

                    $update["option_title"] = $this->input->post("sold_listings_title");

                    $array = array(

                        "agent_id" => $this->session->userdata('agent_id'),
                        "option_name" => "sold_listings_title"
                    );

                    $this->db->where( $array );

                    if( $this->db->update("home_options", $update) ) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                } 

            } else {

                $save["agent_id"] = $this->session->userdata('agent_id');
                $save["option_name"] = "sold_listings_title";
                $save["option_title"] = $this->input->post("sold_listings_title");
                
                if( $this->db->insert("home_options", $save) ) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
    }
    public function get_saved_search_title(){

         $this->db->select("*")
            ->from("home_options")
            ->where("agent_id", $this->session->userdata('agent_id'))
            ->where("option_name", "saved_search_title");
        
        return $this->db->get()->row_array();
    }

    public function get_sold_listings_title(){

         $this->db->select("*")
            ->from("home_options")
            ->where("agent_id", $this->session->userdata('agent_id'))
            ->where("option_name", "sold_listings_title");
        
        return $this->db->get()->row_array();
    }

    public function add_selected_saved_search( $search_id= NULL, $saved_search_name = NULL, $saved_search_filter = NULL ){

        if($search_id){

            $data = array(
                'saved_search_id' => $search_id,
                'user_id' => $this->session->userdata("user_id"),
                'saved_search_name' => $saved_search_name,
                'saved_search_filter' => $saved_search_filter,
                'saved_search_type' => 'selected',
                'date_created' =>  date('Y-m-d H:i:s')
            );

            $this->db->insert("saved_search_options",$data);
        }

      return TRUE;
    }

    public function add_featured_saved_search( $data = array(), $user_id  = null ){

        if($data) {

            //remove other Featured Options and replace with Featured SavedSearch
            $this->db->set('is_featured', 0)
                ->where('agent_id',$this->session->userdata('agent_id'))
                ->update('home_options');

            $this->db->where('user_id', $this->session->userdata("user_id"));
            $this->db->where('saved_search_type', 'featured');
            $query = $this->db->get("saved_search_options");

            $savedSearchData = array(
                'user_id' => $user_id ? $user_id : $this->session->userdata("user_id"),
                'saved_search_id' => $data["Id"],
                'saved_search_name' => $data["Name"],
                'saved_search_filter' => $data["Filter"],
                'saved_search_type' => 'featured',
                'date_created' =>  date('Y-m-d H:i:s')
            );

            if($query->num_rows() > 0) {

                $updateData= array(
                    'saved_search_id' => $data["Id"],
                    'saved_search_name' => $data["Name"],
                    'saved_search_filter' => $data["Filter"],
                    'date_modified' =>  date('Y-m-d H:i:s')
                );

                $arr = array(
                    'user_id' => $user_id ? $user_id : $this->session->userdata("user_id"),
                    'saved_search_type' => 'featured'
                );

                $this->db->where($arr);
                $this->db->update('saved_search_options', $updateData);

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;

            } else {

                $this->db->insert("saved_search_options", $savedSearchData);

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;
            }
        } 
    }

    public function remove_featured_saved_search( $search_id= NULL ){

        $arr = array(
            'saved_search_id' => $search_id,
            'user_id' => $this->session->userdata("user_id"),
            'saved_search_type' => 'featured'
        );

        $this->db->where($arr);
        $this->db->delete("saved_search_options");

      return TRUE;
    }

    public function remove_selected_saved_search( $search_id= NULL ){

        $arr = array(
            'saved_search_id' => $search_id,
            'user_id' => $this->session->userdata("user_id"),
            'saved_search_type' => 'selected'
        );

        $this->db->where($arr);
        $this->db->delete("saved_search_options");

      return TRUE;
    }

    public function remove_selected_sold_listings( $sold_id= NULL ){

        if($sold_id){

            $arr = array(
            'listing_id' => $sold_id,
            'user_id' => $this->session->userdata("user_id")
        );

        $this->db->where($arr);
        $this->db->delete("sold_listings_options");

        }

      return TRUE;
    }

    public function get_featured_saved_search(){

          $this->db->select("id, user_id,saved_search_id as Id,saved_search_name as Name,saved_search_filter as Filter")
            ->from("saved_search_options")
            ->where("user_id", $this->session->userdata('user_id'))
            ->where("saved_search_type", "featured");
        
        return $this->db->get()->row_array();
    }

    public function get_selected_saved_search(){

         $this->db->select("id, user_id,saved_search_id as Id,saved_search_name as Name,saved_search_filter as Filter")
            ->from("saved_search_options")
            ->where("user_id", $this->session->userdata('user_id'))
            ->where("saved_search_type", "selected");
        
        return $this->db->get()->result_array();
    }

    public function edit_saved_search_name($savedsearch_id = NULL, $savedsearch_name = NULL){

        if(!empty($savedsearch_name)) {
            $arr = array(
                'saved_search_name' => $savedsearch_name,
                'date_modified' => date('Y-m-d H:i:s')
            );

            $this->db->where('user_id', $this->session->userdata('user_id'));
            $this->db->where('saved_search_id', $savedsearch_id);
            $this->db->update('saved_search_options', $arr);

            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }
    }

    public function get_sold_listings($counter = FALSE,  $param = array()) {

        $json_unparse_address  = "json_extract(listings, '$.StandardFields.UnparsedFirstLineAddress') AS UnparseAddress";
        
        $this->db->distinct();
        $this->db->select("$json_unparse_address,user_id,listing_id")
                ->from("sold_listings_data")
                ->where("user_id", $this->session->userdata('user_id'));

        if($counter) {
            return $this->db->count_all_results();
        }

        if(!empty($param["limit"])) { 
            $this->db->limit($param["limit"]);
        }

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);
        }

        $this->db->order_by("date_sold", "desc");
        
        $query = $this->db->get();

        if($query->num_rows()>0) {
            return $query->result_array();
        }

        return FALSE;
    }

    public function get_selected_sold_listings() {

        $json_unparse_address  = "json_extract(listings, '$.StandardFields.UnparsedFirstLineAddress') AS UnparseAddress";
        
        $this->db->distinct();
        $this->db->select("$json_unparse_address,sld.user_id,sld.listing_id")
            ->from("sold_listings_data sld")
            ->join("sold_listings_options slo", "sld.listing_id=slo.listing_id", "inner")
            ->where("sld.user_id", $this->session->userdata('user_id'))
            ->group_by("sld.listing_id")
            ->order_by("sld.date_sold","desc");

        $query = $this->db->get();

        if($query->num_rows()>0) {
            return $query->result_array();
        }

        return FALSE;
    }

    public function get_sold_listings_data($sold_id = NULL){

        if($sold_id){

            $json_unparse_address  = "json_extract(listings, '$.StandardFields.UnparsedFirstLineAddress') AS UnparseAddress";
            
            $this->db->distinct();
            $this->db->select("$json_unparse_address,user_id,listing_id")
                    ->from("sold_listings_data")
                    ->where("listing_id", $sold_id)
                    ->limit(1);

            $query = $this->db->get();

            if($query->num_rows()>0) {
                return $query->result_array();
            }
        }
        return FALSE;
    }

    public function add_selected_sold_listings($data = array()){

        $arr = array(
            'user_id' => $this->session->userdata('user_id'),
            'listing_id' => $data['listing_id'],
            'sold_listings_name' => str_replace('"', '', $data["UnparseAddress"]),
            'date_created' => date('Y-m-d H:i:s')
        );

        $this->db->insert('sold_listings_options', $arr);

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }

}