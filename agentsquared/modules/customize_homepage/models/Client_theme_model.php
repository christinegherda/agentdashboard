<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_theme_model extends CI_Model {
  public function __construct() {
    parent::__construct();

    $this->load->dbforge();
  }

  function checkTable(){
    if($this->db->table_exists('agent_theme_settings')){
      return true;
    }
    return false;
  }

  function createTable(){ // note: this is only for use in the dashboard.
    if(!$this->db->table_exists('agent_theme_settings')){
      // Table structure for table 'users_ga'
      $this->dbforge->add_field(array(
        'id' => array(
          'type' => 'INT',
          'constraint' => '11',
          'auto_increment' => TRUE
        ),
        'agent_id' => array(
          'type' => 'VARCHAR',
          'constraint' => '100',
        ),
        'theme' => array(
          'type' => 'VARCHAR',
          'constraint' => '20',
        ),
        'layout' => array(
          'type' => 'LONGTEXT',
        ),
        'settings' => array(
          'type' => 'LONGTEXT',
        ),
      ));
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('agent_theme_settings');
      return true;
    }
    return false;
  }

  function insert($data){
    $query = $this->get($data['agent_id']);
    if(empty($query)){
      if($this->db->insert('agent_theme_settings', $data)){
        return true;
      }
    }
    return false;
  }
  
  function get($agent_id = ''){
    $this->db->where('agent_id', $agent_id);
    $query = $this->db->get('agent_theme_settings')->row();
    if(!empty($query)){
      return $query;
    }
    return false;
  }
  
  function update($agent_id = '', $data){
    if($agent_id === ''){
      $agent_id = ci()->session->userdata("agent_id");
    }
    if($data){
      if($this->get($agent_id)){
        
        $this->db->where('agent_id', $agent_id);
        if($this->db->update('agent_theme_settings', $data)){
          return true;
        }
      }else{
        $data['agent_id'] = $agent_id;
        return $this->insert($data);
      }
    }
    return false;
  }
}
