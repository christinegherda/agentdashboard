<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MX_Controller  {

	function __construct() {
		parent::__construct();

		$this->load->library('Ajax_pagination');
		$this->load->model("Menu_model","menu");
		//$this->load->model("agent_sites/Agent_sites_model");
	}

	function index() {

		$data['title'] = "Menu";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["default_recent_pages"] = $this->menu->get_all_default_recent_pages();
		$data["custom_recent_pages"] = $this->menu->get_all_custom_recent_pages();
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		$data["js"] = array("menu/menu.js","menu/jquery.nestable.js","menu/jquery.nestable-custom.js","menu/jquery.nestable-plus.js","menu/jquery.nestable-custom-plus.js");
		$data['css'] = array("dashboard/menu.css");

		//default menu
		$data['default_dd_menu'] = $this->menu->select_default_menu_priority();
        $data['default_menu_data'] = $this->get_default_menuData($data['default_dd_menu']);
        $data['build_default_menu_dashboard'] = $this->build_default_menu_dashboard(0,$data['default_menu_data']);

        //custom menu
		$data['custom_dd_menu'] = $this->menu->select_custom_menu_priority();
        $data['custom_menu_data'] = $this->get_custom_menuData($data['custom_dd_menu']);
        $data['build_custom_menu_dashboard'] = $this->build_custom_menu_dashboard(0,$data['custom_menu_data']);

        $data['default_pages'] = $this->menu->get_all_default_pages();

        //custom menu pagination config
        $param["limit"] = 10;
        $total = count($this->menu->get_all_custom_pages());
        $config['target']      = '#all-custom-pages';
        $config["base_url"] = base_url().'/menu/custom_ajaxPaginationData';
        $config['total_rows']  = $total;
        $config['per_page']    = $param["limit"];
        $this->ajax_pagination->initialize($config);
      	$data["custom_ajax_pagination"] = $this->ajax_pagination->create_links();
        $data['custom_pages'] = $this->menu->get_all_custom_pages(array('limit'=>$param["limit"]));

		$this->load->view('menu', $data);
	}

    public function custom_ajaxPaginationData()
	{
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }

        $param["limit"] = 10;
        $total = count($this->menu->get_all_custom_pages());
        $config['target']      = '#all-custom-pages';
        $config["base_url"] = base_url().'/menu/custom_ajaxPaginationData';
        $config['total_rows']  = $total;
        $config['per_page']    = $param["limit"];
        $this->ajax_pagination->initialize($config);

        $data["custom_ajax_pagination"] = $this->ajax_pagination->create_links();
        $data['custom_pages'] = $this->menu->get_all_custom_pages(array('start'=>$offset,'limit'=>$param["limit"]));

        $this->load->view('custom_menu_pagination_view', $data, false);
    }

	public function get_default_menu_data(){

		$data['default_dd_menu'] = $this->menu->select_default_menu_priority();
		$data['default_menu_data'] = $this->get_default_menuData($data['default_dd_menu']);
        $data['build_default_menu_header'] = $this->build_default_menu_header(0,$data['default_menu_data']);
       // printA($data['menu_data']);exit;
        return $data['build_default_menu_header'];
	}

	public function get_custom_menu_data(){

		$data['custom_dd_menu'] = $this->menu->select_custom_menu_priority();
		$data['custom_menu_data'] = $this->get_default_menuData($data['custom_dd_menu']);
        $data['build_custom_menu_header'] = $this->build_custom_menu_header(0,$data['custom_menu_data']);
       // printA($data['menu_data']);exit;
        return $data['build_custom_menu_header'];
	}

	public function add_default_menu(){

		if($_POST){

			$this->menu->add_default_menu();
			$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Default menu has been successfully added!'));

		}

		redirect("menu", "refresh");
	}

	public function add_custom_menu(){

		if($_POST){

			$this->menu->add_custom_menu();
			$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Custom menu has been successfully added!'));

		}

		redirect("menu?custom_menu", "refresh");
	}

	public function add_custom_link_menu(){

		 if(!empty($this->input->post('custom_link_menu_name'))) {
		 	if($this->input->post('custom_link_menu_name') != strip_tags($this->input->post('custom_link_menu_name'))) {
		 		$this->session->set_flashdata('session' , array('status' => FALSE, 'message' => 'Invalid Value For Menu Name!'));
			} else {
				$this->menu->add_custom_link_menu();
				$this->session->set_flashdata('session' , array('status' => TRUE, 'message' => 'Custom menu has been successfully added!'));
			}
		}

		redirect("menu?custom_menu", "refresh");
	}

	public function remove_menu(){

		$pages_id = $this->get_all_agent_pages_id();

        if (isset($_GET['menu_id']) && is_numeric($_GET['menu_id'])){

            if(in_array($_GET['menu_id'], $pages_id,true)){

                if( $this->menu->remove_menu($_GET["menu_id"])){

					$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Menu has been successfully removed!'));
				} else {
					$this->session->set_flashdata('session' , array('status' => FALSE,'message'=>'Failed to remove menu!'));
				}

				$parentMenu = $this->menu->getParentMenu( $this->input->get('menu_id') );
				if(!empty($parentMenu)){
					foreach($parentMenu as $p){
						$this->menu->remove_parenting($p['id']);
					}
				}

            } else{
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to remove this menu!'));
                
            }

        } else{
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to remove menu!'));
        }

        $menu = $this->menu->get_menu_type( $this->input->get('menu_id'));

		if($menu["menu_type"] == "custom_menu"){
			redirect("menu?custom_menu", "refresh");
		} else {
			redirect("menu", "refresh");
		}

	}

	public function remove_custom_menu(){

		$pages_id = $this->get_all_agent_pages_id();

        if (isset($_GET['menu_id']) && is_numeric($_GET['menu_id'])){

            if(in_array($_GET['menu_id'], $pages_id,true)){

                if( $this->menu->remove_custom_menu($_GET["menu_id"])){

					$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Custom Menu has been successfully removed!'));
				} else {
					$this->session->set_flashdata('session' , array('status' => FALSE,'message'=>'Failed to remove custom menu!'));
				}

				$parentMenu = $this->menu->getParentMenu( $this->input->get('menu_id') );

				if(!empty($parentMenu)){
					foreach($parentMenu as $p){
						$this->menu->remove_parenting($p['id']);
					}
				}

            } else{
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to remove this custom menu!'));
                
            }

        } else{
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to remove menu!'));
        }

        redirect("menu?custom_menu", "refresh");		
	}

	public function edit_title(){

		$pages_id = $this->get_all_agent_pages_id();

        if (isset($_GET['menu_id']) && is_numeric($_GET['menu_id'])){

            if(in_array($_GET['menu_id'], $pages_id,true)){

                $update_title = $this->menu->update_title( $_GET["menu_id"]);

                if($update_title["success"]) {
					$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=> $update_title["message"]));
				} else {
					$this->session->set_flashdata('session' , array('status' => FALSE, 'message' => $update_title["message"]));
				}

            } else{
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to update page title!')); 
            }

        } else{
            
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to update page title!'));
        }
      	
      	redirect("menu", "refresh");
	}

	public function edit_page_title(){

		$pages_id = $this->get_all_agent_pages_id();

        if (isset($_GET['menu_id']) && is_numeric($_GET['menu_id'])){

            if(in_array($_GET['menu_id'], $pages_id,true)){

                $update_page_title = $this->menu->update_page_title( $_GET["menu_id"]);

                if($update_page_title["success"]) {
					$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=> $update_page_title["message"]));
				} else {
					$this->session->set_flashdata('session' , array('status' => FALSE, 'message' => $update_page_title["message"]));
				}

            } else{
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to update menu title!')); 
            }

        } else{
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to update menu title!'));
        }

       redirect("menu?custom_menu", "refresh");

	}

	public function edit_custom_title(){

		$pages_id = $this->get_all_agent_pages_id();

        if (isset($_GET['menu_id']) && is_numeric($_GET['menu_id'])){

            if(in_array($_GET['menu_id'], $pages_id,true)){

                $update_custom_title = $this->menu->update_page_title( $_GET["menu_id"]);

                if($update_custom_title["success"]) {
					$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=> $update_custom_title["message"]));
				} else {
					$this->session->set_flashdata('session' , array('status' => FALSE, 'message' => $update_custom_title["message"]));
				}

            } else{
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to update custom menu title!')); 
            }

        } else{
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to update custom menu title!'));
        }

        redirect("menu?custom_menu", "refresh");
	}

	public function build_default_menu_dashboard($parent_menu_id, $default_menuData)
	{

	    $ol = '';
	    $html = '';

	    if (isset($default_menuData['parents'][$parent_menu_id]))
	    {
	    	$base_url = base_url();

	        foreach ($default_menuData['parents'][$parent_menu_id] as $itemId){

	        	$hide = '';

	        	if($default_menuData['items'][$itemId]['page_type'] == "custom link"){

	        		$remove_url = "<a style='color:#fff' class='btn btn-default btn-submit' href='".$base_url."menu/remove_custom_menu?menu_id=".$default_menuData['items'][$itemId]['id']."'> Remove</a>";

	        	} else {

	        		$remove_url = "<a style='color:#fff' class='btn btn-default btn-submit' href='".$base_url."menu/remove_menu?menu_id=".$default_menuData['items'][$itemId]['id']."'> Remove</a>";
	        	}

	        	if($default_menuData['items'][$itemId]['slug'] == "find-a-home" || $default_menuData['items'][$itemId]['slug'] == "testimonials" || $default_menuData['items'][$itemId]['slug'] == "home"){

                   	$url = $base_url."menu/edit_title?menu_id=".$default_menuData['items'][$itemId]['id'];

                } elseif($default_menuData['items'][$itemId]['page_type'] == "custom link"){

                    $url = $base_url."menu/edit_custom_title?menu_id=".$default_menuData['items'][$itemId]['id'];

                } else {

                    $url = $base_url."menu/edit_page_title?menu_id=".$default_menuData['items'][$itemId]['id'];
                }
                
                if($default_menuData['items'][$itemId]['slug'] == "home"){
                	$input = ""; 
                	$button = "";
                	$edit_icon = "";
                	$trash_icon = "";
                } else if($default_menuData['items'][$itemId]['slug'] == "find-a-home" || $default_menuData['items'][$itemId]['slug'] == "buyer" || $default_menuData['items'][$itemId]['slug'] == "seller" || $default_menuData['items'][$itemId]['slug'] == "contact" || $default_menuData['items'][$itemId]['slug'] == "blog"){

                	$input = " <input type='text' class='form-control input-name' name='page_title' id='editInputName' maxlength='255' pattern='[a-zA-Z0-9-_.,\s]+'' title='Allowed characters: A-Z, 0-9, ., -, _' placeholder='Item name' value='".$default_menuData['items'][$itemId]['title']."'required>";

                	$button = "<button type ='submit' id = 'editButton' class ='btn btn-default btn-submit'>Update</button>";

                	$edit_icon = "<span style='right: -28px;' class='button-edit btn btn-default btn-xs pull-right' data-owner-id='".$default_menuData['items'][$itemId]['id']."'>
		                  <a href ='#' data-toggle ='modal' data-target ='#myModalEdit".$default_menuData['items'][$itemId]['id']."'<i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></span>";
		            $trash_icon = "";
                }else{

                	$input = " <input type='text' class='form-control input-name' name='page_title' id='editInputName' maxlength='255' pattern='[a-zA-Z0-9-_.,\s]+'' title='Allowed characters: A-Z, 0-9, ., -, _' placeholder='Item name' value='".$default_menuData['items'][$itemId]['title']."'required>";

                	$button = "<button type ='submit' id = 'editButton' class ='btn btn-default btn-submit'>Update</button>";

                	$edit_icon = "<span class='button-edit btn btn-default btn-xs pull-right' data-owner-id='".$default_menuData['items'][$itemId]['id']."'>
		                  <a href ='#' data-toggle ='modal' data-target ='#myModalEdit".$default_menuData['items'][$itemId]['id']."'<i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></span>";
		            $trash_icon = " <span class='button-delete btn btn-default btn-xs pull-right' data-owner-id='".$default_menuData['items'][$itemId]['id']."'>
	                  <a href ='#' data-toggle ='modal' data-target ='#myModal".$default_menuData['items'][$itemId]['id']."'<i class='fa fa-trash' aria-hidden='true'></i></a>
	                </span>";

                }

                if ($this->config->item('disallowUser')) {
	                if ($default_menuData['items'][$itemId]['slug'] == "buyer" || $default_menuData['items'][$itemId]['slug'] == "seller") {
	                	$hide = "hide";
	                }
                }

	            $html .= "<li class= 'dd-item dd3-item ".$hide."' data-depth='".$default_menuData['items'][$itemId]['parent_menu_id']."' data-id ='".$default_menuData['items'][$itemId]['id']."' data-slug ='".$default_menuData['items'][$itemId]['slug']."' data-name ='".$default_menuData['items'][$itemId]['title']."' data-pagetype ='".$default_menuData['items'][$itemId]['page_type']."'>
	            <div class='dd-handle dd3-handle'></div><div class='dd3-content'><span class='menu-text'>".$default_menuData['items'][$itemId]['title']."</span><span style='float:right' class='menu-page-type'>".ucwords($default_menuData['items'][$itemId]['page_type'])."</span></div>

	           	".$trash_icon."

				<!-- Modal -->
				<div class ='modal fade menu-modal' id ='myModal".$default_menuData['items'][$itemId]['id']."' tabindex ='-1' role ='dialog'
				   aria-labelledby ='myModalLabel' aria-hidden = 'true'>
				   <div class ='modal-dialog'>
				      <div class ='modal-content'>
				         <div class ='modal-header'>
				            <button type ='button' class ='close' data-dismiss ='modal' aria-hidden ='true'>
				                  &times;
				            </button>
				            <h4 class ='modal-title' id ='myModalLabel'>
				               Remove Menu
				            </h4>
				         </div>
				         <div class ='modal-body'>
				            Are you really want to remove <strong>".$default_menuData['items'][$itemId]['title']."</strong> menu?
				         </div>
				         <div class = 'modal-footer'>
				            ".$remove_url."

				            <!--<button type ='button' class ='btn btn-default btn-submit' data-dismiss ='modal'>
				               Close
				            </button>-->
				         </div>
				      </div><!-- /.modal-content -->
				   </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				".$edit_icon."

                <!-- Modal -->
				<div class ='modal fade menu-modal' id ='myModalEdit".$default_menuData['items'][$itemId]['id']."' tabindex ='-1' role ='dialog'
				   aria-labelledby ='myModalLabel' aria-hidden = 'true'>
				   <div class ='modal-dialog'>
				      <div class ='modal-content'>
				         <div class ='modal-header'>
				            <button type ='button' class ='close' data-dismiss ='modal' aria-hidden ='true'>
				                  &times;
				            </button>
				            <h4 class ='modal-title text-center' id ='myModalLabel'>
				               Editing <strong>".$default_menuData['items'][$itemId]['title']."</strong>
				            </h4>
				         </div>
				         <div class ='modal-body'>
				         <form id='menu-edit' action= '".$url."' method ='post'>
				            ".$input."
				         </div>
				         <div class = 'modal-footer'>
				            ".$button."
							</form>
				            <!--<button type ='button' class ='btn btn-default btn-submit' data-dismiss ='modal'>
				               Close
				            </button>-->

				         </div>
				      </div><!-- /.modal-content -->
				   </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

                <!--<span class='button-edit btn btn-default btn-xs pull-right'
                      data-owner-id='".$default_menuData['items'][$itemId]['id']."'>
                  <i class='fa fa-pencil-square-o' aria-hidden='true'></i>
                </span>-->";

	            // find childitems recursively
	            $html .= $this->build_default_menu_dashboard($itemId, $default_menuData);

	            $html .= '</li>';

	        }

	         $ol = "<ol class='dd-list'>".$html."</ol>";
	    }

	    return $ol;
	}

	public function build_custom_menu_dashboard($parent_menu_id, $custom_menuData)
	{

	    $ol = '';
	    $html = '';

	    if (isset($custom_menuData['parents'][$parent_menu_id]))
	    {
	    	$base_url = base_url();

	        foreach ($custom_menuData['parents'][$parent_menu_id] as $itemId){

	        	if($custom_menuData['items'][$itemId]['page_type'] == "custom link"){

	        		$remove_url = "<a class='btn btn-default btn-submit' style='color:#fff' href='".$base_url."menu/remove_custom_menu?menu_id=".$custom_menuData['items'][$itemId]['id']."'> Remove</a>";

	        	} else {

	        		$remove_url = "<a class='btn btn-default btn-submit' style='color:#fff' href='".$base_url."menu/remove_menu?menu_id=".$custom_menuData['items'][$itemId]['id']."'> Remove</a>";
	        	}

	        	if($custom_menuData['items'][$itemId]['slug'] == "find-a-home" || $custom_menuData['items'][$itemId]['slug'] == "testimonials"){

                   $url = $base_url."menu/edit_title?menu_id=".$custom_menuData['items'][$itemId]['id'];

                } elseif($custom_menuData['items'][$itemId]['page_type'] == "custom link"){

                     $url = $base_url."menu/edit_custom_title?menu_id=".$custom_menuData['items'][$itemId]['id'];

                } else {

                     $url = $base_url."menu/edit_page_title?menu_id=".$custom_menuData['items'][$itemId]['id'];
                }

	            $html .= "<li class= 'dd-item dd3-item' data-depth='".$custom_menuData['items'][$itemId]['parent_menu_id']."' data-id ='".$custom_menuData['items'][$itemId]['id']."' data-slug ='".$custom_menuData['items'][$itemId]['slug']."' data-name ='".$custom_menuData['items'][$itemId]['title']."' data-pagetype ='".$custom_menuData['items'][$itemId]['page_type']."'>
	            <div class='dd-handle dd3-handle'></div><div class='dd3-content'><span class='menu-text'>".$custom_menuData['items'][$itemId]['title']."</span><span style='float:right' class='menu-page-type'>".ucwords($custom_menuData['items'][$itemId]['page_type'])."</span></div>
	            <span class='button-delete btn btn-default btn-xs pull-right'
                      data-owner-id='".$custom_menuData['items'][$itemId]['id']."'>
                  <a href ='#' data-toggle ='modal' data-target ='#myModal".$custom_menuData['items'][$itemId]['id']."'<i class='fa fa-trash' aria-hidden='true'></i></a>
                </span>

				<!-- Modal -->
				<div class ='modal fade menu-modal' id ='myModal".$custom_menuData['items'][$itemId]['id']."' tabindex ='-1' role ='dialog'
				   aria-labelledby ='myModalLabel' aria-hidden = 'true'>
				   <div class ='modal-dialog'>
				      <div class ='modal-content'>
				         <div class ='modal-header'>
				            <button type ='button' class ='close' data-dismiss ='modal' aria-hidden ='true'>
				                  &times;
				            </button>
				            <h4 class ='modal-title' id ='myModalLabel'>
				               Remove Menu
				            </h4>
				         </div>
				         <div class ='modal-body'>
				            Are you really want to remove <strong>".$custom_menuData['items'][$itemId]['title']."</strong> menu?
				         </div>
				         <div class = 'modal-footer'>
				            ".$remove_url."

				            <!--<button type ='button' class ='btn btn-default btn-submit' data-dismiss ='modal'>
				               Close
				            </button>-->
				         </div>
				      </div><!-- /.modal-content -->
				   </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				 <span class='button-edit btn btn-default btn-xs pull-right'
                      data-owner-id='".$custom_menuData['items'][$itemId]['id']."'>
                  <a href ='#' data-toggle ='modal' data-target ='#myModalEdit".$custom_menuData['items'][$itemId]['id']."'<i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                </span>

                <!-- Modal -->
				<div class ='modal fade menu-modal' id ='myModalEdit".$custom_menuData['items'][$itemId]['id']."' tabindex ='-1' role ='dialog'
				   aria-labelledby ='myModalLabel' aria-hidden = 'true'>
				   <div class ='modal-dialog'>
				      <div class ='modal-content'>
				         <div class ='modal-header'>
				            <button type ='button' class ='close' data-dismiss ='modal' aria-hidden ='true'>
				                  &times;
				            </button>
				            <h4 class ='modal-title text-center' id ='myModalLabel'>
				               Editing <strong>".$custom_menuData['items'][$itemId]['title']."</strong>
				            </h4>
				         </div>
				         <div class ='modal-body'>
				         <form id='menu-edit' action= '".$url."' method ='post'>
				            <input type='text' class='form-control input-name' name='page_title' id='editInputName' maxlength='255' pattern='[a-zA-Z0-9-_.,\s]+'' title='Allowed characters: A-Z, 0-9, ., -, _' placeholder='Item name' value='".$custom_menuData['items'][$itemId]['title']."' required>
				         </div>
				         <div class = 'modal-footer'>
				            <button type ='submit' id = 'editButton' class ='btn btn-default btn-submit'>Update</button>
							</form>
				            <!--<button type ='button' class ='btn btn-default btn-submit' data-dismiss ='modal'>
				               Close
				            </button>-->

				         </div>
				      </div><!-- /.modal-content -->
				   </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

                <!--<span class='button-edit btn btn-default btn-xs pull-right'
                      data-owner-id='".$custom_menuData['items'][$itemId]['id']."'>
                  <i class='fa fa-pencil-square-o' aria-hidden='true'></i>
                </span>-->";

	            // find childitems recursively
	            $html .= $this->build_custom_menu_dashboard($itemId, $custom_menuData);

	            $html .= '</li>';

	        }

	         $ol = "<ol class='dd-list'>".$html."</ol>";
	    }

	    return $ol;
	}

	public function build_default_menu_header($parent_menu_id, $default_menuData)
		{
		    $html = '';
		  //printA( $menuData ); exit;
		    if (isset($default_menuData['parents'][$parent_menu_id]))

		    {
		    	$base_url = base_url();

		        $html = "<ul class='nav navbar-nav navbar-right main-nav nav-darken'>";

		        foreach ($default_menuData['parents'][$parent_menu_id] as $itemId)
		        {
		        	$slug           = $default_menuData['items'][$itemId]['slug'];
		        	$page_type      = $default_menuData['items'][$itemId]['page_type'];
		        	$title          = $default_menuData['items'][$itemId]['title'];
		        	$have_sub_menu  = $default_menuData['items'][$itemId]['have_sub_menu'];

		        	$is_submenu_icon = ($have_sub_menu == TRUE) ? " <span class='fa fa-angle-down pull-right' aria-hidden='true'></span>" : "" ;
		        	$has_dropdown    = ($have_sub_menu == TRUE) ? "has-dropdown" : "" ;


		        	if($slug === "home" || $slug === "buyer" || $slug === "seller" || $slug === "find-a-home" || $slug === "contact" || $slug === "blog" || $slug === "testimonials"){

		        		$html .= "<li class='parent-li ".$has_dropdown."'><a href=".$base_url.$slug."> ".$title."</a>" . $is_submenu_icon;

		        	} elseif($page_type === "custom link"){

		        		$html .= "<li class='parent-li ".$has_dropdown."'><a href=".$slug."> ".$title."</a>" . $is_submenu_icon;

		        	} else {


		        		$html .= "<li class='parent-li ".$has_dropdown."'><a href=".$base_url."home/page/".$slug."> ".$title."</a>" . $is_submenu_icon;
		        	}

		            // find childitems recursively
		            $html .= $this->build_default_menu_header($itemId, $default_menuData);

		             $html .= '</li>';
		        }

		        $html .= '</ul>';
		    }

		    return $html;
	}

	public function build_custom_menu_header($parent_menu_id, $custom_menuData)
		{
		    $html = '';
		  //printA( $menuData ); exit;
		    if (isset($custom_menuData['parents'][$parent_menu_id]))

		    {
		    	$base_url = base_url();

		        $html = "<ul class='custom-pages'>";

		        foreach ($custom_menuData['parents'][$parent_menu_id] as $itemId)
		        {
		        	$slug           = $custom_menuData['items'][$itemId]['slug'];
		        	$page_type      = $custom_menuData['items'][$itemId]['page_type'];
		        	$title          = $custom_menuData['items'][$itemId]['title'];
		        	$have_sub_menu  = $custom_menuData['items'][$itemId]['have_sub_menu'];

		        	$is_submenu_icon = ($have_sub_menu == TRUE) ? " <i class='fa fa-angle-down' aria-hidden='true'></i>" : "" ;
		        	$has_dropdown    = ($have_sub_menu == TRUE) ? "custom-pages-hasdropdown" : "" ;


		        	if($slug === "home" || $slug === "buyer" || $slug === "seller" || $slug === "find-a-home" || $slug === "contact" || $slug === "blog" || $slug === "testimonials"){

		        		$html .= "<li class='custom-li ".$has_dropdown."'><a href=".$base_url.$slug."> ".$title."</a>" . $is_submenu_icon;

		        	} elseif($page_type === "custom link"){

		        		$html .= "<li class='custom-li ".$has_dropdown."'><a href=".$slug." target='_blank'> ".$title."</a>" . $is_submenu_icon;

		        	} else {


		        		$html .= "<li class='custom-li ".$has_dropdown."'><a href=".$base_url."home/page/".$slug."> ".$title."</a>" . $is_submenu_icon;
		        	}

		            // find childitems recursively
		            $html .= $this->build_custom_menu_header($itemId, $custom_menuData);

		             $html .= '</li>';
		        }

		        $html .= '</ul>';
		    }

		    return $html;
	}


	public function get_default_menuData($default_dd_menu){

		$default_menuData = array(
		    'items' => array(),
		    'parents' => array(),

		);

		foreach($default_dd_menu as $dd){

		  $dd["have_sub_menu"] 	=  $this->default_isHaveSubMenu($dd);
		  $default_menuData['items'][$dd['id']] = $dd;
	      $default_menuData['parents'][$dd['parent_menu_id']][] = $dd['id'];
		}

		return $default_menuData;
		//printA($default_menuData);exit;
	}

	public function get_custom_menuData($custom_dd_menu){

		$custom_menuData = array(
		    'items' => array(),
		    'parents' => array(),

		);

		foreach($custom_dd_menu as $dd){

		  $dd["have_sub_menu"] 	=  $this->custom_isHaveSubMenu($dd);
		  $custom_menuData['items'][$dd['id']] = $dd;
	      $custom_menuData['parents'][$dd['parent_menu_id']][] = $dd['id'];
		}

		return $custom_menuData;
		//printA($custom_menuData);exit;
	}

	public function default_isHaveSubMenu( $list = array() ){

		$return  = $this->menu->default_isHaveSubMenu( $list["id"] );

		return $return;
	}

	public function custom_isHaveSubMenu( $list = array() ){

		$return  = $this->menu->custom_isHaveSubMenu( $list["id"] );

		return $return;
	}

	public function update_default_menu_priority() {

            $data = $this->input->post("product_data");
            //printA($data);exit;

            if (count($data)) {
                $update = $this->menu->update_default_priority_data($data);
                if ($update) {

                    $result = array(
                    	'status' => 'success',
                    	'redirect' => base_url().'menu'
                    );

                    $this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Menu Order has been successfully updated!'));
                } else {
                    $result['status'] = "error";
                    $this->session->set_flashdata('session' , array('status' => FALSE,'message'=>'Failed to update menu order!'));
                }
            } else {
                $result['status'] = "error";
            }
            echo json_encode($result);
    }

    public function update_custom_menu_priority() {

            $data = $this->input->post("product_data");
            //printA($data);exit;

            if (count($data)) {
                $update = $this->menu->update_custom_priority_data($data);
                if ($update) {

                    $result = array(
                    	'status' => 'success',
                    	'redirect' => base_url().'menu?custom_menu'
                    );

                    $this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Menu Order has been successfully updated!'));
                } else {
                    $result['status'] = "error";
                    $this->session->set_flashdata('session' , array('status' => FALSE,'message'=>'Failed to update menu order!'));
                }
            } else {
                $result['status'] = "error";
            }
            echo json_encode($result);
    }

    public function get_all_agent_pages_id(){

		$agent_pages_id = $this->menu->get_agent_pages_id();
        $pages_id = array();

        foreach($agent_pages_id as $ap_id){
            $pages_id[] = $ap_id->id;
        }

        return $pages_id;
	}

}?>
