<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>
    <div class="content-wrapper">
      <div class="page-title">
        <div class="col-md-7 col-sm-6">
            <h3 class="heading-title">
              <span> Menu Configuration</span>
            </h3>
        </div>
      </div>
       <div class="col-md-5 col-sm-6 col-md-3 pull-right">
          <div class="otherpage video-box video-collapse">
              <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-5">
                  <h1>Need Help?</h1>
                  <p>See how by watching this short video.</p>
                  <div class="checkbox">
                      <input type="checkbox" id="hide" name="hide" value="hide">
                      <label for="hide">Minimize this tip.</label>
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="text-center">
                    <span class="fa fa-long-arrow-right"></span>
                  </div>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-7">
                 <div class="tour-thumbnail tour-video">
                   <img src="<?php echo base_url()."assets/images/dashboard/v_menu.png"; ?>" alt="Google Analytics" title="Google Analytics Guide" style="max-width: 100%;"/>
                 </div>
                </div>
              </div>
          </div>
      </div>      
        <section class="content">
            <?php $this->load->view('session/launch-view'); ?>
            <div class="container-fluid">
                <?php if( !empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret")) ) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> Your website is not live yet and is not yet visible to the public. <a href="javascript:void(0)" class="btn btn-warning btn-warning-right btn-submit pull-right"><i class="fa fa-globe"></i> Launch your site now</a></h4>
                    </div>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <div class="flash-data">
                                <?php $session = $this->session->flashdata('session');
                                    if( isset($session) )
                                    {
                                ?>
                                    <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
                                <?php } ?>

                        </div>
                        <!-- Default Menu Area -->
                        <div id="default-page-area" class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href="javascript:void(0);">Default Pages</a>
                                        <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in moreDetails" role="tabpanel">
                                    <div class="panel-body">
                                        <div>
                                          <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Check the boxes below next to the custom pages you would like to publish on your website, then select Add to Default Menu.</p>
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#default-recent" aria-controls="home" role="tab" data-toggle="tab">Recent</a></li>
                                                <li role="presentation"><a href="#all-default-pages" aria-controls="profile" role="tab" data-toggle="tab">View All</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="default-recent">

                                                 <form action="<?= base_url()?>menu/add_default_menu" method="POST" class="form-horizontal add-menu-form">

                                                    <ul class="menu-list">

                                                    <?php if( !empty($default_recent_pages) ) {
                                                                foreach ($default_recent_pages as $rp) {
                                                                    $hide = '';

                                                                    if($rp->slug != "blog"){?>
                                                                        <?php if ($this->config->item('disallowUser')): ?>
                                                                            <?php if ($rp->slug == "seller" || $rp->slug == "buyer"): ?>
                                                                               <?php $hide = 'hide'; ?>
                                                                            <?php endif ?>
                                                                        <?php endif ?>

                                                                        <li class="<?php echo $hide; ?>">
                                                                            <label class="checkbox-inline ">
                                                                                <input type="checkbox" name="add_default_menu[]" value="<?php echo $rp->id;?>" <?php echo ( $rp->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($rp->title);?>
                                                                            </label>
                                                                        </li>
                                                              <?php }?>


                                                        <?php }?>

                                                        <?php } else { ?>

                                                            <p class="no-page-created text-center">
                                                                <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No page created yet!
                                                            </p>

                                                        <?php }?>

                                                    </ul>
                                                        <?php if(!empty($default_recent_pages)){?>
                                                             <button type="submit" class="btn btn-default btn-submit">Add to Default menu</button>
                                                        <?php }?>
                                                    </form>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="all-default-pages">
                                                 <form action="<?= base_url()?>menu/add_default_menu" method="POST" class="form-horizontal add-menu-form">
                                                    <ul class="menu-list">
                                                       <?php if( !empty($default_pages) ) {
                                                            $hide = '';
                                                                foreach ($default_pages as $page) {
                                                                    $hide = '';
                                                                    if($page->slug != "blog"){?>
                                                                        <?php if ($this->config->item('disallowUser')): ?>
                                                                            <?php if ($page->slug == "seller" || $page->slug == "buyer"): ?>
                                                                               <?php $hide = 'hide'; ?>
                                                                            <?php endif ?>
                                                                        <?php endif ?>

                                                                        <li class="<?php echo $hide; ?>">
                                                                            <label class="checkbox-inline">
                                                                                <input type="checkbox" name="add_default_menu[]" value="<?php echo $page->id;?>" <?php echo ( $page->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($page->title);?>
                                                                            </label>
                                                                        </li>

                                                                   <?php }
                                                        ?>

                                                        <?php } ?>
                                                        <?php } else { ?>

                                                            <p class="no-page-created text-center">
                                                                <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No page created yet!
                                                            </p>

                                                        <?php }?>

                                                    </ul>
                                                        <?php if(!empty($default_pages)){?>
                                                            <button type="submit" class="btn btn-default btn-submit">Add to Default menu</button>
                                                        <?php }?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <!-- Custom Menu Area -->
                        <div id="custom-page-area" class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href="javascript:void(0);">Custom Pages</a>
                                        <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in moreDetails" role="tabpanel">
                                    <div class="panel-body">
                                        <div>
                                          <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Check the boxes below next to the custom pages you would like to publish on your website, then select Add to Custom Menu.</p>
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#custom-recent" aria-controls="home" role="tab" data-toggle="tab">Recent</a></li>
                                                <li role="presentation"><a href="#all-custom-pages" aria-controls="profile" role="tab" data-toggle="tab">View All</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="custom-recent">
                                                 <form action="<?= base_url()?>menu/add_custom_menu" method="POST" class="form-horizontal add-menu-form">
                                                    <ul class="menu-list">

                                                    <?php if( !empty($custom_recent_pages) ) {
                                                                foreach ($custom_recent_pages as $rp) :
                                                    ?>
                                                        <li>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" name="add_custom_menu[]" value="<?php echo $rp->id;?>" <?php echo ( $rp->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($rp->title);?>
                                                            </label>
                                                        </li>

                                                        <?php endforeach; ?>

                                                        <?php } else { ?>

                                                            <p class="no-page-created text-center">
                                                                <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No page created yet!
                                                            </p>

                                                        <?php }?>

                                                    </ul>
                                                        <?php if(!empty($custom_recent_pages)){?>
                                                             <button type="submit" class="btn btn-default btn-submit">Add to Custom menu</button>
                                                        <?php }?>
                                                    </form>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="all-custom-pages">
                                                 <form action="<?= base_url()?>menu/add_custom_menu" method="POST" class="form-horizontal add-menu-form">
                                                    <ul class="menu-list">
                                                       <?php if( !empty($custom_pages) ) {
                                                                foreach ($custom_pages as $page) :
                                                        ?>
                                                        <li>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" name="add_custom_menu[]" value="<?php echo $page->id;?>" <?php echo ( $page->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($page->title);?>
                                                            </label>
                                                        </li>

                                                        <?php endforeach; ?>
                                                        <?php } else { ?>

                                                            <p class="no-page-created text-center">
                                                                <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No page created yet!
                                                            </p>

                                                        <?php }?>

                                                         <div class="col-md-12">
                                                            <?php if( isset($custom_ajax_pagination) ) : ?>
                                                                <div class="pagination-area pull-right">
                                                                    <nav>
                                                                        <ul class="pagination">
                                                                            <?php echo $custom_ajax_pagination; ?>
                                                                        </ul>
                                                                    </nav>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>

                                                    </ul>
                                                        <?php if(!empty($custom_pages)){?>
                                                            <button type="submit" class="btn btn-default btn-submit">Add to Custom menu</button>
                                                        <?php }?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href="javascript:void(0);"> Custom Link</a>
                                        <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                         <form action="<?= base_url()?>menu/add_custom_link_menu" method="POST" id="customLinkMenu" class="form-horizontal add-custom-menu-form">
                                           <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Add a custom link to your website that points to another website.</p>
                                             <div class="form-group">
                                                <label for="menu-url">Url</label>
                                                <input type="text" class="form-control" id="custom_menu_url" name="custom_menu_url" placeholder="http://page.com">
                                              </div>
                                              <div class="form-group">
                                                <label for="menu-name">Menu Name</label>
                                                <input type="text" class="form-control" id="custom_link_menu_name" name="custom_link_menu_name" maxlength="40" pattern="[a-zA-Z0-9-_.,\s]+">
                                              </div>
                                            <button type="submit" class="btn btn-default btn-submit">Add to Custom menu</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8">

                        <div class="save-property-listing menu-tab">

                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li id="defaultMenu" role="presentation" class="active"><a class="defaultMenu" href="#default-menu" aria-controls="home" role="tab" data-toggle="tab">Default Menu</a></li>
                            <li id="customMenu" role="presentation"><a class="customMenu" href="#custom-menu" aria-controls="profile" role="tab" data-toggle="tab">Custom Menu</a></li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="default-menu">

                              <!-- Display Default Menu -->
                            <?php
                                if (isset($default_dd_menu)) {

                                    if (count($default_dd_menu) > 0) {
                                        ?>
                                        <div class="row">
                                            <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Set Menu and Submenu order by drag and drop pages to rearrange order in which pages are displayed. To create sub menus, drag pages slightly to the right.</p>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="dd nestable" id="nestable">
                                            <?php
                                                if(isset($build_default_menu_dashboard) && !empty($build_default_menu_dashboard)){

                                                echo $build_default_menu_dashboard;
                                            }?>

                                        </div>

                                    <?php } else { ?>

                                         <p class="text-center">No Menu Created!</p>

                                    <?php }

                                } else { ?>

                                    <p class="text-center">No Pages created!</p>

                                <?php } ?>


                                <?php if(isset($build_default_menu_dashboard) && !empty($build_default_menu_dashboard)){?>

                                     <button style="margin-top: 10px;" class="btn btn-default btn-submit" id="saveDefaultMenuOrder">Save</button>

                                <?php }?>

                            </div>
                            <div role="tabpanel" class="tab-pane" id="custom-menu">

                                 <!-- Display Custom Menu -->
                                <?php
                                    if (isset($custom_dd_menu)) {

                                        if (count($custom_dd_menu) > 0) {
                                            ?>
                                            <div class="row">
                                                <p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> Set Menu and Submenu order by drag and drop pages to rearrange order in which pages are displayed. To create sub menus, drag pages slightly to the right.</p>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="dd nestable" id="nestable-custom">
                                                <?php
                                                    if(isset($build_custom_menu_dashboard) && !empty($build_custom_menu_dashboard)){

                                                    echo $build_custom_menu_dashboard;
                                                }?>

                                            </div>

                                        <?php } else { ?>

                                             <p class="text-center">No Menu Created!</p>

                                        <?php }

                                    } else { ?>

                                        <p class="text-center">No Pages created!</p>

                                    <?php } ?>


                                    <?php if(isset($build_custom_menu_dashboard) && !empty($build_custom_menu_dashboard)){?>

                                         <button style="margin-top: 10px;" class="btn btn-default btn-submit" id="saveCustomMenuOrder">Save</button>

                                    <?php }?>

                            </div>

                           <!--  <form style="display: none;" id="menu-editor">
                                <h3>Editing <span id="currentEditName"></span></h3>
                                <div class="form-group">
                                  <label for="addInputName">Name</label>
                                    <input type="hidden" id="menu-id" name="currentEditId" value="">
                                    <input type="hidden" id="menu-slug" name="currentEditSlug" value="">
                                    <input type="hidden" id="menu-page-type" name="currentEditPageType" value="">
                                    <input type="text" class="form-control input-name" name="page_title" id="editInputName" placeholder="Item name" value="" required>
                                </div>
                                <button type="submit" class="btn btn-default btn-submit" id="editButton">Save</button>
                            </form> -->

                          </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php $this->load->view('session/footer'); ?>

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            //Save Menu order
            $("#saveDefaultMenuOrder").off("click").on("click", function() {

                    var product_data = $("#nestable").nestable("serialize");
                    var data = {product_data: product_data};
                    var url = base_url + "menu/update_default_menu_priority";
                    console.log(data);

                    $.ajax({
                        url: url,
                        type: "post",
                        dataType: "json",
                        data: data,
                        beforeSend: function() { },
                        success: function(result) {
                            if (result['status'] == "success") {
                                window.location.href = result['redirect'];
                            }
                        }
                    });
            });

             $("#saveCustomMenuOrder").off("click").on("click", function() {

                    var product_data = $("#nestable-custom").nestable("serialize");
                    var data = {product_data: product_data};
                    var url = base_url + "menu/update_custom_menu_priority";
                    console.log(data);

                    $.ajax({
                        url: url,
                        type: "post",
                        dataType: "json",
                        data: data,
                        beforeSend: function() { },
                        success: function(result) {
                            if (result['status'] == "success") {
                                window.location.href = result['redirect'];
                            }
                        }
                    });
            });

          // Edit Menu Page Title
            // $("#menu-editor-custom").off("click").on("click", function() {

            //         var data = $("#menu-editor").serialize();
            //         var menu_id = $("#menu-id").val();
            //         var menu_slug = $("#menu-slug").val();
            //         var page_type = $("#menu-page-type").val();

            //         if(menu_slug == "find-a-home" || menu_slug == "testimonials"){

            //             var url = base_url + "menu/edit_title/"+menu_id;

            //         } else if(page_type == "custom link"){

            //             var url = base_url + "menu/edit_custom_title/"+menu_id;

            //         } else {

            //             var url = base_url + "menu/edit_page_title/"+menu_id;
            //         }

            //         console.log(page_type);

            //         $.ajax({
            //             url: url,
            //             type: "post",
            //             dataType: "json",
            //             data: data,
            //             beforeSend: function() { },
            //             success: function(result) {
            //                 if (result['status'] == "success") {
            //                      window.location.href = result['redirect'];
            //                 }
            //             }
            //         });

            // });

            $('.button-edit').on("click", function() {
                var enter = $('#editInputName').val();
                if(enter == "Home") {
                     $('#editInputName').prop('readonly', true);
                    $('#editInputName').removeProp('required');

                }
            });

            $('#nestable').nestable({
                maxDepth: 3,
                //group : 1
            })
            .on('change', updateOutput);

             $('#nestable-custom').nestable({
                maxDepth: 3,
                //group : 1
            })
            .on('change', updateOutput);

        });
    </script>
