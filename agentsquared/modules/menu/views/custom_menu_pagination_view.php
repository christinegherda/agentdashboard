       
       <div role="tabpanel" class="tab-pane" id="all-pages">
         <form action="<?= base_url()?>menu/add_custom_menu" method="POST" class="form-horizontal add-menu-form">
            <ul class="menu-list">
               <?php if( !empty($custom_pages) ) { 
                        foreach ($custom_pages as $page) :
                ?>  
                <li>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="add_custom_menu[]" value="<?php echo $page->id;?>" <?php echo ( $page->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($page->title);?>
                    </label>
                </li>
                
                <?php endforeach; ?>
                
                <?php }?>

                 <div class="col-md-12">
                    <?php if( isset($custom_ajax_pagination) ) : ?>
                        <div class="pagination-area pull-right">
                            <nav>
                                <ul class="pagination">
                                    <?php echo $custom_ajax_pagination; ?>
                                </ul>
                            </nav>
                        </div>
                    <?php endif; ?>
                </div>

            </ul>
                <?php if(!empty($custom_pages)){?>
                    <button type="submit" class="btn btn-default btn-submit">Add to Custom menu</button>
                <?php }?>  
            </form>
        </div>