<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Menu_model extends CI_Model { 

	public function __construct()
    {
        parent::__construct();
    }
	
     public function get_all_default_pages($params = array()){

        $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'page_type' => "page",
            'menu_type' => "default_menu",
            'status'    => "published"
        );

        $this->db->select('*')
            ->from('pages')
            ->where($array)
            ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

            $this->db->limit($params['limit'],$params['start']);
        }

        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

            $this->db->limit($params['limit']);
        }
        
         $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function get_all_custom_pages($params = array()){

        $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'page_type' => "page",
            'menu_type' => "custom_menu",
            'status'    => "published"
        );

        $this->db->select('*')
            ->from('pages')
            ->where($array)
            ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){

            $this->db->limit($params['limit'],$params['start']);
        }

        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){

            $this->db->limit($params['limit']);
        }
        
         $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function get_all_default_recent_pages()
    {
         $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'page_type' => "page",
            'menu_type' => "default_menu",
            'status'    => "published"
        );
        $this->db->select("*")
                ->from("pages")
                ->where($array)
                ->order_by("id", "desc")
                ->limit("5");

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function get_all_custom_recent_pages()
    {
         $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'page_type' => "page",
            'menu_type' => "custom_menu",
            'status'    => "published"
        );
        $this->db->select("*")
                ->from("pages")
                ->where($array)
                ->order_by("id", "desc")
                ->limit("5");

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function add_default_menu()
    {
        if(gettype($_POST['add_default_menu'])=="array"){
            foreach($_POST['add_default_menu'] as $val){
             $menu_id = $val;
             
              $array = array(
                'id' => $menu_id,
                'agent_id' => $this->session->userdata('agent_id')
            );

           $this->db->set('is_menu', 1)
                ->where($array)
                ->update('pages');
            }
        }
    }

     public function add_custom_menu()
    {
        if(gettype($_POST['add_custom_menu'])=="array"){
            foreach($_POST['add_custom_menu'] as $val){
             $menu_id = $val;
             
              $array = array(
                'id' => $menu_id,
                'agent_id' => $this->session->userdata('agent_id')
            );

           $this->db->set('is_menu', 1)
                ->where($array)
                ->update('pages');
            }
        }
    }

     public function add_custom_link_menu()
    {

         $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'is_menu'   => 1,
            'menu_type' => "custom_menu",
            'status'    => "published"
        );
        $this->db->select("*")
                 ->from("pages")
                 ->where($array);

         $query = $this->db->get();
         $data = $query->result_array();
         $menu_count = count($data);
         $menu_order = $menu_count + 1;
         //printA($menu_order);exit;

        $post["title"] = $this->input->post("custom_link_menu_name");
        $post["slug"] = $this->input->post("custom_menu_url");
        $post["post_date"] = date("Y-m-d H:i:s");
        $post["post_modified"] = date("Y-m-d H:i:s");
        $post["status"] = "published";
        $post["page_type"] = "custom link";
        $post["is_menu"] = "1";
        $post["menu_type"] = "custom_menu";
        $post["menu_order"] = $menu_order;
        $post["agent_id"] = $this->session->userdata('agent_id');

         if( $this->db->insert("pages", $post)){
              return $this->db->insert_id();

          } else {

              return FALSE;
          }
    }

    public function get_menu_type($menu_id= NULL){

        $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'page_type' => "page",
            'status'    => "published",
            'id'        => $menu_id
        );

        $this->db->select('menu_type')
            ->from('pages')
            ->where($array);

        return $this->db->get()->row_array();
    }

    public function remove_menu($menu_id= NULL){

       $this->db->set('is_menu', 0)
            ->set('parent_menu_id', 0)
            ->where("id", $menu_id)
            ->update('pages');

        return ( $this->db->affected_rows() ) ? TRUE : FALSE ;
    }


    public function remove_custom_menu($menu_id= NULL){

        $this->db->where("id", $menu_id);

        if( $this->db->delete( "pages" ) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function update_title( $page_id = NULL ){

        $data = array(
           'title' => $this->input->post("page_title"),
           'post_modified' => date("Y-m-d H:i:s")
        );

         $this->db->select("title")
                ->from("pages")
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->where("title", $this->input->post("page_title"))
                ->where("status", "published")
                ->limit(1);

        $query = $this->db->get();

        $this->db->where('id', $page_id);

         return ($query->num_rows()>0) ? array("success"=>FALSE, "message"=>"Duplicate page name! Please enter another page name!") : (($this->db->update("pages", $data)) ? array("success"=>TRUE,"message"=>"Page title has been successfully updated!") : array("success"=>FALSE,"message" => "Failed to update page title!")); 

    }

    public function update_page_title( $page_id = NULL ){

        $data = array(
           'title' => $this->input->post("page_title"),
           'slug' => strtolower(str_replace(" ", "-", $this->input->post("page_title"))),
           'post_modified' => date("Y-m-d H:i:s")
        );

        $this->db->select("title")
                ->from("pages")
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->where("title", $this->input->post("page_title"))
                ->where("status", "published")
                ->limit(1);

        $query = $this->db->get();

        $this->db->where('id', $page_id);

        return ($query->num_rows()>0) ? array("success"=>FALSE, "message"=>"Duplicate page name! Please enter another page name!") : (($this->db->update("pages", $data)) ? array("success"=>TRUE,"message"=>"Page title has been successfully updated!") : array("success"=>FALSE,"message" => "Failed to update page title!")); 

    }

     public function update_custom_title( $page_id = NULL ){

        $data = array(
           'title' => $this->input->post("page_title"),
           'post_modified' => date("Y-m-d H:i:s")
        );

        $this->db->select("title")
                ->from("pages")
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->where("title", $this->input->post("page_title"))
                ->where("status", "published")
                ->limit(1);

        $query = $this->db->get();

        $this->db->where('id', $page_id);

        return ($query->num_rows()>0) ? array("success"=>FALSE, "message"=>"Duplicate custom menu name! Please enter another custom menu name!") : (($this->db->update("pages", $data)) ? array("success"=>TRUE,"message"=>"Custom Menu title has been successfully updated!") : array("success"=>FALSE,"message" => "Failed to update custom menu title!")); 

    }

    public function select_default_menu_priority(){
          $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'is_menu'   => 1,
            'menu_type'   => "default_menu",
            'status'    => "published"
        );
        $this->db->select("*")
                 ->from("pages")
                 ->order_by("menu_order")
                 ->where($array);

         $query = $this->db->get();
         $data = $query->result_array();

         //printA( $data ); exit();
        return $data;

    }

    public function select_custom_menu_priority(){
          $array = array(
            'agent_id'    => $this->session->userdata('agent_id'),
            'is_menu'   => 1,
            'menu_type'   => "custom_menu",
            'status'    => "published"
        );
        $this->db->select("*")
                 ->from("pages")
                 ->order_by("menu_order")
                 ->where($array);

         $query = $this->db->get();
         $data = $query->result_array();

         //printA( $data ); exit();
        return $data;

    }

    public function update_default_priority_data($data, $parent = NULL){
        $i = 1;
        //printA($data);exit;
        foreach ($data as $d) {
            if (array_key_exists("children", $d)) {
                $this->update_default_priority_data($d['children'], $d['id']);
            }
            $update_array = array("menu_order" => $i, "parent_menu_id" => $parent);
            $update = $this->db->where("id", $d['id'])->update("pages", $update_array);
            $i++;
        }
        return $update;
    }

    public function update_custom_priority_data($data, $parent = NULL){
        $i = 1;
        //printA($data);exit;
        foreach ($data as $d) {
            if (array_key_exists("children", $d)) {
                $this->update_custom_priority_data($d['children'], $d['id']);
            }
            $update_array = array("menu_order" => $i, "parent_menu_id" => $parent);
            $update = $this->db->where("id", $d['id'])->update("pages", $update_array);
            $i++;
        }
        return $update;
    }

    public function default_isHaveSubMenu( $menu_id = NULL){

        $this->db->select("COUNT(id) as haveMenu")
                ->from("pages")
                ->where('parent_menu_id', $menu_id)
                ->where('status', 'published');

       $query = $this->db->get()->row();

       return ($query->haveMenu > 0) ? TRUE : FALSE;
    }

    public function custom_isHaveSubMenu( $menu_id = NULL){

        $this->db->select("COUNT(id) as haveMenu")
                ->from("pages")
                ->where('parent_menu_id', $menu_id)
                ->where('status', 'published');

       $query = $this->db->get()->row();

       return ($query->haveMenu > 0) ? TRUE : FALSE;
    }

     public function getParentMenu( $menu_id = NULL){

        $this->db->select("id")
                ->from("pages")
                ->where('parent_menu_id', $menu_id);

      $query = $this->db->get();

        return $query->result_array();
    }

     public function remove_parenting( $id = NULL){

         $this->db->set('parent_menu_id', 0)
            ->where("id", $id)
            ->update('pages');

        return ( $this->db->affected_rows() ) ? TRUE : FALSE ;
    }

    public function get_agent_pages_id(){

        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
        );
        $this->db->select("id")
                ->from("pages")
                ->where($array);
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }
   
}