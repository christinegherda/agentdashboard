<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {

	//public $data;

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		$this->load->model('landing_page/registration_model', 'reg_model');
		$this->load->model('idx_login/idx_model', 'idx_model');
		$this->load->model('mysql_properties/properties_model', 'pm');
		$this->load->model('agent_sites/Agent_sites_model');

	}

	// redirect if needed, otherwise display the user list
	function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->_render_page('auth/index', $data);
		}
	}
	
	// log the user in
	function login()
	{
		$data['title'] = "Login";

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() == true) {

			if(filter_var($this->input->post('identity'), FILTER_VALIDATE_EMAIL)) {

				// check to see if the user is logging in
				// check for "remember me"
				$remember = (bool) $this->input->post('remember');

				if($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
					//if the login is successful
					//redirect them back to the home page
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					$this->session->set_userdata('logged_in_auth', true);

					if($this->session->userdata('agent_id')) {

						$this->load->library("idx_auth", $this->session->userdata('agent_id'));

						if($this->idx_auth->isTokenValid()) {

							$row = $this->reg_model->check_sync_status($this->session->userdata('agent_id'));

							if($row->sync_status) {

								$hasProperty = $this->pm->isHasProperties(array('user_id'=>$this->session->userdata('user_id')));
								$hasAccountMeta = $this->pm->isHasAccountMeta(array('user_id'=>$this->session->userdata('user_id')));

								if($hasProperty && $hasAccountMeta) {

									$meta = modules::load('mysql_properties/Mysql_properties/');
									$account_information = $meta->get_account($this->session->userdata('user_id'), 'account_info');

									$agent_arr = array(
							        	'agent_name'  	=> (isset($account_information->FirstName) && ($account_information->FirstName)) ? $account_information->FirstName : 'Admin',
							           	'agent_photo'	=> (isset($account_information->Images[0]->Uri) && ($account_information->Images[0]->Uri)) ? $account_information->Images[0]->Uri : AGENT_DASHBOARD_URL . 'assets/images/no-profile-img.gif',
							            'site_title' 	=> (isset($row->site_name) && ($row->site_name)) ? $row->site_name : 'Your Website',
							        );

									$this->session->set_userdata('agentdata', $agent_arr);

									$subscription = Modules::run('idx_login/agent_subscription', $this->session->userdata('agent_id'));

									if($subscription == "idx") {

										//run agent listing updater
										$query = $this->pm->get_access_token($this->session->userdata('agent_id'));

										if(isset($query->access_token) && !empty($query->access_token)) {
											Modules::run('idx_login/agent_listings_updater', array('user_id'=>$this->session->userdata('user_id'), 'agent_id'=>$this->session->userdata('agent_id'), 'access_token'=>$query->access_token));
										}

										
										//insert agent's ip if new
										$this->idx_model->save_agent_ip();

										redirect('dashboard', 'refresh');

									} elseif($subscription == "spw") {

										//run agent listing updater
										$query = $this->pm->get_access_token($this->session->userdata('agent_id'));

										if(isset($query->access_token) && !empty($query->access_token)) {
											Modules::run('idx_login/agent_listings_updater', array('user_id'=>$this->session->userdata('user_id'), 'agent_id'=>$this->session->userdata('agent_id'), 'access_token'=>$query->access_token));
										}


										redirect('single_property_listings', 'refresh');

									} else {

										$data['message'] = "You do not have authority to login.";
										$logout = $this->ion_auth->logout();
										$this->_render_page('login/auth/login', $data);
									}

								} else {
									$msp = modules::load('mysql_properties/Mysql_properties');
									$msp->sync_home_properties($this->session->userdata('user_id'), $this->session->userdata('agent_id'));
								}

							} else {
								$obj = modules::load('idx_login/Idx_login/');
								$obj->resync_data_view($this->session->userdata('user_id'), $this->session->userdata('agent_id'));
							}

						} else {
							$this->ion_auth->logout();
							redirect('idx_login/Idx_login');
						}

					} else {
						$data['message'] = "You do not have authority to login.";
						$logout = $this->ion_auth->logout();
						$this->_render_page('login/auth/login', $data);
					}
				} else {
					// if the login was un-successful
					// redirect them back to the login page
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					redirect('login/auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
				}

			} else {

				$this->session->set_flashdata('message', 'The email you provided was invalid!');
				redirect('login/auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries

			}
		} else {
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$data['password'] = array('name' => 'password',
				'id'   => 'password',
				'type' => 'password',
			);

			$this->_render_page('login/auth/login', $data);
		}
	}

	public function agent_site_login() {

		$flag=FALSE;

		$user_id = $this->input->get('user_id');
		$agent_id = $this->input->get('agent_id');

		if(!empty($user_id) && !empty($agent_id)) {

	        if(isset($_COOKIE['agent_ip_'.$agent_id])) {

	        	$env_ip_add = getenv('HTTP_X_FORWARDED_FOR');
	        	$agent_ip_add = !empty($env_ip_add) ? $env_ip_add : $this->input->ip_address(); //ip add

	        	if(in_array($agent_ip_add, $_COOKIE['agent_ip_'.$agent_id])) {
	        		$flag = TRUE;
	        	}
	        }

	        if($flag) {

		  		$this->load->library('idx_auth', $agent_id);

				if($this->idx_auth->isTokenValid()) {

					$hasAccountMeta = $this->pm->isHasAccountMeta(array('user_id'=>$user_id));

					if($hasAccountMeta) {

						$meta = modules::load('mysql_properties/Mysql_properties/');
						$account_info = $meta->get_account($user_id, 'account_info');
						$row = $this->idx_model->fetch_user(NULL, $agent_id);

						$agent_arr = array(
				        	'agent_name'  => (isset($row->first_name) && !empty($row->first_name)) ? $row->first_name : ((isset($account_info->FirstName) && !empty($account_info->FirstName)) ? $account_info->FirstName : 'Admin'),
				           	'agent_photo'=> (isset($row->agent_photo) && !empty($row->agent_photo)) ? $row->agent_photo : ((isset($account_info->Images[0]->Uri) && !empty($account_info->Images[0]->Uri)) ? $account_info->Images[0]->Uri : AGENT_DASHBOARD_URL . 'assets/images/no-profile-img.gif'),
				            'site_title' => (isset($row->site_name) && ($row->site_name)) ? $row->site_name : 'Your Website',
				        );
						
						$this->session->set_userdata('agentdata', $agent_arr);

						$subscription = Modules::run('idx_login/agent_subscription', $agent_id);

						$this->session->set_userdata(
					  		array(
								'user_id'		=> $row->id,
								'agent_id'		=> $row->agent_id,
								'email'	 		=> $row->email,
								'code' 			=> $row->code,
								'identity' 		=> $row->email,
								'old_last_login'=> $row->created_on,
								'logged_in_auth'=> true
							)
				  		);

						switch ($subscription) {
							case 'idx':
								if($this->input->get('fresh')) {
									redirect('dashboard?modal_welcome=true', 'refresh');
								} else {
									redirect('dashboard', 'refresh');
								}
								break;
							
							case 'spw':
								redirect('single_property_listings', 'refresh');
								break;

							default:
								$data['message'] = "You do not have authority to login.";
								$logout = $this->ion_auth->logout();
								$this->_render_page('login/auth/login', $data);
								break;
						}

					}

				} else {
					$this->ion_auth->logout();
					redirect('idx_login/Idx_login');
					/*$this->session->unset_userdata('user_id');
					$this->session->unset_userdata('agent_id');
					redirect(AGENT_DASHBOARD_URL.'login/auth/login/', 'refresh');*/
				}

		  	} else {
		  		$this->session->unset_userdata('user_id');
				$this->session->unset_userdata('agent_id');
				$this->session->unset_userdata('logged_in_auth');
		  		redirect(AGENT_DASHBOARD_URL.'login/auth/login/', 'refresh');
		  	}

	  	} else {
	  		$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('agent_id');
			$this->session->unset_userdata('logged_in_auth');
	  		redirect(AGENT_DASHBOARD_URL.'login/auth/login/', 'refresh');
	  	}

	}
	
	function logout()
	{
		$data['title'] = "Logout";
		$logout = $this->ion_auth->logout();
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('login/auth/login', 'refresh');
	}

	public function change_password()
	{
		$data['title'] = "Dashboard";

		if($_POST) {

			$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
			$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			//$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

			if($this->form_validation->run() == false) {
				// display the form
				// set the flash data error message if there is one
				$data['message'] = (validation_errors()) ? validation_errors() : "";
				$this->session->set_flashdata('session' , array('status' => FALSE,'message' =>  validation_errors() ));

			} else {

				$identity = $this->session->userdata('identity');
				$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

				if($change) {
					$this->change_password_email($this->input->post('new')); //Email here
					$this->session->set_flashdata('session' , array('status' => TRUE,'message' =>  "Password has been successfully changed!" ));
				} else {
					$this->session->set_flashdata('session' , array('status' => FALSE,'message' =>  $this->ion_auth->errors() ));
				}

			}

		}

		$this->load->view('dashboard/change_password', $data);
	}

	public function change_password_ajax() {
		//printA($this->input->post());exit;
		$data['title'] = "Dashboard";

		if($_POST) {

				$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
				//$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|matches[new_confirm]');
				$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
				$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

			if($this->form_validation->run() == false) {

				$data['message'] = (validation_errors()) ? validation_errors() : "";
				//$this->session->set_flashdata('session' , array('status' => FALSE, 'message' =>  validation_errors()));
				$response = array("success"=>FALSE, "message"=>validation_errors(), "redirect"=>AGENT_DASHBOARD_URL."dashboard?reset_pass=TRUE");
				//redirect('dashboard?reset_pass=TRUE', 'refresh');
			} else {

				$url = "https://www.google.com/recaptcha/api/siteverify";

				$post = array(
					'secret' 	=> "6Lf9eRsUAAAAABfHFErjABI8DUaozthQj06y_95G",
					'response' => $this->input->post('g-recaptcha-response'),
					'remoteip'	=> $this->input->ip_address(),
				);

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 0);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
				curl_setopt($ch, CURLOPT_URL, $url);

				$result = curl_exec($ch);
				$result_info = json_decode($result, true);

				if(!empty($result_info)) {

					if($result_info['success']) {

						$identity = $this->session->userdata('identity');
						$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

						if($change) {
							$this->change_password_email($this->input->post('new'));
							$response = array("success"=>TRUE, "message"=>"Password has been successfully changed!", "redirect"=>AGENT_DASHBOARD_URL."dashboard");
							//$this->session->set_flashdata('session' , array('status'=>TRUE,'message'=>"Password has been successfully changed!"));
						} else {
							$response = array("success"=>FALSE, "message"=>$this->ion_auth->errors(), "redirect"=>AGENT_DASHBOARD_URL."dashboard?reset_pass=TRUE");
							//$this->session->set_flashdata('session' , array('status'=>FALSE,'message'=>$this->ion_auth->errors()));
							//redirect('dashboard?reset_pass=TRUE', 'refresh');
						}

					} else {
							$response = array("success"=>FALSE, "message"=>"Google Captcha Error: ".$result_info['error-codes'][0]." Please try again.", "redirect"=>AGENT_DASHBOARD_URL."dashboard?reset_pass=TRUE");
					}

				} else {
					$response = array("success"=>FALSE, "message"=>"Please complete all fields including the recaptcha.", "redirect"=>AGENT_DASHBOARD_URL."dashboard?reset_pass=TRUE");
					//$this->session->set_flashdata('session' , array('status'=>FALSE,'message'=>$result_info['error-codes'][0]));
					//redirect('dashboard?reset_pass=TRUE', 'refresh');
				}

			}

		} else {
			$response = array("success"=>FALSE, "message"=>"Please complete all fields!", "redirect"=>AGENT_DASHBOARD_URL."dashboard?reset_pass=TRUE");
		}

		exit(json_encode($response));
		//redirect('dashboard', 'refresh');
	}

	private function change_password_email($password) {

		$this->load->library("email");

		$agent_info = $this->Agent_sites_model->get_branding_infos();

		return $this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	$agent_info->email, //recipient email
        	'honradokarljohn@gmail.com,paul@agentsquared.com,joce@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'5f950652-ecc6-4d9d-92f5-828be7b6d512', //'5687e26d-0e4e-4515-931f-91b5d0b4dc5d', //template_id
        	$email_data = array(
        		'subs'=>array(
        			'first_name'	=> $agent_info->first_name,
        			'login_url'		=> AGENT_DASHBOARD_URL.'login/auth/login',
        			'agent_email'	=> $agent_info->email,
        			'agent_password'=> $password
        		)
        	)
        );
	}

	// forgot password
	function forgot_password()
	{
		// setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') != 'email') {
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		} else {
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}

		if($this->form_validation->run() == false) {

			$data['type'] = $this->config->item('identity','ion_auth');
			// setup the input
			$data['identity'] = array('name' => 'identity',
				'id' => 'identity',
			);

			if($this->config->item('identity', 'ion_auth') != 'email') {
				$data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			} else {
				$data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->_render_page('login/auth/forgot_password', $data);
		
		} else {

			$identity_column = $this->config->item('identity','ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if(empty($identity)) {

        		if($this->config->item('identity', 'ion_auth') != 'email') {
            		$this->ion_auth->set_error('forgot_password_identity_not_found');
            	} else {
            	   $this->ion_auth->set_error('forgot_password_email_not_found');
            	}

                $this->session->set_flashdata('message', $this->ion_auth->errors());
        		redirect("login/auth/forgot_password", 'refresh');
    		}
    		
			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->email);

			if($forgotten) {
				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("login/auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
				
			} else {
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("login/auth/forgot_password", 'refresh');
			}
		}
	}

	// reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		/*if(!$code && !$this->input->get('code')) {
			show_404();
		}*/

		$code = $this->input->get('code');

		$user = $this->ion_auth->forgotten_password_check($code);
		//printA($user);exit;
		if($user) {
			// if the code is valid then display the password reset form

			//$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				// display the form

				// set the flash data error message if there is one
				$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'pattern' => '^.{'.$data['min_password_length'].'}.*$',
				);
				$data['new_password_confirm'] = array(
					'name'    => 'new_confirm',
					'id'      => 'new_confirm',
					'type'    => 'password',
					'pattern' => '^.{'.$data['min_password_length'].'}.*$',
				);
				$data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$data['csrf'] = $this->_get_csrf_nonce();
				$data['code'] = $code;
				//printA($data);exit;
				//echo "nisud sa if";exit;
				// render
				$this->_render_page('login/auth/reset_password', $data);

			} else {
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("login/auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('login/auth/reset_password?code='.$code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("login/auth/forgot_password", 'refresh');
		}
	}


	// activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	// deactivate the user
	function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$data['csrf'] = $this->_get_csrf_nonce();
			$data['user'] = $this->ion_auth->user($id)->row();

			$this->_render_page('auth/deactivate_user', $data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			// redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	// create a new user
	function create_user()
    {
        $data['title'] = "Create User";

        /*if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('auth', 'refresh');
        }*/

        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
        if($identity_column!=='email')
        {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true)
        {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'company'    => $this->input->post('company'),
                'phone'      => $this->input->post('phone'),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data))
        {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("Landing_page", 'refresh');
        }
        else
        {
            // display the create user form
            // set the flash data error message if there is one
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->_render_page('Login/auth/create_user', $data);
        }
    }

	// edit a user
	function edit_user($id)
	{
		$data['title'] = "Edit User";

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => $this->input->post('company'),
					'phone'      => $this->input->post('phone'),
				);

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			// check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		// display the edit user form
		$data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$data['user'] = $user;
		$data['groups'] = $groups;
		$data['currentGroups'] = $currentGroups;

		$data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$data['company'] = array(
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company', $user->company),
		);
		$data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);

		$this->_render_page('auth/edit_user', $data);
	}

	// create a new group
	function create_group()
	{
		$data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			// display the create group form
			// set the flash data error message if there is one
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('auth/create_group', $data);
		}
	}

	// edit a group
	function edit_group($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		// set the flash data error message if there is one
		$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$data['group_name'] = array(
			'name'    => 'group_name',
			'id'      => 'group_name',
			'type'    => 'text',
			'value'   => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('auth/edit_group', $data);
	}


	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}

}
