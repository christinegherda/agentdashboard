<!-- 
<?php echo form_open('login/auth/reset_password/' . $code);?>

	<p>
		<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
		<?php echo form_input($new_password);?>
	</p>

	<p>
		<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
		<?php echo form_input($new_password_confirm);?>
	</p>

	<?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>

	<p><?php echo form_submit('submit', lang('reset_password_submit_btn'));?></p>

<?php echo form_close();?> -->




<?php $this->load->view('landing_page/session/header'); ?>

        <section class="sign-in-page clearfix">
            <div class="sign-up-strip clearfix">
                <div class="container">
                    <div class="row">
                            <div class="left-sign-up">
                                <div class="col-md-6 col-sm-6">
                                    <div class="welcoming-sign-up">
                                    <h1>Welcome to</h1>
                                    </div>
                                    <img src="<?= base_url().'/assets/images/logo.png'?>" alt="Agentsquared Signup" class="sign-up-logo">
                                </div>
                            </div>
                            <div class="right-sign-up">
                                <div class="col-md-6 col-sm-6">
                                    <div class="sign-in-container clearfix">
                                        <h2><?php echo lang('reset_password_heading');?></h2>
                                        <p><?php echo $message;?></p>
                                        
                                        <form class="form-signin" action="<?php echo site_url('login/auth/reset_password?code='.$code); ?>" method="post">
                                        <!-- <form class="form-signin" action="<?php echo site_url('login/auth/reset_password'); ?>" method="post"> -->
                                            <?php echo form_input($user_id);?>
                                            <?php echo form_hidden($csrf); ?>
                                            <!-- <input type="hidden" name="code" value="<?=$code?>"> -->
                                            <div class="form-group mb-25px">
                                                <label for="password">New Password</label>
                                                <input type="password" id="new"  name="new" class="form-control " required>
                                            </div>

                                             <div class="form-group mb-25px">
                                                <label for="password">Confirm Password</label>
                                                <input type="password" id="new_confirm"  name="new_confirm" class="form-control " required>
                                            </div>
                                            
                                            <div class="col-md-12 col-sm-12  text-right no-padding-right">
                                                <button class="btn btn-default btn-create-account" id="login-btn" type="submit">submit</button> 
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="sign-in-account text-center">
                                                    <!-- <p>Don't have an account?</p>
                                                    <p><a href="<?= site_url('sign_up?type=idx'); ?>">Create Account</a></p> -->
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
                    </div>
                </div>
            </div>
        </section>


    <?php 
        $this->load->view('landing_page/session/wizard');
    ?>
    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 
    <script src="<?= base_url()?>assets/js/landing_page/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/main.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/owl.carousel.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/jquery.validate.js"></script>
    <script src="<?= base_url() ?>assets/js/landing_page/jquery.steps.js"></script>
    <script src="<?= base_url() ?>assets/js/landing_page/bootstrap-filestyle.js"></script>   
</body>
</html>