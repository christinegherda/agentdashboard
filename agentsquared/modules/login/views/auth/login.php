<?php $this->load->view('landing_page/session/header'); ?>

        <section class=" clearfix">
            <div class="sign-up-strip clearfix">
                <div class="container">
                    <div class="row">
                            <div class="left-sign-up">
                                <div class="col-md-6 col-sm-6">
                                    <div class="welcoming-sign-up">
                                    <h1>Welcome to</h1>
                                    </div>
                                    <img src="<?= base_url().'/assets/images/logo.png'?>" alt="Agentsquared Signup" class="sign-up-logo">
                                </div>
                            </div>
                            <div class="right-sign-up">
                                <div class="col-md-6 col-sm-6">
                                    <div class="sign-in-container clearfix">
                                        <h2>Login</h2>
                                        <?php if (isset($message)) : ?>
                                          <div class="alert alert-danger">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <?php echo $message; ?></div>
                                        <?php endif;?>
                                        <form class="form-signin" action="<?php echo site_url('login/auth/login'); ?>" method="post">
                                        <!-- <form class="sign_up_form form-signin" action="<?php echo site_url('login/auth/login'); ?>" method="post">  -->
                                            <div class="form-group">
                                                <label for="email">Username or Email</label>
                                                 <input type="email" id="inputEmail" name="identity" class="form-control" required autofocus> 
                                            </div>
                                            <div class="form-group mb-25px">
                                                <label for="password">Password</label>
                                                <input type="password" id="inputPassword"  name="password" class="form-control" required>
                                            </div>
                                            <div class="col-md-12 col-sm-12  text-right no-padding-right">
                                                <button class="btn btn-default btn-create-account" id="login-btn" type="submit">Sign In</button> 
                                            </div>
                                            <div class="col-md-12 col-sm-12  text-right no-padding-right mobile-center" style="margin-top:10px;">
                                                <a href="<?php echo site_url('login/auth/forgot_password'); ?>" style="text-decoration:underline">Forgot Password?</a>
                                            </div>
                                            <div class="col-md-12 col-sm-12 ">
                                                <div class="sign-in-account text-center">
                                                    <!-- <p>Don't have an account?</p>
                                                    <p><a href="<?= site_url('sign_up?type=idx'); ?>">Create Account</a></p> -->
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
                    </div>
                </div>
            </div>
        </section>


    <?php 
        $this->load->view('landing_page/session/wizard');
    ?>
    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 
    <script src="<?= base_url()?>assets/js/landing_page/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/main.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/owl.carousel.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/jquery.validate.js"></script>
    <script src="<?= base_url() ?>assets/js/landing_page/jquery.steps.js"></script>
    <script src="<?= base_url() ?>assets/js/landing_page/bootstrap-filestyle.js"></script>   
    
    <script type="text/javascript">
    $(document).ready(function(){
        var window_height = $(window).height();
        $(".sign-up-page").css('min-height', window_height);
    });
    </script>
</body>
</html>