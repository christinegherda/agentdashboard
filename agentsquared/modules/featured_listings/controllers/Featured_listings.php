<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Featured_listings extends MX_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model("agent_sites/Agent_sites_model");
	}

	function index() {
		$data['title'] = "Featured Listings";		
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		//$data['active_listings'] = Modules::run('dsl/getAgentActivePropertyListings');
		$data['active_listings'] = Modules::run('single_property_listings/getallActiveProperties');
	    $domain_info = $data["domain_info"];

        if(!empty($domain_info)) {
            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $base_url = base_url();
        }

	    $data["base_url"] = $base_url;

		$this->load->view('featured_listings', $data);
	}
}

?>