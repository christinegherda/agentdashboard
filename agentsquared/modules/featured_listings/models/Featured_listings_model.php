<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Featured_listings_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function fetch_active_property_details_db() {

    	$status = array('Active', 'Contingent','Pending');

    	$this->db->select("*")
                ->from("property_listing_data")
                ->where('user_id', $this->session->userdata('user_id'))
                ->order_by("id", "desc")
                ->where_in('property_status', $status)
                ->where_in('is_deleted', 0);

        return $this->db->get()->result();
    }

    public function fetch_property_details_db($fields = "*") {

    	$this->db->select($fields)
                ->from("property_listing_data")
                ->where('user_id', $this->session->userdata('user_id'));

        return $this->db->get()->result();
    }

    public function fetch_mls_meta() {

    	$this->db->select("*")
                ->from("account_mls_meta")
                ->where('user_id', $this->session->userdata('user_id'));

        return $this->db->get()->result();
    }

	public function get_property($listing_id = NULL) {

		$ret = false;

		if($listing_id) {
			$ret = $this->db->select("*")->from("property_listing_data")->where("ListingID", $listing_id)->get()->row();
		}

		return $ret;
	}
	
	public function check_listingID($listingid) {

		$array = array('mlsid' => $listingid);
		$this->db->where($array);
		$query = $this->db->get('property_details')->row();

        return ($query) ? TRUE : FALSE;
	}

    public function check_active_listings($user_id) {

        $ret = FALSE;

        if($user_id) {

            $data = array('user_id'=>$user_id, 'is_deleted'=>0);

            $this->db->select("id, json_standard_fields_updated, ListingID")
                ->from("property_listing_data")
                ->where($data)
                /*->where('user_id', $user_id)
                ->where('is_deleted', 0)*/
                ->order_by("id","desc");

            $ret = $this->db->get()->result();
        }

        return $ret;
    }

    public function update_property($fields=array(), $data = array()) {

        $this->db->where($fields);
        $this->db->update('property_listing_data', $data);

        return ($this->db->affected_rows()) ? TRUE : FALSE;
    }
}