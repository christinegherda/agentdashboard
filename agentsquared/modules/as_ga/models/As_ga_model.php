<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class As_ga_model extends CI_Model{ 

    function __construct() {
        parent:: __construct();
		$this->load->helper('form');
        //$this->load->db();
        $this->load->dbforge();
    }
	
	function checkTable(){
		if($this->db->table_exists('users_ga')){
			return true;
		}
		return false;
	}
	
	function createTable(){ // note: this is only for use in the frontend.
		if(!$this->db->table_exists('users_ga')){
			// Table structure for table 'users_ga'
			$this->dbforge->add_field(array(
				'id' => array(
					'type' => 'MEDIUMINT',
					'constraint' => '8',
					'unsigned' => TRUE,
					'auto_increment' => TRUE
				),
				'agent_id' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
				),
				'domain' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
				),
				'account_id' => array(
					'type' => 'VARCHAR',
					'constraint' => '40',
				),
				'property_id' => array(
					'type' => 'VARCHAR',
					'constraint' => '40'
				),
				'profile_id' => array(
					'type' => 'VARCHAR',
          'constraint' => '40'
				),
				'other_ga_data' => array(
					'type' => 'LONGTEXT',
					'default' => NULL
				),
				'client_email' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'default' => NULL
				),
				'access_token' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'default' => NULL
				),
				'is_enabled' => array(
					'type' => 'TINYINT',
					'default' => 0
				),
			));
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('users_ga');

			// Dumping data for table 'users'
		}
	}
	
	function insert($data){
		$query = $this->get($data['agent_id']);
		if(empty($query)){
			$this->db->insert('users_ga', $data);
			return true;
		}
		return false;
	}
	
	function get($agent_id = '', $domain = ''){
		if($agent_id === ''){
			$agent_id = ci()->session->userdata("agent_id");
		}
    $this->db->where('agent_id', $agent_id);
    if($domain){
      $this->db->where('domain', $domain);
    }
		$query = $this->db->get('users_ga');
		if( $query->num_rows() > 0 ){
			return $query->result();
		}
		return false;
	}
	
	function update($agent_id = '', $data){
		if($agent_id === ''){
			$agent_id = ci()->session->userdata("agent_id");
		}
		if($data){
			$this->db->where('agent_id', $agent_id);
			$this->db->update('users_ga', $data); 
			return true;
		}
		return false;
	}
}

