<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH .'modules/as_ga/libraries/Google/vendor/autoload.php';

class As_ga extends MX_Controller {
	
	const KEY_FILE_LOCATION = APPPATH . 'modules/as_ga/private/as-ga-8bc731478d1f.json';
	private $accessToken;
	private $views = array();
  private $accounts = array();
	
	public function __construct(){
		
		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');
		$this->load->model('agent_sites/Agent_sites_model', 'domain');
		
		$this->load->model('as_ga/as_ga_model');
		$this->as_ga_model->createTable();
	}

	public function widget(){
		// prepare data
		$domain = $this->domain->fetch_domain_info();
    
		// init all variables to save logical expressions
		$data["profile"] = false;
		$data["domain_info"] = false;
		$data["view"] = false; 
		$data["access_token"] = false; 
		$data["agent_id"] = $this->session->userdata("agent_id");
    $data["client_ga"] = false;
    $data["client_tracking_code"] = '';
    
    if(!empty($domain)){
      $profile = $this->getClientData($domain[0]->domains);
      $data["profile"] = $profile;
      $data["domain_info"] = $domain[0];
      
      $ga = $this->initialize();
      $this->getAccountViews($ga); // get all account profile views
      
      if($profile){
        // this section here is for fallback only.
        if($view = $this->getViewByDomain($domain[0]->domains)){
          $data["view"] = $view; // (object) pass view
          $data["access_token"] = $this->accessToken; // (string)
        }
        //// overrides views.
        //if(property_exists($profile, 'other_ga_data')){
        //  if($profile->other_ga_data){
        //    $newView = unserialize($profile->other_ga_data);
        //    $data["profile"]->other_ga_data = unserialize($profile->other_ga_data);
        //    $data["view"] = $this->getViewById($newView['profile_id']);
        //    $data["client_ga"] = true;
        //  }else{
        //    $data["view"] = $this->getViewById($profile->profile_id);
        //  }
        //}
        
        if(isset($profile->client_tracking_code)){
          $data["client_tracking_code"] = $profile->client_tracking_code;
        }
        if($profile->is_activated){
          $this->load->view('as_ga/as_ga_view', $data);
        }
      }else{
        $this->load->view('as_ga/as_ga_view', $data);
      }
    }else{
      $this->load->view('as_ga/as_ga_view', $data);
    }
	}
	
	private function initialize(){
		
		// Create and configure a new client object.
		$client = new Google_Client();
		$client->setApplicationName("AgentSquared Analytics");
		$client->setAuthConfig(self::KEY_FILE_LOCATION);
		$client->setScopes([
			'https://www.googleapis.com/auth/analytics.readonly', // <--- always make sure this is included.
			'https://www.googleapis.com/auth/analytics.edit', 
			'https://www.googleapis.com/auth/analytics.manage.users'
		]);
		$analytics = new Google_Service_Analytics($client);
		
		$client->refreshTokenWithAssertion();
		$token = $client->getAccessToken();
		$this->accessToken = $token['access_token'];
	
		return $analytics;
	}
	
	public function getTrackingCode(){
		if($this->as_ga_model->checkTable()){
			//prepare data
			$profile = $this->getClientData();
			
      if($profile){
        $data['profile'] = $profile; // for debugging purposes
        $data['property_id'] = $profile->property_id;
        $data['client_ga'] = false;
        if($profile->other_ga_data){
          $data['client_ga'] = unserialize($profile->other_ga_data);
        }
        $this->load->view('as_ga/as_ga_track', $data);
      }
		}
	}
	// on going pa ang development ani.
	public function updateClientData(){
		if(isset($_POST)){
			if(isset($_POST['other_ga']) || $_POST['other_ga'] === true){
				$agent_id = ci()->session->userdata("agent_id");
				$success = $this->as_ga_model->update($agent_id, array(
					'other_ga_data' => serialize(array(
						'account_id' => $_POST['accountId'],
						'property_id' => $_POST['webPropertyId'],
						'profile_id' => $_POST['profileId'],
            'date_added' => date("Y-m-d h:i:s A T"),
            'is_enabled' => '1',
					)),
				));
				if($success){
					echo json_encode(array(
						'success' => true,
						'message' => 'Successfully added your Google Analytics Account'
					));
				}else{
					echo json_encode(array(
						'success' => false,
						'message' => 'There was a problem adding your account. Please contact support.'
					));
				}
			}
		}
	}
	
	public function addTrackingCode(){
    if(isset($_POST)){
      if(isset($_POST['as_tracking_code'])){
        $agent_id = ci()->session->userdata("agent_id");
        $success = $this->as_ga_model->update($agent_id, array(
					'client_tracking_code' => $_POST['as_tracking_code'],
				));
        //header('Content-Type: application/json');
				if($success){
          //header($_SERVER['SERVER_PROTOCOL'] . ' ' . 'OK', true, 200);
					echo json_encode(array(
						'success' => true,
						'message' => 'Successfully added your tracking code.'
					));
				}else{
          //header($_SERVER['SERVER_PROTOCOL'] . ' ' . 'Internal Server Error', true, 500);
					echo json_encode(array(
						'success' => false,
						'message' => 'There was a problem adding your tracking code, please contact support.'
					));
				}
      }
    }
  }
	
	public function addProperty(){
    
		if(isset($_POST)){
			if(isset($_POST['domain'])){
				$analytics = $this->initialize();
				
				$property = new Google_Service_Analytics_Webproperty();
				$property->setName($_POST['domain']);
				$property->setWebsiteUrl('http://'.$_POST['domain'].'/');
				$property->setIndustryVertical('REAL_ESTATE');
				
				try {
          
          $this->getAccountViews($analytics);
          
          $exist = false;
          foreach($this->views as $view){
            if(strpos($view->websiteUrl, $_POST['domain']) !== false){
              $exist = true;
              break;
            }
          }
          if(!$exist){
            $exceed = true;
            foreach($this->getAccounts($analytics) as $account){
              $properties = $analytics->management_webproperties->listManagementWebproperties($account->getId());
              if($properties->totalResults < 50){
                $response = $analytics->management_webproperties->insert($account->getId(), $property);
                $exceed = false;
                break;
              }
            }
            if($exceed){
              echo json_encode(array(
                'success' => false,
                'code' => 143,
                'message' => 'There was a problem adding your domain to our Google Analytics account, please contact <a href="https://www.agentsquared.com" target="_blank">support</a>.'
              ));
              return;
            }
          }else{
            echo json_encode(array(
              'success' => false,
              'code' => 143,
              'message' => 'Your custom domain or sub domain already exists in our Google Analytics.'
            ));
            return;
          }
				} catch (apiServiceException $e) {
					echo json_encode(array(
						'success' => false,
						'code' => $e->getCode(),
						'message' => $e->getMessage()
					));
          return;
				} catch (apiException $e) {
					echo json_encode(array(
						'success' => false,
						'code' => $e->getCode(),
						'message' => $e->getMessage()
					));
          return;
				}
				
				$profile = new Google_Service_Analytics_Profile();
				$profile->setName('All Web Site Data');
				
				try {
					$result = $analytics->management_profiles->insert($response->accountId, $response->id, $profile);
				} catch (apiServiceException $e) {
					echo json_encode(array(
						'success' => false,
						'code' => $e->getCode(),
						'message' => $e->getMessage()
					));
          return;
				} catch (apiException $e) {
					echo json_encode(array(
						'success' => false,
						'code' => $e->getCode(),
						'message' => $e->getMessage()
					));
          return;
				}
				//associate the newly created profile view into the recently created property
				$property->setDefaultProfileId($result->id);
				
				try {
					//update the property from our google analytics account
					$final = $analytics->management_webproperties->update($response->accountId, $response->id, $property);
					
					$this->addAccountUser($analytics, $response->accountId, 'jovanni@agentsquared.com', array('EDIT', 'MANAGE_USERS'));
					
					$this->as_ga_model->insert(array(
						'agent_id' => ci()->session->userdata("agent_id"),
						'domain' => $_POST['domain'],
						'account_id' => $final->accountId,
						'property_id' => $final->id,
						'profile_id' => $result->id,
						'is_enabled' => '1',
						'is_activated' => '1',
					));
					
					echo json_encode(array(
						'success' => true,
						'message' => 'Successfully added '.$_POST['domain'].' to our Google Analytics account.',
						'data' => array(
							'viewId' => $result->id,
							'accountId' => $final->accountId,
							'webPropertyId' => $final->id,
							'access_token' => $this->accessToken,
						)
					));
				} catch (apiServiceException $e) {
					echo json_encode(array(
						'success' => false,
						'code' => $e->getCode(),
						'message' => $e->getMessage()
					));
				} catch (apiException $e) {
					echo json_encode(array(
						'success' => false,
						'code' => $e->getCode(),
						'message' => $e->getMessage()
					));
				}
				
			}else{
				echo json_encode(array(
					'success' => false,
					'message' => 'Domain is not defined.'
				));
			}
		}else{
			echo json_encode(array(
				'success' => false,
				'message' => 'No data received.'
			));
		}
		
	}
	/**
	*	@param (object) $analytics
	*	@param (string) $accountId
	*	@param (string) $email
	*	@param (array) $userPermissions
	*/
	private function addAccountUser($analytics, $accountId, $email, $userPermissions = array('COLLABORATE', 'READ_AND_ANALYZE') ){
		// set email
		$userRef = new Google_Service_Analytics_UserRef();
		$userRef->setEmail($email);
		// set permissions for the email
		$permissions = new Google_Service_Analytics_EntityUserLinkPermissions();
		$permissions->setLocal($userPermissions);
		
		// Create the user link.
		$userLink = new Google_Service_Analytics_EntityUserLink();
		$userLink->setPermissions($permissions);
		$userLink->setUserRef($userRef);
		
		// This request creates a new Account User Link.
		try{
			$analytics->management_accountUserLinks->insert($accountId, $userLink);
			return array(
				'success' => true,
				'message' => 'Successfully added '.$email.' to account '.$accountId
			);
		}catch(apiServiceException $e) {
			return array(
				'code' => $e->getCode(),
				'message' => $e->getMessage()
			);
		}catch(apiException $e) {
			return array(
				'code' => $e->getCode(),
				'message' => $e->getMessage()
			);
		}
	}
	
  private function getProfilesByAccountId($analytics, $accountId){
    $accounts = $analytics->management_accounts->listManagementAccounts();
		if(count($accounts->getItems()) > 0) {
			$accountItems = $accounts->getItems();
			foreach($accountItems as $account){
				if($account->getId() === $accountId){
          $profiles = $analytics->management_profiles->listManagementProfiles($account->getId(), '~all');
          if(count($profiles->getItems()) > 0) {
            $profileItems = $profiles->getItems();
            $viewArray = array();
            foreach($profileItems as $profile){
              array_push($viewArray, $profile);
            }
            return $viewArray;
          }
          break;
        }
			}
		}
    return false;
  }
  
  private function getAccounts($analytics){
    return $analytics->management_accounts->listManagementAccounts();
  }
  
	private function getAccountViews($analytics){
		$accounts = $analytics->management_accounts->listManagementAccounts();
		if(count($accounts->getItems()) > 0) {
			$this->accounts = $accountItems = $accounts->getItems();
			foreach($accountItems as $account){
				//$this->addAccountUser($analytics, $account->getId(), 'jovanni@agentsquared.com', array('EDIT', 'MANAGE_USERS'));
				//$this->getAllProfiles($analytics, $account->getId());
				$profiles = $analytics->management_profiles->listManagementProfiles($account->getId(), '~all');
				if(count($profiles->getItems()) > 0) {
					$profileItems = $profiles->getItems();
					foreach($profileItems as $profile){
						array_push($this->views, $profile);
					}
				}
			}			
		}
	}
	/* DEPRECATED
	private function getAllProfiles($analytics, $accountId){
		$profiles = $analytics->management_profiles->listManagementProfiles($accountId, '~all');
		if(count($profiles->getItems()) > 0) {
			$items = $profiles->getItems();
			foreach($items as $item){
				array_push($this->views, $item);
			}
		}
	}*/
	
	private function getViewByDomain($domain){
		if($this->views){
			foreach($this->views as $view){
				if(strpos($view->websiteUrl, $domain) !== false){ // if website url contains domain string
					return $view;
				}
			}
		}
		return false;
	}
	
	private function getViewById($viewId){
		if($this->views){
			foreach($this->views as $view){
				if($view->id === $viewId){
					return $view;
				}
			}
		}
		return false;
	}
	
	private function getClientData($domain){
		//$query = $this->as_ga_model->get($this->session->userdata("agent_id"), $domain);
		$query = $this->as_ga_model->get($this->session->userdata("agent_id"));
		//$query = $this->db->get_where('users_ga', array('agent_id' => ci()->session->userdata("agent_id")));
		if($query){
			return $query[0];
		}
		return false;
	}

}

class Client_GA{
  public $test;
  
  function __construct($array){
    
  }
  
  function set($array){
    foreach ($array as $key => $value){
        if ( property_exists ( $this , $key ) ){
            $this->{$key} = $value;
        }
    }
  }
}


?>