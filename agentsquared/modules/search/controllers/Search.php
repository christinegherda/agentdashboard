<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MX_Controller {

	public function __construct() {

		parent::__construct();

		$userData = $this->session->userdata('user_id');

        if(empty($userData)) {
            $this->session->set_flashdata('flash_data', 'You don\'t have access!');
            redirect('login/auth/login', 'refresh');
        }
        
		$this->load->model("dsl/dsl_model","dsl_model");

	}

	public function get_search_property($getData=NULL) {

		parse_str($getData, $getArr);

		if(isset($getArr['size'])){
			$limit = $getArr['size'];
		} else {
			$limit = 25;
		}

		if(isset($getArr['status'])) {
			$status = $getArr['status']; 
		}
		if(isset($getArr['Search'])) {
			//$search = str_replace(" ", "", $getArr['Search']); 
			$search = trim($getArr['Search']," ");
		}
		if(isset($getArr['bedroom'])) {
			$bedroom = $getArr['bedroom'];
		}
		if(isset($getArr['bathroom'])) {
			$bathroom = $getArr['bathroom']; 
		}
		if(isset($getArr['PropertyClass'])) {
			$type = $getArr['PropertyClass'];
		}
		if(isset($getArr['property_type'])) {
			$property_type = $getArr['property_type'];
		}
		if(isset($getArr['a_type'])) {
			$a_type = $getArr['a_type'];
		}
		if(isset($getArr['b_type'])) {
			$b_type = $getArr['b_type'];
		}
		if(isset($getArr['c_type'])) {
			$c_type = $getArr['c_type'];
		}
		if(isset($getArr['d_type'])) {
			$d_type = $getArr['d_type'];
		}
		if(isset($getArr['e_type'])) {
			$e_type = $getArr['e_type'];
		}
		if(isset($getArr['f_type'])) {
			$f_type = $getArr['f_type'];
		}

		if(isset($getArr['listing_change_type'])) {
			$listing_change_type = $getArr['listing_change_type'];
		}
		if(isset($getArr['recently_sold'])) {
			$recently_sold = $getArr['recently_sold'];
		}
		if(isset($getArr['min_price'])) {
			$minprice = $getArr['min_price'];
		}
		if(isset($getArr['max_price'])) {
			$maxprice = $getArr['max_price'];
		}
		if(isset($getArr['garage'])) {
			$garage = $getArr['garage'];
		}
		if(isset($getArr['features'])) {
			$features = $getArr['features'];
		}
		if(isset($getArr['house_age'])) {
			$house_age = $getArr['house_age'];
		}
		if(isset($getArr['lot_size'])) {
			$lot_size = $getArr['lot_size'];
		}
		if(isset($getArr['house_size'])) {
			$house_size = $getArr['house_size'];
		}
		if(isset($getArr['page'])) {
			$page = $getArr['page'];
		}
		else {
			$page = 1;
		}

		if(!empty($recently_sold)) {
			$filterStr = "(MlsStatus Eq 'Sold' Or MlsStatus Eq 'Closed')";
		} else {
			$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Active Contingent' Or MlsStatus Eq 'Pending' Or MlsStatus Eq 'Contingent' )";
		}

		if(!empty($search)) {
			if(preg_match("/^[0-9]{26}$/", $search)) {
				$mlsid = $search;
				$filterStr .= " And (MlsId Eq '".urldecode($mlsid) ."')";
			} else {
				$filterStr .= " And (UnparsedAddress Eq '*".urldecode($search)."*' Or PublicRemarks Eq '*".urldecode($search)."*' Or ListingId Eq '".urldecode($search)."')";
			}
		}

		if(!empty($bedroom)) {
			$filterStr .= " And (BedsTotal Ge ".$bedroom.")";  
		}
		if(!empty($bathroom)) {
			$filterStr .= " And (BathsTotal Ge ".$bathroom.")";
		}
		if(!empty($type)) {
			$filterStr .= " And (PropertyType Eq '".$type."')";
		}
		if(!empty($minprice) && !empty($maxprice)) {
			$filterStr .= " And (CurrentPrice Bt ".$minprice.",".$maxprice.")";
		}
		if(!empty($property_type)) {
			$str = " And (";
			$count = 0;
			foreach ($property_type as $key) {
				if($count == 0) {
					$str .= "PropertyClass Eq '*".$key."*'";
				} else {
					$str .= " Or PropertyClass Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		
		//recently sold property
		if(!empty($recently_sold)) {
			$current_year = date("Y-m-d");

			$time = strtotime("-1 month", time());
			$recent_sold = date("Y-m-d", $time);

			$str = " And (";
			//$filterStr = "(MlsStatus Eq 'Sold' Or MlsStatus Eq 'Closed')";
			$count = 0;
			foreach ($recently_sold as $key) {
				if($count == 0) {
					 $str .= "PropertyClass Eq '".$key ."' And CloseDate Bt ".$recent_sold.",".$current_year."";
				} else {
					$str .= " Or PropertyClass Eq '".$key ."' And CloseDate Bt ".$recent_sold.",".$current_year."";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		
		if(!empty($house_size)) {
			$filterStr .= " And (BuildingAreaTotal Ge ".$house_size.")";
		}
		if(!empty($lot_size)) {
			$filterStr .= " And (LotSizeArea Ge ".$lot_size." Or BuildingAreaTotal Ge ".$lot_size." Or LotSizeSquareFeet Ge ".$lot_size.")";
		}
		if(!empty($house_age)) {
			$current_year = date("Y");
			if($house_age == 'fiveyears') {
				$year_built = $current_year - 5;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'tenyears') {
				$year_built = $current_year - 10;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'fifthteenyears') {
				$year_built = $current_year - 15;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'twentyyears') {
				$year_built = $current_year - 20;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'fiftyyears') {
				$year_built = $current_year - 50;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'fiftyyearsplus') {
				$year_built = $current_year - 51;
				$filterStr .= " And (YearBuilt Le ".$year_built.")";
			}
		}

		/*** PROPERTY SUB TYPES ***/
		if(!empty($a_type)) {
			$str = " And (";
			$count = 0;
			foreach ($a_type as $key) {
				if($count == 0) {
					if($key == "Single Family")
						$str .= "PropertySubType Eq '*".$key."*' Or PropertySubType Eq '*Detached*'";
					else
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					if($key == "Single Family")
						$str .= " Or PropertySubType Eq '*".$key."*' Or PropertySubType Eq '*Detached*'";
					else
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($b_type)) {
			$str = " And (";
			$count = 0;
			foreach ($b_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($c_type)) {
			$str = " And (";
			$count = 0;
			foreach ($c_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($d_type)) {
			$str = " And (";
			$count = 0;
			foreach ($d_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($e_type)) {
			$str = " And (";
			$count = 0;
			foreach ($e_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($f_type)) {
			$str = " And (";
			$count = 0;
			foreach ($f_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($listing_change_type)) {
			$str = " And (";
			$count = 0;
			foreach ($listing_change_type as $key) {
				if($count == 0) {
					$str .= "MajorChangeType Eq '".$key ."' Or PublicRemarks Eq '*".$key ."*'";
				} else {
					$str .= " Or MajorChangeType Eq '".$key ."' Or PublicRemarks Eq '*".$key ."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($features)) {
			$str = " And (";
			$count = 0;
			foreach($features as $key) {
				if($count == 0) {
					$str .= "PublicRemarks Eq '*".$key ."*'";
				} else {
					$str .= " Or PublicRemarks Eq '*".$key ."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}

		$obj = modules::load("dsl");
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$config = $obj->get_config();

		$token = $this->dsl_model->getUserAccessTokenOld($this->session->userdata("agent_id"));

		if($token) {
			$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ""
			);
		} else {
			$bearer = $this->dsl_model->getUserBearerToken($this->session->userdata("agent_id"));

			$access_token = array(
				'access_token' =>"",
				'bearer_token' => $bearer->bearer_token
			);
		}

		$endpoint = "spark-listings?_limit=".$limit."&_pagination=1&_page=".$page."&_filter=(".$filter_string.")&_expand=PrimaryPhoto";

		$results = $obj->post_endpoint($endpoint, $access_token);
		$return = json_decode($results, true);

		return $return;
	}
}