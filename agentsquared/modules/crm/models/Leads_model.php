<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads_model extends CI_Model {

	public function __construct() {
        parent::__construct();

    }

	public function set_leads_capture_config($data=array()) {

		if($data) {

			$data['date_modified'] = date("Y-m-d H:m:s");

			$this->db->where('user_id', $this->session->userdata('user_id'));
			$this->db->update('leads_config', $data);

			if($this->db->affected_rows()>0) {
				return TRUE;
			} else {
				$data['date_created'] = date("Y-m-d H:m:s");
				$this->db->insert('leads_config', $data);
				return ($this->db->insert_id()) ? TRUE : FALSE;
			}

		}

		return FALSE;

	}

    public function get_leads_config() {

        $this->db->select("*")
                ->from("leads_config")
                ->where("user_id", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get()->row();

        return (!empty($query)) ? $query : FALSE;

    }

    public function save_contacts( $datas = NULL){

        $password = "agentsquared1234";

        $save["contact_id"] = $datas["Id"];
        $save["first_name"] = ( isset($datas["GivenName"]) && !empty($datas["GivenName"]) ) ? $datas["GivenName"] : '';
        $save["last_name"] = ( isset($datas["FamilyName"]) && !empty($datas["FamilyName"]) ) ? $datas["FamilyName"] : '';
        $save["email"] = ( isset($datas["PrimaryEmail"]) && !empty($datas["PrimaryEmail"]) ) ? $datas["PrimaryEmail"] : '';
        $save["phone"] = ( isset($datas["PrimaryPhoneNumber"]) && !empty($datas["PrimaryPhoneNumber"]) ) ? $datas["PrimaryPhoneNumber"] : '';
        $save["modified"] = ( isset($datas["ModificationTimestamp"]) && !empty($datas["ModificationTimestamp"]) ) ? $datas["ModificationTimestamp"] : '';
        $save["created"] = date("Y-m-d H:m:s");
        $save["agent_id"] = $this->session->userdata("agent_id");
        $save["user_id"] = $this->session->userdata("user_id");
        $save["type"] = "flex";
        $save["ip_address"] = $_SERVER['REMOTE_ADDR'];
        $save["password"] = '$2y$10$nJ5lOGF8nVh4nt6iNvvAPOMODWwEj/raSlcHy1HmD0Q/iibQPRWh6'; //password_hash( $password, PASSWORD_DEFAULT);
        $save["active"] = "1";

        $this->db->insert( "contacts", $save );

        return ($this->db->insert_id()) ? FALSE : TRUE;

    }
    public function get_contacts( $counter = FALSE, $param = array() )
    {
        $this->db->select("*")
                ->from("contacts")
                ->order_by('id', 'desc');

        $sql_str = "user_id = '".$this->session->userdata('user_id')."' AND agent_id='".$this->session->userdata('agent_id')."' AND is_deleted=0";

        if($this->input->get('type')) {
            $sql_str .= " AND type='".$this->input->get('type')."'";
        }

        if($this->input->get('keywords')) {

            $keywords = $this->input->get('keywords');
            $key_arr = explode(" ", $keywords);
            
            $sql_str .= " AND (first_name='".$keywords."' OR last_name='".$keywords."' OR email='".$keywords."' OR phone='".$keywords."'";
            $sql_str .= " OR first_name LIKE '%".$keywords."%' OR last_name LIKE '%".$keywords."%' OR email LIKE '%".$keywords."%'";
            $sql_str .= " OR CONCAT_WS(' ', first_name, last_name) LIKE '%".$keywords."%')";

        }

        $this->db->where($sql_str);

        if($counter) {
            return $this->db->count_all_results();
        }

        if(!empty($param["limit"])) {
            $this->db->limit( $param["limit"]);
        }

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);
        }

        $this->db->order_by("modified", "desc");

        $query = $this->db->get();

        if($query->num_rows() > 0) {

            $contacts =  $query->result();

            foreach ($contacts as &$contact) {
               $contact->favourites = $this->CountFavorites( $contact->id );
               $save_search_db = $this->CountSaveSearch( $contact->id );
               $save_search_flex = $this->CountSaveSearchFlex( $contact->contact_id );
               $contact->save_search = $save_search_db->count + $save_search_flex;
               $contact->messages = $this->CountMessages( $contact->id );
               $contact->schedule_showings = $this->CountScheduleShowings( $contact->id );
            }

            return $contacts;
        }

        return FALSE;
    }

    public function get_all_contacts(){

        $this->db->select("contact_id")
                ->from("contacts")
                ->where("user_id", $this->session->userdata('user_id'))
                ->where("agent_id", $this->session->userdata('agent_id'));

        $query = $this->db->get();

        if($query->num_rows() > 0) {

            return $query->result();
        }

        return FALSE;
    }

    public function get_contact_by_email($email) {
        $this->db->select("contact_id")
                ->from("contacts")
                ->where("email", $email);

        $query = $this->db->get();

        if($query->num_rows() > 0) {

            return $query->result();
        }

        return FALSE;
    }

    public function get_contact_by_email_and_user_id($email) {
        $this->db->select("contact_id")
                ->from("contacts")
                ->where("user_id", $this->session->userdata('user_id'))
                ->where("email", $email);

        $query = $this->db->get();

        if($query->num_rows() > 0) {

            return $query->result();
        }

        return FALSE;
    }

    public function get_customer($data=array(),$type=NULL) {
        $ret = FALSE;

        if($data && $type) {
            $customer = $this->db->select("*")->from("customer_".$type)->where($data)->get()->result();
            $ret = ($customer) ? $customer : FALSE;
        }

        return $ret;

    }

    public function get_customer_notes( $id = NULL){
        $this->db->select("*")
                ->from("customer_notes")
                ->where("property_id", $id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function get_customer_message( $id = NULL)
    {
        $this->db->select("*")
                ->from("customer_messaging")
                ->where("property_id", $id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function CountFavorites( $id = NULL ){

        $this->db->select(" COUNT(cpid) as count")
                ->from("customer_properties")
                ->where("customer_id", $id);

        return $this->db->get()->row();

    }

    public function CountSaveSearch( $id = NULL ){

        $this->db->select(" COUNT(csid) as count")
                ->from("customer_searches")
                ->where("customer_id", $id);

        return $this->db->get()->row();

    }

    public function CountMessages( $id = NULL ){

        $this->db->select(" COUNT(id) as count")
                ->from("customer_messaging")
                ->where("customer_id", $id);

        return $this->db->get()->row();

    }
     public function CountScheduleShowings( $id = NULL ){

        $this->db->select(" COUNT(cssid) as count")
                ->from("customer_schedule_of_showing")
                ->where("customer_id", $id);

        return $this->db->get()->row();

    }

    public function CountSaveSearchFlex( $contact_id = NULL )
    {

        $this->db->select("id,json_saved_search_updated,img,date_modified")
                ->from("saved_searches")
                ->where("FIND_IN_SET('".$contact_id."',ContactIds) != ",0);

        $query = $this->db->get();

        return $query->num_rows();

    }

    public function get_profile_info( $contact_id = NULL ){
        $this->db->select("*")
                ->from("contacts")
                ->where("contact_id", $contact_id)
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {

            $profile = $query->row();

            $profile->favourites = $this->getFavouritesList( $profile->id );
            $profile->save_searches = $this->getSaveSearches( $profile->id );
            $profile->messages = $this->getMessages( $profile->id );
            $profile->schedule_showing = $this->getSchedule( $profile->id );
            $profile->notes = $this->getNotes( $contact_id );
            $profile->tasks = $this->getTasks( $contact_id );

            $save_searches = $this->getSaveSearches( $profile->id );
            $save_searches_flex = $this->getSaveSearchesFlex( $contact_id );
            $save_searches_flex = ( empty($save_searches_flex) ) ? array() : $save_searches_flex;

            $profile->saveSearches = array_merge($save_searches, $save_searches_flex);

            return $profile;
        }

        return FALSE;

    }

    public function get_profile_messages( $contact_id = NULL ){
        $this->db->select("*")
                ->from("contacts")
                ->where("contact_id", $contact_id)
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {

            $profile = $query->row();
            $profile->messages = $this->getMessages( $profile->id );

            return $profile;
        }

        return FALSE;

    }

    public function get_profile_messages_pagination( $contact_id = NULL, $param = array() ){

        $this->db->select("*")
                ->from("contacts")
                ->where("contact_id", $contact_id)
                ->limit(1);

        $query = $this->db->get();

        if( isset($param["limit"]) ){

            $this->db->limit( $param["limit"] );
        }

        if( !empty($param["start"]) ){

            $this->db->offset( $param["start"] );
        }

        if($query->num_rows() > 0) {

            $profile = $query->row();
            $profile->messages = $this->getMessages( $profile->id );

            return $profile;
        }

        return FALSE;

    }

    public function getFavouritesList( $id = NULL ){

        $this->db->select("*")
                ->from("customer_properties")
                ->where("customer_id", $id);

        return $this->db->get()->result();
    }

    public function getContactFavouritesList( $id = NULL ){

        $this->db->select("*")
                ->from("customer_properties")
                ->where("contact_id", $id);

        return $this->db->get()->result();
    }

    public function getSaveSearches( $id = NULL ){
        $this->db->select("*")
                ->from("customer_searches")
                ->where("customer_id", $id);

        return $this->db->get()->result();
    }

    public function getContactSaveSearches( $id = NULL ){
        $this->db->select("*")
                ->from("customer_searches")
                ->where("contact_id", $id);

        return $this->db->get()->result();
    }

    public function getMessages( $id = NULL ){

        $this->db->select("customer_messaging.*, CONCAT(contacts.first_name ,' ', contacts.last_name) as contact_name, CONCAT(users.first_name ,' ', users.last_name) as agent_name ")
                ->from("customer_messaging")
                ->join("contacts", "customer_messaging.contact_id = contacts.contact_id", "left")
                ->join("users", "users.agent_id = customer_messaging.agent_id", "left")
                ->where("customer_messaging.customer_id", $id)
                ->where("customer_messaging.agent_id !=", 0)
                ->where("customer_messaging.type !=","email")
                ->order_by("id", "desc");


        return $this->db->get()->result();
    }

    public function getContactMessages( $id = NULL ){

        $this->db->select("customer_messaging.*, CONCAT(contacts.first_name ,' ', contacts.last_name) as contact_name, CONCAT(users.first_name ,' ', users.last_name) as agent_name ")
                ->from("customer_messaging")
                ->join("contacts", "customer_messaging.contact_id = contacts.contact_id", "left")
                ->join("users", "users.agent_id = customer_messaging.agent_id", "left")
                ->where("customer_messaging.contact_id", $id)
                ->where("customer_messaging.agent_id !=", 0)
                ->order_by("id", "desc");


        return $this->db->get()->result();
    }

    public function getSchedule( $id = NULL ){
        $this->db->select("*")
                ->from("customer_schedule_of_showing")
               ->where("customer_id", $id);

        return $this->db->get()->result();
    }

    public function getContactSchedule( $id = NULL ){
        $this->db->select("*")
                ->from("customer_schedule_of_showing")
               ->where("contact_id", $id);

        return $this->db->get()->result();
    }

    public function update_profile( $contact_id = NULL ){

        $profile = $this->input->post("profile");

        $this->db->where("contact_id", $contact_id);
        $this->db->update("contacts", $profile);

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;

    }

    public function save_email( $customer_id = "", $contact_id = "" ){

        $save["customer_id"] =  $customer_id;
        $save["contact_id"] =  $contact_id;
        $save["agent_id"] =  $this->session->userdata("agent_id");
        $save["sent_by"] =  $this->session->userdata("agent_id");
        $save["created"] =  date("Y-m-d H:m:s");
        $save["type"] = "email";
        $save["message"] = $_POST["message"];

        $this->db->insert("customer_messaging", $save);

        return ( $this->db->insert_id() ) ? $this->db->insert_id() : FALSE;
    }

    public function get_activities( $contact_id = NULL )
    {
        $this->db->select("*")
                ->from("core_activities")
                ->where("contact_id", $contact_id)
                ->where("agent_id", $this->session->userdata("agent_id") )
                ->order_by("id","desc");

        return $this->db->get()->result();
    }

    public function getSaveSearchesFlex( $contact_id = NULL )
    {

        $this->db->select("id,json_saved_search_updated,img,date_modified")
                ->from("saved_searches")
                ->where("FIND_IN_SET('".$contact_id."',ContactIds) != ",0);

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            $data = $query->result();

            foreach ($data as &$key) {
                $key->json_decoded = json_decode($key->json_saved_search_updated);
            }

            return $data;
        }

        return FALSE;
    }

    public function get_message_contect_ajax( $message_id = NULL )
    {
        $this->db->select("*")
                ->from("customer_messaging")
                ->where("id", $message_id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function add_notes( $contact_id = "" ){

        $notes["created_by"] = $this->session->userdata("agent_id");
        $notes["notes"] = $this->input->post("notes");
        $notes["contact_id"] = $contact_id;
        $notes["date_created"] = date("Y-m-d H:i:s");

        $this->db->insert("contact_notes", $notes);

        return ( $this->db->insert_id() ) ? $this->db->insert_id() : FALSE;
    }

    public function add_tasks( $contact_id = "" ){

        $tasks["created_by"] = $this->session->userdata("agent_id");
        $tasks["tasks"] = $this->input->post("tasks");
        $tasks["contact_id"] = $contact_id;
        $tasks["date_created"] = date("Y-m-d H:i:s");

        $this->db->insert("contact_tasks", $tasks);

        return ( $this->db->insert_id() ) ? $this->db->insert_id() : FALSE;
    }

    public function getNotes( $contact_id = NULL )
    {
        $this->db->select("cn.*, CONCAT(u.first_name ,' ', u.last_name) as created_by")
                ->from("contact_notes cn")
                ->join("users u", "cn.created_by = u.agent_id", "left")
                ->where("cn.contact_id", $contact_id)
                ->order_by("cn.id", "desc");

        return $this->db->get()->result();

    }

    public function getTasks( $contact_id = NULL )
    {
        $this->db->select("ct.*, CONCAT(u.first_name ,' ', u.last_name) as created_by")
                ->from("contact_tasks ct")
                ->join("users u", "ct.created_by = u.agent_id", "left")
                ->where("ct.contact_id", $contact_id)
                ->order_by("ct.id", "desc");

        return $this->db->get()->result();
    }

    public function delete_notes() {        

        $this->db->where("id", $this->input->post('noteid'));
        $this->db->delete("contact_notes");

        if($this->db->affected_rows()>0) {

            $this->db->where("contact_id", $this->input->post("contact_id"));
            $this->db->where("activity_id", $this->input->post("noteid"));
            $this->db->where("agent_id", $this->session->userdata("agent_id"));
            $this->db->where("activity_type", "added notes");
            $this->db->delete("core_activities");

            return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        }

        return FALSE;
    }

    public function delete_tasks() {

        $this->db->where("id", $this->input->post("taskid"));
        $this->db->delete("contact_tasks");

        if($this->db->affected_rows()>0) {

            $this->db->where("contact_id", $this->input->post("contact_id"));
            $this->db->where("activity_id", $this->input->post("taskid"));
            $this->db->where("agent_id", $this->session->userdata("agent_id"));
            $this->db->where("activity_type", "added tasks");
            $this->db->delete("core_activities");

            return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        }

        return FALSE;
    }

    public function get_contacts_lists()
    {
        $this->db->select("*")
                ->from("contacts")
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->where("is_deleted", 0)
                ->order_by("id", "asc");

        return $this->db->get()->result();
    }

    public function insert_csv( $insert_csv = array() )
    {
        $this->db->insert("contacts", $insert_csv );

        return ($this->db->insert_id()) ? TRUE : FALSE;
    }

    public function update_csv($data) {
        $this->db->where('email', $data['email']);
        $this->db->where('user_id', $this->session->userdata("user_id"));
        $this->db->update('contacts', $data);
        return ( $this->db->affected_rows() > 0 ) ? TRUE : FALSE;
    }

    public function is_contacts_exists($datas)
    {
        $this->db->select("email")
                ->from("contacts")
                ->where("email", $datas["PrimaryEmail"])
                ->where("user_id", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? TRUE : FALSE;

    }

    public function get_capture_leads($agent_id) {

        $this->db->select('capture_leads, is_email_market')
                ->from('users')
                ->where('agent_id', $agent_id);

        return $this->db->get()->row();
    }

    public function set_capture_leads($data) {

        $this->db->where('agent_id', $this->session->userdata('agent_id'));
        $this->db->update('users', $data);

    }

    public function get_agent_domain() {

        $arr = array(
            'agent' => $this->session->userdata('user_id'),
            'type'  => 'agent_site_domain',
            'status' => 'completed'
        );

        $this->db->select("domains")
                        ->from("domains")
                        ->where($arr);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->row()->domains : FALSE;
    }

    public function delete_lead($contact_id = NULL) {

        $this->db->where("contact_id", $contact_id);
        $this->db->update("contacts", array("is_deleted" => 1));

        return ( $this->db->affected_rows() > 0 ) ? TRUE : FALSE;
    }

    public function get_contacts_flex( $counter = FALSE, $param = array() )
    {
        $this->db->select("*")
                ->from("contacts")
                ->where("user_id", $this->session->userdata("user_id"))
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->where("type", "flex")
                ->where("is_deleted", 0);

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        $this->db->order_by("id","desc");

        $query = $this->db->get();

        if( $query->num_rows() > 0 ){
            $contacts =  $query->result();

            return $contacts;
        }

        return FALSE;
    }

    public function update_contacts( $datas = NULL){
        $display_name = ( isset($datas["DisplayName"]) && !empty($datas["DisplayName"]) ) ? explode(' ', $datas["DisplayName"]) : '';

        // $save["first_name"] = ( isset($datas["GivenName"]) && !empty($datas["GivenName"]) ) ? $datas["GivenName"] : $display_name[0];
        // $save["last_name"] = ( isset($datas["FamilyName"]) && !empty($datas["FamilyName"]) ) ? $datas["FamilyName"] : $display_name[1];
        $save["first_name"] =  ( isset($display_name[0]) && !empty($display_name[0]) ) ? $display_name[0] : $datas['GivenName'];
        $save["last_name"] = ( isset($display_name[1]) && !empty($display_name[1]) ) ? $display_name[1] :$datas['FamilyName'];
        $save["email"] = ( isset($datas["PrimaryEmail"]) && !empty($datas["PrimaryEmail"]) ) ? $datas["PrimaryEmail"] : '';
        $save["phone"] = ( isset($datas["PrimaryPhoneNumber"]) && !empty($datas["PrimaryPhoneNumber"]) ) ? $datas["PrimaryPhoneNumber"] : '';
        $save["modified"] = ( isset($datas["ModificationTimestamp"]) && !empty($datas["ModificationTimestamp"]) ) ? $datas["ModificationTimestamp"] : '';
        
        $this->db->where("contact_id", $datas["Id"]);
        $this->db->where("user_id",  $this->session->userdata("user_id"));
        $this->db->update( "contacts", $save );

        return ($this->db->affected_rows()) ? TRUE : FALSE;

    }
}
