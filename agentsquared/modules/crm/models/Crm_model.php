<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_model extends CI_Model {

	public function __construct() {
        parent::__construct();

    }

    public function get_customers_info( $counter = FALSE, $param = array() )
    {
        $this->db->select("u.id as user_id, u.email as orig_email, u.first_name as orig_fname, u.last_name as orig_lname, u.created_on as created_on,cp.*")
                ->from("users u")
                ->join("customer_profile cp", "u.id = cp.customer_id", "left")
                ->join("users_groups ug", "ug.user_id = u.id", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->where("ug.group_id", 3)
                ->order_by("u.id");

        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = strtotime($param["start_date"]);
            $end_date = strtotime($param["end_date"]);

            $this->db->where('created_on >= "'.$start_date.'" AND created_on <= "'.$end_date.'" ' );
        }

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db->where("(u.first_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db->where("u.last_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db->or_where("cp.first_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("cp.last_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("u.email LIKE '%$param[keywords]%'");
            $this->db->or_where("u.phone LIKE '%$$param[keywords]%')");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db->limit( $param["limit"] );
        }

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }

        return $this->db->get()->result();
    }

    public function get_save_search_properties( $counter = FALSE, $param = array() )
    {
        $this->db->select("cs.*, cp.*, u.email as orig_email")
                ->from("customer_searches cs")
                ->join("users u", "cs.customer_id = u.id", "left")
                ->join("customer_profile cp", "cp.customer_id = u.id", "left")
                ->join("users_groups ug", "ug.user_id = u.id", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->where("ug.group_id", 3)
                ->order_by("cs.csid");

        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = strtotime($param["start_date"]);
            $end_date = strtotime($param["end_date"]);

            $this->db->where('created_on >= "'.$start_date.'" AND created_on <= "'.$end_date.'" ' );
        }

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db->where("(u.first_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db->where("u.last_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db->or_where("cp.first_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("cp.last_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("u.email LIKE '%$param[keywords]%'");
            $this->db->or_where("u.phone LIKE '%$$param[keywords]%')");
        }

        if( $counter )
        {
            $query = $this->db->get();
            return $query->num_rows();
        }

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db->limit( $param["limit"] );
        }

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }

        $query = $this->db->get();

        return $query->result_array();

    }

    private function get_customer_searches( $customer_id = NULL )
    {
        $this->db->select("cs.*")
                ->from("customer_searches cs")
                ->where("cs.customer_id", $customer_id);

        return $this->db->get()->result_array();
    }

    public function get_save_favorates_properties( $counter = FALSE, $param = array() )
    {
        $this->db->select("cp.*,cpp.*, u.email as orig_email")
                ->from("customer_properties cp")
                ->join("users u", "cp.customer_id = u.id", "left")
                ->join("customer_profile cpp", "cpp.customer_id = u.id", "left")
                ->join("users_groups ug", "ug.user_id = u.id", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->where("ug.group_id", 3)
                ->order_by("cp.cpid");

        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = strtotime($param["start_date"]);
            $end_date = strtotime($param["end_date"]);

            $this->db->where('created_on >= "'.$start_date.'" AND created_on <= "'.$end_date.'" ' );
        }

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db->where("(u.first_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db->where("u.last_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db->or_where("cp.first_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("cp.last_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("u.email LIKE '%$param[keywords]%'");
            $this->db->or_where("u.phone LIKE '%$$param[keywords]%')");
        }

        if( $counter )
        {
            $query = $this->db->get();
            return $query->num_rows();
        }

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db->limit( $param["limit"] );
        }

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }

        $query = $this->db->get();

        if( $counter )
        {
            return $query->num_rows();
        }

        return $query->result_array();
    }

    public function get_buyers_info()
    {
        $this->db->select("*")
                ->from("customer_buyer")
                ->where("agent_id", $this->session->userdata("user_id"))
                ->order_by("id", "desc");

        return $this->db->get()->result();
    }

    public function get_seller_info()
    {
        $this->db->select("*")
                ->from("customer_seller")
                ->where("agent_id", $this->session->userdata("user_id"))
                ->order_by("id", "desc");

        return $this->db->get()->result();
    }

    public function get_messages_info()
    {
        $this->db->select("cq.*")
                ->from("customer_questions cq")
                ->where("agent_id", $this->session->userdata("user_id"))
                ->order_by("cqid", "desc");

        $query =  $this->db->get();

        if( $query->num_rows() > 0 )
        {
            $datas = $query->result();

            foreach ($datas as &$key) {
                $key->messages = $this->get_message( $key->cqid );
            }

            return $datas;

        }
        return FALSE;

    }

    public function get_message( $qid = NULL )
    {
        $this->db->select("*")
                ->from("customer_messaging")
                ->where("mid", $qid);

        return $this->db->get()->result();
    }

    public function insert_reply()
    {
        $reply["message"] = $this->input->post("message");
        $reply["type"] = "email";
        $reply["mid"] = $this->input->post("mid");
        $reply["created"] = date("Y-m-d H:m:s");

        $this->db->insert("customer_messaging", $reply);

        return ($this->db->insert_id()) ? $this->db->insert_id() : FALSE;
    }

    public function get_customer_info( $message_id = NULL)
    {
        $this->db->select("*")
                ->from("customer_questions")
                ->where("cqid", $message_id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function get_agent_info() {

        $this->db->select("u.first_name, u.last_name, u.phone, u.company, u.agent_photo, u.logo, d.domains as domain_name, d.status as domain_status, d.type as domain_type, d.subdomain_url, d.is_ssl")
                ->from("users u")
                ->join("domains d", "u.id=d.agent")
                ->where("u.id", $this->session->userdata("user_id"))
                ->limit(1);

        $data["agent_info"] = $this->db->get()->row();

        $obj = Modules::load("mysql_properties/mysql_properties");
        $data["account_info"] = $obj->get_account($this->session->userdata("user_id"), "account_info");

        return $data;

    }

    public function get_schedule_of_showing( $counter = FALSE, $param = array() )
    {
        $this->db->select("css.*, css.created as showing_created ,cp.*, (select u.email from users u where u.id = css.customer_id LIMIT 1) as orig_email")
                ->from("customer_schedule_of_showing css")
                ->join("customer_profile cp", "cp.customer_id = css.customer_id", "left")
                //->join("users u", "u.id = css.customer_id", "left")
                ->where("css.agent_id", $this->session->userdata("user_id"))
                 ->where("css.type", "agent_site")
                ->order_by("cssid", "desc");

        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = date("Y-m-d", strtotime($param["start_date"]) );
            $end_date = date("Y-m-d", strtotime($param["end_date"]) );

            $this->db->where('css.date_scheduled >= "'.$start_date.'" AND css.date_scheduled <= "'.$end_date.'" ' );
        }

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db->where("(css.property_name LIKE '%$param[keywords]%' ");
            $this->db->or_where("cp.first_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("cp.last_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("u.email LIKE '%$param[keywords]%'");
            $this->db->or_where("u.phone LIKE '%$$param[keywords]%')");
        }

        if( $counter )
        {
            $query = $this->db->get();
            return $query->num_rows();
        }

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db->limit( $param["limit"] );
        }

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }

        $query = $this->db->get();

        if( $counter )
        {
            return $query->num_rows();
        }

        return $query->result();

    }

     public function get_spw_schedule_of_showing( $counter = FALSE, $param = array() )
     {
	    $this->db->from("customer_schedule_of_showing css")
                ->join("contacts as c", "c.id = css.customer_id", "left")
                ->where("css.agent_id", $this->session->userdata("user_id") )
                ->where('(css.type="spw" OR css.type="agent_site")');

        if( (isset($param["start_date"]) && '' !== $param["start_date"]) AND (isset($param["end_date"]) && '' !== $param["end_date"]) )
        {
            $start_date = date("Y-m-d", strtotime($param["start_date"]) );
            $end_date = date("Y-m-d", strtotime($param["end_date"]) );

	        $date_filtered = "css.date_scheduled >= '{$start_date}' AND css.date_scheduled <= '{$end_date}'";
			$order_field = 'css.date_scheduled';

            if ( isset($param['filter-date']) && 'created' === $param['filter-date'] )
            {
	            $date_filtered = "DATE(css.created) >= '{$start_date}' AND DATE(css.created) <= '{$end_date}'";
	            $order_field = 'css.created';
            }

            $this->db
	            ->where($date_filtered)
	            ->order_by($order_field, 'desc');
        }

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db->where("(css.property_name LIKE '%$param[keywords]%' ");
            $this->db->or_where("css.customer_name LIKE CONCAT('%$param[keywords]%')");
            $this->db->or_where("c.first_name LIKE '%$param[keywords]%'");
            $this->db->or_where("c.last_name LIKE '%$param[keywords]%'");
            $this->db->or_where("css.customer_email LIKE '%$param[keywords]%'");
            $this->db->or_where("c.email LIKE '%$param[keywords]%'");
            $this->db->or_where("css.customer_number LIKE '%$param[keywords]%')");
            $this->db->or_where("c.phone LIKE ('%$param[keywords]%')");
            $this->db->or_where("css.type LIKE '%$param[keywords]%'");
        }

        if( $counter )
        {
	        return $this->db->count_all_results();
        }

	    $this->db->select(
	    	"css.*, 
	    	c.phone as customer_number_agentsite,
	    	c.contact_id,
	    	c.email as customer_email_agentsite,
	    	CONCAT(c.first_name ,' ', c.last_name) as customer_name_agentsite"
	    );

        if( isset($param["limit"]) AND (int)$param["limit"] !== 0)
        {
            $this->db->limit( $param["limit"] );
        }

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }

	    return $this->db->get()->result();
    }

    public function get_property_sold( $counter = FALSE, $param = array() ){

        $this->db->select("*")
                ->from("property_activated")
                ->where("property_status", "Sold");

        if($counter)
        {
            return $this->db->count_all_results();
        }

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();
        }

        return FALSE;
    }

    public function get_favorites_property( $counter = FALSE, $param = array() ){
        //echo $this->session->userdata("code"); exit;
        $this->db->select("cp.*,u.code")
                ->from("customer_properties cp")
                ->join("users u", "cp.customer_id = u.id", "left")
                ->join("users_groups ug", "ug.user_id = 3", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->group_by('cp.property_id');

        if($counter)
        {
            return $this->db->count_all_results();
        }

        $query = $this->db->get();
        //printA($query->result()); exit;
        if( $query->num_rows() > 0 )
        {
            $datas =  $query->result_array();
            foreach ($datas as &$key) {
               $key["counter"] = $this->count_favorates_properties($key["property_id"]);
            }

            return $datas;
        }

        return FALSE;
    }

    private function count_favorates_properties( $property_id = NULL )
    {
        $this->db->select()
                ->from("customer_properties")
                ->where("property_id", $property_id);

        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_search_property( $counter = FALSE, $param = array() ){

        $this->db->select("cs.*")
                ->from("customer_searches cs")
                ->join("users u", "cs.customer_id = u.id", "left")
                ->join("users_groups ug", "ug.user_id = 3", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->group_by('cs.details');

        if($counter)
        {
            return $this->db->count_all_results();
        }

        $query = $this->db->get();
        //printA($query->result()); exit;
        if( $query->num_rows() > 0 )
        {
            $datas =  $query->result_array();

            foreach ($datas as &$key) {
               $key["counter"] = $this->count_search_properties($key["details"]);
            }

            return $datas;
        }

        return FALSE;
    }

    private function count_search_properties( $details = NULL )
    {
        $this->db->select()
                ->from("customer_searches")
                ->like("details", $details);

        $query = $this->db->get();

        return $query->num_rows();
    }

    function delete_spw_schedule($id) {
        $this->db->where("cssid", $id);
        $this->db->delete("customer_schedule_of_showing");

        return $this->db->affected_rows()>0;
    }

}
