<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Scheduled Showings   </span>
    </h3>
    <p>Enter schedules for meetings with your client</p>
  </div>
    <section class="content">
        <div class="row">
            <!-- <div class="col-md-4">
                <div class="datepicker-area">
                    <div class="input-group date" data-provide="datepicker">
                    </div>    
                </div>
            </div> -->
            <div class="col-md-12">
                <div class="meeting-list">
                   <div class="col-md-10 col-sm-12 col-xs-12 pull-left date-search">                         
                    <div class="row">
                        <div class="col-md-10 col-sm-7 col-xs-12 pull-left date-search">                         
                             <form class="form-inline" action="" method="get" >                                  
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail3"></label>
                                    <input type="input" name="start_date" value="<?php echo (isset($_GET["start_date"])) ? $_GET["start_date"] : ""; ?>" class="form-control" id="startdatepicker" placeholder="Start Date">
                                  </div>
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword3"></label>
                                    <input type="input" name="end_date" value="<?php echo (isset($_GET["end_date"])) ? $_GET["end_date"] : ""; ?>" class="form-control" id="enddatepicker" placeholder="End Date">
                                  </div>                                 
                                  <button type="submit" class="btn btn-success">Search</button>
                            </form>
                        </div>
                        <div class="col-md-2  col-sm-5 col-xs-12">                         
                             <form class="form-inline" action="" method="get" >
                                  <div class="form-group">
                                    <div class="input-group input-group-md" style="width: 150px;">
                                      <input type="input" name="keywords"  value="<?php echo (isset($_GET["keywords"])) ? $_GET["keywords"] : ""; ?>" class="form-control" id="keywords" placeholder="keywords">
                                      <div class="input-group-btn">
                                        <button type="submit" class="btn btn-success">Search</button>
                                      </div>
                                    </div>
                                  </div>
                            </form>
                        </div>
                    </div>
                    <p class="clearfix"></p>
                    <p class="clearfix"></p>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Date Schedule</th>
                                    <th>Notes</th>
                                    <th>Date Created</th>
                                    <th>Property</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($schedules) ) : 
                                    foreach( $schedules as $schedule) : 
                                ?>
                                        <tr>
                                            <td><?php echo (isset($schedule->first_name) AND !empty($schedule->first_name)) ? $schedule->first_name.' '.$schedule->last_name: "Customer"; ?></td>
                                            <td><?php echo $schedule->orig_email; ?></td>
                                            <td><?php echo date("F d, Y", strtotime($schedule->date_scheduled))?></td>
                                            <td><?php echo $schedule->message;?></td>
                                            <td><?php echo date("F d, Y H:m:s",strtotime($schedule->showing_created))?></td>                                        
                                            <td><a href="<?php echo site_url("other-property-details/".$schedule->property_id); ?>" target="_blank"><?php echo $schedule->property_name;?></a></td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else : ?>
                                    <tr align="center"><td colspan="10"><i>no records found!</i></td></tr>
                                <?php endif;?>
                                
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                        <?php if( $pagination ) : ?>
                            <div class="pagination-area pull-right">
                                <nav>
                                    <ul class="pagination">
                                        <?php echo $pagination; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>