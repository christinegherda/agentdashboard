<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Customers</span>
    </h3>
  </div>
    <section class="content">
        <div class="row">
            <div class="reports-content">
                <div class="col-md-12">
                    
                        <div class="col-md-7 col-xs-7 pull-left">                         
                             <form class="form-inline" action="" method="get" >                                  
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail3"></label>
                                    <input type="input" name="start_date" value="<?php echo (isset($_GET["start_date"])) ? $_GET["start_date"] : ""; ?>" class="form-control" id="startdatepicker" placeholder="Start Date">
                                  </div>
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword3"></label>
                                    <input type="input" name="end_date" value="<?php echo (isset($_GET["end_date"])) ? $_GET["end_date"] : ""; ?>" class="form-control" id="enddatepicker" placeholder="End Date">
                                  </div>                                 
                                  <button type="submit" class="btn btn-success">Search</button>
                            </form>
                        </div>
                        <div class="col-md-4 col-xs-4 pull-right">                         
                             <form class="form-inline" action="" method="get" >
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail3"></label>
                                    <input type="input" name="keywords"  value="<?php echo (isset($_GET["keywords"])) ? $_GET["keywords"] : ""; ?>" class="form-control" id="keywords" placeholder="keywords">
                                  </div>
                                                                
                                  <button type="submit" class="btn btn-success">Search</button>
                            </form>
                        </div>
                        
                    
                    <div class="row" style="margin: 80px 0px 0px 10px" >
                        <p>Total Customers: <span class="badge badge-info"><?php echo (!empty($total)) ? $total : 0; ?></span></p>
                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Mobile</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($customers) ) :
                                    foreach ( $customers as $customer ) : 
                                ?>
                                    <tr>
                                        <td><?=(isset($customer->first_name)) ? $customer->first_name.' '.$customer->last_name : "Customer"; ?></td>
                                        <td><?=(isset($customer->email)) ? $customer->email : $customer->orig_email;?></td>
                                        <td><?=(isset($customer->phone)) ? $customer->phone : "" ;?></td>
                                        <td><?=(isset($customer->mobile)) ? $customer->mobile : "";?></td>
                                        <td><?=date("F d, Y", $customer->created_on)?></td>
                                    </tr>
                                   
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="center"><td colspan="8"><i>no records found!</i></td></tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <?php if( $pagination ) : ?>
                            <div class="pagination-area pull-right">
                                <nav>
                                    <ul class="pagination">
                                        <?php echo $pagination; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                </div>
                <!-- <div class="col-md-6">
                    <h4>2016 Properties Sold per Month</h4>
                    <hr>
                    <div id="salesummary" style=""></div>
                </div> -->
               
                
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>