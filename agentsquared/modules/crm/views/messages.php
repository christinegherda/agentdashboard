<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Messages</span>
    </h3>
    <p>You've got mail!</p>
  </div>
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <h4>Clients</h4>

                    <?php $i=1; if( !empty($messages) ) :  ?>

                    <ul class="message-clients">

                    <?php
                            foreach ($messages as $message) :
                    ?>
                                <li class="<?=($i==1) ? "selected" : ""?>">
                                    <div class="col-md-3">
                                        <img src="<?= base_url().'assets/images/no-profile-img.gif'?>" alt="" class="img-circle img-responsive">
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#"><?php echo $message->name?></a> 
                                        <p><?php echo substr($message->messages[0]->message, 0, 25);?>...</p>   
                                    </div>
                                    <div class="col-md-3">
                                        <p><?=date("g:i A", strtotime($message->date_created));?></p>
                                    </div>
                                </li>
                            <?php $i++; endforeach; ?>
                        </ul>
                    <?php else: ?>
                        <p>No records found!</p>
                    <?php endif; ?>
                    <!-- <li>
                        <div class="col-md-3">
                            <img src="<?= base_url().'assets/images/dashboard/customer2.jpg'?>" alt="" class="img-circle img-responsive">
                        </div>
                        <div class="col-md-6">
                            <a href="">Felico Nostradamo</a> 
                            <p>I would like to inquire about...</p>   
                        </div>
                        <div class="col-md-3">
                            <p>2:18 PM</p>
                    </li>
                    <li>
                        <div class="col-md-3">
                            <img src="<?= base_url().'assets/images/dashboard/pia.jpg'?>" alt="" class="img-circle img-responsive">
                        </div>
                        <div class="col-md-6">
                            <a href="">Pia Wurtzbach</a> 
                            <p>I would like to inquire about...</p>   
                        </div>
                        <div class="col-md-3">
                            <p>2:18 PM</p>
                    </li> -->
            </div>
            <div class="col-md-6 messaging-area">
                <div class="mess-details">
                </div> 
                <!-- <h4>Cam Brady</h4>
                <p>cambrady@gmail.com</p>
                <hr>
                <div class="client-reply">
                    <div class="col-md-9">
                        <p class="message-content">
                            I would like to inquire about the property.
                            How can we meet? 
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam molestie, magna sit amet convallis auctor, magna mi pretium tortor, in dapibus neque elit vel eros.  
                        </p>    
                    </div>
                    <div class="col-md-3">
                        <span>Feb 3 - 2:18 PM</span>    
                    </div>    
                </div>
                <div class="agent-reply">
                    <div class="col-md-3">
                        <span>Feb 3 - 2:18 PM</span>    
                    </div>
                    <div class="col-md-9">
                        <p class="message-content">      
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam molestie, magna sit amet convallis auctor, magna mi pretium tortor, in dapibus neque elit vel eros.  
                        </p>    
                    </div>
                </div>
                <div class="textbox-area">
                    <textarea name="" id="" cols="10" rows="5" class="form-control message-textarea" placeholder="click here to reply"></textarea>
                </div>
                <p>press enter to send</p> -->
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>