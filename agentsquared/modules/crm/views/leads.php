 <?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>

  <!-- Full Width Column -->
  <div class="content-wrapper lead-list">

      <div class="page-title">
        <div class="col-md-7 col-sm-6">
          <h3 class="heading-title">
            <span>Leads</span>
          </h3>
        </div>
        <div class="col-md-5 col-sm-6 col-md-3 pull-right">
            <div class="otherpage video-box video-collapse">
                <div class="row">
                  <div class="col-md-5 col-sm-5 col-xs-5">
                    <h1>Need Help?</h1>
                    <p>See how by watching this short video.</p>
                    <div class="checkbox">
                        <input type="checkbox" id="hide" name="hide" value="hide">
                        <label for="hide">Minimize this tip.</label>
                    </div>
                  </div>
                  <div class="col-md-1">
                    <div class="text-center">
                      <span class="fa fa-long-arrow-right"></span>
                    </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-7">
                   <div class="tour-thumbnail tour-video">
                       <img src="<?php echo base_url()."assets/images/dashboard/v_leads.png"; ?>" alt="Google Analytics" title="Google Analytics Guide" style="max-width: 100%;"/>
                     </div>
                  </div>
                </div>
            </div>
        </div>
      </div>

      <!-- Main content -->
      <section class="content">
          <?php $this->load->view('session/launch-view'); ?>
            <div class="capture-leads-header">
              <div class="item-container">
                  <div class="item item-1">
                    <h4>Capture Leads: 
                      <a href="javascript://" data-toggle="tooltip" title="Capture Leads Must be toggled ON to enable lead capture business rules, which allow you force users to register to use site."><i class="fa fa-question-circle" aria-hidden="true"></i></a> 
                      <input type="checkbox" name="my-checkbox" data-url="<?php echo base_url(); ?>crm/leads/set_lead_capture">
                    </h4>
                  </div>
                  <div class="item item-2">
                    <button type="button" class="btn btn-info" id="lead_config_btn" data-toggle="modal" data-target="#lead_config_modal"><span class="glyphicon glyphicon-cog"></span> Lead Capture Settings </button> <span><a href="javascript://" data-toggle="tooltip" title="Capture Leads Must be toggled ON to enable lead capture business rules, which allow you force users to register to use site."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                  </div>
                  <div class="item item-3">
                    <h4>Email Marketing: <a href="javascript://" data-toggle="tooltip" title="Send new listing update to customers based on their saved search"><i class="fa fa-question-circle" aria-hidden="true"></i></a> <input type="checkbox" name="email_marketing" data-url="<?php echo base_url(); ?>crm/leads/set_email_market"></h4>
                  </div>
                  <div class="item item-3" style="min-width:250px;">
                    <?php
                      if($this->config->item('disallowUser')) { ?>
                        <a href="<?=base_url().'crm/leads?modal_premium=true'?>" class="btn btn-success">Export Contacts</a>
                        <a href="<?=base_url().'crm/leads?modal_premium=true'?>" type="button" class="btn btn-blue" id="import_contacts">Import Contacts</a>
                    <?php
                      } else { ?>
                        <a href="<?php echo site_url('crm/leads/export_contacts'); ?>" type="button" class="btn btn-success">Export Contacts</a>
                        <a href="<?php echo site_url('crm/leads/import_contacts'); ?>" type="button" class="btn btn-info" id="import_contacts">Import Contacts</a>
                    <?php
                      }
                    ?>
                  </div>
                  <div class="item item-4">
                    <form class="search-form form-inline" >
                      <div class="form-group">
                        <select id="type"  name="type" class="form-control">
                            <option value="">Type</option>
                            <option value="general_question" <?php echo (isset($_GET["type"]) && $_GET["type"] == "general_question" ) ? 'selected="selected"' : "" ?>>General Question</option>
                            <option value="property_question" <?php echo (isset($_GET["type"]) && $_GET["type"] == "property_question" ) ? 'selected="selected"' : "" ?>>Property Question</option>
                            <option value="signup_form" <?php echo (isset($_GET["type"]) && $_GET["type"] == "signup_form" ) ? 'selected="selected"' : "" ?>>Signup Form</option>
                            <option value="buyer" <?php echo (isset($_GET["type"]) && $_GET["type"] == "buyer" ) ? 'selected="selected"' : "" ?> >Buyer</option>
                            <option value="seller" <?php echo (isset($_GET["type"]) && $_GET["type"] == "seller" ) ? 'selected="selected"' : "" ?>>Seller</option>
                            <option value="flex" <?php echo (isset($_GET["type"]) && $_GET["type"] == "flex" ) ? 'selected="selected"' : "" ?>>Flex</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <div class="input-group input-group-md" style="width: 250px;">
                          <input type="text" name="keywords" value="<?php echo (isset($_GET["keywords"]) && !empty($_GET["keywords"])) ? $_GET["keywords"] : "" ?>" class="form-control pull-right" placeholder="Search">
                          <div class="input-group-btn">
                            <button type="submit" class="btn btn-default btn-search-submit"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>

            <!-- Modal -->
            <div id="lead_config_modal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title text-center">Configure Capture Leads</h3>
                  </div>
                  <div class="modal-body">
                    <form action="leads/leads_capture_configuration" method="POST" id="lead_capture_config_form">
                      <h4> Show Registration Form: </h4><br>
                      <div id="ret_msg"></div>
                      <div class="form-group">
                        <label><input type="checkbox" name="by_view_site_chk" <?=(isset($leads_config->viewSite) && $leads_config->viewSite==1) ? "checked" : ""; ?>> to view whole website</label>
                        <span><a href="javascript://" data-toggle="tooltip" title="Pop up signup form when someone lands on your website."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" name="by_view_search_chk" <?=(isset($leads_config->searchSubmit) && $leads_config->searchSubmit==1) ? "checked" : ""; ?>> before view search results (quick search only) </label>
                        <span><a href="javascript://" data-toggle="tooltip" title="Pop up signup form before displaying the search results."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" name="by_view_lstng_chk" <?=(isset($leads_config->viewLIsting) && $leads_config->viewLIsting==1) ? "checked" : ""; ?>> to view listings details</label>
                        <span><a href="javascript://" data-toggle="tooltip" title="Pop up signup form before customer can view the property details."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" name="by_min_pass_chk" <?=(isset($leads_config->minutesPass) && $leads_config->minutesPass==1) ? "checked" : ""; ?>> after <input type="text" name="minute_capture" id="minute_capture" value="<?=(isset($leads_config->minute_capture)) ? $leads_config->minute_capture : '';?>" style="width: 50px; height: 25px; padding: 5px;"> minutes have passed</label>
                        <span><a href="javascript://" data-toggle="tooltip" title="Set minutes before the signup form will pop up."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" name="by_search_perf_chk" <?=(isset($leads_config->countSearchPerf) && $leads_config->countSearchPerf==1) ? "checked" : ""; ?>> after <input type="text" name="search_count" id="search_count" value="<?=(isset($leads_config->search_count)) ? $leads_config->search_count : '';?>" style="width: 50px; height: 25px; padding: 5px;"> searches have been performed</label>
                        <span><a href="javascript://" data-toggle="tooltip" title="Set search limit before the signup form will pop up."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" name="by_listing_view_count_chk" <?=(isset($leads_config->countListingView) && $leads_config->countListingView==1) ? "checked" : ""; ?>> after <input type="text" name="listing_view_count" id="listing_view_count" value="<?=(isset($leads_config->listing_view_count)) ? $leads_config->listing_view_count : '';?>"  style="width: 50px; height: 25px; padding: 5px;"> listings have been viewed</label>
                        <span><a href="javascript://" data-toggle="tooltip" title="Set limit in viewing the properties before the signup form will pop up."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" name="allow_skip_chk" <?=(isset($leads_config->allowSkip) && $leads_config->allowSkip==1) ? "checked" : ""; ?>> Allow visitors to skip the above registration options</label>
                        <span><a href="javascript://" data-toggle="tooltip" title="Allow customer to skip the signup process."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                      <?php
                        if($this->config->item('disallowUser')) { ?>
                          <a href="<?=base_url().'crm/leads?modal_premium=true'?>" class="btn btn-primary">Save</a>
                      <?php
                        } else { ?>
                          <button type="submit" class="btn btn-primary">Save</button>
                      <?php 
                        }
                      ?>
                      </form>
                  </div>
                </div>

              </div>
            </div>
          <div class="row mb-30px">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <?php if(empty($count_contacts) && ($has_flex_contacts)) { ?>
                
                <h4 id="saved_search_tagline">You haven't saved your contacts yet from flex. Please the click the button below.</h4>
                <button type="button" class="btn btn-default btn-submit" id="sync_contacts">Sync My Contacts</button>
                
              <?php }else{ ?>
                <button type="button" class="btn btn-default btn-submit" id="sync_contacts">Sync to update my contacts</button>
              <?php } ?>
            </div>
          </div>
          <div class="clearfix"></div>

          <?php $session = $this->session->flashdata('session');
            if( isset($session) )
            {
          ?>
            <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
          <?php } ?>

          <div class="table-responsive">
            <table class="table table-striped tablesorter" id="myTable" >
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Type</th>
                  <th>Mobile</th>
                  <th>Phone</th>
                  <th>Last Activity</th>
                  <th>Schedule Showings</th>
                  <th>Messages</th>
                  <th>Favorites</th>
                  <th>Saved Searches</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if( !empty($contacts) ) :
                  foreach ($contacts as $contact) :
                ?>
                    <tr id="lead_tr_<?php echo $contact->contact_id; ?>">
                      <td>
                      <?php
                        if($this->config->item("disallowUser")) { ?>
                          <a href="?modal_premium=true">
                      <?php
                        } else { ?>
                        <a href="<?php echo site_url("crm/leads/profile?contact_id=".$contact->contact_id); ?>">
                      <?php 
                        } 
                        echo (isset($contact->first_name) && !empty($contact->first_name)) ? str_replace("0", "", $contact->first_name." ".$contact->last_name) : ""; ?> 
                        </a>
                      </td>
                      <td><?php echo ( isset($contact->email) && !empty($contact->email) ) ? $contact->email : "";?></td>
                      <td><?php echo ( isset($contact->type) && !empty($contact->type) ) ? $contact->type : "";?></td>
                      <td><?php echo ( isset($contact->mobile) && !empty($contact->mobile) ) ? $contact->mobile : "";?></td>
                      <td><?php echo ( isset($contact->phone) && !empty($contact->phone) ) ? $contact->phone : "";?></td>
                      <td><?php echo $contact->modified; ?></td>
                      <td><?php echo ( isset($contact->schedule_showings->count) && !empty($contact->schedule_showings->count) ) ? $contact->schedule_showings->count : "0";?></td>
                      <td><?php echo ( isset($contact->messages->count) && !empty($contact->messages->count) ) ? $contact->messages->count : "0";?></td>
                      <td><?php echo ( isset($contact->favourites->count) && !empty($contact->favourites->count) ) ? $contact->favourites->count : "0";?></td>
                      <td><?php echo ( isset($contact->save_search) && !empty($contact->save_search) ) ? $contact->save_search : "0";?></td>
                      <td>
                      <?php
                        if($this->config->item("disallowUser")) { ?>
                          <a href="?modal_premium=true">  
                      <?php
                        } else { ?>
                        <a href="<?php echo site_url('crm/leads/send_email'); ?>" data-contact-id="<?=$contact->contact_id;?>" class="leads_send_email">
                        <?php } ?>
                          <span class="glyphicon glyphicon-envelope" title="Send Email" aria-hidden="true"></span>
                        </a> &nbsp; &nbsp;
                      <?php
                        /*
                        if($this->config->item("disallowUser")) { ?>
                          <a href="?modal_premium=true">  
                      <?php
                        } else { ?>
                        <a href="<?php echo site_url("crm/leads/profile?contact_id=".$contact->contact_id); ?>" >
                      <?php } ?>
                          <span class="glyphicon glyphicon-pencil" title="Edit Profile" aria-hidden="true"></span>
                        </a> &nbsp;

                      <?php*/
                        if($this->config->item("disallowUser")) { ?>
                          <a href="?modal_premium=true">
                      <?php
                        } else { ?>
                        <a href="<?php echo site_url("crm/leads/delete_leads?lead_id=".$contact->contact_id); ?>" class="delete-leads">
                      <?php } ?>
                          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                <?php else : ?>
                  <tr class="center"><td colspan="11"><i>no records found!</i></td></tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>

          <div class="row">
            <div class="col-md-12 search-pagination">
              <div class="pull-right">
                <?php 
                       if($pagination_data->cur_page == "0"){
                          $cur_page = $pagination_total_rows;
                       } else{
                          $cur_page = $pagination_data->cur_page;
                       }

                      $cur_total = $cur_page * $pagination_data->per_page;
                       if ($cur_total > $pagination_total_rows){
                         $cur_total = $pagination_total_rows;
                       }?>

                    <span>
                      Showing <?=$cur_total?>  of <?=$pagination_total_rows?> total <?=($pagination_total_rows == 0 || $pagination_total_rows == 1 ) ? "result" : "results" ?> 
                    </span>
                  <ul class="pagination-list">
                     <?php echo $pagination; ?>
                  </ul>
              </div>
            </div>
          </div>
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>

<div class="modal fade bs-example-modal-lg" id="SendEmailLead" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Send Email</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="POST" id="sbt-send-email" class="form-horizontal sbt-send-email-leads">
          <input type="hidden" name="contact_id">
          <textarea name="message" class="form-control summernote" rows="15"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bs-example-modal-lg" id="importContacts" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <form action="#" method="POST" id="sbtImportContacts" class="form-horizontal sbtImportContacts" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import Contacts</h4>
      </div>
      <div class="modal-body">
        <p>Please follow this format in order to have a successful import of contacts</p>
        <p><a target="_blank" href="https://s3-us-west-2.amazonaws.com/agentsquared-media/uploads/contacts.csv">Download format</a></p>
        
            <input type="file" name="userfile" id="fileToImport" accept=".csv">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-send" >Submit</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <!-- /.content-wrapper -->
<?php $this->load->view('session/footer'); ?>
<script type="text/javascript">

  $(document).ready(function()
    {
      $('#myTable').DataTable( {
        "paging":   false,
        "searching": false,
        "info":     false
      });

      $('.summernote').summernote({
            height: 400,
            // placeholder: 'Write content here...'
      });

      $('body').on('click', '#btn-send', function(e) {
        e.preventDefault();
        var files = $('#fileToImport').get(0).files;
        if (files.length === 0) {
            alert('Please upload file.');
        }else if(files[0].name.split('.').pop() != 'csv') {
            alert('Invalid file type it must be text/csv.');
        } else {
          $('#sbtImportContacts').submit();
        }
        return false;
      });

    }
  );
</script>
