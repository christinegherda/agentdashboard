<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> CRM</span>
    </h3>
    <p>See what's going on your site</p>
  </div>
	<section class="content">
        <div class="row">
            <div class="crm-content">
                <div class="col-md-8">
                    <div class="crm-panel">
                        <h4>2016 Properties Sold per Month</h4>
                        <hr>
                        <div id="salesummary" style=""></div>    
                    </div>                            
                </div>
                <div class="col-md-4">
                    <div class="crm-panel">
                        <h4>Messages <span class="badge">4</span></h4>
                        <hr>
                        <ul>
                            <li>
                                <small>February 28, 2016</small>
                                <p><a href="">247 Privet Drive</a></p>
                                <p>From: Ray Michaels, raymichaels@gmail.com</p>
                                <p>How do we see this property? We would like to view this...</p>
                            </li>
                            <li>
                                <small>February 28, 2016</small>
                                <p><a href="">247 Privet Drive</a></p>
                                <p>From: Ray Michaels, raymichaels@gmail.com</p>
                                <p>Is this property near Palo Alto? When can we view this...</p>
                            </li>
                            <li></li>
                        </ul>
                        <h4><a href="">view all</a></h4>
                    </div>
                </div>
                <p class="clearfix"></p>
                <div class="col-md-4">
                    <div class="crm-panel">
                        <h4>Recent Activity</h4>
                        <hr>
                        <ul>
                            <li>
                                <small>January 16, 2016</small> 
                                <p>created a blog <a href="">How to choose a property?</a></p>
                            </li>
                            <li>
                                <small>January 21, 2016</small>
                                <p>published a page <a href="">The Property</a></p>
                            </li>
                            <li>
                                <small>January 21, 2016</small>
                                <p>added <a href="">5</a> listings to featured property</p>
                            </li>
                        </ul>
                        <h4><a href="">view all</a></h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="crm-panel">
                        <h4>Schedules <span class="badge">10</span></h4>
                        <hr>
                        <ul>
                            <li>
                                <small>March 1, 2016 / 11:30AM</small> 
                                <p><a href="">247 Privet Drive</a>, christian@gmail.com</p>
                            </li>
                            <li>
                                <small>March 7, 2016 / 8:00AM</small> 
                                <p><a href="">Villa del Villo</a>, cambrady@yahoo.com</p>
                            </li>
                            <li>
                                <small>March 9, 2016 / 10:30AM</small> 
                                <p><a href="">Oakland Neighborhood</a>, <a href="mailto:willferrell@gmail.com">willferrell@gmail.com</a></p>
                            </li>
                        </ul>
                        <h4><a href="">view all</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>