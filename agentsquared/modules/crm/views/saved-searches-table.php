<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Saved Searches</span>
    </h3>
  </div>
    <section class="content">
        <div class="row">
            <div class="reports-content">
                <div class="col-md-12">
                    
                        <div class="col-md-7 col-xs-7 pull-left">                         
                             <form class="form-inline" action="" method="get" >                                  
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail3"></label>
                                    <input type="input" name="start_date" value="<?php echo (isset($_GET["start_date"])) ? $_GET["start_date"] : ""; ?>" class="form-control" id="startdatepicker" placeholder="Start Date">
                                  </div>
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword3"></label>
                                    <input type="input" name="end_date" value="<?php echo (isset($_GET["end_date"])) ? $_GET["end_date"] : ""; ?>" class="form-control" id="enddatepicker" placeholder="End Date">
                                  </div>                                 
                                  <button type="submit" class="btn btn-success">Search</button>
                            </form>
                        </div>
                        <div class="col-md-4 col-xs-4 pull-right">                         
                             <form class="form-inline" action="" method="get" >
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail3"></label>
                                    <input type="input" name="keywords"  value="<?php echo (isset($_GET["keywords"])) ? $_GET["keywords"] : ""; ?>" class="form-control" id="keywords" placeholder="keywords">
                                  </div>
                                                                
                                  <button type="submit" class="btn btn-success">Search</button>
                            </form>
                        </div>
                        
                    
                    <div class="row" style="margin: 80px 0px 0px 10px" >
                        <p>Total Saved: <span class="badge badge-info"><?php echo (!empty($total)) ? $total : 0; ?></span></p>
                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>Saved Searches</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($searches_properties) ) : 
                                    foreach ( $searches_properties as $property ) : 

                                    $city = str_replace("&type", "", $property['details'] ) ;
                                    $city = str_replace("&PropertyClass", "", $city ) ;
                                ?>
                                    <tr>
                                        <td><?=($property["first_name"]) ? $property["first_name"].' '.$property["last_name"] : $property["orig_email"];?></td>
                                        <td>
                                            <ul class="lead-item-list">
                                                <li><a href="<?php echo site_url("listings?".$property["url"])?>" target="_blank"><?=$city?></a></li>
                                            </ul>
                                        </td>
                                        <td><?=date("F d, Y", strtotime($property["date_created"]));?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="center"><td colspan="8"><i>no records found!</i></td></tr>
                                <?php endif; ?>
                                
                            </tbody>
                        </table>
                    </div>
                   <div class="col-md-12">
                        <?php if( $pagination ) : ?>
                            <div class="pagination-area pull-right">
                                <nav>
                                    <ul class="pagination">
                                        <?php echo $pagination; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                </div>
                <!-- <div class="col-md-6">
                    <h4>2016 Properties Sold per Month</h4>
                    <hr>
                    <div id="salesummary" style=""></div>
                </div> -->
               
                
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>