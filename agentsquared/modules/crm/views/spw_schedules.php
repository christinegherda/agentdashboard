<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Scheduled Showings   </span>
    </h3>
    <p>Enter schedules for meetings with your client</p>
  </div>
    <section class="content">
        <?php $this->load->view('session/launch-view'); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="meeting-list">
                   <div class="row">
                        <div class="col-md-10 col-sm-7 col-xs-12 pull-left date-search">                         
                             <form class="form-inline" method="get">
								<div class="form-group">
									<label for="filter-date" aria-hidden="true" style="display: none;">Filter Date</label>
									<select name="filter-date" class="form-control" id="filter-date" title="Filter Date">
										<option value="scheduled"<?= isset($_GET['filter-date']) && 'scheduled' === $_GET['filter-date'] ? ' selected' : ''?>>date schedule</option>
										<option value="created"<?= isset($_GET['filter-date']) && 'created' === $_GET['filter-date'] ? ' selected' : ''?>>date created</option>
									</select>
								</div>
								<div class="form-group">
									<input type="text"
									       name="start_date"
									       value="<?php echo isset($_GET["start_date"]) ? $_GET["start_date"] : ''; ?>"
									       class="form-control"
									       id="startdatepicker"
									       autocomplete="off"
									       placeholder="Start Date">
								</div>
								<div class="form-group">
									<input type="text"
									       name="end_date"
									       value="<?php echo isset($_GET["end_date"]) ? $_GET["end_date"] : ''; ?>"
									       class="form-control"
									       id="enddatepicker"
									       autocomplete="off"
									       placeholder="End Date">
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success">Search</button>
								</div>
                            </form>
                        </div>
                        <div class="col-md-2  col-sm-5 col-xs-12">                         
                             <form class="text-right form-inline" action="" method="get" >
                                  <div class="form-group">
                                    <div class="input-group">
                                        <input type="input" name="keywords"  value="<?php echo (isset($_GET["keywords"])) ? $_GET["keywords"] : ""; ?>" class="form-control" id="keywords" placeholder="keywords" pattern='[a-zA-Z0-9-_.,@()\s]+' maxlength="50">
                                      <div class="input-group-btn">
                                        <button type="submit" class="btn btn-success">Search</button>
                                      </div>
                                    </div> 
                                  </div>                       
                            </form>
                        </div>
                    </div>
                    <p class="clearfix"></p>
                    <p class="clearfix"></p>
                    <div class="table-responsive">
                        <table class=" table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Number</th>
                                    <th>Message</th>
                                    <th>Property</th>
                                    <th>Type</th>
                                    <th>Date Schedule</th>
                                    <th>Date Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($spw_schedules) ) : 
                                    foreach( $spw_schedules as $schedule) : 
                                ?>
                                        <tr>
                                            <td> 
                                            <?php if ($this->config->item("disallowUser")): ?>
                                                <a href="?modal_premium=true"><?php echo (isset($schedule->customer_name_agentsite) AND !empty($schedule->customer_name_agentsite)) ? $schedule->customer_name_agentsite: $schedule->customer_name; ?></a>
                                            <?php else: ?>
                                                <a href="<?php echo site_url("crm/leads/profile?contact_id=".$schedule->contact_id); ?>">
                                                    <?php echo (isset($schedule->customer_name_agentsite) AND !empty($schedule->customer_name_agentsite)) ? $schedule->customer_name_agentsite: $schedule->customer_name; ?>
                                                </a>
                                            <?php endif ?>
                                            <?php //echo (isset($schedule->customer_name_agentsite) AND !empty($schedule->customer_name_agentsite)) ? $schedule->customer_name_agentsite: $schedule->customer_name; ?>
                                            </td>
                                            <td><?php echo (isset($schedule->customer_email_agentsite)) ? $schedule->customer_email_agentsite : $schedule->customer_email; ?></td>
                                            <td><?php echo (isset($schedule->customer_number_agentsite)) ? $schedule->customer_number_agentsite : $schedule->customer_number; ?></td>
                                            <td><?php echo $schedule->message;?></td>
                                            <td>
                                            <?php
                                                if($this->config->item('disallowUser')) { ?>
                                                    <a href="<?=base_url().'crm/spw_schedules?modal_premium=true'?>"><?php echo $schedule->property_name;?></a>
                                            <?php
                                                } else { ?>
                                                    <a href="<?=$base_url.'other-property-details/'.$schedule->property_id?>" target="_blank"><?php echo $schedule->property_name;?></a>
                                            <?php
                                                }
                                            ?>
                                            </td>
                                            <td><?php echo $schedule->type;?></td>
                                            <td><?php echo date("F d, Y", strtotime($schedule->date_scheduled))?></td>
                                            <td><?php echo date("F d, Y H:m:s",strtotime($schedule->created))?></td>
                                            <td>
                                                <a href="<?php echo $this->config->item("disallowUser") ? '?modal_premium=true' : site_url("crm/spw_schedules/delete_schedules?cssid=".$schedule->cssid); ?>" class="delete-spw_schedule">
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else : ?>
                                    <tr align="center"><td colspan="10"><i>no records found!</i></td></tr>
                                <?php endif;?>
                                
                            </tbody>
                        </table>
                        
                    </div>

                    <div class="col-md-12">
                        <?php if( $pagination ) : ?>
                            <div class="pagination-area pull-right">
                                <nav>
                                    <ul class="pagination">
                                        <?php echo $pagination; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>