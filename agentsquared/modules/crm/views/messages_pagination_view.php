            <div class="mb-30px" id="profile_messages">
              <!-- Request Information -->
              <h3>Request Information / Messages</h3>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Message</th>
                      <th>Property</th>
                      <th>Phone</th>
                      <th>Sent By</th>
                      <th>Type</th>
                      <th>Date</th> 

                    </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($profile_messages->messages)) : ?>
                      <?php foreach($profile_messages->messages as $message) : ?>
                        <?php if($message->type != "email") : ?>
                          <tr>
                            <td>
                              <a href="javascript:void(0)" data-message-id="<?php echo $message->id; ?>" class="email-view" title="view all">  <i class=" fa fa-search"></i> View</a></td>

                            <?php if(isset($message->property_id) && !empty($message->property_id)){?>
                                <td><a href="<?php echo $base_url."other-property-details/".$message->property_id;?>"> <?= $message->property_address?></a> </td>
                            <?php } else {?>
                                <td class="text-center">General Inquiry Form</td>
                             <?php } ?>

                            <td><?= isset($profile_messages->phone) ? $profile_messages->phone : ""?></td>
                            <td><?= (($message->type == 1) ? $message->agent_name : (!empty($message->contact_name) ? $message->contact_name : $profile_messages->first_name." ".$profile_messages->last_name)); ?></td>
                            <td><?= isset($message->type) ? $message->type : ""?></td>
                            <td><?= date( "Y-m-d" ,strtotime($message->created));?></td>
                          </tr>
                         <?php endif; ?>
                      <?php endforeach; ?>

                     

                    <?php else : ?>
                      <tr class="center"> <td colspan="8"> <i> No records found!</i> </td> </tr>
                    <?php endif; ?>
                    
                  </tbody>

                </table>

                 <div class="col-md-12">
                        <?php if( isset($messages_pagination) ) : ?>
                            <div class="pagination-area pull-right">
                                <nav>
                                    <ul class="pagination">
                                        <?php echo $messages_pagination; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                
              </div>
              <div class="">
                  <button type="submit" class="btn btn-green pull-right" data-toggle="modal" data-target="#SendEmail" >Send Email</button>
                </div>
            </div>

            