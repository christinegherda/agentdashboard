<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>
  <!-- Full Width Column -->
  <div class="content-wrapper profile-content">
<!--     <div class="page-title">
      <h3 class="heading-title">
        <span> Messages</span>
      </h3>
      <p>You've got mail!</p>
    </div> -->

      <h3 class="heading-title">
        <span>Contact Details</span>
      </h3>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <?php $session = $this->session->flashdata('session');
              if( isset($session) )
              {
            ?>
              <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
            <?php } ?>
          </div>

          <div class="col-md-6">
            <div class="col-md-12">
              <h1 class="col-md-12 text-center pr-30 profile-name"><?php echo $profile->first_name." ".$profile->last_name ?></h1>
            </div>
            <form action="<?=base_url().'crm/leads/update_profile'?>" method="POST" id="sbt-update-profile" class="form-horizontal">
                <input type="hidden" name="contact_id" value="<?=$contact_id?>">
                <div class="form-group">
                  <label for="" class="col-lg-2 col-md-3 text-right">Email Address: </label>
                  <div class="col-lg-6 col-md-5">
                    <input readonly type="email" name="profile[email]"  class="form-control" value="<?=(isset($profile->email)) ? $profile->email : ''; ?>" id="email" placeholder="Email" required>
                  </div>
                  <div class="col-lg-3 col-md-4 invisible-xs">
                     <!-- <button type="submit" class="btn btn-green">Update Profile</button> -->
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-lg-2 col-md-3 text-right">Phone: </label>
                  <div class="col-md-4">
                    <input readonly type="text" name="profile[phone]" class="form-control phone_number" value="<?=(isset($profile->phone)) ? $profile->phone : ''; ?>" id="phone" placeholder="Phone" required>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label for="" class="col-lg-2 col-md-3 text-right">Mobile: </label>
                  <div class="col-md-4">
                    <input type="text" name="profile[mobile]" class="form-control phone_number" value="<?=(isset($profile->mobile)) ? $profile->mobile : ''; ?>" id="mobile" placeholder="Mobile" required>
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="" class="col-lg-2 col-md-3 text-right">Address: </label>
                  <div class="col-md-7">
                    <input type="text" name="profile[address]" class="form-control" value="<?=(isset($profile->address)) ? $profile->address : ''; ?>" id="address" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-lg-2 col-md-3 text-right">City: </label>
                  <div class="col-md-3">
                    <input type="text" name="profile[city]" class="form-control" value="<?=(isset($profile->city)) ? $profile->city : ''; ?>" id="City" disabled>
                  </div>
                  <label for="" class="col-md-1 text-right">State: </label>
                  <div class="col-md-3">
                    <input type="text" name="profile[state]" class="form-control" value="<?=(isset($profile->state)) ? $profile->state : ''; ?>" id="State" disabled>
                  </div>
                  <label for="" class="col-md-1 text-right">Zip Code: </label>
                  <div class="col-md-1">
                    <input type="text" name="profile[zipcode]" class="form-control" value="<?=(isset($profile->zipcode)) ? $profile->zipcode : ''; ?>" id="zipcode" disabled>
                  </div>
                </div>
                <div class="form-group visible-xs visible-sm">
                  <div class="col-md-3">
                     <!-- <button type="submit" class="btn btn-green pull-right">Update Profile</button> -->
                  </div>
                </div>

            </form>

            <div class="mb-30px" id="profile_messages">
              <!-- Request Information -->
              <h3>Request Information / Messages</h3>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Message</th>
                      <th>Property</th>
                      <th>Phone</th>
                      <th>Sent By</th>
                      <th>Type</th>
                      <th>Date</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($profile_messages->messages)) : ?>
                      <?php foreach($profile_messages->messages as $message) : ?>
                        <?php if($message->type != "email") : ?>
                          <tr>
                            <td>
                              <a href="javascript:void(0)" data-message-id="<?php echo $message->id; ?>" class="email-view" title="view all">  <i class=" fa fa-search"></i> View</a></td>

                            <?php if(isset($message->property_id) && !empty($message->property_id)){?>
                                <td><a href="<?php echo $base_url."other-property-details/".$message->property_id;?>" target="_blank"> <?= $message->property_address?></a> </td>
                            <?php } else {?>
                                <td class="text-center">General Inquiry Form</td>
                             <?php } ?>

                            <td><?= isset($profile_messages->phone) ? $profile_messages->phone : ""?></td>
                            <td><?= (($message->type == 1) ? $message->agent_name : (!empty($message->contact_name) ? $message->contact_name : $profile_messages->first_name." ".$profile_messages->last_name)); ?></td>
                            <td><?= isset($message->type) ? $message->type : ""?></td>
                            <td><?= date( "Y-m-d" ,strtotime($message->created));?></td>
                          </tr>
                         <?php endif; ?>
                      <?php endforeach; ?>



                    <?php else : ?>
                      <tr class="center"> <td colspan="8"> <i> No records found!</i> </td> </tr>
                    <?php endif; ?>

                  </tbody>
                </table>

                <div class="col-md-12">
                        <?php if( isset($profile_messages->messages_pagination) ) : ?>
                            <div class="pagination-area pull-right">
                                <nav>
                                    <ul class="pagination">
                                        <?php echo $profile_messages->messages_pagination; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif; ?>

                    </div>

              </div>
              <div class="">
                  <a href="<?php echo site_url('crm/leads/send_email'); ?>" data-contact-id="<?=$contact_id;?>" class="leads_send_email btn btn-green pull-right">Send Email</a>
              </div>
            </div>


            <!-- Activities -->
            <div class="activities-list">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#activities" aria-controls="activities" role="tab" data-toggle="tab">All Activities</a></li>
                <li role="presentation"><a href="#email-listing" aria-controls="email-listing" role="tab" data-toggle="tab">Email</a></li>
                <li role="presentation"><a href="#phonenumber" aria-controls="phonenumber" role="tab" data-toggle="tab">Phone</a></li>
                <li role="presentation"><a href="#meetings" aria-controls="meetings" role="tab" data-toggle="tab">Meetings</a></li>
                <li role="presentation"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">Notes</a></li>
                <li role="presentation"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab">Tasks</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">

                <!-- All activities -->
                <div role="tabpanel" class="tab-pane active" id="activities">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="25%">Action</th>
                          <th width="50%">Details</th>
                          <th width="25%">Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($activities) ) : ?>
                            <?php foreach ($activities as $activity) :

                              if( $activity->activity_type == "send email"){
                                $label = '<span class="label-alert label-green"><i class="fa fa-envelope"></i> Emailed</span>';
                              }
                              else if( $activity->activity_type == "phone" )
                              {
                                $label = '<span class="label-alert label-yellow"><i class="fa fa-phone"></i> Left Voicemail</span>';
                              }
                              else if( $activity->activity_type == "meetings" )
                              {
                                $label = '<span class="label-alert label-yellow"><i class="fa fa-phone"></i> Left Voicemail</span>';
                              }
                              else
                              {
                                $label = '<span class="label-alert label-orange"><i class="fa fa-users"></i> '.ucwords($activity->activity_type).'</span>';
                              }

                            ?>
                              <tr>
                                <td width="25%">
                                  <p class="activity-name"><?php echo ($activity->user_type == "agent") ? "You" : "Customer"?></p>
                                  <?php echo $label; ?>
                                  <p class="lead-label">Lead:</p>
                                  <p class="activity-name"><?php echo $profile->first_name." ".$profile->last_name ?></p>
                                </td>
                                <td width="50%">
                                  <?php echo $activity->activity; ?>
                                </td>
                                <td width="25%" class="date-column">
                                  <p><?php echo date("l", strtotime($activity->created)); ?></p>
                                  <p><?php echo date("F d, Y", strtotime($activity->created)); ?> </p>
                                  <p><?php echo date("H i a", strtotime($activity->created)); ?></p>
                                </td>
                              </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>

                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Email -->
                <div role="tabpanel" class="tab-pane" id="email-listing">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="25%">Action</th>
                          <th width="50%">Details</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                         <?php if( !empty($activities) ) : ?>
                            <?php foreach ($activities as $activity) :

                              if( $activity->activity_type == "send email"){
                                $label = '<span class="label-alert label-green"><i class="fa fa-envelope"></i> Emailed</span>';
                            ?>
                              <tr>
                                <td width="20%">
                                  <p class="activity-name"><?php echo ($activity->user_type == "agent") ? "You" : "Customer"?></p>
                                  <?php echo $label; ?>
                                  <p class="lead-label">Lead:</p>
                                  <p class="activity-name"><?php echo $profile->first_name." ".$profile->last_name ?></p>
                                </td>
                                <td width="50%">
                                  <?php echo $activity->activity; ?>
                                </td>
                                <td class="date-column">
                                  <p><?php echo date("l", strtotime($activity->created)); ?></p>
                                  <p><?php echo date("F d, Y", strtotime($activity->created)); ?> </p>
                                  <p><?php echo date("H i a", strtotime($activity->created)); ?></p>
                                </td>
                              </tr>
                              <?php } ?>
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>

                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Phone -->
                <div role="tabpanel" class="tab-pane" id="phonenumber">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="25%">Action</th>
                          <th width="50%">Details</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($activities) ) : ?>
                            <?php foreach ($activities as $activity) :

                              if( $activity->activity_type == "phone"){
                                $label = '<span class="label-alert label-yellow"><i class="fa fa-phone"></i> Left Voicemail</span>';
                            ?>
                              <tr>
                                <td width="20%">
                                  <p class="activity-name"><?php echo ($activity->user_type == "agent") ? "You" : "Customer"?></p>
                                  <?php echo $label; ?>
                                  <p class="lead-label">Lead:</p>
                                  <p class="activity-name"><?php echo $profile->first_name." ".$profile->last_name ?></p>
                                </td>
                                <td width="50%">
                                  <?php echo $activity->activity; ?>
                                </td>
                                <td class="date-column">
                                  <p><?php echo date("l", strtotime($activity->created)); ?></p>
                                  <p><?php echo date("F d, Y", strtotime($activity->created)); ?> </p>
                                  <p><?php echo date("H i a", strtotime($activity->created)); ?></p>
                                </td>
                              </tr>
                              <?php } ?>
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>

                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Meetings -->
                <div role="tabpanel" class="tab-pane" id="meetings">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="25%">Action</th>
                          <th width="50%">Details</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($activities) ) : ?>
                            <?php foreach ($activities as $activity) :

                              if( $activity->activity_type == "phone"){
                                $label = '<span class="label-alert label-orange"><i class="fa fa-users"></i> Meetings</span>';
                            ?>
                              <tr>
                                <td width="20%">
                                  <p class="activity-name"><?php echo ($activity->user_type == "agent") ? "You" : "Customer"?></p>
                                  <?php echo $label; ?>
                                  <p class="lead-label">Lead:</p>
                                  <p class="activity-name"><?php echo $profile->first_name." ".$profile->last_name ?></p>
                                </td>
                                <td width="50%">
                                  <?php echo $activity->activity; ?>
                                </td>
                                <td class="date-column">
                                  <p><?php echo date("l", strtotime($activity->created)); ?></p>
                                  <p><?php echo date("F d, Y", strtotime($activity->created)); ?> </p>
                                  <p><?php echo date("H i a", strtotime($activity->created)); ?></p>
                                </td>
                              </tr>
                              <?php } ?>
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Notes -->
                <div role="tabpanel" class="tab-pane" id="notes">
                  <div class="table-responsive">

                    <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#addNotes"  style="margin:5px" > Notes <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>

                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="50%">Notes</th>
                          <th>Created By</th>
                          <th>Date Created</th>
                          <th width="10%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($profile->notes) ) : ?>
                          <?php foreach( $profile->notes as $note ) : ?>
                              <tr id="notesRow<?=$note->id;?>" >
                                <td width="50%">
                                  <?php echo $note->notes; ?>
                                </td>
                                <td>
                                  <?php echo $note->created_by; ?>
                                </td>
                                <td >
                                  <p><?php echo date("Y-m-d H:i:s", strtotime($note->date_created)); ?></p>
                                </td>
                                <td width="10%">
                                  <a href="<?php echo site_url('crm/leads/delete_notes'); ?>" data-id="<?=$note->id;?>" data-contact-id="<?=$contact_id?>" class="delete_notes"><span class="glyphicon glyphicon-remove" title="Delete Notes" aria-hidden="true"></span></a>
                                </td>
                              </tr>

                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Tasks -->
                <div role="tabpanel" class="tab-pane" id="tasks">
                  <div class="table-responsive">
                    <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#addTasks"  style="margin:5px" > Tasks <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="50%">Tasks</th>
                          <th >Created By</th>
                          <th>Date Created</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($profile->tasks) ) : ?>
                          <?php foreach( $profile->tasks as $task ) : ?>
                              <tr id="taskRow<?=$task->id;?>" >
                                <td width="50%">
                                  <?php echo $task->tasks; ?>
                                </td>
                                <td>
                                  <?php echo $task->created_by; ?>
                                </td>
                                <td >
                                  <p><?php echo date("Y-m-d H:i:s", strtotime($task->date_created)); ?></p>
                                </td>
                                <td width="10%">
                                  <a href="<?php echo site_url('crm/leads/delete_tasks'); ?>" data-id="<?=$task->id;?>" data-contact-id="<?=$contact_id?>" class="delete_tasks"><span class="glyphicon glyphicon-remove" title="Delete Tasks" aria-hidden="true"></span></a>
                                </td>
                              </tr>

                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>


              </div>
            </div>
          </div>
          <div class="col-md-6">

            <!-- Custom Tabs -->
            <div class="save-property-listing">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#favorites" aria-controls="favorites" role="tab" data-toggle="tab">Favorites</a></li>
                <li role="presentation"><a href="#searches" aria-controls="searches" role="tab" data-toggle="tab">Saved Searches</a></li>
                <li role="presentation"><a href="#scheduleforshowing" aria-controls="scheduleforshowing" role="tab" data-toggle="tab">Schedule for Showing</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <!-- Favorites -->
                <div role="tabpanel" class="tab-pane active" id="favorites">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="20%">Image</th>
                          <th width="60%">Property Details</th>
                          <th width="20%">Note</th>
                          <!-- <th>Address</th>
                          <th>Details</th> -->
                        </tr>
                      </thead>
                      <tbody>

                        <?php if( !empty($profile->favourites) ) : ?>
                          <?php foreach( $profile->favourites as $favourite ) : ?>
                            <?php if(isset($favourite->properties_details["ListingKey"]) && !empty($favourite->properties_details["ListingKey"])) : ?>
                                <tr>
                                  <td class="table-img">
                                    <a href="<?php echo $base_url."other-property-details/".$favourite->properties_details["ListingKey"];?>" target="_blank">
                                      <img src="<?= isset($favourite->property_photos[0]["StandardFields"]["Photos"][0]["Uri300"]) ? $favourite->property_photos[0]["StandardFields"]["Photos"][0]["Uri300"] : "" ?>" alt="">
                                    </a>
                                  </td>
                                  <td class="property-details">
                                    <p class="property-name"><?= isset($favourite->properties_details["UnparsedAddress"]) ? $favourite->properties_details["UnparsedAddress"] : ""?></p>
                                   <!--  <p class="property-desc"><?= isset($favourite->properties_details["PublicRemarks"]) ? str_limit($favourite->properties_details["PublicRemarks"] ,150) : ""?>...</p>-->
                                    <p class="property-price"> $<?= isset($favourite->properties_details["CurrentPrice"]) ? number_format($favourite->properties_details["CurrentPrice"]) : ""?></p>
                                    <ul class="list-icon">
                                    <?php if(isset($favourite->properties_details["BedsTotal"]) && !empty($favourite->properties_details["BedsTotal"])){
                                          if(($favourite->properties_details["BedsTotal"] != "********")){?>
                                                <li><i class="fa fa-bed"></i> <?=$favourite->properties_details["BedsTotal"]?> Bed</li>
                                        <?php }
                                        }?>
                                        <?php if(isset($favourite->properties_details["BathsTotal"]) && !empty($favourite->properties_details["BathsTotal"])){
                                                if(($favourite->properties_details["BathsTotal"] != "********")){?>
                                                <li><i class="icon-toilet"></i> <?=$favourite->properties_details["BathsTotal"]?> Bath</li>
                                        <?php }
                                        }?>
                                        <?php if(isset($favourite->properties_details["BuildingAreaTotal"]) && !empty($favourite->properties_details["BuildingAreaTotal"])){
                                                if(($favourite->properties_details["BuildingAreaTotal"] != "********")){?>
                                                <li><?=number_format($favourite->properties_details["BuildingAreaTotal"])?> sqft.</li>
                                        <?php }
                                      }?>

                                    </ul>
                                  </td>

                                  <?php
                                      $prop_id = $favourite->properties_details["ListingKey"];
                                      $data = Modules::run('crm/leads/get_customer_notes',$prop_id);
                                  ?>

                                  <?php if(!empty($data) && $data->property_id == $prop_id){?>
                                      <td class="text-center">
                                        <a href="#" data-toggle="modal" data-target="#viewNoteModal<?=$favourite->properties_details["ListingKey"]?>"><i class="fa fa-search"></i> View Note</a>
                                      </td>
                                  <?php } else {?>
                                      <td class="text-center">

                                      </td>

                                  <?php }?>

                                  <!-- View Customer note modal -->
                                  <div class="modal fade" id="viewNoteModal<?=$favourite->properties_details["ListingKey"]?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel"> View Note</h4>
                                        </div>
                                        <div class="modal-body">
                                        <?php
                                            $prop_id = $favourite->properties_details["ListingKey"];
                                            $data = Modules::run('crm/leads/get_customer_notes',$prop_id);
                                        ?>
                                          <?php echo isset($data->notes) ? htmlentities($data->notes) : "" ?>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </tr>
                            <?php endif; ?>
                          <?php endforeach;?>

                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Searches -->
                <div role="tabpanel" class="tab-pane" id="searches">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Link</th>
                          <th>Details</th>
                          <th>Created</th>
                          <th>Email Frequency</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($profile->saveSearches) ) : ?>
                          <?php foreach( $profile->saveSearches as $savedSearches ) : ?>
                            <?php if( !isset($savedSearches->json_decoded) ) { ?>
                              <tr>
                                <td> <a href="<?php echo $base_url."search-results?".$savedSearches->url;?>" target="_blank">Run this search >></a> </td>
                                <td><?=substr($savedSearches->url,0,50);?>...</td>
                                <td><?= date("Y-m-d", strtotime($savedSearches->date_created)); ?></td>
                                <td><?=ucfirst($profile->email_schedule);?></td>
                              </tr>
                            <?php }else{ ?>
                              <tr>
                                <td> <a href="<?php echo $base_url."/home/saved_searches/".$savedSearches->json_decoded->Id;?>" target="_blank">Run this search >></a> </td>
                                <td><?=substr($savedSearches->json_decoded->Name,0,60);?></td>
                                <td><?= date("Y-m-d", strtotime($savedSearches->date_modified)); ?></td>
                                <td><?= ucfirst($profile->email_schedule); ?></td>
                              </tr>
                            <?php } ?>
                          <?php endforeach;?>

                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>

                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Schedule for showing -->
                <div role="tabpanel" class="tab-pane" id="scheduleforshowing">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Property</th>
                          <th>Schedule Date</th>
                          <th>Date Created</th>
                          <th>Action</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          if( !empty($profile->schedule_showing) ) {?>
                          <?php foreach( $profile->schedule_showing as $schedule ) { ?>

                            <tr>
                              <td><?=$profile->first_name.' '.$profile->last_name;?></td>
                              <td><?=$profile->email;?></td>
                              <td><?=$profile->phone;?></td>
                               <td> <a href="<?php echo $base_url."other-property-details/".$schedule->property_id;?>" target="_blank"> <?=$schedule->property_name?> </a>
                              </td>
                              <td><?=date("Y-m-d", strtotime($schedule->date_scheduled))?></td>
                              <td><?=date("Y-m-d", strtotime($schedule->created))?></td>
                              <td class="text-center">
                                <a style="margin-right:8px" href="#" data-toggle="modal" data-target="#customer-sched-modal<?=$schedule->property_id?>" data-title="Tooltip" data-trigger="hover" title="View Message"><i class="fa fa-search"></i></a>
                              </td>
                            </tr>

                             <!-- //Sched Showing note modal -->
                                <div id="customer-sched-modal<?=$schedule->property_id?>" class="modal fade modalNote" role="dialog">
                                  <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title text-center">Schedule Showing Message</h4>
                                      </div>
                                        <div class="modal-body">
                                            <textarea readonly class="form-control" rows="7"><?= $schedule->message?></textarea>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                  </div>
                                </div> <!-- customer-sched-modal -->

                          <?php }?>

                        <?php } else { ?>

                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>

                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <?php if($profile->type=="seller" || $profile->type=="buyer") { ?>
            <div class="col-md-6">
              <h3>Customer Type: <strong><?=ucfirst($profile->type);?></strong></h3>
              <h3 style="text-align:center;">Home Information</h3>
              <div class="table-responsive table-seller-buyer-info">
                <table class="table table-striped" style="width:100%">
                  <thead>
                    <tr>
                    <?php if($profile->type=="seller") { ?>
                      <th width="40%">Address</th>
                      <th width="10%">City</th>
                      <th width="10%">State</th>
                      <th width="10%">Zip Code</th>
                      <th width="10%"># of Bedrooms</th>
                      <th width="10%"># of Bathrooms</th>
                      <th width="20%">Square Ft.</th>
                    <?php } else { ?>
                      <th width="100"># of Bedrooms</th>
                      <th width="100"># of Bathrooms</th>
                      <th width="100">Squaret Ft.</th>
                      <th width="100">Contact You By</th>
                      <th width="100">Price Range</th>
                      <th width="100">When do you want to move?</th>
                      <th width="100">When did you start looking?</th>
                      <th width="100">Where would you like to own?</th>
                      <th width="100">Are you currently with an agent?</th>
                      <th width="300">Describe your dream home</th>
                    <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if(isset($customer) && !empty($customer)) {
                        if($profile->type=="seller") { 
                          foreach($customer as $key) { ?>
                            <tr>
                              <td><?=$key->address;?></td>
                              <td><?=$key->city;?></td>
                              <td><?=$key->state;?></td>
                              <td><?=$key->zip_code;?></td>
                              <td><?=$key->bedrooms;?></td>
                              <td><?=$key->bathrooms;?></td>
                              <td><?=$key->square_feet;?></td>
                            </tr>
                    <?php
                          }
                        } else { 
                          foreach($customer as $key) { ?>
                            <tr>
                              <td><?=$key->bedrooms;?></td>
                              <td><?=$key->bathrooms;?></td>
                              <td><?=$key->square_feet;?></td>
                              <td><?=$key->contact_by;?></td>
                              <td><?=$key->price_range;?></td>
                              <td><?=$key->when_do_you_want_to_move;?></td>
                              <td><?=$key->when_did_you_start_looking;?></td>
                              <td><?=$key->wher_would_you_like_to_own;?></td>
                              <td><?=($key->are_you_currently_with_an_agent) ? 'Yes' : 'No';?></td>
                              <td>
                                <div class="dream-job">
                                  <?=$key->describe_your_dream_home;?>  
                                </div>
                              </td>
                            </tr>
                    <?php
                          }
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          <?php } ?>
        </div>
      </section>
  </div>

<div class="modal fade bs-example-modal-lg" id="SendEmailLead" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Send Email</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="POST" id="sbt-send-email" class="form-horizontal sbt-send-email-leads">
          <input type="hidden" name="contact_id" value="<?=$contact_id?>">
          <textarea name="message" class="form-control summernote" rows="15"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-send" >Send</button>
      </div>

      </form>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bs-example-modal-lg" id="SentEmailView" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Customer Requested Information / Messages</h4>
      </div>
      <div class="modal-body">
        <span id="sent-email-content"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade bs-example-modal-lg" id="addNotes" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Notes</h4>
      </div>
      <div class="modal-body">
        <div id="notes_ret_msg"></div>
        <form action="<?=base_url().'crm/leads/add_notes'?>" method="POST" id="sbt-add-notes" class="form-horizontal">
          <input type="hidden" name="contact_id" value="<?=$contact_id?>">
          <textarea name="notes" class="form-control summernote" rows="15"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-notes" >Submit</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bs-example-modal-lg" id="addTasks" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Tasks</h4>
      </div>
      <div class="modal-body">
        <form action="<?=base_url().'crm/leads/add_tasks'?>" method="POST" id="sbt-add-tasks" class="form-horizontal">
          <input type="hidden" name="contact_id" value="<?=$contact_id;?>">
          <textarea name="tasks" class="form-control summernote" rows="15"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-tasks" >Submit</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <!-- /.content-wrapper -->
<?php $this->load->view('session/footer'); ?>
<script>
    $(document).ready(function(){

      $('.summernote').summernote({
            height: 400,
            placeholder: 'Write content here...'
      });

       tinymce.init({
        selector: '#pageTextarea',
        theme: 'modern',
        content_style: '.mce-content-body {font-size:14px;font-family:Arial,sans-serif;}',
        fontsize_formats: '12px 14px 16px 18px 20px 24px 36px 42px',
        plugins: [
          'autosave autoresize autolink code link image',
          'searchreplace wordcount media lists print',
          'preview table emoticons paste textcolor imagetools'
        ],
        autosave_interval: '10s',
        autoresize_min_height: 350,
        autoresize_max_height: 500,
        menubar: 'file edit format table',
        toolbar_items_size: 'small',
        toolbar: 'undo redo | fontsizeselect fontselect | blockquote bold italic underline forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink image media | emoticons code preview help',

       });
    });
</script>