<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends IDX_Auth {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Crm_model", "crm");	
		$this->load->model("agent_sites/Agent_sites_model");
	}

	public function index() {
		//echo $this->session->userdata("cus");
		$data['title'] = "Reports";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		
		$page = 1;
		
		$account_info = Modules::run('dsl/getAgentAccountInfo');
		$param["mlsID"] = $account_info->Id;		
		$data["property_sold"] = $this->Agent_sites_model->get_your_sold_properties_listings( FALSE, $param );

		// $data["property_sold"] = $this->crm->get_property_sold(FALSE, $param = array() );
		$favorates_properties = $this->crm->get_favorites_property(FALSE, $param = array() );
		$search_properties = $this->crm->get_search_property(FALSE, $param = array() );
		//printA($search_properties); exit;
		if( !empty($favorates_properties) )
		{
			foreach ($favorates_properties as &$properties) {
				$properties["properties_details"] = $this->api->GetListing( $properties["property_id"] );
			}
		}

		$data["favorates_properties"] = $favorates_properties;
		$data["search_properties"] = $search_properties;

		$this->load->view('reports', $data);

	}

}

