<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedules extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Crm_model", "crm");	
		$this->load->model("agent_sites/Agent_sites_model", "domain");
	}

	public function index() {

		$data['title'] = "Schedules";
		$this->load->library('pagination');

		$param = $this->input->get();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 25;

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];

		$total = $this->crm->get_schedule_of_showing(TRUE, $param);
		$data["schedules"] = $this->crm->get_schedule_of_showing( FALSE, $param );
		//printA($data["schedules"]); exit;

		$config["base_url"] = base_url().'crm/schedules';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		$data['css'] = array('bootstrap-datepicker.css', 'bootstrap-datepicker.standalone.css', 'bootstrap-datepicker3.css');

		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		
		$this->load->view('schedules', $data);

	}

}

?>