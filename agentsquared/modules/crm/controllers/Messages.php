<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Crm_model", "crm");		
		$this->load->model("agent_sites/Agent_sites_model");
	}

	public function index() {

		$data['title'] = "Messages";

		$data["messages"] = $this->crm->get_messages_info();

		//printA($data["messages"]); exit;
		$data['js'] = array("message-data-load.js");


		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$this->load->view('messages', $data);

	}

	public function get_message_json()
	{
		$messages = $this->crm->get_messages_info();		
		//printA($messages); exit;
		exit(json_encode($messages));
	}

	public function message_reply()
	{
		if( $this->input->is_ajax_request() )
		{
			$this->load->library("email");

			//printA( $this->input->post() ); exit;
			if( $id = $this->crm->insert_reply() )
			{
				//send email to customer
				$customer_info = $this->crm->get_customer_info( $this->input->post("mid") );
				$agent_info = $this->crm->get_agent_info(  );
				
				$from_email = (isset($agent_info->email)) ? $agent_info->email : "";
				$from_name = (isset($agent_info->site_name)) ? $agent_info->site_name : "Agent Property";

				$subject = "Good Day!";

				$content = $this->input->post("message");

				// $this->email->send_email(

			 // 		$from_email,

			 // 		$from_name,

			 // 		$customer_info->email,

			 // 		$subject,

			 // 		'email/template/default-email-body',

			 // 		array(
			 // 			"message" => $content,
			 // 			"subject" => $subject
			 // 		)
			 // 	);

				$to_append = "";
				$to_append .= '<div class="agent-reply"><div class="col-md-3">';
	            $to_append .= '<span>'.date("Y-m-d H:m:s").'</span> </div>';
	            $to_append .= '<div class="col-md-9"><p class="message-content">  ';    
	            $to_append .= $content;
	            $to_append .=' </p>  </div></div>';

			 	$response = array(
			 		'success' => TRUE,
			 		'content' => $content,
			 		'to_append' => $to_append,
			 		"message" => "Message has been successfully sent!"
			 	);

			 	exit( json_encode($response)); 
			}

			exit( json_encode(array("success" => FALSE , "message" => "Failed to send message!" )) );
		}
	}

}
