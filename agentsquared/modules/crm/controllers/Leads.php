<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');


		$this->load->library('Ajax_pagination');
		$this->load->model("Crm_model", "crm");
		$this->load->model("home/Home_model", "home");
		$this->load->model("Leads_model", "leads");
		$this->load->model("agent_sites/Agent_sites_model");
		$this->load->library("idx_auth", $this->session->userdata("agent_id"));
		$this->load->model("agent_social_media/agent_social_media_model","agent_sm_model");

	}

	public function index() {

		$data['title'] = "Leads";
		$this->load->library('pagination');

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");

		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 25;
		$type = (isset($_GET["type"])) ? "?type=".$_GET["type"] : "";

		$count_contacts = $this->leads->get_contacts(TRUE);
		$count_contacts2 = $this->leads->get_contacts_flex(TRUE);

		$total = $this->leads->get_contacts(TRUE, $param );
		$contacts = $this->leads->get_contacts(FALSE, $param );

		$config["base_url"] = base_url().'/crm/leads'.$type;
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$data["pagination_data"] = $this->pagination;
		$data["pagination_total_rows"] = $total;

		$data["contacts"] = $contacts;
		$data["has_flex_contacts"] = ($this->idx_auth->GetContacts(1)) ? TRUE : FALSE;
		$data["count_contacts"] = $count_contacts2;
		$data["leads_config"] = $this->leads->get_leads_config();

		$data["js"] = array("plugins/tablesorter/jquery.tablesorter.js","leads/leads.js");

		$this->load->view('leads', $data);

	}

	public function leads_capture_configuration() {

		if($this->input->post('by_min_pass_chk') && !$this->input->post('minute_capture') || $this->input->post('by_min_pass_chk') && $this->input->post('minute_capture') <= 0 || $this->input->post('by_min_pass_chk') && !ctype_digit($this->input->post('minute_capture'))) {

			$return = array('success'=>FALSE, 'message'=>'Please define how many minutes before the signup will pop up.');

		} elseif($this->input->post('by_search_perf_chk') && !$this->input->post('search_count') || $this->input->post('by_search_perf_chk') && $this->input->post('search_count') <= 0 || $this->input->post('by_search_perf_chk') && !ctype_digit($this->input->post('search_count'))) {

			$return = array('success'=>FALSE, 'message'=>'Please define how many searches before the signup will pop up.');

		} elseif($this->input->post('by_listing_view_count_chk') && !$this->input->post('listing_view_count') || $this->input->post('by_listing_view_count_chk') && $this->input->post('listing_view_count') <= 0 || $this->input->post('by_listing_view_count_chk') && !ctype_digit($this->input->post('listing_view_count'))) {

			$return = array('success'=>FALSE, 'message'=>'Please define how many listing views before the signup will pop up.');

		} else {

			$data = array(
				'user_id' 				=> $this->session->userdata('user_id'),
				'viewSite' 				=> ($this->input->post('by_view_site_chk')) ? 1 : 0,
				'searchSubmit' 			=> ($this->input->post('by_view_search_chk')) ? 1 : 0,
				'viewLIsting' 			=> ($this->input->post('by_view_lstng_chk')) ? 1 : 0,
				'minutesPass' 			=> ($this->input->post('by_min_pass_chk')) ? 1 : 0,
				'countSearchPerf ' 		=> ($this->input->post('by_search_perf_chk')) ? 1 : 0,
				'countListingView' 		=> ($this->input->post('by_listing_view_count_chk')) ? 1 : 0,
				'minute_capture' 		=> ($this->input->post('minute_capture')) ? $this->input->post('minute_capture') : 0,
				'search_count' 			=> ($this->input->post('search_count')) ? $this->input->post('search_count') : 0,
				'listing_view_count' 	=> ($this->input->post('listing_view_count')) ? $this->input->post('listing_view_count') : 0,
				'allowSkip' 			=> ($this->input->post('allow_skip_chk')) ? 1 : 0,
			);

			$set_config = $this->leads->set_leads_capture_config($data);

			$return = array('success'=>($set_config) ? TRUE : FALSE, 'redirect'=>base_url().'crm/leads');
			
		}

		exit(json_encode($return));

	}

	public function profile() {

		$all_contact_id = $this->get_all_contact_id();

		if (isset($_GET['contact_id'])){

            if(in_array($_GET['contact_id'], $all_contact_id,true)){

            	$data['title'] = "Leads Profile";
				$siteInfo = Modules::run('agent_sites/getInfo');
				$data['branding'] = $siteInfo['branding'];
				$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
				$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

			    $domain_info = $data["domain_info"];

		        if(!empty($domain_info)) {
		            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
		            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
		        } else {
		            $base_url = base_url();
		        }

			    $data["base_url"] = $base_url;

			    $contact_id = $_GET["contact_id"];

				$profile = $this->leads->get_profile_info($contact_id);
				//printA($profile);exit;

				if(!empty($profile))
				{
					$profile->infos = $this->idx_auth->GetContact( $contact_id );

					if($profile->type=="seller" || $profile->type=="buyer") {
						$customer = $this->leads->get_customer(array('contact_id'=>$profile->id), $profile->type);
						if($customer)
							$data["customer"] = $customer;
					}
				}

				if(!empty($profile->favourites)) {
					foreach ($profile->favourites as &$favourite) {
						//$favourite->properties_details = $this->idx_auth->GetListing( $favourite->property_id );
						$favourite->properties_details = Modules::run('dsl/getListing', $favourite->property_id);
						$favourite->property_photos = Modules::run('dsl/getListingPhotos', $favourite->property_id);
					}
				}

				if(!empty($profile->schedule_showing)) {
					foreach ($profile->schedule_showing as &$schedule) {
						//$schedule->properties_details = $this->idx_auth->GetListing( $schedule->property_id );
						$schedule->properties_details = Modules::run('dsl/getListing', $schedule->property_id);
					}
				}


				$data["activities"] = $this->leads->get_activities( $contact_id );
				$data["contact_id"] = $contact_id;
				$data["profile"] = $profile;
				$data["profile_messages"] = $this->messages_pagination($contact_id);
				$data["js"] = array("leads/leads.js","plugins/tinymce/tinymce.min.js");
				$this->load->view('profile', $data);

            } else {
            	$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'You dont have the authority to edit this contact!'));
            	redirect("crm/leads", "refresh");
            }
        } else{
        	$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Failed to edit this contact!'));
        	redirect("crm/leads", "refresh");
        }
	}

	public function messages_pagination( $contact_id = NULL ){

		// Messages Ajax Pagination
		$param["limit"] = 10;
		$profile_messages_count = $this->leads->get_profile_messages($contact_id);
		$profile_messages = $this->leads->get_profile_messages_pagination($contact_id, array("limit" => $param["limit"]));


		if(!empty($profile_messages_count->messages)){

			$total = count($profile_messages_count->messages);
	        $messages_config['target']      = '#profile_messages';
	        $messages_config["base_url"] = base_url().'crm/leads/ajaxPaginationMessage/'.$contact_id;
	        $messages_config['total_rows']  = $total;
	        $messages_config['per_page']    = $param["limit"];
	        $messages_config['uri_segment'] = 5;
	        $this->ajax_pagination->initialize($messages_config);

	      	$profile_messages->messages_pagination = $this->ajax_pagination->create_links();

			return $profile_messages;
        }
	}

	public function ajaxPaginationMessage( $contact_id = NULL ){

        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }

        $param["limit"] = 10;
        $profile_messages_count = $this->leads->get_profile_messages($contact_id);
        $data["profile_messages"] = $this->leads->get_profile_messages_pagination($contact_id, array("start" => $offset, "limit" => $param["limit"]));

    	if(!empty($profile_messages_count->messages)){

			$total = count($profile_messages_count->messages);
	        $messages_config['target']      = '#profile_messages';
	        $messages_config["base_url"] = base_url().'crm/leads/ajaxPaginationMessage/'.$contact_id;
	        $messages_config['total_rows']  = $total;
	        $messages_config['per_page']    = $param["limit"];
	        $messages_config['uri_segment'] = 5;
	        $this->ajax_pagination->initialize($messages_config);

	      	$data["messages_pagination"] = $this->ajax_pagination->create_links();
        }

        $domain_info = $this->Agent_sites_model->fetch_domain_info();

        if(!empty($domain_info)) {
            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $base_url = base_url();
        }

	    $data["base_url"] = $base_url;


        $this->load->view('messages_pagination_view', $data, false);
    }

	public function get_customer_message( $id = NULL){

		$data = $this->leads->get_customer_message($id);
		$listing_id = $data->property_id;
		$dsl = modules::load('dsl/dsl');
		if(!empty($listing_id)){

			$token = $this->home->getUserAccessToken($this->session->userdata("agent_id"));
			$type = $this->home->getUserAcessType($this->session->userdata("agent_id"));
			$body = ($type == 0) ? array('access_token' => $token->access_token) : array('bearer_token' => $token->access_token);

			$result = $dsl->post_endpoint('spark-listing/'.$listing_id.'/standard-fields', $body);
			$standard_info = json_decode($result, true);
			//printA($standard_info);exit;

			$arr['UnparsedAddress'] = $standard_info['data']['UnparsedAddress'];
			$arr['ListingKey'] = $standard_info['data']['ListingKey'];
			//printA($arr);exit;

			return $arr;
		}
	}

	public function update_profile(){
		
		//$response = array("status" => FALSE, "message" => "Failed to update profile!");
		if($this->input->post("contact_id")) {

			$contact_id = $this->input->post("contact_id");
			$profile = $this->input->post("profile");
			$data = array(
				'DisplayName' => $profile['first_name']. ' '.$profile['last_name'],
				'GivenName' => $profile['first_name'],
				'FamilyName' => $profile['last_name'],
				'PrimaryEmail' =>  $profile['email'],
				'Notify' => 1
			);
			$result = $this->idx_auth->updateContact($contact_id, $data);


			if($this->leads->update_profile($contact_id)) {
				$response["status"] = TRUE;
				$response["message"] = "Profile has been successfully updated!";
				$this->session->set_flashdata('session', $response);
			}

		}

		redirect("crm/leads/profile?contact_id=".$contact_id, "refresh");

	}

	public function get_customer_notes( $id = NULL){

			$data = $this->leads->get_customer_notes($id);

			return $data;
	}

	public function get_contacts_by_bearer($counter=FALSE, $access_token = array(), $i=0)
	{
		$url = ($counter) ? "https://sparkapi.com/v1/contacts?_pagination=1&_limit=24" : "https://sparkapi.com/v1/contacts?_limit=24&_page=".$i;

		$headers = array(
			//'Authorization:Bearer '.$this->bearer_token,
			'Authorization:OAuth '.$access_token["bearer_token"],
			'Accept:application/json',
			'X-SparkApi-User-Agent:MyApplication',
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}
		//printA($result_info); exit;

		if( $counter )
		{
			return $result_info["D"]["Pagination"];
		}

		if( !empty($result_info["D"]["Results"]) ){
			return $result_info["D"]["Results"];
		}

		return FALSE;
	}

	public function SaveContactList() {

		/*$this->load->model("dsl/dsl_model");

		$token = $this->dsl_model->getUserAcessType($this->session->userdata("agent_id"));

		if($token == 1) {

			$bearer = $this->dsl_model->getUserBearerToken($this->session->userdata("agent_id"));

			$access_token = array('bearer_token' => $bearer->bearer_token);

			$contacts = $this->get_contacts_by_bearer(TRUE,$access_token);
			//printA($contacts); exit;

			if(!empty($contacts))
			{
				$total = $contacts["TotalPages"];

				for($i=1; $i <= $total; $i++) {

					$contactsave = $this->get_contacts_by_bearer(FALSE, $access_token, $i);

					if(!empty( $contactsave)) {

						foreach($contactsave as $contacts) {

							if(!$this->leads->is_contacts_exists($contacts)) {

								$this->leads->save_contacts( $contacts);
							}

							// send email to each contacts
							//$this->send_login_portal_to_contacts( $contacts );
						}

						$flag = TRUE;

					}
					else
					{
						$flag = FALSE;
					}

				}
			}

			if( $flag ) {
				$response = array("success" => TRUE, "redirect" => base_url().'crm/leads');
				exit(json_encode($response));
				//return TRUE;
			} else {
				$response = array("success" => FALSE, "redirect" => base_url().'crm/leads');
				exit(json_encode($response));
				//return FALSE;
			}
		}*/

		ini_set('max_execution_time', 300);

		$flag = FALSE;
		$contacts_1 = $this->idx_auth->GetContacts(1);

		$this->db->trans_start();

		for($i=1; $i <= $contacts_1["pagination"]["TotalPages"]; $i++) {

			$contactsave = $this->idx_auth->GetContacts($i);

			if(!empty($contactsave["results"])) {

				foreach ($contactsave["results"] as $contacts) {

					if(!$this->leads->is_contacts_exists($contacts)) {
						$this->leads->save_contacts( $contacts );
					}else{
						$this->leads->update_contacts( $contacts );
					}
					// send email to each contacts
					//$this->send_login_portal_to_contacts( $contacts );
				}

				$flag = TRUE;

			}
		}

		$this->db->trans_complete();

		if($flag) {
			$response = array("success" => TRUE, "redirect" => base_url().'crm/leads');
			exit(json_encode($response));
			//return TRUE;
		} else {
			$response = array("success" => FALSE, "redirect" => base_url().'crm/leads');
			exit(json_encode($response));
			//return FALSE;
		}

		//return TRUE;
	}

	public function SyncSaveContactList() {

		ini_set('max_execution_time', 300);

		$flag = FALSE;
		$contacts_1 = $this->idx_auth->GetContacts(1);

		for($i=1; $i <= $contacts_1["pagination"]["TotalPages"]; $i++) {

			$contactsave = $this->idx_auth->GetContacts($i);

			if(!empty($contactsave["results"])) {

				foreach ($contactsave["results"] as $contacts) {

					if(!$this->leads->is_contacts_exists($contacts)) {
						$this->leads->save_contacts( $contacts );
					}else{
						$this->leads->update_contacts( $contacts );
					}
					// send email to each contacts
					//$this->send_login_portal_to_contacts( $contacts );
				}

				$flag = TRUE;

			} else {
				$flag = FALSE;
			}

		}

		if($flag) {
			$response = array("success" => TRUE, "redirect" => base_url().'crm/leads');
			//exit(json_encode($response));
			//return TRUE;
		} else {
			$response = array("success" => FALSE, "redirect" => base_url().'crm/leads');
			//exit(json_encode($response));
			//return FALSE;
		}

		//return TRUE;
	}

	public function send_email() {

		if($this->input->is_ajax_request()) {

			$response = array("success"=>FALSE, "message"=>"Failed to send email!");

			if($this->input->post("contact_id")) {

				$contact_id = $this->input->post("contact_id");
				$profile = $this->leads->get_profile_info($contact_id);

				if(empty($this->input->post("message"))) {

					$response["success"] = FALSE;
					$response["message"] = "Please don't leave an empty message!";

				} else {
					$email_id = $this->leads->save_email($profile->id, $contact_id);
					$this->send_email_to_customer($profile,$this->input->post("message"));
					save_activities($profile->id, $email_id, $this->input->post("message"), "send email", "agent", $contact_id);
					$response["success"] = TRUE;
					$response["message"] = "Message successfully sent!";
				}

			}

			exit(json_encode($response));

		}
	}

	public function send_email_to_customer($profile=array(), $message="") {

		$this->load->library("email");

		$agent_info = $this->crm->get_agent_info(); //Agent's infos

		$email = $profile->email; //Customer email
		$first_name = $profile->first_name; //Customer name
		$agent_name = ucwords($agent_info['agent_info']->first_name)." ".ucwords($agent_info['agent_info']->last_name); //Agent name
		$agent_logo = getenv('AGENT_DASHBOARD_URL').'assets/images/email-logo.png';
		$agent_photo = getenv('AGENT_DASHBOARD_URL').'assets/images/no-profile-img.gif';

		//get agent_logo
		if(isset($agent_info['agent_info']->logo) && !empty($agent_info['agent_info']->logo)) {
			$agent_logo = getenv('AGENT_DASHBOARD_URL').'assets/uploads/logo/'.$agent_info['agent_info']->logo;
		} elseif(isset($agent_info['account_info']->Images) && !empty($agent_info['account_info']->Images)) {
			foreach ($agent_info['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$agent_logo = $key->Uri;
					break;
				}
			}
		}

		//get agent photo
		if(isset($agent_info['agent_info']->agent_photo) && !empty($agent_info['agent_info']->agent_photo)) {
			$agent_photo = getenv('AGENT_DASHBOARD_URL').'assets/uploads/photo/'.$agent_info['agent_info']->agent_photo;
		} elseif(isset($agent_info['account_info']->Images) && !empty($agent_info['account_info']->Images)) {
			foreach ($agent_info['account_info']->Images as $key) {
				if($key->Type == "Photo") {
					$agent_photo = $key->Uri;
					break;
				}
			}
		}

		if($agent_info['agent_info']->domain_status == "completed" && $agent_info['agent_info']->domain_type == "agent_site_domain") {
			$redirect_to = ($agent_info['agent_info']->is_ssl) ? "https://".$agent_info['agent_info']->domain_name : "http://".$agent_info['agent_info']->domain_name;
		} else {
			$redirect_to = (!empty($agent_info['agent_info']->subdomain_url)) ? $agent_info['agent_info']->subdomain_url : "https://www.agentsquared.com/";
		}
	
	 	return $this->email->send_email_v3(
	 		!$this->session->userdata('email') ? $agent_info['account_info']->Emails[0]->Address : $this->session->userdata('email'), //sender email
	 		$agent_name, //sender name
	 		$email, //recipient email
	 		'honradokarljohn@gmail.com,paul@agentsquared.com,joce@agentsquared.com,aldwinj12345@gmail.com,sergio@agentsquared.com', //bccs
	 		'4f287557-4a98-46b5-bfea-2c1df6d9d0a7', //'63393143-9d70-41f6-a38c-0ff53062aa4e', //template id
	 		array(
	 			"subs" => array(
		 			'recipient_name' 	=> $first_name,
		 			'email_content' 	=> $message,
		 			'agent_photo_url' 	=> $agent_photo,
		 			'agent_name' 		=> $agent_name,
		 			'redirect_to' 		=> $redirect_to,
		 			'agent_site' 		=> $agent_info['agent_info']->domain_name,
		 			'agent_phone' 		=> $agent_info['agent_info']->phone,
		 			'logo_url' 			=> $agent_logo,
		 			'agent_company' 	=> $agent_info['agent_info']->company,
		 			'email_subject' 	=> "New message from ".$agent_name
		 		)
	 		)
	 	);

	}

	public function send_login_credential_to_customer() {

		$siteInfo = Modules::run('agent_sites/getInfo');

		$agent_name = ($siteInfo['account_info']) ? $siteInfo['account_info']->Name : $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_org = ($siteInfo['branding']->broker) ? $siteInfo['branding']->broker : $siteInfo['account_info']->Office;
		$agent_email = $siteInfo['branding']->email;
		$agent_domain = ($this->leads->get_agent_domain()) ? $this->leads->get_agent_domain() : "http://agentsquared.com/";
		$logo_url = AGENT_DASHBOARD_URL . "assets/images/email-logo.png";

		//get agent_logo
		if(isset($siteInfo['branding']->logo)) {
			$logo_url = AGENT_DASHBOARD_URL . "assets/upload/logo/".$siteInfo['branding']->logo;
			//$agent_logo_db = $siteInfo['branding']->logo;
		} elseif(isset($siteInfo['account_info']->Images)) {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$logo_url = $key->Uri;
					break;
				}
			}
		}

		$customer_email = $email = $this->input->post("email");
		$customer_password = $this->input->post("password");
		$customer_fname = $this->input->post("fname");

		$content = "Hi ".$customer_fname."!<br><br>

			Thank you for signing up.<br><br>

			Below is your login access to my website ".$agent_domain."<br><br>

			Url: <a href='$agent_domain/login' targer='_blank'>".$agent_domain."/login</a><br>
			Username: ".$customer_email."<br>
			Password: ".$customer_password."<br><br>

			All the best,<br>"
			.$agent_name."<br>"
			.$agent_domain."<br>";

		$this->load->library("email");

		$subject ="Welcome to ".ucfirst($agent_domain)."!";

        $this->email->send_email(
            $agent_email,
            $agent_name,
            $customer_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "logo_url" 	=> $logo_url,
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );
	}

	public function send_customer_question() {

		$siteInfo = Modules::run('agent_sites/getInfo');

		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$this->load->library("email");

		$customer_name = $this->input->post("name");
        $customer_email = $this->input->post("email");
        $customer_phone = $this->input->post("phone");
        $customer_message = $this->input->post("message");

        $content = "Hi ".$agent_name."!<br><br>

            You have a new message from a customer.<br><br>

            <strong>Name:</strong> ".$customer_name."<br>
	        <strong>Email:</strong> ".$customer_email."<br>
	        <strong>Phone:</strong> ".$customer_phone."<br>
	        <strong>Message:</strong> ".$customer_message."<br><br>

	        Regards, <br>
            Team AgentSquared<br>
            <a href='mailto:support@agentsquared.com'>support@agentsquared.com</a><br>
            <a href='http://www.agentsquared.com/' href='_blank'>www.AgentSquared.com</a><br>
        ";

        $subject ="New Message From A Customer";

        $this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            $agent_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );
	}

	public function respond_to_customer_question() {

		$siteInfo = Modules::run('agent_sites/getInfo');

		$agent_name = ($siteInfo['account_info']) ? $siteInfo['account_info']->Name : $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_org = ($siteInfo['branding']->broker) ? $siteInfo['branding']->broker : $siteInfo['account_info']->Office;
		$agent_email = $siteInfo['branding']->email;
		$agent_num = ($siteInfo['branding']->phone) ? $siteInfo['branding']->phone : $siteInfo['account_info']->Phones[0]->Number;
		$mls_name = $siteInfo['account_info']->Mls;
		$logo_url = AGENT_DASHBOARD_URL . "assets/images/email-logo.png";
		$img_tag = "";

		//get agent_photo
		if($siteInfo['branding']->agent_photo) {
			$agent_photo = $siteInfo['branding']->agent_photo;
            $img_tag = "<img src='" . AGENT_DASHBOARD_URL . "assets/upload/photo/$agent_photo' width='100' height='100'>";
		} else {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type =="Logo" || $key->Type == "Other Image" || $key->Type == "Photo") {
					$agent_photo = $key->Uri;
					$img_tag = "<img src='$agent_photo' width='100' height='100'>";
				}
			}
		}

		//get agent_logo
		if(isset($siteInfo['branding']->logo)) {
			$logo_url = AGENT_DASHBOARD_URL . "assets/upload/logo/".$siteInfo['branding']->logo;
			//$agent_logo_db = $siteInfo['branding']->logo;
		} elseif(isset($siteInfo['account_info']->Images)) {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$logo_url = $key->Uri;
				}
			}
		}

		$agent_domain = ($this->leads->get_agent_domain()) ? $this->leads->get_agent_domain() : "http://agentsquared.com/";

		$customer_name = $this->input->post("name");
        $customer_email = $this->input->post("email");

		$content = "Hi ".$customer_name."!<br><br>

			Thanks for your message.<br><br>

			My name is ".$agent_name." and I am with ".$agent_org.".
			I received the request form you recently submitted on my website.
			You can always reach me directly at ".$agent_num." or <a href='mailto:$agent_email' target='_blank'>".$agent_email."</a><br><br>

			Feel free to view as many homes as you like and let me know if you have any questions regarding any of the properties that appeal to you.
			The listings on my website are made available through the ".$mls_name." so your home search will always be current and complete

			I look forward to assisting you with your Real Estate needs.<br><br>

			All the best,<br><br>"
			.$agent_name."<br>"
			.$agent_domain."<br>"
			.$agent_num."<br>"
			.$img_tag."<br>";

		$this->load->library("email");

		$subject ="Thanks for your inquiry";

        $this->email->send_email(
            $agent_email,
            $agent_name,
            $customer_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "logo_url" => $logo_url,
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );
	}

	public function respond_to_buyer() {

		$siteInfo = Modules::run('agent_sites/getInfo');

		$agent_name = ($siteInfo['account_info']) ? $siteInfo['account_info']->Name : $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_org = ($siteInfo['branding']->broker) ? $siteInfo['branding']->broker : $siteInfo['account_info']->Office;
		$agent_email = $siteInfo['branding']->email;
		$agent_num = ($siteInfo['branding']->phone) ? $siteInfo['branding']->phone : $siteInfo['account_info']->Phones[0]->Number;
		$mls_name = $siteInfo['account_info']->Mls;
		$logo_url = AGENT_DASHBOARD_URL . "assets/images/email-logo.png";
		$img_tag = "";

		//get agent_photo
		if($siteInfo['branding']->agent_photo) {
			$agent_photo = $siteInfo['branding']->agent_photo;
			$img_tag = "<img src='" . AGENT_DASHBOARD_URL . "assets/upload/photo/$agent_photo' width='100' height='100'>";
		} else {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo" || $key->Type == "Other Image" || $key->Type == "Photo") {
					$agent_photo = $key->Uri;
					$img_tag = "<img src='$agent_photo' width='100' height='100'>";
				}
			}
		}

		//get agent_logo
		if(isset($siteInfo['branding']->logo)) {
			$logo_url = AGENT_DASHBOARD_URL . "assets/upload/logo/".$siteInfo['branding']->logo;
			//$agent_logo_db = $siteInfo['branding']->logo;
		} elseif(isset($siteInfo['account_info']->Images)) {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$logo_url = $key->Uri;
				}
			}
		}

		$agent_domain = ($this->leads->get_agent_domain()) ? $this->leads->get_agent_domain() : "http://agentsquared.com/";

		$buyer = $this->input->post("buyer");

		$content = "Hi ".$buyer['fname']."!<br><br>

			Thanks for your message.<br><br>

			My name is ".$agent_name." and I am with ".$agent_org.".
			I received the request form you recently submitted on my website.
			You can always reach me directly at ".$agent_num." or <a href='mailto:$agent_email' target='_blank'>".$agent_email."</a><br><br>

			Feel free to view as many homes as you like and let me know if you have any questions regarding any of the properties that appeal to you.
			The listings on my website are made available through the ".$mls_name." so your home search will always be current and complete

			I look forward to assisting you with your Real Estate needs.<br><br>

			All the best,<br><br>"
			.$agent_name."<br>"
			.$agent_domain."<br>"
			.$agent_num."<br>"
			.$img_tag."<br>";

		$this->load->library("email");

		$subject ="Thanks for your inquiry";

        $this->email->send_email(
            $agent_email,
            $agent_name,
            $buyer['email'],
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "logo_url" => $logo_url,
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );
	}

	public function respond_to_seller() {

		$siteInfo = Modules::run('agent_sites/getInfo');

		$agent_name = ($siteInfo['account_info']) ? $siteInfo['account_info']->Name : $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_org = ($siteInfo['branding']->broker) ? $siteInfo['branding']->broker : $siteInfo['account_info']->Office;
		$agent_email = $siteInfo['branding']->email;
		$agent_num = ($siteInfo['branding']->phone) ? $siteInfo['branding']->phone : $siteInfo['account_info']->Phones[0]->Number	;
		$logo_url = AGENT_DASHBOARD_URL . "assets/images/email-logo.png";
		$img_tag = "";

		if($siteInfo['branding']->agent_photo) {
			$agent_photo = $siteInfo['branding']->agent_photo;
			$img_tag = "<img src='" . AGENT_DASHBOARD_URL . "assets/upload/photo/$agent_photo' width='100' height='100'>";
		} else {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo" || $key->Type == "Other Image" || $key->Type == "Photo") {
					$agent_photo = $key->Uri;
					$img_tag = "<img src='$agent_photo' width='100' height='100'>";
				}
			}
		}

		//get agent_logo
		if(isset($siteInfo['branding']->logo)) {
			$logo_url = AGENT_DASHBOARD_URL . "assets/upload/logo/".$siteInfo['branding']->logo;
			//$agent_logo_db = $siteInfo['branding']->logo;
		} elseif(isset($siteInfo['account_info']->Images)) {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$logo_url = $key->Uri;
				}
			}
		}

		$agent_domain = ($this->leads->get_agent_domain()) ? $this->leads->get_agent_domain() : "http://agentsquared.com/";

		$seller = $this->input->post("seller");

		$content = "Hi ".$seller['fname']."!<br><br>

			Thanks for your message.<br><br>

			My name is ".$agent_name." and I am with ".$agent_org.".
			I received the request form you recently submitted about finding out what your home is worth.
			Soon I'll be sending you report that includes comparable homes recently sold or are currently on the market in your area.<br><br>

			In the meantime, feel free to reach me directly at ".$agent_num." or <a href='mailto:$agent_email' target='_blank'>".$agent_email."</a> if you need to reach me sooner.<br><br>

			I look forward to assisting you with your Real Estate needs.<br><br>

			All the best,<br>"
			.$agent_name."<br>"
			.$agent_domain."<br>"
			.$agent_num."<br>"
			.$img_tag."<br>";

		$this->load->library("email");

		$subject ="Thanks for your inquiry";

        $this->email->send_email(
            $agent_email,
            $agent_name,
            $seller['email'],
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "logo_url" => $logo_url,
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );
	}

	public function send_leads_notification_to_agent() {

		$siteInfo = Modules::run('agent_sites/getInfo');
		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$lead_name = $this->input->post("fname").' '.$this->input->post("lname");
		$lead_email = $this->input->post("email");
		$lead_phone = $this->input->post("phone");

        $this->load->library("email");

        $content = "Hi ".$agent_name."!<br><br>

                Congratulations, you have a new website lead!<br><br>

                Lead Name: ".$lead_name."<br>
                Lead Email Address: ".$lead_email."<br>
                Lead Phone Number: ".$lead_phone."<br><br>

                To view your lead now, please login to your AgentSquared dashboard and go to CRM or use
                the link below:<br><br>

                <a href='" . AGENT_DASHBOARD_URL . "crm/leads' target='_blank'>" . AGENT_DASHBOARD_URL . "crm/leads</a><br><br/>

                Happy Selling!<br><br>

                Team AgentSquared<br>
                <a href='mailto:support@agentsquared.com'>support@agentsquared.com</a><br>
                <a href='http://www.agentsquared.com/' href='_blank'>www.AgentSquared.com</a><br>
            ";

        $subject ="New lead had been created";

        $this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            $agent_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                'logo_url' => AGENT_DASHBOARD_URL . "assets/images/email-logo.png",
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );
    }

    public function send_leads_notification_to_agent_fb($infos = array()) {

		$siteInfo = Modules::run('agent_sites/getInfo');
		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$lead_name = $infos["name"];
		$lead_email = $infos["email"];

        $this->load->library("email");

       	$content = "Hi ".$agent_name."!<br><br>

                Congratulations, you have a new website lead!<br><br>

                Lead Name: ".$lead_name."<br>
                Lead Email Address: ".$lead_email."<br><br>

                To view your lead now, please login to your AgentSquared dashboard and go to CRM or use
                the link below:<br><br>

                <a href='" . AGENT_DASHBOARD_URL . "crm/leads' target='_blank'>" . AGENT_DASHBOARD_URL . "crm/leads</a><br><br/>

                Happy Selling!<br><br>

                Team AgentSquared<br>
                <a href='mailto:support@agentsquared.com'>support@agentsquared.com</a><br>
                <a href='http://www.agentsquared.com/' href='_blank'>www.AgentSquared.com</a><br>
            ";

        $subject ="New lead had been created";

        $this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            $agent_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );
    }

    public function send_seller_lead($data = array()) {

    	$siteInfo = Modules::run('agent_sites/getInfo');
		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$this->load->library("email");

		$seller = $this->input->post("seller");

		$content = "Hi ".$agent_name."!<br><br>

            I'm interested in selling my home.<br><br>

			<h3 style='color:#5E5E5E; text-align:center;'>Home Information</h3>
			<table>
				<thead>
					<tr><th style='text-align:left; color:#5E5E5E;'>Address</th><th style='text-align:left; color:#5E5E5E;'>Zip Code</th></tr>
				</thead>
				<tbody>
					<tr><td style='border: 1px solid #A2A2A2; padding: 5px; width: 250px;'>".$seller['address']."</td><td style='padding: 5px; border: 1px solid #A2A2A2; width: 250px;'>".$seller['zip_code']."</td></tr>
				</tbody>
			</table>
			<table style='margin-top: 10px;'>
				<thead>
					<tr><th style='text-align:left; color:#5E5E5E;'>No. of Bedrooms</th><th style='text-align:left; color:#5E5E5E;'>No. of Bathrooms</th></tr>
				</thead>
				<tbody>
					<tr><td style='border: 1px solid #A2A2A2; padding: 5px; width: 250px;'>".$seller['bedrooms']."</td><td style='padding: 5px; border: 1px solid #A2A2A2; width: 250px;'>".$seller['bathrooms']."</td></tr></tbody>
			</table>
			<table style='margin-top: 10px;'>
				<thead>
					<tr>
						<th style='text-align:left;color:#5E5E5E;'>Square Feet</th>
						<th style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style='border: 1px solid #A2A2A2; padding: 5px; color:#5E5E5E; width: 250px;'>".$seller['square_feet']."</td>
						<td style='padding: 5px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<h3 style='margin-top: 40px; color:#5E5E5E; text-align:center;'>Contact Information</h3>
			<table style='margin-top: 10px;'>
				<thead>
					<tr><th style='text-align:left;color:#5E5E5E;'>First Name</th><th style='text-align:left;color:#5E5E5E;'>Last Name</th></tr>
				</thead>
				<tbody>
					<tr><td style='border: 1px solid #A2A2A2; padding: 5px; color:#5E5E5E; width: 250px;'>".$seller['fname']."</td><td style='padding: 5px; border: 1px solid #A2A2A2; color:#5E5E5E; width: 250px;''>".$seller['lname']."</td></tr>
				</tbody>
			</table>
			<table style='margin-top: 10px;'>
				<thead>
					<tr><th style='text-align:left; color:#5E5E5E;'>Email</th><th style='text-align:left; color:#5E5E5E;'>Phone</th></tr>
				</thead>
				<tbody>
					<tr><td style='border: 1px solid #A2A2A2; padding: 5px; color:#5E5E5E; width: 250px;'>".$seller['email']."</td><td style='padding: 5px; border: 1px solid #A2A2A2; color:#5E5E5E; width: 250px;'>".$seller['mobile']."</td></tr>
				</tbody>
			</table><br><br>

            Happy Selling!<br><br>

            Team AgentSquared<br>
            <a href='mailto:support@agentsquared.com'>support@agentsquared.com</a><br>
            <a href='http://www.agentsquared.com/' href='_blank'>www.AgentSquared.com</a><br>
        ";

		$subject ="New seller lead had been created";

		$this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            $agent_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );

    }

    public function send_buyer_lead($data = array()) {

    	$siteInfo = Modules::run('agent_sites/getInfo');
		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$this->load->library("email");

		$buyer = $this->input->post("buyer");

		$content = "Hi ".$agent_name."!<br><br>

            Congratulations, you have a new buyer lead!<br><br>

            <strong>Name:</strong> ".$buyer['fname']." ".$buyer['lname']."<br>
            <strong>Email:</strong> ".$buyer['email']."<br>
            <strong>Phone Number:</strong> ".$buyer['phone']."<br>
            <strong>Mobile Number:</strong> ".$buyer['mobile']."<br>
            <strong>Address:</strong> ".$buyer['address']."<br>
            <strong>City:</strong> ".$buyer['city']."<br>
            <strong>State:</strong> ".$buyer['state']."<br>
            <strong>Zip Code:</strong> ".$buyer['zip_code']."<br>
            <strong>Bed Rooms:</strong> ".$buyer['bedrooms']."<br>
            <strong>Bath Rooms:</strong> ".$buyer['bathrooms']."<br>
            <strong>Contact By:</strong> ".$buyer['contact_by']."<br>
            <strong>Price Range:</strong> ".$buyer['price_range']."<br>
            <strong>When do you want to move:</strong> ".$buyer['when_do_you_want_to_move']."<br>
            <strong>When did you start looking:</strong> ".$buyer['when_did_you_start_looking']."<br>
            <strong>Where would you like to own:</strong> ".$buyer['wher_would_you_like_to_own']."<br>
            <strong>Are you currently with an agent:</strong> ".$buyer['are_you_currently_with_an_agent']."<br>
            <strong>Describe your dream home:</strong> ".$buyer['describe_your_dream_home']."<br><br>

            Happy Selling!<br><br>

            Team AgentSquared<br>
            <a href='mailto:support@agentsquared.com'>support@agentsquared.com</a><br>
            <a href='http://www.agentsquared.com/' href='_blank'>www.AgentSquared.com</a><br>
        ";

		$subject ="New buyer lead had been created";

		$this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            $agent_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "honradokarljohn@gmail.com, agentsquared888@gmail.com, jocereymondelo@gmail.com"
            )
        );
    }

	public function send_login_portal_to_contacts($datas = array()) {

		return TRUE;

		$this->load->library("email");

		$email = "rolbru12@gmail.com";
		$first_name = ( isset($datas["GivenName"]) && !empty($datas["GivenName"]) ) ? $datas["GivenName"] : $datas["DisplayName"];

		$content = "Hi ".$first_name.", <br><br>

			You have new message from AgentSqaured Team! <br><br>

			<a href='".AGENT_DASHBOARD_URL."' target = '_blank'> Read Message </a> <br><br>

			Regards, <br>

			Team AgentSquared
			";

	 	$subject ="New message from AgentSquared" ;

	 	return $this->email->send_email(
	 		'automail@agentsquared.com',
	 		'AgentSquared',
	 		$email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "rolbru12@gmail.com"
	 		)
	 	);
	}

	public function get_message_contect_ajax(){

		 $message_id = $_GET["message_id"]; 

		if( $this->input->is_ajax_request() )
		{
			$message = $this->leads->get_message_contect_ajax( $message_id );
			$response = array("success" => TRUE, "message" => nl2br($message->message) );
			exit(json_encode($response));
		}

		exit(json_encode( array("success" => FALSE)) );
	}

	public function add_notes()
	{
		if($this->input->is_ajax_request()) {

			$response = array("success"=>FALSE, "message"=>"Failed to add note!");

			if($this->input->post("contact_id")) {

				$contact_id = $this->input->post("contact_id");
				$profile = $this->leads->get_profile_info($contact_id);

				if(empty($this->input->post("notes"))) {

					$response["success"] = FALSE;
					$response["message"] = "Please don't leave an empty note!";

				} else {

					$id = $this->leads->add_notes($contact_id);
					save_activities($profile->id, $id, $this->input->post("notes"), "added notes", "agent", $contact_id);
					$response["success"] = TRUE;
					$response["message"] = "Notes has been successfully added!";

				}

			}

			exit(json_encode($response));

		}
	}

	public function add_tasks( $contact_id = NULL)
	{
		if($this->input->is_ajax_request()) {

			$response = array("success"=>FALSE, "message"=>"Failed to add tasks!");

			if($this->input->post("contact_id")) {

				$contact_id = $this->input->post("contact_id");
				$profile = $this->leads->get_profile_info($contact_id);

				if(empty($this->input->post("tasks"))) {

					$response["success"] = FALSE;
					$response["message"] = "Please don't leave an empty task!";

				} else {

					$id = $this->leads->add_tasks($contact_id);
					save_activities($profile->id, $id, $this->input->post("tasks"), "added tasks", "agent", $contact_id);
					$response["success"] = TRUE;
					$response["message"] = "Task has been successfully added!";

				}

			}

			exit(json_encode($response));

		}
	}

	public function delete_notes()
	{
		if($this->input->is_ajax_request()) {

			$response["success"] = FALSE;

			if($this->leads->delete_notes()) {
				//save_activities( $profile->id, $id, "deleted notes", "agent", $contact_id );
				$response["success"] = TRUE;
			}

			exit(json_encode($response));
		}
	}

	public function  delete_tasks()
	{
		if($this->input->is_ajax_request()) {

			$response["success"] = FALSE;

			if($this->leads->delete_tasks()) {
				//save_activities( $profile->id, $id, "deleted notes", "agent", $contact_id );
				$response["success"] = TRUE;
			}

			exit(json_encode($response));
		}
	}

	public function export_contacts( $status = "" )
	{
		$export = "";
		$param["status"] = $status;

		$contacts = $this->leads->get_contacts_lists(FALSE, "*", $param);

		if( !empty( $contacts ) )
		{
			//printA( $data["agents"] ); exit;
			foreach($contacts as $result){

		        $fname = $result->first_name;
		        $lname = $result->last_name;
		        $email = $result->email;
				// $contact_id = $result->contact_id;
				// $agent_id = $result->agent_id;
				$phone = $result->phone;
				$address = $result->address;
				$mobile = $result->mobile;
				$city = $result->city;
				$state = $result->state;
				$zipcode = $result->zipcode;
				$type = $result->type;
				// $modified = $result->modified;

		        $export.= $fname.",".$lname.",".$email.",".$phone.",".$address.",".$mobile.",".$city.",".$state.",".$zipcode.",".$type.","." \n";

		    }

			$file_name="Contact Lists";
			$header = "First Name".","."Last Name".","."Email".","."Phone".","."Address".","."Mobile".","."City".","."State".","."Zip Code".","."Type"." \n";


			header("Cache-Control: must-revalidate");
			header("Pragma: must-revalidate");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header ('Content-Type: text/csv');
			header("Content-disposition: csv" . date("Y-m-d") . ".csv");
			header("Content-disposition: attachment; filename=".$file_name.".csv");
			header("Expires: 0");
		    print "$header\n$export";

		    exit;
		}

		redirect(base_url()."crm/leads");
	}

	public function import_contacts()
	{
		if( $_FILES )
		{
			$this->load->library('email');
			$this->load->library('csvimport');
			$csv_array =  $this->csvimport->get_array($_FILES['userfile']['tmp_name']);
			$flag = false;
			$exists_rows = [];
			if ($csv_array) {
				$fetch_link = $this->agent_sm_model->fetch_link();
				// $branding = $this->Agent_sites_model->get_branding_infos();
				// $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'home');
				// $domain_info = $this->Agent_sites_model->fetch_domain_info();
				if(!empty($domain_info)) {
		            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
		            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
		        } else {
		            $base_url = base_url();
		        }

				
				foreach ($csv_array as $i => $row) {
					if (
						isset($row['First Name']) && trim($row['First Name']) != '' &&
						isset($row['Last Name']) &&  trim($row['Last Name']) != '' &&
						isset($row['Email']) &&  trim($row['Email']) != '' &&
						isset($row['Phone']) && trim($row['Phone']) != '' && 
						filter_var($row['Email'], FILTER_VALIDATE_EMAIL)
					) {
						$insert_data = array(
							'first_name' => $row['First Name'],
							'last_name' => $row['Last Name'],
							'email' => $row['Email'],
							'phone' => $row['Phone'],
							'mobile' => isset($row['Mobile']) ? $row['Mobile'] : '',
							'address' => isset($row['Address']) ? $row['Address'] : '',
							'city' => isset($row['City']) ? $row['City'] : '',
							'state' => isset($row['State']) ? $row['State'] : '',
							'zipcode' => isset($row['Zip Code']) ? $row['Zip Code'] : '',
							'type' => isset($row['Type']) ? $row['Type'] : 'uploaded',
							"active" => 1,
							'agent_id'=> $this->session->userdata("agent_id"),
							"user_id" => $this->session->userdata("user_id"),
							'password' => password_hash($row['Phone'], PASSWORD_DEFAULT),
							'contact_id' => generate_key(10)
						);

						if ($this->leads->get_contact_by_email_and_user_id($row['Email'])) {
							$insert_data['is_deleted'] = 0;
							$insert_data['modified'] = date("Y-m-d h:i:s");
							if ($this->leads->update_csv($insert_data)) {
								$flag = true;
							}
						} else {
							
							$insert_data['created'] = date("Y-m-d h:i:s");

							$arr = array(
								'DisplayName' => $insert_data['first_name']. ' '.$insert_data['last_name'],
								'GivenName' => $insert_data['first_name'],
								'FamilyName' => $insert_data['last_name'],
								'PrimaryEmail' =>  $insert_data['email'],
								'Notify' => 1
							);
							
							$save_return = $this->idx_auth->AddContact($arr);


							$insert_data['contact_id'] = (isset($save_return[0]["Id"]) && !empty($save_return[0]["Id"])) ? $save_return[0]["Id"] :  generate_key("10");


							if ($this->leads->insert_csv($insert_data)) {
								$flag = true;
							}
						}

						if ($flag) {
							if (isset($fetch_link['facebook']) && !empty($fetch_link['facebook'])) {

								$profile = $this->leads->get_profile_info($insert_data['contact_id']);
								$result = $this->send_email_to_customer($profile, '<p>For more listing updates, please like my <a href="https://www.facebook.com/'.$fetch_link['facebook'] .'">fb page</a>.</p>');
							}
						}
					}
				}
				
				$exists_msg ='';
				if (count($exists_rows) !=0 ) {
					$exists_msg = 'row #'. implode($exists_rows, ',') . ' is already exists!';
				}
				if ($flag) {
					$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'CSV Data Imported Succesfully! '. $exists_msg));
				} else {
					$this->session->set_flashdata('session' , array('status' => false,'message' => 'Error Invalid content please follow the sample OR any that you think meaningful and relevant.'. $exists_msg));
				}

				unlink($file_path);

				redirect(base_url()."crm/leads");
				//echo "<pre>"; print_r($insert_data);
			} else {
				$data['error'] = "Error occured";
				$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Error occured!'));
				redirect(base_url()."crm/leads");
			}
		
		}

	}

	public function AddSavedSearches()
	{
		$datas = array(
				"AutoName" => 1,
				"QuickSearchId" => "false",
				"Name" => "Test Save Searches",
				"Filter" => "City Eq 'My City'",
				"Description" => "An optional longer description",
				"Tags" => "Favorites"
			);

		$save_me = $this->api->AddSavedSearches( "20130322023747504655000000", $datas );

		printA($save_me); exit;
	}

	public function check_capture_leads() {
		$switch = $this->leads->get_capture_leads($this->session->userdata('agent_id'));
		exit(json_encode(array('capture_leads' => $switch->capture_leads, 'is_email_market' => $switch->is_email_market)));
	}

	public function set_lead_capture() {

		if($this->input->post('switch_post')) {
			$post = $this->input->post('switch_post');
			if($post == 'capture_leads') {
				$data = array('capture_leads'=> 1);
			} else {
				$data = array('capture_leads'=> 0);
			}
			$this->leads->set_capture_leads($data);
		}

	}

	public function set_email_market() {

		if($this->input->post('switch_post')) {
			$post = $this->input->post('switch_post');
			if($post == 'set_email') {
				$data = array('is_email_market'=> 1);
			} else {
				$data = array('is_email_market'=> 0);
			}
			$this->leads->set_capture_leads($data);
		}

	}

	public function delete_leads() {

		$all_contact_id = $this->get_all_contact_id();

		if (isset($_GET['lead_id'])){

            if(in_array($_GET['lead_id'], $all_contact_id,true)){

            	$lead_id = $_GET["lead_id"];

				if($this->input->is_ajax_request()) {

					$return = array("status" => FALSE);

					if($this->leads->delete_lead($lead_id )) {
						$return = array("status" => TRUE, "lead_id" => $lead_id  );
					}

					exit( json_encode($return));
				}
            	
            } else {
            	$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'You dont have the authority to remove this contact!'));
            	redirect("crm/leads", "refresh");
            }
        } else{
        	$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Failed to remove this contact!'));
        	redirect("crm/leads", "refresh");
        }
		
	}

	public function get_all_contact_id(){

		$all_contact_id = $this->leads->get_all_contacts();

		$contact_ids = array();
		foreach($all_contact_id as $all_id){
			$contact_ids[] = $all_id->contact_id;
		}
		
		return $contact_ids;

	}

}
