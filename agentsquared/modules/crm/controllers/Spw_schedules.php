<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spw_schedules extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Crm_model", "crm");	
		$this->load->model("agent_sites/Agent_sites_model", "domain");
	}

	public function index() {

		$data['title'] = "Schedules";
		$this->load->library('pagination');

		$param = $this->input->get();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 25;

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];

		$total = $this->crm->get_spw_schedule_of_showing(TRUE, $param);
		$data["spw_schedules"] = $this->crm->get_spw_schedule_of_showing( FALSE, $param );
		//printA($data["spw_schedules"]); exit;

		$config["base_url"] = base_url().'crm/spw_schedules';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		// $data['css'] = array('bootstrap-datepicker.css', 'bootstrap-datepicker.standalone.css', 'bootstrap-datepicker3.css');

		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");

		$domain_info = $data["domain_info"];

        if(!empty($domain_info)) {
            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
            $data["base_url"] = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $data["base_url"] = base_url();
        }

		$data["js"] = array("leads/leads.js");

		$this->load->view('spw_schedules', $data);

	}

	function delete_schedules() {
		$param = $this->input->get();
		if(isset($param['cssid'])) {
			$result = $this->crm->delete_spw_schedule($param['cssid']);
			exit( json_encode(array('status' => $result)));
		}
		redirect("crm/spw_schedules", "refresh");
		

	}

}

?>