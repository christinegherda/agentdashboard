<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends MX_Controller {

	public $dsl;
	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Crm_model", "crm");	
		$this->load->model("Leads_model", "leads");	
		$this->load->model("agent_sites/Agent_sites_model");

		$this->dsl = modules::load('dsl/dsl');
	}

	public function index() {

		$data['title'] = "Leads";
		$this->load->library('pagination');
		
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 1;
		$param["limit"] = 24;
		$data["contacts"] = array();

		$page = ($param["offset"] / $param["limit"]) + 1;

		$page = ( $page >= 2 ) ? $page: 1;
						
		$endpoint = "contacts/".$this->session->userdata("user_id");
		$results = $this->dsl->post_endpoint($endpoint,array("_page" => $page, "_limit" => $param["limit"]));
		$contacts = json_decode($results, true);
		//printA($contacts); exit;		
		$total = $contacts["count"];

		if(!empty($contacts["data"]))
		{
			$data["contacts"] = $contacts["data"];
		}

		$config["base_url"] = base_url().'/crm/contacts';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		//printA( $data["contacts"] ); exit;
		
		$data["js"] = array("plugins/tablesorter/jquery.tablesorter.js","leads/leads.js");

		$this->load->view('contacts', $data);

	}

	public function profile( $contact_id = NULL ) {

		$this->load->library("idx_auth", $this->session->userdata("agent_id"));

		$data['title'] = "Leads Profile";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$d_url = site_url("home");
		if(!config_item("is_paid"))
	    {
	        $d_url = site_url("home"); 
	    }else
	    {
	        if(isset($data["domain_info"][0]->status) && isset($data["domain_info"][0]->is_subdomain)) {
	          if($data["domain_info"][0]->status == "completed" || $data["domain_info"][0]->is_subdomain == "1") { 
	              $d_url = isset($data["is_reserved_domain"]['domain_name']) ? "http://". $data["is_reserved_domain"]['domain_name'] : site_url("home");
	            
	          }elseif($data["is_reserved_domain"][0]->status != "completed" && $data["is_reserved_domain"][0]->is_subdomain == "0") { 
	              $d_url = !empty($data["is_reserved_domain"][0]->subdomain_url) ? "http://". $data["is_reserved_domain"][0]->subdomain_url : site_url("home");
				  
	          }
	          else { 
	              $d_url = site_url("home"); 
	          }
	        }
	    }
	    
	    $data["d_url"] = $d_url;

		$endpoint = "contacts/".$this->session->userdata("user_id")."/".$contact_id;
		$results = $this->dsl->get_endpoint($endpoint);
		$profile = json_decode($results, true);	
				
		if(!empty($profile["data"]))
		{ 
			if( isset($profile["data"]["type"]) )
			{
				if($profile["data"]["type"]=="seller" || $profile["data"]["type"]=="buyer") {
					$customer = $this->leads->get_customer(array('contact_id'=>$profile["Id"]),$profile["data"]["type"]);
					if($customer)
						$data["customer"] = $customer; 
				}
			}
		}

		$data["contact_messages"] = $this->leads->getContactMessages($contact_id);
		$data["contact_notes"] = $this->leads->getNotes($contact_id);
		$data["contact_tasks"] = $this->leads->getTasks($contact_id);
		$data["contact_favourates"] = $this->leads->getContactFavouritesList($contact_id);
		$save_searches = $this->leads->getContactSaveSearches($contact_id);

		$save_searches_flex = $this->leads->getSaveSearchesFlex( $contact_id );
        $save_searches_flex = ( empty($save_searches_flex) ) ? array() : $save_searches_flex;

        $data["contact_save_searches"] = array_merge($save_searches, $save_searches_flex);
        $data["contact_sched"] = $this->leads->getContactSchedule($contact_id);

		
		if(!empty($data["contact_favourates"])) {
			foreach ($data["contact_favourates"] as &$favourite) {
				$favourite->properties_details = $this->idx_auth->GetListing( $favourite->property_id );
			}
		}
		
		if(!empty($data["contact_sched"])) {
			foreach ($data["contact_sched"] as &$schedule) {
				$schedule->properties_details = $this->idx_auth->GetListing( $schedule->property_id );
			}	
		}
		
		//$message = $this->api->GetContactsMessages($contact_id, array('_orderby' => "CreatedTimestamp"));	
		
		$data["activities"] = $this->leads->get_activities( $contact_id );
		$data["contact_id"] = $contact_id;
		$data["profile"] = $profile["data"];
		$data["js"] = array("leads/leads.js");

		$this->load->view('contacts-profile', $data);

	}

	public function update_profile( $contact_id = NULL )
	{
		if( $_POST )
		{
			//printA(json_encode($this->input->post("profile"))); exit;
			$endpoint = "contacts/".$this->session->userdata("user_id")."/".$contact_id."/update";

			$profile  = $this->input->post("profile");

			$updates = array(
				"PrimaryEmail" => $profile["email"],
				"PrimaryPhoneNumber" => $profile["phone"],
				"MobilePhoneNumber" => $profile["mobile"],
				"HomeStreetAddress" => $profile["address"],
				"HomeLocality" => $profile["city"],
				"HomeRegion" => $profile["state"],
				"HomePostalCode" => $profile["zipcode"],
			);
			
			$results = $this->dsl->post_endpoint($endpoint,$updates);

			if($this->leads->update_profile( $contact_id ))
			{
				$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Profile has been successfully update!'));
				redirect("crm/leads/profile/".$contact_id, "refresh");
			}
			else
			{

				$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Failed to update profile!'));
				redirect("crm/leads/profile/".$contact_id, "refresh");
			}
		}

	}

}

