<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer_leads extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');
	
		$this->load->model("Crm_model", "crm");		
		$this->load->model("agent_sites/Agent_sites_model");
	}

	public function index() {

		$data['title'] = "Buyer Leads";

		$data["buyers"] = $this->crm->get_buyers_info();
		//printA($data["buyers"]); exit;
		$data['js'] = array("buyer-data-load.js");
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$this->load->view('buyer_leads', $data);
	}

	public function get_buyer_json()
	{
		$buyers = $this->crm->get_buyers_info();		

		exit(json_encode($buyers));
	}

}
