<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crm extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("agent_sites/Agent_sites_model");
	}

	public function index() {

		$data['title'] = "CRM";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

		$this->load->view('crm', $data);
	}


}

