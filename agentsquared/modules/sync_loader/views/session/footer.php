    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 

    <script type="text/javascript">
    $(document).ready(function(){
        var window_height = $(window).height();
        $(".content-wrapper").css('min-height', window_height);
    });

       $(".display-alert").delay(2000) .fadeOut(2000);

    </script>
</body>
</html>