<?php $this->load->view('sync_loader/session/header'); ?>
	<section class="content-wrapper section-loader">
		<div class="container">
			<div class="section-load">
				<div class="as-brand">
					<img src="<?= base_url().'assets/images/logo-brand-no-shadow'?>">
				</div>
				<p>
					Please wait while we are preparing your data. <br>
					This might take a while...
				</p>
				<div class="text-center">
			    	<img src="<?= base_url().'assets/img/sync/squares.gif'?>">
			    </div>
			</div>
		</div>
	</section>
<?php $this->load->view('sync_loader/session/footer'); ?>