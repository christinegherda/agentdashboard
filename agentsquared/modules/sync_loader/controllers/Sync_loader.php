<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sync_loader extends MX_Controller {

	function __construct() {
 		parent::__construct();

 		$this->load->model("home_model");
 	
	}
	public function index() {
		$data['title'] = "Sync Loader";
		$this->load->view('sync_loader', $data);
	}

	public function sync_update() {

		$data['title'] = "Sync Update";

		$dsl = modules::load('dsl/dsl');

		$token = $this->home_model->getUserAccessToken($this->session->userdata("agent_id"));
		
		$type = $this->home_model->getUserAcessType($this->session->userdata("agent_id"));
		
		if($type == 0) {
			$body = array(
				'access_token' => $token->access_token,
				'bearer_token' => ""
			);
		}
		else {
			$body = array(
				'bearer_token' => $token->access_token
			);
		}

		$endpoint = "agent-listings-updater/".$this->session->userdata("user_id");

		$result = $dsl->post_endpoint_updater($endpoint, $body);

		$this->load->view('sync_loader/sync_update', $data);
	}

}

?>