<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	$route['about_us'] = 'landing_page/about_us';
	$route['howitworks'] = 'landing_page/howitworks';
	$route['contact_us'] = 'landing_page/contact';
	$route['support'] = 'landing_page/support';
	$route['log_in'] = 'login/auth/login';
	$route['sign_up'] = 'landing_page/sign_up';
	$route['sync_data'] = 'landing_page/sync_data';

	//$route['test'] = "dashboard/";

?>