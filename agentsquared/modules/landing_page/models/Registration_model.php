<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		
	}

	public function fetch_user($user_id) {
		$array = array('id' => $user_id);
		$this->db->where($array);
		$query = $this->db->get('users')->row();

		if(isset($query)) {
			$data[] = array(
				'email' => $query->email,
				'id' => $query->id,
				'code' => $query->code,
                'agent_id' => $query->agent_id,
                'first_name' => $query->first_name,
				'created_on' => $query->created_on
			);
			return $data;
		} else
			return false;
	}

	public function insert_user($arr) {

		$user = array(
			'ip_address' => $arr['ip_address'],
			'email' => $arr['email'],
			'password' => password_hash($arr['password'], PASSWORD_DEFAULT),
			'active' => 1,
			'created_on' => date('Y-m-d H:i:s'),
			'code' => $arr["code"],
			'sync_status' => 0
		);

		$this->db->insert('users', $user);

		$user_id = $this->db->insert_id();

		$group["group_id"] = 4; 
        $group["user_id"] = $user_id;

        $this->db->insert('users_groups', $group);

        return $user_id;
	}

    public function insert_spark_user($arr) {
        $user = array(
            'agent_id'      => $arr['id'],
            'ip_address'    => $arr['ip_address'],
            'email'         => $arr['email'],  
            'password'      => password_hash($arr['password'], PASSWORD_DEFAULT),
            'first_name'    => $arr['first_name'],
            'last_name'     => $arr['last_name'],
            'company'       => $arr['company'],
            'phone'         => $arr['phone'],
            'city'          => $arr['city'],
            'address'       => $arr['address'],
            'zip'           => $arr['zip'],
            'broker'        => $arr['broker'],
            'theme'        => $arr['theme'],
            //'broker_number' => $arr['broker_number'],
            'active'        => 1,
            'created_on'    => date('Y-m-d H:i:s'),
            'code'          => $arr["code"],
            'sync_status'   => 0
        );

        $this->db->insert('users', $user);

        $user_id = $this->db->insert_id();

        $group["group_id"] = 4; 
        $group["user_id"] = $user_id;

        $this->db->insert('users_groups', $group);

        return $user_id;
    }

	public function update_sync_status($id = NULL, $arr = NULL) {
		$ret = false;
        if($id && $arr) {
            $this->db->where("id", $id);
            $this->db->update("users", $arr);
            $ret = true;
        }
        return $ret;
	}	

	public function readEmail($email) {
		$array = array('email' => $email);
		$this->db->where($array);
		$query = $this->db->get('users')->row();

		if(isset($query))
			return true;
		else
			return false;
	}

    public function readAgentID($agent_id) {
        $array = array('agent_id' => $agent_id);
        $this->db->where($array);
        $query = $this->db->get('users')->row();

        if(isset($query))
            return true;
        else
            return false;
    }

    public function readAgentToken($agent_id) {

        $ret = FALSE;

        if($agent_id) {

            $this->db->select("*")
                    ->from("users_idx_token")
                    ->where("agent_id", $agent_id);

            $query = $this->db->get()->row();

            if(!$query) {

                $this->db->select("*")
                        ->from("user_bearer_token")
                        ->where("agent_id", $agent_id);
                

                $query = $this->db->get()->row();
            }

            $ret = $query;
        }

        return $ret;
    }

    //Login
    public function check_sync_status($agent_id = NULL) {

        $ret = false;

        if($agent_id) {
            $ret = $this->db->select("site_name, sync_status")
                            ->from("users")
                            ->where("agent_id", $agent_id)->get()->row();
        }

        return $ret;
    }

    //Check IDX
    public function check_idx($user_id = NULL) {

        $ret = false;

        if($user_id) {
            $ret = $this->db->select("*")
                            ->from("property_idx")
                            ->where("user_id", $user_id)->get()->row();
        }

        return $ret;
    }


	//Sign Up
	public function signup($user) {

		$arr = array(
			'ip_address' => $user['ip_address'],
			'email' => $user['email'],
			'password' => password_hash($user['password'], PASSWORD_DEFAULT),
			'code' => $user["code"],
			'active' => 1,
			'created_on' => date('Y-m-d H:i:s')
		);

		$this->db->insert('users', $arr);

		$user_id = $this->db->insert_id();
		
        $group["group_id"] = 4; 
        $group["user_id"] = $user_id;

        $this->db->insert('users_groups', $group);

        return $user_id;
	}

    public function add_subdomain($data = array()) {

        $ret = false;

        if($data) {
            $this->db->insert('domains', $data);
            $ret = true;
        }
        return $ret;
    }

	//Check if domain is already in the database
	public function domain_exist($domain) {

		$array = array('domains' => $domain);
		$this->db->where($array);
		$query = $this->db->get('domains')->row();

		if(isset($query))
			return true;
		else 
			return false;
	}

    public function add_purchase_domain($domain_name)
    {	
        $this->db->select("domains")
                ->from("domains")
                ->where("domains", $domain_name)
                ->limit(1);

        $query = $this->db->get();
        //printA($query); 
        // if( $query->num_rows() > 0 )
        // {
        //     return array('success' => FALSE, 'type' => "exist_domain", 'error' => 'Selected domain is already reserved by agent squared!');      
        // }
        // else
        // {
            $domain['domains']  = $domain_name;//$post["domain_name"];
            $domain["date_created"] = date("Y-m-d h:m:s");
            $domain["status"] = "pending";
            $domain["type"] = "agent_site_domain";

            $this->db->insert("domains", $domain);

            if($this->db->insert_id())
            {
                $response["message"] = "Domain info has been successfully added!";
                $response["success"] = TRUE;
                $response["domain_id"] = $this->db->insert_id();
                //send to admin
                //$this->send_notification_to_admin(array("user_name" => $domain["user_name"],"domain_name" => $domain['domains']));
                //$this->send_notification_to_user(array("user_name" => $domain["user_name"],"domain_name" => $domain['domains']));
                return $response; 
            }   
            else
            {
                return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
            }
        //}
    }

    public function add_agent_id_domain($arr) {
    	$data = array('agent' => $arr['user_id']);
   		$this->db->where('domains', $arr['domain_name']);
   		$this->db->update('domains', $data);
    }

    public function update_agent_domain_status( $domain_id = NULL )
    {
        $update["status"] = "registered";
        $this->db->where('id', $domain_id);
        $this->db->update("domains", $update);

        if( $this->db->affected_rows() > 0 )
        {
           return TRUE;
        }

        return FALSE;
    }

    public function update_domain_user_id( $domain_id = NULL, $user_id = NULL )
    {
    	$update["agent"] = $user_id;
        $this->db->where('id', $domain_id);
        $this->db->update("domains", $update);

        if( $this->db->affected_rows() > 0 )
        {
           return TRUE;
        }

        return FALSE;
    }

}

?>