<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/sync_loader.css" type="text/css">
</head>
<body>
	<div class="container">
		<div class="sync_container">
			<h2 class="text">Please wait while we are preparing your data. This might take a while...</h2>
		    <div class="progress progress-striped progress-bar active" id="theprogressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
		        <div id="bar-text" class="bar" style="width: 0%;"></div>
		    </div>
		</div>
	</div>
</body>
	<script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
	<script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var $i;
			var $url = "";
			var $synced = 0;
			var $user_id = "<?php echo $user_id; ?>";
			var formData = {'user_id' : $user_id};
			var $bar = $('.bar');
			$.ajax({
				method: "POST",
				url: "<?php echo base_url(); ?>landing_page/add_account_mls_meta/",
				data: formData,
				dataType: 'json',
				beforesSend: function() {
					console.log("Fetching data");
				},
				success: function(data) {
					$('.bar').css("width", "8%");
					$bar.text("Creating user id");
					$('#theprogressbar').attr('aria-valuenow', "9");
					meta_property_types();
				}
			});

			function meta_property_types() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/meta_property_types/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "16%");
						$bar.text("Fetching meta property types");
						$('#theprogressbar').attr('aria-valuenow', "18");
						meta_units();
					}
				});
			}

			function meta_units() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/meta_units/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "24%");
						$bar.text("Fetching meta units");
						$('#theprogressbar').attr('aria-valuenow', "27");
						meta_rooms();
					}
				});
			}

			function meta_rooms() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/meta_rooms/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "32%");
						$bar.text("Fetching meta rooms");
						$('#theprogressbar').attr('aria-valuenow', "36");
						account_info();
					}
				});
			}

			function account_info() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/account_info/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "40%");
						$bar.text("Fetching account info");
						$('#theprogressbar').attr('aria-valuenow', "45");
						meta_field_groups();
					}
				});
			}

			function meta_field_groups() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/meta_field_groups/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "48%");
						$bar.text("Fetching meta field group");
						$('#theprogressbar').attr('aria-valuenow', "54");
						meta_standard_fields();
					}
				});
			}

			function meta_standard_fields() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/meta_standard_fields/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "56%");
						$bar.text("Fetching meta standard fields");
						$('#theprogressbar').attr('aria-valuenow', "63");
						meta_field_order();
					}
				});
			}

			function meta_field_order() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/meta_field_order/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "64%");
						$bar.text("Fetching meta field order");
						$('#theprogressbar').attr('aria-valuenow', "72");
						meta_custom_fields();
					}
				});
			}

			function meta_custom_fields() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/meta_custom_fields/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "72%");
						$bar.text("Fetching meta custom fields");
						$('#theprogressbar').attr('aria-valuenow', "81");
						system_info();
					}
				});
			}

			function system_info() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>idx_login/system_info/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "80%");
						$bar.text("Fetching system info");
						$('#theprogressbar').attr('aria-valuenow', "48");
						saved_searches();
					}
				});
			}

			function saved_searches() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/sync_saved_searches/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "88%");
						$bar.text("Fetching saved searches");
						$('#theprogressbar').attr('aria-valuenow', "90");
						save_contact_list();
					}
				});
			}

			function save_contact_list() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/sync_contact_list/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						$('.bar').css("width", "95%");
						$bar.text("Fetching contact list");
						$('#theprogressbar').attr('aria-valuenow', "90");
						property_standard_listings_fields();
					}
				});
			}

			function property_standard_listings_fields() {
				$.ajax({
					method: "POST",
					url: "<?php echo base_url(); ?>landing_page/property_standard_listings_fields/",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching property standard listings fields");
					},
					success: function(data) {
						$('.bar').css("width", "100%");
						$bar.text("Fetching property standard listings fields");
						$('#theprogressbar').attr('aria-valuenow', "100");
						update_sync_status();
					}
				});
			}

			function update_sync_status() {
				var syncStatus = {'id' : $user_id, 'sync_status' : 1};
				var $status_url = "<?php echo base_url(); ?>landing_page/update_sync_status/";
				$.ajax({
					method: "POST",
					url: $status_url,
					data: syncStatus,
					dataType: 'json',
					beforeSend: function() {
						console.log("Updating Sync Status");
					},
					success: function(data) {
						console.log(data);
						if(data.success) {
							console.log("Successfully Synced!");
							$bar.text("Completed");
		    				$('.progress').removeClass('active');
							window.location.href = "<?php echo base_url(); ?>dashboard";
							//window.location.href = "<?php echo base_url(); ?>featured_listings";
						}
					}
				});
			}
		});
	</script>
</html>
