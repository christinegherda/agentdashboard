<?php
    $this->load->view('landing_page/session/header');
    $this->load->view('landing_page/session/nav');
?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="contact-area">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <h2 class="text-center">CONTACT AGENT</h2>
                        </div>
                        <form action="">
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Message</label>
                                <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <button class="btn btn-default btn-block btn-submit">Submit</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="side-contact">
                            <h4>Or Contact us at these options</h4>
                            <ul>
                                <li>Phone: </li>
                                <li>Mobile: </li>
                                <li>Email: </li>
                            </ul>
                            <h4>You can also contact us via your social media accounts:</h4>
                            <ul class="list-inline contact-social-media">
                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php 
    $this->load->view('landing_page/session/footer');
?>
