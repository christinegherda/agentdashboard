<?php $this->load->view('landing_page/session/header'); ?>
        <section class="sign-up-page clearfix">
           <div class="sign-up-strip clearfix">
               <div class="container">
                   <div class="row">
                           <div class="left-sign-up">
                               <div class="col-md-6 col-sm-6">
                                   <img src="<?= base_url().'/assets/images/logo.png'?>" alt="Agentsquared Signup" class="sign-up-logo">
                                   <div class="welcoming-sign-up">
                                       <h1>Welcome to Agentsquared</h1>
                                       <h4>Thank you for purchasing!</h4>
                                   </div>
                               </div>
                           </div>
                           <div class="right-sign-up">
                               <div class="col-md-6 col-sm-6">
                                   <div class="sign-up-container clearfix">
                                       <h2>Sign Up</h2>
                                       <p>You are about to create your agent site. You will need your FLEX MLS IDX key. If you do not have it <a href="#">click here</a>.</p>
                                       <span class="display-status alert alert-flash alert-danger" style="display:none;"></span>
                                        <form method="post" id="signupForm" class="sign_up_form">
                                           <div class="form-group">
                                               <label for="email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control required">
                                           </div>
                                           <div class="form-group">
                                               <label for="password">Password</label>
                                               <input type="password" name="password" id="password" class="form-control required">
                                           </div>
                                           <div class="form-group">
                                               <label for="confirm_password">Confirm Password</label>
                                               <input type="password" name="confirm_password" id="confirm_password" class="form-control required">
                                           </div>
                                           <p>
                                               By clicking Create Account, I agree to the  <a href="" data-toggle="modal" data-target="#modalSignupTerms">Terms of Service</a> and <a href="" data-toggle="modal" data-target="#modalSignupPrivacy">Privacy Policy</a>.
                                           </p>
                                           <div class="col-md-6 col-sm-12 no-padding-left">
                                               <button type="submit" name="submit" class="btn btn-default btn-create-account create-account" data-type="<?php echo $_GET['type']; ?>">Create Account</button>    
                                           </div>
                                           <div class="col-md-6 col-sm-12">
                                               <div class="have-account">
                                                   <p>Already have an account?</p>
                                                   <p><a href="<?= site_url('login/auth/login'); ?>">Login</a></p>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                           </div>
      </section>

               <!-- terms of use -->
               <div class="modal fade legal-modal" id="modalSignupTerms" tabindex="-1" role="dialog">
                   <div class="modal-dialog modal-lg">
                       <div class="modal-content theme-modal">
                           <div class="modal-header">
                               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                               <h2 style="color:#fff" class="text-center">Terms of Use</h2>
                           </div>
                           <div class="modal-body clearfix">
                               <p>
                                PLEASE READ THESE TERMS AND CONDITIONS OF USE ("SITE TERMS") CAREFULLY. YOUR USE OF THE SITE IS CONDITIONED UPON YOUR ACCEPTANCE OF THESE SITE TERMS WITHOUT MODIFICATION. 
                                BY ACCESSING OR USING THIS WEB SITE, YOU AGREE TO BE BOUND BY THESE SITE TERMS AND ALL TERMS INCORPORATED BY REFERENCE. 
                                IF YOU DO NOT AGREE TO THESE SITE TERMS, DO NOT USE THIS WEB SITE.
                               <p>
                                The Web site you are accessing (the "Site") is hosted by AgentSquared.com on behalf of one of its real estate industry customers (the "Company"). 
                                AgentSquared and Company are independent contractors and are not partners or otherwise engaged in any joint venture in delivering the Site. 
                                Notwithstanding that fact, these Site Terms apply to both Agent Squared and Company and as such, any reference to "we", "us" or "our" herein will apply 
                                equally to both Agent Squared and Company.
                                These Site Terms apply exclusively to your access to, and use of, the Site so please read them carefully. 
                                These Site Terms do not alter in any way the terms or conditions of any other agreement you may have with Agent Squared, Company, or 
                                their respective subsidiaries or affiliates, for services, products or otherwise. We reserve the right to change or modify any of the Site 
                                Terms and the Site, at any time. If we decide to change our Site Terms, we will post a new version on the Site and update the date. 
                                Any changes or modifications will be effective immediately upon posting of the revisions on the Site, and you waive any right you may have to receive specific notice of such changes or modifications. 
                                Your use of the Site following the posting of changes or modifications to the Site Terms will constitute your acceptance of the revised Site Terms. 
                                Therefore, you should frequently review the Site Terms and applicable policies from time-to-time to understand the terms and conditions that apply to your use of the Site. 
                                If you do not agree to the amended terms, you must immediately stop using the Site.
                               </p>

                               <h4>Privacy</h4>
                               <p>
                                We believe strongly in providing you notice of how we collect and use your data, including personally identifying information, 
                                collected from the Sites. We have adopted a Privacy Statement to which you should refer to fully understand how we collect and use 
                                personally identifying information.
                               </p>

                               <h4>Consent to Communications</h4>
                               <p>
                                By filling out any forms on the Site or making any inquiries through the Site you acknowledge that we have an established business relationship and you expressly consent to being contacted us, 
                                whether by phone, mobile phone, email, mail or otherwise.
                               </p>

                               <h4>Property Listing Data</h4>                   
                               <p>
                                Any real estate listing data provided to you in connection with the Site is not intended to be a representation of the complete Multiple Listing Service data for any of our MLS sources. 
                                We are not liable for and do not guarantee the accuracy of any listing data or other data or information found on the Site, and all such information should be independently verified. The 
                                information provided in connection with the Site is for the personal, non-commercial use of consumers and may not be 
                                used for any purpose other than to identify prospective properties consumers may be interested in purchasing. 
                                Some properties which appear for sale on this website may no longer be available because they are under contract, have sold or are no longer being offered for sale.
                               </p>

                               <h4>Use of Site; Limited License</h4>
                               <p>
                                You are granted a limited, non-sublicensable license to access and use the Site and all content, data, information and materials included in the Site (the "Site Materials") 
                                solely for your personal use, subject to the terms and conditions set forth in these Site Terms. You may not use the Site or any Site Materials for commercial purposes. You agree that you will 
                                not modify, copy, distribute, resell, transmit, display, perform, reproduce, publish, license, create derivative works from, frame in another Web page, use on any other web site or service any of the Site Materials. 
                                You will not use the Site or any of the Site Materials other than for its intended purpose or in any way that is unlawful, or harms Agent Squared, Company and/or their suppliers.
                               </p>
                               <p>
                                Without prejudice to the foregoing, you may not engage in the practices of "screen scraping," "database scraping," "data mining" or any other activity with the purpose of obtaining lists of users or other information 
                                from the Site or that uses web "bots" or similar data gathering or extraction methods. You may not obtain or attempt to obtain any materials or information through any means not 
                                intentionally made available or provided for through the Site.
                               </p>
                               <p>
                                Any use of the Site or the Site Materials other than as specifically authorized herein is strictly prohibited and will terminate the license granted herein. Such unauthorized use may also violate applicable laws including without limitation copyright and 
                                trademark laws and applicable communications regulations and statutes. Unless explicitly stated herein, nothing in these Site Terms shall be construed as conferring any license to 
                                intellectual property rights, whether by estoppel, implication or otherwise. This license is revocable by us at any time.
                               </p>

                               <h4>Responsibility for Your Conduct and User Content</h4>
                               <p>
                                You are solely liable for your conduct and for any information, text, photos, content, materials or messages that you upload, post or transmit to the Site (collectively the "User Content"). 
                                You may not provide false or misleading information to the Site or submit information under false pretenses.
                               </p>
                               <p>
                                Without limiting anything else in these Site Terms, we may immediately terminate your access to and use of any of the Sites if you violate any of the foregoing or 
                                if you provide false or misleading information or submit information under false pretenses.
                               </p>

                               <h4>Account Security</h4>
                               <p>
                                Each person that is provided with a password and user ID to use the Site must agree to abide by these Site Terms and is responsible for all activity under such user ID. 
                                You are responsible for maintaining the confidentiality and security of any password connected with your account.
                               </p>

                               <h4>Submissions</h4>
                               <p>
                                You agree that any materials, including but not limited to questions, comments, suggestions, ideas, plans, notes, drawings, original or creative materials or other information regarding Company or the Site, provided by you in the form of e-mail or 
                                submissions to us, or postings on the Sites, are non-confidential (subject to our Privacy Policy). We will own exclusive rights, including all intellectual property 
                                rights, and will be entitled to the unrestricted use of these materials for any purpose, commercial or otherwise, without acknowledgment or compensation to you.
                               </p>

                               <h4>Hyperlinks</h4>
                               <p>
                                We make no claim or representation regarding, and accept no responsibility for, the quality, content, nature or reliability of third-party Web sites accessible by hyperlink from the Site, or Web sites linking to the Site. 
                                Such sites are not under our control and we are not responsible for the content of any linked site or any link contained in a linked site, or any review, changes or updates to such sites. We provide these links to you only as a convenience, and the inclusion of any 
                                link does not imply affiliation, endorsement or adoption by us of any site or any information contained therein. When you leave the Site, you should be aware that our terms and policies no longer govern. 
                                You should review the applicable terms and policies, including privacy and data gathering practices, of any site to which you navigate from the Site.
                               </p>

                               <h4>Third Party Products and Services; Third-Party Content</h4>
                               <p>
                                We may run advertisements and promotions from third parties on the Site or may otherwise provide information about or links or referrals to third-party products or services on the Site. Your business dealings or correspondence with, or participation in promotions of, 
                                such third parties, and any terms, conditions, warranties or representations associated with such dealings or promotions are solely between you and such third party. We are not responsible or liable for any loss or damage of any sort incurred as the result of any 
                                such dealings or promotions or from any third party products or services, and you use such third party products and services at your own risk. We do not sponsor, endorse, recommend or approve any such third party who advertises their goods or services through the Site. 
                                You should investigate and use your independent judgment regarding the merits, quality and reputation of any individual, entity or information that you find on or through the Site. We do not represent or warrant that any third party who places advertisements on the Site is 
                                licensed, qualified, reputable or capable of performing any such service.
                               </p>
                               <p>
                                We do not make any assurance as to the timeliness or accuracy of any third-party content or information provided on or linked to from the Site ("Third Party Content"). We do not monitor or have any control over any Third Party Content, and do not endorse or adopt any 
                                Third Party Content and can make no guarantee as to its accuracy or completeness. We will not update or review any Third Party Content, and if you choose to use such Third Party Content you do so at your own risk.
                               </p>

                               <h4>Disclaimer</h4>
                               <p>
                                THE SITE AND THE SITE MATERIALS (INCLUDING ALL THIRD PARTY CONTENT), AND ALL LINKS, INFORMATION, MATERIALS, EVALUATIONS, RECOMMENDATIONS, SERVICES 
                                AND PRODUCTS PROVIDED ON OR THROUGH THE SITES ARE PROVIDED ON AN "AS IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. YOU EXPRESSLY 
                                AGREE THAT USE OF THE SITE AND THE MATERIALS IS AT YOUR SOLE RISK. COMPANY AND AGENT SQUARED AND THEIR RESPECTIVE SUPPLIERS DISCLAIM ALL WARRANTIES, 
                                EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT 
                                AS TO THE SITE, THE SITE MATERIALS, LINKS, INFORMATION, MATERIALS, SERVICES AND PRODUCTS AVAILABLE ON OR THROUGH ON THE SITE.
                                NEITHER COMPANY NOR AGENT SQUARED REPRESENTS OR WARRANTS THAT THE MATERIALS ARE ACCURATE, COMPLETE, RELIABLE, CURRENT, OR ERROR-FREE; OR IS 
                                RESPONSIBLE FOR TYPOGRAPHICAL ERRORS OR OMISSIONS RELATING TO PRICING, TEXT, OR PHOTOGRAPHY. WHILE WE ATTEMPT TO ENSURE YOUR ACCESS AND USE OF THE 
                                SITES IS SAFE, NEITHER COMPANY NOR AGENT SQUARED REPRESENTS OR WARRANTS THAT THE SITES OR ITS SERVER(S) ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. 
                                AGENT SQUARED IS MERELY A SERVICE PROVIDER TO COMPANY, AND IN NO EVENT SHALL AGENT SQUARED BE LIABLE TO USER FOR ANY OF THE PRODUCTS, SERVICES, CONTENT 
                                OR INFORMATION PROVIDED THROUGH THE SITE OR OTHERWISE PROVIDED BY OR ON BEHALF OF COMPANY, AND AGENT SQUARED MAKES NO REPRESENTATION OR WARRANTY WITH 
                                RESPECT THERETO.
                               </p>

                               <h4>Limitation of Liability</h4>
                               <p>
                                IN NO EVENT WILL COMPANY OR AGENT SQUARED OR ANY OF THEIR RESPECTIVE SUPPLIERS BE LIABLE FOR ANY DIRECT, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, 
                                OR ANY OTHER DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO LOSS OF USE, LOSS OF PROFITS, OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT, TORT 
                                (INCLUDING BUT NOT LIMITED TO NEGLIGENCE), OR OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF THE SITE OR THE SITE MATERIALS.
                               </p>

                               <h4>Termination; Refusal to Provide Services.</h4>
                               <p>
                                We may terminate or suspend your access to the Site at any time, with or without cause, and with or without notice. Upon such termination or suspension, 
                                your right to use the Site will immediately cease. Furthermore, we reserve the right not to respond to any requests for information for any reason, 
                                or no reason.
                               </p>

                               <h4>Applicable Law and Venue</h4>
                               <p>
                                These terms and conditions are governed by and construed in accordance with the laws of the State of California, applicable to agreements made and 
                                entirely to be performed within the State of California, without resort to its conflict of law provisions. You agree that any action at law or in equity 
                                arising out of or relating to these terms and conditions can be filed only in state or federal court located in San Diego, California, and you hereby 
                                irrevocably and unconditionally consent and submit to the exclusive jurisdiction of such courts over any suit, action or proceeding arising out of these 
                                terms and conditions.
                               </p>

                               <h4>General Terms</h4>
                               <p>
                                If any part of the Site Terms is determined to be invalid or unenforceable pursuant to applicable law, then the invalid or unenforceable provision will 
                                be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the Site Terms 
                                will continue in effect. We may assign this Agreement, in whole or in part, at any time with or without notice to you. You may not assign this Agreement, 
                                or assign, transfer or sublicense your rights, if any, in the Sites. Except as expressly stated herein, the Site Terms constitute the entire agreement 
                                between you and us with respect to the Sites.
                               </p>

                               <h4>Copyright and Trademark</h4>
                               <p>
                                Unless otherwise indicated in the Site, the Site Materials and the selection and arrangement thereof are the proprietary property of Agent Squared, 
                                Company or their suppliers and are protected by U.S. and international copyright laws. The Company name, and any Company products and services slogans 
                                or logos referenced herein are either trademarks or registered trademarks of Company in the United States and/or other countries. The names of actual 
                                third party companies and products mentioned in the Site may be the trademarks of their respective owners. Any rights not expressly granted herein are 
                                reserved.
                               </p>
                               <p>
                                The parties have required that the Site Terms and all documents relating thereto be drawn up in English.  
                               </p>
                           </div>
                       </div>
                   </div>   
        </div>

    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 
    <script src="<?= base_url()?>assets/js/landing_page/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/main.js"></script>

</body>
</html>