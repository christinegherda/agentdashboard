<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/sync_loader.css" type="text/css">
</head>
<body>
	<div class="container">
		<div class="sync_container">
			<h2 class="text">Please wait while we are preparing your data. This might take a while...</h2>
		    <div class="progress progress-striped progress-bar active">
		        <div id="bar-text" class="bar" style="width: 0%;"></div>
		    </div>
		</div>
	</div>
	<div id="sync_modal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      	<div class="modal-header" style="background:#FEA42D;">
		        	<h2 class="modal-title" style="color:#FFFFFF; text-align:center;">Important!</h2>
		      	</div>
		      	<div class="modal-body" style="height:175px; padding: 45px;">
		        	<h4 class="text">
		        		You have updated your data in your MLS account. <br><br/> Please Click the <span style="color:green">CONTINUE</span> button to update your website.</h4>
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-danger no-radius" data-dismiss="modal" id="cancel_sync">CLOSE</button>
		        	<button type="button" class="btn btn-success no-radius" id="confirm_sync">CONTINUE</button>
		      	</div>
		    </div>
		</div>
	</div>
</body>
	<script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
	<script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			//var agent_idx_website = "<?php echo $this->session->userdata('agent_idx_website');?>";
			var agent_idx_website = "<?php echo $this->config->item('agent_idx_website'); ?>";

			$('#cancel_sync').click(function () {
				if(agent_idx_website == 1) {
					window.location.href = "<?php echo base_url(); ?>dashboard";
				} else {
					window.location.href = "<?php echo base_url(); ?>agent_sites/single_listings";
				}
			});

			$(".sync_container").hide();
			$("#sync_modal").modal('show');

			$("#confirm_sync").on("click", function() {

				$("#sync_modal").modal('hide');
				$(".sync_container").show();

				var $i;
				var $url = "";
				var $synced = 0;
				var $user_id = "<?php echo $user_id; ?>";
				var formData = { 'user_id' : $user_id };
				var $bar = $('.bar');

				for($i = 0; $i <= 1; $i++) {
					switch($i) {
					    case 0: 
					    	$url = "<?php echo base_url(); ?>landing_page/update_listings_json_property_standard_listings_fields/"; 
					    	break;
					    default:
					}
					if($i == 1) {
						console.log($bar.width());
						if($bar.width() >= 1500) {
							$bar.text("Completed");
					    	$('.progress').removeClass('active');
					    	var syncStatus = {'id' : $user_id, 'sync_status' : 1};
							var $status_url = "<?php echo base_url(); ?>landing_page/update_sync_status/";
							$.ajax({
								method: "POST",
								url: $status_url,
								data: syncStatus,
								dataType: 'json',
								beforeSend: function() {
									console.log("Updating Sync Status");
								},
								success: function(data) {
									console.log(data);
									if(data.success) {
										console.log("Successfully Synced!");
										window.location.href = "<?php echo base_url(); ?>dashboard";
									}
								}
							});
						}
					} else {
						$.ajax({
							method: "POST",
							url: $url,
							data: formData,
							dataType: 'json',
							beforesSend: function() {
								console.log("Fetching data");
							},
							success: function (data) {
								console.log(data);
								if(data.success) {
									 if($bar.width() < 1500) {							    	
								    	$bar.width($bar.width() + 1500);
								        $bar.text("Fetching property standard listings fields");
								        console.log($bar.width());
								        setTimeout( function (){
								        	$bar.text("Completed");	
								        	$('.progress').removeClass('active');
									    	var syncStatus = {'id' : $user_id, 'sync_status' : 1};
											var $status_url = "<?php echo base_url(); ?>landing_page/update_sync_status/";
											$.ajax({
												method: "POST",
												url: $status_url,
												data: syncStatus,
												dataType: 'json',
												beforeSend: function() {
													console.log("Updating Sync Status");
												},
												success: function(data) {
													console.log(data);
													if(data.success) {
														console.log("Successfully Synced!");
														if(agent_idx_website == 1) {
															window.location.href = "<?php echo base_url(); ?>dashboard";
															//window.location.href = "<?php echo base_url(); ?>featured_listings";
														} else {
															window.location.href = "<?php echo base_url(); ?>agent_sites/single_listings";
														}
													}
												}
											});
								        }, 3000);
								    }
								}	
							}
						});
					}
				}
			});
		});
	</script>
</html>
