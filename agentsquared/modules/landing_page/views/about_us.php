<?php
    $this->load->view('landing_page/session/header');
    $this->load->view('landing_page/session/nav');
?>
	<section class="page-container">
        <div class="page-title-container clearfix">
            <div class="col-md-12">
                <h2 class="text-center">About Us</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="about-us-description">
                        <p>
                            AgentSquared is a real estate market platform developed by Internet pioneers who have founded, built and sold leading brand name Internet companies such as Media Temple, Miva, and Attracta. AgentSquared is our latest success. We help Real Estate Agents sell homes and get new listings by giving them automated marketing tools such as Social Media sharing tools for new listings, and single property websites that are instantly created and updated through partnerships with leading MLS organizations throughout the world.
                        </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="about-us-contact">
                        <h4>Contact Info</h4>
                        <ul>
                            <li><i class="fa fa-envelope-o"></i>: <a href="">support@agentsquared.com</a></li>
                            <li><i class="fa fa-phone"></i>: 800-901-4428</li>
                            <li><i class="fa fa-map-marker"></i>: 888 Prospect St #200, La Jolla, CA 92037</li>
                        </ul>    
                    </div>
                    
                </div>
                
                <div class="page-section">
                    <div class="col-md-12">
                        <h2 class="text-center">Meet the Team</h2>
                    </div>
                    <div class="col-md-12">
                        <div class="team-container">
                            <div class="col-md-3">
                                <div class="team-item">
                                    <p class="text-center"><img src="<?= base_url()?>assets/img/landing/troy.png" alt="" class="img-circle team"></p>
                                    <p class="text-center">Troy McCasland</p>
                                    <p class="text-center">CEO</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-item">
                                    <p class="text-center"><img src="<?= base_url()?>assets/img/landing/albert.png" alt="" class="img-circle team"></p>
                                    <p class="text-center">Albert Lopez</p>
                                    <p class="text-center">CTO</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-item">
                                    <p class="text-center"><img src="<?= base_url()?>assets/img/landing/jeanne.png" alt="" class="img-circle team"></p>
                                    <p class="text-center">Jeanne Taylor</p>
                                    <p class="text-center">CFO</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-item">
                                    <p class="text-center"><img src="<?= base_url()?>assets/img/landing/eugene.png" alt="" class="img-circle team"></p>
                                    <p class="text-center">Eugene Kwon</p>
                                    <p class="text-center">Director of Channel Sales</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php 	
    $this->load->view('landing_page/session/footer');
?>