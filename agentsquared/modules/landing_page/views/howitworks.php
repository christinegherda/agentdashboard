<?php
    $this->load->view('landing_page/session/header');
    $this->load->view('landing_page/session/nav');
?>
	<section class="page-container">
        <div class="page-title-container clearfix">
            <div class="col-md-12">
                <h2 class="text-center">How it Works</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <br>
                    <img src="<?= base_url().'/assets/images/dashboard/how-it-works-page.svg'?>" alt="" class="img-responsive howitworks-flow">
                </div>
                <div class="col-md-6">
                    <ol>
                        <li>
                            <h3>Your MLS</h3>
                            <p>
                                Look for AgentSquared in your MLS dashboard. Click the link and bam! You get a beautiful mobile-friendly single property website to wow sellers and impress home buyers.
                            </p>    
                        </li>
                        <li>
                            <h3>Auto Posts to Social Media</h3>
                            <p>
                                After creating your Single Property Website click Social Media to authenticate Facebook, LinkedIn, and Twitter, and enable automatic postings: Check Out My New Listing, Come to My Open House, Price Reduced, Sold! 
                            </p>
                        </li>
                        <li>
                            <h3>Google Search SEO</h3>
                            <p>
                                Websites are built using SEO best practices so they appear high in organic search when home buyers search for the property address. 
                            </p>
                        </li>
                        <li>
                            <h3>Email Newsletters</h3>
                            <p>
                                Send emails to your contacts letting them know about your single property website to double your commission and get more listing.
                            </p>
                        </li>
                        <li>
                            <h3>Off-line Marketing</h3>
                            <p>Use the street address domain name for your website on for-sale signs, in print ads, and for flyers to focus buyer attention on your property and create easy smart phone access as they drive by.</p>
                        </li>
                        <li>
                            <h3>Analytics</h3>
                            <p>Analytics (coming soon) measure online traffic sources and helps determine which are most effective.</p>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
<?php 
    $this->load->view('landing_page/session/footer');
?>
