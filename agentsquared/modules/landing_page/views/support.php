<?php
    $this->load->view('landing_page/session/header');
    $this->load->view('landing_page/session/nav');
?>
	<section class="page-container">
        <div class="page-title-container clearfix">
            <div class="col-md-12">
                <h2 class="text-center">Support</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <form action="" class="contact-us-form">
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Message</label>
                            <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <button class="btn btn-default btn-submit">Submit</button>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="about-us-contact">
                        <h4>Contact Info</h4>
                        <ul>
                            <li><i class="fa fa-envelope-o"></i>: <a href="">support@agentsquared.com</a></li>
                            <li><i class="fa fa-phone"></i>: 800-901-4428</li>
                            <li><i class="fa fa-map-marker"></i>: 888 Prospect St #200, La Jolla, CA 92037</li>
                        </ul>    
                    </div>
                </div>
                
                <div class="page-section">
                    
                </div>
            </div>
        </div>
    </section>
<?php 
    $this->load->view('landing_page/session/footer');
?>
