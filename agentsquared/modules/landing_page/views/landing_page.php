<?php
    $this->load->view('landing_page/session/header');
    $this->load->view('landing_page/session/nav');
    $this->load->view('landing_page/session/flash');
?>
    <section class="marketing-pitch-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-intro">
                        <h2 class="text-center">Take Control of Your Online Presence </h2>
                        <p class="intro-line"><span ></span></p>   
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="marketing-item">
                        <div class="icon-image">
                            <i class="fa fa-television"></i>
                            <p class="text-center"><span class="diamond"></span></p>
                        </div>
                        <div class="description">
                            <h4>IDX Websites</h4>
                            <p>Beatiful and responsive real estates</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="marketing-item">
                        <div class="icon-image">
                            <i class="fa fa-area-chart"></i>
                            <p class="text-center"><span class="diamond"></span></p>
                        </div>
                        <div class="description">
                            <h4>CRM</h4>
                            <p>Manage leads, set tasks & reminders</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="marketing-item">
                        <div class="icon-image">
                            <i class="fa fa-tint"></i>
                            <p class="text-center"><span class="diamond"></span></p>
                        </div>
                        <div class="description">
                            <h4>Drip Marketing</h4>
                            <p>Nurture prospects with email campaigns</p>
                        </div>
                    </div>                    
                </div>
                <div class="col-md-3">
                    <div class="marketing-item">
                        <div class="icon-image">
                            <i class="fa fa-share-alt"></i>
                            <p class="text-center"><span class="diamond"></span></p>
                        </div>
                        <div class="description">
                            <h4>Online Advertising</h4>
                            <p>Google Adwords, Facebook, & more</p>
                        </div>
                    </div>
                </div>
                <p class="clearfix"></p>
                <div class="excess-pitch clearfix">
                    
                </div>
            </div>
        </div>
    </section>
    <section class="customer-reviews-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="review-title">Hear What Our Customers Are Saying </h2>
                    <p class="intro-line"><span ></span></p>
                    <div id="testimonial" class="animated fadeInUp">
                        <div class="testimonial-item text-center">
                            <img src="<?= base_url()?>assets/img/landing/brad-fielding.png" alt="Our Clients" class="img-circle">
                            <div class="clearfix">
                                <h3>Brad Fielding</h3>
                                <p>President at US Property, Investment & Mortgage Approval Center</p>
                                <h4>
                                    <i class="fa fa-quote-left"></i> 
                                        I love how beautiful the web sites look and it only took 3 minutes.
                                    <i class="fa fa-quote-right"></i>
                                </h4>
                            </div>
                        </div>
                        <div class="testimonial-item text-center">
                            <img src="<?= base_url()?>assets/img/landing/sean-germon.png" alt="Our Clients" class="img-circle">
                            <div class="clearfix">
                                <h3>Sean Germon</h3>
                                <p>Real Estate Developer/Broker with Coldwell Banker</p>
                                <h4>
                                    <i class="fa fa-quote-left"></i> 
                                        The social media sharing tools are amazing and help me get the word out about my new listings.
                                    <i class="fa fa-quote-right"></i>
                                </h4>
                            </div>
                        </div>
                        <div class="testimonial-item text-center">
                            <img src="<?= base_url()?>assets/img/landing/jason-pee.png" alt="Our Clients" class="img-circle">
                            <div class="clearfix">
                                <h3>Jason Pee'</h3>
                                <p>Preservationists, Development Consultant, Wholesale Broker, Realtor CIPS LMC TRC with Re/MAX</p>
                                <h4>
                                    <i class="fa fa-quote-left"></i> 
                                        The integration with my MLS makes this the best and easiest marketing solution for busy listing agents like me.
                                    <i class="fa fa-quote-right"></i>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pricing" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section id="price">
                        <div class="container">
                            <div class="row">

                                <div class="sec-title text-center animated fadeInDown">
                                    <h2>Price</h2>
                                    <p>Pick your service</p>
                                </div>
                                <div class="col-md-4 animated fadeInUp">
                                    <div class="price-table text-center">
                                        <span>Trial</span>
                                        <div class="value">
                                            <span></span>
                                            <span>FREE</span><br>
                                            <span>month</span>
                                        </div>
                                        <ul>
                                            <li>No Bonus Points</li>
                                            <li>No Bonus Points</li>
                                            <li>No Bonus Points</li>
                                            <li>No Bonus Points</li>
                                            <li><a href="<?= site_url('sign_up?type=trial'); ?>">sign up</a></li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="col-md-4 animated fadeInUp">
                                    <div class="price-table featured text-center">
                                        <span>Single Property Website</span>

                                        <div class="value">
                                            <span>$</span>
                                            <span>99</span><br>
                                            <span>month</span>
                                        </div>
                                        <ul>
                                            <li>Free Trial</li>
                                            <li>Free Trial</li>
                                            <li>Free Trial</li>
                                            <li>Free Trial</li>
                                            <li><a href="<?= site_url('sign_up?type=spw'); ?>">Purchase</a></li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="col-md-4 animated fadeInUp">
                                    <div class="price-table text-center">
                                        <span>IDX Website</span>
                                        <div class="value">
                                            <span>$</span>
                                            <span>125</span><br>
                                            <span>month</span>
                                        </div>
                                        <ul>
                                            <li>All Bonus Points</li>
                                            <li>All Bonus Points</li>
                                            <li>All Bonus Points</li>
                                            <li>All Bonus Points</li>
                                            <li><a href="<?= site_url('sign_up?type=idx'); ?>">Purchase</a></li>
                                        </ul>
                                    </div>
                                </div>
                
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <?php 
        $this->load->view('landing_page/session/wizard');
        $this->load->view('landing_page/session/footer'); 
    ?>
