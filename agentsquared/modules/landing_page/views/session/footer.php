    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="footer-copyright text-center">
                        2016 All Rights Reserved. Developed by Agent Squared
                    </h4>
                </div>
                <div class="col-md-12">
                    <div class="social-icons text-center">
                        <ul class="list-inline">
                            <li><a href="javascript:;"><i class="fa fa-envelope"></i></a></li>
                            <li><a href="https://www.facebook.com/AgentSquared/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/agentsquared" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                            <li><a href="https://plus.google.com/+Agentsquared/posts" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/agentsquared" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  -->
    <script type="text/javascript" src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/owl.carousel.js"></script>
    <script src="<?= base_url()?>assets/js/landing_page/jquery.validate.js"></script>
    <script src="<?= base_url() ?>assets/js/landing_page/bootstrap-filestyle.js"></script>   
    <script src="<?= base_url()?>assets/js/landing_page/main.js"></script>
    <?php if(isset($js)) :
    foreach($js as $key => $val ) :  ?>
      <script src="<?= base_url()?>assets/js/<?php echo $val?>" type="text/javascript"> </script>
    <?php endforeach;?>
    <?php endif;?>
    <script>
        $(document).ready(function(){
            $("#owl-carousel").owlCarousel({
                navigation : true, // Show next and prev buttons
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem:true
                // "singleItem:true" is a shortcut for:
                // items : 1, 
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false
            });

            $("#testimonial").owlCarousel({  
                navigation : true,
                pagination : false,
                slideSpeed : 700,
                paginationSpeed : 400,
                singleItem:true,
                navigationText: ["<i class='fa fa-angle-left fa-lg'></i>","<i class='fa fa-angle-right fa-lg'></i>"]
            });
        });
    </script>
</body>
</html>