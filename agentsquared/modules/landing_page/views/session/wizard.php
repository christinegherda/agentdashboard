<div class="modal fade" id="wizard-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                    <form action="landing_page/register" method="post" enctype="multipart/form-data" id="wizard-form">
                        <div id="wizard">
                            <span class="response-text"></span>
                            <span class="display-status alert alert-danger col-md-7" style="display:none;"></span>
                            <h3>IDX Integration <span class="triangle"></span></h3>
                            <section class="animated slideInRight">
                                <div class="col-md-offset-0 col-md-6">
                                    <h4>Integrate your flexMLS IDX</h4>
                                    <h4>API Credentials</h4>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <input type="hidden" name="email" id="agent_email">
                                            <input type="hidden" name="password" id="agent_password">
                                            <label class="col-md-3" for="api_key">API Key</label>
                                            <div class="col-md-9">
                                                <input type="text" name="api_key" class="form-control">
                                            </div>     
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3" for="api_secret">API Secret</label>
                                            <div class="col-md-9">
                                                <input type="text" name="api_secret" class="form-control">
                                            </div>     
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <h3>Theme Selection <span class="triangle"></span></h3>
                            <section class="animated slideInRight">
                                <h4>Choose a Design</h4>
                                <p>With just one click, your site is now an eye candy</p>
                                <div class="col-md-4">
                                    <div class="theme-picture clearfix">
                                        <div class="col-md-12">
                                            <h4 class="theme-name text-center">Agent Haven</h4>
                                        </div>
                                        <img src="<?= base_url()?>assets/img/landing/agent-haven.jpg" alt="" class="img-responsive">    
                                        <div class="theme-description">
                                            <div class="activate-buttons">
                                                <input type="hidden" name="theme" value="">
                                                <button type="button" class="btn btn-default btn-action" id="agent-haven-btn"><i class="fa fa-check-square"></i> Activate</button>
                                                <!-- <button type="button" class="btn btn-default btn-action" data-toggle="modal" data-target="#preview-site">
                                                    <i class="fa fa-search"></i> Preview
                                                </button>  -->   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-picture clearfix">
                                        <div class="col-md-12">
                                            <h4 class="theme-name text-center">Reality</h4>
                                        </div>
                                        <img src="<?= base_url()?>assets/img/landing/reality-theme.png" alt="" class="img-responsive">    
                                        <div class="theme-description">
                                            <!-- <div class="activate-buttons">
                                                <input type="hidden" name="theme" value="">
                                                <button type="button" class="btn btn-default btn-action" id="greenery-btn"><i class="fa fa-check-square"></i> Activate</button>
                                                <button type="button" class="btn btn-default btn-action" data-toggle="modal" data-target="#preview-site">
                                                    <i class="fa fa-search"></i> Preview
                                                </button>    
                                            </div> -->
                                            <div class="theme-status"><span class="btn btn-danger">Not Available</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-picture clearfix">
                                        <div class="col-md-12">
                                            <h4 class="theme-name text-center">Landmark</h4>
                                        </div>
                                        <img src="<?= base_url()?>assets/img/landing/landmark-theme.png" alt="" class="img-responsive">    
                                        <div class="theme-description">
                                           <!--  <div class="activate-buttons">
                                                <input type="hidden" name="theme" value="">
                                                <button type="button" class="btn btn-default btn-action" id="proper-btn"><i class="fa fa-check-square"></i> Activate</button>
                                                <button type="button" class="btn btn-default btn-action" data-toggle="modal" data-target="#preview-site">
                                                    <i class="fa fa-search"></i> Preview
                                                </button>    
                                            </div> -->
                                            <div class="theme-status"><span class="btn btn-danger">Not Available</span></div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h3>Site Info <span class="triangle"></span></h3>
                            <section class="animated slideInRight">
                                <h4>Site Information, make your pitch with a tag line</h4>
                                <p>Customize your site furthermore with your branding, site name, etc.</p>
                                <div class="agent-site-info-content">
                                    <div class="branding agent-site-info-panels">
                                        <h4>Branding</h4>
                                        <div class="col-md-offset-0 col-md-8">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label for="site_name" class="col-md-2">Site Name</label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="site_name" class="form-control">    
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tag_line" class="col-md-2">Tag Line</label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="tag_line" class="form-control">    
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="logo" class="col-md-2">Logo</label>
                                                    <div class="col-md-10">
                                                        <div class="input-group">
                                                            <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                                        </div>    
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="favicon" class="col-md-2">Favicon<br/><small>should be 16x16</small></label>
                                                    <div class="col-md-10">
                                                        <div class="input-group">
                                                            <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="clearfix"></p>
                                </div>
                            </section>

                            <!-- <h3>Custom Domain <span class="triangle"></span></h3>
                            <section class="animated slideInRight">
                                <div class="col-md-8"><h5 class="text-success"><i class="fa fa-info-circle"></i> Setting up custom domain is optional at this time, and you can do it later. If you wish to set up now, please proceed below.</h5></div>
                                <div id="set_up_container">
                                    <div class="col-md-12" id="existing_domain">
                                        <h4>Do you have an existing domain?</h4>
                                        <button class="btn btn-default btn-action" id="domain_yes" type="button">Yes</button><button class="btn btn-default btn-action" id="domain_no" type="button">No</button>
                                    </div>
                                    <div class="col-md-12" id="domain_exist_container" style="display:none;">  
                                        <h4>Enter your existing domain</h4>
                                        <div class="col-md-offset-0 col-md-6">
                                            <form method="GET" action="<?php echo base_url(); ?>landing_page/check_domain" id="domain-form" class="form-horizontal domain-ajaxq">
                                                <div class="col-md-6 no-padding-left">
                                                    <div class="input-group">
                                                        <input placeholder="example.com" type="text" name="domain_name" id="domain_name" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <button class="btn btn-default btn-action domain-ajax" type="button" id="check_domain_sbt"><i class="fa fa-search"></i>&nbsp;Check Domain</button>
                                                    <button class="btn btn-info btn-action" id="checkDomainLoad" type="button" style="width: 127px; font-size: 16px; display:none;"><i class="fa fa-cog fa-spin"></i>&nbsp;<strong>Checking...</strong></button>
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-12" id="domain_container" style="display:none;">
                                        <p id="domain-error-msg" style="display:none; color:red;"></p>
                                        <h4>Enter your domain.</h4>
                                        <p>Check if your domain exist</p>
                                        <div class="col-md-offset-0 col-md-6">
                                            <form method="GET" action="<?php echo base_url(); ?>landing_page/check_domain" id="domain-form" class="form-horizontal domain-ajaxq">
                                                <div class="col-md-7 clearfix">
                                                    <div class="input-group">
                                                        <input type="hidden" name="domain_check" class="domain_check" value="1"> 
                                                        <input type="hidden" name="domain_url" class="domain_url" value="<?php echo base_url(); ?>landing_page/check_domain">   
                                                        <input type="hidden" name="domainUrl" value="<?php echo site_url('landing_page/available_domain'); ?>" id="domainUrl">                                                      
                                                        <input type="hidden" name="check_domain_exist" value="<?php echo site_url('landing_page/check_domain_exist'); ?>" id="check_domain_exist">                                                      
                                                        <input type="hidden" name="purchaseDomainUrl" value="<?php echo site_url('landing_page/purchase_domain'); ?>" id="purchaseDomainUrl">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 no-padding-left">
                                                    <input placeholder="Domain Name" type="text" name="domain_name" id="domain_name" class="form-control">
                                                    <input type="hidden" name="domain_name_holder">
                                                    <input type="hidden" name="domain_id" value="" class="purchased_domain_id">
                                                </div>
                                                <div class="col-md-3">
                                                    <select id="suffix" class="form-control" name="suffix">
                                                        <option value="com">.com</option>
                                                        <option value="net">.net</option>
                                                        <option value="org">.org</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <button class="btn btn-default btn-action domain-ajax" type="button" id="check_domain_sbt"><i class="fa fa-search"></i>&nbsp;Check Domain</button>
                                                    <button class="btn btn-info btn-action" id="checkDomainLoad" type="button" style="width: 127px; font-size: 16px; display:none;"><i class="fa fa-cog fa-spin"></i>&nbsp;<strong>Checking...</strong></button>
                                                </div>
                                            </form>
                                            <div class="alert alert-success return-mess" style="display:none"></div>
                                            <div class="col-md-12 return-hide" >
                                                <div id="domain-result" class="clearfix">
                                                    <p class="clearfix"></p>
                                                    <div id="domainResultAv" class="domain-group" style="display:none;">
                                                       
                                                        <div class="availableDomains" style="display:none;">
                                                            <div class="col-md-12 col-sm-5">
                                                                <h5>Available Domaine Name:</h5>
                                                            </div>
                                                            <div class="col-md-12 col-sm-5">
                                                                <h5></h5>
                                                            </div>
                                                            <ul class="domain-list"></ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 return-hide">
                                                <div id="domain-result" class="clearfix">
                                                    <p class="clearfix"></p>
                                                    <div id="domainResult"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section> -->

                            <h3>Social Media <span class="triangle"></span></h3>
                            <section class="animated slideInRight">
                                <div class="col-md-8"><h5 class="text-success"><i class="fa fa-info-circle"></i> Social Network Integration is optional at this time, and you can do it later. <!-- If you wish to integrate it now, click <a href="javascript:;" id="set_up_social_links">here</a>. --></h5></div>
                                <div id="sm_integrate" class="col-md-12">
                                    <span class="display-text"></span>
                                    <h4>Social Network Integration</h4>
                                    <p>Let the world know you exist. Connect with the world.</p>
                                    <div class="col-md-offset-0 col-md-6 clearfix">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-3" for="facebook_url"><i class="fa fa-facebook"></i> Facebook</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="fb_link" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3" for="twitter_url"><i class="fa fa-twitter"></i> Twitter</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="tw_link" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3" for="googleplus_url"><i class="fa fa-google-plus"></i> Google Plus</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="gplus_link" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3" for="linkedin_url"><i class="fa fa-linkedin"></i> Linkedin</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="ln_link" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- <div id="domainModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static"  data-width="800">   -->
    <div id="domainModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-content" style="width: 39%; margin: auto; margin-left: 317px; margin-top: 126px;">
          <div class="modal-header">
            <h1>Reserve a Domain</h1>
            <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body2" style="height: 250px; width: 100%; position: relative; padding: 15px;">
            <p style="font-size: 20px; margin-top: 30px;">Click continue if you are sure this is the domain you want.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btnDomainCancel">Close</button>
            <a type="button" class="btn btn-primary confirm-purchase-domain" data-domain-name="" data-domain-price="" >Continue</a>
          </div>
        </div><!-- /.modal-content -->
     <!-- /.modal-dialog -->
    </div><!-- /.modal -->