    <section class="slider">
        <div class="carousel animated" data-animation="fadeIn">
            <div class="carousel-inner" role="listbox">
                <div class="item active animated fadeIn">
                    <img src="<?= base_url()?>assets/img/landing/bg02.jpg" alt="...">
                    <div class="overlay"></div>
                    <div class="carousel-caption">
                        <h1>REAL ESTATE MARKETING <br> MADE EASY</h1>
                        <a href="" class="btn btn-default btn-submit">LEARN MORE</a>
                    </div>
                </div>
            </div>
        </div>
    </section>