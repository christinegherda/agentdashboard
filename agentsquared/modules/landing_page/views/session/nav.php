<nav class="navbar navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="pull-right registration-header">
                    <!--<a href="<?= site_url('login'); ?>">LOGIN</a> | <a href="" data-target="#wizard-modal" data-toggle="modal">SIGN UP</a>-->
                    <a href="<?= site_url('login/auth/login'); ?>">LOGIN</a> | <a href="<?php if($this->uri->segment(1) == 'howitworks' || $this->uri->segment(1) == 'about_us' || $this->uri->segment(1) == 'support' || $this->uri->segment(1) == 'contact_us') echo base_url().'landing_page#pricing'; else echo '#pricing'; ?>">SIGN UP</a>
                    <!-- <a href="<?= site_url('login'); ?>">LOGIN</a> | <a href="<?= site_url('sign_up');?>">SIGN UP</a> -->
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= site_url(''); ?>"><img src="<?= base_url()?>assets/img/landing/as_logo.png" alt="" class="img-responsive"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right main-menu">
                <li class="active">
                    <a href="<?= site_url(''); ?>">HOME</a>
                </li>
                <li><a href="<?= site_url('howitworks'); ?>">HOW IT WORKS</a></li>
                <li><a href="javascript:;">PARTNERS</a></li>
                <li><a href="<?= site_url('about_us'); ?>">ABOUT US</a></li>
                <li><a href="<?= site_url('support'); ?>">SUPPORT</a></li>
                <li><a href="<?= site_url('contact_us'); ?>">CONTACT</a></li>
            </ul>
        </div>
    </div>
</nav>