<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign_up extends MX_Controller {

	protected $api; 

	function __construct() {
		parent::__construct();

		$this->load->model('registration_model', 'signup_model');
	}

	public function index() {

		$data['title'] = "Sign Up";
		$this->load->view('sign_up', $data);

	}

	public function signup() {

		$user_id = $this->insert_user();

		if($user_id) {
			$this->auto_login($user_id);
			$response = array( "success" => TRUE, "redirect" => base_url()."agent_sites/spark_member" );
			exit( json_encode( $response) );
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function signup_trial() {
		$user_id = $this->insert_user();

		if($user_id) {
			$this->auto_login($user_id);
			$response = array( "success" => TRUE, "redirect" => base_url()."dashboard" );
			exit( json_encode( $response) );
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function insert_user() {

		$email = $this->input->post('email');
		$password = $this->input->post('password');

		if(!$this->check_email($email)) {

			$code = generate_key( 8 );
			
			$user = array(
				'ip_address' => $this->input->ip_address(),
				'email' => $email,
				'password' => $password,
				'code' => $code
			);

			$id = $this->signup_model->signup($user);
			return $id;
		} else {
			return false;
		}
	}

	public function auto_login($user_id) {
		$user = $this->signup_model->fetch_user($user_id);
		$arr = array(
			'user_id' => $user[0]['id'],
			'email' => $user[0]['email'],
			'identity' => $user[0]['email'],
			'old_last_login' => $user[0]['created_on'],
			'logged_in_auth' => true,
			'code' => $user[0]['code'],
		);
		$this->session->set_userdata($arr);
	}

	public function check_email($email) {

		return $this->signup_model->readEmail($email) ? true : false;
	
	}

}

?>