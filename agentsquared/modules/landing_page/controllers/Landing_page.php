<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing_page extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('registration_model', 'reg_model');
		$this->load->model('idx_integration/idx_model', 'idx_model');
		$this->load->model('choose_theme/choose_theme_model', 'theme_model');
		$this->load->model('agent_social_media/agent_social_media_model', 'social_media_model');
		$this->load->model('agent_sites/agent_sites_model', 'site_model');
	}

	public function index() {
		$data['title'] = "AgentSquared";
		$data['js'] = array('landing_page/jquery.steps.js');
		$this->load->view('landing_page', $data);
	}

	public function testalvin() {
		#$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');
		$q = $this->db->query("SHOW VARIABLES LIKE 'max_allowed_packet'");
		$r = $q->result();
		var_dump($r[0]->Value);
	}

	public function about_us() {
		$data['title'] = "About Us";
		$this->load->view('landing_page/about_us', $data);
	}

	public function contact() {
		$data['title'] = "Contact";
		$this->load->view('landing_page/contact_us', $data);
	}

	public function support() {
		$data['title'] = "Support";
		$this->load->view('landing_page/support', $data);
	}

	public function howitworks() {
		$data['title'] = "How it works";
		$this->load->view('landing_page/howitworks', $data);
	}

	public function change_custom_domain() {
		$data['title'] = "Change Custom Domain";
		$this->load->view('change_custom_domain', $data);
	}
	
	public function register() {

		if($this->input->post('email') && $this->input->post('password')) {

			$user_id = $this->add_user();

			if($user_id) {
				$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');
				#$q = $this->db->query("SHOW VARIABLES LIKE 'max_allowed_packet'");
				#$r = $q->result();
				$this->add_idx($user_id);
				$this->add_theme($user_id);
				$this->add_social_media($user_id);
				//$this->add_domain_agent($user_id);
				$this->auto_login($user_id);
				$this->add_site_info($user_id);
				//$this->add_subdomain($user_id);

				if(isset($_POST["domain_id"]) && !empty($_POST["domain_id"]))
				{
					$this->reg_model->update_domain_user_id( $_POST["domain_id"], $user_id);
				}

				// send welcome email to realstate agent
				$this->send_notification_to_user( $user_id );			

				$this->sync_data_view($user_id);
			}
			else {
				redirect(base_url());
			}
		}
		else {
			redirect(base_url());
		}
	}

	public function send_notification_to_user( $user_id = NULL )
	{
	 	$this->load->library("email");

	 	$user = $this->reg_model->fetch_user($user_id);
		
		$email = $user[0]['email'];
		$first_name = $user[0]['first_name'];

	 	$content = "Hi ".$first_name.", <br><br>

			Thank you for signing up on Agent Squared. <br>

			You are on your way creating your own agent site with IDX integration. <br>

			Login URL: " . AGENT_DASHBOARD_URL . "login/auth/login  <br><br>

			Regards, <br>

			Agent Squared Team
	 	";

	 	$subject ="Welcome to Agent Squared" ;

	 	return $this->email->send_email(
	 		'automail@agentsquared.com',
	 		'admin',
	 		'rolbru12@gmail.com',
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			'recipient_name' => "Admin",
	 		)
	 	);
	}
	
	public function add_subdomain($user_id) {

		$user = $this->reg_model->fetch_user($user_id);
		$agent_info = Modules::run('agent_sites/property/get_account_info');

		if($agent_info) {
            $reg = array(" ",",",".");
            $first_name = str_replace($reg, "", $agent_info['FirstName']);
            $last_name = str_replace($reg, "", $agent_info['LastName']);
            $subdomain = strtolower($first_name)."".strtolower($last_name)."".".agentsquared.com"; 
			//$sub_domain = strtolower($agent_info['FirstName'])."-".strtolower($agent_info['LastName'])."-".$user[0]['code'].".agentsquared.com";
		}

		$arr = array('agent' => $user_id, 'domains' => $sub_domain, 'date_created' => date("Y-m-d h:m:s"), 'status' => 'registered', 'type' => 'agent_site_domain', 'is_subdomain' => 1);
		$this->reg_model->add_subdomain($arr);
	}

	/*Syncing Data Ajax Starts here*/
	public function sync_data_view($user_id) {
		$data['title'] = "Sync MLS Data";
		$data['user_id'] = $user_id;
		$this->load->view('landing_page/sync_loader', $data);
	}

	public function resynced($user_id) {
		$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');
		$data['title'] = "Resync MLS Data";
		$data['user_id'] = $user_id;
		$this->load->view('landing_page/resync', $data);
	}

	public function update_listings($user_id) {
		$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');
		$data['title'] = "Update MLS Data";
		$data['user_id'] = $user_id;
		$this->load->view('landing_page/update_listings', $data);
	}


	public function update_sync_status() {
		$id = $_POST['id'];
		$sync_status = $_POST['sync_status'];

		$data = array('sync_status' => $sync_status);
		$success = $this->reg_model->update_sync_status($id, $data);

		if($success) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}
	
	public function check_sync_status() {
		$email = $this->input->post('email');
		
		if($email) {
			$row = $this->reg_model->check_sync_status($email);
			if($row->sync_status == 0) {
				$response = array("success" => FALSE);
				exit;
			} else {
				$response = array("success" => TRUE);
				exit;
			}
		}
	}

	public function add_account_mls_meta() {
		$user_id = $_POST['user_id'];
		$data = array('user_id' => $user_id);

		$account_mls_meta = Modules::run('agent_sites/property/save_account_mls_meta', $data);

		if($account_mls_meta) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function update_account_mls_meta() {
		$user_id = $_POST['user_id'];
		$data = array('user_id' => $user_id);

		$check_mls_meta = Modules::run('agent_sites/property/check_account_mls_meta', $user_id);

		if($check_mls_meta) {
			$account_mls_meta = Modules::run('agent_sites/property/update_account_mls_meta', $data);
		}

		if($account_mls_meta) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function meta_custom_fields() {
		$user_id = $_POST['user_id'];
		$custom_fields = Modules::run('agent_sites/property/save_json_meta_custom_fields', $user_id);

		if($custom_fields) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function meta_field_groups() {
		$user_id = $_POST['user_id'];
		$field_groups = Modules::run('agent_sites/property/save_json_meta_field_groups', $user_id);

		if($field_groups) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function meta_field_order() {
		$user_id = $_POST['user_id'];
		$field_order= Modules::run('agent_sites/property/save_json_meta_field_order', $user_id);

		if($field_order) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function meta_property_types() {
		$user_id = $_POST['user_id'];
		$property_types = Modules::run('agent_sites/property/save_json_meta_property_types', $user_id);

		if($property_types) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function meta_rooms() {
		$user_id = $_POST['user_id'];
		$meta_rooms = Modules::run('agent_sites/property/save_json_meta_rooms', $user_id);

		if($meta_rooms) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function meta_standard_fields() {
		$user_id = $_POST['user_id'];
		$standard_fields = Modules::run('agent_sites/property/save_json_meta_standard_fields', $user_id);

		if($standard_fields) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function meta_units() {
		$user_id = $_POST['user_id'];
		$meta_units = Modules::run('agent_sites/property/save_json_meta_units', $user_id);

		if($meta_units) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function account_info() {
		$user_id = $_POST['user_id'];
		$account_info = Modules::run('agent_sites/property/save_json_account_info', $user_id);

		if($account_info) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function sync_saved_searches() {

		$saved_search =  Modules::run('agent_sites/saved_search_db');

		$response = array("success" => TRUE);
		exit(json_encode($response));
	}

	public function sync_contact_list() {

		$contact_list = Modules::run('crm/leads/SaveContactList');

		$response = array('success' => TRUE);
		exit(json_encode($response));
	}

	public function property_standard_listings_fields() {
		$user_id = $_POST['user_id']; 
		$property_counter = 1;
		$property = Modules::run('agent_sites/property/save_json_property_standard_listings_fields', array("user_id" => $user_id, "property_counter" => $property_counter ));

		if($property) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function resync_json_property_standard_listings_fields() {
		$user_id = $_POST['user_id']; 
		$property_counter = 1;
		$property = Modules::run('agent_sites/property/update_json_property_standard_listings_fields', array("user_id" => $user_id, "property_counter" => $property_counter));

		if($property) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function update_listings_json_property_standard_listings_fields() {
		$user_id = $_POST['user_id']; 
		$property_counter = 1;
		$property = Modules::run('agent_sites/property/update_listings_json_property_standard_listings_fields', array("user_id" => $user_id, "property_counter" => $property_counter));

		if($property) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	/*Syncing Data Ajax Ends Here*/

	public function add_user() {
		$code = generate_key(8);
		$user = array(
			// 'code' => $code,
			// 'ip_address' => $this->input->ip_address(),
			'email' 	=> $this->input->post('email'),
			'password' 	=> $this->input->post('password'),
			'phone' 	=> $this->input->post('phone'),
			'company' 	=> $this->input->post('company'),
			'about_agent' => $this->input->post('about_agent')
		);

		$user['code'] = $code;
		$user['ip_address'] = $this->input->ip_address();
		$id = $this->reg_model->insert_user($user);

		return $id;
	}

	public function add_idx($user_id) {
		$api_key = $this->input->post('api_key');
		$api_secret = $this->input->post('api_secret');

		$arr = array(
			'user_id' => $user_id,
			'api_key' => $api_key,
			'api_secret' => $api_secret
		);
		$this->idx_model->insert_idx($arr);
	}

	public function add_theme($user_id) {
		$arr = array(
			'id' => $user_id,
			'theme' => $this->input->post('theme'),
		);
		$this->theme_model->add_theme($arr);
	}

	public function add_site_info($user_id) {

		$agent_info = Modules::run('agent_sites/property/get_account_info');

		if($agent_info) {
			$first_name = $agent_info['FirstName'];
			$last_name = $agent_info['LastName'];
			$address = $agent_info['Addresses'][0]['Address'];
			$city = $agent_info['Addresses'][0]['City'];
			$zip = $agent_info['Addresses'][0]['PostalCode'];
			$phone = $agent_info['Phones'][0]['Number'];
			//$agent_photo = $agent_info['Images'][0]['Uri'];
			$company = $agent_info['Office'];
			$broker = $agent_info['Office'];
			$broker_number = $agent_info['Phones'][0]['Number'];
		}

		$arr = array(
			'id' => $user_id,
			'site_name' => $this->input->post('site_name'),
			'tag_line' => $this->input->post('tag_line'),
			'first_name' => $first_name,
			'last_name' => $last_name,
			'address' => $address,
			'city' => $city,
			'zip' => $zip,
			'phone' => $phone,
			//'agent_photo' => $agent_photo,
			'company' => $company,
			'broker' => $broker,
			'broker_number' => $broker_number,
		);

		if($_POST) {
			if($this->site_model->insert_info($arr)) {
				if($_FILES) {	
					$this->do_img_upload($user_id);
				}
			}
		}
	}

	public function add_social_media($user_id) {
		$facebook = $this->input->post('fb_link');
		$twitter = $this->input->post('tw_link');
		$linkedin = $this->input->post('ln_link');
		$googleplus = $this->input->post('gplus_link');

		$arr = array(
			'user_id' => $user_id,
			'facebook' => $facebook,
			'twitter' => $twitter,
			'linkedin' => $linkedin,
			'googleplus' => $googleplus,
		);

		$this->social_media_model->insert_link($arr);
	}

	public function add_domain_agent($user_id) {

		if($_POST['domain_name_holder'] != "") {
			$domain_name = $_POST['domain_name_holder'];
			$arr = array(
				'user_id' => $user_id,
				'domain_name' => $domain_name
			);
			$this->reg_model->add_agent_id_domain($arr);
		}
	}

	public function check_domain_exist() {

		$domain = $this->input->post("domain_name_holder");
		$exist = $this->reg_model->domain_exist($domain);

		if($exist) {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		} else {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		}
	}

	public function auto_login($user_id) {
		$user = $this->reg_model->fetch_user($user_id);
		$arr = array(
			'user_id' => $user[0]['id'],
			'email' => $user[0]['email'],
			'code' => $user[0]['code'],
			'identity' => $user[0]['email'],
			'old_last_login' => $user[0]['created_on'],
			'logged_in_auth' => true
		);
		$this->session->set_userdata($arr);
	}

	public function do_img_upload($user_id)
	{
		$this->load->library('upload');

		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		
		$files = $_FILES;
		$count = count($_FILES['userfile']['name']);

		for($i=0; $i < $count; $i++) { 
			if( $i == '0' ) {
				$config['upload_path'] = FCPATH.'assets/upload/logo';		
			} else {
				$config['upload_path'] = FCPATH.'assets/upload/favicon';		
			}
			$this->load->library('upload', $config);

			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$i];   
	        
			$this->upload->initialize($config); 

			if( $this->upload->do_upload() )
			{
				$img_data = $this->upload->data();
				if( $i == '0' )
				{
					$res = $this->site_model->insert_logo_info($img_data, $user_id);
				}
				else
				{
					$res = $this->site_model->insert_favicon_info($img_data, $user_id);		
				}

				if( $res )
				{
					$data = array('status' => TRUE);
				}

				$data = array('status' => FALSE);
			}
			else
			{
				
				$data = array('status' => FALSE);		
			}
		}
		return $data;
	}

	public function check_domain()
	{
		if(isset($_GET['domain_check']) AND $_GET['domain_check'] == 1) {

			$id = 'troymccas';
			$pw = 'b5n7RolJukp3';
			$d['status'] = 0;
			if(isset($_GET['domain_name'])) {
				$sld = trim($_GET['domain_name']);
				$tld = trim($_GET['suffix']);
				$apiurl = "https://reseller.enom.com/";

				//load xml document for to check if the domain is available or not
				$check = $apiurl.'interface.asp?command=check&sld='.$sld.'&tld='.$tld.'&responsetype=xml&uid='.$id.'&pw='.$pw;
				$xmlCheck = simplexml_load_file($check); //or die("Error: Cannot create object");
				//printA($xmlCheck); exit;
				$d['domainName'] = $xmlCheck->DomainName;
				$rrpCode = $xmlCheck->RRPCode;

				if($rrpCode == '210') {
					$d['status'] = 1;
					$getPrice = $apiurl . 'interface.asp?command=PE_GETPRODUCTPRICE&uid='.$id.'&pw='.$pw.'&ProductType=10&tld='.$tld.'&responsetype=xml';
					$getpricexml = simplexml_load_file($getPrice);

					$d['domainPrice'] = $getpricexml->productprice->price;
				}
				else {

					$d['domain'] 	= $sld;
					$d['tld']		= $tld;
				}
			}

			//printA($d); exit;
			echo json_encode($d);
			exit;
		}
	}

	public function available_domain()
	{
		if( isset($_GET['domain_tld_get']) AND $_GET['domain_tld_get'] == 1) {
			$d['status'] = 1;

			$id = 'troymccas';
			$pw = 'b5n7RolJukp3';
			$apiurl = "https://reseller.enom.com/";

			/* Recommended TLD's */
			$prioDomains = array(
				"com", 
			 	"net", 
			 	"me",  
			 	"org", 
			 	"us",  
			 	"info",
			 	"biz" 
			);
			$domainList = "DomainList=";
			foreach($prioDomains as $prioDomain) {
			 	$domainList .= $_GET['domain'].".".$prioDomain.",";
			}
			
			$getTLDs = $apiurl . 'interface.asp?Command=Check&responsetype=xml&uid='.$id.'&pw='.$pw.'&'.rtrim($domainList, ",");
			$tlds = simplexml_load_file($getTLDs);

			$domains = array();
			$dStatus = array();
			foreach($tlds as $key => $val) {
				$domains[] = $key . "_" . $val;
			}

			$d['domainCount'] = $tlds->DomainCount;

			$d['domains'] = $domains;
			$d['dstatus'] = $dStatus;

			$d['tlds'] = $tlds;

			echo json_encode($d); // $_GET['domain']
			exit;
		}
	}

	public function purchase_domain()
	{	
		$response = array("success" => FALSE);

		if(isset($_POST['domain_name'])) {

			$post_domain_name = $_POST['domain_name'];
            //$post['user_name'] = $this->session->userdata("user_id");
          	
            $result = $this->reg_model->add_purchase_domain($post_domain_name);

            if($result["success"]) 
            {
            	//purchase to ENUM API
	        	$id = 'troymccas';
				$pw = 'b5n7RolJukp3';
				$d['status'] = 0;
				if(isset($_POST['domain_name'])) { 
					$domain_name = explode(".", $_POST['domain_name']);
					$sld = trim($domain_name[0]);
					$tld = trim($domain_name[1]);
					$apiurl = "https://reseller.enom.com/";

					//load xml document for to check if the domain is available or not
					$check = $apiurl.'interface.asp?command=Purchase&sld='.$sld.'&tld='.$tld.'&responsetype=xml&uid='.$id.'&pw='.$pw;
					$xmlCheck = simplexml_load_file($check); //or die("Error: Cannot create object");
					//print_r($xmlCheck ); exit;
					$rrpCode = $xmlCheck->RRPCode;

					if($rrpCode == '200') {						
						//update to database API REST
						$res = $this->reg_model->update_agent_domain_status($result["domain_id"]);
					}
				}

				$response = array("domain_id" => $result["domain_id"], "success" => TRUE, 'message' => 'You have successfully reserved a domain name ['.$_POST["domain_name"].']. We will contact you as soon as possible once your domain is ready.');
            }
            else
            {
            	$response = array("success" => FALSE, 'error' => $result["error"]);
            }
		}
		exit(json_encode($response));
	}

	public function check_idx() {

		$api_key = $_POST['api_key'];
		$api_secret = $_POST['api_secret'];

		$idx = new API_Controller($api_key, $api_secret);

		if(!$idx->result) {
			$response = array("success" => FALSE);
			exit(json_encode($response));
		} else {
			$response = array("success" => TRUE);
			exit(json_encode($response));
		}
	}

	public function check_email($email) {
		return $this->signup_model->readEmail($email) ? true : false;
	}

	public function email_exist() {
		$email = $_POST['email'];
		echo $this->reg_model->readEmail($email) ? "1" : "0";
	}
}

?>
