<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span>Create/Import Reviews</span>
    </h3>
    <p>Read and approve testimonials that your clients leave for you.</p>
    
  </div>

  <section class="content">
    <?php $this->load->view('session/launch-view'); ?>
    <div class="flash-data">
        <?php $session = $this->session->flashdata('session');
            if( isset($session) )
            {
        ?>
            <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
        <?php } ?>

    </div>
    <a href="<?php echo base_url() . 'reviews'; ?>" class="btn btn-primary" style="margin-bottom:2rem;">Back to list</a>
    
    <div class="row">
      <div class="col-md-6">
        <form id="createReviewForm" action="<?php echo base_url() . '/reviews/create'; ?>" method="POST">
          <div class="form-group">
            <label for="name" class="form-label">Name</label>
            <input type="text" name="name" class="form-control" maxlength="60" value="<?php echo $this->input->post('name') ?>" />
          </div>
          <div class="form-group">
            <label for="email" class="form-label">Email</label>
            <input type="text" name="email" class="form-control" value="<?php echo $this->input->post('email') ?>" />
          </div>
          <!--<div class="form-group">
            <label for="phone" class="form-label">Phone</label>
            <input type="phone" name="phone" class="form-control phone_number"  value="<?php echo $this->input->post('phone') ?>"/>
          </div>-->
          <div class="form-group">
            <label for="phone" class="form-label">Rating</label>
            <select name="rating" class="form-control">
                <option value="1">1 Star</option>
                <option value="2">2 Stars</option>
                <option value="3">3 Stars</option>
                <option value="4">4 Stars</option>
                <option value="5" selected>5 Stars</option>
            </select>
          </div>
          <div class="form-group">
            <label for="subject" class="form-label">Subject</label>
            <input type="text" name="subject" class="form-control" maxlength="250" value="<?php echo $this->input->post('subject') ?>" />
          </div>
          <div class="form-group">
            <label for="message" class="form-label">Message</label>
            <textarea name="message" class="form-control" rows="5" cols="500" ><?php echo $this->input->post('message') ?></textarea>
          </div>
          <button type="submit" class="btn btn-primary" id="createReviewBtn">Save</button>
        </form>
      </div>
      <div class="col-md-1 text-center"><h3>--OR--</h3></div>
      <div class="col-md-5">
            <p>Please click download link for the format. <a target="_blank" href="<?php echo base_url() . 'assets/upload/reviews/sample-reviews.csv';?>">Download</a></p>
            <form action="<?php echo base_url() . '/reviews/create'; ?>"  enctype="multipart/form-data" method="POST">
                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon1">Select File</span>
                    <input type="file" name="file" class="form-control"  accept=".csv">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">Import</button>
                    </span>
                </div><!-- /input-group -->
                <p>Allowed format (csv only).</p>
                <input type="hidden" name="import" value="1">
            </form>
      </div>
    </div>
  </section>
</div>

<?php $this->load->view('session/footer'); ?>
