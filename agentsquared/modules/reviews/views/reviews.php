<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span>Reviews</span>
    </h3>
    <p>Read and approve testimonials that your clients leave for you.</p>
  </div>
  <section class="content">
    <?php $this->load->view('session/launch-view'); ?>
    <div class="row">
      <div class="col-md-12">
        <?php
          $message = $this->session->flashdata("message");
          if(!empty($message)) : ?>
            <div class="alert alert-success flash-data" role="alert"><?php echo $message; ?></div>
        <?php 
          endif;
        ?>
        <a class="btn btn-primary pull-right" href="<?php echo base_url() . '/reviews/create'; ?>" style="color:#FFF;">Create/Import</a>
        <ul class="nav nav-pills">
          <li class="active"><a data-toggle="pill" href="#published">Published</a></li>
          <li><a data-toggle="pill" href="#pending">Pending</a></li>
          <li><a data-toggle="pill" href="#archive">Archive</a></li>
        </ul>
        
        <div class="tab-content">
          <div id="published" class="tab-pane fade in active">
            <h3>Published Reviews</h3>
            <div class="table-responsive">
              <table class="table table-striped tablesorter" id="myTable">
                <thead>
                  <tr>
                    <th class="col-md-2">Name</th>
                    <th class="col-md-2">Email</th>
                    <th class="col-md-1">Rating</th>
                    <th class="col-md-2">Subject</th>
                    <th class="col-md-3">Message</th>
                    <th class="col-md-1">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  if(!empty($testimonials['publish'])) {
                    foreach($testimonials['publish'] as $pub) {
                      if ($pub->customer_id) {
                        $name = (isset($pub->first_name) && isset($pub->last_name)) ? $pub->first_name." ".$pub->last_name : '';
                        $email = $pub->email;
                      } else {
                        $name = $pub->name;
                        $email = $pub->email;
                      }
                      ?>
                      <tr id="review<?=$pub->id?>">
                        <td><?=$name; ?></td>
                        <td><?=$email; ?></td>
                        <td><div class="ratefixed" data-rate="<?=$pub->rating > 5 ? 5 : $pub->rating;?>"></div></td>
                        <td class="testimonial-message">
                          <p>
                            <?php if (strlen($pub->subject) > 30) : ?>
                            <?=substr(htmlentities($pub->subject), 0, 30);?> <a href="javascript:;" data-toggle="modal" data-target="#review-modal" data-title='<?=$pub->subject?>' data-content='<?=$pub->message;?>' class="message-modal">[...]</a>
                            <?php else: 
                              echo htmlentities($pub->subject);
                            endif;?>
                          </p>
                        </td>
                        <td class="testimonial-message">
                          <?php if (strlen($pub->message) > 30) : ?>
                            <p><?=substr(htmlentities($pub->message), 0, 30);?> <a href="javascript:;" data-toggle="modal" data-target="#review-modal" data-title='<?=$pub->subject?>' data-content='<?=$pub->message;?>' class="message-modal">[...]</a></p>
                          <?php else: 
                              echo htmlentities($pub->subject);
                            endif;?>
                        </td>
                        <td class="text-center">
                          <form action="<?=base_url();?>reviews/update_status" method="POST" style="display: inline;">
                            <input type="hidden" name="id" value="<?=$pub->id?>">
                            <input type="hidden" name="update_type" value="archive">
                            <button type="submit" class="btn btn-warning" data-toggle="tooltip" title="Don't display this review to testimonials page.">
                              <i class="fa fa-archive"></i> Move to archive
                            </button>
                          </form>
                        </td>
                      </tr>
                <?php
                    }
                  }
                ?>
                </tbody>
              </table>
            </div>
          </div>
          <div id="pending" class="tab-pane fade">
            <h3>Pending Reviews</h3>
            <div class="table-responsive">
              <table class="table table-striped tablesorter" id="myTable">
                <thead>
                  <tr>
                    <th class="col-md-2">Name</th>
                    <th class="col-md-2">Email</th>
                    <th class="col-md-1">Rating</th>
                    <th class="col-md-2">Subject</th>
                    <th class="col-md-3">Message</th>
                    <th class="col-md-1">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  if(!empty($testimonials['pending'])) {
                    foreach($testimonials['pending'] as $pen) { ?>
                      <tr id="review<?=$pen->id?>">
                        <td><?=(isset($pen->first_name) && isset($pen->last_name)) ? $pen->first_name." ".$pen->last_name : ''?></td>
                        <td><?=isset($pen->email) ? $pen->email : ''?></td>
                        <td><div class="ratefixed" data-rate="<?=$pen->rating?>"></div></td>
                        <td class="testimonial-message">
                          <p>
                            <?php if (strlen($pen->subject) > 30) : ?>
                            <?=substr(htmlentities($pen->subject), 0, 30);?> <a href="javascript:;" data-toggle="modal" data-target="#review-modal" data-title='<?=$pen->subject?>' data-content='<?=$pen->message;?>' class="message-modal">[...]</a>
                            <?php else: 
                              echo htmlentities($pen->subject);
                            endif;?>
                          </p>
                        </td>
                        <td class="testimonial-message">
                          <?php if (strlen($pen->message) > 30) : ?>
                            <p><?=substr(htmlentities($pen->message), 0, 30);?> <a href="javascript:;" data-toggle="modal" data-target="#review-modal" data-title='<?=$pen->subject?>' data-content='<?=$pen->message;?>' class="message-modal">[...]</a></p>
                          <?php else: 
                              echo htmlentities($pen->subject);
                            endif;?>
                        </td>
                        <td class="text-center">
                          <form action="<?=base_url();?>reviews/update_status" method="POST" style="display: inline;">
                            <input type="hidden" name="id" value="<?=$pen->id?>">
                            <input type="hidden" name="update_type" value="publish">
                            <button type="submit" class="btn btn-success" data-toggle="tooltip" title="Publish this review to your testimonials page.">
                              <i class="fa fa-upload"></i> Publish this review
                            </button>
                          </form>
                        </td>
                      </tr>
                <?php
                    }
                  }
                ?>
                </tbody>
              </table>
            </div>
          </div>
          <div id="archive" class="tab-pane fade">
            <h3>Archived Reviews</h3>
            <div class="table-responsive">
              <table class="table table-striped tablesorter" id="myTable">
                <thead>
                  <tr>
                    <th class="col-md-2">Name</th>
                    <th class="col-md-2">Email</th>
                    <th class="col-md-1">Rating</th>
                    <th class="col-md-2">Subject</th>
                    <th class="col-md-3">Message</th>
                    <th class="col-md-1">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  if(!empty($testimonials['archive'])) {
                    foreach($testimonials['archive'] as $arc) { ?>
                      <tr id="review<?=$arc->id?>">
                        <td><?=(isset($arc->first_name) && isset($arc->last_name)) ? $arc->first_name." ".$arc->last_name : ''?></td>
                        <td><?=(isset($arc->email)) ? $arc->email : ''?></td>
                        <td><div class="ratefixed" data-rate="<?=$arc->rating?>"></div></td>
                        <td class="testimonial-message">
                          <p>
                            <?php if (strlen($arc->subject) > 30) : ?>
                            <?=substr(htmlentities($arc->subject), 0, 30);?> <a href="javascript:;" data-toggle="modal" data-target="#review-modal" data-title='<?=$arc->subject?>' data-content='<?=$arc->message;?>' class="message-modal">[...]</a>
                            <?php else: 
                              echo htmlentities($arc->subject);
                            endif;?>
                          </p>
                        </td>
                        <td class="testimonial-message">
                          <?php if (strlen($arc->message) > 30) : ?>
                            <p><?=substr(htmlentities($arc->message), 0, 30);?> <a href="javascript:;" data-toggle="modal" data-target="#review-modal" data-title='<?=$arc->subject?>' data-content='<?=$arc->message;?>' class="message-modal">[...]</a></p>
                          <?php else: 
                              echo htmlentities($arc->subject);
                            endif;?>
                        </td>
                        <td class="text-center">
                          <form action="<?=base_url();?>reviews/update_status" method="POST" style="display: inline;">
                            <input type="hidden" name="id" value="<?=$arc->id?>">
                            <input type="hidden" name="update_type" value="restore">
                            <button type="submit" class="btn btn-warning" data-toggle="tooltip" title="Don't display this review to testimonials page.">
                              <i class="fa fa-window-restore"></i> Restore this review
                            </button>
                          </form>
                        </td>
                      </tr>
                <?php
                    }
                  }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!--<div class="table-responsive">
          <table class="table table-striped tablesorter" id="myTable">
            <thead>
              <tr>
                <th class="col-md-1">Status</th>
                <th class="col-md-2">Name</th>
                <th class="col-md-2">Email</th>
                <th class="col-md-1">Rating</th>
                <th class="col-md-2">Subject</th>
                <th class="col-md-3">Message</th>
                <th class="col-md-1">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php
              if($testimonials) {
                foreach($testimonials as $review) { ?>
                  <tr id="review<?=$review->id?>">
                    <td><?=($review->is_approved) ? "Approved" : "Pending";?></td>
                    <td><?=$review->first_name." ".$review->last_name?></td>
                    <td><?=$review->email?></td>
                    <td><div class="ratefixed" data-rate="<?=$review->rating?>"></div></td>
                    <td class="testimonial-message">
                      <p>
                        <?php
                          echo htmlentities($review->subject);
                          //$subject = strip_tags($review->subject);
                          //echo $subject;
                        ?>  
                      </p>
                    </td>
                    <td class="testimonial-message">
                      <p class="col-md-8">
                        <?php
                          echo htmlentities($review->message);
                          //$message = strip_tags($review->message);
                          //echo $message;
                        ?>
                      </p>
                      <a href="javascript:;" data-toggle="modal" data-target="#review-modal" data-title='<?=$review->subject?>' data-content='<?=$review->message;?>' class="message-modal col-md-4">read more</a>
                    </td>
                    <td class="text-center">
                      <?php
                        if($review->is_approved) { ?>
                          <form action="<?=base_url();?>reviews/update_status" method="POST" style="display: inline;">
                            <input type="hidden" name="id" value="<?=$review->id?>">
                            <button type="submit" class="btn btn-warning" data-toggle="tooltip" title="Don't display this review to testimonials page.">
                              <span class="glyphicon glyphicon-remove"></span> 
                            </button>
                            <button type="button" class="btn btn-danger delete_review_btn" data-action="<?=base_url();?>reviews/remove" data-id="<?=$review->id?>" data-toggle="tooltip" title="Delete this review.">
                              <span class="glyphicon glyphicon-trash"></span> 
                            </button>
                          </form>
                      <?php
                        } else { ?>
                          <form action="<?=base_url();?>reviews/approve" method="POST" style="display: inline;">
                            <input type="hidden" name="id" value="<?=$review->id?>">
                            <button type="submit" class="btn btn-success" data-toggle="tooltip" title="Display this review to testimonials page.">
                              <span class="glyphicon glyphicon-ok"></span> 
                            </button>
                            <button type="button" class="btn btn-danger delete_review_btn" data-action="<?=base_url();?>reviews/remove" data-id="<?=$review->id?>" data-toggle="tooltip" title="Delete this review.">
                              <span class="glyphicon glyphicon-trash"></span> 
                            </button>
                          </form>
                      <?php
                        }
                      ?>
                    </td>
                  </tr>
            <?php
                }
              }
            ?>
            </tbody>
          </table>
        </div>-->
      </div>
    </div>
  </section>
  <!-- Modal -->
  <div class="modal fade" id="review-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="text-center review-subject"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <p class="message-content">
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('session/footer'); ?>
