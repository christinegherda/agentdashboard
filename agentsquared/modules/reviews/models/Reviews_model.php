<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Reviews_model extends CI_Model { 

	public function __construct()
    {
        parent::__construct();
    }
    public function add_columns() {
        $query = $this->db->query("ALTER TABLE testimonials ADD COLUMN name VARCHAR(120) NULL after customer_id, ADD COLUMN email VARCHAR(120) NULL after customer_id, ADD COLUMN phone VARCHAR(30) NULL after customer_id, ADD COLUMN ip VARCHAR(30) NULL");
        return $query;
    }
    
    public function fetch_testimonials($status=array()) {

        if(!empty($status)) {

            foreach ($status as $key) {
                
                if($key == 'publish') {
                    $this->db->select("t.*, IF(t.customer_id = 0, t.name, c.first_name) as first_name, IF(t.customer_id = 0, t.name, c.last_name) as last_name, IF(t.customer_id = 0, t.email, c.email) as email")
                            ->join("contacts c", "t.customer_id=c.contact_id", "left")
                            ->where('t.user_id', $this->session->userdata('user_id'))
                            ->where('t.is_approved', 1)
                            ->where('t.is_archive', 0)
                            ->order_by("t.id", "desc");
                    $data['publish'] = $this->db->get('testimonials t')->result();
                }

                if($key == 'pending') {
                    $this->db->select("t.*, IF(t.customer_id = 0, t.name, c.first_name) as first_name, IF(t.customer_id = 0, t.name, c.last_name) as last_name, IF(t.customer_id = 0, t.email, c.email) as email")
                            ->join("contacts c", "t.customer_id=c.contact_id", "left")
                            ->where('t.user_id', $this->session->userdata('user_id'))
                            ->where('t.is_approved', 0)
                            ->where('t.is_archive', 0)
                            ->order_by("t.id", "desc");
                    $data['pending'] = $this->db->get('testimonials t')->result();
                    
                }

                if($key == 'archive') {
                    $this->db->select("t.*, IF(t.customer_id = 0, t.name, c.first_name) as first_name, IF(t.customer_id = 0, t.name, c.last_name) as last_name, IF(t.customer_id = 0, t.email, c.email) as email")
                            ->join("contacts c", "t.customer_id=c.contact_id", "left")
                            ->where('t.user_id', $this->session->userdata('user_id'))
                            ->where('t.is_approved', 0)
                            ->where('t.is_archive', 1)
                            ->order_by("t.id", "desc");
                    $data['archive'] = $this->db->get('testimonials t')->result();
                    
                }

            }

            return (isset($data) && !empty($data)) ? $data : FALSE;

        }

        return FALSE;

    }

    public function fetch_approved_reviews($param=array()) {
        
    	$this->db->select("t.*, c.first_name, c.last_name, c.email")
    			->from("testimonials t")
    			->join("contacts c", "t.customer_id=c.id", "left")
    			->where('t.user_id', $this->session->userdata('user_id'))
    			->where('t.is_approved', 1);

        if(!empty($param["limit"])) { 
                $this->db->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);
        }

        $this->db->order_by("t.id", "desc");

    	$query = $this->db->get();

    	return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function count_approved_reviews() {
        $this->db->select("*")
                ->from("testimonials")
                ->where("is_approved", 1);

        return $this->db->count_all_results();
    }

    public function sort_by_highest($param=array()) {
        $this->db->select("t.*, c.first_name, c.last_name, c.email")
                ->from("testimonials t")
                ->join("contacts c", "t.customer_id=c.id", "left")
                ->where('t.user_id', $this->session->userdata('user_id'))
                ->where('t.is_approved', 1)
                ->order_by("t.rating","desc");

        if(!empty($param["limit"])) { 
                $this->db->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);
        }

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function sort_by_lowest($param=array()) {
        $this->db->select("t.*, c.first_name, c.last_name, c.email")
                ->from("testimonials t")
                ->join("contacts c", "t.customer_id=c.id", "left")
                ->where('t.user_id', $this->session->userdata('user_id'))
                ->where('t.is_approved', 1)
                ->order_by("t.rating");

        if(!empty($param["limit"])) { 
                $this->db->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);
        }

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function sort_by_newest($param=array()) {
        $this->db->select("t.*, c.first_name, c.last_name, c.email")
            ->from("testimonials t")
            ->join("contacts c", "t.customer_id=c.id", "left")
            ->where('t.user_id', $this->session->userdata('user_id'))
            ->where('t.is_approved', 1)
            ->order_by("t.date_created", "desc");

        if(!empty($param["limit"])) { 
                $this->db->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);
        }

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function sort_by_oldest($param=array()) {
        $this->db->select("t.*, c.first_name, c.last_name, c.email")
                ->from("testimonials t")
                ->join("contacts c", "t.customer_id=c.id", "left")
                ->where('t.user_id', $this->session->userdata('user_id'))
                ->where('t.is_approved', 1)
                ->order_by("t.date_created");

        if(!empty($param["limit"])) { 
                $this->db->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);
        }

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function changed_status($id=NULL, $data=array()) {

    	$ret = FALSE;

    	if($id && $data) {
            
    		$this->db->where('id',$id);
    		$this->db->update('testimonials', $data);

 			$ret = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    	}

    	return $ret;
    }

    public function delete_testimonial() {
    	
        //$this->db->where('id', $this->input->post('id'));
		////$this->db->delete('testimonials');
		//return ($this->db->affected_rows() > 0) ? TRUE : FALSE;

    }


    public function save_testimonial($data=array()) {
        $ret = FALSE;
        if($data) {
            $this->db->insert('testimonials', $data);
            $ret = TRUE;
        }
        return $ret;
    }
}