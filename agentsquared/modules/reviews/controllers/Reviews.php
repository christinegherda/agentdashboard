<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends MX_Controller {

	function __construct() {
		parent::__construct();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

        $this->load->model('reviews_model', 'review');
        $this->load->model("agent_sites/Agent_sites_model", "domain");
	}

	function index() {

		$data['title'] = "Reviews";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["testimonials"] = $this->review->fetch_testimonials(array('publish','pending','archive'));
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		$data["js"] = array("reviews/reviews.js");
		
		$this->load->view('reviews', $data);
	}

	function approve() {

		if($this->input->post('id')) {

			$data = array('is_approved'=>1);

			$status = $this->review->changed_status($this->input->post('id'), $data);

			if($status)
				$this->session->set_flashdata("message", "<p>Testimonial approved and can now be seen in home page!</p>" );

		}

		redirect('reviews');
	}

	function remove() {

		if($this->input->is_ajax_request()) {

			$response["success"] = FALSE;

			if($this->input->post('id')) {

				$status = $this->review->delete_testimonial();
				$response["success"] = ($status) ? TRUE : FALSE;
			
			}

			exit(json_encode($response));

		}

	}

	function update_status() {

		if($this->input->post('id') && $this->input->post('update_type')) {

			$data=array();

			if($this->input->post('update_type') == 'publish') { //publish review to testimonials page
				$data['is_approved'] = 1;
				$msg = 'Review successfully published to your testimonials page.';
			} elseif($this->input->post('update_type') == 'restore') { //archive move to pending
				$data['is_approved'] = 1;
				$data['is_archive'] = 0;
				$msg = 'Review successfully restored.';
			} elseif($this->input->post('update_type') == 'archive') { //publish move to archive
				$data['is_approved'] = 0;
				$data['is_archive'] = 1;
				$msg = 'Review successfully moved to archive.';
			}

			$status = $this->review->changed_status($this->input->post('id'), $data);

			if($status) {
				$this->session->set_flashdata("message", "<p>".$msg."</p>" );
			}

		}

		redirect('reviews');
	}

	function fetch_approved_reviews($param=array()) {
		$result = $this->review->fetch_approved_reviews($param);
		return ($result) ? $result : FALSE;
	}
	function compute_average_review() {
		$param["offset"] = 0;
		$param["limit"] = 0;

		$result = $this->fetch_approved_reviews($param);
		$count=0;
		$total=0;
		$average=0;

		foreach($result as $res) {
			$total+=$res->rating;
			$count++;
		}

		$average = $total/$count;
		$data = array('average'=>round($average), 'total'=>$count);

		return $data;
	}
	function get_sort_reviews($param=array()) {
		if($this->input->get('reviews_order')) {
			$sort_by = $this->input->get('reviews_order');
			switch ($sort_by) {
			    case "highest":
			    	$sort = $this->review->sort_by_highest($param);
			    	return ($sort) ? $sort : FALSE;
			        break;
			    case "lowest":
			    	$sort = $this->review->sort_by_lowest($param);
			    	return ($sort) ? $sort : FALSE;
			        break;
			    case "newest":
			    	$sort = $this->review->sort_by_newest($param);
			    	return ($sort) ? $sort : FALSE;
			        break;
			   	case "oldest":
			   		$sort = $this->review->sort_by_oldest($param);
			    	return ($sort) ? $sort : FALSE;
			   		break;
			    default:
			    	$sort = $this->review->fetch_approved_reviews($param);
					return ($sort) ? $result : FALSE;
			}
		}
	}

	/**
	 * create()
	 * Create/Import existing reviews/testimonials
	 * 
	 * @param none
	 * @return none
	 */
	public function create() {

		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			try {
				//Check if post is import
				if ($this->input->post('import') && isset($_FILES['file']['tmp_name'])) {
					
					$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
					
					if (!in_array($_FILES['file']['type'], $mimes)) {
						$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Invalid file type. accept only CSV file.'));
						redirect('reviews/create');
					}
					$this->load->library('csvimport');
					$flag = false;
					if ($csv_array =  $this->csvimport->get_array($_FILES['file']['tmp_name'])) {
						if ($csv_array) {
							foreach ($csv_array as $row) {
								//Check if Name/Subject/Message is empty
								if (
									(!isset($row['Name'])) 
									|| (!isset($row['Message'])) 
									|| (!isset($row['Rating'])) 
									|| (isset($row['Name']) && empty($row['Name'])) 
									|| (isset($row['Message']) && empty($row['Message']))
									|| (isset($row['Rating']) && empty($row['Rating']))
								) {continue;}
									
								$arr = array(
									'user_id' => $this->session->userdata("user_id"),
									'customer_id' => 0,
									'name' => $row['Name'],
									'email' => $row['Email'],
									'phone' => $row['Phone'],
									'rating' => $row['Rating'] > 5 ? 5 : $row['Rating'],
									'subject' => $row['Subject'],
									'message' => $row['Message'],
									'date_created' => date('Y-m-d H:i:s'),
									'is_approved' => 0, //Set to 1 because it comes from old website
								);
								if ($this->review->save_testimonial($arr)) {
									//Set flag if there has data that's being imported
									$flag = true;
								}

							}

							unlink($file_path);
						}
					}
					if ($flag) {
						$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Reviews successfully imported but the status is still pending!'));
						redirect('reviews/create');
					} else {
						$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Unable to import reviews!'));
					}

				} else {

					$this->form_validation->set_rules('name', 'Name', 'required|trim');
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
					$this->form_validation->set_rules('message', 'Message', 'required|trim');
					$this->form_validation->set_rules('subject', 'Subject', 'required|trim');

					if($this->form_validation->run() == true) {

						$data = array(
							'user_id' => $this->session->userdata("user_id"),
							'customer_id' => 0,
							'name' => $this->input->post('name'),
							'email' => $this->input->post('email'),
							// 'phone' => $this->input->post('phone'),
							'rating' => $this->input->post('rating') > 5 ? 5 : $this->input->post('rating'),
							'subject' => $this->input->post('subject'),
							'message' => $this->input->post('message'),
							'date_created' => date('Y-m-d H:i:s'),
							'is_approved' => 0, //Set to 1 because it comes from old website
						);
						$insert = $this->review->save_testimonial($data);
						if($insert) {
							$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Testimonial Successfully Saved but the status is still pending!'));
							redirect('reviews/create');
						}
					} else {
						$this->session->set_flashdata('session' , array('status' => False,'message' => 'Please check required fields!'));
					}
				}
				
			} catch(\Exception $e) {
				$this->session->set_flashdata('session' , array('status' => TRUE,'message' => $e->getMessage()));
			}
			

		}


		$data = array();
		$data['title'] = "Reviews";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data["js"] = array("reviews/reviews.js");
		$data['branding'] = $siteInfo['branding'];
		
		$this->load->view('create', $data);
	}

	public function add_columns() {
		$result = $this->review->add_columns();
		printA($result);
		die;
	}
}
