<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Saved Search</span>
    </h3>
  </div>
    <!-- Main content -->
    <section class="content">
        <?php $this->load->view('session/launch-view'); ?>
        <div class="row">
            <?php if(isset($savedsearches) && !empty($savedsearches)) {?>
                <div class="col-md-12">                         
                        <?php $session = $this->session->flashdata('session');                                  
                            if( isset($session) )
                            {
                        ?>
                            <div class="alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>  
                        <?php } ?>
                    
                 </div>
                <div class="col-md-12">
                    <div class="meeting-list">
                        <table class="table-responsive tablesorter table table-striped" id="savedSearchTable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Savedsearch Id</th>
                                    <th>Modification Date</th>
                                    <th>Links</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($savedsearches) ) {
                                    foreach( $savedsearches as $savedsearch) {?> 
                                        <tr>
                                            <td><?php echo (isset($savedsearch["Name"])) ? $savedsearch["Name"]: ""; ?>  

                                            <?php if(isset($selected_saved_search) && !empty($selected_saved_search)){
                                                foreach($selected_saved_search as $selected){?>

                                                    <?=($selected['Id'] == $savedsearch["Id"]) ? "<span  style='font-size:11px'; class='label label-primary'>Selected Savedsearch</span>" : "" ?> 
                                                <?php }
                                            } ?>

                                            <?=(isset($featured_saved_search['Id']) && $featured_saved_search['Id'] == $savedsearch["Id"]) ? "<span style='font-size:11px'; class='label label-primary'>Featured Savedsearch</span>" : "" ?>
                                                
                                            </td>
                                            <td><?php echo $savedsearch["Id"];?></td>
                                            <td><?php echo date("F d, Y H:m:s",strtotime($savedsearch["ModificationTimestamp"]))?></td>  
                                            <td><a href="<?php echo site_url('agent_sites/saved_search?search_id='.$savedsearch["Id"]); ?>">View Search</a></td>
                                           <!--  <td><a href="<?php echo site_url('agent_sites/edit_saved_search/'.$savedsearch["Id"]); ?>" title="Edit Save Name" data-search-name="<?=$savedsearch["Name"];?>" data-search-id="<?=$savedsearch["Id"];?>" id="edit_search" class="edit_search"  ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> </a></td>   --> 
                                        </tr>
                                    <?php } ?>
                                    <?php } else {?>
                                    <tr align="center"><td colspan="10"><i>no records found!</i></td></tr>
                                <?php } ?>
                                
                            </tbody>
                        </table>

                        <div class="col-md-12">
                            <?php if( isset($pagination) ) : ?>
                                <div class="pagination-area pull-right">
                                    <nav>
                                        <ul class="pagination">
                                            <?php echo $pagination; ?>
                                        </ul>
                                    </nav>
                                </div>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            <?php } else { 
                if(!empty($live_savedsearches)){?>
                <div class="col-md-12">
                    <h4 id="saved_search_tagline">You haven't saved your saved searches yet. Please click the button below.</h4>
                    <button type="button" class="btn btn-default btn-submit" ><a style="color:#fff" href="<?php echo base_url()?>dsl/syncSavedSearches">Sync My Saved Searches</a></button>
                </div>
            <?php } else {?>
                <h4 class="text-center">No Saved Search Found!</h4>

           <?php }
            } ?> 
        </div>
    </section>
</div>
<div id="edit-search-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Search Name </h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="sbt-edit-search-name" class="sbt-edit-search-name" >
                    <div class="form-group">
                        <label for="exampleInputEmail1">Search  Name</label>
                        <input type="text" name="search_name" class="form-control search_name" id="search_name" placeholder="Name">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $this->load->view('session/footer'); ?>

<script type="text/javascript">
    
    $(".edit_search").on( "click", function(e) {
        e.preventDefault();
       
        $(".search_name").val($(this).data("search-name"));
        $("#sbt-edit-search-name").attr("action", $(this).attr("href") );
        $("#edit-search-modal").modal("show");

    }) ;

  $(document).ready(function() 
    { 
      $("#savedSearchTable").tablesorter( ); 
    } 
  ); 

        
</script>