
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                      <strong><h3>Insert Media</h3></strong>
                  </div>

                  <div class="modal-body addMediaModal">
                      <div class="row">
                          <div class="col-md-9">
                              <form id="insertMedia" action="<?php echo base_url()?>pages/insertMedia" method="POST">
                                      <!-- <div class="col-md-9 modal-media"> -->

                                        <?php if(!empty($media)){

                                                if(!empty($published_media)) { ?>

                                                  <ul class="list-item">

                                                    <?php foreach ($published_media as $media){?> 

                                                        <li class="list-item-grid">
                                                          <div class="media-grid-actions">
                                                              <span class="checked">
                                                                  <i class="fa fa-check"></i>
                                                              </span>
                                                              <div class="ischeckbox" data-id="media<?=$media->id?>">
                                                                  <input type="checkbox" class="modal-checkbox" name="bulk_insertMedia" value="<?php echo $media->id ?>">
                                                              </div>
                                                          </div>

                                                            <?php if($media->file_type === "application/pdf" ){?>

                                                                <div class="item-grid media<?=$media->id?>">
                                                                    <img src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?php echo $media->alt_text?>">
                                                                     <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : base_url()."assets/upload/file/".$media->file_name?>" target="_blank"><img style="width:15%" src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>"></a>'/>
                                                                      
                                                                </div>

                                                               <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.word") { ?>

                                                                 <div class="item-grid media<?=$media->id?>">
                                                                      <img src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?php echo $media->alt_text?>">
                                                                       <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : base_url()."assets/upload/file/".$media->file_name?>" target="_blank"><img style="width:15%" src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>'/>
                                                                       
                                                                  </div>
                                                                
                                                               <?php } else {?>

                                                               <div class="item-grid media<?=$media->id?>">
                                                                <img src="<?php echo base_url()?>assets/upload/file/<?=$media->file_name?>" alt="<?php echo $media->alt_text?>"> <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : base_url()."assets/upload/file/".$media->file_name?>" target="_blank"><img style="width:60%" src="<?php echo base_url()?>assets/upload/file/<?php echo $media->file_name?>" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>' />

                                                              </div>

                                                               <?php }?>
                                                          
                                                        </li>

                                                <?php }?>

                                                </ul>

                                             <?php } 

                                          }  else { ?>

                                          <div class="no-media-uploaded">

                                             <p class="no-media">No <strong><?php echo isset($_GET["keywords"]) ?  ucwords($_GET["keywords"]) : ""?></strong> files found!<br><br>

                                               <?php if(isset($_GET["keywords"])){?>
                                                        <a href="<?php echo site_url("media?mode=grid"); ?>" class="btn btn-default btn-submit">Back to Media Library</a>
                                                <?php } else {?>
                                                         <a href="<?php echo site_url("media/add_media"); ?>" class="btn btn-default btn-submit">Upload Media</a>

                                                 <?php } ?>
                                            </p>
                                            
                                          </div>

                                        <?php } ?>
                                      <!-- </div> -->
                                     
                                      
                                      <?php if( $pagination ) : ?>
                                          <div class="pagination-area pull-right">
                                              <nav>
                                                  <ul class="pagination">
                                                      <?php echo $pagination; ?>
                                                  </ul>
                                              </nav>
                                          </div>
                                      <?php endif; ?>
                              </form>
                          </div>      
                          <div class="col-md-3 pr-30 image-details">
                              
                                  <h3>Image Details</h3>

                                  <div class="show-notif"></div>

                                    <?php if(!empty($published_media)){

                                         foreach ($published_media as $media){?> 

                                              <div id="media<?=$media->id?>" class="media-details hide-media">

                                               <form id="updateMediaForm" class="updateMedia" action="<?php echo base_url()?>pages/update_details/<?=$media->id?>" method="POST">

                                               <input type="hidden" name="media_id" value="<?=$media->id?>">
                                               <input type="hidden" name="page_id" value="site_info">
                                               <input type="hidden" name="file_name" value="<?=$media->file_name?>">
                                                   <p><label for="media-title">Title</label></p>
                                                  <input type="text" name="media_title" id="media-title" value="<?php echo (isset($media->title)) ? $media->title : "" ?>" class="form-control">

                                                  <?php if($media->file_type === "application/pdf"){?>

                                                      <p class="media-icon"><img src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

                                                 <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.word") { ?>

                                                      <p class="media-icon"><img src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

                                                 <?php } else {?>

                                                      <p class="media-icon"><img src="<?php echo base_url()?>assets/upload/file/<?php echo $media->file_name?>" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

                                                 <?php }?>

                                                 <label for="media-link">Image Link</label>
                                                  <input type="text" name="media_link" id="media-link" value="<?php echo (isset($media->link)) ? $media->link : "" ?>" class="form-control"> <br>

                                                  <label for="media-caption">Caption</label>
                                                  <textarea name="media_caption" id="media-caption" class="form-control"><?php echo (isset($media->caption)) ? $media->caption : "" ?></textarea><br>

                                                  <label for="media-alt-text">Alternative Text</label>
                                                  <input type="text" name="media_alt_text" id="media-alt-text" value="<?php echo (isset($media->alt_text)) ? $media->alt_text : "" ?>" class="form-control"> <br>
                          
                                                  <label for="media-description">Description</label>
                                                  <textarea name="media_description" id="media-description" class="form-control"><?php echo (isset($media->description)) ? $media->description : "" ?></textarea><br>

                                                  <button type="submit" class="btn btn-default btn-submit">Update</button>
                                                   </form>
                                              </div>
                                      <?php }
                                  }?>

                          </div>      
                      </div>
                  </div>

                  <div class="modal-footer">
                      <button type="submit" id="insertIntoPage" class="btn btn-default btn-submit pull-left">Insert Into Page</button>
                  </div>

              </div>
          </div>

       <script>

            $(document).ready(function(){

              $("#insertIntoPage").on("click", function() {
                // e.preventDefault();
                $dataString = $("#insertMedia").serialize();
                console.log($("#insertMedia").attr('action'));

                $.ajax({
                    type: 'POST',
                    url: $("#insertMedia").attr('action'),
                    data: $dataString,
                    dataType: 'json',
                    success: function(data) {
                        if(data.success) {
                            //$('.note-editable').append('<p>'+data.mediaUrl+'</p>');

                            $('.summernote').summernote('editor.pasteHTML', data.mediaUrl);
                            $('.insertMediaModal').modal('hide');

                            // reset selected media
                            $(".modal-checkbox").prop('checked',false);
                            $(".list-item-grid").removeClass("media-selected");
                            $(".checked").hide();
                        }
                    }
                });

            });

            $('.media-grid-actions .ischeckbox').on("click", function (e) { 
                  e.preventDefault();
                   var checkbox = $(this).find(':checkbox');
                    $(".modal-checkbox").prop('checked',false);
                    $(".list-item-grid").removeClass("media-selected");
                    $(".checked").hide();
                    checkbox.prop('checked', true);
                    checkbox.parent().parent().parent().addClass("media-selected");
                    checkbox.parent().parent().find(".checked").show();
             });

              // set min-height for modal
              // var window_height = $(window).height();
              // var content_height = parseInt($(".content-wrapper").css("min-height"));
              // $(".insertMediaModal .modal-body").css('min-height', window_height-153);
              // $("#agentinfo-mediamodal").css('min-height', content_height-394);



              $(".hide-media").hide();
              //check media to add page
              $(".ischeckbox").click(function() {    
                  $(".hide-media").hide();
                  var dataType = $(this).attr('data-id');
                  $("#" + dataType).show();
              });

              //update media details
              $(".updateMedia").on("submit", function(e) {
                e.preventDefault();

                var dataString = $(this).serialize();
                console.log(dataString);

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: dataString,
                    dataType: 'json',
                    success: function(data) {
                       console.log(data);
                        if(data.success) {

                              $(".show-notif").html(data.message);
                              $(".show-notif").show().delay(2000).fadeOut(1000);
                              window.location.href = data.redirect;
                              //$('.media'+data.media_id+'').html(data.html_data);

                          } else {
                              $(".show-notif").html(data.message);
                          }
                    }
                });

              });    
              
            });

        </script>