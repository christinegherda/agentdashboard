<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>

<!-- Full Width Column -->
<div class="content-wrapper">
    <div class="page-title">
        <div class="row">
            <div class="col-md-7 col-sm-6">
                <h3 class="heading-title">
                    <span> Agent Site Info</span>
                </h3>
                <h4>Enter your site's details, logo and agent info</h4>
            </div>
            <div class="col-md-5 col-sm-6 col-md-3 pull-right">
                <div class="otherpage video-box video-collapse">
                    <div class="row">
                      <div class="col-md-5 col-sm-5 col-xs-5">
                        <h1>Need Help?</h1>
                        <p>See how by watching this short video.</p>
                        <div class="checkbox">
                            <input type="checkbox" id="hide" name="hide" value="hide">
                            <label for="hide">Minimize this tip.</label>
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="text-center">
                          <span class="fa fa-long-arrow-right"></span>
                        </div>
                      </div>
                      <div class="col-md-7 col-sm-7 col-xs-7">
                       <div class="tour-thumbnail tour-video">
                         <img src="<?php echo base_url()."assets/images/dashboard/v_agentsite.png"; ?>" alt="Google Analytics" title="Google Analytics Guide" style="max-width: 100%;"/>
                       </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <?php 
                if(!empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret"))) : 
                    $this->load->view('session/launch-view'); 
                endif; 
            ?>
            <div class="row">
                <div class="col-md-12">
                     <p class="alert alert-info"><i class="fa fa-info-circle"></i>  All edits to the data will only be reflected on your website. This system will not update your MLS data</p>
                </div>
                <?php $session = $this->session->flashdata('session');
                    if( isset($session) )
                    {
                ?>
                <div class="col-md-12 alert-status">
                    <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><i class="fa fa-info-circle"></i> <?php echo $session["message"]; ?></div>
                </div>
                <?php } ?>

                 <div class="col-md-12 alert-status-slider-upload">
                   
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Branding</h3><br><br>
                            <p><i class="fa text-primary fa-info-circle"></i> <small>Max. upload file size: <strong>5 MB</strong></small> |
                            <small>Allowed file type: <strong>jpg</strong> | <strong>jpeg</strong> | <strong>png</strong> | <strong>gif</strong></small>
                        </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <form action="" class="form-horizontal" method="POST" enctype="multipart/form-data" id="saveAgentInfos">
                            <div class="box-body">
                                <div class="form-group" style="display: none;">
                                    <label for="" class="col-md-2">Site Name <a href="javascript://" data-toggle="tooltip" title="This will appear in your homepage browser tab"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                    <div class="col-md-10">
                                        <input type="text" name="site_name" value="<?=(isset($branding->site_name) && !empty($branding->site_name)) ? $branding->site_name : "";?>" class="form-control" maxlength="150">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Tag Line <a href="javascript://" data-toggle="tooltip" title="Tag Line will be displayed directly under your company logo "><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                    <div class="col-md-10">
                                        <input type="text" name="tag_line"  class="form-control"
                                        value="<?php
                                            if (isset($branding->tag_line) && !empty($branding->tag_line)) {
                                                echo  $branding->tag_line;
                                            } else {
                                                if ($account_info->MlsId == "20130226165731231246000000") {
                                                    echo "The Voice For Real Estate in Brevard";
                                                } else {
                                                    echo "Your home away from home";
                                                }
                                            }
                                        ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label style="font-size: 13px;" class="col-md-2">Banner Tagline <a href="javascript://" data-toggle="tooltip" title="Banner Tagline will be displayed directly above your IDX search bar on the homepage"><i class="fa fa-question-circle" aria-hidden="true"></i></a><br> </label>

                                    <div class="col-md-10">
                                        <input type="text" name="banner_tagline" value="<?=(isset($branding->banner_tagline) && !empty($branding->banner_tagline)) ? $branding->banner_tagline : "Everyone needs a place to call HOME";?>" class="form-control" maxlength="75">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Logo <a href="javascript://" data-toggle="tooltip" title=" Company logo will be added in the header of your site"><i class="fa fa-question-circle" aria-hidden="true"></i></a><br/><small>should be 250x65</small></label>
                                    <div class="col-md-10">
                                        <?php if( isset($branding->logo) AND !empty($branding->logo)) { ?>

                                            <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/logo/" . $branding->logo;?>" width="150">

                                            <br /><br />
                                            <div class="input-group imageupload">
                                                <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                            </div>
                                        <?php }else { ?>
                                            <div class="input-group imageupload">
                                                <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Favicon <a href="javascript://" data-toggle="tooltip" title="an icon associated with a URL!"><i class="fa fa-question-circle" aria-hidden="true"></i></a><br/><small>should be 16x16</small></label>
                                    <div class="col-md-10">
                                    <?php if( isset($branding->favicon) AND !empty($branding->favicon)) { ?>

                                                <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/favicon/" . $branding->favicon;?>" width="150">

                                            <br /><br />
                                            <div class="input-group imageupload">
                                                <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                            </div>

                                    <?php }else { ?>
                                        <div class="input-group imageupload">
                                            <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                                <hr>
                                <h3>Agent Info</h3>
                                <div class="form-group">
                                    <label class="col-md-2">Photo <a href="javascript://" data-toggle="tooltip" title="This image is dynamically generated from your MLS. You can always update your profile picture here by selecting choose file."><i class="fa fa-question-circle" aria-hidden="true"></i></a><br/><small>should be 400x400</small></label>
                                    <div class="col-md-10">
                                        <?php if( isset($branding->agent_photo) AND !empty($branding->agent_photo)) { ?>

                                            <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/photo/" . $branding->agent_photo;?>" width="150">
                                           
                                            <br /><br />
                                            <div class="input-group imageupload">
                                                <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                            </div>
                                        <?php } else { ?>
                                            <?php if (isset($account_info->Images[0]->Uri) AND !empty($account_info->Images[0]->Uri)){?>
                                                <img src="<?=$account_info->Images[0]->Uri?>" class="img-circle" width="200">
                                            <?php } else { ?>
                                                <img src="<?= base_url()?>/assets/images/no-profile-img.gif" width="200" alt="No Profile Image">
                                            <?php } ?>
                                             <div class="input-group imageupload">
                                                <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">First Name</label>
                                    <div class="col-md-10">
                                        <input type="text" name="agent_fname" value="<?=(isset($branding->first_name) && !empty($branding->first_name)) ? $branding->first_name : $account_info->FirstName;?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Last Name</label>
                                    <div class="col-md-10">
                                        <input type="text" name="agent_lname" value="<?=(isset($branding->last_name) && !empty($branding->last_name)) ? $branding->last_name : $account_info->LastName;?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Office No.</label>
                                    <div class="col-md-10">
                                        <input type="text" name="agent_office_no" value="<?php if(isset($branding->phone) AND !empty($branding->phone)) { ?><?php echo $branding->phone; ?><?php } elseif(isset($account_info->Phones)){foreach($account_info->Phones as $phone){if($phone->Name == "Office"){echo $phone->Number;}}}?>" pattern="[0-9x-\s()]+" title="Enter a valid phone number(numbers,() and - only)" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Mobile No.</label>
                                    <div class="col-md-10">
                                        <input type="text" name="agent_mobile_no" value="<?php if(isset($branding->mobile) AND !empty($branding->mobile)) { ?><?php echo $branding->mobile; ?><?php } elseif(isset($account_info->Phones)){foreach($account_info->Phones as $phone){if($phone->Name == "Mobile"){echo $phone->Number;}}}?>" pattern="[0-9x-\s()]+" title="Enter a valid mobile number(numbers,() and - only)" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">City</label>
                                    <div class="col-md-10">
                                        <input type="text" name="agent_city" value="<?php if(isset($branding->city) AND !empty($branding->city)) { ?><?php echo $branding->city; ?>
                                            <?php } elseif(isset($account_info->Addresses[0]->City) AND !empty($account_info->Addresses[0]->City)) { ?><?php echo $account_info->Addresses[0]->City ?><?php } else { ?><?php echo "Not Specified" ;?><?php } ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Address</label>
                                    <div class="col-md-10">
                                        <input type="text" name="agent_address" value="<?php if(isset($branding->address) AND !empty($branding->address)) { ?><?php echo $branding->address; ?><?php } elseif(isset($account_info->Addresses[0]->Address) AND !empty($account_info->Addresses[0]->Address)) { ?><?php echo $account_info->Addresses[0]->Address ?><?php } else { ?><?php echo "Not Specified" ;?><?php } ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Zip</label>
                                    <div class="col-md-10">
                                        <input type="text" name="agent_zip" value="<?php if(isset($branding->zip) AND !empty($branding->zip)) { ?><?php echo $branding->zip; ?><?php } elseif(isset($account_info->Addresses[0]->PostalCode) AND !empty($account_info->Addresses[0]->PostalCode)) { ?><?php echo $account_info->Addresses[0]->PostalCode ?><?php } else { ?><?php echo "Not Specified" ;?><?php } ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2">Email<a href="javascript://" data-toggle="tooltip" title="This email is displayed on your Home page, Contact page, and is used for logging in, and where leads are sent."> <i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                    <div class="col-md-10">
                                        <input type="email" name="agent_email" value="<?php if(isset($branding->email) AND !empty($branding->email)) { ?><?php echo $branding->email; ?><?php } elseif(isset($account_info->Emails[0]->Address) AND !empty($account_info->Emails[0]->Address)) { ?><?php echo $account_info->Emails[0]->Address ?><?php } else { ?><?php echo "Not Specified" ;?><?php } ?>" class="form-control" required>
                                    </div>
                                </div>

                                <?php if ($account_info->MlsId == "20180216183507684268000000" ): ?>
                                    <div class="form-group supervisor">
                                        <label class="col-md-2">Supervisor<a href="javascript://" data-toggle="tooltip" title="This information is required by Texas State Law, and will be displayed on the Information about Brokerages Services page" data-placement="right"> <i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                        <div class="col-md-10">
                                            <input type="text" name="supervisor" value="<?php if(isset($branding->supervisor) AND !empty($branding->supervisor)) { echo $branding->supervisor ; } ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2">Supervisor License No. </label>
                                        <div class="col-md-10">
                                            <input type="text" name="supervisor_license_no" value="<?php if(isset($branding->supervisor_license_no) AND !empty($branding->supervisor_license_no)) { echo $branding->supervisor_license_no ; } ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2">Supervisor Email </label>
                                        <div class="col-md-10">
                                            <input type="email" name="supervisor_email" value="<?php if(isset($branding->supervisor_email) AND !empty($branding->supervisor_email)) { echo $branding->supervisor_email ; } ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2">Supervisor Phone No. </label>
                                        <div class="col-md-10">
                                            <input type="text" name="supervisor_phone" value="<?php if(isset($branding->supervisor_phone) AND !empty($branding->supervisor_phone)) { echo $branding->supervisor_phone ; } ?>" class="form-control" required>
                                        </div>
                                    </div>
                                <?php endif ?>
                                <!-- <a  href="#" type="button" class="btn btn-default btn-submit" data-toggle="modal" data-target="#modalAddPage"><i class="fa fa-file-image-o" aria-hidden="true"></i> Add Media</a> -->
                                <div class="form-group">
                                    <label class="col-md-2">About <a href="javascript://" data-toggle="tooltip" title="Take some time to tell your clients about yourself. You can add images, designations, videos and more to this page. This will appear on your homepage directly under your contact information."><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                    <div class="col-md-10">
                                        <a  style="margin-bottom: 10px;" href="javascript://"  type="button" class="btn btn-default btn-submit" data-toggle="modal" data-target="#modalAddPage"><i class="fa fa-file-image-o" aria-hidden="true"></i> Add Media</a>
                                        <textarea id="pageTextarea" class="form-control" name="about_agent" cols="20" rows="9"><?=(isset($branding->about_agent) && !empty($branding->about_agent)) ? $branding->about_agent : "";?></textarea>
                                    </div>
                                </div>
                            </div>

                      <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-default btn-save btn-submit pull-right" ><i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Save</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->

                     <!-- SOLD LISTINGS OPTIONS -->
                             <?php if(!$sold_searchable){?>
                                 <p><i class="fa text-primary fa-info-circle"></i> <strong>Sold</strong> and <strong>Closed</strong> is not searchable on your MLS. Please contact your MLS to support this feature!</p>
                           <?php }?>
                            
                             <h4 class="sold-enable">My Sold Listings:  
                              <a href="javascript://" data-toggle="tooltip" title="Sold Listings must be toggled ON to show Sold Listings section on your site."><i class="fa fa-question-circle" aria-hidden="true"></i></a> 
                              <input type="checkbox" name="sold-checkbox" data-url="<?php echo base_url(); ?>agent_sites/display_sold_listings" <?= (!$sold_searchable) ? "disabled" : ""?> >
                            </h4>

                        <div class="modal insertMediaModal fade" id="modalAddPage" tabindex="-1" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                     <div class="alert-status-media-upload"></div>
                                        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                         <?php if(!empty($published_media)){?>
                                             <strong><h3>Insert Media or Upload New Media</h3></strong> 
                                        <?php }else{?>
                                             <strong><h3>Upload New Media</h3></strong> 
                                        <?php }?>
                                       
                                        <form id="upload-media-page" method="POST" action="<?= base_url()?>pages/upload_media_page" enctype="multipart/form-data">
                              
                                            <div style="margin-bottom: 5px" class="input-group imageupload">
                                                <input type="file" name="userfile" class="filestyle" data-buttonBefore="true" data-icon="false">
                                            </div>

                                              <small>Max. upload file size: <strong>10 MB</strong></small> | <small>Allowed file type: <strong>jpg</strong> | <strong>jpeg</strong> | <strong>png</strong> | <strong>gif</strong> | <strong>doc</strong> | <strong>docx</strong> | <strong>xls</strong> | <strong>xlsx</strong> | <strong>csv</strong> | <strong>pdf</strong> | <strong>txt</strong></small>

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-default btn-save upload-media-page btn-submit"> <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Upload</button>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="modal-body addMediaModal">
                                        <div class="row">
                                            <div class="col-md-9 upload-media-preview">
                                                <form id="insertMedia" action="<?php echo base_url()?>pages/insertMedia" method="POST">
                                                        <!-- <div class="col-md-9 modal-media"> -->

                                                        <ul class="list-item">

                                                          <?php if(!empty($media)){

                                                                  if(!empty($published_media)) { ?>

                                                                      <?php foreach ($published_media as $media){?>

                                                                          <li class="list-item-grid">
                                                                            <div class="media-grid-actions">
                                                                                <span class="checked">
                                                                                    <i class="fa fa-check"></i>
                                                                                </span>
                                                                                <div class="ischeckbox" data-id="media<?=$media->id?>">
                                                                                    <input type="checkbox" class="modal-checkbox" name="bulk_insertMedia" value="<?php echo $media->id ?>">
                                                                                </div>
                                                                            </div>

                                                                            <?php if($media->is_freemium == "1"){

                                                                                $src = getenv('AWS_S3_ASSETS') . "uploads/file/".$media->file_name;
                                                                              } else{

                                                                                   $src = base_url()."assets/upload/file/".$media->file_name;
                                                                              }?>

                                                                          <?php if($media->file_type === "application/pdf" ){?>

                                                                              <div class="item-grid media<?=$media->id?>">
                                                                                  <img src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?php echo $media->alt_text?>">
                                                                                   <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block;" src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>"></a>'/>

                                                                              </div>

                                                                             <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.word" || $media->file_type === "application/msword") { ?>

                                                                               <div class="item-grid media<?=$media->id?>">
                                                                                    <img src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?php echo $media->alt_text?>">
                                                                                     <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block;" src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>'/>

                                                                                </div>

                                                                             <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){?>
                                                                                <div class="item-grid media<?=$media->id?>">
                                                                                    <img src="<?php echo base_url()?>assets/images/excel-icon.png" alt="<?php echo $media->alt_text?>">
                                                                                     <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block;" src="<?php echo base_url()?>assets/images/excel-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>'/>

                                                                                </div>
                                                                             <?php }elseif($media->file_type === "text/plain"){?>

                                                                                 <div class="item-grid media<?=$media->id?>">
                                                                                    <img src="<?php echo base_url()?>assets/images/txt-icon.png" alt="<?php echo $media->alt_text?>">
                                                                                     <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block;" src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>'/>

                                                                                </div>

                                                                              <?php }elseif($media->file_type === "text/csv"){?>

                                                                                 <div class="item-grid media<?=$media->id?>">
                                                                                    <img src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?php echo $media->alt_text?>">
                                                                                     <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block;" src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>'/>

                                                                                </div>
                                                                             <?php } else {?>

                                                                                 <div class="item-grid media<?=$media->id?>">
                                                                                  <img src="<?php echo $src ?>" alt='<?php echo $media->alt_text?>'> <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:50%;display:block;" src="<?php echo $src ?>" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>' />

                                                                                </div>

                                                                             <?php }?>

                                                                          </li>

                                                                  <?php }?>

                                                                  

                                                               <?php }

                                                            }  else { ?>

                                                            <div class="no-media-uploaded">

                                                               <p class="no-media">No files found!<br></p>

                                                            </div>

                                                          <?php } ?>

                                                          </ul>
                                                        <!-- </div> -->

                                                </form>
                                            </div>
                                            <div class="col-md-3 image-details">

                                                <h3 class="image-details-header" style="display:none">Image Details</h3>

                                                <div class="show-notif"></div>

                                                <div class="image-deatils-preview">

                                                  <?php if(!empty($published_media)){

                                                       foreach ($published_media as $media){?>

                                                            <div id="media<?=$media->id?>" class="media-details hide-media clearfix">

                                                             <form id="updateMediaForm" class="updateMedia clearfix" action="<?php echo base_url()?>pages/update_details/<?=$media->id?>" method="POST">

                                                             <input type="hidden" name="media_id" value="<?=$media->id?>">
                                                             <input type="hidden" name="page_id" value="site_info">
                                                             <input type="hidden" name="file_name" value="<?=$media->file_name?>">
                                                                 <p><label for="media-title">Title</label></p>
                                                                <input type="text" name="media_title" id="media-title" value='<?php echo (isset($media->title)) ? $media->title : "" ?>' class="form-control" maxlength="30" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _">

                                                                 <?php if($media->is_freemium == "1"){

                                                                        $src = getenv('AWS_S3_ASSETS') . "uploads/file/".$media->file_name;
                                                                  } else{

                                                                       $src = base_url()."assets/upload/file/".$media->file_name;
                                                                  }?>

                                                                <?php if($media->file_type === "application/pdf"){?>

                                                                    <p class="media-icon"><img src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

                                                               <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.word" || $media->file_type === "application/msword" ) { ?>

                                                                    <p class="media-icon"><img src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

                                                               <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){?>

                                                                    <p class="media-icon"><img src="<?php echo base_url()?>assets/images/excel-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

                                                            
                                                                   <?php } elseif($media->file_type === "text/plain"){?>

                                                                        <p class="media-icon"><img src="<?php echo base_url()?>assets/images/txt-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

                                                                   <?php } elseif($media->file_type === "text/csv"){?>

                                                                        <p class="media-icon"><img src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

                                                                   <?php } else {?>
                                                                 
                                                                    <p class="media-icon"><img src='<?php echo $src ?>' alt='<?php echo $media->alt_text?>' width="80" height="80"></p><br>

                                                               <?php }?>

                                                              <label for="media-link">Image Link</label>

                                                                <input type="text" readonly="readonly" name="media_link" id="media-link" value="<?php echo (!empty($media->link)) ? $media->link : $src ?>" class="form-control" disabled>  <br>

                                                                <label for="media-caption">Caption</label>
                                                                <input type="text" name="media_caption" id="media-caption" value='<?php echo (!empty($media->caption)) ? $media->caption : "" ?>' class="form-control" maxlength="100" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>

                                                                <label for="media-alt-text">Alternative Text</label>
                                                                <input type="text" name="media_alt_text" id="media-alt-text" value='<?php echo (!empty($media->alt_text)) ? $media->alt_text : "" ?>' class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>

                                                                <label for="media-description">Description</label>
                                                                <input type="text" name="media_description" id="media-description" value='<?php echo (!empty($media->description)) ? $media->description : "" ?>' class="form-control" maxlength="200" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>

                                                                <button type="submit" class="btn btn-default btn-submit">Update</button>
                                                                 </form>
                                                            </div>
                                                        <?php }
                                                    }?>
                                                </div>

                                        </div>
                                        </div>
                                    </div>

                                    <div style="display:none" class="modal-footer insertPage">
                                        <button type="submit" id="insertIntoPage" class="btn btn-default btn-submit pull-left">Insert Into Page</button>
                                    </div>

                                </div>
                            </div>
                        </div> <!-- ModalPage!-->

                </div>
                <!-- End of col-md-6 -->
                <div class="col-md-6">
                    <!-- general form elements -->
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Slider Photo <a href="javascript://" data-toggle="tooltip" title="By default, your slider photos will be images from your active listings that rotate on your homepage. You can upload your own photos here if you don’t want to use your active listing photos"><i class="fa fa-question-circle" aria-hidden="true"></i></a></h3>
                        </div>

                                <div class="box-body padding-0">
                                    <div class="slider agent-site-info-panels">
                                        <form id="upload-slider-photo" method="POST" action="<?php echo base_url()?>agent_sites/upload_slider_photo" enctype="multipart/form-data">

                                            <div style="margin-bottom: 5px" class="input-group imageupload">
                                                <input type="file" name="slider_photo" class="filestyle" data-buttonBefore="true" data-icon="false">
                                            </div>

                                            <small>Max. upload file size: <>5 MB</strong></small> | <small>Allowed file type: <strong>jpg</strong> | <strong>jpeg</strong> | <strong>png</strong> | <strong>gif</strong></small>

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-default btn-save upload-slider-photo btn-submit pull-right"> <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Upload</button>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="slider-photos-preview">
                                        <ul>

                                            <?php
                                                if(isset($slider_photos) AND !empty($slider_photos)){

                                                  foreach($slider_photos as $photo){?>

                                                   <li id="slider-photo-<?=$photo->id?>" class="img-preview">
                                                            <span>
                                                                <a href="#" data-toggle="modal" data-target="#delete_slider_photo-modal-<?=$photo->id?>" data-id="<?=$photo->id?>">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                             </span>
                                                             <?php if($photo->is_freemium == "1"){?>
                                                                <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/slider/" . $photo->photo_url?>" width="200" height="150" alt="Slider Photos">

                                                            <?php } else{?>

                                                                <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/slider/" .$photo->photo_url?>" width="200" height="150" alt="Slider Photos">

                                                            <?php }?>

                                                        </li>

                                                        <!-- delete confirmation modal -->
                                                        <div id="delete_slider_photo-modal-<?=$photo->id?>" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="delete_slider_photo-modal-<?=$photo->id?>" aria-hidden="false">
                                                          <div class="modal-dialog modal-md">
                                                            <div class="modal-content">
                                                              <div class="modal-body text-center">
                                                                  <p class="del-photo">Are you sure you want to delete this photo?</p>

                                                            <?php if($photo->is_freemium == "1"){?>
                                                                 <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/slider/" . $photo->photo_url?>" width="300" height="200" alt="Slider Photos">
                                                                
                                                            <?php } else{?>

                                                                <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/slider/" . $photo->photo_url?>" width="300" height="200" alt="Slider Photos">

                                                            <?php }?>

                                                                  
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                <a class="delete_slider_photo" data-id="<?=$photo->id?>" href="<?php echo site_url('agent_sites/delete_slider_photo?photo_url=');?><?=$photo->photo_url?>&photo_id=<?=$photo->id?>"><button type="button" class="btn btn-primary">Yes</button></a>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                 <?php }
                                              }?>
                                        </ul>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                      </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
   </section>
</div>
<?php $this->load->view('session/footer'); ?>

<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        var copied = document.execCommand("copy");
        $temp.remove();
        console.log(copied);
        
        if(copied){
          $(".copied-success").show();
        }else{
            $(".copied-failed").show();
        }

        $(".copied-status").delay(1000) .fadeOut(1000);
    }
</script>

<script>
    $(document).ready(function(){

        tinymce.init({
            selector: '#pageTextarea',
            theme: 'silver',
            content_style: '.mce-content-body {font-size:14px;font-family:Arial,sans-serif;}',
            fontsize_formats: '12px 14px 16px 18px 20px 24px 36px 42px',
            plugins: [
              'autosave autoresize autolink code link',
              'searchreplace wordcount media lists print',
              'preview table emoticons paste textcolor imagetools'
            ],
            autosave_interval: '10s',
            min_height: 350,
            max_height: 500,
            menubar: 'file edit format table',
            toolbar_items_size: 'small',
            toolbar: 'undo redo | fontsizeselect fontselect | blockquote bold italic underline forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink media | emoticons code preview help',
           default_link_target: "_blank",
           link_assume_external_targets: true,
           link_context_toolbar: true,
           link_title: false
        });

    });
</script>

