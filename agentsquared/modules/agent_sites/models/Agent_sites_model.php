<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Agent_sites_model extends CI_Model { 

	public function __construct()
    {
        parent::__construct();
    }

     public function get_your_properties_listings($counter = FALSE, $datas = array())
    {
        $this->db->select("*")
                ->from("property_activated")
                ->where('list_agentId', $datas["mlsID"]);
                //->where('property_status', 'Active');

        return $this->db->get()->result();
    }

    public function get_branding_infos()
    {
        $this->db->select("code,site_name,tag_line,banner_tagline,about_agent,agent_photo,logo,favicon,first_name,last_name,mls_name,phone,mobile,city,address,zip,email,broker,broker_number,license_number,company,theme,supervisor,supervisor_license_no,supervisor_email,supervisor_phone")
                ->from("users")
                ->where("id", $this->session->userdata("user_id"))
                ->limit(1);

        return $this->db->get()->row();
    }

    public function get_slider_photos()
    {
        $this->db->select("*")
                ->from("slider_photos")
                ->where("user_id", $this->session->userdata("user_id"));

        return $this->db->get()->result();
    }

    public function get_mls_users($mls_id=NULL,$limit="")
    {
        $this->db->select("id")
                ->from("users")
                ->where("mls_id", $mls_id)
                ->limit($limit);

        return $this->db->get()->result();
    }

    public function check_default_slider_photos($mls_id=NULL,$user_id=NULL)
    {

        $this->db->select("*")
                ->from("slider_photos")
                ->where("user_id", $user_id)
                ->where("photo_url", $mls_id.'-1.jpg');

        return $this->db->get()->result();
    }

    public function check_slider_photos($user_id=NULL)
    {

        $this->db->select("*")
                ->from("slider_photos")
                ->where("user_id", $user_id)
                ->limit(1);

        return $this->db->get()->result();
    }

   public function update() {

        $info["site_name"] = $this->input->post("site_name");
        $info["tag_line"] = $this->input->post("tag_line");
        $info["banner_tagline"] = $this->input->post("banner_tagline");
        $info["first_name"] = $this->input->post("agent_fname");
        $info["last_name"] = $this->input->post("agent_lname");
        $info["phone"] = $this->input->post("agent_office_no");
        $info["mobile"] = $this->input->post("agent_mobile_no");
        $info["city"] = $this->input->post("agent_city");
        $info["address"] = $this->input->post("agent_address");
        $info["zip"] = $this->input->post("agent_zip");
        $info["email"] = $this->input->post("agent_email");
        $info["about_agent"] = $this->input->post("about_agent");
        $info["broker"] = $this->input->post("broker");
        $info["broker_number"] = $this->input->post("broker_number");
        $info["license_number"] = $this->input->post("license_number");
        $info["supervisor"] = $this->input->post("supervisor");
        $info["supervisor_license_no"] = $this->input->post("supervisor_license_no");
        $info["supervisor_email"] = $this->input->post("supervisor_email");
        $info["supervisor_phone"] = $this->input->post("supervisor_phone");

        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $info);

        if($this->db->affected_rows() > 0) {

            if($info["email"] != $this->session->userdata('email')) {

                //Send Email here
                $sess_data = $this->db->select("code, created_on")
                                        ->from("users")
                                        ->where("id", $this->session->userdata('user_id'))
                                        ->get()->row();
                $send = array(
                    'old_email' => $this->session->userdata('email'),
                    'new_email' => $info['email'],
                    'name'      => $info["first_name"].' '.$info["last_name"],
                    'user_id'   => $this->session->userdata('user_id'),
                    'agent_id'  => $this->session->userdata('agent_id'),
                    'code'      => $sess_data->code,
                    'old_last_login' => $sess_data->created_on
                );

                $this->send_update_agent_info_email($send);

                //Set new email session
                $arr = array(
                    'email'     => $info["email"],
                    'identity'  => $info["email"]
                );

                $this->session->set_userdata($arr);

            }

            return TRUE;
        }
         
        return FALSE;
    }

    public function send_update_agent_info_email($data=array()) {

        if($data) {

            $this->load->library("email");

            //Send to old email
            $content = "Hi ".$data['name']."!<br><br>

                You've successfully changed your email address associated with your AgentSquared IDX Website's dashboard from: <br><br>"

                .$data['old_email']."<br><br> 

                to: <br><br>"

                .$data['new_email'].".<br><br>

                If you didn't changed your email address on your account, and you believe your account had been compromised, go to our <a href='https://www.agentsquared.com/support/' target='_blank'>support</a> page. <br><br>

                Best Regards,<br><br>

                AgentSquared Team
            ";

            $subject ="AgentSquared IDX Website: Change Email Notification" ;

            $this->email->send_email(
                'automail@agentsquared.com',
                'AgentSquared',
                'paul@agentsquared.com',
                $subject,
                'email/template/default-email-body',
                array(
                    "message" => $content,
                    "subject" => $subject,
                    "to_bcc" => "honradokarljohn@gmail.com,joce@agentsquared.com,raul@agentsquared.com"
                )
            );
            
            //Send to new email
            $content1 = "Hi ".$data['name']."!<br><br>

                You've successfully changed your email address on your AgentSquared IDX Website's dashboard.<br><br>

                To verify your new email address, just click <a href='".AGENT_DASHBOARD_URL."/idx_login/verify_new_email/".urlencode($data['user_id']."/".$data['new_email']."/".$data['agent_id']."/".$data['code']."/".$data['old_last_login'])."' target='_blank'>here</a>

                <br><br>

                Best Regards,<br><br>

                AgentSquared Team
            ";

            $subject1 ="AgentSquared IDX Website: Change Email Vefication" ;

            $this->email->send_email(
                'automail@agentsquared.com',
                'AgentSquared',
                'paul@agentsquared.com',
                $subject1,
                'email/template/default-email-body',
                array(
                    "message" => $content1,
                    "subject" => $subject1,
                    "to_bcc" => "honradokarljohn@gmail.com,joce@agentsquared.com,raul@agentsquared.com"
                )
            );

            return TRUE;
        }

        return FALSE;
    }

    public function insert_info($arr = array()) {

        $array = array('id' => $arr['id']);
        $this->db->where($array);
        $data = array(
            'site_name' => $arr['site_name'],
            'tag_line' => $arr['tag_line'],
            'banner_tagline' => $arr['banner_tagline'],
            'first_name' => $arr['first_name'],
            'last_name' => $arr['last_name'],
            'address' => $arr['address'],
            'city' => $arr['city'],
            'zip' => $arr['zip'],
            'phone' => $arr['phone'],
            //'agent_photo' => $arr['agent_photo'],
            'company' => $arr['company'],
            'broker' => $arr['broker'],
            'broker_number' => $arr['broker_number'],
        );

        $this->db->update('users', $data);

        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return TRUE;
    }

    public function insert_logo_info($datas = array(), $user_id)
    {
        $img["logo"] = $datas['file_name'];
        $this->db->where("id", $user_id);
        $this->db->update("users", $img);

        return TRUE;
    }

    public function insert_favicon_info( $datas = array(), $user_id )
    {
        $img["favicon"] = $datas['file_name'];
        $this->db->where("id", $user_id);
        $this->db->update("users", $img);

        return TRUE;
    }

    public function insert_agent_photo_info( $datas = array(), $user_id )
    {
        $img["agent_photo"] = $datas['file_name'];
        $this->db->where("id", $user_id);
        $this->db->update("users", $img);

        return TRUE;
    }

    public function add_agent_photo( $datas = array() )
    {
        $img["agent_photo"] = $datas['file_name'];
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $img);

        return TRUE;
    }

     public function add_agent_photo_aws( $photo = "" )
    {
        $img["agent_photo"] = $photo;
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $img);

        return TRUE;
    }

     public function add_slider_photo( $datas = array() )
    {
        $img["user_id"] = $this->session->userdata("user_id");
        $img["photo_url"] = $datas["file_name"];
        $img["photo_type"] = $datas["file_type"];
        $img["photo_size"] = $datas["file_size"];

        if(isset($datas["is_freemium"])){
            $img["is_freemium"] = "1";
        }
        $img["date_created"] = date("Y-m-d h:m:s");
        $this->db->insert("slider_photos", $img);

        return $this->db->insert_id();
    }

     public function insert_default_slider_photo( $datas = array() )
    {
        $img["user_id"] = $datas["user_id"];
        $img["photo_url"] = $datas["file_name"];
        $img["photo_type"] = $datas["file_type"];
        $img["photo_size"] = $datas["file_size"];

        if(isset($datas["is_freemium"])){
            $img["is_freemium"] = "1";
        }
        $img["date_created"] = date("Y-m-d h:m:s");
        $this->db->insert("slider_photos", $img);

        return $this->db->insert_id();
    }
    
     public function delete_slider_photo($photo_url = NULL, $photo_id = NULL)
    {
       $array = array(
            'id' => $photo_id,
            'photo_url' => $photo_url,
            'user_id' => $this->session->userdata("user_id")
        );
       $this->db->where($array);
       $this->db->delete('slider_photos');

    return TRUE;
    }

    public function update_logo_info( $datas = array() )
    {
        $img["logo"] = $datas['file_name'];
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $img);

        return TRUE;
    }

    public function update_favicon_info( $datas = array() )
    {
        $img["favicon"] = $datas['file_name'];
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $img);

        return TRUE;
    }

    public function update_agent_photo_info( $datas = array() )
    {
        $img["agent_photo"] = $datas['file_name'];
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $img);

        return TRUE;
    }

    public function update_logo_info_aws( $logo = NULL )
    {
        $img["logo"] = $logo;
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $img);

        return TRUE;
    }

    public function update_favicon_info_aws( $favicon = NULL )
    {
        $img["favicon"] = $favicon;
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $img);

        return TRUE;
    }

    public function update_agent_photo_info_aws( $agent_photo = NULL )
    {
        $img["agent_photo"] = $agent_photo;
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->update("users", $img);

        return TRUE;
    }

    public function check_agent() {

        $ret = FALSE;

        $query = $this->db->get_where("domains", array("agent" => $this->session->userdata("user_id")));

        if($query->num_rows() > 0) {
            $ret = TRUE;
        }

        return $ret;
    }

    public function fetch_domain_info() {

        $this->db->select("*")
                ->from("domains")
                ->where('agent', $this->session->userdata('user_id'))
                ->where('type', "agent_site_domain");

        return $this->db->get()->result();

    }

    public function fetch_spw_domain__info() {

        $this->db->select("*")
                ->from("domains");

        return $this->db->get()->result();

    }

    public function add_existing_domain($post = array()) {
        
        $this->db->select("domains")
                ->from("domains")
                ->where("domains",$post["domain_name"])
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return array('success' => FALSE, 'type' => "exist_domain", 'error' => 'Selected domain is already reserved by agent squared!');      
        } else {
            $domain['domains']  = $post["domain_name"];
            $domain["agent"] = $this->session->userdata('user_id');
            $domain["date_created"] = date("Y-m-d h:m:s");
            $domain["status"] = "pending";
            $domain["type"] = "agent_site_domain";
            $domain["existing_domain"] = 1;
            $domain["is_subdomain"] = 0;

            if(!$this->check_agent()) {

                $this->db->insert("domains", $domain);

                if($this->db->insert_id()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            } else {
                $this->db->where("agent", $this->session->userdata("user_id"));
                $this->db->update("domains", $domain);

                if($this->db->affected_rows()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            }
        }
    }

    public function add_subdomain($post = array()) {

        $this->db->select("domains")
                ->from("domains")
                ->where("domains",$post["domain_name"])
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return array('success' => FALSE, 'type' => "exist_domain", 'error' => 'Selected domain is already reserved by agent squared!');      
        } else {
            $domain['domains']  = $post["domain_name"];
            $domain["agent"] = $this->session->userdata('user_id');
            $domain["date_created"] = date("Y-m-d h:m:s");
            $domain["status"] = "pending";
            $domain["type"] = "agent_site_domain";
            $domain["existing_domain"] = 0;
            $domain["is_subdomain"] = 1;
            $domain["subdomain_url"] = "http://".$post['domain_name'];

            if(!$this->check_agent()) {

                $this->db->insert("domains", $domain);

                if($this->db->insert_id()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            } else {
                $this->db->where("agent", $this->session->userdata("user_id"));
                $this->db->update("domains", $domain);

                if($this->db->affected_rows()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            }
        }
    }

    public function add_purchase_domain($post = array())
    {
        $this->db->select("domains")
                ->from("domains")
                ->where("domains", $post["domain_name"])
                ->limit(1);

        $query = $this->db->get();
       
        if($query->num_rows() > 0) {

            return array('success' => FALSE, 'type' => "exist_domain", 'error' => 'Selected domain is already reserved by agent squared!');  

        } else {

            if(isset($post["type"]) && $post["type"] == "spw") { //For SPW Domain

                $domain['domains']  = strtolower($post["domain_name"]);
                $domain["agent"] = $this->session->userdata('user_id');
                $domain["user_name"] = $post["spw_username"];
                $domain["date_created"] = date("Y-m-d h:m:s");
                $domain["status"] = "pending";
                $domain["type"] = "single_property_website";
                $domain["existing_domain"] = 0;
                $domain["is_subdomain"] = 0;

                $this->db->select("domains")
                            ->from("domains")
                            ->where("user_name", $post["spw_username"])
                            ->limit(1);

                $uname_exist = $this->db->get();

                if($uname_exist->num_rows() > 0) {
                    
                    $this->db->where("user_name", $post["spw_username"]);
                    $this->db->update("domains", $domain);

                    if($this->db->affected_rows()) {
                        $response["message"] = "Domain info has been successfully added!";
                        $response["success"] = TRUE;
                        return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                    }   
                    else {
                        return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                    }

                } else {
                    $this->db->insert("domains", $domain);

                    if($this->db->insert_id()) {
                        $response["message"] = "Domain info has been successfully added!";
                        $response["success"] = TRUE;
                        return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 
                    }   
                    else {
                        return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                    }
                }

            } else { //For IDX Domain

                $domain['domains']  = strtolower($post["domain_name"]);
                $domain["agent"] = $this->session->userdata('user_id');
                $domain["date_created"] = date("Y-m-d h:m:s");
                $domain["status"] = "pending";
                $domain["type"] = "agent_site_domain";
                $domain["existing_domain"] = 0;
                $domain["is_subdomain"] = 0;
                
                if(!$this->check_agent()) {

                    $this->db->insert("domains", $domain);

                    if($this->db->insert_id()) {

                        $response["message"] = "Domain info has been successfully added!";
                        $response["success"] = TRUE;

                        return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 

                    } else {
                        return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                    }

                } else {

                    $arr = array(
                        "agent"     => $this->session->userdata("user_id"),
                        "type"      => "agent_site_domain",
                        "is_ssl"    => 0,
                        "force_ssl" => 0
                    );

                    //$this->db->where("agent", $this->session->userdata("user_id"));
                    $this->db->where($arr);
                    $this->db->update("domains", $domain);

                    if($this->db->affected_rows()) {
                        $response["message"] = "Domain info has been successfully added!";
                        $response["success"] = TRUE;
                        return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                    }   
                    else {
                        return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                    }
                }
            }
        }
    }

    public function send_notification_to_admin($datas = array())
    {
        $this->load->library("email");

        $content = "Hi Admin <br><br>

            You have new requested domain. Please check the details below:<br><br>

            Name: ".$datas['user_name']." <br>
            Domain Name: ".$datas['domain_name']." <br><br>

            Best Regards,<br><br>

            Agent Squared Management
        ";

        $subject ="New Domain Request" ;

        $this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            'rolbru12@gmail.com',
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "troy@agentsquared.com,albert@agentsquared.com,joce@agentsquared.com,raul@agentsquared.com,paul@agentsquared.com"
            )
        );
    }

    public function update_agent_domain_status_post()
    {
        $update["status"] = "registered";
        $this->db->where('agent', $this->session->userdata("user_id"));
        $this->db->update("domains", $update);

        if( $this->db->affected_rows() > 0 )
        {
           return TRUE;
        }

        return FALSE;
    }

    public function is_reserved_domain()
    {   
        $this->db->select("id,domains,status,existing_domain,is_subdomain,subdomain_url");
        $this->db->from("domains");
        $this->db->where("type", "agent_site_domain");
        $this->db->where("agent", $this->session->userdata("user_id"));
        $this->db->limit(1);

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            $data = $query->row();
            return array('success' => TRUE, 'domain_id' => $data->id, 'domain_name' => $data->domains, 'status' => $data->status, 'domain_exist' => $data->existing_domain, 'is_subdomain' => $data->is_subdomain,'subdomain_url' => $data->subdomain_url);       
        }

        return array('success' => FALSE); 
    }

    public function get_idx( $user_id = NULL )
    {
        return $this->db->select("*")
                ->where("user_id", $user_id)
                ->limit(1)
                ->get("property_idx")->row();

    }

    public function get_wizards_details()
    {
        $user_id = $this->session->userdata("user_id");

        $this->db->select("u.*, p.*, s.*, (SELECT domains FROM domains WHERE u.id = domains.agent AND domains.type='agent_site_domain' LIMIT 1) as domain_name")
                ->from("users u")
                ->join("property_idx p","u.id = p.user_id", "left")
                ->join("social_media_links s","u.id = s.user_id", "left")
                ->where("u.id", $user_id)
                ->limit(1);

        return  $this->db->get()->row();

    }

    public function sync_old_agents()
    {
        $agents = $this->db->select("*")
                ->from("agent_subscription")
                ->get()->result();

        return $agents;
    }

    public function get_customer_all_infos()
    {
        $this->db->select("u.*, d.domains as domain_name, p.api_key, p.api_secret")
                ->from("users u")
                ->join("domains d", "u.id = d.agent" , "left")
                ->join("property_idx p", "u.id = p.user_id" , "left")
                ->where("u.id", $this->session->userdata("user_id"))
                ->limit(1);

        return $this->db->get()->row();

    }

    public function getAgentsInfos()
    {
        $this->db->select("*")
                ->from("agent_subscription")
                ->where("ApplicationType", "Agent_IDX_Website");

        $query = $this->db->get();
        $isLaunch = array();

        if( $query->num_rows() > 0 )
        {
            $datas = $query->result();

            foreach ($datas as $key) {

                if( !$this->isLaunch($key) )
                {
                    $isLaunch[] = $key;
                }

            }

            return $isLaunch;

        }

        return FALSE;
    }

    public function isLaunch( $infos = array() )
    {
        $this->db->select("*")
                ->from("users")
                ->where("agent_id", $infos->UserId);

        $query = $this->db->get();

        if( $query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }   

    public function is_domain_free(){
        $this->db->select("id")
                ->from("domains")
                ->where("agent", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? FALSE: TRUE;
    }

    public function get_domains()
    {
        $this->db->select("domains")
                ->from("domains")
                ->where("status","completed")
                ->where("type","agent_site_domain");

        return $this->db->get()->result();
    }

    public function update_domain($data = array()) {

        $update["operationalId"] = $data["operationalId"];
        $this->db->where('agent', $this->session->userdata("user_id"));
        $this->db->update("domains", $update);

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;

    }

     public function get_sold_listings_option($agent_id) {

        $this->db->select('show_sold_listings')
                ->from('users')
                ->where('agent_id', $agent_id);

        return $this->db->get()->row();
    }

    public function save_sold_listings_options($data) {

        $this->db->where('agent_id', $this->session->userdata('agent_id'));
        $this->db->update('users', $data);

    }

    public function cancel_agent_subscription() {

        $this->db->select("u.first_name, u.last_name, u.phone, u.mls_name, as.UserName, as.ApplicationType, as.ApplicationName, d.domains")
                ->from("users u")
                ->join("agent_subscription as", "as.UserId=u.agent_id", "inner")
                ->join("domains d", "d.agent=u.id", "inner")
                ->where("u.id", $this->session->userdata("user_id"))
                ->limit(1);

        $query = $this->db->get()->row();
        
        if(!empty($query)) {
            //save agent info to cancel agent table
            $data = array(
                'user_id'       => $this->session->userdata('user_id'),
                'agent_id '     => $this->session->userdata('agent_id'),
                'name'          => $query->first_name.' '.$query->last_name,
                'email'         => $this->session->userdata('email'),
                'cancel_reason' => $this->input->post('cancel_reason'),
                'cancel_date'   => date('Y-m-d H:i:s')
            );
            
            $this->db->insert('agent_cancelled', $data);
            
            $this->load->library("email");

            $this->email->send_email_v3(
                'support@agentsquared.com', //sender email
                'AgentSquared', //sender name
                'sales@agentsquared.com', //recipient email
                'honradokarljohn@gmail.com, paul@agentsquared.com, joce@agentsquared.com, aldwinj12345@gmail.com', //bccs
                '3558567f-0e8a-42d2-a842-76c9511eb003', //'7e85d26c-e721-4962-863a-69657177d8d2', //template_id
                $email_data = array(
                    'subs' => array(
                        'agent_id'              => $this->session->userdata('agent_id'),
                        'agent_first_name'      => $query->first_name,
                        'agent_last_name'       => $query->last_name,
                        'agent_email'           => $this->session->userdata('email'),
                        'agent_phone'           => $query->phone,
                        'agent_mls'             => $query->mls_name,
                        'agent_username'        => $query->UserName,
                        'agent_website'         => $query->domains,
                        'agent_product'         => $query->ApplicationType,
                        'agent_application_name'=> $query->ApplicationName,
                        'agent_cancel_reason'   => $this->input->post('cancel_reason')
                    )
                )
            );
            
            return TRUE;

        }

        return FALSE;

    }

}