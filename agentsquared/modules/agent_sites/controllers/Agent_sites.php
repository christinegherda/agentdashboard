<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_sites extends MX_Controller {

	function __construct() {

		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Agent_sites_model");
		$this->load->model("dsl/dsl_model");
		$this->load->model("media/Media_model","media");
		$this->load->model("idx_login/Idx_model");
		$this->load->model("customize_homepage/customize_homepage_model", "custom");

		// Load Aws_s3 Library
  		$this->load->library('s3');
  		$this->config->load('s3');

		// Load GoHighLevel Library
		$this->load->library('Gohighlevel');

	}

	public function index() {

		$data['title'] = "Agent Site Info";

		if( $_POST ){

			//update agent info
			if( $this->Agent_sites_model->update() ){

				$status = TRUE;
				$message = "Successfully updated agent info!";
			} else {

				$status = FALSE;
				$message = "No changes has been made!";
			}

			//upload agent logo
			if(!empty($_FILES['userfile']['name'][0])){
				if($_FILES["userfile"]["error"][0] == 0 && $_FILES["userfile"]["size"][0] <= "5242880"){

					if( $_FILES["userfile"]["type"][0] == "image/jpeg" || $_FILES["userfile"]["type"][0] == "image/png" || $_FILES["userfile"]["type"][0] == "image/gif"){

						//generate random-id
						$filename = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['userfile']['name'][0]);
						$logo = uniqid().'-'.($filename);
						$file = $_FILES["userfile"]["tmp_name"][0];
						$bucket_name = $this->config->item('bucket_name');
						$file_name = "uploads/logo/".$logo;
						$acl = "public-read";

						if(!empty($file)){

							//upload logo to aws S3
				 	 		$this->s3->putObjectFile($file,$bucket_name,$file_name,$acl);
						}

				 	 	if(!empty($_FILES['userfile']['name'][0])){
				 	 		
					 	 	//save photo to db
				 	 		$this->Agent_sites_model->update_logo_info_aws($logo);
				 	 	}

				 	 	$status = TRUE;
						$message = "Successfully updated logo!";

					} else{
						$status = FALSE;
						$message = "Logo: The filetype you are attempting to upload is not allowed!";

					}

				} else {
					$status = FALSE;
					$message = "Logo: The uploaded file exceeds the maximum upload filesize!";
				}

			}

			//upload agent favicon
			if(!empty($_FILES['userfile']['name'][1])){
				if($_FILES["userfile"]["error"][1] == 0 && $_FILES["userfile"]["size"][1] <= "5242880"){

					if( $_FILES["userfile"]["type"][1] == "image/jpeg" || $_FILES["userfile"]["type"][1] == "image/png" || $_FILES["userfile"]["type"][1] == "image/gif"){

						//generate random-id
						$filename = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['userfile']['name'][1]);
						$favicon = uniqid().'-'.($filename);
						$file = $_FILES["userfile"]["tmp_name"][1];
						$bucket_name = $this->config->item('bucket_name');
						$file_name = "uploads/favicon/".$favicon;
						$acl = "public-read";

				 	 	if(!empty($file)){

							//upload favicon to aws S3
				 	 		$this->s3->putObjectFile($file,$bucket_name,$file_name,$acl);
						}

				 	 	if(!empty($_FILES['userfile']['name'][1])){

					 	 	//save photo to db
					 	 	$this->Agent_sites_model->update_favicon_info_aws($favicon);
				 	 	}

				 	 	$status = TRUE;
						$message = "Successfully updated favicon!";

					} else{
						$status = FALSE;
						$message = "Favicon: The filetype you are attempting to upload is not allowed!";

					}

				} else {
					$status = FALSE;
					$message = "Favicon: The uploaded file exceeds the maximum upload filesize!";
				}
			}

			//upload agent photo
			if(!empty($_FILES['userfile']['name'][2])){
				if($_FILES["userfile"]["error"][2] == 0 && $_FILES["userfile"]["size"][2] <= "5242880"){

					if( $_FILES["userfile"]["type"][2] == "image/jpeg" || $_FILES["userfile"]["type"][2] == "image/png" || $_FILES["userfile"]["type"][2] == "image/gif"){

						//generate random-id
						$filename = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['userfile']['name'][2]);
						$agent_photo = uniqid().'-'.($filename);
						$file = $_FILES["userfile"]["tmp_name"][2];
						$bucket_name = $this->config->item('bucket_name');
						$file_name = "uploads/photo/".$agent_photo;
						$acl = "public-read";

				 	 	if(!empty($file)){

							//upload agent_photo to aws S3
				 	 		$this->s3->putObjectFile($file,$bucket_name,$file_name,$acl);
						}

				 	 	if(!empty($_FILES['userfile']['name'][2])){

					 	 	//save photo to db
				 	 		$this->Agent_sites_model->update_agent_photo_info_aws($agent_photo);
				 	 	}

						$status = TRUE;
						$message = "Successfully updated agent photo!";

					} else{
						$status = FALSE;
						$message = "Photo: The filetype you are attempting to upload is not allowed!";

					}

				} else {
					$status = FALSE;
					$message = "Photo: The uploaded file exceeds the maximum upload filesize!";
				}
			}

			$this->session->set_flashdata('session' , array('status' => $status,'message' => $message));

			redirect("agent_sites", "refresh");
		}

		$data["branding"] = $this->Agent_sites_model->get_branding_infos();
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["slider_photos"] = $this->Agent_sites_model->get_slider_photos();
		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","blogs/blogs.js","summernote-image-attributes.js","media/media.js","agent_sites.js");
		$data["css"] = array("dashboard/media.css");
		//printA($data["slider_photos"]); exit();


		$this->load->library('Ajax_pagination');

		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 30;

		$data["media"] = $this->media->get_all_media( FALSE, $param );
		$data['published_media'] = $this->media->get_all_published_media_new(array('start'=>$param["offset"],'limit'=>$param["limit"]));

		$total = $this->media->get_all_media( TRUE, $param );
		$config['target']   = '.insertMediaModal';
		$config["base_url"] = base_url().'/agent_sites/media_modal_pagination';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['uri_segment'] = 4;
		$config['query_string_segment'] = 'offset';

		$this->ajax_pagination->initialize($config);
		$data["pagination"] = $this->ajax_pagination->create_links();
		$data["sold_searchable"] = $this->checkSoldSearchable();

		$this->load->view('agent_site_info', $data);

	}

	 public function checkSoldSearchable(){

		$status = TRUE;

		$sold_endpoint = Modules::load("mysql_properties/mysql_properties");
		$currentSoldListings = $sold_endpoint->getCurrentOfficeSoldListings($this->session->userdata("agent_id"), 1,TRUE);

		if(isset($currentSoldListings["SparkQLErrors"][0]["Message"]) && isset($currentSoldListings["SparkQLErrors"][1]["Message"]) && $currentSoldListings["SparkQLErrors"][0]["Message"] == "The specified field is not searchable." && $currentSoldListings["SparkQLErrors"][1]["Message"] == "The specified field is not searchable."){

			$status = FALSE;
			//set show sold listings in db to 0
			$data = array('show_sold_listings'=> 0);
			$this->Agent_sites_model->save_sold_listings_options($data);

		}else{
			$status = TRUE;
		}

		return $status;
	}

	//=================== Start of AWS S3 Endpoints ==================//

	public function list_buckets() {

		//get List of Buckets
	 	printA($this->s3->listBuckets());exit;

	 	return $this->s3->listBuckets();
	}

	public function get_bucket( $bucket_name = "" ){

		$bucket_name = $this->config->item("bucket_name");
	  	//get specific buckets
	 	printA($this->s3->getBucket($bucket_name));exit;

	 	return $this->s3->getBucket($bucket_name);
		
	}

	public function create_bucket($bucket_name = ""){

		$bucket_name ="agentsquared-test";

	 	return $this->s3->putBucket($bucket_name);
		
	}

	public function delete_bucket($bucket_name =""){

		$bucket_name ="agentsquared-test";

	 	return $this->s3->deleteBucket($bucket_name);
		
	}

	public function put_object($file = "" ,$bucket_name = "",$file_name = "", $acl = ""){

		$file = "Test file upload!";
		$bucket_name = $this->config->item("bucket_name");
		$file_name = "test-upload";
		$acl = "public-read";

	 	return $this->s3->putObject($file,$bucket_name,$file_name,$acl);
		
	}

	public function put_object_file($file = "", $bucket_name = "", $file_name = "", $acl = ""){

		$file = FCPATH.'assets/upload/slider/test.png';
		$bucket_name = $this->config->item("bucket_name");
		$file_name = "uploads/logo/test2.png";
		$acl = "public-read";

	 	return $this->s3->putObjectFile($file,$bucket_name,$file_name,$acl);
		
	}

	public function delete_object($bucket_name = "", $uri = ""){

		$bucket_name = $this->config->item("bucket_name");
		$uri = "uploads/logo/599ea4b3ec611-brad-logo.png";

	 	return $this->s3->deleteObject($bucket_name,$uri);
		
	}

	//=================== END of AWS S3 Endpoints ==================//

	public function upload_slider_photo(){

			if(!empty($_FILES["slider_photo"]["name"])){

				if($_FILES["slider_photo"]["error"] == 0 && $_FILES["slider_photo"]["size"] <= "5242880" ){

					if( $_FILES["slider_photo"]["type"] == "image/jpeg" || $_FILES["slider_photo"]["type"] == "image/png" || $_FILES["slider_photo"]["type"] == "image/gif"){

						//generate random-id
						$photo_name = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['slider_photo']['name']);
						$photo = uniqid().'-'.($photo_name);
						$file = $_FILES["slider_photo"]["tmp_name"];
						$bucket_name = $this->config->item('bucket_name');
						$file_name = "uploads/slider/".$photo;
						$acl = "public-read";

						$data = array(
							"file_name" => $photo,
							"file_size" => $_FILES['slider_photo']['size'],
							"file_type" => $_FILES['slider_photo']['type'],
							"is_freemium" => "1",
						);

						if(!empty($file)){

							//upload slider_photo to aws S3 
				 	 		$this->s3->putObjectFile($file,$bucket_name,$file_name,$acl);
						}

						if(!empty($photo)){

							//save photo to db
							$photo_id = $this->Agent_sites_model->add_slider_photo($data);

							if($photo_id){

								$status = "success";
								$msg = "Slider Photo has been successfully added!";
					            $photo_id = $photo_id;
					            $photo_url = $photo;
					            $is_freemium = TRUE;

							} else {

								$status = "error";
								$msg = "Failed to upload slider photo!";
					            $photo_id = "";
					            $photo_url ="";
					            $is_freemium = TRUE;

							}
						}

					} else {

						$status = "error";
						$msg = "The filetype you are attempting to upload is not allowed!";
			            $photo_id = "";
			            $photo_url ="";
			            $is_freemium = TRUE;
					}

				} else{

					$status = "error";
		            $msg = "The uploaded file exceeds the maximum upload filesize!!";
		            $photo_id = "";
		            $photo_url ="";
		            $is_freemium = TRUE;

				}

				
			} else {

				$status = "error";
                $msg = "You did not select a file to upload!";
                $photo_id = "";
                $photo_url ="";
                $is_freemium = TRUE;
			}

	    echo json_encode(array('status' => $status, 'msg' => $msg, 'photo_id' => $photo_id, 'photo_url' => $photo_url, 'is_freemium' => $is_freemium));
	}

	public function delete_slider_photo(){

			$obj = Modules::load("mysql_properties/mysql_properties");
			$account_info = $obj->get_account($this->session->userdata("user_id"), "account_info");
			$mls_id = $account_info->MlsId;

			$default_slider_photo = array(
				$mls_id."-1.jpg", $mls_id."-2.jpg", $mls_id."-3.jpg",$mls_id."-4.jpg",$mls_id."-5.jpg"
			);
		
			if (in_array($_GET["photo_url"], $default_slider_photo)) {

				//delete slider photos on database only
			    if($this->Agent_sites_model->delete_slider_photo($_GET["photo_url"],$_GET["photo_id"])){
		        $status = 'success';
			        $msg = 'Successfully deleted slider photo';
			    } else {
			        $status = 'error';
			        $msg = 'Failed to delete slider photo';
			    }
			} else {

				//delete slider photos in amazon S3 and database
				$uri = "uploads/slider/".$_GET["photo_url"];
				$this->s3->deleteObject($this->config->item("bucket_name"),$uri);

			    if($this->Agent_sites_model->delete_slider_photo($_GET["photo_url"],$_GET["photo_id"])){

			        $status = 'success';
			        $msg = 'Successfully deleted slider photo';
			    } else {
			        $status = 'error';
			        $msg = 'Failed to delete slider photo';
			    }

			}

	    echo json_encode(array('status' => $status, 'msg' => $msg));exit;

	}

	public function upload_agent_photo(){

			if(!empty($_FILES)){

				if($_FILES["userfile"]["error"] == 0 && $_FILES["userfile"]["size"] <= "5242880" ){

					if( $_FILES["userfile"]["type"] == "image/jpeg" || $_FILES["userfile"]["type"] == "image/png" || $_FILES["userfile"]["type"] == "image/gif"){

						//generate random-id
						$filename = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['userfile']['name']);
						$photo = uniqid().'-'.($filename);
						$file = $_FILES["userfile"]["tmp_name"];
						$bucket_name = $this->config->item('bucket_name');
						$file_name = "uploads/photo/".$photo;
						$acl = "public-read";

						if(!empty($file)){
							//upload agent_photo to aws S3
				 	 		$this->s3->putObjectFile($file,$bucket_name,$file_name,$acl);
						}
						
				 	 	//save photo to db
				 	 	$this->Agent_sites_model->add_agent_photo_aws($photo);

			 	 		$status = TRUE;
						$message = "Agent Photo has been successfully updated!";

					} else {
						
						$status = FALSE;
						$message = "The filetype you are attempting to upload is not allowed!";
					}

				} elseif($_FILES["agent_photo"]["error"] == 1) {

					$status = FALSE;
					$message = "The uploaded file exceeds the maximum upload filesize!";
				}
				
			} else {

				$status = FALSE;
				$message = "You did not select a file to upload!";
			}


		$this->session->set_flashdata('session' , array('status' => $status,'message' => $message));

		if($this->config->item('agent_idx_website')) {
			redirect("dashboard", "refresh");
		} else {
			redirect("single_property_listings", "refresh");
		}

	}

	public function getInfo() {

		$data["branding"] = $this->Agent_sites_model->get_branding_infos();
		$data["slider_photos"] = $this->Agent_sites_model->get_slider_photos();

		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->session->userdata("user_id"), "account_info");

		$param["mlsID"] = $data["account_info"]->Id;

		$data["properties"] = $this->Agent_sites_model->get_your_properties_listings(FALSE, $param);

		return $data;
	}

	public function media_modal_pagination(){

		$this->load->library('Ajax_pagination');

		$page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }

        $param = array();
		$param["limit"] = 30;

		$data["media"] = $this->media->get_all_media( FALSE, $param );
		$data['published_media'] = $this->media->get_all_published_media_new(array('start'=>$offset,'limit'=>$param["limit"]));

		$total = $this->media->get_all_media( TRUE, $param );
		$config['target']   = '.insertMediaModal';
		$config["base_url"] = base_url().'/agent_sites/media_modal_pagination';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->ajax_pagination->initialize($config);
		$data["pagination"] = $this->ajax_pagination->create_links();

        $this->load->view('media_modal_pagination', $data);
    }

	public function insert_site_info($data) {
		$arr = array(
			'id' => $data['id'],
			'site_name' => $data['site_name'],
			'tag_line' => $data['tag_line'],
		);
		if( $_POST ) {

			if($this->Agent_sites_model->insert_info($arr)) {
				if( $_FILES )
				{
					$this->do_img_upload();
				}
			}
		}
	}

	public function spark_member()
	{
		$data["title"]  =  "Spark Member";
		$this->load->view('spark_member', $data);
	}

	public function saved_searches(){

		$data["title"] = "Saved Searches";
		$this->load->library('pagination');

		$dsl_featured_saved_search = Modules::run('dsl/getFeaturedAgentSavedSearch');
		$dsl_selected_saved_search = Modules::run('dsl/getSelectedAgentSavedSearch');

		//check if dsl featured savedsearch is not empty
		if(!empty($dsl_featured_saved_search)){
			$data["featured_saved_search"] = $dsl_featured_saved_search[0] ;
		}else{
			$data["featured_saved_search"] = $this->custom->get_featured_saved_search();
		}

		//check if dsl selected savedsearch is not empty
		if(!empty($dsl_selected_saved_search)){
			$data["selected_saved_search"] = $dsl_selected_saved_search ;
		}else{
			$data["selected_saved_search"] = $this->custom->get_selected_saved_search();
		}

		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 1;
		$param["limit"] = 24;
		$savedsearches = Modules::run('dsl/getAgentSavedSearch', array("page" => $param["offset"],  "limit" => $param["limit"]) );

		if(!empty($savedsearches["data"]) && !empty($savedsearches["count"])){

			$total = $savedsearches["count"];
			$config["base_url"] = base_url().'/agent_sites/saved_searches';
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';

			$data["savedsearches"] = $savedsearches["data"];
			$this->pagination->initialize($config);
		}

		$data['css'] = array('dashboard/bootstrap-datepicker.css', 'dashboard/bootstrap-datepicker.standalone.css', 'dashboard/bootstrap-datepicker3.css');
		$data["js"] = array("plugins/tablesorter/jquery.tablesorter.js");
		$data["pagination"] = $this->pagination->create_links();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		$data["branding"] = $this->Agent_sites_model->get_branding_infos();
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = $token->access_token;
		$saved_search = $this->saved_searches_live($access_token);
		$data["live_savedsearches"] = $saved_search["D"]["Results"];

		$this->load->view('saved-searches', $data);
	}

	public function edit_saved_search( $search_id = NULL )
	{
		if(empty($search_id)) return FALSE;

		if( $_POST )
		{
			$updateName = array(

			  'Name' => $this->input->post("search_name")
			);

			$dsl = modules::load('dsl/dsl');
			$updateSavedSearchName = $dsl->put_endpoint('savedsearch/'.$this->session->userdata("user_id").'/'.$search_id.'/update', $updateName);

			if( $updateSavedSearchName)
			{
				$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Search name has been successfully edited!'));
			}
			else
			{
				$this->session->set_flashdata('session' , array('status' => FALSE,'message'=>'Failed to edit name!'));
			}
		}

		redirect(base_url()."agent_sites/saved_searches", "refresh");

	}

	public function saved_search(){

		$data["title"] = "Saved Search";
		$search_id = $this->input->get('search_id');
		$this->load->library("idx_auth", $this->session->userdata("agent_id"));
		$page = ($this->input->get('page')) ? $this->input->get('page') : 1;
 		$data['limit'] = $limit = 12;
		$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

		if(isset($_GET['offset']) && !isset($_GET['page'])) {

 			$total_page = $totalrows / $limit;
 			$remainder = $totalrows % $limit;

 			if($remainder != 0){
 				$total_page++;
 			}

 			$total_page = intval($total_page);
 			$actual_page = $_GET['offset'] / $limit;
 			$actual_page = intval($actual_page);
 			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

 			if($actual_page < $total_page) {
				$page += $actual_page;
 			}
 		}

		$savedSearch = $this->idx_auth->getSavedSearch($search_id);

		$dsl = modules::load('dsl/dsl');
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
			'access_token' => $token->access_token
		);
		$filterStr = "(".$savedSearch[0]['Filter'].")";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "spark-listings?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$savedSearches['results'] = json_decode($results, true);
		$data['saved'] = $savedSearches['results'];

 		if(isset($data['saved']['pagination'])) {
			$data['current_page'] = $data['saved']['pagination']['CurrentPage'];
 			$data['totalrows'] = $data['saved']['pagination']['TotalRows'];
 		}

		$query_arr = array();

 		foreach($_GET as $key=>$val) {
 			if($key!="totalrows" && $key !="search_id" && $key !="offset") {
 				$query_arr[$key]=$val;
			}
 		}

 		if(isset($data['totalrows']) && isset($data['current_page'])) {
 			$query_arr['totalrows']=$data['totalrows'];
		}
 		$build_query = http_build_query($query_arr);

 		$this->load->library('pagination');

 		if(!isset($data['saved']["response"])) {
 			$data['total_pages'] = $data['saved']['pagination']['TotalPages'];
 			$data['current_page'] = $data['saved']['pagination']['CurrentPage'];
 			$data['total_count'] = $data['saved']['pagination']['TotalRows'];
 			$data['page_size'] = $data['saved']['pagination']['PageSize'];

 			$param["limit"] = 12;
 			$total = $data['saved']['pagination']['TotalRows'];
 			$config["base_url"] = base_url().'/agent_sites/saved_search?search_id='.$search_id.'&'.$build_query;
 			$config["total_rows"] = $total;
 			$config["per_page"] = $param["limit"];
 			$config['page_query_string'] = TRUE;
 			$config['query_string_segment'] = 'offset';

 			$this->pagination->initialize($config);
 		}

 		$data["pagination"] = $this->pagination->create_links();
		$data["properties"] = $savedSearches['results'];
		$data["saved_search_name"] = isset($savedSearch[0]["Name"]) ? $savedSearch[0]["Name"] : "";

		$data['css'] = array('dashboard/bootstrap-datepicker.css', 'dashboard/bootstrap-datepicker.standalone.css', 'dashboard/bootstrap-datepicker3.css');
		$data["branding"] = $this->Agent_sites_model->get_branding_infos();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$domain_info = $data["domain_info"];

        if(!empty($domain_info)) {
            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $base_url = base_url();
        }
	    $data["base_url"] = $base_url;

		$this->load->view('saved-search', $data);
	}

	public function saved_searches_live($access_token) {

		if($access_token) {
			$url = "https://sparkapi.com/v1/savedsearches";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $access_token,
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
			return $result_info;
		}
	}

	public function savedsearch_debugger(){

		$search_id = $_GET["search_id"];

		$this->load->library("idx_auth", $this->session->userdata("agent_id"));

		$savedSearch = $this->idx_auth->getSavedSearch($search_id);

		$dsl = modules::load('dsl/dsl');
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
			'access_token' => $token->access_token
		);

		$filterStr = "(".$savedSearch[0]['Filter'].")";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "spark-listings?&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$savedSearches['results'] = json_decode($results, true);

		printA($savedSearches['results']);exit;

  	}

  	public function cancel_agent_subscription() {

  		if($this->input->is_ajax_request()) {
  				
			$res = $this->Agent_sites_model->cancel_agent_subscription();

			if($res) {

				$ghl = new Gohighlevel; // create instance for GHL library

				$user = $ghl::agent_data($this->session->userdata('user_id')); // fetch agent data to be serve to GHL cancel campaign

				if($user) {

					$user['Cancellation_Date__c'] = date('Y-m-d'); // addition fields for cancel campagin
					$ghl->process($user, 'cancel');

				}

			}

			$response['success'] = ($res) ? TRUE : FALSE;

			exit(json_encode($response));

		}

  	}
  	
  	//Upload default slider photo!
  	public function upload_default_slider_photo($mls_id = NULL, $user_id = NULL) {
  		$count = 0;

  		//if MLS is CPAR add 5 default photos
			$default_mls_slider_5 = array(
				"20150128160104800461000000"
			);

  		//if MLS is MOMLS/SpaceCoast/GEPAR add 4 default photos
			$default_mls_slider_4 = array(
				"20140311223451933927000000",
				"20130226165731231246000000",
				"20180216183507684268000000"
			);
		
			if (in_array($mls_id, $default_mls_slider_5)){
				$photoCount = 4;
			} elseif (in_array($mls_id, $default_mls_slider_4)){
				$photoCount = 3;
			} else {
				$photoCount = 2;
			}

  		for ($x = 0; $x <= $photoCount; $x++) {
  			$count++;

				$data = array(
					"user_id" => $user_id,
					"file_name" => $mls_id.'-'.$count.".jpg",
					"file_size" => "174.9",
					"file_type" => "jpeg",
					"is_freemium" => "1",
				);
			//save photo to db
			$this->Agent_sites_model->insert_default_slider_photo($data);

		} 

		return TRUE;

	}

	//MANUAL UPLOAD per_user slider photo!
  	public function manual_upload_slider_photos_per_user(){

  		$user_id = $_GET["user_id"];
  		$mls_id = $_GET["mls_id"];

  		$default_slider_photos = $this->Agent_sites_model->check_default_slider_photos($mls_id,$user_id);
  		//printA($default_slider_photos);exit;

  		if(empty($default_slider_photos)){

  			$this->upload_default_slider_photo($mls_id,$user_id);
  		}else{

  			echo "Status: Default slider photos already added!";
  			printA($default_slider_photos);exit;
  		}
	}


	//one time MANUAL UPLOAD default slider photo!
  	public function manual_upload_default_slider_photos(){

  		$mls_id = $_GET["mls_id"];
  		$limit = isset($_GET["limit"]) ? $_GET["limit"] : "";
  		$print =  isset($_GET["print"]) ? $_GET["print"] : "";

  		$mls_users = $this->Agent_sites_model->get_mls_users($mls_id,$limit);

  		if($print){
  			printA($mls_users);
  		}
  		
  		if(!empty($mls_users)){
  			$count = 0;
  			foreach($mls_users as $mls){
  				$count++;

  				$default_slider_photos = $this->Agent_sites_model->check_default_slider_photos($mls_id,$mls->id);

  				$has_slider_photos = $this->Agent_sites_model->check_slider_photos($mls->id);

  				if($print){
		  			printA($has_slider_photos);exit;
		  		}

  				if(empty($default_slider_photos) && empty($has_slider_photos)){

		  			$this->upload_default_slider_photo($mls_id,$mls->id);
		  			printA($count.") Status: Successfully added default slider photos of ".$mls->id."!");
		  		}else{

		  			if($has_slider_photos[0]->photo_url == "armls-slider-photo1.jpeg" || $has_slider_photos[0]->photo_url == "michric-slider-photo1.jpg" ){
		  				printA($count.") Status: Default slider photos of ".$mls->id." already added!");
		  			}else{
		  				printA($count.") Status: Agent ".$mls->id." already added custom slider photo!");
		  			}

		  		}
	  			
	  		}
  		}
	}

	public function check_sold_listings_option() {
		$switch = $this->Agent_sites_model->get_sold_listings_option($this->session->userdata('agent_id'));
		exit(json_encode(array('show_sold_listings' => $switch->show_sold_listings)));
	}

	public function display_sold_listings() {

		if($this->input->post('switch_post')) {
			$post = $this->input->post('switch_post');
			if($post == 'show_sold_listings') {
				$data = array('show_sold_listings'=> 1);
			} else {
				$data = array('show_sold_listings'=> 0);
			}
			$this->Agent_sites_model->save_sold_listings_options($data);
		}
	}
  	
}
