<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Properties_model extends CI_Model { 

	public function __construct() {
        parent::__construct();
    }
	
    public function get_property($user_id=NULL,$type=NULL) {

        if($user_id && $type) {

            $this->db->select("listing")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    //remove this where clause after deleting listings(BLOB type) column in homepage properties
                    ->where("listing_id !=", "")
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }

    }

     public function get_property_old($user_id=NULL,$type=NULL) {

        if($user_id && $type) {

            $this->db->select("listings")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }

    }

    public function get_property_new($user_id=NULL, $type=NULL) {

        if($user_id && $type) {

            $this->db->select("listing")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    ->where("listing_id !=", "")
                    ->limit(1);

            $query = $this->db->get()->result();

            return ($query) ? $query : FALSE;
        }
    }

    public function insert_homepage_listings($data=array()) {

        if($data) {

            $this->db->where('user_id', $data['user_id']);
            $this->db->where('type', $data['type']);
            $this->db->where('listing_id', $data['listing_id']);
            $this->db->limit(1);

            $query = $this->db->get("homepage_properties");

            //insert or delete data 
            if($query->num_rows() > 0) {

               //delete data
               $this->db->where('user_id', $data['user_id']);
               $this->db->where('type', $data['type']);
               //remove this where clause after deleting listings(BLOB type) column in homepage properties
               $this->db->where("listing_id !=", "");
               $this->db->delete('homepage_properties');

               //update listings data type
                $arr = array(
                    'data_type' => "1",
                );
                $this->db->where('user_id', $data['user_id']);
                $this->db->update('homepage_properties', $arr);

               //insert data
               $this->db->insert("homepage_properties", $data);

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;

            } else {

                if(!empty($data['listing'])){

                    //update listings data type
                    $arr = array(
                        'data_type' => "1",
                    );
                    $this->db->where('user_id', $data['user_id']);
                    $this->db->update('homepage_properties', $arr);

                    $this->db->insert("homepage_properties", $data);
                }

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;
            }
                 
        }

        return FALSE;

    }

    public function insert_homepage_listings_old($data=array()) {

        if($data) {

            $this->db->where('user_id', $data['user_id']);
            $this->db->where('type', $data['type']);

            $query = $this->db->get("homepage_properties");

            if($query->num_rows() > 0) {

                $arr = array(
                    'listings' => $data['listings'],
                    'date_modified' => date('Y-m-d H:i:s')
                );

                if(!empty($arr["listings"])){
                    
                    $this->db->where('user_id', $data['user_id']);
                    $this->db->where('type', $data['type']);
                    $this->db->update('homepage_properties', $arr);
                }
                
                return ($this->db->affected_rows()>0) ? TRUE : FALSE;

            } else {

                $this->db->insert("homepage_properties", $data);

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;
            }
        }

    }
    public function delete_listings($data = array()){

        if($data){

           $arr = array(
                'user_id' => $data['user_id'],
                'type' => $data['type']
            );
           
           $this->db->where($arr);
           $this->db->delete('homepage_properties');

          return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
        }

        return FALSE;
    }

    public function insert_account($data=array()) {

        if($data) {

            $this->db->where('user_id', $data['user_id']);
            $this->db->where('type', $data['type']);

            $query = $this->db->get("account_meta");

            if($query->num_rows() > 0) {

                $arr = array(
                    'account' => $data['account'],
                    'date_modified' => date('Y-m-d H:i:s')
                );

                $this->db->where('user_id', $data['user_id']);
                $this->db->where('type', $data['type']);
                $this->db->update('account_meta', $arr);

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;

            } else {

                $this->db->insert("account_meta", $data);

                return ($this->db->affected_rows()>0) ? TRUE : FALSE;

            }
        }

    }

    public function get_account($user_id= NULL, $type=NULL) {

        if($type) {

            $this->db->select("account")
                    ->from("account_meta")
                    ->where("type", $type)
                    ->where("user_id",$user_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }

    }

    public function get_users_id() {

        $this->db->select("homepage_properties.user_id, users.agent_id")
                ->from("homepage_properties")
                ->join("users", "users.id = homepage_properties.user_id")
                ->group_by("homepage_properties.user_id");

        return $this->db->get()->result();
    }

    public function get_users_id_active() {

        $this->db->select("homepage_properties.user_id,users.agent_id,users_idx_token.agent_id,users_idx_token.is_valid")
                ->from("homepage_properties")
                ->join("users", "users.id = homepage_properties.user_id","left")
                ->join("users_idx_token", "users.agent_id = users_idx_token.agent_id","left")
                ->where("users_idx_token.agent_id !=", "")
                ->where("homepage_properties.data_type =","0")
                ->group_by("homepage_properties.user_id");

        return $this->db->get()->result();
    }

     public function get_users_id_bearer() {

        $this->db->select("homepage_properties.user_id, users.agent_id,user_bearer_token.agent_id")
                ->from("homepage_properties")
                ->join("users", "users.id = homepage_properties.user_id","left")
                ->join("user_bearer_token", "users.agent_id = user_bearer_token.agent_id","left")
                ->where("user_bearer_token.agent_id !=", "")
                ->where("homepage_properties.data_type =","0")
                ->group_by("homepage_properties.user_id");

        return $this->db->get()->result();
    }

    public function isHasProperties($data=array()) {

        $this->db->select("user_id")
                ->from("homepage_properties")
                ->where($data);

        $query = $this->db->get()->result();

        return (!empty($query)) ? TRUE : FALSE;
    }

    public function isHasAccountMeta($data=array()) {

        $this->db->select("user_id")
                ->from("account_meta")
                ->where($data);

        $query = $this->db->get()->result();

        return (!empty($query)) ? TRUE : FALSE;   

    }

    public function check_listing($user_id=NULL, $type=NULL) {

        if($user_id && $type) {

            $this->db->select("listing")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    ->where("listing_id !=", "")
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }
    }

    public function get_access_token($agent_id=NULL) {

        $ret = FALSE;

        if(!empty($agent_id)) {

            $this->db->select('access_token')
                    ->from('users_idx_token')
                    ->where('agent_id', $agent_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            if(!empty($query)) {
                
                $ret = $query;

            } else {

                $this->db->select('bearer_token as access_token')
                        ->from('user_bearer_token')
                        ->where('agent_id', $agent_id)
                        ->limit(1);

                $query = $this->db->get()->row();

                if(!empty($query))
                    $ret = $query;

            }

        }

        return $ret;

    }


    public function getAgentIdByUserId($user_id) {
         $this->db->select("agent_id")
                ->from("users")
                ->where("id", $user_id);
        return $this->db->get()->row();
    }

    public function getUserAccessTokenByUserId( $user_id = NULL ) {
        /* Distinguising between Bearer and Auth Token users */
        $this->db->select("auth_type, agent_id")
                ->from("users")
                ->where("id", $user_id);
        if ($ret = $this->db->get()->row()) {
            $type = $ret->auth_type;

            if($type) {
                $this->db->select("bearer_token as access_token")
                    ->from("user_bearer_token")
                    ->where("agent_id", $ret->agent_id);
            }
            else {
                $this->db->select("access_token")
                    ->from("users_idx_token")
                    ->where("agent_id", $ret->agent_id);
            }
            return $this->db->get()->row();
        }
        

        return false;
        
    }


    public function getUnUpdatedFeaturedSavedSearchListing() {

        $sql = "SELECT * FROM `featured_saved_search_listings` where DATE_FORMAT(date_modified, '%Y-%m-%d') < DATE_FORMAT(NOW(), '%Y-%m-%d') LIMIT 10";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getUnUpdatedFeaturedSavedSearchListingByUserId($user_id) {

        $sql = "SELECT * FROM `featured_saved_search_listings` where user_id = ". $user_id ." LIMIT 10";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function updateIsUpdatedFeaturedSavedSearch($id) {
        $this->db->where('id', $id);
        $data['date_modified'] = date('Y-m-d H:i:s');
        $this->db->update('featured_saved_search_listings', $data);

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }


    public function delete_homepage_listings($user_id="",$type=""){
       $this->db->where('user_id', $user_id);
       $this->db->where('type', $type);
       $this->db->delete('homepage_properties');

       return TRUE;
    }

    public function update_nearby_latlon($agent_id="",$data=array()) {

        if($data) {

            $latlon["latitude"] = $data["lat"];
            $latlon["longitude"] = $data["lon"];

            $this->db->where('agent_id', $agent_id);
            $this->db->update('users', $latlon);

            return ($this->db->affected_rows()>0) ? TRUE : FALSE;

        }

        return TRUE;
    }

     public function get_account_latlon($agent_id="") {

        $this->db->select("latitude,longitude")
                ->from("users")
                ->where("agent_id", $agent_id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function get_contacts($user_id=NULL, $agent_id=NULL) {

        if(!empty($user_id) && !empty($agent_id)) {

            $this->db->select('CONCAT(contacts.first_name, " ", contacts.last_name) as contact_name, contacts.email as contact_email, customer_searches.csid, customer_searches.json_search, customer_searches.count')
                    ->from('customer_searches')
                    ->join('contacts', 'contacts.id=customer_searches.customer_id', 'inner')
                    ->where('customer_searches.user_id', $user_id)
                    ->where('contacts.user_id', $user_id)
                    ->where('contacts.agent_id', $agent_id)
                    ->where('contacts.is_deleted', 0)
                    ->where('contacts.is_subscribe_email', 1);

            return $this->db->get()->result();
            
        }

        return FALSE;

    }

    public function update_search_count($csid=NULL, $count=NULL) {

        if(!empty($csid) && !empty($count)) {

            $this->db->where('csid', $csid);
            $this->db->update('customer_searches', array('count'=>$count));

            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }  

        return FALSE;

    }

    public function get_agent_info($user_id=NULL) {

        $ret = FALSE;

        if(!empty($user_id)) {

            $this->db->select('u.first_name, u.last_name, u.email, u.mls_name, u.phone, u.agent_photo, u.logo, u.company, d.domains')
                    ->from('users u')
                    ->join('domains d', 'd.agent=u.id', 'inner')
                    ->join('freemium f', 'f.subscriber_user_id=u.id', 'inner')
                    ->where('u.id', $user_id)
                    ->where('u.trial', 0)
                    ->where('f.plan_id', 2)
                    ->limit(1);

            $query = $this->db->get()->row();

            if(!empty($query))
                $ret = $query;

        }

        return $ret;

    }

    public function agent_scale($data=array()) {

        if(!empty($data)) {

            $this->db->select('id')
                    ->from('agent_listings_scale')
                    ->where('agent_id', $data['agent_id'])
                    ->limit(1);

            $query = $this->db->get()->row();

            if(!empty($query)) {

                $this->db->where('agent_id', $data['agent_id']);
                $this->db->update('agent_listings_scale', $data);

            } else {
                $this->db->insert('agent_listings_scale', $data);
            }

            return TRUE;

        }

        return FALSE;

    }

    public function get_gsf_fields($agent_id = NULL){
        $this->db->select("global_search_filters_string,gsf_standard")
            ->from("settings")
            ->where("agent_id", $agent_id)
            ->limit(1);
        
        $query = $this->db->get()->row();
        if(!empty($query)){
            if(isset($query->global_search_filters_string) && isset($query->gsf_standard)){
                return array('gsf_custom_fields' => $query->global_search_filters_string, 'gsf_standard_fields' => $query->gsf_standard);
            }
            return false;
        }
        return false;
    }


    public function check_sold_listings_data($user_id = NULL, $type = NULL){

        $this->db->select("user_id")
                ->from("sold_listings_data")
                ->where("user_id", $user_id)
                ->where("type", $type)
                ->limit(1);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? TRUE : FALSE;
    }

    public function check_date_sold($user_id = NULL, $type = NULL){

        $this->db->select("date_sold")
                ->from("sold_listings_data")
                ->where("user_id", $user_id)
                ->where("type", $type)
                ->limit(1);

        $query =  $this->db->get()->row()->date_sold;

        return (!empty($query)) ? TRUE : FALSE;
    }

    public function save_sold_listings_data( $user_id = NULL,$datas = NULL,$type = NULL){

        $save["listings"] = json_encode($datas);
        $save["listing_id"] = $datas['Id'];
        $save["date_sold"] = ($datas['StandardFields']['CloseDate'] != "********") ? $datas['StandardFields']['CloseDate'] : NULL ;
        $save["user_id"] = $user_id;
        $save["type"] = $type;
        $save["date_created"] = date("Y-m-d H:m:s");

        $this->db->insert( "sold_listings_data", $save );   

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }

    public function update_sold_listings_data( $user_id = NULL,$datas = NULL,$type = NULL){

        $update["listings"] = json_encode($datas);
        $update["date_sold"] = ($datas['StandardFields']['CloseDate'] != "********") ? $datas['StandardFields']['CloseDate'] : NULL ;
        $update["date_modified"] = date("Y-m-d H:m:s");

        $this->db->where('user_id', $user_id);
        $this->db->where('listing_id', $datas["Id"]);
        $this->db->where('type', $type);
        $this->db->update( "sold_listings_data", $update );  

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }

    public function insert_date_sold( $user_id = NULL,$datas = NULL,$type = NULL){

        $insert["date_sold"] = ($datas['StandardFields']['CloseDate'] != "********") ? $datas['StandardFields']['CloseDate'] : NULL ;
        $insert["date_modified"] = date("Y-m-d H:m:s");

        $this->db->where('user_id', $user_id);
        $this->db->where('listing_id', $datas["Id"]);
        $this->db->where('type', $type);
        $this->db->update( "sold_listings_data", $insert );  

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }

    public function sold_listings_data_db_count( $user_id = NULL,$type = NULL){

        $this->db->select("listings")
                ->from("sold_listings_data")
                ->where("user_id", $user_id)
                ->where("type", $type);

        return $this->db->count_all_results();
    }

     public function delete_sold_listings_data( $user_id = NULL,$type = NULL){

       $this->db->where('user_id', $user_id);
       $this->db->where('type', $type);
       $this->db->delete('sold_listings_data');

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }

    public function checkSoldFromHomepageProp(){

        $this->db->select("hp.user_id")
                ->from("users u")
                ->join("homepage_properties hp","hp.user_id = u.id","left")
                ->join("users_idx_token uit","uit.agent_id = u.agent_id","left")
                ->where("hp.type","sold")
                ->where("uit.is_valid","1")
                ->group_by("hp.user_id");

        return  $this->db->get()->result_array();
    }

    public function checkSoldFromSoldListingsData(){

        $this->db->select("user_id")
                ->from("sold_listings_data")
                ->group_by("user_id");

        return  $this->db->get()->result_array(); 
    }

    public function getAgentIdPlanIdByUserId($user_id = NULL){

        $this->db->select("u.id as user_id,u.agent_id,f.plan_id,as.EventType,uit.is_valid")
                ->from("users u")
                ->join("freemium f","f.subscriber_user_id = u.id","left")
                ->join("agent_subscription as","as.UserId = u.agent_id","left")
                ->join("users_idx_token uit","uit.agent_id = u.agent_id","left")
                ->where("as.EventType", "purchased")
                ->where("u.id", $user_id)
                ->group_by("u.id");

        return  $this->db->get()->row_array();
    }

    public function getUsersPremium(){

        $this->db->select("u.id as user_id,u.agent_id,f.plan_id,as.EventType,uit.is_valid,sld.date_sold")
                ->from("users u")
                ->join("freemium f","f.subscriber_user_id = u.id","left")
                ->join("agent_subscription as","as.UserId = u.agent_id","left")
                ->join("users_idx_token uit","uit.agent_id = u.agent_id","left")
                ->join("sold_listings_data sld","sld.user_id = u.id","left")
                ->where("as.EventType", "purchased")
                ->where("f.plan_id","2")
                ->group_by("u.id");

        return  $this->db->get()->result_array();
    }

    public function getUsersFreemium(){

        $this->db->select("u.id as user_id,u.agent_id,f.plan_id,as.EventType,uit.is_valid,sld.date_sold")
                ->from("users u")
                ->join("freemium f","f.subscriber_user_id = u.id","left")
                ->join("agent_subscription as","as.UserId = u.agent_id","left")
                ->join("users_idx_token uit","uit.agent_id = u.agent_id","left")
                ->join("sold_listings_data sld","sld.user_id = u.id","left")
                ->where("as.EventType", "purchased")
                ->where("f.plan_id","1")
                ->group_by("u.id");

        return  $this->db->get()->result_array();
    }
  
    public function checkLastModified($user_id = NULL,$type = NULL){

        $this->db->select("date_modified,date_created")
                ->from("sold_listings_data")
                ->where("user_id",$user_id)
                ->where("type",$type)
                ->limit(1);

        if($type == "new"){
          return     $this->db->get()->row()->date_modified; 
        }else{
          return    $this->db->get()->row()->date_created;  
        }
    }
}

