<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/sync/register.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="<?= base_url()?>assets/js/sync/modernizr.custom.js"></script>
</head>
<body>
    <section class="content-wrapper section-loader">
        <div class="container">
            <div class="section-load">
                <div class="as-brand">
                    <img src="<?= base_url().'assets/images/logo-brand-no-shadow'?>">
                </div>
                <p>
                    Please wait while we are syncing your data..
                </p>
                <div class="text-center">
                    <img src="<?= base_url().'assets/img/sync/squares.gif'?>">
                </div>
            </div>
        </div>
    </section>

    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $dataString = {'user_id':<?=$user_id?>, 'agent_id': '<?=$agent_id?>'};
            console.log($dataString);

            $.ajax({
                type: "POST",
                url: "<?=base_url();?>mysql_properties/process_sync",
                data: $dataString,
                dataType: "json",
                beforeSend: function() {
                    console.log("syncing");
                },
                success: function(data) {
                    console.log(data);
                    if(data.success) {
                        window.location.href = "<?=base_url();?>dashboard";
                    }
                }
            });
        });
    </script>
</body>
</html>