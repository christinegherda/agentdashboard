<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mysql_properties extends MX_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->model("properties_model", "pm");
		$this->load->model("dsl/dsl_model", "dsl");
		$this->load->model("crm/Leads_model", "leads");
		$this->load->model("customize_homepage/Customize_homepage_model","custom");
		//$this->output->enable_profiler(TRUE);
	}

	//listings data checker for syncing listings data
	public function check_sync_data(){

		$info_count = count($this->pm->get_users_id_active());
		$bearer_count = count($this->pm->get_users_id_bearer());
		$info = $this->pm->get_users_id_active();
		$bearer = $this->pm->get_users_id_bearer(); 

		$data = array(
			"access_token" => $info_count,
			"bearer_token" => $bearer_count,
			"access_token_data" => $info,
			"bearer_token_data" => $bearer
		);

		printA($data);exit;
	}

	//this is one time function for old users to sync thier listings in database
	public function sync_listings_data_per_user(){

		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];

		if(!empty($user_id) && !empty($agent_id) ){

			$this->active_listings($user_id,$agent_id);
			$this->sold_listings($user_id);
			$this->new_listings($user_id,$agent_id);
			$this->office_listings($user_id,$agent_id);
			$this->nearby_listings($user_id,$agent_id);
		}

	}

	//this is one time function for old users to sync thier listings in database->(BEARER TOKEN)
	public function sync_listings_data_bearerToken(){

		$bearer = $this->pm->get_users_id_bearer(); 
		//printA($bearer);exit;

		if( !empty($bearer) ){
			$i = 0;
			foreach ($bearer as $key) {

				if($i == 10) break;

				if(!empty($key->user_id) && !empty($key->agent_id) ){

					$this->active_listings($key->user_id,$key->agent_id);
					$this->sold_listings($key->user_id);
					$this->new_listings($key->user_id,$key->agent_id);
					$this->office_listings($key->user_id,$key->agent_id);
					$this->nearby_listings($key->user_id,$key->agent_id);
				}

				$i++;
			}
		}
	}

	//this is one time function for old users to sync thier listings in database->(ACCESS_TOKEN)
	public function sync_listings_data_accessToken(){

		$info = $this->pm->get_users_id_active();
		//printA($info);exit;

		if( !empty($info) ){
			$i = 0;
				foreach ($info as $key){

					if($i == 10) break;

					if(!empty($key->user_id) && !empty($key->agent_id) ){

						$this->active_listings($key->user_id,$key->agent_id);
						$this->sold_listings($key->user_id);

						if($key->is_valid == 1){

							$this->new_listings($key->user_id,$key->agent_id);
							$this->office_listings($key->user_id,$key->agent_id);
							$this->nearby_listings($key->user_id,$key->agent_id);

						}
					}

					$i++;

				}
			
		}
	}

	//added in cron job to update listings to the database
	public function pull_new_property_data_per_user_aws(){
		
		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];

		if( !empty($user_id) && !empty($agent_id) )
		{
			//$this->active_listings($user_id,$agent_id);
			//$this->sold_listings($user_id);
			$this->get_my_listings($agent_id);
			$this->get_sold_listings($agent_id);
			$this->new_listings($user_id,$agent_id);
			$this->office_listings($user_id,$agent_id);
			$this->nearby_listings($user_id,$agent_id);
			//exit(json_encode(array("success" => TRUE)));
		}
	}

	public function new_listings_alert() { //send new listings alert to agent's customer based on seach they saved

		if(isset($_GET['user_id']) && !empty($_GET['user_id']) && isset($_GET['agent_id']) && !empty($_GET['agent_id'])) {

			$user_id = $_GET['user_id'];
			$agent_id = $_GET['agent_id'];

			$contacts = $this->pm->get_contacts($user_id, $agent_id);
			//printA($contacts);
			$agent_info = $this->pm->get_agent_info($user_id);
			//printA($agent_info);

			$account_info = $this->get_account_info($agent_id);
			if(!empty($contacts) && !empty($agent_info)) {

				$this->load->library('email');
				$obj = modules::load('dsl/dsl');

				$token = $this->dsl->getUserAccessToken($agent_id);

				$access_token = array(
					'access_token' => $token->access_token,
					'bearer_token' => ''
				);

				foreach($contacts as $contact) {
					//$params = json_decode('{"Search":"chandler"}');
					$params = json_decode($contact->json_search);

					$lastday = gmdate('Y-m-d\TH:i:s\Z', strtotime('- 1 day'));
					$today =  gmdate('Y-m-d\TH:i:s\Z');

					$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold') And (OnMarketDate Bt ".$lastday.",".$today.") And (PublicRemarks Eq '*".$params->Search."*' Or UnparsedAddress Eq '*".$params->Search."*')";

					$filter_url = rawurlencode($filterStr);
					$filter_string = substr($filter_url, 3, -3);

					$endpoint = "spark-listings?_limit=3&_pagination=1&_page=1&_filter=(".$filter_string.")&_orderby=-OnMarketDate&_expand=PrimaryPhoto";

					$results = $obj->post_endpoint($endpoint, $access_token);
					$result_listings = json_decode($results, true);

					if(isset($result_listings['pagination']['TotalRows']) && $result_listings['pagination']['TotalRows'] > 0) {

						$count = $result_listings['pagination']['TotalRows'];
						$this->pm->update_search_count($contact->csid, $count); //update search current search count

						if($result_listings['pagination']['TotalRows'] >= $contact->count) {
							$count -= $contact->count;
						}

						if($count<=0)
							continue; //proceeds to next contact

						$total = $count;
						$redirect_url = $agent_info->domains.'/home/new_listings_search?Search='.$params->Search.'&from_time='.$lastday.'&to_time='.$today;
						$email_str = '';
						$count = ($count>3) ? 3 : $count;

						for($i=0; $i<$count; $i++) {

							$prop_url 		= $agent_info->domains.'/other-property-details/'.$result_listings['data'][$i]['StandardFields']['ListingKey'];
							$prop_address 	= $result_listings['data'][$i]['StandardFields']['UnparsedAddress'];
							$prop_photo 	= isset($result_listings['data'][$i]['Photos']['Uri300']) && !empty($result_listings['data'][$i]['Photos']['Uri300']) ? $result_listings['data'][$i]['Photos']['Uri300'] : getenv('AGENT_DASHBOARD_URL').'assets/images/image-not-available.jpg';
							$bed_total 		= $result_listings['data'][$i]['StandardFields']['BedsTotal'];
							$bath_total 	= $result_listings['data'][$i]['StandardFields']['BathsTotal'];
							$bldg_area 		= is_numeric($result_listings['data'][$i]['StandardFields']['BuildingAreaTotal']) ? number_format($result_listings['data'][$i]['StandardFields']['BuildingAreaTotal']) : $result_listings['data'][$i]['StandardFields']['BuildingAreaTotal'];
							$lot_area 		= is_numeric($result_listings['data'][$i]['StandardFields']['LotSizeSquareFeet']) ? number_format($result_listings['data'][$i]['StandardFields']['LotSizeSquareFeet']) : $result_listings['data'][$i]['StandardFields']['LotSizeSquareFeet'];
							$current_price 	= is_numeric($result_listings['data'][$i]['StandardFields']['CurrentPrice']) ? number_format($result_listings['data'][$i]['StandardFields']['CurrentPrice']) : $result_listings['data'][$i]['StandardFields']['CurrentPrice'];
							$public_remarks = $result_listings['data'][$i]['StandardFields']['PublicRemarks'];

							$email_str .= '<table>
											<thead>
												<tr>
													<th colspan="3" style="text-align:center">
														<a href="'.$prop_url.'" target="_blank">
															<h3 style="margin-bottom:10px;">'.$prop_address.'</h3>
														</a>
													</th>
												</tr>
											</thead>
											<tbody>
											  	<tr>
												    <td>
												    	<a href="'.$prop_url.'" target="_blank">
												    		<img src="'.$prop_photo.'" style="width:100%; height:auto;">
												    	</a>
												    </td>
											  	</tr>
											  	<tr>
											  		<td style="font-family: Segoe UI,Tahoma,Helvetica,Arial,sans-serif; padding: 12px 0 5px 30px; font-size: 14px; color: #969696; font-weight:bold">'
											  			.$bed_total.' beds | '
											  			.$bath_total.' baths | '
											  			.$bldg_area.' sqft | '
											  			.$lot_area.' lot | 
											  			<span style="color: #1891f6; font-size: 16px;"> $ '.$current_price.'</span>
											  		</td>
											  	</tr>
											  	<tr>
											  		<td style="
											  			font-family: Segoe UI,Tahoma,Helvetica,Arial,sans-serif;
											  			padding: 10px 30px 6px;
											  			color: #515666;
											  			font-size: 14px;
											  			line-height: 150%;">'
											  			.$public_remarks.
											  		'</td>
											  	</tr>
											</tbody>
										</table>';


						}

						$this->email->send_email_v3(
				        	'support@agentsquared.com', //sender email
				        	'AgentSquared', //sender name
				        	$contact->contact_email, //recipient email
				        	'honradokarljohn@gmail.com,aldwinj12345@gmail.com,joce@agentsquared.com', //bccs
				        	'85ae4f9b-8736-4096-b135-3d1a3bf4648f', //'1302de8d-8107-4ec3-a0d0-39499fc6f5df', //template_id
				        	array(
				        		'subs' => array(
				    				'customer_name' => $contact->contact_name,
				    				'agent_name' 	=> $agent_info->first_name.' '.$agent_info->last_name,
				    				'listing_count' => (string) $total,
				    				'search_query' 	=> $params->Search,
				    				'new_listing_html_template' => $email_str,
				    				'redirect_url' 	=> $redirect_url,
				    				'agent_mls'		=> $agent_info->mls_name,
				    				'agent_phone' 	=> $agent_info->phone,
				    				'agent_email' 	=> $agent_info->email,
				    				'agent_site'	=> $agent_info->domains,
				    				'agent_company'	=> $account_info->Office,
				    				'agent_website_url'	=> 'http://'.$agent_info->domains,
				    				'agent_website_photo' => !empty($agent_info->agent_photo) ? getenv('AGENT_DASHBOARD_URL').'assets/uploads/photo/'.$agent_info->agent_photo : getenv('AGENT_DASHBOARD_URL').'assets/images/no-profile-img.gif',
				    				'logo_url' => !empty($agent_info->logo) ? getenv('AGENT_DASHBOARD_URL').'assets/uploads/logo/'.$agent_info->logo : getenv('AGENT_DASHBOARD_URL').'assets/images/email-logo.png'
				        		)
							)
				        );
						
					}

				}

			}

		}

		return TRUE;

	}

	// //added in cron job to update new listings to the database every 10 mins.
	// public function sync_new_listings_data(){

	// 	//return true;
	// 	//NEW LISTINGS is now using proxy(spark-listings endpoint). Comment this method to prevent update in db.

	// 	$user_id = $_GET["user_id"];
	// 	$agent_id = $_GET["agent_id"];
		
	// 	if( !empty($user_id) && !empty($agent_id) ){
	// 		$this->new_listings($user_id,$agent_id);
	// 	}
	// }

	//added in cron job to update office listings to the database every 10 mins.
	// public function sync_office_listings_data(){

	// 	$user_id = $_GET["user_id"];
	// 	$agent_id = $_GET["agent_id"];
		
	// 	if( !empty($user_id) && !empty($agent_id) ){

	// 		$this->office_listings($user_id,$agent_id);
	// 	}
	// }

	//added in cron job to update nearby listings to the database every 10 mins.
	// public function sync_nearby_listings_data(){

	// 	$user_id = $_GET["user_id"];
	// 	$agent_id = $_GET["agent_id"];
		
	// 	if( !empty($user_id) && !empty($agent_id) ){

	// 		$this->nearby_listings($user_id,$agent_id);
	// 	}
	// }

  	public function sync_save_contact_list_by_agent_id() {

  		$agent_id = (isset($_GET['agent_id']) && !empty($_GET['agent_id'])) ? $_GET['agent_id'] : '';//$this->input->get('agent_id');

    	if (!empty($agent_id)) {

			$this->load->library("idx_auth", $agent_id);
			ini_set('max_execution_time', 300);
			$contacts_1 = $this->idx_auth->GetContacts(1);

			if(isset($contacts_1["pagination"]["TotalPages"]) && !empty($contacts_1["pagination"]["TotalPages"])) {

				$this->db->trans_start();

		      	for($i=1; $i <= $contacts_1["pagination"]["TotalPages"]; $i++) {

		        	$contactsave = $this->idx_auth->GetContacts($i);

		        	if(!empty($contactsave["results"])) {
		          		foreach ($contactsave["results"] as $contacts) {
		            		if(!$this->leads->is_contacts_exists($contacts)) {
		              			$this->leads->save_contacts( $contacts );
		            		}
		          		}
		        	}
		      	}

		      	$this->db->trans_complete();
		   	}

    	}
  	}

	public function pull_new_account_data_per_user($user_id = NULL){
		if( !empty($user_id) )
		{
			$this->account_info($user_id);
			$this->system_info($user_id);

			//exit(json_encode(array("success" => TRUE)));
		}
	}

	public function pull_new_property_data()
	{
		//$info = $this->pm->get_users_id();
		$info = $this->pm->get_users_id_active();

		if( !empty($info) )
		{
			foreach ($info as $key) {

				$this->active_listings($key->user_id,$key->agent_id);
				$this->sold_listings($key->user_id);

				if($key->is_valid == 1){

					$this->new_listings($key->user_id,$key->agent_id);
					$this->office_listings($key->user_id,$key->agent_id);
					$this->nearby_listings($key->user_id,$key->agent_id);

				}

				//
				sleep(15);
			}
		}

		exit(json_encode(array("success" => TRUE)));
	}

	public function isUpdatedListings($user_id="425", $agent_id=""){
		$this->load->library("idx_auth","");
		$dbListings = $this->get_properties($user_id,'active');
		$params = array(
			'_pagination' => TRUE,
			'_page' => 1,
			'_limit' => 4

			);
		$sparkListings = $this->idx_auth->GetMyListings($params);

		printA($sparkListings); exit;
	}

	public function pull_new_account_data()
	{

		$info = $this->pm->get_users_id();

		if( !empty($info) )
		{
			foreach ($info as $key) {
				$this->account_info($key->user_id);
				$this->system_info($key->user_id);

				sleep(15);
			}
		}

		exit(json_encode(array("success" => TRUE)));
	}

	public function active_listings($user_id=NULL,$agent_id=NULL) {

		if($user_id) {

			$activeList = array();

			$mls_status = Modules::run('dsl/get_mls_statuses',$agent_id);



			$endpoint = "properties/".$user_id."/status/".$mls_status."?_limit=4&_orderby=-OnMarketDate";
			$active_listings = Modules::run('dsl/get_endpoint', $endpoint);
			$activeListingData = json_decode($active_listings);

			if(isset($activeListingData->data) && !empty($activeListingData->data)){
				foreach ($activeListingData->data as $active){
					$active->standard_fields[0]->date_created = $active->date_created;
			 		$activeList[] = $active->standard_fields[0];
			 	}
			}

			if(isset($activeListingData->data) && !empty($activeListingData->data)){

				for ($x = 0; $x <= count($activeList); $x++) {

					$count = 0;

				   foreach($activeList as $active){

				   		if($count < 4) { 

							$data = array(
								'user_id'	=> $user_id,
								'listing_id' => $active->Id,
								'listing' 	=> json_encode($active),
								'type' 		=> 'active',
								'data_type' 		=> '1',
								'date_created' => date('Y-m-d H:i:s')
							);


							$this->pm->insert_homepage_listings($data);

						} $count++;
					}

				} 

			}
		}
	}

	public function sold_listings($user_id=NULL) {

		if($user_id) {

			$soldList = array();

			$endpoint = "properties/".$user_id."/sold"."?_limit=8&_orderby=-OnMarketDate";
			$sold_listings = Modules::run('dsl/get_endpoint', $endpoint);
			$soldListingData = json_decode($sold_listings);

			$isCheck = $this->getStandardMlsStatus($user_id);

			if($isCheck) {
				if(!empty($soldListingData)) {
				 	foreach ($soldListingData->data as $sold) {
				 		$soldList[] = $sold->standard_fields[0];
				 	}
				}
			}

			if(!empty($soldList)) {

				for ($x = 0; $x <= count($soldList); $x++) {

				   foreach($soldList as $sold){

						$data = array(
							'user_id'	=> $user_id,
							'listing_id' => $sold->Id,
							'listing' 	=> json_encode($sold),
							'type' 		=> 'sold',
							'data_type' 		=> '1',
							'date_created' => date('Y-m-d H:i:s')
						);

						$this->pm->insert_homepage_listings($data);
					}

				} 
			}
		}

	}

	public function new_listings($user_id=NULL, $agent_id=NULL) {

		if($user_id && $agent_id) {

			$obj = modules::load('dsl/dsl');
			$token = $this->dsl->getUserAccessToken($agent_id);

			$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ''
			);
			$gsf_fields = $this->pm->get_gsf_fields($agent_id);
			$filterGeo = '';
			//subtract 1 day from today's date
			$lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
			$today =  gmdate("Y-m-d\TH:i:s\Z");
			
			$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented') And (OnMarketDate Bt ".$lastday.",".$today.")";
			if(isset($gsf_fields['gsf_standard_fields']) && !empty($gsf_fields['gsf_standard_fields'])){
				$gsf_standard_fields = json_decode($gsf_fields['gsf_standard_fields']);
				if(isset($gsf_standard_fields->PropertyClass)){
					$filterStr .= " And ( ";
					for ($i=0; $i < count($gsf_standard_fields->PropertyClass); $i++) { 
						if($i == 0){
							$filterStr .= "PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";
						}else{
							$filterStr .= " Or PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";						
						}
					}
					$filterStr .= " )";				
				}

				if(isset($gsf_standard_fields->City)){
					for ($i=0; $i < count($gsf_standard_fields->City); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."'" : " City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."' ";
					}						
				}

				if(isset($gsf_standard_fields->CountyOrParish)){				
					for ($i=0; $i < count($gsf_standard_fields->CountyOrParish); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."'" : " CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."' ";					
					}				
				}

				if(isset($gsf_standard_fields->PostalCode)){
					for ($i=0; $i < count($gsf_standard_fields->PostalCode); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."'" : " PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."' ";
					}							
				}
			}
			
			if($filterGeo != ''){
				$filterStr .= " AND (".$filterGeo.")";
			}
			if(isset($gsf_fields['gsf_custom_fields']) && !empty($gsf_fields['gsf_custom_fields'])){
				$filterStr .= " And ".$gsf_fields["gsf_custom_fields"];
			}
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);
			//printA($filter_string);exit;

			$endpoint = "spark-listings?_limit=10&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=-OnMarketDate";
			//$endpoint = "spark-listings?_limit=4&".$filter_string;

			$new_listings = $obj->post_endpoint($endpoint, $access_token);
			$newlistingData = json_decode($new_listings);

			if(isset($newlistingData->data) && !empty($newlistingData->data)) {
				foreach ($newlistingData->data as $new){
					$newList[] = $new;
				}
			}

			if(!empty($newList)) {

				for ($x = 0; $x <= count($newList); $x++) {

					$count = 0;

				   foreach($newList as $new){

				   		if($count < 4) { 

							$data = array(
								'user_id'	=> $user_id,
								'listing_id' => $new->StandardFields->ListingKey,
								'listing' 	=> json_encode($new),
								'type' 		=> 'new',
								'data_type' 		=> '1',
								'date_created' => date('Y-m-d H:i:s')
							);

							$this->pm->insert_homepage_listings($data);

						} $count++;
					}

				} 

			}
		}

	}

	public function office_listings($user_id=NULL, $agent_id=NULL) {
		
		if($user_id && $agent_id) {

			$obj = modules::load('dsl/dsl');
			$officeList = array();
			$token = $this->dsl->getUserAccessToken($agent_id);

			$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ''
			);

			$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);
		
			$endpoint = "office/listings/?_limit=6&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=-OnMarketDate";
			$office_listings = $obj->post_endpoint($endpoint, $access_token);
			$officelistingData = json_decode($office_listings,true);
	

			if(isset($officelistingData['data']) && !empty($officelistingData['data'])) {
				foreach ($officelistingData['data'] as $office){
					$officeList[] = $office;
				}
			}

			if(!empty($officeList)) {

				//delete office listings
				$this->pm->delete_homepage_listings($user_id,'office');

				for ($x = 0; $x <= count($officeList); $x++) {

					$count = 0;

				    foreach($officeList as $office){

					   	if($count < 6) { 

							$data = array(
								'user_id'	=> $user_id,
								'listing_id' => $office["StandardFields"]["ListingKey"],
								'listing' 	=> json_encode($office),
								'type' 		=> 'office',
								'data_type' 		=> '1',
								'date_created' => date('Y-m-d H:i:s')
							);

							$this->pm->insert_homepage_listings($data);

						} $count++;

					} 

				} 
			}

		}

	}

	public function nearby_listings($user_id=NULL, $agent_id=NULL) {

		if($user_id && $agent_id) {

			$obj = modules::load('dsl/dsl');

			$nearbyList = array();
			$token = $this->dsl->getUserAccessToken($agent_id);
			$access_token = array('access_token' => $token->access_token);
			$gsf_fields = $this->pm->get_gsf_fields($agent_id);
			$filterGeo = '';
			
			$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";
			if(isset($gsf_fields['gsf_standard_fields']) && !empty($gsf_fields['gsf_standard_fields'])){
				$gsf_standard_fields = json_decode($gsf_fields['gsf_standard_fields']);
				if(isset($gsf_standard_fields->PropertyClass)){
					$filterStr .= " And ( ";
					for ($i=0; $i < count($gsf_standard_fields->PropertyClass); $i++) { 
						if($i == 0){
							$filterStr .= "PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";
						}else{
							$filterStr .= " Or PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";						
						}
					}
					$filterStr .= " )";				
				}

				if(isset($gsf_standard_fields->City)){
					for ($i=0; $i < count($gsf_standard_fields->City); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."'" : " City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."' ";
					}						
				}

				if(isset($gsf_standard_fields->CountyOrParish)){				
					for ($i=0; $i < count($gsf_standard_fields->CountyOrParish); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."'" : " CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."' ";					
					}				
				}

				if(isset($gsf_standard_fields->PostalCode)){
					for ($i=0; $i < count($gsf_standard_fields->PostalCode); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."'" : " PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."' ";
					}							
				}
			}
			
			if($filterGeo != ''){
				$filterStr .= " AND (".$filterGeo.")";
			}
			if(isset($gsf_fields['gsf_custom_fields']) && !empty($gsf_fields['gsf_custom_fields'])){
				$filterStr .= " And ".$gsf_fields["gsf_custom_fields"];
			}
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);
			
			$latlon_info = $this->pm->get_account_latlon($agent_id);
			$account_info = $this->get_account_info($agent_id);

			//latlon from DB
			if(!empty($latlon_info->latitude) && !empty($latlon_info->longitude)){

				$latlon = array(

					"lat" => $latlon_info->latitude,
					"lon" => $latlon_info->longitude
				);

			//latlon from google geocoding api	
			} elseif(!empty($account_info->Addresses[0]->Address) && $account_info->Addresses[0]->Address != "********"){

				$address = $account_info->Addresses[0]->Address;
				$latlon = $this->getlatlon($address);
				//printA($address);exit;

				if(isset($latlon["lat"]) && !empty($latlon["lat"])){

					$latlon = $this->getlatlon($address);

				}else{
					
					$latlon = $this->get_coordinates($user_id);
				}

			//latlon from active listings
			} else {

				$latlon = $this->get_coordinates($user_id);
			}

			$nearby_listings = $obj->post_endpoint('spark-nearby-listings?_limit=4&_expand=PrimaryPhoto&_distance=20&_units=miles&_filter=('.$filter_string.')&_orderby=-OnMarketDate&_lat='.$latlon["lat"].'&_lon='.$latlon["lon"].'', $access_token);
			$nearby_proxy = json_decode($nearby_listings);

			if(isset($nearby_proxy->data) && !empty($nearby_proxy->data)) {
				foreach($nearby_proxy->data as $nearby) {
			 		$nearbyList[] = $nearby;
			 	}
			}

			if(!empty($nearbyList)) {

				//delete nearby listings
				$this->pm->delete_homepage_listings($user_id,'nearby');

				for ($x = 0; $x <= count($nearbyList); $x++) {

					$count = 0;

				   foreach($nearbyList as $nearby){

				   		if($count < 4) { 

							$data = array(
								'user_id'	=> $user_id,
								'listing_id' => $nearby->StandardFields->ListingKey,
								'listing' 	=> json_encode($nearby),
								'type' 		=> 'nearby',
								'data_type' 		=> '1',
								'date_created' => date('Y-m-d H:i:s')
							);

							$this->pm->insert_homepage_listings($data);

						} $count++;
					}

				} 

			}
		}
	}

	public function add_agent_listings_scale($agent_id=NULL) {
		
		if(isset($_GET["agent_id"])) {
			$agent_id = $_GET["agent_id"];
		}

		if(!empty($agent_id)) {

			$this->load->library('idx_auth', $agent_id);
		    
		    $data['agent_id'] = $agent_id;

			$mls_status = Modules::run('dsl/getMlsStatus', $agent_id);

			if(in_array("Sold", $mls_status)) {
				$filter_sold = "Or MlsStatus Ne 'Sold'";
			}

			if(in_array("Leased", $mls_status)){
				$filter_leased = true;
			}

			if(in_array("Pending", $mls_status)){
				$filter_pending = true;
			}

			if(isset($filter_pending) && isset($filter_leased)){
				$lp_filter = "MlsStatus Ne 'Leased' Or MlsStatus Ne 'Pending'";
			} elseif(isset($filter_leased)){
				$lp_filter = "MlsStatus Ne 'Leased'";
			} elseif(isset($filter_pending)){
				$lp_filter = "MlsStatus Ne 'Pending'";
			}

			$sold_filter = isset($filter_sold)? $filter_sold :"";
			$filter = isset($lp_filter) ? ") And (".$lp_filter.")" : "";
			$filterStr = "(MlsStatus Ne 'Closed' ".$sold_filter." ".$filter."";

			$system_info = $this->get_system_info($agent_id);

			if(!empty($system_info) && $system_info->MlsId == "20170119210918715503000000"){
				$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Active Under Contract')";
			}

			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

		    $active = $this->idx_auth->GetMyListings(
		    	array(
					'_pagination' => 1,
					'_page'       => 1,
					'_limit'      => 1,
					'_filter'	  => $filter_string,
					'_orderby'    => '-OnMarketDate'
			    )
		    );

		    $data['total_active'] 			= (isset($active['pagination']->TotalRows) && !empty($active['pagination']->TotalRows)) ? $active['pagination']->TotalRows : 0;
		    $data['recent_active_date'] 	= (isset($active['results'][0]->StandardFields->OnMarketDate)) ? $active['results'][0]->StandardFields->OnMarketDate : '';
		    $data['recent_active_property'] = (isset($active['results'][0]->StandardFields->UnparsedAddress)) ? $active['results'][0]->StandardFields->UnparsedAddress : '';
		    $data['active_listing_id'] 		= (isset($active['results'][0]->Id)) ? $active['results'][0]->Id : '';

		    $filterStr = "(MlsStatus Eq 'Closed' Or MlsStatus Eq 'Sold' Or MajorChangeType Eq 'Closed')";
		    $filter_url = rawurlencode($filterStr);
		    $filter_string = substr($filter_url, 3, -3);

		    $sold = $this->idx_auth->GetMySoldListing(
		    	array(
					'_pagination' => 1,
					'_page'       => 1,
					'_limit'      => 1,
					'_filter'     => $filter_string,
					'_orderby'    => '-CloseDate'
			    )
		    );
		    
		    $data['total_sold'] 			= (isset($sold['pagination']->TotalRows) && !empty($sold['pagination']->TotalRows)) ? $sold['pagination']->TotalRows : 0;
		    $data['recent_sold_date'] 		= (isset($sold['results'][0]->StandardFields->CloseDate)) ? $sold['results'][0]->StandardFields->CloseDate : '';
		    $data['recent_sold_property'] 	= (isset($sold['results'][0]->StandardFields->UnparsedAddress)) ? $sold['results'][0]->StandardFields->UnparsedAddress : '';
		    $data['sold_listing_id'] 		= (isset($sold['results'][0]->Id)) ? $sold['results'][0]->Id : '';

		    $this->pm->agent_scale($data);

		    return TRUE;

		}

		return FALSE;

	}

	private function getStandardMlsStatus($user_id){

	    $endpoint = "agent/".$user_id."/mls-status";
		$mlsstatus = Modules::run('dsl/get_endpoint', $endpoint);
		$result_info = json_decode($mlsstatus, true);

		if(!isset($result_info{'data'})) {
			return FALSE;
		}

		$flag =  FALSE;

		foreach ($result_info["data"]["0"]["MlsStatus"]["FieldList"] as $key ) {
			if($key["StandardStatus"] == "Closed" || $key["StandardStatus"] == "Sold"){

				return TRUE;

				break;
			}
		}

		return $flag;

	}

	public function get_coordinates($user_id=NULL) {

		$ret = FALSE;

		if($user_id) {

			$flag = FALSE;
			$lat='';
			$lon='';
			$address='';

			$status = array('active', 'office', 'new');

			$i=0;

			//Fetch longitude and latitude to be use in nearby listings
			while($i<count($status)) {

				$prop = $this->get_properties($user_id, $status[$i]);

				if(!empty($prop)) {

					$address = $prop[0]->StandardFields->UnparsedAddress;
					$pos = strpos($prop[0]->StandardFields->Latitude, '*');
					$pos1 = strpos($prop[0]->StandardFields->Longitude, '*');

					if($pos===FALSE && $pos1===FALSE) {

						$lat = $prop[0]->StandardFields->Latitude;
						$lon = $prop[0]->StandardFields->Longitude;
						$flag = TRUE;

						break;

					}
				}

				if($flag)
					break;

				$i++;
			}

			if($flag) {

				$ret = array('lat'=>$lat, 'lon'=>$lon);

			} else {

				if(!empty($address) && $address != '********') {
					$ret = $this->getlatlon($address);
				} else {
					$this->load->model('idx_login/idx_model', 'idx_model');
					$user = $this->idx_model->fetch_user($user_id);
					if(!empty($user)) {
						$address = $user->address;
						$ret = $this->getlatlon($address);
					}
				}

			}
			//$ret = ($flag) ? array('lat'=>$lat, 'lon'=>$lon) : ((!empty($address)) ? $this->getlatlon($address) : FALSE);
		}

		return $ret;

	}

	public function get_account($user_id =NULL, $type=NULL){

		$ret = FALSE;

		if($type && $user_id) {

	   		$account_json = $this->pm->get_account($user_id, $type);
	   		
	   		if(!empty($account_json)) {

	   			$account_data = gzdecode($account_json->account);
	   			$account_data = json_decode($account_data);
	   			//printA($account_data);
	   			$ret = $account_data;
	   		}
	  	} 

	  	return $ret;
	}

	public function listings_checker($user_id=NULL, $type=NULL){

		$listing_checker = $this->pm->check_listing($user_id, $type);

		return !empty($listing_checker) ? 1 : 0;
	}


	public function get_properties($user_id=NULL, $type=NULL) {

		if($type) {

			$listing_checker = $this->pm->check_listing($user_id, $type);

			if(!empty($listing_checker)){

				//get data from listing(TEXT) column
				$listing_json = $this->pm->get_property_new($user_id, $type);

				$listings_data = array();

				if(!empty($listing_json)){
					foreach($listing_json as $listing){
						$listings_data[] = json_decode($listing->listing);
					}

					return $listings_data;	
				}

			} else {

				//get data from listings(BLOB) column
				$listings_json = $this->pm->get_property_old($user_id, $type);

				if(!empty($listings_json)) {
					$listings_data = gzdecode($listings_json->listings);
					$listings_data = json_decode($listings_data);

					return $listings_data;
				}
			}	
			
		}

	}


	public function get_properties_old($user_id=NULL, $type=NULL) {

		if($type) {

			$listings_json = $this->pm->get_property_old($user_id, $type);

			if(!empty($listings_json)) {
				$listings_data = gzdecode($listings_json->listings);
				$listings_data = json_decode($listings_data);
				return $listings_data;
			}

		}

	}

	public function get_office($user_id=NULL, $type=NULL) {

		if($type) {

			$listing_checker = $this->pm->check_listing($user_id, $type);

			if(!empty($listing_checker)){

				//get data from listing(TEXT) column
				$listings_json = $this->pm->get_property_new($user_id, $type);

				$listings_data = array();

				if(!empty($listings_json)){
					foreach($listings_json as $listing){
						$listings_data[] = json_decode($listing->listing, true);
					}

					return $listings_data;	
				}

			} else {

				//get data from listings(BLOB) column
				$listings_json = $this->pm->get_property_old($user_id, $type);

				if(!empty($listings_json)) {
					$listings_data = gzdecode($listings_json->listings);
					$listings_data = json_decode($listings_data, true);

					return $listings_data;
				}
			}	
			
		}

	}

	public function get_office_old($user_id=NULL, $type=NULL) {

		if($type) {

			$listings_json = $this->pm->get_property_old($user_id, $type);

			if(!empty($listings_json)) {
				$listings_data = gzdecode($listings_json->listings);
				$listings_data = json_decode($listings_data, true);
				return $listings_data;
			}

		}
	}


	public function account_info($user_id=NULL) {

		if($user_id) {

			$endpoint = "agent/".$user_id."/account-info";
			$account_info = Modules::run('dsl/get_endpoint', $endpoint);
			$account_info_data = json_decode($account_info);

			if(!empty($account_info_data)) {

				$json = json_encode($account_info_data);
				$json_properties = gzencode($json, 9);

				$data = array(
					'user_id' => $user_id,
					'account' => $json_properties,
					'type' => 'account_info',
					'date_created' => date('Y-m-d H:i:s')
				);

				$this->pm->insert_account($data);
			}
		}
	}

	public function system_info($user_id=NULL) {

		if($user_id) {

			$endpoint = "agent/".$user_id."/system-info";
			$system_info = Modules::run('dsl/get_endpoint', $endpoint);
			$system_info_data = json_decode($system_info);

			if(!empty($system_info_data)) {

				$json = json_encode($system_info_data);
				$json_properties = gzencode($json, 9);

				$data = array(
					'user_id' => $user_id,
					'account' => $json_properties,
					'type' => 'system_info',
					'date_created' => date('Y-m-d H:i:s')
				);

				$this->pm->insert_account($data);
			}
		}
	}

	public function get_my_listings($agent_id=NULL) {

		$this->load->library("idx_auth", $agent_id);

		$data = array();
		$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";

		//DEV-1765: Using this filter for specific MLS bec. we can't filter LEASED status on this MLS
		//Pasadena-Foothills Board of Realtors MLS
		$system_info = $this->get_system_info($agent_id);
		if(!empty($system_info) && $system_info->MlsId == "20170119210918715503000000"){
			$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Active Under Contract')";
		}
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$param = array(
			'_pagination' 	=> 1,
			'_page' 		=> 1,
			'_limit' 		=> 4,
			'_filter'		=> $filter_string,
			'_orderby'		=> '-OnMarketDate'
		);

		$listings = $this->idx_auth->GetMyListings($param);

		// if(isset($_GET["debugger"])){
		// 	echo '1: ';
		// 	printA($listings); exit;
		// }

		if(isset($listings['results']) && !empty($listings['results'])) {

			$my_listings_key = array(
				'BathsTotal',
				'BedsTotal',
				'BathsFull',
				'BathsHalf',
				'BuildingAreaTotal',
				'City',
				'ClosePrice',
				'Country',
				'CountyOrParish',
				'CurrentPrice',
				'Latitude',
				'ListAgentId',
				'ListAgentName',
				'ListPrice',
				'ListingId',
				'ListingKey',
				'ListingNumber',
				'Longitude',
				'LotDimensionsSource',
				'LotFeatures',
				'LotSizeAcres',
				'LotSizeArea',
				'LotSizeDimensions',
				'LotSizeSquareFeet',
				'LotSizeUnits',
				'MlsStatus',
				'PhotosCount',
				'PhotosTotal',
				'PostalCity',
				'PostalCode',
				'PropertyClass',
				'PropertySubType',
				'PropertyType',
				'PublicRemarks',
				'StateOrProvince',
				'SubdivisionName',
				'UnparsedAddress',
				'YearBuilt',
				'UnparsedFirstLineAddress',
				'ListAgentFirstName',
				'ListAgentMiddleName',
				'ListAgentLastName',
				'ListAgentPreferredPhone',
				'ListAgentOfficePhone',
				'ListAgentCellPhone',
				'ListAgentFax',
				'ListAgentEmail',
				'ListAgentURL',
				'ListAgentStateLicense',
				'ListOfficeName',
				'ListOfficePhone',
				'ListOfficeFax',
				'ListOfficeEmail',
				'Photos',
			);

			for($i=0; $i<count($listings['results']); $i++) {

				$arr = array();
				$key_arr = array();
				$val_arr = array();

				foreach($listings['results'][$i]->StandardFields as $key=>$val) {
					if(in_array($key, $my_listings_key)) {
						$key_arr[]=$key;
						$val_arr[]=$val;
					}
				}

				$c = array_combine($key_arr,$val_arr);
				$arr["Id"] = $listings['results'][$i]->Id;
				$arr["StandardFields"] = (object)$c;
				$data[$i] = (object) $arr;

				if(isset($_GET["debugger"])){
					echo '2: ';
					printA($data[$i]); exit;
				}

			}
		}

		//printA($data);
		return $data;
	}

	public function get_office_listings($agent_id=NULL) {

		$this->load->library("idx_auth", $agent_id);

		$data = array();

		$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$param = array(
			'_pagination'	=> 1,
			'_page' 		=> 1,
			'_limit' 		=> 4,
			'_filter'		=> $filter_string,
			'_orderby'		=> '-OnMarketDate'
		);

		$listings = $this->idx_auth->GetOfficeListings($param);

		if(isset($listings['results']) && !empty($listings['results'])) {

			$office_listings_key = array(
				'BathsTotal',
				'BedsTotal',
				'BathsFull',
				'BathsHalf',
				'BuildingAreaTotal',
				'City',
				'CountyOrParish',
				'CurrentPrice',
				'Latitude',
				'ListAgentId',
				'ListingKey',
				'Longitude',
				'LotSizeAcres',
				'LotSizeArea',
				'LotSizeSquareFeet',
				'LotSizeUnits',
				'LotSizeDimensions',
				'MlsStatus',
				'PostalCity',
				'PostalCode',
				'PropertyClass',
				'PublicRemarks',
				'StateOrProvince',
				'UnparsedFirstLineAddress',
				'MlsId',
				'ModificationTimestamp',
				'UnparsedAddress',
				'ListingUpdateTimestamp',
			);
				
			for($i=0; $i<count($listings['results']); $i++) {

				$arr = array();
				$key_arr = array();
				$val_arr = array();
				$photos = array();

				foreach($listings['results'][$i]['StandardFields'] as $key=>$val) {

					if(in_array($key, $office_listings_key)) {
						$key_arr[]=$key;
						$val_arr[]=$val;
					} elseif($key=="Photos") {
						$photos["Uri300"] = (isset($val[0]['Uri300'])) ? $val[0]['Uri300'] : AGENT_DASHBOARD_URL . "assets/images/image-not-available.jpg";
					} else {
						continue;
					}

				}

				$c = array_combine($key_arr,$val_arr);
				$arr["StandardFields"] = (object)$c;
				$arr["Photos"] = (object)$photos;

				$data[$i] = (object) $arr;

			}
		}

		//printA($data);
		return $data;
	}

	public function get_new_listings($agent_id=NULL) {

		$this->load->library("idx_auth", $agent_id);

		$data = array();
		$gsf_fields = $this->pm->get_gsf_fields($agent_id);
		$filterGeo = '';
		//subtract 1 day from today's date
		$lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
		$today =  gmdate("Y-m-d\TH:i:s\Z");
		//printA($lastday);exit;

		$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented') And (OnMarketDate Bt ".$lastday.",".$today.")";

		if(isset($gsf_fields['gsf_standard_fields']) && !empty($gsf_fields['gsf_standard_fields'])){
			$gsf_standard_fields = json_decode($gsf_fields['gsf_standard_fields']);
			if(isset($gsf_standard_fields->PropertyClass)){
				$filterStr .= " And ( ";
				for ($i=0; $i < count($gsf_standard_fields->PropertyClass); $i++) { 
					if($i == 0){
						$filterStr .= "PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";
					}else{
						$filterStr .= " Or PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";						
					}
				}
				$filterStr .= " )";				
			}

			if(isset($gsf_standard_fields->City)){
				for ($i=0; $i < count($gsf_standard_fields->City); $i++) { 
					$filterGeo .= ($filterGeo != '') ? " Or City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."'" : " City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."' ";
				}						
			}

			if(isset($gsf_standard_fields->CountyOrParish)){				
				for ($i=0; $i < count($gsf_standard_fields->CountyOrParish); $i++) { 
					$filterGeo .= ($filterGeo != '') ? " Or CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."'" : " CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."' ";					
				}				
			}

			if(isset($gsf_standard_fields->PostalCode)){
				for ($i=0; $i < count($gsf_standard_fields->PostalCode); $i++) { 
					$filterGeo .= ($filterGeo != '') ? " Or PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."'" : " PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."' ";
				}							
			}
		}
		if($filterGeo != ''){
			$filterStr .= " AND (".$filterGeo.")";
		}
		if(isset($gsf_fields['gsf_custom_fields']) && !empty($gsf_fields['gsf_custom_fields'])){
			$filterStr .= " And ".$gsf_fields["gsf_custom_fields"];
		}
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$param = array(
			'_pagination' 	=> 1,
			'_page' 		=> 1,
			'_limit' 		=> 4,
			'_filter'		=> $filter_string,
			'_orderby'		=> '-OnMarketDate'
		);

		$listings = $this->idx_auth->GetListings($param);

		if(isset($listings['results']) && !empty($listings['results'])) {

			$new_listings_key = array(
				'BathsTotal',
				'BedsTotal',
				'BathsFull',
				'BathsHalf',
				'BuildingAreaTotal',
				'City',
				'CountyOrParish',
				'CurrentPrice',
				'Latitude',
				'ListAgentId',
				'ListingKey',
				'Longitude',
				'LotSizeAcres',
				'LotSizeArea',
				'LotSizeSquareFeet',
				'LotSizeUnits',
				'MajorChangeTimestamp',
				'LotSizeDimensions',
				'MlsStatus',
				'PostalCity',
				'PostalCode',
				'PropertyClass',
				'PublicRemarks',
				'StateOrProvince',
				'UnparsedFirstLineAddress',
				'MlsId',
				'ModificationTimestamp',
				'UnparsedAddress',
				'ListingUpdateTimestamp',
				'OnMarketDate',
				'OnMarketContractDate',
			);

			for($i=0; $i<count($listings['results']); $i++) {

				$arr = array();
				$key_arr = array();
				$val_arr = array();
				$photos = array();

				foreach($listings['results'][$i]['StandardFields'] as $key=>$val) {

					if(in_array($key, $new_listings_key)) {
						$key_arr[]=$key;
						$val_arr[]=$val;
					} elseif($key=="Photos") {
						$photos["Uri300"] = (isset($val[0]['Uri300'])) ? $val[0]['Uri300'] : AGENT_DASHBOARD_URL . "assets/images/image-not-available.jpg";
					} else {
						continue;
					}

				}

				$c = array_combine($key_arr,$val_arr);
				$arr["StandardFields"] = (object)$c;
				$arr["Photos"] = (object)$photos;

				$data[$i] = (object) $arr;

			}
		}

		//printA($data);
		return $data;
	}

	public function get_sold_listings($agent_id='') {

		$this->load->library("idx_auth", $agent_id);

		$data = array();

		$filterStr = "(MlsStatus Eq 'Closed' Or MlsStatus Eq 'Sold' Or MajorChangeType Eq 'Closed')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$param = array(
			'_pagination' 	=> 1,
			'_page' 		=> 1,
			'_limit' 		=> 8,
			'_filter'		=> $filter_string,
			'_orderby'		=> '-CloseDate'
		);

		$listings = $this->idx_auth->GetMySoldListing($param);
		
		if(isset($listings['results']) && !empty($listings['results'])) {

			$sold_listings_key = array(
				'BathsFull',
				'BathsHalf',
				'City',
				'CurrentPrice',
				'ClosePrice',
				'CloseDate',
				'ListAgentId',
				'ListingId',
				'ListingKey',
				'MajorChangeTimestamp',
				'ModificationTimestamp',
				'PostalCity',
				'PostalCode',
				'PublicRemarks',
				'StateOrProvince',
				'UnparsedFirstLineAddress',
				'ListingUpdateTimestamp',
				'BedsTotal',
				'BathsTotal',
				'BathsFull',
				'BathsHalf',
				'BuildingAreaTotal',
				'LotSizeArea',
				'LotSizeUnits',
				'LotSizeSquareFeet',
				'LotSizeAcres',
				'LotSizeDimensions',
			);

			for($i=0; $i<count($listings['results']); $i++) {

				$arr = array();
				$key_arr = array();
				$val_arr = array();
				$photos = array();

				foreach($listings['results'][$i]->StandardFields as $key=>$val) {

					if(in_array($key, $sold_listings_key)) {
						$key_arr[]=$key;
						$val_arr[]=$val;
					} elseif($key=="Photos") {
						$key_arr[]=$key;
						$uri[0] = (object)array('Uri300' => (isset($val[0]->Uri300)) ? $val[0]->Uri300 : AGENT_DASHBOARD_URL . "assets/images/image-not-available.jpg");
						$val_arr[]=$uri;
					} else {
						continue;
					}

				}

				$arr["Id"] = $listings['results'][$i]->Id;
				$c = array_combine($key_arr,$val_arr);
				$arr["StandardFields"] = (object)$c;
				$data[$i] = (object) $arr;

			}
		}

		return $data;
	}

	public function get_nearby_listings($user_id='', $agent_id='') {
		$this->load->library('idx_auth', $agent_id);
		$gsf_fields = $this->pm->get_gsf_fields($agent_id);
		
		$filterGeo = '';
		$data = array();

		$latlon_info = $this->pm->get_account_latlon($agent_id);
		$account_info = $this->get_account_info($agent_id);

		//latlon from DB
		if(!empty($latlon_info->latitude) && !empty($latlon_info->longitude)){

			$latlon = array(

				"lat" => $latlon_info->latitude,
				"lon" => $latlon_info->longitude
			);

		//latlon from google geocoding api	
		} elseif(!empty($account_info->Addresses[0]->Address) && $account_info->Addresses[0]->Address != "********"){

			$address = $account_info->Addresses[0]->Address;
			$latlon = $this->getlatlon($address);
			//printA($latlon);exit;

			if(isset($latlon["lat"]) && !empty($latlon["lat"])){

				$latlon = $this->getlatlon($address);

			}else{
				
				$latlon = $this->get_coordinates($user_id);
			}

		//latlon from active listings
		} else {

			$latlon = $this->get_coordinates($user_id);
		}


		if((isset($latlon['lat']) && !empty($latlon['lat'])) && (isset($latlon['lon']) && !empty($latlon['lon']))) {

			$x = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";

			if(isset($gsf_fields['gsf_standard_fields']) && !empty($gsf_fields['gsf_standard_fields'])){
				$gsf_standard_fields = json_decode($gsf_fields['gsf_standard_fields']);
				if(isset($gsf_standard_fields->PropertyClass)){
					$x .= " And ( ";
					for ($i=0; $i < count($gsf_standard_fields->PropertyClass); $i++) { 
						if($i == 0){
							$x .= "PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";
						}else{
							$x .= " Or PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";						
						}
					}
					$x .= " )";				
				}

				if(isset($gsf_standard_fields->City)){
					for ($i=0; $i < count($gsf_standard_fields->City); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."'" : " City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."' ";
					}						
				}

				if(isset($gsf_standard_fields->CountyOrParish)){				
					for ($i=0; $i < count($gsf_standard_fields->CountyOrParish); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."'" : " CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."' ";					
					}				
				}

				if(isset($gsf_standard_fields->PostalCode)){
					for ($i=0; $i < count($gsf_standard_fields->PostalCode); $i++) { 
						$filterGeo .= ($filterGeo != '') ? " Or PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."'" : " PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."' ";
					}
				}
			}

			if($filterGeo != ''){
				$x .= " AND (".$filterGeo.")";
			}
			if(isset($gsf_fields['gsf_custom_fields']) && !empty($gsf_fields['gsf_custom_fields'])){
				$x .= " And ".$gsf_fields["gsf_custom_fields"];
			}

			$y = rawurlencode($x);
			$z = substr($y, 3, -3);

			$param = array(
				'_pagination' 	=> 1,
				'_page' 		=> 1,
				'_limit' 		=> 4,
				'_filter'		=> $z,
				'_lat'          => $latlon['lat'],
				'_lon'          => $latlon['lon'],
				'_distance'          => 20,
				'_units'          => 'miles',
				'_orderby'		=> '-OnMarketDate'
			);
			
			$listings = $this->idx_auth->GetNearbyListings($param);
			
			if(isset($listings['results']) && !empty($listings['results'])) {

				$nearby_listings_key = array(
					'BathsTotal',
					'BedsTotal',
					'BathsFull',
					'BathsHalf',
					'BuildingAreaTotal',
					'City',
					'CurrentPrice',
					'ListAgentId',
					'ListingKey',
					'LotSizeAcres',
					'LotSizeArea',
					'LotSizeSquareFeet',
					'LotSizeUnits',
					'LotSizeDimensions',
					'MlsStatus',
					'PostalCity',
					'PostalCode',
					'PropertyClass',
					'StateOrProvince',
					'UnparsedFirstLineAddress',
					'UnparsedAddress',
				);

				for($i=0; $i<count($listings['results']); $i++) {

					$arr = array();
					$key_arr = array();
					$val_arr = array();
					$photos = array();

					foreach($listings['results'][$i]['StandardFields'] as $key=>$val) {

						if(in_array($key, $nearby_listings_key)) {
							$key_arr[]=$key;
							$val_arr[]=$val;
						} elseif($key=="Photos") {
							$photos["Uri300"] = (isset($val[0]['Uri300'])) ? $val[0]['Uri300'] : AGENT_DASHBOARD_URL . "assets/images/image-not-available.jpg";
						} else {
							continue;
						}
					
					}

					$c = array_combine($key_arr,$val_arr);
					$arr["StandardFields"] = (object)$c;
					$arr["Photos"] = (object)$photos;

					$data[$i] = (object) $arr;

				}
			}

		}

		return $data;
	}

	public function get_account_info($agent_id=NULL) {

		$this->load->library("idx_auth", $agent_id);
		$account = $this->idx_auth->GetMyAccount();

		return (!empty($account)) ? $account[0] : "";
	}

	public function get_system_info($agent_id=NULL) {

		$this->load->library("idx_auth", $agent_id);
		$system = $this->idx_auth->GetSystemInfo();

		return (!empty($system)) ? $system[0] : "";

	}

	public function sync_home_properties($user_id=NULL, $agent_id=NULL) {

		$data['title'] = "Sync Properties";
		$data['user_id'] = $user_id;
		$data['agent_id'] = $agent_id;

		$this->load->view('sync_view', $data);
	}

	public function process_sync() {

		$user_id = $this->input->post('user_id');
		$agent_id = $this->input->post('agent_id');

		$active_listings = $this->get_my_listings($agent_id);
		$office_listings = $this->get_office_listings($agent_id);
		$new_listings = $this->get_new_listings($agent_id);
		$sold_listings = $this->get_sold_listings($agent_id);
		$nearby_listings = $this->get_nearby_listings($agent_id);

		$account_info = $this->get_account_info($agent_id);
		$system_info = $this->get_system_info($agent_id);

		$listings_arr = array('user_id' => $user_id, 'table' => 'homepage_properties');
		$meta_arr = array('user_id' => $user_id, 'table' => 'account_meta');

		if(!empty($active_listings)) {

			$listings_arr['items'] = $active_listings;
			$listings_arr['type'] = 'active';

			$this->save_listings($listings_arr);
		}

		if(!empty($office_listings)) {

			$listings_arr['items'] = $office_listings;
			$listings_arr['type'] = 'office';

			$this->save_listings($listings_arr);
		}

		if(!empty($new_listings)) {

			$listings_arr['items'] = $new_listings;
			$listings_arr['type'] = 'new';

			$this->save_listings($listings_arr);

		}

		if(!empty($sold_listings)) {

			$listings_arr['items'] = $sold_listings;
			$listings_arr['type'] = 'sold';

			$this->save_listings($listings_arr);

		}

		if(!empty($nearby_listings)) {

			$listings_arr['items'] = $nearby_listings;
			$listings_arr['type'] = 'nearby';

			$this->save_listings($listings_arr);
		}

		if(!empty($account_info)) {

			$meta_arr['items'] = $account_info;
			$meta_arr['type'] = 'account_info';

			$this->save_listings($meta_arr);

		}

		if(!empty($system_info)) {

			$meta_arr['items'] = $system_info;
			$meta_arr['type'] = 'system_info';

			$this->save_listings($meta_arr);

		}

		$response = array('success'=>TRUE);
		exit(json_encode($response));
	}

	public function save_listings($data=array()) {

		if(isset($_GET["debugger"])){
			echo '2: ';
			printA($data); exit;
		}
				
		if($data) {

			$listings_data = $data['items'];
			$type = $data['type'];

			if($data['table'] == "homepage_properties") {

				for ($x = 0; $x <= count($listings_data); $x++) {

				   foreach($listings_data as $listing){

					   	if($type == "active" || $type == "sold"){
					   		$listing_id = $listing->Id;
					   	} else {
					   		$listing_id = $listing->StandardFields->ListingKey;
					   	}

						$data = array(
							'user_id'	=> $data['user_id'],
							'listing_id' => $listing_id,
							'listing' 	=> json_encode($listing),
							'type' 		=> $data['type'],
							'date_created' => date('Y-m-d H:i:s')
						);

						if($type == "sold") {
							$data['date_sold'] = $listing->StandardFields->CloseDate;
						}

						$this->pm->insert_homepage_listings($data);
					}

				} 

			} else {

				$json = json_encode($data['items']);
				$json_properties = gzencode($json, 9);

				$datas = array(
					'user_id'	=> $data['user_id'],
					'type' 		=> $data['type'],
					'date_created' => date('Y-m-d H:i:s')
				);

				$datas['account'] = $json_properties;
				$this->pm->insert_account($datas);
			}		

		}

	}

	public function compress_old($data=array()) {

		if($data) {

			$json = json_encode($data['items']);
			$json_properties = gzencode($json, 9);

			$datas = array(
				'user_id'	=> $data['user_id'],
				'type' 		=> $data['type'],
				'date_created' => date('Y-m-d H:i:s')
			);

			if($data['table'] == "homepage_properties") {
				$datas['listings'] = $json_properties;
				$this->pm->insert_homepage_listings($datas);
			} else {
				$datas['account'] = $json_properties;
				$this->pm->insert_account($datas);
			}

		}

	}

	public function getlatlon_old($address = NULL) {
		return FALSE;
		$ret = FALSE;

		if(!empty($address)) {

			$location = array('address'=> $address);
			$options = http_build_query($location);
			$api_call = 'https://maps.googleapis.com/maps/api/geocode/json?'.$options.'&key='.getenv('GOOGLE_MAP_APIKEY');
			$api_data = @file_get_contents($api_call);
			$data = json_decode($api_data, true);

		 	if($data['status'] === 'OK'){

			  	$latlon = array(
			  		'lat' => $data['results'][0]['geometry']['location']['lat'],
			  		'lon' => $data['results'][0]['geometry']['location']['lng'],
			  	);

		  		$ret = $latlon;
		  	}

		}

	  	return $ret;
	}

	public function getlatlon($address = NULL) {

		$ret = FALSE;
		//$address = '701 Grand Central Ave., Lavallette, NJ 08735';

		if(!empty($address)) {

			$agent_address = urlencode($address);
			$api_call = 'http://www.datasciencetoolkit.org/street2coordinates/'.$agent_address;
			$api_data = @file_get_contents($api_call);
			$decoded_data = json_decode($api_data, true);
			$data = $decoded_data[$address];

			if(!empty($data)){

				$latlon = array(
					'lat' => $data['latitude'],
					'lon' => $data['longitude'],
				);

				$ret = $latlon;
			}

		}

		return $ret;
	}

	//added in cron job to update sold listings to the database every 10 mins.
	public function sync_sold_listings_data(){

		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];
		
		if( !empty($user_id) && !empty($agent_id) ){
			
			$sold_listings = $this->get_sold_listings($agent_id);
			$listings_arr = array('user_id' => $user_id, 'table' => 'homepage_properties');

			if(!empty($sold_listings)) {

				//delete sold listings
				$this->pm->delete_homepage_listings($user_id,'sold');

				$listings_arr['items'] = $sold_listings;
				$listings_arr['type'] = 'sold';

				$this->save_listings($listings_arr);

			}

			$this->add_agent_listings_scale($agent_id);

		}

		return TRUE;
	}

	//added in cron job to update active listings to the database every 10 mins.
	public function sync_active_listings_data(){


		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];

		if( !empty($user_id) && !empty($agent_id) ){
			
			$active_listings = $this->get_my_listings($agent_id);
			$listings_arr = array('user_id' => $user_id, 'table' => 'homepage_properties');

			if(!empty($active_listings)) {

				//delete active listings
				$this->pm->delete_homepage_listings($user_id,'active');

				$listings_arr['items'] = $active_listings;
				$listings_arr['type'] = 'active';

				$this->save_listings($listings_arr);

			}

		}

		return TRUE;
	}

	//added in cron job to update new listings to the database every 10 mins.
	public function sync_new_listings_data(){

		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];

		if( !empty($user_id) && !empty($agent_id) ){
			
			$new_listings = $this->get_new_listings($agent_id);
			$listings_arr = array('user_id' => $user_id, 'table' => 'homepage_properties');

			if(!empty($new_listings)) {

				//delete active listings
				$this->pm->delete_homepage_listings($user_id,'new');

				$listings_arr['items'] = $new_listings;
				$listings_arr['type'] = 'new';

				$this->save_listings($listings_arr);

			}

		}

		return TRUE;
	}

	public function sync_office_listings_data(){

		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];

		if( !empty($user_id) && !empty($agent_id) ){
			
			$office_listings = $this->get_office_listings($agent_id);
			$listings_arr = array('user_id' => $user_id, 'table' => 'homepage_properties');

			if(!empty($office_listings)) {

				//delete active listings
				$this->pm->delete_homepage_listings($user_id,'office');

				$listings_arr['items'] = $office_listings;
				$listings_arr['type'] = 'office';

				$this->save_listings($listings_arr);

			}

		}

		return TRUE;
	}

	public function sync_nearby_listings_data(){

		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];

		if( !empty($user_id) && !empty($agent_id) ){
			
			$nearby_listings = $this->get_nearby_listings($user_id,$agent_id);
			$listings_arr = array('user_id' => $user_id, 'table' => 'homepage_properties');

			if(!empty($nearby_listings)) {

				//delete active listings
				$this->pm->delete_homepage_listings($user_id,'nearby');

				$listings_arr['items'] = $nearby_listings;
				$listings_arr['type'] = 'nearby';

				$this->save_listings($listings_arr);

			}

		}

		return TRUE;
	}

	public function save_nearby_latlon($agent_id=NULL){

		$account_info = $this->get_account_info($agent_id);
		//printA($account_info);exit;

		if(!empty($account_info->Addresses[0]->Address) && $account_info->Addresses[0]->Address != "********"){

			$address = $account_info->Addresses[0]->Address;
			$latlon = $this->getlatlon($address);

			if(!empty($latlon)){
				$this->pm->update_nearby_latlon($agent_id,$latlon);
			}
		}
		return TRUE;
	}

	public function getPreviousOfficeSoldListings($agent_id = NULL,$page = NULL,$pagination = FALSE){

		$token = $this->dsl->getUserAccessToken($agent_id);
		$access_token = array('access_token' => $token->access_token ); 

		$selectFields = 'UnparsedFirstLineAddress,ListingKey,City,PostalCode,StateOrProvince,PostalCity,ClosePrice,CloseDate,ListAgentName,MlsStatus';

		$filterStr = "(HistoricalListAgentId Eq '".$agent_id."' And (MlsStatus Eq 'Closed' Or MlsStatus Eq 'Sold'))";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$url = "https://sparkapi.com/v1/listings?_pagination=1&_limit=25&_page=".$page."&_expand=PrimaryPhoto&_select=".$selectFields."&_page=".$page."&_filter=(".$filter_string.")&_orderby=-CloseDate";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		  'Authorization: OAuth '. $access_token["access_token"],
		  'Accept-Encoding: gzip, deflate'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$unzipped_data = gunzip($result);
		$result_info = json_decode($unzipped_data, true);
		//printA($result_info);exit;
		return $result_info["D"]["Success"] ? (($pagination) ? $result_info["D"]: $result_info["D"]["Results"] )  : FALSE;

	}

	public function getCurrentOfficeSoldListings($agent_id = NULL, $page = NULL,$pagination = FALSE){

	    $token = $this->dsl->getUserAccessToken($agent_id);
	    $access_token = array('access_token' => $token->access_token);

	    $selectFields = 'UnparsedFirstLineAddress,ListingKey,City,PostalCode,StateOrProvince,PostalCity,ClosePrice,CloseDate,ListAgentName,MlsStatus';
	    $filterStr = "(MlsStatus Eq 'Closed' Or MlsStatus Eq 'Sold')";
	    $filter_url = rawurlencode($filterStr);
	    $filter_string = substr($filter_url, 3, -3);

	    $url = "https://sparkapi.com/v1/my/listings?_pagination=1&_limit=25&_page=".$page."&_expand=PrimaryPhoto&_select=".$selectFields."&_filter=(".$filter_string.")&_orderby=-CloseDate";

	    $headers = array(
	      'Content-Type: application/json',
	      'User-Agent: Spark API PHP Client/2.0',
	      'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
	      'Authorization: OAuth '. $access_token["access_token"],
	      'Accept-Encoding: gzip, deflate'
	    );

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_URL, $url);

	    $result = curl_exec($ch);
	    $unzipped_data = gunzip($result);
	    $result_info = json_decode($unzipped_data, true);
	    //printA($result_info);exit;
	    return $result_info["D"]["Success"] ? (($pagination) ? $result_info["D"] : $result_info["D"]["Results"] )  : FALSE;

	}

	public function syncAgentSoldListings($user_id = NULL, $agent_id = NULL,$updater = TRUE){

		if($updater){
			$user_id = isset($_GET["user_id"])? $_GET["user_id"] : "";
			$agent_id = isset($_GET["agent_id"])? $_GET["agent_id"] : "";
		}

		if( !empty($user_id) && !empty($agent_id) ){

			$currentSoldListingsData = array();
			$previousSoldListingsData = array();
			$currentSoldListings = $this->getCurrentOfficeSoldListings($agent_id, 1,TRUE);
			$previousSoldListings = $this->getPreviousOfficeSoldListings($agent_id, 1,TRUE);

			//save new sold listings data
			if(isset($currentSoldListings["SparkQLErrors"][0]["Message"]) && isset($currentSoldListings["SparkQLErrors"][1]["Message"]) && $currentSoldListings["SparkQLErrors"][0]["Message"] == "The specified field is not searchable." && $currentSoldListings["SparkQLErrors"][1]["Message"] == "The specified field is not searchable."){

				//Sold and Closed is not searchable - DO NOTHING JON SNOW!!!
			}else{

				//save new sold listings data
				if(!empty($currentSoldListings)){
					$totalCurrentSL = $currentSoldListings["Pagination"]["TotalPages"];

					$newSoldListingsExist = $this->pm->check_sold_listings_data( $user_id, "new" );

					if(!$newSoldListingsExist){

						$this->db->trans_start();

						for($i=1; $i <= $totalCurrentSL; $i++) {

							$currentSoldListingsData = $this->getCurrentOfficeSoldListings($agent_id, $i);

							if(!empty($currentSoldListingsData)){

								foreach ($currentSoldListingsData as $sold) {

									$this->pm->save_sold_listings_data( $user_id, $sold, "new" );
								}
							}
						}

						$this->db->trans_complete();
					}else{

						$soldListingsLive =  isset($currentSoldListings["Pagination"]["TotalRows"]) ? $currentSoldListings["Pagination"]["TotalRows"] : "";
						$soldListingsLocal =  $this->pm->sold_listings_data_db_count($user_id, "new");
							
						if($soldListingsLive != $soldListingsLocal){

						$this->db->trans_start();

							$this->pm->delete_sold_listings_data( $user_id, "new");

							for($i=1; $i <= $totalCurrentSL; $i++) {

								$currentSoldListingsData = $this->getCurrentOfficeSoldListings($agent_id, $i);

								if(!empty($currentSoldListingsData)){

									foreach ($currentSoldListingsData as $sold) {

										$this->pm->save_sold_listings_data( $user_id, $sold, "new" );
									}
								}
							}

						$this->db->trans_complete();

						}else{

							$this->db->trans_start();
								for($i=1; $i <= $totalCurrentSL; $i++) {

									$currentSoldListingsData = $this->getCurrentOfficeSoldListings($agent_id, $i);

									if(!empty($currentSoldListingsData)){

										foreach ($currentSoldListingsData as $sold) {

											$this->pm->update_sold_listings_data( $user_id, $sold, "new" );
										}
									}
								}
							$this->db->trans_complete();
						}
					}
				}

				//save old sold listings data
				if(!empty($previousSoldListings)){
					$totalPreviousSL = $previousSoldListings["Pagination"]["TotalPages"];

					$oldSoldListingsExist = $this->pm->check_sold_listings_data($user_id, "old" );

					if(!$oldSoldListingsExist){ 

						$this->db->trans_start();

						for($i=1; $i <= $totalPreviousSL; $i++) {

							$previousSoldListingsData = $this->getPreviousOfficeSoldListings($agent_id, $i);

							if(!empty($previousSoldListingsData)){

								foreach ($previousSoldListingsData as $sold) {

									$this->pm->save_sold_listings_data( $user_id, $sold, "old" );
								}
							}
						}

						$this->db->trans_complete();
					}

					//insert date_sold on db
					$dateSoldExist = $this->pm->check_date_sold($user_id, "old");

					if(!$dateSoldExist){

						$this->db->trans_start();

						for($i=1; $i <= $totalPreviousSL; $i++) {

							$previousSoldListingsData = $this->getPreviousOfficeSoldListings($agent_id, $i);

							if(!empty($previousSoldListingsData)){

								foreach ($previousSoldListingsData as $sold) {

									$this->pm->update_sold_listings_data( $user_id, $sold, "old" );
								}
							}
						}

						$this->db->trans_complete();
					}
				}
			}

		}

		return TRUE;
	}

	public function syncAgentSoldListingsDateSold($user_id = NULL, $agent_id = NULL,$updater = TRUE){

		if($updater){
			$user_id = isset($_GET["user_id"])? $_GET["user_id"] : "";
			$agent_id = isset($_GET["agent_id"])? $_GET["agent_id"] : "";
			$type = isset($_GET["type"])? $_GET["type"] : "";
		}

		if( !empty($user_id) && !empty($agent_id) ){

			$currentSoldListingsData = array();
			$previousSoldListingsData = array();
			$currentSoldListings = $this->getCurrentOfficeSoldListings($agent_id, 1,TRUE);
			$previousSoldListings = $this->getPreviousOfficeSoldListings($agent_id, 1,TRUE);

			//save new sold listings data
			if(isset($currentSoldListings["SparkQLErrors"][0]["Message"]) && isset($currentSoldListings["SparkQLErrors"][1]["Message"]) && $currentSoldListings["SparkQLErrors"][0]["Message"] == "The specified field is not searchable." && $currentSoldListings["SparkQLErrors"][1]["Message"] == "The specified field is not searchable."){

				//Sold and Closed is not searchable - DO NOTHING JON SNOW!!!
			}else{

				

				if(!empty($currentSoldListings)){

					$totalCurrentSL = $currentSoldListings["Pagination"]["TotalPages"];

					//insert date_sold on type: NEW
					$dateSoldExist = $this->pm->check_date_sold($user_id, "new");

					if(!$dateSoldExist){

						$this->db->trans_start();

						for($i=1; $i <= $totalCurrentSL; $i++) {

							$currentSoldListingsData = $this->getCurrentOfficeSoldListings($agent_id, $i);

							if(!empty($currentSoldListingsData)){

								foreach ($currentSoldListingsData as $sold) {

									$this->pm->insert_date_sold( $user_id, $sold, "new" );
								}
							}
						}

						$this->db->trans_complete();
					}
				}

				if(!empty($previousSoldListings)){
					$totalPreviousSL = $previousSoldListings["Pagination"]["TotalPages"];

					//insert date_sold on type: OLD
					$dateSoldExist = $this->pm->check_date_sold($user_id, "old");

					if(!$dateSoldExist){

						$this->db->trans_start();

						for($i=1; $i <= $totalPreviousSL; $i++) {

							$previousSoldListingsData = $this->getPreviousOfficeSoldListings($agent_id, $i);

							if(!empty($previousSoldListingsData)){

								foreach ($previousSoldListingsData as $sold) {

									$this->pm->insert_date_sold( $user_id, $sold, "old" );
								}
							}
						}

						$this->db->trans_complete();
					}
				}
			}

		}

		return TRUE;
	}

	public function soldListingsDataChecker(){

        $oldSoldData = array();
        $newSoldData = array();

		$soldHp = $this->pm->checkSoldFromHomepageProp();
		$soldSLD = $this->pm->checkSoldFromSoldListingsData();
		foreach ($soldHp as $key => $value){
			$oldSoldData[] = $value['user_id'];
		}

		foreach ($soldSLD as $key => $value){
			$newSoldData[] = $value['user_id'];
		}

		$noSoldListingsData = array_diff($oldSoldData, $newSoldData);
		$haveSoldListingsData = array_intersect($oldSoldData, $newSoldData);

		$noSoldId = array();

		foreach($noSoldListingsData as $user_id){

			$noSoldId[] = $this->pm->getAgentIdPlanIdByUserId($user_id);
		}

		$sold_checker = array(
			'soldListingsFromHP' => $oldSoldData,
			'soldListingsFromSLD' => $newSoldData,
			'haveSoldListingsinSLD' => $haveSoldListingsData,
			'noSoldListingsinSLD' => $noSoldId,
		);

		printA($sold_checker);exit;
	}

	public function syncSoldListingsDataManual(){

		$limit = isset($_GET['limit']) ? $_GET['limit'] : "" ;
		$debug = isset($_GET['debugger']) ? $_GET['debugger'] : "" ;
		$limitEnd = isset($_GET['limitEnd']) ? $_GET['limitEnd'] : "" ;

        $oldSoldData = array();
        $newSoldData = array();

		$soldHp = $this->pm->checkSoldFromHomepageProp();
		$soldSLD = $this->pm->checkSoldFromSoldListingsData();
		foreach ($soldHp as $key => $value){
			$oldSoldData[] = $value['user_id'];
		}

		foreach ($soldSLD as $key => $value){
			$newSoldData[] = $value['user_id'];
		}

		$noSoldListingsData = array_diff($oldSoldData, $newSoldData);

		if($limitEnd){
			$limitSoldListingsData = array_slice($noSoldListingsData, -$limit);
		}else{
			$limitSoldListingsData = array_slice($noSoldListingsData, 0, $limit); 
		}

		$noSoldId = array();

		foreach($limitSoldListingsData as $user_id){

			$noSoldId[] = $this->pm->getAgentIdPlanIdByUserId($user_id);
		}

		if($debug){
			printA($noSoldId);exit;
		}

		foreach($noSoldId as $sold){
			if($sold['is_valid'] === "1"){
				$this->syncAgentSoldListings($sold['user_id'],$sold['agent_id'],FALSE);
			}
		}
	}

	public function syncSoldListingsDateSold(){

		$premium = isset($_GET['planPremium']) ? $_GET['planPremium'] : "" ;
		$limit = isset($_GET['limit']) ? $_GET['limit'] : "" ;
		$debug = isset($_GET['debugger']) ? $_GET['debugger'] : "" ;
		$debugLimit = isset($_GET['debuggerLimit']) ? $_GET['debuggerLimit'] : "" ;
		$limitEnd = isset($_GET['limitEnd']) ? $_GET['limitEnd'] : "" ;


		if($premium){
			$users = $this->pm->getUsersPremium();
		}else{
			$users = $this->pm->getUsersFreemium();
		}
		
		if($debug){
			printA($users);exit;
		}

		if($limitEnd){
			$limitSold = array_slice($users, -$limit);
		}else{
			$limitSold = array_slice($users, 0, $limit);
		}
		
		if($debugLimit){
			printA($limitSold);exit;
		}

		foreach($limitSold as $sold){

			if($sold['is_valid'] === "1" && empty($sold['date_sold'])){
				$this->syncAgentSoldListingsDateSold($sold['user_id'],$sold['agent_id'],FALSE);
			}
			
		}

	}


	public function sync_saved_search_data() {
		$user_id = $_GET["user_id"];
		$agent_id = $_GET["agent_id"];

		if ($result = $this->pm->getUnUpdatedFeaturedSavedSearchListingByUserId($user_id)) {
			foreach($result as $item) {
				$b = $this->pm->getUserAccessTokenByUserId($item->user_id);

				if ($b) {
					$this->addFeaturedSavedSearchMysql($item->saved_search_id, $b->access_token, $item->user_id, $agent_id);
					$this->addFeaturedSavedSearchDsl($item->saved_search_id, $b->access_token, $item->user_id, $agent_id);
				}
				$this->pm->updateIsUpdatedFeaturedSavedSearch($item->id);
			}
		}

		return TRUE;
	}

	public function sync_saved_search() {
		$arr = array();
		if ($result = $this->pm->getUnUpdatedFeaturedSavedSearchListing()) {

			foreach($result as $item) {
				$b = $this->pm->getUserAccessTokenByUserId($item->user_id);
				$a = $this->pm->getAgentIdByUserId($item->user_id);

				if ($b && $a) {
					$this->addFeaturedSavedSearchMysql($item->saved_search_id, $b->access_token, $item->user_id, $a->agent_id);
					$this->addFeaturedSavedSearchDsl($item->saved_search_id, $b->access_token, $item->user_id, $a->agent_id);
				}
				$this->pm->updateIsUpdatedFeaturedSavedSearch($item->id);
			}
			echo 'updated ' . count($result) . ' rows!';
			
		} else {
			echo 'No more to update';
		}

		exit;
	}


	public function addFeaturedSavedSearchMysql($saved_search_id, $access_token, $user_id, $agent_id){

		$dsl = modules::load('dsl/dsl');

		if(!empty($saved_search_id)){
			$this->load->library("idx_auth", $agent_id);
			$featuredSavedSearch = $this->idx_auth->getSavedSearch($saved_search_id);
			
			if(!empty($featuredSavedSearch)){

			  //add featured saved search data to saved_search_options	
			  $this->custom->add_featured_saved_search($featuredSavedSearch[0], $user_id);

			  //add featured saved search listings data to featured_saved_search_listings
				$access_token = array(
		          'access_token' => $access_token
		      );


		      $filterStr = "(".$featuredSavedSearch[0]['Filter'].")";
		      $filter_url = rawurlencode($filterStr);
		      $filter_string = substr($filter_url, 3, -3);

		      $endpoint = "spark-listings?_pagination=1&_page=1&_limit=6&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=-OnMarketDate";
		      $results = $dsl->post_endpoint($endpoint, $access_token);
		      $featured_saved_search_results = json_decode($results);
				
		      if(isset($featured_saved_search_results->data) && !empty($featured_saved_search_results->data)){

						$json = json_encode($featured_saved_search_results->data);
						$json_properties = gzencode($json, 9);

						$data = array(
							'user_id' => $user_id,
							'saved_search_id' => $featuredSavedSearch[0]['Id'],
							'listings' => $json_properties,
							'date_created' => date('Y-m-d H:i:s')
						);
						
						$this->custom->insert_featured_saved_search_listings($data);

							$status = "success";
										$msg = "<div class='alert-flash alert alert-success' role='alert'>Saved Search has been successfully added!</div>";

					} else{

						$data = array(
							'user_id' => $user_id,
							'saved_search_id' => $featuredSavedSearch[0]['Id'],
							'listings' => '',
							'date_created' => date('Y-m-d H:i:s')
						);
					
						$this->custom->insert_featured_saved_search_listings($data);

						$status = "error";
								$msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to add saved search. Saved search is empty!</div>";

				}

			} else {

				$status = "error";
         		$msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to add saved search!</div>";
			}

			return array( 'status' => $status, 'message' => $msg );	
		}	
	}


	public function addFeaturedSavedSearchDsl($search_id, $access_token, $user_id, $agent_id){

		if(!empty($search_id)){

			$dsl = modules::load('dsl/dsl');

			//remove old featured saved search
			$old_saved_search_featured = Modules::run('dsl/getFeaturedAgentSavedSearch', $user_id);
			if(!empty($old_saved_search_featured)){

				$old_featured_id = $old_saved_search_featured[0]['Id'];

				$removeFeatured = array( 'isFeatured' => 0 );

				$dsl->put_endpoint('savedsearch/'.$user_id.'/'.$old_featured_id.'/update', $removeFeatured);
			}

			//remove db featured property
			$this->custom->removeFeaturedProperty();
			
			//add new featured saved search
			$addFeatured = array( 'isFeatured' => 1 );

			$dsl->put_endpoint('savedsearch/'.$user_id.'/'.$search_id.'/update', $addFeatured);

			//saved featured saved search listings in database
			$dsl_featured_saved_search = Modules::run('dsl/getFeaturedAgentSavedSearch', $user_id);
		

			$access_token = array(
		          'access_token' => $access_token
		      );

			//check if dsl featured savedsearch is not empty
			if(!empty($dsl_featured_saved_search)){
				$featured_saved_search = $dsl_featured_saved_search[0] ;
			}else{
				$featured_saved_search = $this->custom->get_featured_saved_search();
			}

			if(!empty($featured_saved_search)){

		      $filterStr = "(".$featured_saved_search['Filter'].")";
		      $filter_url = rawurlencode($filterStr);
		      $filter_string = substr($filter_url, 3, -3);

		      $endpoint = "spark-listings?_pagination=1&_page=1&_limit=6&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=-OnMarketDate";
		      $results = $dsl->post_endpoint($endpoint, $access_token);
		      $featured_saved_search_results = json_decode($results);
		
		      if(isset($featured_saved_search_results->data) && !empty($featured_saved_search_results->data)){

					$json = json_encode($featured_saved_search_results->data);
					$json_properties = gzencode($json, 9);

					$data = array(
						'user_id' => $user_id,
						'saved_search_id' => $featured_saved_search['Id'],
						'listings' => $json_properties,
						'date_created' => date('Y-m-d H:i:s')
					);
					
					$this->custom->insert_featured_saved_search_listings($data);

					  $status = "success";
          			  $msg = "<div class='alert-flash alert alert-success' role='alert'>Featured Saved search has been successfully added!</div>";

				} else{

					$data = array(
						'user_id' => $user_id,
						'saved_search_id' => $featured_saved_search['Id'],
						'listings' => '',
						'date_created' => date('Y-m-d H:i:s')
					);
					
					$this->custom->insert_featured_saved_search_listings($data);

					if(!empty($featured_saved_search)){

						$featured_saved_search_id = $featured_saved_search['Id'];

						$removeFeatured = array( 'isFeatured' => 0 );

						$dsl->put_endpoint('savedsearch/'.$user_id.'/'.$featured_saved_search_id.'/update', $removeFeatured, $access_token, $user_id);
					}

					$status = "error";
         			$msg = "<div class='alert-flash alert alert-danger' role='alert'>Failed to add featured saved search. Saved search is empty!</div>";

				}

				return array( 'status' => $status, 'message' => $msg );	
		    }
		} 
	}
}
