<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Data Service Layer Endpoint Controller
	Fetching Data's from DSL is done here.
*/
class Dsl extends MX_Controller {

	private $authToken;
	private $userId;
	public $login_status = false;

	public function __construct() {
 		parent::__construct();

		$this->load->config('dsl');
		$this->load->model("dsl_model");

		if( !$this->session->userdata('login_status') ) {
			$this->login();
		}
 	}

 	// public function api_logs() {
 	// 	$data = $this->dsl_model->request_stats();
 	// 	$this->load->view('requests_data', $data);
 	// } 

 	public function login() {

 		$status = false;
 		$dsl = array(
 			'username' => getenv('DSL_USERNAME'),
 			'password' => getenv('DSL_PASSWORD')
 		);

 		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, rtrim(getenv('DSL_SERVER_URL'), '/')."/".'login/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$dsl['username'].'&password='.$dsl['password']);
		$content = curl_exec($ch);
		$login = json_decode($content);

		if(!empty($login) && $login->status == 'success') {
			$this->session->set_userdata(
				array(
					'login_status' 	=> true,
					'authToken' 	=> $login->data->authToken,
					'userId' 		=> $login->data->userId
					)
			);
			$status = true;
		}
		return $status;
 	}

 	public function logout() {
 		$array_items = array('login_status', 'authToken', 'userId','user_id','agent_id');
 		$this->session->unset_userdata($array_items);
 		return 1;
 	}

 	/* Returns JSON String of Agent Information */
 	public function get_endpoint($endpoint = NULL) {

 		if($endpoint) {
 			$call_url = rtrim(getenv('DSL_SERVER_URL'), '/')."/".$endpoint; #'agent/'.$this->config->config['user_id'].'/system-info';

	 		$ch = curl_init();
	 		curl_setopt($ch, CURLOPT_URL, $call_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$this->session->userdata('authToken'), 'Accept-Encoding: gzip, deflate', 'X-User-Id: '.$this->session->userdata('userId'), 'Content-Type:application/json'));
			$content = curl_exec($ch);
			if(strpos($endpoint, 'spark-')) {
				$this->dsl_model->log_request();
				$this->dsl_model->log_request_history($endpoint, 'GET', '');
			}
			
			$unzipped_data = gunzip($content);
			return $unzipped_data;
 		}
 		else {
 			echo 0;
 		}
 	}

 	/* POST Type */
 	public function post_endpoint($endpoint = NULL, $body=array()) {

 		if($endpoint) {

 			$call_url = rtrim(getenv('DSL_SERVER_URL'), '/')."/".$endpoint;

 			$headers = array(
	    		'Content-Type: application/json',
	    		'X-Auth-Token: '.$this->session->userdata('authToken'),
	    		'X-User-Id: '.$this->session->userdata('userId'),
	    		'Accept-Encoding: gzip, deflate',
	    	);

 			$body_json = json_encode($body, true);

 			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$result_info  = curl_exec($ch);
			//$result_info = json_decode($result, true);

			if(strpos($endpoint, 'spark-')) {
				$this->dsl_model->log_request();
				$this->dsl_model->log_request_history($endpoint, 'POST', $body);
			}

			if(!$result_info) {
				$result_info = curl_error($ch);
			}

			$unzipped_data = gunzip($result_info);
			return $unzipped_data;
 		}
 	}

 	/* POST Type Updater */
 	public function post_endpoint_updater($endpoint = NULL, $body=array()) {

 		if($endpoint) {

 			$call_url = getenv('DSL_UPDATE_SERVER').$endpoint;

 			$headers = array(
	    		'Content-Type: application/json',
	    		'X-Auth-Token: '.$this->session->userdata('authToken'),
	    		'X-User-Id: '.$this->session->userdata('userId'),
	    		'Accept-Encoding: gzip, deflate',
	    	);

 			$body_json = json_encode($body, true);

 			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$result_info  = curl_exec($ch);
			//$result_info = json_decode($result, true);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}

			$unzipped_data = gunzip($result_info);
			return $unzipped_data;
 		}
 	}

 	public function put_endpoint($endpoint = NULL, $data=array()) {

 		if($endpoint) {

 			$call_url = rtrim(getenv('DSL_SERVER_URL'), '/')."/".$endpoint;

 			$headers = array(
	    		'Content-Type: application/json',
	    		'X-Auth-Token: '.$this->session->userdata('authToken'),
	    		'X-User-Id: '.$this->session->userdata('userId'),
	    		'Accept-Encoding: gzip, deflate',
	    	);

 			$data_json = json_encode($data, true);

 			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);


			$result_info  = curl_exec($ch);
			//$result_info = json_decode($result, true);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}

			$unzipped_data = gunzip($result_info);
			return $unzipped_data;
 		}
 	}

 	public function get_config() {
 		$config = array(
 			'agent_id' => $this->session->userdata("agent_id"),
 			'user_id' =>$this->session->userdata("user_id"),
 		);

 		return $config;
 	}

 	public function post_command_center_endpoint($endpoint = NULL, $body=array()) {
 		if($endpoint) {

 			$call_url = 'http://138.68.13.196:3000/api/command/'.$endpoint;

 			$headers = array(
	    		'Content-Type: application/json',
	    	);

 			$body_json = json_encode($body, true);

 			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$result_info  = curl_exec($ch);
			//$result_info = json_decode($result, true);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}

			return $result_info;
 		}
 		else {
 			return false;
 		}
 	}

 	public function getAgentActivePropertyListings( $limit = FALSE, $count = FALSE, $status = "") {

 		$status = $this->get_mls_statuses();

 		$endpoint = "properties/".$this->session->userdata("user_id")."/status/".$status."?_limit=".$limit;

 		if( !$limit)
 		{
 			$endpoint = "properties/".$this->session->userdata("user_id")."/status/".$status;
 		}

 		$active_listings = $this->get_endpoint($endpoint);
		$activeListingData = json_decode($active_listings);
		$activeList = array();

		if($count){

 			if( !empty($activeListingData)){
 				return $activeListingData->count;
 			} else {
 				return 0;
 			}
 		}

		if( !empty($activeListingData)){
			if(property_exists($activeListingData, 'data') ) {
				foreach ($activeListingData->data as $active){
					$active->standard_fields[0]->date_created = $active->date_created;
			 		$activeList[] = $active->standard_fields[0];
			 	}
			}
		 }
		return $activeList;
	}

	public function getAgentSoldPropertyListings($limit = FALSE) {

		$endpoint = "properties/".$this->session->userdata("user_id")."/sold"."?_limit=".$limit;

		if( !$limit)
		{
			$endpoint = "properties/".$this->session->userdata("user_id")."/sold";
		}

		$sold_listings =$this->get_endpoint($endpoint);
		$soldListingData = json_decode($sold_listings);
		$soldList = array();

		$isCheck = $this->getStandardMlsStatus();

		if( $isCheck )
		{
			if( !empty($soldListingData)){

			 	foreach ($soldListingData->data as $sold){
			 		$soldList[] = $sold->standard_fields[0];
			 	}
			}

			return $soldList;
		}

		return $soldList;
	}

 	public function getOfficeListings() {

		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ''
		);

		$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "office/listings/?_limit=4&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		//$endpoint = "spark-listings?_limit=4&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$office_listings = $this->post_endpoint($endpoint,$access_token);
		$officelistingData = json_decode($office_listings,true);
		$officeList = array();

		if( !empty($officelistingData)){

			foreach ($officelistingData['data'] as $office){
				$officeList[] = $office;
			}
		}
		return $officeList;
	}

 	public function getNewListings() {

		$endpoint = "new-listings/".$this->session->userdata("user_id")."?limit=4";
		$new_listings = $this->get_endpoint($endpoint);
		$newlistingData = json_decode($new_listings);
		$newList = array();

		if( !empty($newlistingData)){

			foreach ($newlistingData->data as $new){
		 		$newList[] = $new;
		 	}
		 }
		return $newList;
	}
	
	public function getNewListingsProxy() {

		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
			'access_token' => $token->access_token,
			'bearer_token' => ''
		);

		$endpoint = "spark-listings?_limit=4&listing_change_type%5B%5D=New+Listing";
		$new_listings = $this->post_endpoint($endpoint,$access_token);
		$newlistingData = json_decode($new_listings);

		$newList = property_exists($newlistingData, 'data') ? $newlistingData->data: false;
		echo "<pre>";
		var_dump($newList);
		echo "</pre>";
		return $newList;
	}

	public function getNearbyListings($datas = array()) {

		$endpoint = "nearby-listings/".$this->session->userdata("user_id")."?limit=4&_lat=".$datas["lat"]."&_lon=".$datas["lon"]."";
		$nearby_listings = $this->get_endpoint($endpoint);

		if($datas["lat"] != "********" && $datas["lon"] != "********"){

			$nearbylistingData = json_decode($nearby_listings);
			$nearbyList = array();

			if( !empty($nearbylistingData)){

			 	foreach ($nearbylistingData->data as $nearby){
			 		$nearbyList[] = $nearby;
			 	}
			 }
		}
		return $nearbyList;
	}

	public function getNearbyListingsProxy( $datas = array() ) {

		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
				'access_token' => $token->access_token
		);

		$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$nearby_listings = $this->post_endpoint('spark-nearby-listings?_limit=4&_expand=PrimaryPhoto&_filter=('.$filter_string.')&_lat='.$datas["lat"].'&_lon='.$datas["lon"].'', $access_token);

		if($datas["lat"] != "********" && $datas["lon"] != "********"){

			$nearby_proxy = json_decode($nearby_listings);
			$nearbyList = array();

			if( !empty($nearby_proxy)){

			 	foreach ($nearby_proxy->data as $nearby){
			 		$nearbyList[] = $nearby;
			 	}
			}
		}
		return $nearbyList;
	}

	// public function getAgentSavedSearch( $param = array() ) {

	// 	$endpoint = "savedsearches/".$this->session->userdata("user_id")."";

	// 	$page = ($param["page"] / $param["limit"]) + 1;

	// 	$page = ( $page >= 2 ) ? $page: 1;
	// 	$param = array(
	// 		'_pagination' => $page,
	// 		'_limit' => $param["limit"],
	// 	);

	// 	$saved_search = $this->post_endpoint($endpoint, $param);

	// 	$savedSearchData = json_decode($saved_search,true);

	// 	return $savedSearchData;
	// }

	public function getAgentSavedSearch( $param = array() ) {

		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token =  $token->access_token;

		$endpoint = "savedsearches/".$this->session->userdata("user_id")."";

		$page = ($param["page"] / $param["limit"]) + 1;

		$page = ( $page >= 2 ) ? $page: 1;
		$param = array(
			'_pagination' => $page,
			'_limit' => $param["limit"],
			'access_token' => $access_token
		);

		$saved_search = $this->post_endpoint($endpoint, $param);

		$savedSearchData = json_decode($saved_search,true);

		return $savedSearchData;
	}

	public function getAllAgentSavedSearchFromSpark() {

		$endpoint = "savedsearches/".$this->session->userdata("user_id")."";

		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token =  $token->access_token;

		$param = array(
			'_pagination' => 1,
			'_limit' => 24,
			'access_token' => $access_token
		);

		$saved_search = $this->post_endpoint($endpoint,$param);

		$savedSearchData = json_decode($saved_search,true);

		return $savedSearchData;
	}

	public function getAllAgentSavedSearchFromDSL() {

		$endpoint = "savedsearches/".$this->session->userdata("user_id")."";

		$saved_search = $this->get_endpoint($endpoint);

		$savedSearchData = json_decode($saved_search,true);

		return $savedSearchData;
	}

	public function getSelectedAgentSavedSearch() {


		$endpoint = "savedsearches/".$this->session->userdata("user_id");


		$saved_search = $this->get_endpoint($endpoint);
		$savedSearchData = json_decode($saved_search,true);
		$selectedSavedSearch = array();

		if( !empty($savedSearchData)){

		 	foreach ($savedSearchData['data']['saved_searches'] as $selected){
		 		if(isset($selected['isSelected'])){
		 			if($selected['isSelected'] == 1){
		 				$selectedSavedSearch[] = $selected;
		 			}
		 		}
		 	}
		 }

		return $selectedSavedSearch;
	}
	
	public function getFeaturedAgentSavedSearch( $user_id = null ) {

		$endpoint = "savedsearches/". ($user_id ? $user_id : $this->session->userdata("user_id"));

		$saved_search = $this->get_endpoint($endpoint);
		$savedSearchData = json_decode($saved_search,true);
		$featuredSavedSearch = array();

		if( !empty($savedSearchData)){

		 	foreach ($savedSearchData['data']['saved_searches'] as $featured){
		 		if(isset($featured['isFeatured'])){
		 			if($featured['isFeatured'] == 1){
		 				$featuredSavedSearch[] = $featured;
		 			}
		 		}
		 	}
		 }

		return $featuredSavedSearch;
	}

	public function getAgentAccountInfo() {

		$endpoint = "agent/".$this->session->userdata("user_id")."/account-info";
		$account_info = $this->get_endpoint($endpoint);
		$content = json_decode($account_info);

		return $content;
	}

	public function getSystemInfo() {

		$endpoint = "agent/".$this->session->userdata("user_id")."/system-info";
		$system_info = $this->get_endpoint($endpoint);
		$content = json_decode($system_info);

		return $content;
	}

	public function get_property_types() {

		$endpoint = "agent/".$this->session->userdata("user_id")."/property-types";
		$property_types = $this->get_endpoint($endpoint);
		$content = json_decode($property_types, true);

		return ($content["status"] == "success" && !empty($content["data"])) ? $content["data"] : FALSE;
	}

	public function get_property_subtypes() {

		$endpoint = "agent/".$this->session->userdata("user_id")."/property-subtypes";
		$property_subtypes = $this->get_endpoint($endpoint);
		$content = json_decode($property_subtypes, true);

		return (!empty($content["data"]) && (isset($content["data"]["FieldList"]) && !empty($content["data"]["FieldList"]))) ? $content["data"]["FieldList"] : FALSE;
	}

	public function syncSavedSearches() {
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
				'access_token' => $token->access_token
		);

		$this->post_endpoint('sync-saved-searches', $access_token);

		if(!empty($this->getAllAgentSavedSearchFromSpark())){

			redirect('agent_sites/saved_searches');
		} else {

			redirect('agent_sites/saved_searches');
		}
	}

	public function updateSavedSearches() {

		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
				'access_token' => $token->access_token
		);

		$this->post_endpoint('sync-saved-searches', $access_token);

	}

	public function getListing( $id = NULL) {
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
				'access_token' => $token->access_token
		);

		$listing_data = $this->post_endpoint('spark-listing/'.$id.'/standard-fields', $access_token);
		$data = json_decode($listing_data, true);
		return $data['data'];
	}

	public function getListingPhotos( $id = NULL ) {
		$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		$access_token = array(
				'access_token' => $token->access_token
		);

		$listing_photo = $this->post_endpoint('spark-listing/'.$id.'/photos', $access_token);
		$data = json_decode($listing_photo, true);
		return $data['data'];
	}

	public function getStandardMlsStatus( $type = NULL ){

	    $endpoint = "agent/".$this->session->userdata("user_id")."/mls-status";
		$mlsstatus = $this->get_endpoint($endpoint);
		$result_info = json_decode($mlsstatus, true);

		if(!$result_info) {
			return FALSE;
		}

		$flag =  FALSE;

		foreach ($result_info["data"]["0"]["MlsStatus"]["FieldList"] as $key ) {
			if( $key["StandardStatus"] == "Closed" || $key["StandardStatus"] == "Sold" ){

				return TRUE;

				break;
			}
		}

		return $flag;

	}
	
	public function getSparkProxy($method = 'listings', $params = '', $decode = false, $agent_id=NULL) {

		$call_url = 'https://sparkapi.com/v1/'.$method.'?'.$params;
		if(empty($agent_id)){
			$tokenType = $this->dsl_model->getUserAcessType($this->session->userdata("agent_id"));
			$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		} else{
			$tokenType = $this->dsl_model->getUserAcessType($agent_id);
			$token = $this->dsl_model->getUserAccessToken($agent_id);
		}

		$header = array('Authorization: OAuth '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: SparkAPIExamples', 'Content-Type:application/json');
		if($tokenType) {
			// bearer_token
			$header = array('Authorization: Bearer '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: MyApplication' , 'Content-Type:application/json');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$content = curl_exec($ch);
		$unzipped_data = gunzip($content);
		$json_data = json_decode($unzipped_data,$decode);

		$this->dsl_model->log_request();
		$this->dsl_model->log_request_history($call_url, 'PROXY', $params);

		return isset($json_data->D->Results) ? $json_data->D->Results: null;
 	}

 	public function getSparkProxyComplete($method = 'listings', $params = '', $decode = false, $agent_id=NULL) {

		$call_url = 'https://sparkapi.com/v1/'.$method.'?'.$params;
		if($agent_id === NULL){
			$tokenType = $this->dsl_model->getUserAcessType($this->session->userdata("agent_id"));
			$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
		} else{
			$tokenType = $this->dsl_model->getUserAcessType($agent_id);
			$token = $this->dsl_model->getUserAccessToken($agent_id);
		}

		$header = array('Authorization: OAuth '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: SparkAPIExamples', 'Content-Type:application/json');
		if($tokenType) {
			// bearer_token
			$header = array('Authorization: Bearer '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: MyApplication' , 'Content-Type:application/json');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$content = curl_exec($ch);
		$unzipped_data = gunzip($content);
		$json_data = json_decode($unzipped_data,$decode);

		$this->dsl_model->log_request();
		$this->dsl_model->log_request_history($call_url, 'PROXY', $params);

		return isset($json_data->D->Results) ? $json_data->D: null;
 	}

	public function get_mls_statuses($agent_id=NULL) {

		$jsOn_mlsStatus = $this->getSparkProxy("standardfields/MlsStatus",NULL,NULL,$agent_id);
		$mlsStatusFieldsList = $jsOn_mlsStatus[0]->MlsStatus->FieldList;

		if($jsOn_mlsStatus[0]->MlsStatus->Searchable) {
			$statuses = array();
			foreach($mlsStatusFieldsList as $mfl) {
				 if($mfl->Name != "Closed" && $mfl->Name !="Sold") {
				 	if($mfl->Name != "Leased" && $mfl->Name != "Pending"){
				 		$statuses[] = $mfl->Name;
		    			
				 	}
		   		 }
			}

			$mls_status_implode = implode (",", $statuses);
			$mls_status = rawurlencode($mls_status_implode);
			return $mls_status;
		}
		else {
			return false;
		}
	}
	
	//added new function to fetch MlsStatus from SPARK
	public function getMlsStatusFromSpark( $agent_id = NULL ){

		$call_url = 'https://sparkapi.com/v1/standardfields/MlsStatus';
		$tokenType = $this->dsl_model->getUserAcessType($agent_id);
		$token = $this->dsl_model->getUserAccessToken($agent_id);

		$header = array('Authorization: OAuth '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: SparkAPIExamples', 'Content-Type:application/json');
		if($tokenType) {
			// bearer_token
			$header = array('Authorization: Bearer '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: MyApplication' , 'Content-Type:application/json');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$content = curl_exec($ch);
		$unzipped_data = gunzip($content);
		$json_data = json_decode($unzipped_data,$decode);

		return isset($json_data->D->Results) ? $json_data->D->Results: null;
	}

	public function getMlsStatus( $agent_id = NULL ){

		$jsOn_mlsStatus =  $this->getMlsStatusFromSpark($agent_id);
		$mlsStatusFieldsList = $jsOn_mlsStatus[0]->MlsStatus->FieldList;

		if($jsOn_mlsStatus[0]->MlsStatus->Searchable) {
			$statuses = array();
			foreach($mlsStatusFieldsList as $mfl) {
				$statuses[] = $mfl->Name;
			}
			return $statuses;
		}
		else {
			return false;
		}
	}

}
