<?php $this->load->view('/session/header'); ?>

<section class="slider-area">
</section>    
<?php $this->load->view('/search-view'); ?>
    <section class="featured-listing-area">
        <div class="container">
            <div class="row">
                <div class="featured-title-container">
                    <div class="trapezoid"></div>
                    <h2 class="text-center section-title">Featured Properties</h2>
                    <p class="viewall">
                        <a href="javascript:void()" target="_blank" data-original-title="" title="">View All</a>    
                    </p>
                </div>

                <div class="col-md-12">
                    <div class="featured-list">
                        <?php if(!empty($featured_listings)) { $count = 0;?>
                            <?php foreach($featured_listings as $property) :
                               
                                if(!is_array($property)){
                                    $property = get_object_vars($property); 
                                } 

                                $feature = ( is_array($property["StandardFields"]) ) ? (object) $property["StandardFields"] : $property["StandardFields"];
                                $photos =  ( is_array($feature->Photos[0]) ) ? (object) $feature->Photos[0] : $feature->Photos[0];
                                
                                if($count < 4) {
                            ?>
                                <div class="col-md-3 col-sm-6 featured-list-item">
                                    <div class="property-image">
                                        <a href="javascript:void()"><img src='<?=$photos->Uri300?>' alt="" class="img-responsive"></a>
                                        <div class="property-listing-description">
                                            <p><a href="javascript:void()" class="listing-link"><b><?=$feature->UnparsedFirstLineAddress?></b></a></p>
                                            <p>
                                                <?php
                                                    $mystring =$feature->City;
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);

                                                    if($pos === false)
                                                        echo $feature->City . ", " . $feature->StateOrProvince . " " . $feature->PostalCode;
                                                    else
                                                        echo $feature->PostalCity . ", " . $feature->StateOrProvince . " " . $feature->PostalCode;
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="property-listing-status">
                                            <?=$feature->MlsStatus; ?>                          
                                    </div>
                                    <div class="property-listing-price">
                                        <div class="property-listing-type">
                                            <?=$feature->PropertyClass;?>
                                        </div>
                                        $<?=number_format($feature->CurrentPrice);?>
                                    </div>
                                    <div class="property-quick-icons">
                                        <ul class="list-inline">
                                            
                                            <?php if (array_key_exists('BedsTotal', $feature)): ?>
                                                <?php if ($feature->BedsTotal && is_numeric($feature->BedsTotal) || strpos($feature->BedsTotal, "*")): ?>
                                                    <li><i class="fa fa-bed"></i> <?=$feature->BedsTotal?> Bed</li>
                                                <?php elseif (empty($feature->BedsTotal)): ?>
                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                                <?php else: ?>
                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <li><i class="fa fa-bed"></i> N/A</li>
                                            <?php endif ?>

                                            <?php if (array_key_exists('BathsTotal', $feature)): ?>
                                                <?php if ($feature->BathsTotal && is_numeric($feature->BathsTotal) ||  strpos($feature->BathsTotal, "*")): ?>
                                                    <li><i class="icon-toilet"></i> <?=$feature->BathsTotal?> Bath</li>
                                                <?php elseif (empty($feature->BathsTotal)): ?>
                                                    <li><i class="icon-toilet"></i> N/A</li>
                                                <?php else: ?>
                                                    <li><i class="icon-toilet"></i> N/A</li>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <li><i class="icon-toilet"></i> N/A</li>
                                            <?php endif ?>

                                             <?php
                                                if(!empty($feature->LotSizeArea) && ($feature->LotSizeArea != "0")   && is_numeric($feature->LotSizeArea)) {

                                                    if(!empty($feature->LotSizeUnits)  && ($feature->LotSizeUnits === "Acres") ){?>

                                                        <li class="lot-item">lot size area: <?=number_format($feature->LotSizeArea, 2, '.', ',')?></li>

                                                    <?php } else { ?>

                                                        <li class="lot-item">lot size area: <?=number_format($feature->LotSizeArea)?></li>

                                                    <?php } ?> 

                                            <?php } elseif(!empty($feature->LotSizeSquareFeet) && ($feature->LotSizeSquareFeet != "0")   && is_numeric($feature->LotSizeSquareFeet)) {?>

                                                    <li class="lot-item">lot size area: <?=number_format($feature->LotSizeSquareFeet)?>
                                                    </li> 

                                             <?php } elseif(!empty($feature->LotSizeAcres) && ($feature->LotSizeAcres != "0")   && is_numeric($feature->LotSizeAcres)) {?>

                                                    <li class="lot-item">lot size area: <?=number_format($feature->LotSizeAcres,2 ,'.',',')?></li>

                                            <?php } else {?>
                                                <li class="lot-item">lot size area: N/A</li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php }else{ break; }; ?>
                            <?php $count++; endforeach; ?>
                        <?php } ?>
                    </div>
                </div>  
                
            </div>
        </div>
    </section>
    
    <section class="other-listing-area">
        <div class="container">
            <div class="row">
                <?php if( !empty($new_listings) ) { ?>
                <div class="col-md-6 col-sm-12">
                    <div class="new-listings-area">
                        <h2 class="other-listing-title">New Listings
                            <span class="viewall-newlisting">
                                <a href="javascript:void()" target="_blank" data-original-title="" title="">View All</a>    
                            </span>
                        </h2>
                        
                            <?php foreach($new_listings as $new_listing) : ?>
                            <div class="col-md-6 col-sm-6 featured-list-item">
                                <div class="property-image <?php if($new_listing['StandardFields']['PropertyClass'] == 'Land' || $new_listing['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                    <a href="javascript:void()">
                                        <?php if(isset($new_listing['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                            <img src="<?=$new_listing['StandardFields']['Photos'][0]['Uri300']?>" alt="<?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                        <?php } else { ?>
                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                        <?php } ?>
                                        </a>
                                    <div class="property-listing-description">
                                        <p><a href="javascript:void()" class="listing-link"><b><?php echo $new_listing['StandardFields']['UnparsedFirstLineAddress']; ?></b></a></p>
                                        <p>
                                            <?php
                                                $mystring = $new_listing['StandardFields']['City'];
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);

                                            if($pos === false)
                                                echo $new_listing['StandardFields']['City'] . ", " . $new_listing['StandardFields']['StateOrProvince'] . " " . $new_listing['StandardFields']['PostalCode'];
                                                else
                                                echo $new_listing['StandardFields']['PostalCity'] . ", " . $new_listing['StandardFields']['StateOrProvince'] . " " . $new_listing['StandardFields']['PostalCode'];
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="property-listing-status">
                                    <?php echo $new_listing['StandardFields']['MlsStatus'];?>                               
                                </div>
                                
                                <div class="property-listing-price">
                                    <div class="property-listing-type">
                                        <?=$new_listing['StandardFields']['PropertyClass'];?>
                                    </div>
                                    $<?=number_format($new_listing['StandardFields']['CurrentPrice']);?>
                                </div>
                                <div class="property-quick-icons">
                                    <ul class="list-inline">
                                        <?php if (array_key_exists('BedsTotal', $new_listing['StandardFields'])): ?>
                                            <?php if ($new_listing['StandardFields']['BedsTotal'] && is_numeric($new_listing['StandardFields']['BedsTotal']) || strpos($new_listing['StandardFields']['BedsTotal'], "*")): ?>
                                                <li><i class="fa fa-bed"></i> <?=$new_listing['StandardFields']['BedsTotal']?> Bed</li>
                                            <?php elseif (empty($new_listing['StandardFields']['BedsTotal'])): ?>
                                                <li><i class="fa fa-bed"></i> N/A</li>
                                            <?php else: ?>
                                                <li><i class="fa fa-bed"></i> N/A</li>
                                            <?php endif ?>
                                        <?php else: ?>
                                            <li><i class="fa fa-bed"></i> N/A</li>
                                        <?php endif ?>

                                        <?php if (array_key_exists('BathsTotal', $new_listing['StandardFields'])): ?>
                                            <?php if ($new_listing['StandardFields']['BathsTotal'] && is_numeric($new_listing['StandardFields']['BathsTotal']) ||  strpos($new_listing['StandardFields']['BathsTotal'], "*")): ?>
                                                <li><i class="icon-toilet"></i> <?=$new_listing['StandardFields']['BathsTotal']?> Bath</li>
                                            <?php elseif (empty($new_listing['StandardFields']['BathsTotal'])): ?>
                                                <li><i class="icon-toilet"></i> N/A</li>
                                            <?php else: ?>
                                                <li><i class="icon-toilet"></i> N/A</li>
                                            <?php endif ?>
                                        <?php else: ?>
                                            <li><i class="icon-toilet"></i> N/A</li>
                                        <?php endif ?>

                                       <?php
                                        if(!empty($new_listing['StandardFields']['LotSizeArea']) && ($new_listing['StandardFields']['LotSizeArea'] != "0")   && is_numeric($new_listing['StandardFields']['LotSizeArea'])) {

                                            if(!empty($new_listing['StandardFields']['LotSizeUnits']) && ($new_listing['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                <li class="lot-item">lot size area: <?=number_format($new_listing['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                            <?php } else {?>

                                                <li class="lot-item">lot size area: <?=number_format($new_listing['StandardFields']['LotSizeArea'])?> </li>

                                            <?php }?>

                                        <?php } elseif(!empty($new_listing['StandardFields']['LotSizeSquareFeet']) && ($new_listing['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($new_listing['StandardFields']['LotSizeSquareFeet'])) {?>

                                                <li class="lot-item">lot size area: <?=number_format($new_listing['StandardFields']['LotSizeSquareFeet'])?></li> 

                                         <?php } elseif(!empty($new_listing['StandardFields']['LotSizeAcres']) && ($new_listing['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($new_listing['StandardFields']['LotSizeAcres'])) {?>

                                                <li class="lot-item">lot size area: <?=number_format($new_listing['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                        <?php } else {?>
                                            <li class="lot-item">lot size area: N/A</li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                                <?php endforeach; ?>
                        
                    </div>
                </div>
                <?php } ?>
                <?php if($nearby_listings) { ?>
                <div class="col-md-6 col-sm-12">
                    <div class="nearby-listings-area">
                        <h2 class="other-listing-title">Nearby Listings
                            <span class="viewall-newlisting">
                                <a href="javascript:void()" target="_blank" data-original-title="" title="">View All</a>    
                            </span>
                        </h2>
                        <ul>
                            <?php foreach($nearby_listings as $nearby) : ?>
                                <li>
                                    <div class="col-md-4 col-sm-4 no-padding-left">
                                        <div class="nearby-image">
                                            <a href="javascript:void()">
                                            <?php if(isset($nearby['StandardFields']['Photos'][0]['Uri300'])) { ?>
                                                <img src="<?=$nearby['StandardFields']['Photos'][0]['Uri300'];?>" alt="<?= $nearby['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                                            <?php } else { ?>
                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$nearby['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } ?>
                                            </a>
                                        </div>
                                        <div class="property-listing-status">
                                            <?php echo $nearby['StandardFields']['MlsStatus'];?>
                                        </div>
                                    </div>
                                    <div class="nearby-description clearfix">
                                        <div class="col-md-8 col-sm-8">
                                            <h4><a href="javascript:void()"><?php echo $nearby['StandardFields']['UnparsedFirstLineAddress']; ?></a></h4>
                                            <p>
                                                <?php
                                                    $mystring = $nearby['StandardFields']['City'];
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);

                                                if($pos === false)
                                                    echo $nearby['StandardFields']['City'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                    else
                                                    echo $nearby['StandardFields']['PostalCity'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                                ?>
                                            </p>
                                            <ul class="list-inline">
                                            <?php
                                               if($nearby['StandardFields']['BedsTotal'] && is_numeric($nearby['StandardFields']['BedsTotal'])) {?>

                                                <li><i class="fa fa-bed"></i> <?=$nearby['StandardFields']['BedsTotal']?> Bed</li>

                                            <?php } elseif(empty($nearby['StandardFields']['BedsTotal'])){?>

                                                <li><i class="fa fa-bed"></i> N/A</li>

                                           <?php }?>

                                           <?php
                                               if($nearby['StandardFields']['BathsTotal'] && is_numeric($nearby['StandardFields']['BathsTotal'])) {?>

                                                <li><i class="icon-toilet"></i> <?=$nearby['StandardFields']['BathsTotal']?> Bath</li>

                                            <?php } elseif(empty($nearby['StandardFields']['BathsTotal'])){?>

                                                <li><i class="icon-toilet"></i> N/A</li>

                                           <?php }?>

                                            <?php
                                            if(!empty($nearby['StandardFields']['LotSizeArea']) && ($nearby['StandardFields']['LotSizeArea'] != "0")   && is_numeric($nearby['StandardFields']['LotSizeArea'])) {

                                                if(!empty($nearby['StandardFields']['LotSizeUnits']) && ($nearby['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                    <li class="lot-item">lot size area: <?=number_format($nearby['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                                <?php } else {?>

                                                    <li class="lot-item">lot size area: <?=number_format($nearby['StandardFields']['LotSizeArea'])?> </li>

                                                <?php }?>


                                        <?php } elseif(!empty($nearby['StandardFields']['LotSizeSquareFeet']) && ($nearby['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($nearby['StandardFields']['LotSizeSquareFeet'])) {?>

                                                <li class="lot-item">lot size area: <?=number_format($nearby['StandardFields']['LotSizeSquareFeet'])?></li> 

                                         <?php } elseif(!empty($nearby['StandardFields']['LotSizeAcres']) && ($nearby['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($nearby['StandardFields']['LotSizeAcres'])) {?>

                                                <li class="lot-item">lot size area: <?=number_format($nearby['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                        <?php } else {?>
                                            <li class="lot-item">lot size area: N/A</li>
                                        <?php } ?>
                                        </ul>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach; ?>
                            
                        </ul>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
    
<?php $this->load->view('/saved-searches-view'); ?>

    <section class="agent-info-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 mobilepadding-0">
                    <div class="mortage-calculator">
                        <h3 class="text-center">CONTACT AGENT</h3>

                        <div class="question-mess" ></div>  <br/>    
                        <form action="" method="POST" class="customer_questions" id="customer_questions" >
                            <div class="form-group">
                                <input type="text" name="name" value="" class="form-control" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" value="" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="" value="" cols="30" rows="10" class="form-control" placeholder="Message" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-default btn-block submit-button submit-question-button" style="text-transform:uppercase;">Submit</button>
                        </form>

                    </div>
                </div>

                <div class="col-md-8 col-sm-12 nopadding-left">
                    <div class="col-md-3 col-sm-4 nopadding">
                        <div class="agent-image">
                            <img src="<?=AGENT_DASHBOARD_URL . 'assets/upload/photo/'.$user_info->agent_photo?>" class="img-thumbnail" width="300">
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8 nopadding">

                <!-- Get data from Featured Listings -->   

                    <h4 class="agent-name">
                        <?=$user_info->first_name." ".$user_info->last_name?>
                    </h4>
                    <div class="about-agent">
                        <?php echo $user_info->about_agent;?>
                    </div>
                    <strong>
                        <ul class="agent-detail">
                            <li><i class="fa fa-phone"></i> Phone Number: <?=$user_info->phone;?> </li>
                            <li><i class="fa fa-2x fa-mobile"></i> Mobile Number: <?=$user_info->mobile;?>  </li>
                            <li><i class="fa fa-map-marker"></i> Address: <?=$user_info->address;?></li>
                            <li><i class="fa fa-envelope"></i> Email: <?=$user_info->email;?></li>
                            <li><i class="fa fa-university"></i> Brokerage Name:<?=$user_info->broker;?></li>                                 
                        </ul>                     
                    </strong>
                     <div class="col-md-12 properties-sold">
                        <div class="sold-property-container"> 

                        <?php foreach($sold_listings as $sold) : ?>  

                            <div class="other-listing-item">
                                <div class="sold-banner"><img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/sold.png"></div>
                                <div class="sold-image">
                                    <a href="javascript:void()">
                                        <?php if(isset($sold->standard_fields[0]->StandardFields->Photos[0]->Uri300)){?>
                                            <img src="<?php echo $sold->standard_fields[0]->StandardFields->Photos[0]->Uri300;?>" alt="<?php echo $sold->standard_fields[0]->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                        <?php } else { ?>
                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $sold->standard_fields[0]->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive">
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="sold-property-details">
                                    <p><a href="javascript:void()"><?=$sold->standard_fields[0]->StandardFields->UnparsedFirstLineAddress;?></a></p>
                                    <p><i class="fa fa-map-marker"></i>
                                        <?php
                                            $mystring = $sold->standard_fields[0]->StandardFields->City;
                                            $findme   = '*';
                                            $pos = strpos($mystring, $findme);
                                            
                                            if($pos === false) 
                                                echo $sold->standard_fields[0]->StandardFields->City . ", " . $sold->standard_fields[0]->StandardFields->StateOrProvince . " " . $sold->standard_fields[0]->StandardFields->PostalCode;
                                            else
                                                echo $sold->standard_fields[0]->StandardFields->PostalCity . ", " . $sold->standard_fields[0]->StandardFields->StateOrProvince . " " . $sold->standard_fields[0]->StandardFields->PostalCode;
                                        ?>
                                    </p>
                                    <p><i class="fa fa-usd"></i> <?=number_format($sold->standard_fields[0]->StandardFields->CurrentPrice);?></p>
                                </div>
                            </div>

                        <?php endforeach; ?>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('/session/footer'); ?>
