<div class="save-search-slider" style="margin-top:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                  <div id="save-search-grid" class="owl-carousel owl-theme">
                    <?php if(!empty($saved_searches)) { 
                        foreach($saved_searches as $saved) {
                          if(!empty( $saved->results )) {
                    ?>
                     <div class="item" style="background: linear-gradient( rgba(125, 125, 125, 0.45), rgba(123, 130, 130, 0.63) ), url('<?=$saved->picture?>');">
                        <div class="col-md-6 col-sm-5 col-xs-12 padding-0">
                            <div class="row">
                                <div class="col-md-12">
                                   
                                     <div class="save-search-desc">
                                        <h1 class="save-search-name"><?=$saved->search_name?></h1>
                                          <a href="javascript:void()" class="btn btn-view" target="_blank" >View All</a> 
                                     </div> 
                                </div>
                            </div>    
                        </div>
                        <div class="col-md-6 col-sm-7 col-xs-12 padding-0"> 
                            <?php foreach( $saved->results as $results) : ?>
                              <div class="col-md-6 col-sm-6 col-xs-12 padding-0">
                                <div class="save-property-image" >
                                        <a href="javascript:void()"><img src="<?=$results["StandardFields"]["Photos"][0]["UriThumb"];?>" alt="" class="img-responsive" style="width:100%;"></a>
                                </div>
                                <div class="property-listing-status">
                                    <?=$results["StandardFields"]["MlsStatus"];?>                         
                                </div>
                                <div class="save-property-price">
                                    <p> <?=$results["StandardFields"]["PropertyClass"];?></p>
                                    <p> $<?=number_format($results["StandardFields"]["CurrentPrice"]);?></p>
                                </div>
                                <div class="save-property-title">
                                    <p class="save-property-name">
                                       <?=$results["StandardFields"]["CurrentPrice"];?>
                                    </p>
                                    <p>
                                         <?php
                                            $mystring = $results['StandardFields']['City'];
                                            $findme   = '*';
                                            $pos = strpos($mystring, $findme);

                                        if($pos === false)
                                            echo $results['StandardFields']['City'] . ", " . $results['StandardFields']['StateOrProvince'] . " " . $results['StandardFields']['PostalCode'];
                                            else
                                            echo $results['StandardFields']['PostalCity'] . ", " . $results['StandardFields']['StateOrProvince'] . " " . $results['StandardFields']['PostalCode'];
                                        ?>
                                    </p>
                                </div>
                                <div class="property-quick-icons">
                                    <ul class="list-inline">
                                          <?php if (array_key_exists('BedsTotal', $results['StandardFields'])): ?>
                                              <?php if ($results['StandardFields']['BedsTotal'] && is_numeric($results['StandardFields']['BedsTotal']) || strpos($results['StandardFields']['BedsTotal'], "*")): ?>
                                                  <li><i class="fa fa-bed"></i> <?=$results['StandardFields']['BedsTotal']?> Bed</li>
                                              <?php elseif (empty($results['StandardFields']['BedsTotal'])): ?>
                                                  <li><i class="fa fa-bed"></i> N/A</li>
                                              <?php else: ?>
                                                  <li><i class="fa fa-bed"></i> N/A</li>
                                              <?php endif ?>
                                          <?php else: ?>
                                              <li><i class="fa fa-bed"></i> N/A</li>
                                          <?php endif ?>

                                          <?php if (array_key_exists('BathsTotal', $results['StandardFields'])): ?>
                                              <?php if ($results['StandardFields']['BathsTotal'] && is_numeric($results['StandardFields']['BathsTotal']) ||  strpos($results['StandardFields']['BathsTotal'], "*")): ?>
                                                  <li><i class="icon-toilet"></i> <?=$results['StandardFields']['BathsTotal']?> Bath</li>
                                              <?php elseif (empty($results['StandardFields']['BathsTotal'])): ?>
                                                  <li><i class="icon-toilet"></i> N/A</li>
                                              <?php else: ?>
                                                  <li><i class="icon-toilet"></i> N/A</li>
                                              <?php endif ?>
                                          <?php else: ?>
                                              <li><i class="icon-toilet"></i> N/A</li>
                                          <?php endif ?>

                                        <?php
                                        if(!empty($results['StandardFields']['LotSizeArea']) && ($results['StandardFields']['LotSizeArea'] != "0")   && is_numeric($results['StandardFields']['LotSizeArea'])) {

                                            if(!empty($results['StandardFields']['LotSizeUnits']) && ($results['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                                <li class="lot-item">lot size area: <?=number_format($results['StandardFields']['LotSizeArea'], 2, '.', ',' )?> </li>

                                            <?php } else {?>

                                                <li class="lot-item">lot size area: <?=number_format($results['StandardFields']['LotSizeArea'])?> </li>

                                            <?php }?>

                                        <?php } elseif(!empty($results['StandardFields']['LotSizeSquareFeet']) && ($results['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($results['StandardFields']['LotSizeSquareFeet'])) {?>

                                                <li class="lot-item">lot size area: <?=number_format($results['StandardFields']['LotSizeSquareFeet'])?></li> 

                                         <?php } elseif(!empty($results['StandardFields']['LotSizeAcres']) && ($results['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($results['StandardFields']['LotSizeAcres'])) {?>

                                                <li class="lot-item">lot size area: <?=number_format($results['StandardFields']['LotSizeAcres'],2 ,'.',',')?></li>
                                        <?php } else {?>
                                            <li class="lot-item">lot size area: N/A</li>
                                        <?php } ?>
                                        </ul>
                                </div>
                              </div>
                              <?php endforeach; ?> 
                                               
                        </div>
                    </div>
                        <?php } ?>  
                      <?php } ?>
                  <?php } ?>
                </div>    
                </div>
                
            </div>
        </div>
    </div>