<!doctype html>
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>New Demo Account</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="<?=base_url()?>/assets/images/default-favicon.png"/>
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/agent-haven/main.css">
    <script src="<?=base_url()?>assets/js/agent-haven/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontello/css/fontello.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/agent-haven/vegas.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/agent-haven/selectize.bootstrap3.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/agent-haven/owl.carousel.css" type="text/css">
    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <nav class="navbar navbar-fixed-top main-navbar home-navbar" role="navigation">
        <div class="container">
            <div class="col-md-12">
                <p class="text-right registration-nav">
                    <a href="login.html">Login</a> <span>|</span> <a href="signup.html">Sign Up</a>    
                </p>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=base_url()?>demo/agent_haven">
                <?php if(!empty($user_info->logo)){?>

                    <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/logo/<?php echo $user_info->logo;?>" alt="" width="200px" height="65px">

                <?php } else {?>

                    <img src="<?=base_url()?>assets/images/dashboard/demo/agent-haven/agent-logo-2.jpg" alt="" width="200px" height="65px">
                <?php }?>
                </a>
                <p class="tagline"><?php echo $tag_line;?></p>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right main-nav nav-darken">
                    <li><a href="<?=base_url()?>demo/agent_haven">Home</a></li>
                    <li><a href="agent_haven/listings">Find a Home</a></li>
                    <li><a href="agent_haven/buyer">Buyer</a></li>
                    <li><a href="agent_haven/seller">Seller</a></li>
                    <li><a href="agent_haven/about">About</a></li>
                    <!-- <li><a href="agent_haven/blog-detail-page">Blog</a></li> -->
                    <li><a href="agent_haven/contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
