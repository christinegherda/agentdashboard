<section class="filters">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="filter-property">
                        <div class="filter-tabs">
                            <h1><?php echo $banner_tagline;?></h1>
                        </div>
                        <form action="agent_haven/listings">
                            <div class="input-group">
                                <input type="text" class="form-control home-search" placeholder="State, City, Zip, MLS ID">
                                <span class="input-group-btn">
                                    <button class="btn btn-default submit-button" type="button">Search</button>
                                </span>
                            </div>
                        </form>
                        
                        <div class="help-info">
                            <div class="arrow-up"></div>
                            <p>Examples:</p>
                            <ul>
                                <li>
                                    <div class="col-md-4">
                                        <p>City, State</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>Las Vegas, NV</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-4">
                                        <p>Zip Code</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>90210</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-4">
                                        <p>Address</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>910 Hamilton ave, Campbell, Ca</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-4">
                                        <p>County</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>Santa Clara County, Ca</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-4">
                                        <p>MLSID</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>#12345</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>