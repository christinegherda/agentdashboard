<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

    <title>AS Status</title>
  </head>
  <body>
    <div class="container-fluid">
      <h1>AS Stats</h1>
      <hr>
      <div class="card">
        <div class="card-body">
          <p>API Call Count : Separated interval : 5 Mins</p>
        </div>
        <table class="table table-hover table-bordered">
            <tr>
              <th>ID</th>
              <th>Count</th>
              <th>Type</th>
              <th>Date</th>
            </tr>
            <?php
              foreach($requests as $request) {
            ?> 
            <tr>
              <td><?=$request->id?></td>
              <td><?=$request->count?></td>
              <td><?=ucfirst($request->type)?></td>
              <td><?=date("Y/m/d H:i:s", strtotime($request->request_time)) . " " .date_default_timezone_get()?></td>
            </tr>
            <?php
              }
            ?>
          </table> 
      </div>
      <hr>
      <div class="card">
        <div class="card-body">
          <p>API Call History</p>
        </div>
        <table class="table table-hover table-bordered">
            <tr>
              <th>ID</th>
              <th>Endpoint</th>
              <th>Full Endpoint</th>
              <th>Type</th>
              <th>Source</th>
              <th>Request Body</th>
              <th>Date</th>
            </tr>
            <?php
              foreach($history as $hist) {
            ?>
            <tr>
              <td><?=$hist->id?></td>
              <td><small><?=substr(str_replace("https://sparkapi.com/v1/", "", $hist->endpoint), 0, 30);?>...</small></td>
              <td>
                <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#exampleModal<?=$hist->id?>">View full</button>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal<?=$hist->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?=$hist->id?>" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel<?=$hist->id?>">Endpoint</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <code>
                          <?php
                            echo $hist->endpoint;
                          ?>
                        </code>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                
              </td>
              <td><?=ucfirst($hist->type)?></td>
              <td><?=ucfirst($hist->source)?></td>
              <td>
                <?php
                  if(!empty($hist->body) && $hist->body != '""' && $hist->body != 'null') {
                ?>
                <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#exampleModalBody<?=$hist->id?>">View</button>
                <!-- Modal -->
                <div class="modal fade" id="exampleModalBody<?=$hist->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelBody<?=$hist->id?>" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabelBody<?=$hist->id?>">Body</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <code>
                          <?php
                            echo $hist->body;
                          ?>
                        </code>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                <?php 
                  }
                  else {
                    echo "No Data";
                  }
                ?>
              </td>
              <td><?=date("Y/m/d H:i:s", strtotime($hist->date)) . " " .date_default_timezone_get()?></td>
            </tr>
            <?php
              }
            ?>
          </table> 
      </div>
       
      
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  </body>
</html>