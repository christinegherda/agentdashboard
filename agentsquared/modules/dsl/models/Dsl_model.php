<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Dsl_model extends CI_Model { 

    public function __construct() {
        parent::__construct();
    }
    
    // public function request_stats() {
    //     // Request Count
    //     $this->db->select("*")
    //         ->from("api_requests")->order_by('request_time', 'desc');
    //     $api_requests = $this->db->get()->result();

    //     // Request History
    //     $this->db->select("*")
    //         ->from("api_requests_history")->order_by('date', 'desc');
    //     $api_requests_history = $this->db->get()->result();

    //     return array('requests' => $api_requests, 'history' => $api_requests_history);
    // }

    public function log_request($type = 'dsl') {
        echo true;
        // $this->db->select("*") 
        //         ->from("api_requests")
        //         ->where("request_time >= (CURRENT_TIMESTAMP() - INTERVAL 5 MINUTE)");

        // $query = $this->db->get()->row();

        // if(count($query)) {
            
        //     $data = array('count' => ($query->count + 1));
        //     $this->db->where('id', $query->id);
        //     $this->db->update("api_requests", $data);
        // }
        // else {
        //     $data = array('count' => 1);
        //     $this->db->insert("api_requests", $data);
        // }
    }

    public function log_request_history($endpoint = NULL, $type = NULL, $body = NULL) {
        echo true;
        // $data = array(
        //     'endpoint'  => $endpoint,
        //     'type'      => $type,
        //     'source'    => 'agentdashboard',
        //     'body'      => json_encode($body)
        // );
        // $this->db->insert("api_requests_history", $data);
    }

    public function getUsersInfo( $user_id = NULL ) {
        $this->db->select("*")
                ->from("users")
                ->where("id", $user_id);

        return $this->db->get()->row();
    }

    public function getUserAcessType($agent_id = NULL) {
        /* Distinguising between Bearer and Auth Token users */
        $this->db->select("auth_type")
                ->from("users")
                ->where("agent_id", $agent_id);
        $ret = $this->db->get()->row();
        $type = $ret->auth_type;

        return $type;
    }
    public function getUserAccessToken( $agent_id = NULL ) {
        /* Distinguising between Bearer and Auth Token users */
        $this->db->select("auth_type")
                ->from("users")
                ->where("agent_id", $agent_id);

        $ret = $this->db->get()->row();

        $type = $ret->auth_type;

        if($type) {
            $this->db->select("bearer_token as access_token")
                    ->from("user_bearer_token")
                    ->where("agent_id", $agent_id);
        }
        else {
            $this->db->select("access_token")
                    ->from("users_idx_token")
                    ->where("agent_id", $agent_id);
        }

        return $this->db->get()->row();
    }

    public function getUserAccessTokenOld( $agent_id = NULL ) {
        $this->db->select("access_token")
                ->from("users_idx_token")
                ->where("agent_id", $agent_id);

        return $this->db->get()->row();
    }

    public function getUserBearerToken( $agent_id = NULL ) {
        $this->db->select("bearer_token")
                ->from("user_bearer_token")
                ->where("agent_id", $agent_id);

        return $this->db->get()->row();
    }

    public function getSliderPhotos()
    {
        $this->db->select("*")
                ->from("slider_photos")
                ->where("user_id", $this->session->userdata("user_id"));

        return $this->db->get()->result();
    }

    public function getSaveSearches( $limit = NULL ) {

        $this->db->select("id, search_id, json_saved_search_updated,img as picture")
                ->from("saved_searches")
                ->where("user_id", $this->session->userdata("user_id"))
                ->where("is_recommended_search", "1");  

        if( !empty($limit) )
        {
            $this->db->limit($limit);
        }

        $query = $this->db->get(); 

        if( $query->num_rows() > 0 )
        {
            $data = $query->result();
            
            foreach ($data as &$key) {
                $json_decoded = json_decode($key->json_saved_search_updated);
                $key->search_name = $json_decoded->Name;
                $key->filter = $json_decoded->Filter;
            }

            return $data;
        }

        return FALSE;
    }

    public function save_testimonial($data=array()) {
        $ret = FALSE;
        if($data) {
            $this->db->insert('testimonials', $data);
            $ret = TRUE;
        }
        return $ret;
    }
}   