<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		
	}

	public function save_branding_info($id=NULL, $data=array()) {

        $ret = FALSE;

        if($id && $data) {
            $this->db->where('id', $id);
            $this->db->update("users", $data);

            $ret = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        }  

        return $ret;
    }

    public function login_bypass($id=NULL) {

        $ret = FALSE;

        if($id) {
            $this->db->select("id, agent_id, email, code, created_on, first_name, last_name")
                    ->from("users")
                    ->where("id", $id);

            $ret = $this->db->get()->row();
        }

        return $ret;
    }

    public function update_logo_info($datas = array(), $id=NULL) {

        $img["logo"] = $datas['file_name'];
        $this->db->where("id", $id);
        $this->db->update("users", $img);

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function update_favicon_info($datas = array(), $id=NULL) {

        $img["favicon"] = $datas['file_name'];
        $this->db->where("id", $id);
        $this->db->update("users", $img);

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function add_existing_domain($data=array()) {

        $ret = FALSE;

        if($data) {
            $has_user = $this->check_user_domain($data["agent"]);

            if($has_user) {
                $this->db->where("agent", $data["agent"]);
                $this->db->update("domains", $data);
                $ret = ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
            } else {
                $this->db->insert("domains", $data);
                $id = $this->db->insert_id();
                $ret = TRUE;
            }
        }

        return $ret;
    }

    public function add_purchase_domain($post = array()) {

        $this->db->select("domains")
                ->from("domains")
                ->where("domains", $post["domain_name"])
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {

            return array('success' => FALSE, 'type' => "exist_domain", 'error' => 'Selected domain is already reserved by agent squared!');      
        
        } else {

            $domain['domains']  = $post["domain_name"];
            $domain["agent"] = $post["user_id"];
            $domain["date_created"] = date("Y-m-d h:m:s");
            $domain["status"] = "pending";
            $domain["type"] = "agent_site_domain";
            $domain["existing_domain"] = 0;
            $domain["is_subdomain"] = 0;

            if(!$this->check_agent($post["user_id"])) {
                $this->db->insert("domains", $domain);

                if($this->db->insert_id()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully added!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }

            } else {

                $arr = array("agent" => $post["user_id"], "type" => "agent_site_domain");
                $this->db->where($arr);
                $this->db->update("domains", $domain);

                if($this->db->affected_rows()) {
                    $response["message"] = "Domain info has been successfully added!";
                    $response["success"] = TRUE;
                    return array('success' => TRUE, 'message' => 'Domain info has been successfully updated!'); 
                }   
                else {
                    return array('success' => FALSE, 'type' => 'database_error', 'error' => 'Unable to reserved your desired domain! Please contact to admin.');         
                }
            }
        }
    }

    public function add_subdomain($post = array()) {

        if($post) {

            $data = array(
                "date_modified" => date("Y-m-d h:m:s"),
                "subdomain_url" => $post["subdomain_url"],
            );

            $arr = array(
                "agent" => $post["agent"],
                "type"  => $post["type"]
            );

            $this->db->where($arr);
            $this->db->update("domains", $data);

            if(!$this->db->affected_rows()) {
                $this->db->insert('domains', $post);
            } 

            return TRUE;
        }

    }

    public function check_domain_exist($domain_name=NULL, $user_id=NULL) {

        $ret = FALSE;

        if($domain_name) {
            $this->db->select("id")
                    ->from("domains")
                    ->where("domains", $domain_name);

            $query = $this->db->get()->row();

            $ret = ($query) ? $query : FALSE;

            if(!$ret) {
                $has_user = $this->check_user_domain($user_id);
                if($has_user) {
                    $ret = TRUE;
                }
            }
        }

        return $ret;
    }

    public function check_user_domain($user_id=NULL) {
        $ret = FALSE;

        $this->db->select("domains")
                ->from("domains")
                ->where("agent", $user_id);

        $ret = $this->db->get()->row();

        return $ret;
    }

    public function check_agent($user_id) {

        $ret = FALSE;

        $query = $this->db->get_where("domains", array("agent" => $user_id, "type" => "agent_site_domain"));

        if($query->num_rows() > 0) {
            $ret = TRUE;
        }

        return $ret;
    }

}