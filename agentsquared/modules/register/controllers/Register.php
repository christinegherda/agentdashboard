<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MX_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->model('registration_model', 'reg_model');
	}

	public function wizard() {

		$data["title"] = "Register";

		if($this->input->get('user_id') && $this->input->get('app_type')) {

			$data["user_id"] = $this->input->get("user_id");
			$data["app_type"] = $this->input->get("app_type");
		}

		$this->load->view('register', $data);
	}

	public function save_branding_info() {

		$id = $this->input->post('user_id');

		$data = array(
			'site_name' => ($this->input->post('site_name')) ? $this->input->post('site_name') : "",
			'tag_line' => ($this->input->post('tag_line')) ? $this->input->post('tag_line') : "",
			'banner_tagline' => ($this->input->post('banner_tagline')) ? $this->input->post('banner_tagline') : "",
		);

		$brand = $this->reg_model->save_branding_info($id, $data);

		$this->do_image_upload($id);

		if($brand) {

			$row = $this->reg_model->login_bypass($id);

			$arr = array(
				'user_id'		=> $row->id,
				'agent_id'		=> $row->agent_id,
				'email'	 		=> $row->email,
				'code' 			=> $row->code,
				'identity' 		=> $row->email,
				'old_last_login' => $row->created_on,
				'logged_in_auth' => true,
				'agent_idx_website' => true
			);

			$this->session->set_userdata($arr);

			$my_account = Modules::run("idx_login/myaccount", $this->session->userdata("access_token"));

			$agent_arr = array(
	        	'agent_name'  	=> isset($my_account["D"]["Results"][0]["FirstName"]) ? $my_account["D"]["Results"][0]["FirstName"] : 'Admin',
	           	'agent_photo'	=> isset($my_account["D"]["Results"][0]["Images"][0]["Uri"]) ? $my_account["D"]["Results"][0]["Images"][0]["Uri"] : AGENT_DASHBOARD_URL . 'assets/images/no-profile-img.gif',
	            'site_title' 	=> ($this->input->post('site_name')) ? $this->input->post('site_name') : 'Your Website',
	        );

			$this->session->set_userdata('agentdata', $agent_arr);

			//Sync Data
			$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');
			$con['title'] = "Sync MLS Data";
			$con['user_id'] = $row->id;

			$subscription = Modules::run('idx_login/agent_subscription', $row->agent_id);

			if($subscription == "idx") {
				$con['agent_idx_type'] = "TRUE";
			} elseif($subscription == "spw") {
				$con['agent_idx_type'] = "FALSE";
			} else {
				redirect('login/auth/logout', 'refresh');
			}

			$this->load->view('idx_login/sync_loader', $con);
		}

	}

	public function do_image_upload($id) {

		if(isset($_FILES['userfile'])) {

			$this->load->library('upload');

			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '1150000';
			$config['max_width'] = '1024';
			$config['max_height'] = '1024';

			$files = $_FILES;
			$count = count($_FILES['userfile']['name']);

			for($i=0; $i < $count; $i++) { 

				if($i == '0') {
					if($_SERVER["SERVER_NAME"] == "localhost") {
						$config['upload_path'] = FCPATH.'assets/upload/logo';
					} else {
						$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/assets/upload/logo';
					}		
				} elseif($i == '1') {
					if( $_SERVER["SERVER_NAME"] == "localhost") {
						$config['upload_path'] = FCPATH.'assets/upload/favicon';
					} else {
						$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/assets/upload/favicon';
					}
				} else {
					if($_SERVER["SERVER_NAME"] == "localhost") {
						$config['upload_path'] = FCPATH.'assets/upload/photo';
					} else {
						$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/assets/upload/photo';
					}
				}

				$this->load->library('upload', $config);

				$_FILES['userfile']['name']= $files['userfile']['name'][$i];
		        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
		        $_FILES['userfile']['size']= $files['userfile']['size'][$i];   
		        
				$this->upload->initialize($config); 

				if($this->upload->do_upload()) {

					$img_data = $this->upload->data();

					if($i == '0') {
						$res = $this->reg_model->update_logo_info($img_data, $id);
					} elseif($i == '1') {
						$res = $this->reg_model->update_favicon_info($img_data, $id);
					}

					$data = ($res) ? array('status' => TRUE) : array('status' => FALSE);

				} else {
					$data = array('status' => FALSE, "error" => $this->upload->display_errors() );					
				}

			}

			return $data;
		}
	}

	public function domains() {

		if(isset($_GET['domain_check']) AND $_GET['domain_check'] == 1) {

			$id = 'troymccas';
			$pw = 'b5n7RolJukp3';
			$d['status'] = 0;

			if(isset($_GET['domain_name'])) {
				$sld = trim($_GET['domain_name']);
				$tld = trim($_GET['suffix']);
				$apiurl = "https://resellertest.enom.com/";

				//load xml document for to check if the domain is available or not
				$check = $apiurl.'interface.asp?command=check&sld='.$sld.'&tld='.$tld.'&responsetype=xml&uid='.$id.'&pw='.$pw;
				$xmlCheck = simplexml_load_file($check); //or die("Error: Cannot create object");

				$d['domainName'] = $xmlCheck->DomainName;
				$rrpCode = $xmlCheck->RRPCode;

				if($rrpCode == '210') {
					$d['status'] = 1;
					$getPrice = $apiurl . 'interface.asp?command=PE_GETPRODUCTPRICE&uid='.$id.'&pw='.$pw.'&ProductType=10&tld='.$tld.'&responsetype=xml';
					$getpricexml = simplexml_load_file($getPrice);

					$d['domainPrice'] = $getpricexml->productprice->price;
				}
				else {

					$d['domain'] 	= $sld;
					$d['tld']		= $tld;
				}
			}
			//echo json_encode($d);
			exit(json_encode($d));
		}

	}

	public function suggested_domains() {
		if( isset($_GET['domain_tld_get']) AND $_GET['domain_tld_get'] == 1) {
			$d['status'] = 1;

			$id = 'troymccas';
			$pw = 'b5n7RolJukp3';
			$apiurl = "https://resellertest.enom.com/";

			/* Recommended TLD's */
			$prioDomains = array(
				"com", 
			 	"net", 
			 	"me",  
			 	"org", 
			 	"us",  
			 	"info",
			 	"biz" 
			);
			$domainList = "DomainList=";

			if(isset($_GET['domain'])) {
				foreach($prioDomains as $prioDomain) {
				 	$domainList .= $_GET['domain'].".".$prioDomain.",";
				}
			}
			
			$getTLDs = $apiurl . 'interface.asp?Command=Check&responsetype=xml&uid='.$id.'&pw='.$pw.'&'.rtrim($domainList, ",");
			$tlds = simplexml_load_file($getTLDs);

			$getTLDPrice = $apiurl . 'interface.asp?Command=PE_GETDOMAINPRICING&responsetype=xml&uid='.$id.'&pw='.$pw;
			$tldPrice = simplexml_load_file($getTLDPrice);

			$domains = array();
			$dStatus = array();
			foreach($tlds as $key => $val) {
				$domains[] = $key . "_" . $val;
			}

			// $xmlTlds->tldlist
			$d['domainCount'] = $tlds->DomainCount;

			$d['domains'] = $domains;
			$d['dstatus'] = $dStatus;

			$d['tlds'] = $tlds;
			$d['tldPrice'] = $tldPrice->pricestructure;
			exit(json_encode($d));
		}
		if(isset($_GET['get_domain_tlds'])) {
			$d['status'] = 1;
			$apiurl = "https://reseller.enom.com/";
			$id = 'troymccas';
			$pw = 'b5n7RolJukp3';
			$getTLDs = $apiurl . 'interface.asp?Command=TLD_GetTLD&responsetype=xml&uid='.$id.'&pw='.$pw.'&Category=Real%20Estate';
			$tlds = simplexml_load_file($getTLDs);

			$d['tld'] = $tlds->TLD_GetTLD;
			exit(json_encode($d));
		}
	}

	public function add_existing_domain() {

		if($this->input->is_ajax_request()) {

			$domain_name = ($this->input->post("existing_domain_name")) ? $this->input->post("existing_domain_name") : FALSE;
			$user_id = $this->input->post("user_id");

			if($domain_name) {

				$check = $this->check_domain_exist($domain_name, $user_id);

				if($check) {
					$response = array("success" => FALSE, "domain_name" => $domain_name, "message" => "This domain is already reserved by another agent. Please select another domain.");
				} else {
					//check if the domain is valid
					if(preg_match("/^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/", $domain_name)) {

					  	$data = array(
							'domains' 			=> $domain_name,
							'agent'				=> $this->input->post("user_id"),
							'status'			=> "registered",
							'type'				=> "agent_site_domain",
							'existing_domain'	=> 1,
							'is_subdomain'		=> 0,
							'date_created'		=> date("Y-m-d h:m:s")
						);

						if($this->reg_model->add_existing_domain($data)) {
							$response = array("success" => TRUE, "domain_name" => $domain_name, "message" => "Domain Successfully Reserved! <br> Please Click Submit to proceed.");
						}

					} else {
						$response = array("success" => FALSE, "domain_name" => $domain_name, "message" => "The domain name you entered is invalid.");
					}
				}

				exit(json_encode($response));
			}

		} else {
			exit('No direct script access allowed');
		}

	}

	public function purchase_domain() {	

		if(isset($_GET['domain_name'])) {

			$post['domain_name'] = $_GET["domain_name"];
			$post['user_id'] = $_GET["user_id"];
			$post['type'] = $_GET["app_type"];

			$result = $this->reg_model->add_purchase_domain($post);

			if($result["success"] == 1) {

				$id = 'troymccas';
				$pw = 'b5n7RolJukp3';

				$d['status'] = 0;

				$domain_name = explode(".", $_GET['domain_name']);
				$sld = trim($domain_name[0]);
				$tld = trim($domain_name[1]);
				$apiurl = "https://resellertest.enom.com/";

				$rrpCode = '200';

				if($rrpCode == '200') {

				}

				$response = array("success" => TRUE, 'message' => 'You have successfully reserve a domain name [ '.$_GET["domain_name"].']. We will contact you as soon as possible once your domain is ready.');

			} else {

            	$response = array("success" => FALSE, 'error' => $result["error"]);
			}
		}
		exit(json_encode($response));
	}

	public function check_domain_exist($domain_name) {

		$ret = FALSE;

		if($domain_name) {
			$ret = $this->reg_model->check_domain_exist($domain_name);
		}

		return $ret;
	}

	public function set_credit($token) {
		$this->session->set_userdata("access_token", $token);
		printA($this->session->all_userdata());
	}

	public function right_triangle($size) {
		for($i=0; $i<=$size; $i++) {
			for($x=0; $x<=$i; $x++) { 
				echo "*";
			}
			echo "<br>";
		}
	}

	public function left_triangle($size) {
		for($i=0; $i<=$size; $i++) {
			for($x=$size; $x>=$i; $x--) {
				echo "*";
			}
			echo "<br>";
		}
	}

}

?>
