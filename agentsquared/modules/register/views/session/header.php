<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/register/transition-animations.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/register/demo-single-page.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/register/fileUploader.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/register/register.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css" type="text/css"> 
    <script src="<?= base_url()?>assets/js/register/modernizr.custom.js"></script>
    <script type="text/javascript">$base_url = "<?php echo base_url(); ?>"; </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-89782830-1', 'auto');
      ga('send', 'pageview');

    </script>
</head>
<body>
    