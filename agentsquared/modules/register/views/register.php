<?php $this->load->view('register/session/header'); ?>
<div class="pt-wrapper">
    <div class="pt-page pt-page-1">
	    <section class="sign-up-page clearfix">
	        <div class="section-content section-flow1">
	            <div class="container">
	                <div class="row">
	                    <div class="col-md-6 col-sm-12">
	                        <h1>Welcome to</h1>
	                        <img src="<?= base_url().'/assets/images/logo_brand.png'?>">
	                    </div>
	                    <div class="col-md-6 col-sm-12 text-right">
    						<button class="pt-trigger btn btn-green" data-animation="42" data-goto="-1">Lets Start!</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="section-footer">
	            <div class="container">
	                <div class="row">
	                    <div class="col-md-12 text-center">
	                        <p>Trusted Partner of: <img src="<?= base_url().'/assets/img/register/flexmls-logo.png'?>"></p>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
    </div>
    <div class="pt-page pt-page-2">
	    <section class="sign-up-page clearfix">
	        <div class="section-content section-flow2">
	            <div class="container">
	                <div class="row">
	                	<form action="<?=base_url();?>register/save_branding_info" class="form-horizontal" method="POST" enctype="multipart/form-data" id="brand_info_form">
		                    <div class="col-md-12 text-center">
		                        <div class="logo-brand">
		                        	<img src="<?= base_url().'/assets/images/logo_brand.png'?>" class="img-responsive">
		                        </div>
		                        <p>Tell us about your brand.</p>
		                    </div>
		                    <div class="col-md-12 text-center">
		                    	<label class="empty error"></label>
		                    </div>
		                    <div class="col-md-offset-3 col-sm-offset-3 mobile-left">
			                    <div class="form-group">
				                    <div class="input-group col-md-8">
									  <span class="input-group-addon">Sitename</span>
									  <input type="text" name="site_name" id="r_site_name" class="form-control">
									  <input type="hidden" name="user_id" id="r_user_id" value="<?=$user_id;?>" class="form-control">
									</div>
			                    </div>
			                    <div class="form-group">
				                    <div class="input-group col-md-8">
									  <span class="input-group-addon">Tagline</span>
									  <input type="text" name="tag_line" id="r_tag_line" class="form-control">
									</div>
			                    </div>
			                    <div class="form-group">
				                    <div class="input-group col-md-8">
									  <span class="input-group-addon">Banner Tagline</span>
									  <input type="text" name="banner_tagline" id="r_banner_tagline" class="form-control">
									</div>
			                    </div>
		                    </div>
		                    <div class="col-md-offset-4 col-sm-offset-3 col-md-4 col-sm-7 text-center">
		                    	<div class="form-group">
			                    	<p>Your Logo</p>
			                    	<input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
		                    	</div>
		                    </div>
		                    <div class="col-md-offset-4 col-sm-offset-3 col-md-4 col-sm-7 text-center">
		                    	<div class="form-group">
			                    	<p>Your Icon</p>
			                    	<input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
			                    </div>
		                    </div>
		                    <div class="col-md-12 text-center next-button">
		                    	<button type="button" class="pt-trigger btn btn-green" data-animation="42" data-goto="-1" id="submit_info_btn">Next</button>
		                    </div>	
		            	</form>
	                </div>
	            </div>
	        </div>
	    </section>
    </div>
    <div class="pt-page pt-page-3">
	    <section class="sign-up-page clearfix">
	        <div class="section-content section-flow3">
	            <div class="container">
	                <div class="row">
	                    <div class="col-md-12 text-center">
	                        <div class="logo-brand">
	                        	<img src="<?= base_url().'/assets/images/logo_brand.png'?>" class="img-responsive">
	                        </div>
	                        <p>Do you have an existing domain?</p>
	                    </div>
	                    <div class="col-md-offset-4 col-sm-offset-3 have-domain">
							<div class="roundedTwo">
							  <input type="radio" id="radio01" name="checkdomain" value="yes" />
							  <label for="radio01"><span></span><small class="label-option">Yes</small></label>
							</div>

							<div class="roundedTwo">
							 <input type="radio" id="radio02" name="checkdomain" value="no" />
							 <label for="radio02"><span></span><small class="label-option">No</small></label>
							</div>
		                </div>
		                <div class="col-md-12 yes-domain"  style="display: none;">
							<div class="col-md-offset-4 col-sm-offset-0">
								<div class="col-md-6">
									<form method="POST" action="<?php echo base_url();?>register/add_existing_domain" id="brand_existing_domain_form" class="domain-ajax">
					                    <div class="form-group">
										  <label>Enter your existing domain.</label>
										  <input type="text" name="existing_domain_name" id="r_existing_domain_name" class="form-control">
										  <input type="hidden" name="user_id" id="r_user_id" value="<?=$user_id;?>" class="form-control">
					                    </div>
					                    <div class="form-group">
					                    	<button class="btn btn-green" id="existing_domain_btn" type="button">Check Domain</button>
					                    </div>
					                </form>
								</div>
							</div>
							<div class="col-md-12 text-center result-domain">
								<label class="error taken" style="display:none;"></label>
								<label class="active available" style="display:none;"></label>
							</div>
		                </div>
		                <div class="col-md-12 no-domain" style="display: none;">
							<div class="col-md-offset-4 col-sm-offset-1">
								<form method="GET" action="<?php echo base_url(); ?>register/domains" id="reg-domain-form" class="domain-ajax">
									<label class="col-md-6">Enter your desired custom domain.</label>
				                    <div class="form-group col-md-12">
										<div class="col-md-4 col-sm-8 mobile-bottom">
										  	<input type="text" name="domain_name" id="domain_name" class="form-control">
										  	<input type="hidden" name="domain_check" value="1"> 
										  	<input type="hidden" name="user_id" id="r_user_id" value="<?=$user_id;?>" class="form-control">
										  	<input type="hidden" name="app_type" id="app_type" value="<?=$app_type;?>" class="form-control">
										</div>
										<div class="col-md-2 col-sm-3 mobile-bottom">
										  	<select id="suffix" class="form-control" name="suffix">
					                            <option value="com">.com</option>
					                            <option value="net">.net</option>
					                            <option value="org">.org</option>
					                        </select>
										</div>
										<small class="col-md-2 col-sm-12 error"></small>
				                    </div>
				                    <div class="form-group col-md-6">
				                    	<button class="btn btn-green" id="check_domain_btn" type="button">Check Domain</button>
				                    </div>
				                </form>
							</div>
							<div class="col-md-12 text-center result-domain">
								<label class="error taken" style="display:none;"></label>
								<label class="active available" style="display:none;"></label>
								<div class="fetching-text"> 
									<span class="fetchloader"></span>
									<span>Fetching available domains. <br> <small>Please wait...</small></span>
								</div>
								<div id="domain-list">
									<p>Suggested Domains:</p>
									<small>Click on the domain you want to purchase below: </small>
									<ul class="domain-list"></ul>
								</div>
							</div>
		                </div>
						<div class="section-flow3-bottom">
		                    <div class="col-md-12 text-center">
					            <button class="pt-trigger btn btn-red btn-back" data-animation="42" data-goto="-2">Back</button>
					            <!-- <button class="pt-trigger btn btn-green" data-animation="42" data-goto="-1">Next</button> -->
					            <button class="btn btn-green" id="reg_submit_btn">Submit</button>
		                    </div>
						</div>	
						<div id="domain_purchase_modal" class="modal fade" tabindex="-1" role="dialog">
						  	<div class="modal-dialog">
							    <div class="modal-content">
							      	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal">&times;</button>
							        	<h4 class="modal-title text-center">Reserve a Domain</h4>
							      	</div>
							      	<div class="modal-body">
							        	<p style="font-size:18px; color:#000;">Click continue if you are sure this is the domain you want.</p>
							      	</div>
							      	<div class="modal-footer">
							        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						      			<a type="button" class="btn btn-primary confirm-purchase-domain" data-domain-name="" data-domain-price="">Continue</a>
							      	</div>
							    </div>
							 </div>
						</div>
	                </div>
	            </div>
	        </div>
	    </section>
    </div>
</div>
    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 
    <script src="<?= base_url()?>assets/js/register/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="<?= base_url()?>assets/js/register/fileUploader.min.js"></script>
    <script src="<?= base_url()?>assets/js/register/transition.js"></script>
    <script src="<?= base_url()?>assets/js/register/gistfile1.js"></script>
    <script src="<?= base_url()?>assets/js/dashboard/bootstrap-filestyle.js"></script>
    <script src="<?= base_url()?>assets/js/register/main.js"></script>
</body>
</html>