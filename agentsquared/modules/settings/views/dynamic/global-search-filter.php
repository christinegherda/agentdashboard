<?php

  $endpoint = "_filter=".rawurlencode("Searchable Eq true");
  $result = Modules::run('dsl/getSparkProxyComplete',
    'customfields',
    $endpoint,
    false,
    $this->session->userdata('agent_id')
  );

  $field_name = $fields[0]['id'];
  $value = isset(${$field_name})? ${$field_name} : '' ; // value of global_search_filters from database

  $field_name_string = $fields[1]['id'];
  $string_value = isset(${$field_name_string})? ${$field_name_string} : '' ; // value of global_search_filters_string from database

  if(isset($result->Success) && $result->Success){
    $i = 0;

    if($this->input->get('debug') == true){
      $hide = '';
    }else{
      $hide = 'type="hidden" style="display: none; opacity: 0; visibility: hidden;"';
    }

?>
<section id="global-search-filters">
  <!-- hidden fields here -->
  <textarea <?php echo $hide; ?> data-type="multiple-checkboxes" data-id="<?php echo $field_name; ?>" data-field style="width: 100%;" id="<?php echo $field_name; ?>"><?php echo $value;?></textarea>
  <textarea <?php echo $hide; ?> data-type="multiple-checkboxes" data-id="<?php echo $field_name_string; ?>" data-field style="width: 100%;" id="<?php echo $field_name_string; ?>"><?php echo $string_value;?></textarea>
  <!-- end of hidden fields here -->
  <div class="col-md-6 gsf" style="box-sizing: border-box; max-height: 450px; overflow-y: auto;">
    <?php

      $customFieldsCategory = array(
        'Documents',
        'Sold Info',
        '4 Bedrooms Units',
        '3 Bedrooms Units',
        '2 Bedroom Units',
        '1 Bedroom Units',
        'Seasonal Month Avlbl',
        'Pnd or UCB',
        'Contract Information',
        'Contract Info',
        'Location Tax Legal',
        'Location Legal',
        'General Property Description',
        'Other Type',
        'Currently Not Used',
        'Status Change Info'
      );

      foreach( (Array) $result->Results[0] as $key => $category){

        if(!in_array($key, $customFieldsCategory)) {
          $cat_id = str_replace(' ', '_', $key);

          $fields = (Array) $category->Fields;

          if(!empty( $fields )){
            $forSearch = $key;

            $forSearch .= '|' . implode('|', array_keys($fields));
    ?>
      <div class="panel panel-default" style="margin-bottom: 0;" data-search="<?php echo strtolower($forSearch); ?>">
        <!-- Default panel contents -->
        <div class="panel-heading" data-toggle="collapse" data-target="<?php echo $cat_id; ?>" onclick="collapse(this)">
          <?php echo $key; ?>
        </div>
        <div id="<?php echo $cat_id; ?>" class="panel-body collapse" data-id="<?php echo $cat_id; ?>">
          <?php
            foreach( $fields as $field => $value ){

              $field_id = str_replace(' ', '_', $field); // don't forget to replace it back to space on the backend aspect.

              if ($value->Searchable) {
          ?>

            <div class="col-md-6 field-item" style="display: flex;" data-search="<?php echo strtolower($field); ?>">
              <input type="checkbox" id="<?php echo $field_id; ?>" data-category="<?php echo $cat_id; ?>" data-choice="<?php echo $field_id; ?>" data-for="<?php echo $field_id; ?>" style="min-width: 18px; min-height: 18px; margin-top: 1px;" class="custom-fields-checkboxes" data-type="<?php echo $value->Type;?>"> <label data-for="<?php echo $field_id; ?>"><?php echo $field;?></label>
            </div>
          <?php
              } // end of if($value->Searchable)
            } // end foreach( $fields as $field => $value )
          ?>
        </div>
      </div>
    <?php
            $i++;
          } // end of if(!empty( $fields ))
        } // end of if(!in_array($key, $customFieldsCategory))
      } // end foreach( (Array) $result->Results[0] as $key => $category) ?>
  </div>
  <div class="col-md-6">
    <div id="search-custom-fields" style="display: flex;">
      <input type="text" onkeyup="searchFields(this, 'global-search-filters')" class="form-control" placeholder="Search Filters" /><button id="clear-custom-search" class="btn btn-default hide" onclick="clearSearch(this,'global-search-filters')">Clear</button>
    </div>
    <h3>Your Custom Filters:</h3>
    <div id="selected-custom-fields" class="selected"></div>
    <p>Maximum Filters: 10 <br>Filters left: <strong id="custom-limit-left"></strong></p>
  </div>
</section>
<!-- custom fields js scripts -->
<script type="text/javascript" defer>
  // document ready state
  (function($) {
    var $customLimitLeft = 10;
    var $customLimit = document.getElementById('custom-limit-left');

    var textarea = document.getElementById('<?php echo $field_name; ?>');
    var textarea_string = document.getElementById('<?php echo $field_name_string; ?>');

    var data = {};
    if(textarea.value !== ''){
      data = JSON.parse(textarea.value);
    }else{
      textarea.value = JSON.stringify(data);
    }

    var fields = new Items();

    fields.init({
      target: 'selected-custom-fields',
      ifEmpty: 'No filters.',
    });

    fields.render();

    $customLimit.textContent = $customLimitLeft - fields.items.length;

    /**
    |----------------------------------------
    | Add text fields for character and decimal custom fields.
    |----------------------------------------
    */
    function enhanceLi(li){

      li.showInput = false;

      if(li.data.type !== 'Boolean'){

        //if(!li.showInput){

          var input = document.createElement('input');
          input.className = 'selected-input';
          input.placeholder = 'Enter '+li.data.field;
          input.type = 'text';

          if(li.data.type === 'Decimal'){

            input.value = li.data.value;
            input.addEventListener('input', function(e){

              this.value = this.value.replace(/[^0-9\.]/g, '');
              data[li.data.category][li.data.field].value = li.data.value = this.value;
              textarea.value = JSON.stringify(data);
              textarea_string.value = updateStringCustomField(data);
              if(this.value.length === 0){
                $(this.parentElement).addClass('required');
              }else{
                $(this.parentElement).removeClass('required');
              }

            });

          }else if(li.data.type === 'Integer'){

            input.value = li.data.value;
            input.addEventListener('input', function(e){

              this.value = this.value.replace(/[^0-9]/g, '');
              data[li.data.category][li.data.field].value = li.data.value = this.value;
              textarea.value = JSON.stringify(data);
              textarea_string.value = updateStringCustomField(data);
              if(this.value.length === 0){
                $(this.parentElement).addClass('required');
              }else{
                $(this.parentElement).removeClass('required');
              }

            });

          }else{ // default to Character

            input.value = li.data.value;
            input.addEventListener('input', function(e){

              data[li.data.category][li.data.field].value = li.data.value = this.value;
              textarea.value = JSON.stringify(data);
              textarea_string.value = updateStringCustomField(data);
              if(this.value.length === 0){
                $(this.parentElement).addClass('required');
              }else{
                $(this.parentElement).removeClass('required');
              }

            });

            li.data.value = '';
          }

          li.appendChild(input);
          //li.showInput = true;
          if(input.value.length === 0){
            $(li).addClass('required');
          }
        //}
      }else{
        li.data.value = true;
      }
    }

    function removeLi(e, $data){
      var input = document.querySelector('#global-search-filters input[type=checkbox][data-category="'+$data.category+'"][data-choice="'+$data.field+'"]');

      input.checked = false;

      //data[$data.category].splice( data[$data.category].indexOf( $data.field), 1 );
      delete data[$data.category][$data.field];

      if(Object.keys(data[$data.category]).length === 0){

        var container = document.querySelector('#global-search-filters div.collapse[data-id="'+$data.category+'"]');
        $(container).removeClass('in');
        $(container.previousElementSibling).removeClass('open');

        delete data[$data.category];
      }

      textarea.value = JSON.stringify(data);
      textarea_string.value = updateStringCustomField(data);

      $customLimitLeft++;
      $customLimit.textContent = $customLimitLeft;
      $('#global-search-filters input[type="checkbox"]:not(:checked)').removeClass('lock-field');
    }

    /**
    |----------------------------------------
    | Initiate the fields and the selected fields
    |----------------------------------------
    */
    var keys = Object.keys(data);
    keys.forEach(function(category){

      var _fields = Object.keys(data[category]);
      _fields.forEach(function(field){

        if($customLimitLeft > 0){
          //console.log(data[category][field]);
          // add fields here
          fields.add(field, category, {
            load: function(li){

              // get checkbox of current field
              var input = document.querySelector('#global-search-filters input[type=checkbox][data-category="'+category+'"][data-choice="'+field+'"]');
              input.checked = true;

              var container = document.querySelector('#global-search-filters div.collapse[data-id="'+category+'"]');

              if( !$(container).hasClass('in') ){
                
                $(container).addClass('in');
                $(container.previousElementSibling).addClass('open');
              }

              li.data.type = data[category][field].type;
              li.data.value = data[category][field].value;
              enhanceLi(li);
            },
            close: function(e, $data){

              removeLi(e, $data);
            },
          });

          $customLimitLeft--;
          $customLimit.textContent = $customLimitLeft;

          if($customLimitLeft === 0){
            $('#global-search-filters input[type="checkbox"]:not(:checked)').addClass('lock-field');
          }

        }else{

          $('#global-search-filters input[type="checkbox"]:not(:checked)').addClass('lock-field');
        } // end of if($customLimitLeft > 0)

      }); //end of data[category].forEach(function(field)

    }); // end of keys.forEach(function(category)
    /**
    |----------------------------------------
    | Custom Fields Checkboxes change event
    |----------------------------------------
    */
    $('.custom-fields-checkboxes').on('change', function(e){
      if( $customLimitLeft > 0){
        var field = this.getAttribute('data-choice');
        var type = this.getAttribute('data-type');
        var category = this.getAttribute('data-category');

        if(!data.hasOwnProperty(category)){

          data[category] = {};

        }

        data[category][field] = {
          type: type,
          value: (type === 'Boolean')? true : '',
        };
        //console.log(data);

        textarea.value = JSON.stringify(data);
        textarea_string.value = updateStringCustomField(data);

        // add selected fields for every checkbox check event
        fields.add(field, category, {
          load: function(li){

            li.data.type = type;
            li.data.value = (type === 'Boolean')? true : '';

            enhanceLi(li);
          },
          close: function(e, $data){

            removeLi(e, $data);
          },
        });

        $customLimitLeft--;
        $customLimit.textContent = $customLimitLeft;

        if( $customLimitLeft === 0 ){
          $('#global-search-filters input[type="checkbox"]:not(:checked)').addClass('lock-field');
        }

      }else{
        $('#global-search-filters input[type="checkbox"]:not(:checked)').addClass('lock-field');
      }
    });

  })(jQuery);
</script>
<!-- ./ custom fields js scripts -->
<?php }else{ ?>
  There was a problem pulling your custom fields. It could be a problem with your token. Please contact support.
<?php } ?>
