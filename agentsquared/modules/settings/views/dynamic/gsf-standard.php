<?php
  $endpoint = "_filter=".rawurlencode("Searchable Eq true");

  $allowed = array('City', 'CountyOrParish', 'PropertyClass', 'PostalCode');

  $standardFields = array();

  foreach($allowed as $field){

    $result = Modules::run('dsl/getSparkProxyComplete',
      'standardfields/'.$field,
      $endpoint,
      false,
      $this->session->userdata('agent_id')
    );
    $standardFields[$field] = $result->Results[0]->{$field};

  }

  $field_name = $fields[0]['id'];
  $value = isset(${$field_name})? ${$field_name} : '' ; // value of global_search_filters from database

  $field_name_string = $fields[1]['id'];
  $string_value = isset(${$field_name_string})? ${$field_name_string} : '' ; // value of global_search_filters_string from database

  if(!empty($standardFields)){
    $i = 0;
?>
<section id="gsf-standard">
  <!-- hidden fields here -->
  <textarea type="hidden" style="display: none; opacity: 0; visibility: hidden;" data-type="multiple-checkboxes" data-id="<?php echo $field_name; ?>" data-field style="width: 100%;" id="<?php echo $field_name; ?>"><?php echo $value;?></textarea>
  <textarea type="hidden" style="display: none; opacity: 0; visibility: hidden;" data-type="multiple-checkboxes" data-id="<?php echo $field_name_string; ?>" data-field style="width: 100%;" id="<?php echo $field_name_string; ?>"><?php echo $string_value;?></textarea>
  <!-- end of hidden fields here -->
  <div class="col-md-6 gsf" style="box-sizing: border-box; max-height: 450px; overflow-y: auto;">
    <?php
    
      foreach( $standardFields as $key => $category){

        $cat_id = str_replace(' ', '_', $key);

        $fields = (Array) $category->FieldList;

        if(!empty( $fields )){
          $forSearch = $key;

          foreach( $fields as $index => $field ){
            $forSearch .= '|' . $field->Name;
          }
            
    ?>
      <div class="panel panel-default" style="margin-bottom: 0;" data-search="<?php echo strtolower($forSearch); ?>">
        <!-- Default panel contents -->
        <div class="panel-heading" data-toggle="collapse" data-target="<?php echo $cat_id; ?>" onclick="collapse(this)">
          <?php echo $category->Label; ?>
        </div>
        <div id="<?php echo $cat_id; ?>" class="panel-body collapse" data-id="<?php echo $cat_id; ?>">
          <?php
            foreach( $fields as $index => $field ){

              $field_id = str_replace(' ', '_', $field->Name); // don't forget to replace it back to space on the backend aspect.
          ?>

            <div class="col-md-6 field-item" style="display: flex;" data-search="<?php echo strtolower($field->Name); ?>">

              <input type="checkbox" id="<?php echo $field_id; ?>" data-category="<?php echo $cat_id; ?>" data-choice="<?php echo $field_id; ?>" data-for="<?php echo $field_id; ?>" style="min-width: 18px; min-height: 18px; margin-top: 1px;" class="standard-fields-checkboxes"> <label data-for="<?php echo $field_id; ?>"><?php echo $field->Value;?></label>
            </div>
          <?php
            } // end foreach( $fields as $field => $value )
          ?>
        </div>
      </div>
    <?php
          $i++;
        } // end of if(!empty( $fields )) 
      } // end foreach( (Array) $result->Results[0] as $key => $category) */
    ?>
  </div>
  <div class="col-md-6">
    <div id="search-standard-fields" style="display: flex;">
      <input type="text" onkeyup="searchFields(this, 'gsf-standard')" class="form-control" placeholder="Search Filters" /><button id="clear-standard-search" class="btn btn-default hide" onclick="clearSearch(this,'gsf-standard')">Clear</button>
    </div>
    <h3>Your Standard Filters:</h3>
    <div id="selected-standard-fields" class="selected"></div>
    <p>Maximum Filters: 10 <br>Filters left: <strong id="standard-limit-left"></strong></p>
  </div>
</section>
<!-- standard fields js scripts -->
<script type="text/javascript" defer>
  // document ready state
  (function($) {
    var $standardLimitLeft = 10;
    var $standardLimit = document.getElementById('standard-limit-left');

    var textarea = document.getElementById('<?php echo $field_name; ?>');
    var textarea_string = document.getElementById('<?php echo $field_name_string; ?>');

    var data = {};
    if(textarea.value !== ''){
      data = JSON.parse(textarea.value);
    }else{
      textarea.value = JSON.stringify(data);
    }

    var fields = new Items();

    fields.init({
      target: 'selected-standard-fields',
      ifEmpty: 'No filters.',
    });

    fields.render();

    $standardLimit.textContent = $standardLimitLeft - fields.items.length;
    /**
    |----------------------------------------
    | Initiate the fields and the selected fields
    |----------------------------------------
    */
    var keys = Object.keys(data);
    keys.forEach(function(category){

      data[category].forEach(function(field){

        if($standardLimitLeft > 0){
          // add fields here
          fields.add(field, category, {
            load: function(li){

              // get checkbox of current field
              var input = document.querySelector('#gsf-standard input[type=checkbox][data-category="'+category+'"][data-choice="'+field+'"]');
              input.checked = true;

              var container = document.querySelector('#gsf-standard div.collapse[data-id="'+category+'"]');

              if( !$(container).hasClass('in') ){
                
                $(container).addClass('in');
                $(container.previousElementSibling).addClass('open');
              }
            },
            close: function(e, $data){

              var input = document.querySelector('#gsf-standard input[type=checkbox][data-category="'+$data.category+'"][data-choice="'+$data.field+'"]');
              input.checked = false;

              data[$data.category].splice( data[$data.category].indexOf($data.field), 1 );

              if(data[$data.category].length === 0){

                var container = document.querySelector('#gsf-standard div.collapse[data-id="'+$data.category+'"]');
                $(container).removeClass('in');
                $(container.previousElementSibling).removeClass('open');

                delete data[$data.category];
              }

              textarea.value = JSON.stringify(data);
              textarea_string.value = updateStringField(data); // @rolbru, anhi ka dnhi mag change sa format. pwede raka magbuhat ug lain nga function para iparse ang required nimo nga string, kay ang `updateStringField()` kay gigamit na sa custom fields.

              $standardLimitLeft++;
              $standardLimit.textContent = $standardLimitLeft;

              $('#gsf-standard input[type="checkbox"]:not(:checked)').removeClass('lock-field');
            },
          });

          $standardLimitLeft--;
          $standardLimit.textContent = $standardLimitLeft;

          if($standardLimitLeft === 0){
            $('#gsf-standard input[type="checkbox"]:not(:checked)').addClass('lock-field');
          }
        }else{
          $('#gsf-standard input[type="checkbox"]:not(:checked)').addClass('lock-field');
        }

      });

    });
    /**
    |----------------------------------------
    | standard Fields Checkboxes change event
    |----------------------------------------
    */
    $('.standard-fields-checkboxes').on('change', function(e){
      if( $standardLimitLeft > 0){
        var field = this.getAttribute('data-choice');
        var category = this.getAttribute('data-category');

        if(!data.hasOwnProperty(category)){

          data[category] = [];

        }

        data[category].push(field);

        textarea.value = JSON.stringify(data);
        textarea_string.value = updateStringField(data);

        // add selected fields for every checkbox check event
        fields.add(field, category, {
          close: function(e, $data){
            var input = document.querySelector('#gsf-standard input[type=checkbox][data-category="'+$data.category+'"][data-choice="'+$data.field+'"]');

            input.checked = false;

            data[$data.category].splice( data[$data.category].indexOf($data.field), 1 );

            if(data[$data.category].length === 0){

              var container = document.querySelector('#gsf-standard div.collapse[data-id="'+$data.category+'"]');
              $(container).removeClass('in');
              $(container.previousElementSibling).removeClass('open');

              delete data[$data.category];
            }

            textarea.value = JSON.stringify(data);
            textarea_string.value = updateStringField(data); // @rolbru, anhi ka dnhi mag change sa format. pwede raka magbuhat ug lain nga function para iparse ang required nimo nga string, kay ang `updateStringField()` kay gigamit na sa custom fields. You can find the `updateStringField()` in settings.js.

            $standardLimitLeft++;
            $standardLimit.textContent = $standardLimitLeft;
            $('#gsf-standard input[type="checkbox"]:not(:checked)').removeClass('lock-field');
          },
        });

        $standardLimitLeft--;
        $standardLimit.textContent = $standardLimitLeft;

        if( $standardLimitLeft === 0 ){
          $('#gsf-standard input[type="checkbox"]:not(:checked)').addClass('lock-field');
        }

      }else{
        $('#gsf-standard input[type="checkbox"]:not(:checked)').addClass('lock-field');
      }
    });

  })(jQuery);
</script>
<!-- ./ standard fields js scripts -->
<?php }else{ ?>
  There was a problem pulling your standard fields. It could be a problem with your token. Please contact support.
<?php } ?>

