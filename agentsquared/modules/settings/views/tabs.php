<?php
  /**
  *   NOTE: Please do not custom codes here not unless you are creating new field type. e.g. Textarea, Checkbox, Number, Text etc.
  */
?>
<!-- tab tab-<?php echo $id;?> -->
<div id="<?php echo $id;?>" class="tab-pane <?php echo (isset($active) && $active || ((!isset($active) && $index === 0) && (!$active && $index === 0)))? 'fade in active': '';?>">
  <h2><?php echo $title;?></h2>
  <div class="content">
    <?php
      foreach($options as $option){

        if(!isset($option['hide']) || (isset($option['hide']) && !$option['hide'])){
    ?>
          <!-- <?php echo $option['class']; ?> -->
          <section class="panel panel-default <?php echo $option['class']; ?>" <?php echo (isset($option['lock']) && $option['lock'])? ' data-lock': '';?><?php echo (isset($option['soon']) && $option['soon'])? ' data-soon': '';?>>

            <?php if(isset($option['title']) && !empty($option['title'])){?>
              <div class="panel-heading">
                <h3 class="panel-title"><?php echo $option['title'];?></h3>
              </div>
            <?php }?>

            <div class="panel-body">
              <?php
                if(isset($option['custom_view'])){
                  
                  $this->load->view('dynamic/'.$option['custom_view'], $option);
                }else{
                  if(isset($option['fields'])){

                    foreach($option['fields'] as $field){

                      if(isset($option['fullwidth']) && $option['fullwidth']){
                      ?>

                        <div class="col-md-12 " field-type="<?php echo $field['type'];?>">

                      <?php 
                        }else{
                      ?>

                        <div class="col-md-6 " field-type="<?php echo $field['type'];?>">

                      <?php
                        }
                      ?>
                          <p>
                            <?php
                            // add field types here.
                            switch($field['type']){
                              case 'textarea': ?>

                                <?php if(isset($field['label']) && !empty($field['label'])){?>
                                  <label for="<?php echo $field['id'];?>"><?php echo $field['label'];?></label>
                                <?php }?>
                                <textarea class="form-control" data-type="<?php echo $field['type'];?>" data-id="<?php echo $field['id'];?>" data-field<?php echo (isset($field['placeholder']))? ' placeholder="'.$field['placeholder'].'"' : '' ; ?>><?php echo (isset(${$field['id']}) && ${$field['id']})? ${$field['id']}: '';?></textarea>
                                <?php break;

                              case 'checkbox': ?>

                                <input type="checkbox" data-type="<?php echo $field['type'];?>" data-id="<?php echo $field['id'];?>" data-field <?php echo (isset(${$field['id']}) && ${$field['id']} == 'true')? 'checked': '';?>/>
                                <?php if(isset($field['label']) && !empty($field['label'])){?>
                                  <label for="<?php echo $field['id'];?>"><?php echo $field['label'];?></label>
                                <?php }?>
                                <?php break;

                              case 'multiple-checkbox': ?>

                                <div class="col-md-12">
                                  <textarea type="hidden" style="display: none; opacity: 0; visibility: hidden;" data-type="<?php echo $field['type'];?>" data-id="<?php echo $field['id'];?>" data-field style="width: 100%;"><?php echo (isset(${$field['id']}) && ${$field['id']})? ${$field['id']} : '';?></textarea>
                                </div>
                                <?php
                                  $field_values = array();
                                  $field_keys = array();
                                  if(isset(${$field['id']}) && ${$field['id']}){

                                    $field_values = (Array) json_decode(${$field['id']});
                                    $field_keys = array_keys($field_values);
                                  }

                                  foreach($field['choices'] as $key => $label){
                                    $checked = '';
                                    if(in_array($key, $field_keys)){

                                      $checked = 'checked';

                                    } else if(isset($field['default']) && !empty($field['default'])){

                                      if(in_array($key, $field['default'])){
                                        $checked = 'checked';
                                      }
                                    }
                                ?>
                                  <div class="col-md-4">
                                    <input type="checkbox" data-choice="<?php echo $key; ?>" data-for="<?php echo $field['id'];?>" <?php echo $checked; ?>/> <label for="<?php echo $key;?>"><?php echo $label;?></label>
                                  </div>
                                <?php }?>
                                <?php break;

                              case 'text': ?>
                                <div class="inline">
                                  <?php if(isset($field['label']) && !empty($field['label'])){?>
                                    <label for="<?php echo $field['id'];?>"><?php echo $field['label'];?></label>
                                  <?php }?>
                                  <input type="text" class="form-control" data-type="<?php echo $field['type'];?>" data-id="<?php echo $field['id'];?>" data-field value="<?php echo (isset(${$field['id']}) && ${$field['id']})? ${$field['id']}: '';?>" style="max-width: 350px;">
                                </div>
                                <?php break;
                            }
                            ?>
                          </p>
                        </div>
                      <?php
                    } // end of foreach($option['fields'] as $field)
                  } // end of if(isset($option['fields']))
                  echo isset($option['info'])? $option['info'] : '';
                } // end of if(isset($option['custom_view']))
              ?>
            </div>
          </section>
          <!-- ./ <?php echo $option['class']; ?> -->
      <?php
        } // end of if(!isset($option['hide']) || (isset($option['hide']) && !$option['hide']))
      } // end of foreach($options as $option)
    ?>
  </div>
</div>
<!-- ./ tab tab-<?php echo $id;?> -->