<?php 
    $this->load->view('header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<link rel="stylesheet" type="text/css" href="/assets/css/dashboard/settings.css" />
<script type="text/javascript" src="/assets/js/dashboard/settings.js"></script>
<main class="content-wrapper">
  <h3 class="heading-title">
    <span>Site Settings</span>
  </h3>
  <div class="content">
    <ul class="nav nav-tabs">
      <?php 
        foreach($this->tabs as $index => $tab){
          $active = (isset($tab['active']) && $tab['active'])? 'class="active"' : '';
      ?>
        <li role="presentation" <?php echo $active; ?>>
          <a data-toggle="tab" href="#<?php echo $tab['id'];?>"><?php echo $tab['title'];?></a>
        </li>
      <?php }?>
    </ul>
    <div class="tab-content">
      <?php

      foreach($this->tabs as $index => $tab){
        $tab['index'] = $index;
        //$this->load->view('tabs/'.$tab['id'], $tab);
        $tab['active'] = isset($tab['active'])? true : false;

        $this->load->view('tabs', $tab);
      }
      ?>
    </div>
    <div class="controls text-right">
      <?php if($is_premium){ ?>
        <button id="update-button" class="btn btn-darkblue">Update</button>
      <?php }else{ ?>
        <span style="color: red; font-size: 24px;">Settings will be applied for <strong>Premium</strong> and <strong>Paid</strong> users only.</span> &nbsp; <button class="btn btn-darkblue" style="-webkit-filter: grayscale(1);filter: grayscale(1);opacity: 0.5;pointer-events: none; display: inline-block;vertical-align: top;">Update</button>
      <?php } ?>
    </div>
  </div>
</main>
<?php $this->load->view('footer'); ?>
<?php //$this->load->view('session/footer'); ?>