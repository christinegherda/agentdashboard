<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model{
  public $tables = array();

  public function __construct(){
    parent::__construct();

    $this->load->dbforge();
  }

  /**
  * Get the table name of the field name.
  * @param string $name The name of the field you are trying to get the table name to.
  *
  * @return mixed Returns the table name of the field. Returns false if field does not exist in any tables. Default: false
  */
  private function get_table($name = null){
    switch ($name) {  // always make sure to add new fields for every field from the controller
      case 'facebook_pixel':
      case 'gtm_head':
      case 'gtm_body':
      case 'robots':
      case 'remarketing_tag':
      case 'listtrac_page_view':
      case 'html_sitemap':
      case 'enable_cloudcma':
      case 'cloudcma_api_key':
        return 'seo';
        break;
      case 'global_search_filters':
      case 'global_search_filters_string':
      case 'gsf_standard':
      case 'gsf_standard_string':
        return 'settings';
        break;
    }
    return false;
  }

  /**
  * Gets the data depending on the arguments you passed.
  * @param string $name The name of the feature you want to get its data from.
  * @param array $args (Associative) Arguments you want to pass or the attributes of this specific query.
  * @param boolean $refresh True if you want to reset the $table property of this object. False otherwise. Default: false.
  * 
  * @return mixed Returns the value of the query. Returns false if it does not exist inside the database.
  */
  public function get($field_name = null, $args = array(), $refresh = false){
    
    if($field_name != null){
      if($refresh){
        $this->tables = array();
      }
      
      $table = $this->get_table($field_name);

      if($table){
        // creates the table if it does not exist.
        if(!$this->db->table_exists($table)){
          
          $this->create_table($table);
        }
        // checks if field exists, creates it if not.
        if(!$this->db->field_exists($field_name, $table)){
          if(isset($args['field']) && !empty($args['field'])){

            $this->create_field($table, $field_name, $args['field']);
          }else{
            $this->create_field($table, $field_name);
          }
        }
        // checks if agent_id exists
        if(isset($args['agent_id']) && !empty($args['agent_id'])){
          $this->db->where('agent_id', $args['agent_id']);
        }else{
          return false;
        }

        if(!array_key_exists($table, $this->tables)){

          $query = $this->db->get($table)->row();
          $this->db->reset_query();

          if(!empty($query)){

            $this->tables[$table] = $query;
            $value = $query->{$field_name};
            return $value;
          }
        }else{
          $this->db->reset_query();
          $query = $this->tables[$table];

          $value = $query->{$field_name};
          return $value;
        } 
      }
    }
    return false;
  } // end of get()

  /**
  * Checks if an agent exists in a specific table.
  * @param string $agent_id (Required) Agent ID.
  * @param strong $table (Required) Table name.
  */
  public function check( $agent_id = null, $table = null ){

    if($table !== null && $agent_id !== null){

      $this->db->where('agent_id', $agent_id);
      $query = $this->db->get($table)->row();
      $this->db->reset_query();

      if(!empty($query)){
        return true;
      }
    }
    return false;
  }

  /**
  * Updates or Inserts a data into a specific table for a specific feature
  * @param string $agent_id (Required) Id of the agent.
  * @param array $args (Required) (Associative) Arguments you want to pass or the attributes of this specific query..
  *
  * @return boolean Returns true if success, returns false if otherwise.
  */
  public function update($agent_id = null, $args = array()){
    if($agent_id != null){

      if(isset($args['data']) && !empty($args['data'])){
        foreach($args['data'] as $key => $value){

          $table = $this->get_table($key);

          if($table){

            if(!$this->db->field_exists($key, $table)){
              $this->dbforge->add_column($table, array(
                $key => array(
                  'type' => 'LONGTEXT',
                )
              ));
            }

            if(array_key_exists($table, $this->tables)){

              $this->tables[$table] = array_merge( $this->tables[$table], array( $key => $value ) );
            }else{
              $this->tables = array_merge( $this->tables, array( $table => array( $key => $value ) ) );
            }
          }
        } // end of foreach($args['data'] as $key => $value)

        $errors = false;
        foreach($this->tables as $name => $data){
          if($this->check( $agent_id, $name ) !== false){ // if agent already exists in $name, then use update.

            $this->db->where('agent_id', $agent_id);
            if($this->db->update($name, $data) !== true){
              $errors = true;
              break;
            }
          }else{ // else use insert
            $data['agent_id'] = $agent_id;
            if($this->db->insert($name, $data) !== true){
              $errors = true;
              break;
            }
          }
        } // end of foreach($this->tables as $name => $data)
        $this->db->reset_query();
        if(!$errors){
          return true;
        }
      }
    }
    return false;
  } // end of update()

  /**
  * Get agent plan_id
  */
  public function get_plan($user_id){

    $this->db->select('users.id, users.agent_id, users.trial, freemium.plan_id');
    $this->db->from('users');
    $this->db->where('users.id', $user_id);

    $this->db->join('freemium', 'subscriber_user_id='.$user_id, 'left');

    $query = $this->db->get()->row();

    if(!empty($query)){
      return $query;
    }
    return false;
  }

  private function create_table($table){
    $this->dbforge->add_field(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => '11',
        'auto_increment' => TRUE
      ),
      'agent_id' => array(
        'type' => 'VARCHAR',
        'constraint' => '30',
      ),
    ));
    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table($table);
  }

  private function create_field($table, $field_name, $field = array()){

    if(!$this->db->field_exists($field_name, $table)){

      if(!empty($field)){
        $this->dbforge->add_column($table, array(
          $field_name => $field
        ));
      }else{
        $this->dbforge->add_column($table, array(
          $field_name => array(
            'type' => 'LONGTEXT',
          ),
        ));
      }
    }
  }
}