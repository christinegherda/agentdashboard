<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MX_Controller {
  public $agent_id = null;
  public $is_premium = false;
  public $tabs = array();

  /**
  |--------------------------------------------
  | Settings object construct
  |--------------------------------------------
  */
  public function __construct(){
    parent::__construct();

    $this->load->model('settings_model', 'settings');

    if($this->session->userdata('user_id') === NULL){
      redirect('/dashboard');
    }

    $plan = $this->settings->get_plan( $this->session->userdata('user_id') );

    if($plan != false && empty($plan->plan_id) && $plan->trial){

      $this->is_premium = false;

    }else if($plan != false && empty($plan->plan_id) && !$plan->trial){

      $this->is_premium = true;

    }else if($plan != false && !empty($plan->plan_id) && $plan->plan_id == 2){

      $this->is_premium = true;

    }else{

      $this->is_premium = false;
    }

    enqueue_script('settings', '/assets/js/dashboard/marketing-codes.js');

    $this->set_fields();
    $this->agent_id = $this->session->userdata('agent_id');
  } // end of __construct()

  /**
  |--------------------------------------------
  | Settings page controller method
  |--------------------------------------------
  */
  public function index(){

    $this->load->model("agent_sites/Agent_sites_model");
    $this->load->model("home/home_model");

    $data = array();

    if($this->agent_id == null){
      die('You are not logged in.'); // we can replace this with redirection
    }

    $data['is_premium'] = $this->is_premium;
    $data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
    $data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
    $data['user_info'] = $this->home_model->getUsersInfo($this->session->userdata("user_id"));
    //$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");

    foreach($this->get_fields() as $field){

      $data[$field] = $this->settings->get($field, array( 'agent_id' => $this->agent_id ));
    }

    $this->load->view('main', $data);
  } // end of index()

  /**
  |--------------------------------------------
  | Get Settings page fields
  |--------------------------------------------
  */
  private function get_fields(){
    $fields = array();

    foreach($this->tabs as $tab){ // get all tabs.
      foreach($tab['options'] as $option){ // get all options in a tab.
        if(isset($option['fields'])){
          foreach($option['fields'] as $field){ // get all fields per option in a tab.
            array_push($fields, $field['id']);
          }
        }
      }
    }

    return $fields;
  } // end of get_fields()

  /**
  |--------------------------------------------
  | Settings page AJAX Requests
  |--------------------------------------------
  */
  public function update(){
    $page = $this->input->post('page');
    if ($this->input->is_ajax_request() && (isset($page) && $page == 'settings')) { // only accept ajax request. if this method is accessed directly, it will show 404.
      $data = array();

      foreach($this->input->post() as $key => $value){
        if(in_array($key, $this->get_fields())){
          $data = array_merge($data, array( $key => $value));
        }
      }

      header('Content-Type: application/json');

      if($this->settings->update($this->agent_id, array( 'data' => $data ) ) && $this->is_premium){
        print_r(json_encode(array(
          'success' => true,
          'message' => 'Site settings successfully updated.',
          'post' => $this->input->post()
        )));
      }else{
        print_r(json_encode(array(
          'success' => false,
          'message' => 'There was an issue updating the data into the database.',
          'post' => $this->input->post()
        )));
      }
    }else{
      show_404();
    }
  } // end of update()

  /**
  |--------------------------------------------
  | Only for placeholders
  |--------------------------------------------
  */
  private $placeholders = array(
    'facebook_pixel' => "<!-- Facebook Pixel Code -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod? 
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','https://connect.facebook.net/en_US/fbevents.js');

      fbq('init', 'FB_PIXEL_ID');
      fbq('track', 'PageView');
      </script>

      <noscript><img height=&#34;1&#34; width=&#34;1&#34; style=&#34;display:none&#34;
      src=&#34;https://www.facebook.com/tr?id=FB_PIXEL_ID&amp;ev=PageView&amp;noscript=1&#34;
      /></noscript>
      <!-- End Facebook Pixel Code -->",
    'gtm_head' => "<!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-XXXX');</script>
      <!-- End Google Tag Manager -->",
    'gtm_body' => "<!-- Google Tag Manager (noscript) -->
      <noscript><iframe src=&#34;https://www.googletagmanager.com/ns.html?id=GTM-XXXX&#34;
      height=&#34;0&#34; width=&#34;0&#34; style=&#34;display:none;visibility:hidden&#34;></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->",
          'remarketing_tag' => '<img height=&#34;1&#34; width=&#34;1&#34; style=&#34;border-style:none;&#34; alt=&#34;&#34; src=&#34;//googleads.g.doubleclick.net/pagead/viewthroughconversion/850051794/?guid=ON&amp;script=0&#34;/>',
  );

  /**
  |--------------------------------------------
  | Setting the fields for use later in the view. If you want to add fields on the dashboard, you will have to define it here instead.
  |--------------------------------------------
  */
  private function set_fields(){
    $this->tabs = array( // these are the tabs on this page. Auto generates the tabs and tab contents on the client side. This also auto generates table column fields in the database.
      array(
        'id' => 'tracking-codes',
        'title' => 'Tracking Codes',
        //'active' => true, // true if this is the default tab.
        'options' => array( // auto generates sections within the tab.
          array(
            'title' => 'Facebook Pixels',
            'class' => 'section-facebook',
            'fields' => array(   // auto generates the fields within each section on a tab.
              array(
                'id' => 'facebook_pixel',
                'type' => 'textarea',
                'label' => 'Enter your Facebook pixel full code here, NOT just your pixel id. This will be placed inside the <code>&lt;head&gt;</code> tag on your agent site. Please see sample placeholder code below on how it looks like.',
                'placeholder' => $this->placeholders['facebook_pixel'],
              ),
            ),
          ), // end of section-facebook
          array(
            'title' => 'Google Tag Manager', // default is in the head
            'class' => 'section-gtm',
            'fields' => array(
              array(
                'id' => 'gtm_head',
                'type' => 'textarea',
                'label' => 'Place your GTM full code designated for the <code>&lt;head&gt;</code> tag here. This will appear on your agent site inside the HTML <code>&lt;head&gt;</code> tag. Please see sample placeholder code below.',
                'placeholder' => $this->placeholders['gtm_head'],
              ),
              array(
                'id' => 'gtm_body',
                'type' => 'textarea',
                'label' => 'Place your GTM full code designated for the <code>&lt;body&gt;</code> tag here. This will appear on your agent site inside the HTML <code>&lt;body&gt;</code> tag. Please see sample placeholder code below.',
                'placeholder' => $this->placeholders['gtm_body'],
              ),
            ),
          ), // end of section-gtm
          array(
            'title' => 'Google Remarketing Tag',
            'class' => 'section-remarketing',
            'fields' => array(
              array(
                'id' => 'remarketing_tag',
                'type' => 'textarea',
                'label' => 'Place your Google Remarketing Tag full code here, not just the ID. This will appear inside the HTML <code>&lt;body&gt;</code> tag on your website.',
                'placeholder' => $this->placeholders['remarketing_tag'],
              ),
            ),
          ),
        ), // end of options
      ), // end of tracking-codes
      array(
        'id' => 'sitemap',
        'title' => 'Sitemap',
        'options' => array(
          array(
            'title' => 'HTML Sitemap',
            'class' => 'section-html-sitemap',
            'fields' => array(
              array(
                'id' => 'html_sitemap',
                'type' => 'multiple-checkbox',
                'label' => 'Select what links will be available on your HTML sitemap.',
                'choices' => array(
                  'main_pages' => 'Main Pages',
                  'custom_menu' => 'Custom Pages',
                  'active' => 'Active Listings',
                  'office' => 'Office Listings',
                  'sold' => 'Sold Listings',
                  'new' => 'New Listings',
                ),
                'default' => array('main_pages', 'custom_menu', 'active', 'office', 'sold', 'new'),
                'fullwidth' => true,
              ),
            ),
          ),
        ),
      ), // end of sitemap
      array(
        'id' => 'cloudcma',
        'title' => 'Cloud CMA',
        'options' => array(
          array(
            'title' => 'Enable Cloud CMA',
            'class' => 'section-cloudcma',
            'info' => '<p>Please <a href="https://cloudcma.com/real-estate-agents" target="_blank">click here</a> to login to your CloudCMA account.</p>',
            'fields' => array(
              array(
                'id' => 'enable_cloudcma',
                'type' => 'checkbox',
                'label' => 'Replace seller form with CloudCMA form on the Seller page.',
              ),
              array(
                'id' => 'cloudcma_api_key',
                'type' => 'text',
                'label' => 'Enter your Cloud CMA API Key here.',
              ),
            ),
            'fullwidth' => true,
          ),
          array(
            'title' => 'Where to get my CloudCMA API Key?',
            'class' => 'section-cloudcma-info',
            'info'  => '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oqkac4OegJI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',
          ),
        ),
      ), // end of cloudcma
      array(
        'id' => 'listtrac',
        'title' => 'Listtrac',
        'options' => array(
          array(
            'title' => 'Event Settings',
            'class' => 'section-listtrac',
            'fields' => array(
              array(
                'id' => 'listtrac_page_view',
                'type' => 'checkbox',
                'label' => 'Page View'
              ),
            ),
            //'hide' => true
            'fullwidth' => true,
            'lock' => true,
            'soon' => true
          ),
        ),
      ), // end of listtrac
      array(
        'id' => 'global_search_results',
        'title' => 'Global Search Filter',
        'active' => true,
        'options' => array(
          array(
            'title' => 'MLS Standard Fields',
            'class' => 'section-standard-fields',
            'custom_view' => 'gsf-standard',
            'fields' => array(
              array(
                'id' => 'gsf_standard',
                'label' => 'Filter your listings on your agent site to your preferred custom fields.',
                'dynamic' => true,
              ),
              array(
                'id' => 'gsf_standard_string',
                'label' => 'Filters stringified for agent site direct usage.',
                'dynamic' => true,
              ),
            ),
          ),
          array(
            'title' => 'MLS Custom Fields',
            'class' => 'section-custom-fields',
            'custom_view' => 'global-search-filter',
            'fields' => array(
              array(
                'id' => 'global_search_filters',
                'label' => 'Filter your listings on your agent site to your preferred custom fields.',
                'dynamic' => true,
              ),
              array(
                'id' => 'global_search_filters_string',
                'label' => 'Filters stringified for agent site direct usage.',
                'dynamic' => true,
              ),
            ),
          ),
        ),
      ), // end of global_search_results
    ); // end of $tabs
  }
}
