<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sso extends MX_Controller {
    public function __construct(){
        parent::__construct();
    }
    public function index() {
        /*
            {
                "code":"74acaff1c2963a5d036e71dc218f3491c3f00dbe256761f72c625bc020eb5b3c",
                "id_token":"eyJhbGciOiJSUzI1NiIsImtpZCI6IkUzOEMwRDRBMzgwNkI1NTMxQTRFN0Y3MTNDQ0ZBMjI2NEE3N0FCNDEiLCJ0eXAiOiJKV1QiLCJ4NXQiOiI0NHdOU2pnR3RWTWFUbjl4UE0taUprcDNxMEUifQ.eyJuYmYiOjE1MjgxNzU2MTgsImV4cCI6MTUyODE3NTkxOCwiaXNzIjoiaHR0cHM6Ly9pZHAuYXJtbHMuY29tIiwiYXVkIjoiS1JTQzFUWG9nUFltZFRLNlRDNU1WTmVvR21TYW56SXp1cmp1QUd2dCIsIm5vbmNlIjoiMTIzNDU2Nzg5IiwiaWF0IjoxNTI4MTc1NjE4LCJjX2hhc2giOiI2Y2RXYkVIbXhFdl9UUUk2OWhIT293Iiwic2lkIjoiNWFkNGE4YjlmNjQ5ZGFkZmZjZjIxODkzYWM4MzA3OTUiLCJzdWIiOiJBRDQ2MTdBRC00MDI0LTREQjgtQkEyMi03NjQzM0JCMzNFMEQiLCJhdXRoX3RpbWUiOjE1MjgxNzU2MTcsImlkcCI6ImxvY2FsIiwiYW1yIjpbImV4dGVybmFsIl19.XjcJ4TAMX1FjSBa5LdipORsKKL0Q3Ys82wZMUabjSpZC254j5hzEDSdejpFj9ztVc026mNVXwCwBdAGT6iV-G2XTz07C6XxtQ751GagB_5_nk1bo7IWc1JmuxmFYDkKIabHnXiFZ16Y3vZNM028cRcu0yjLaligt_8q0mUy0kg5LPgry2Pg234_5vPtYfChDQQaXy5khrQYlTpKwc8VWsZumTTQDXJLdt0qqasFWd6Djoi03KKVlmpOxJ4OD-QWMn2r0dESNs3h1voDK5a_f9SMHuBRlZ_QugkEce068BlVykua-iWJlc1ERqHCRsyaqEt-kPhFd1EqUTl-4XIa1cQ",
                "scope":"openid",
                "state":"agentsquared_test_sso123",
                "session_state":"sSgnJomjSkbyK3fCMyzb1cpvG527RzutYTaV9fTNAj8.f9a43f22c2d9c23e08acdbcd6fbf6cce"\
            }
        */
        $data = $this->input->post();
        /*
            Endpoint: https://idp.armls.com/connect/token
            Method: POST
            Parameters: 
                grant_type: authorization_code
                code: <CODE RETURNED FROM CALLBACK>
                redirect_uri:http://dashboard.agentsquared.com/callback/sso
                client_id: <client_id PROVIDED BY DP>
                client_secret: <client_secret PROVIDED BY DP>
            Headers:
                Content-Type:application/x-www-form-urlencoded
        */        
        $url = 'https://idp.armls.com/connect/token';
        
        if(isset($data['code']) && $data['code']) { // Safe switch
            $headers = ['Content-Type: application/x-www-form-urlencoded'];
            $body  = array(
                'grant_type'    => 'authorization_code',
                'code'          => $data['code'],
                'redirect_uri'  => 'http://dashboard.agentsquared.com/callback/sso',
                'client_id'     => 'KRSC1TXogPYmdTK6TC5MVNeoGmSanzIzurjuAGvt',
                'client_secret' => '3z6NJX90tyqdhmbTJTA49GiRBpsSFUos6LD4WgJ32E9P0CmUpRkaY0TvXOCM2vkDFzzoeHDYSdxbdeyw'
            );
            
            $ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
            $result_info = json_decode($result, true);

            $validate = $this->_id_token_validate();

            $config = $this->_fetch_openid_config();

            echo "<pre>";
            echo var_dump($result_info);
            echo var_dump($validate);
            echo var_dump($config);
            echo "</pre>";

            
        }
        else {
            // Do nothing, or tell APP there is no Activity from OpenID Provider (OP)
        }
        echo json_encode($data);
    }

    private function _id_token_validate($id_token = NULL, $jwt_url = NULL) { /* Safe Switch Parameter Initialize */
        
        $return = false; // Initialize return false as default
        if($id_token && $jwt_url) {
            $id_token_array = explode('.', $id_token); // index 0 = jose header, 1 = JWT payload, id_token signature
            $decoded_id_token = array(
                'jose_headers'  => base64_decode($id_token_array[0]),
                'payload'       => base64_decode($id_token_array[1]),
                'id_token_sig'  => $id_token_array[2]
            );

            $return = $decoded_id_token;
        }

        return $return;
    }

    private function _fetch_openid_config($config_url = NULL) {
        // https://idp.armls.com/.well-known/openid-configuration
        $return = false;

        if($config_url) {
            $config_json = file_get_contents($config_url);
            $config_object = json_decode($config_json);

            if($config_object->jwks_uri) {
                $jwt_config_json = file_get_contents($extra_config_dept->jwks_uri);
                $jwt_config_object = json_decode($jwt_config_json);

                $return['jwt_config'] = $jwt_config_object;
            }
            else {
                $return = $config_object;
            }
            
        }
        return $return;
    }

}