<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_social_media_model extends CI_Model{

	function __construct(){

		parent:: __construct();
	}

	public function fetch_id() {

		$agent_id = $this->session->userdata('agent_id');
		$array = array('agent_id' => $agent_id);
		$this->db->where($array);
		$query = $this->db->get('users')->row();

		$data[] = array(
			'id' => $query->id
		);
		return $data;
	}

    /**
     * @return mixed
     */
	public function user_id()
    {
        $agent_id = $this->session->userdata('agent_id');

        return $this->db->where(['agent_id' => $agent_id])->limit(1)->get('users', ['id', 'agent_id'])->row()->id;
    }

    /**
     * @param array $data
     * @return bool|mixed
     */
    public function save_link(array $data = [])
    {
        if ( empty($data) )
        {
            return false;
        }

        $data['user_id'] = $this->user_id();

        if ( $result = $this->fetch_link('id') )
        {
            return $this->update_link($result['id'], $data);
        }

        return $this->insert_link($data);
    }

	public function fetch_link($fields = null)
    {
        if ( null === $fields )
        {
            $fields = ['facebook', 'twitter', 'linkedin', 'googleplus', 'instagram'];
        }

        if ( !is_array($fields) )
        {
            $fields = (array)$fields;
        }

        $query = $this->db
            ->where(['user_id' => $this->user_id()])
            ->limit(1)
            ->get('social_media_links', $fields);

        if ( $query )
        {
            return $query->row_array();
        }

        return false;
	}

	public function insert_link($data)
    {
        $data['date_modified'] = date('Y-m-d');
        $data['date_created']  = date('Y-m-d');

		return $this->db->insert('social_media_links', $data);
	}

	public function update_link($id, $data)
    {
        $data['date_modified'] = date('Y-m-d');

		return $this->db->where(['user_id' => $data['user_id'], 'id' => $id])->update('social_media_links', $data);
	}

	public function get_property_activate_data( $property_id = NULL)
	{
		$this->db->select("ListingID,address,broker,broker_number,unparsedaddr,primary_photo,site_url,price_previous,price,description,city,state,directions")
			->from("property_activated")
			->where("ID", $property_id)
			->limit(1);

		return $this->db->get()->row();
	}

	public function get_property_activate_data_and_link( $property_id = NULL)
	{
		$this->db->select("ListingID,address,broker,broker_number,unparsedaddr,primary_photo,site_url,price_previous,price,description,city,state,directions, (SELECT domains FROM domains d LEFT JOIN property_activated p ON d.user_name = p.ListingID WHERE d.user_name LIMIT 1) as domain")
			->from("property_activated")
			->where("property_activated.ID", $property_id)
			->limit(1);

		return $this->db->get()->row();
	}

	public function get_property_listing_json( $listingId = NULL)
	{
		$this->db->select("json_photos")
			->from("property_listing_data")
			->where("property_listing_data.ListingID", $listingId)
			->limit(1);

		return $this->db->get()->row();
	}

	/**************************************************************************
	 *                              FACEBOOK MODEL                             *
	 ***************************************************************************/

	public function create_facebook($user){

		// insert new facebook account
		$data = array(
			'id'   => '',
			'user_id' => $_SESSION['user_id'],
			'name' => $user['name'],
			'email' => $user['email'],
			'social_id' => $user['id'],
			'social_type' => 'facebook',
			'facebook_type' => 'user',
            'profile_picture'=> "https://graph.facebook.com/{$user['id']}/picture",
			'app_token' => $this->session->userdata('fb_access_token'),
			'social_media_account' => 'http://facebook.com/'.$user['id']
		);

		$this->db->insert('social_media', $data);
	}

    /**
     * @param $user
     * @return mixed
     */
    public function saveFacebookUser($user)
    {
        $data = [
            'user_id'        => $this->session->userdata('user_id'),
            'name'           => $user['name'],
            'email'          => $user['email'],
            'social_id'      => $user['id'],
            'app_token'      => $user['accessToken'],
            'social_type'    => 'facebook',
            'facebook_type'  => 'user',
            'post_value'     => 1,
            'profile_picture'=> "https://graph.facebook.com/{$user['id']}/picture",
            'social_media_account' => 'https://facebook.com/'.$user['id']
        ];

        return $this->db->insert('social_media', $data);
    }

    /**
     * @param $accounts
     * @return mixed
     */
    public function saveFacebookAccounts($accounts)
    {
        if ( empty($accounts) )
        {
            return false;
        }

        foreach ($accounts as $account)
        {
            $data = [
                'user_id'          => $this->session->userdata('user_id'),
                'name'             => $account['name'],
                'social_id'        => $account['id'],
                'app_token'        => $account['access_token'],
                'profile_picture'  => "https://graph.facebook.com/{$account['id']}/picture",
                'social_type'      => 'facebook',
                'facebook_type'    => 'page',
                'post_value'       => 1,
                'social_media_account' => 'http://facebook.com/'.$account['id']
            ];

            $this->db->insert('social_media', $data);
        }
    }

	public function create_facebook_page($p){

		// insert new facebook account
		$data = array(
			'id'   => '',
			'user_id' => $_SESSION['user_id'],
			'name' => $p['name'],
			'social_id' => $p['id'],
			'social_type' => 'facebook',
			'facebook_type' => 'page',
			'app_token' => $p['access_token'],
			'profile_picture' => "https://graph.facebook.com/{$p['id']}/picture",
			'social_media_account' => 'http://facebook.com/'.$p['id']
		);

		$this->db->insert('social_media', $data);
	}

	public function delete_facebook_account() {
		$this->db->delete('social_media', array('user_id' => $_SESSION['user_id'], 'social_type' => 'facebook', 'is_active' => 'yes'));
	}

	public function check_facebook_account() {

		$this->db->select("*")
			->from("social_media")
			->where("social_type","facebook")
			->where("is_active","yes")
			->where("user_id", $_SESSION['user_id']);

		$query = $this->db->get();

		if( $query->num_rows() > 0 )
		{
			return $query->result();
		}

		return FALSE;
	}

	public function is_facebook_post() {

		$this->db->select("*")
			->from("social_media")
			->where("social_type","facebook")
			->where("post_value","1")
			->where("user_id", $_SESSION['user_id']);

		$query = $this->db->get();

		if( $query->num_rows() > 0 )
		{
			return $query->result();
		}

		return FALSE;
	}

	public function fetch_facebook_data()
	{
		$where = [
			'user_id' => $_SESSION['user_id'],
			'social_type' => 'facebook',
			'is_active' => 'yes',
			'facebook_type' => 'user'
		];

		return $this->db->where($where)->limit(1)->get('social_media')->row();
	}

	public function get_facebook_options($fields = '*', $type = '', $post_value = null)
	{
		$this->db->select($fields)
			->from("social_media")
			->where("social_type","facebook")
			->where("user_id", $_SESSION['user_id']);

		if ( '' !== $type )
		{
			$this->db->where("facebook_type", $type);
		}

		if ( $post_value )
		{
			$this->db->where("post_value", 1);
		}

		$query = $this->db->get();

		if( $query->num_rows() > 0 )
		{
			return $query->result();
		}

		return FALSE;
	}

	public function update_is_selected(){

		$save["post_value"] = ( $this->input->post("is_selected") == 0 ) ? 1 : 0;

		$this->db->where("id", $this->input->post("facebook_id") );
		$this->db->update("social_media", $save );

		return ( $this->db->affected_rows() ) ? TRUE : FALSE;
	}

	/********************************************************************************
	 *                                 TWITTER MODEL                                 *
	 *********************************************************************************/

	public function create_twitter($arr) {

		$data = array(
			'id' => '',
			'user_id' => $_SESSION['user_id'],
			'name' => $arr['name'],
			'email' => $arr['screen_name'],
			'social_id' => $arr['id'],
			'social_type' => 'twitter',
			'app_token' => $arr['access_token'],
			'token_secret' => $arr['token_secret'],
			'created' => date('Y-m-d'),
			'social_media_account' => 'https://twitter.com/'.$arr['screen_name'],
			'profile_picture' => $arr['profile_image_url'],
			'is_active' => 'yes'
		);

		$this->db->insert('social_media', $data);
	}

	public function update_twitter($arr) {

		$data = array(
			'app_token' => $arr['access_token'],
			'token_secret' => $arr['token_secret']
		);

		$this->db->update('social_media', $data);
	}

	public function delete_twitter_user() {
		$this->db->delete('social_media', array('user_id' => $_SESSION['user_id'], 'social_type' => 'twitter'));
	}

	public function check_twitter_user() {

		$array = array('user_id' => $_SESSION['user_id'], 'social_type' => 'twitter', 'is_active' => 'yes');
		$this->db->where($array);

		return $this->db->get('social_media')->row();

	}

	public function fetch_twitter_data() {

		$query = $this->db->select('id, name, email, social_id, social_type, app_token, token_secret, created, social_media_account, is_active')
			->from('social_media')
			->where(array(
				'user_id'     => $this->session->userdata('user_id'),
				'social_type' => 'twitter',
				'is_active'   => 'yes'
			))
			->limit(1)
			->get()->row();

		return (!empty($query)) ? $query : FALSE;

	}

	public function get_twitter_options(){

		$this->db->select("profile_picture")
			->from("social_media")
			->where("social_type","twitter")
			->where("user_id", $_SESSION['user_id']);

		$query = $this->db->get();

		if( $query->num_rows() > 0 )
		{
			return $query->result();
		}

		return FALSE;
	}

	/****************************************************************************
	 *                              GOOGLEPLUS MODEL                             *
	 ***************************************************************************/
	public function create_googleplus() {

	}

	public function update_googleplus() {

	}

	public function delete_googleplus_user() {
		$this->db->delete('social_media', array('property_username' => $_SESSION['username'], 'social_type' => 'googleplus'));
	}

	public function check_googleplus_user() {
		$array = array('property_username' => $_SESSION['username'], 'social_type' => 'googleplus', 'is_active' => 'yes');
		$this->db->where($array);
		return $this->db->get('social_media')->row();
	}

	public function fetch_googleplus_data() {
		$array = array('property_username' => $_SESSION['username'], 'social_type' => 'googleplus', 'is_active' => 'yes');
		$this->db->where($array);

		$query = $this->db->get('social_media')->row();

		if(isset($query)) {
			$data[] = array(
				'id' => $query->id,
				'property_username' => $query->property_username,
				'name' => $query->name,
				'email' => $query->email,
				'social_id' => $query->social_id,
				'social_type' => $query->social_type,
				'app_token' => $query->app_token,
				'created' => $query->created,
				'social_media_account' => $query->social_media_account,
				'is_active' => $query->is_active
			);

			return $data;
		} else

			return false;

	}

	/****************************************************************************
	 *                              LINKEDIN MODEL                               *
	 *****************************************************************************/

	public function create_linkedin($arr) {

		$data = array(
			'id' => '',
			'user_id' => $_SESSION['user_id'],
			'name' => $arr['name'],
			'email' => $arr['name'],
			'social_id' => $arr['id'],
			'social_type' => 'linkedin',
			'app_token' => $arr['app_token'],
			'token_secret' => $arr['token_secret'],
			'expires_at' => $arr['expires_at'],
			'created' => date('Y-m-d'),
			'social_media_account' => $arr['social_media_account'],
			'profile_picture' => $arr['profile_picture'],
			'is_active' => 'yes'
		);

		$this->db->insert('social_media', $data);
	}

	public function fetch_linkedin_data() {

		$array = array('user_id' => $_SESSION['user_id'], 'social_type' => 'linkedin', 'is_active' => 'yes');
		$this->db->where($array);

		$query = $this->db->get('social_media')->row();

		if(isset($query))
		{
			$data[] = array(
				'id' => $query->id,
				'user_id' => $query->user_id,
				'name' => $query->name,
				'email' => $query->email,
				'social_id' => $query->social_id,
				'social_type' => $query->social_type,
				'linkedin_token' => $query->app_token,
				'linkedin_token_secret' => $query->token_secret,
				'created' => $query->created,
				'social_media_account' => $query->social_media_account,
				'is_active' => $query->is_active,
                'expires_at' => $query->expires_at
			);

			return $data;
		}

		return false;
	}

	public function check_linkedin_user() {

		$array = array('user_id' => $_SESSION['user_id'], 'social_type' => 'linkedin', 'is_active' => 'yes');
		$this->db->where($array);

		return $this->db->get('social_media')->row();
	}

	public function delete_linkedin_user() {
		$this->db->delete('social_media', array('user_id' => $_SESSION['user_id'], 'social_type' => 'linkedin'));
	}

	public function get_linkedin_options(){

		$this->db->select("*")
			->from("social_media")
			->where("social_type","linkedin")
			->where("user_id", $_SESSION['user_id']);

		$query = $this->db->get();

		if( $query->num_rows() > 0 )
		{
			return $query->result();
		}

		return FALSE;
	}

	public function get_your_properties_listings($counter = FALSE, $datas = array() )
	{
		$this->db->select("*")
			->from("property_activated")
			->where('list_agentId', $datas["mlsID"]);

		return $this->db->get()->result();
	}

	public function get_your_active_listings()
	{

		$status = array('Active', 'Contingent','Pending');

		$this->db->select("*")
			->from("property_listing_data")
			->where('user_id', $this->session->userdata('user_id'));
		$this->db->where_in('property_status', $status);

		return $this->db->get()->result();
	}

	public function get_website_type(){

		$this->db->select("*")
			->from("agent_subscription")
			->where('UserId', $this->session->userdata('agent_id'));

		return $this->db->get()->result();
	}
}
