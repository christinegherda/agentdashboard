<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span>Social Media Links</span>
    </h3>
    <p> Add your social media links so your clients can connect with you.</p>
  </div>
  <style type="text/css">
    .delete-link {
        line-height: 2.2em;
    }
    form .input-group {
        width: 100%;
    }
    span.input-group-addon {
        text-align: right;
        width: 200px;
    }
  </style>
  <!-- Main content -->
  <section class="content">
        <?php $this->load->view('session/launch-view'); ?>
                <div class="row">
                    <div class="featured-list">
                        <div class="col-md-12">
                            <h2></h2>
                        </div>
                    </div>
                    <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Social Media Links
                  <span class="social-media-note">(add your social media links here)</span></h3>
                </div>
                <!-- /.box-header -->
                  <div class="box-body">
                  <span class="display-status alert alert-success" style="display:none;"></span>
                  <span class="display-error alert alert-danger" style="display:none;"></span>
                    <div class="social-media-form form-horizontal">

                        <div class="form-group facebook-url-form">
                            <form method="post" class="social-link-forms" action="<?php echo site_url('social-media-add-link'); ?>">
                                <input type="hidden" name="link" value="facebook">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1">
                                        <label for="facebook_url" class="col-sm-12 control-label"><i class="fa fa-facebook" data-original-ico="fa-facebook"> </i></label>
                                    </div>
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="input-group has-feedback">
                                            <span class="input-group-addon">https://www.facebook.com/</span>
                                            <input type="text" class="form-control social-link-input facebook-link" name="facebook_uri" value="<?php echo $facebook; ?>" data-link="facebook" maxlength="100" placeholder="my-facebook-profile-url" aria-describedby="">
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span id="facebook-url-input-error-status" class="sr-only">(error)</span>
                                            <span id="facebook-url-input-success-status" class="text-success sr-only">(success)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 col-xs-1">
                                        <a href="<?= site_url('agent_social_media/delete_link/?link=facebook'); ?>" data-link="facebook" title="delete link" class="delete-link text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group twitter-url-form">
                            <form method="post" class="social-link-forms" action="<?php echo site_url('social-media-add-link'); ?>">
                                <input type="hidden" name="link" value="twitter">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1">
                                        <label for="twitter_url" class="col-sm-12 control-label"><i class="fa fa-twitter" data-original-ico="fa-twitter"> </i></label>
                                    </div>
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="input-group has-feedback">
                                            <span class="input-group-addon">https://twitter.com/</span>
                                            <input type="text" class="form-control social-link-input twitter-link" name="twitter_uri" value="<?php echo $twitter; ?>" data-link="twitter" maxlength="100" placeholder="my-twitter-profile-url" aria-describedby="">
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span id="twitter-url-input-error-status" class="sr-only">(error)</span>
                                            <span id="twitter-url-input-success-status" class="text-success sr-only">(success)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 col-xs-1">
                                        <a href="<?= site_url('agent_social_media/delete_link/?link=twitter'); ?>" data-link="twitter" title="delete link" class="delete-link text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="form-group linkedin-url-form">
                            <form method="post" class="social-link-forms" action="<?php echo site_url('social-media-add-link'); ?>">
                                <input type="hidden" name="link" value="linkedin">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1">
                                        <label for="linkedin_url" class="col-sm-12 control-label"><i class="fa fa-linkedin" data-original-ico="fa-linkedin"> </i></label>
                                    </div>
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="input-group has-feedback">
                                            <span class="input-group-addon">https://www.linkedin.com/</span>
                                            <input type="text" class="form-control social-link-input linkedin-link" name="linkedin_uri" value="<?php echo $linkedin; ?>" data-link="linkedin" maxlength="100" placeholder="my-linkedin-profile-url" aria-describedby="">
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span id="linkedin-url-input-error-status" class="sr-only">(error)</span>
                                            <span id="linkedin-url-input-success-status" class="text-success sr-only">(success)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 col-xs-1">
                                        <a href="<?= site_url('agent_social_media/delete_link/?link=linkedin'); ?>" data-link="linkedin" title="delete link" class="delete-link text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="form-group instagram-url-form">
                            <form method="post" class="social-link-forms" action="<?php echo site_url('social-media-add-link'); ?>">
                                <input type="hidden" name="link" value="instagram">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1">
                                        <label for="instagram_url" class="col-sm-12 control-label" title="Instagram"><i class="fa fa-instagram" data-original-ico="fa-instagram"></i></label>
                                    </div>
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="input-group has-feedback">
                                            <span class="input-group-addon">https://www.instagram.com/</span>
                                            <input type="text" class="form-control social-link-input instagram-link" name="instagram_uri" value="<?php echo $instagram; ?>" data-link="instagram" maxlength="100" placeholder="my-instagram-profile-url" aria-describedby="">
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span id="instagram-url-input-error-status" class="sr-only">(error)</span>
                                            <span id="instagram-url-input-success-status" class="text-success sr-only">(success)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 col-xs-1">
                                        <a href="<?= site_url('agent_social_media/delete_link/?link=instagram'); ?>" data-link="instagram" title="delete link" class="delete-link text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->

                    </div>
                </div>
    </section>
</div>

<?php $this->load->view('session/footer'); ?>
