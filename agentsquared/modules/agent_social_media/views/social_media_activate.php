<?php
$this->load->view('session/header');
$this->load->view('session/top-nav'); ?>

<div class="content-wrapper">
	<div class="page-title">
		<div class="col-md-7 col-sm-6">
			<h3 class="heading-title">
				<span>Activate Social Media</span>
			</h3>
		</div>
		<div class="col-md-5 col-sm-6 col-md-3 pull-right">
			<div class="otherpage video-box video-collapse">
				<div class="row">
					<div class="col-md-5 col-sm-5 col-xs-5">
						<h1>Need Help?</h1>
						<p>See how by watching this short video.</p>
						<div class="checkbox">
							<input type="checkbox" id="hide" name="hide" value="hide">
							<label for="hide">Minimize this tip.</label>
						</div>
					</div>
					<div class="col-md-1">
						<div class="text-center">
							<span class="fa fa-long-arrow-right"></span>
						</div>
					</div>
					<div class="col-md-7 col-sm-7 col-xs-7">
						<div class="tour-thumbnail tour-video">
							<video poster="<?php echo base_url()."assets/images/dashboard/v_socialmedia.png"; ?>">
								<source id="videoTour" src="<?php echo base_url()."assets/video/activate-social-media.mp4"?>" type="video/mp4">
							</video>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="content">
		<?php $this->load->view('session/launch-view'); ?>
		<div id="cover-spin"></div>
		<div class="row">
			<div class="featured-list">
				<div class="col-md-12">
					<h2></h2>
				</div>
			</div>
			<div class="col-md-6 social-media-activate">
				<h4>Activate Social Media </h4>
				<div class="panel-group" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab">
							<h4 class="panel-title">
								<i class="fa fa-facebook"></i>

								<?php /** @var bool $fb */
								if(!$fb) { ?>
									<a class="social-title" data-toggle="collapse" data-parent="#accordion" href="#collapseFacebook">Facebook
										<span class="activated pull-right inactive"> &nbsp;&nbsp;Inactive</span>
										<i class="fa fa-exclamation-triangle pull-right inactive"></i>
									</a>
									<?php
								} else { ?>
									<a class="social-title" data-toggle="collapse" data-parent="#accordion" href="#collapseFacebook">Facebook
										<span class="activated pull-right active"> &nbsp;&nbsp;Active</span>
										<i class="fa fa-check-circle pull-right active"></i>
									</a>
									<?php
								}
								?>
							</h4>
						</div>
						<div class="panel-collapse collapse moreDetails" role="tabpanel">
							<div class="panel-body">
								<div class="col-md-12 col-xs-12 col-sm-12"> <?php
								/** @var bool $fb */
								if(!$fb) { ?>
									<p class="info-text">
										<i class="fa fa-info-circle text-primary" aria-hidden="true"></i>
										To connect your Facebook, select activate and sign into your account.
									</p><?php

									if($this->config->item('disallowUser')) { ?>
									<a href="<?=base_url().'agent_social_media/agent_social_media/activate?modal_premium=true'?>">
										<p class="btn btn-save btn-success"> Activate</p>
									</a><?php
									}
									else { ?>
                                        <a href="javascript:void(0)" onclick ="fb_login();" ><p class="btn btn-save btn-success"> Activate</p></a><?php
									}

								}
								else { ?>
									<a href="<?php echo site_url('agent_social_media/deactivate_facebook');?>" onclick="return $('#cover-spin').show();" class="btn btn-danger btn-cancel">
										Deactivate
									</a> <?php
								}

								if(isset($facebook_options) && !empty($facebook_options)) { ?>
									<div class="facebook-pages-option">
									<h5>Select Facebook Pages to post on facebook</h5>
									<?php foreach($facebook_options as $account)
									{
										$fb_profile_photo = base_url('assets/images/fb-default-photo.jpg');

										if ( $account->facebook_type === 'user' )
										{
											$fb_profile_photo = 'https://graph.facebook.com/'.$account->social_id.'/picture';
										}
										elseif ( $account->facebook_type === 'page' )
										{
											$fb_profile_photo = 'https://graph.facebook.com/'.$account->social_id.'/picture?access_token='.$account->app_token;
										}?>
										<div class="checkbox">
										<label>
											<input type="checkbox" id="is_page_selected_<?=$account->id;?>" data-id="<?=$account->id;?>" class="is_selected_post" name="is_selected[]" value="<?php echo $account->post_value?>" <?php echo ((int)$account->post_value === 1) ? 'checked="checked"': ""; ?> >
											<input type="hidden" value="<?php echo $account->social_id?>">
											<span class="fb-photo">
                                                <img title="<?= $account->name ?>" width="30" src="<?php echo $fb_profile_photo ?>">
                                            </span>
											<span style="position: relative;left: -14px;top: 7px;" class="fb-icon">
                                                <img title="<?= $account->name ?>" width="15px" src="<?php echo base_url('assets/images/fb-icon.png')?>">
                                            </span>
											<span class="facebook-page-name"><?php echo $account->name ?></span>
										</label>
										</div><?php
									}?>
									</div><?php // END facebook-pages-option
								} ?>
								</div>
								<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="well">
										<h4>About Facebook</h4>
										<p>Millions of people use Facebook everyday to keep up with friends,
											upload an unlimited number of photos, share links and videos, and learn more about the people they meet.
										</p>
										<p><b>Don't have an account on Facebook?</b> Click <a href="https://www.facebook.com/" target="_blank">here</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab">
							<h4 class="panel-title">
								<i class="fa fa-twitter"></i> <a class="social-title" data-toggle="collapse" data-parent="#accordion" href="#collapseTwitter">Twitter

									<?php if(!$tw) { ?>

									<span class="activated pull-right inactive"> &nbsp;&nbsp;Inactive</span><i class="fa fa-exclamation-triangle pull-right inactive"></i></a>

								<?php } else { ?>

									<span class="activated pull-right active"> &nbsp;&nbsp;Active</span><i class="fa fa-check-circle pull-right active"></i></a>

								<?php } ?>
							</h4>
						</div>
						<div class="panel-collapse collapse" role="tabpanel">
							<div class="panel-body">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> To connect your Twitter, select activate and sign into your account.</p>
									<?php
									if(!$tw) {
										if($this->config->item('disallowUser')) { ?>
											<a href="<?=base_url().'agent_social_media/agent_social_media/activate?modal_premium=true'?>">
												<p class="btn btn-save btn-success"> Activate</p>
											</a>
											<?php
										} else { ?>
											<a href="javascript:void(0)" onclick ="twitt_login();">
												<p class="btn btn-save btn-success"> Activate</p>
											</a>
											<?php
										}
									} else { ?>
										<a href="<?php echo site_url('agent_social_media/deactivate_twitter');?>" class="btn btn-danger btn-cancel">Deactivate</a>
										<?php
									}
									?>
								</div>
								<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="well">
										<h4>About Twitter</h4>
										<p>Twitter is a real-time information network that connects you to the latest information about what you find interesting. Simply find the public streams you find most compelling and follow the conversations.
										</p>

										<p><b>Don't have an account on Twitter?</b> Click <a href="https://twitter.com/" target="_blank">here</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab">
							<h4 class="panel-title"><i class="fa fa-linkedin"></i> <a href = "javascript:void(0)" class="collapsed" >LinkedIn</a>

								<?php if(!$ln) { ?>

									<span class="activated pull-right inactive"> &nbsp;&nbsp;Inactive</span><i class="fa fa-exclamation-triangle pull-right inactive"></i></a>

								<?php } else { ?>

									<span class="activated pull-right active"> &nbsp;&nbsp;Active</span><i class="fa fa-check-circle pull-right active"></i></a>

								<?php } ?>
							</h4>
						</div>
						<div class="panel-collapse collapse" role="tabpanel">
							<div class="panel-body">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<p class="info-text"><i class="fa fa-info-circle text-primary" aria-hidden="true"></i> To connect your Linkedin, select activate and sign into your account.</p>
									<?php
									if(!$ln) {
										if($this->config->item('disallowUser')) { ?>
											<a href="<?=base_url().'agent_social_media/agent_social_media/activate?modal_premium=true'?>">
												<p class="btn btn-save btn-success"> Activate</p>
											</a>
											<?php
										} else { ?>
											<a href="javascript:void(0)" onclick ="linkdn_login();"><p class="btn btn-save btn-success"> Activate</p></a>
										<?php   }
									} else { ?>
										<a href="<?php echo site_url('agent_social_media/deactivate_linkedin');?>" class="btn btn-danger btn-cancel">Deactivate</a>
									<?php } ?>
								</div>
								<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="well">
										<h4>About LinkedIn</h4>
										<p>LinkedIn is a social networking site where alumni,graduates and other professionals connect online.
										</p>
										<p><b>Special Notes</b><br>
											LinkedIn has posting limits of 25 a day.
										</p>
										<p><b>Don't have an account on LinkedIn</b> Click <a href="https://www.linkedin.com/" target="_blank">here</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 social-postarea">
				<h4>Post to your Social Media accounts</h4>
				<div class="select-post">
					<p>Post will distribute to: </p>
					<ul class="list-inline"><?php // Show Facebook Profile and Page Photo
						if ($fb && !empty($facebook_options))
						{
							$fbAccount = '';
							foreach($facebook_options as $account)
							{
								if ( (int)$account->post_value === 1)
								{
									if ( '' === $fbAccount )
									{
										$fbAccount = $account;
									}

									$fb_profile_photo = base_url('assets/images/fb-default-photo.jpg');

									if ( $account->facebook_type === 'user' )
									{
										$fb_profile_photo = 'https://graph.facebook.com/'.$account->social_id.'/picture';
									}
									elseif ( '' !== $account->profile_picture )
									{
										$fb_profile_photo = $account->profile_picture;
									}?>
									<li>
                                        <span class="fb-photo">
                                            <img title="<?= $account->name ?>" width="30" src="<?php echo $fb_profile_photo ?>">
                                        </span>
										<span style="position: relative;left: -14px;top: 7px;" class="fb-icon">
                                            <img width="15px" src="<?php echo base_url('assets/images/fb-icon.png')?>">
                                        </span>
									</li> <?php
								}
							}
						}?>

						<!-- Show Twitter Profile Photo -->
						<?php
						if ($tw){

							if(isset($twitter_options)  && !empty($twitter_options)){

								$tw_profile_photo = $twitter_options[0]->profile_picture;
								$tw_default_photo = base_url('assets/images/fb-default-photo.jpg');

								?>

								<li><span class="tw-photo"><img width="30" src="<?php echo isset($tw_profile_photo) ? $tw_profile_photo : $tw_default_photo ;?>"></span><span style="position: relative;left: -14px;top: 7px;" class="tw-icon"><img width="15px" src="<?php echo base_url()?>/assets/images/twitter-icon.png"></span></li>

								<?php

							}
						} ?>

						<!-- Show Linkedin Profile Photo -->
						<?php
						if ($ln){

							if(isset($linkedin_options)  && !empty($linkedin_options)){

								$ln_profile_photo = $linkedin_options[0]->profile_picture;
								$ln_default_photo = base_url('assets/images/fb-default-photo.jpg');

								?>

								<li><span class="ln-photo"><img width="30" src="<?php echo isset($ln_profile_photo) ? $ln_profile_photo : $ln_default_photo ;?>"></span><span style="position: relative;left: -14px;top: 7px;" class="ln-icon"><img width="15px" src="<?php echo base_url()?>/assets/images/linkedin-icon.png"></span></li>

							<?php   }

						} ?>
					</ul>
				</div>
				<div id="alert-div" class="display-alert alert alert-flash">
					<?php echo $this->session->flashdata('msg'); ?>
				</div> <?php /** @var bool $fb_token_expired */
				if($fb_token_expired) { ?>
					<div class="alert alert-expire alert-danger fade in col-md-12" role="alert">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						Your Facebook token has expired. Please activate to post to your social media account!
					</div><?php
				} else if(!$ln) { ?>
					<div class="display-alert col-md-12">
						<?php echo $this->session->flashdata('msg-expire-linkedin'); ?>
					</div>
				<?php } ?>

				<p>Choose the property to feature</p>
				<div class="social-post">
					<form method="POST">
						<select name="property_selected" id="propertySelected"  class="form-control property-activated">
							<option value="">Select Property</option><?php
							//Custom Domain Option for Social media Posting
							if(isset($domain_info[0]->subdomain_url) || !empty($is_reserved_domain['domain_name']))
							{
								if($domain_info[0]->status === "completed" || (int)$domain_info[0]->is_subdomain === 1)
								{
									$base = !empty($is_reserved_domain['domain_name']) ? "http://" : "" ;
									$base_url = $base.$is_reserved_domain['domain_name'];
								}
								elseif($domain_info[0]->status !== "completed" && (int)$domain_info[0]->is_subdomain === 0)
								{
									$base_url = $domain_info[0]->subdomain_url;
								}
							}

							// SPW Created sites option for Social Media Posting
							if(!empty($spw_properties))
							{
								foreach($spw_properties as $listing)
								{ ?> <option
									data-address="<?= $listing['address'] ?>"
									data-pri-photo="<?= $listing['primary_photo'] ?>"
									data-url="<?= $listing['site_url'] ?>"
									data-type="spw_property"
									value="<?= $listing['id'] ?>"> SPW - <?= $listing['address']; ?> - spw</option><?php
								}
							}

							// Active Property option for Social Media Posting
							if(isset($website_type[0]->ApplicationType) && $website_type[0]->ApplicationType === 'Agent_IDX_Website'){
								//active listings
								if(!empty($active_listings)){
									foreach($active_listings as $listing){?> 
										<option
										data-address="<?= $listing['address'] ?>"
										data-pri-photo="<?= $listing['primary_photo'] ?>"
										data-url="<?= $listing['site_url'] ?>"
										data-type="active_property"
										value="<?= $listing['listing_key'] ?>"> Active - <?= "{$listing['address']} - {$listing['mls_status']}"; ?></option><?php
									}
								}

								//office listings
								if(!empty($office_listings)){
									foreach($office_listings as $listing){?> 
										<option
										data-address="<?= $listing['address'] ?>"
										data-pri-photo="<?= $listing['primary_photo'] ?>"
										data-url="<?= $listing['site_url'] ?>"
										data-type="office_property"
										value="<?= $listing['listing_key'] ?>">Office - <?= "{$listing['address']} - {$listing['mls_status']}"; ?></option><?php
									}
								}

								//new listings
								if(!empty($new_listings)){
									foreach($new_listings as $listing){?> 
										<option
										data-address="<?= $listing['address'] ?>"
										data-pri-photo="<?= $listing['primary_photo'] ?>"
										data-url="<?= $listing['site_url'] ?>"
										data-type="new_property"
										value="<?= $listing['listing_key'] ?>">New - <?= "{$listing['address']} - {$listing['mls_status']}"; ?></option><?php
									}
								}

								//nearby listings
								if(!empty($nearby_listings)){
									foreach($nearby_listings as $listing){?> 
										<option
										data-address="<?= $listing['address'] ?>"
										data-pri-photo="<?= $listing['primary_photo'] ?>"
										data-url="<?= $listing['site_url'] ?>"
										data-type="nearby_property"
										value="<?= $listing['listing_key'] ?>">Nearby - <?= "{$listing['address']} - {$listing['mls_status']}"; ?></option><?php
									}
								}
							}?>
						</select><br>
						<p id="propertySelectedNotFound" class="alert alert-warning fade col-md-12" style="display: none;" role="alert"></p>
					</form>
				</div>

				<form action="<?php echo base_url()?>agent_social_media/social_post" method="POST" id="socialPost" enctype="mutlipart/form-data" accept-charset="utf-8">
					<?php /* <input type="hidden" name="post_facebook" value="1" id="post_facebook"> */ ?>
					<input type="hidden" name="post_twitter" value="1" id="post_twitter">
					<input type="hidden" name="post_linkedin" value="1" id="post_linkedin">

					<div class="form-group">
						<label for="prop-photo">Property Title</label>
						<input type="text" name="property_title" class="form-control" id="prop-title">
					</div>

					<div class="form-group">
						<label for="prop-link">Property Link</label>
						<input type="text" name="property_link" class="form-control" id="prop-link">
					</div>

					<div class="form-group">
						<label for="prop-photo">Property Photo</label>
						<input type="text" name="property_photo" class="form-control" id="prop-photo">
					</div>

					<div class="form-group">
						<label for="prop-desc">Property Description</label>
						<textarea name="your_post" cols="25" rows="4" id="post-area" maxlength="110" class="form-control" placeholder="Checkout my new listings at <address>!"></textarea>
					</div>

					<div id="myModal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<p class="preview-text-post"></p>
								</div>
								<div class="modal-body">
									<div id='preview-post'>
										<!-- <div class="text-center"><i class="fetch-active-loading fa fa-circle-o-notch fa-spin fa-5x"></i></div> -->
										<div id='preview-img'></div>
										<div id='preview-text'>
											<p class="property-address"></p>
											<p class="property-description"></p>
											<p class="property-broker"></p>
											<p style="color:#5C97BF;" class="property-url"></p>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<p>
										<button type="button" class="btn btn-reset btn-cancel btn-warning" data-dismiss="modal">Cancel</button>
										<button class='btn btn-reset btn-save-modal btn-success' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..."><span class="spinner"><i class="icon-spin icon-refresh"></i></span>Post</button>
									</p>
								</div>
							</div>
						</div>
					</div>
				</form>

				<div class="form_button"><?php
				if ( false !== ($ln || $tw) )
				{ ?>
					<button id="btn-preview-post" class='btn btn-preview btn-info' data-toggle="modal" data-target="#myModal" disabled><?php
					if ( !$ln && $tw)
					{ ?>
						<i class="fa fa-twitter"></i> Post to Twitter <?php
					}
					elseif ( !$tw && $ln )
					{ ?>
						<i class="fa fa-linkedin"></i> Post to LinkedIn<?php
					}
					else
					{ ?>
						Post to <i class="fa fa-twitter"></i> Twitter and/or <i class="fa fa-linkedin"></i> LinkedIn<?php
					}?>
					</button><?php
				}

				// show share button when the enabled posting
				if ( $fb && '' !== $fbAccount ) { ?>
				<button data-social-id="<?= $fbAccount->social_id ?>" data-user-type="<?= $fbAccount->facebook_type ?>" type="button" id="fbshare-prop-post" class="btn btn-primary" title="Share to facebook" disabled="disabled">
					<i class="fa fa-facebook"></i>&nbsp;Post to Facebook
				</button><?php
				} ?>
				</div>

			</div>
		</div>
	</section>
</div>

<?php $this->load->view('session/footer');?>
