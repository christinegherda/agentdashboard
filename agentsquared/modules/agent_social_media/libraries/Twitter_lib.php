<?php

/*
 * Karl John Honrado, 2018
 *
 * Encapsulates TwitterOAuth library by Abraham Williams - https://github.com/abraham/twitteroauth
 *
 *
*/

defined('BASEPATH') OR exit('No direct script access allowed');

use Abraham\TwitterOAuth\TwitterOAuth;

class Twitter_lib extends MX_Controller {

    public $connection = null;
    public $tw_config = null;

    public function __construct() {

        $this->load->model('agent_social_media_model', 'agent_sm_model');
		$this->config->load('twitter', true);
		$this->tw_config = $this->config->item('twitter');

      	$this->connection = new TwitterOAuth($this->tw_config['consumer_key'], $this->tw_config['consumer_secret']);
    }

    public function request_token() {

      	$request_token = $this->connection->oauth('oauth/request_token', array('oauth_callback' => $this->tw_config['oauth_callback']));

 		if(isset($request_token['oauth_token']) && isset($request_token['oauth_token_secret'])) {
 			$this->session->set_userdata('tw_oauth_token', $request_token['oauth_token']);
 			$this->session->set_userdata('tw_oauth_token_secret', $request_token['oauth_token_secret']);
            return $request_token;
        }

    }

    public function authorize($oauth_token=NULL) {
    	if(!empty($oauth_token)) {
			$url = $this->connection->url('oauth/authorize', array('oauth_token'=>$oauth_token));
			header('Location:'.$url);
    	}
    }

    public function process_request() {

        $oauth_token = $this->session->userdata('tw_oauth_token');
        $oauth_token_secret = $this->session->userdata('tw_oauth_token_secret');

        $this->connection = new TwitterOAuth($this->tw_config['consumer_key'], $this->tw_config['consumer_secret'], $oauth_token, $oauth_token_secret);

        $access_token = $this->connection->oauth('oauth/access_token', ['oauth_verifier'=>$this->input->get('oauth_verifier')]);

        if(!empty($access_token)) {

            $this->connection = new TwitterOAuth($this->tw_config['consumer_key'], $this->tw_config['consumer_secret'], $access_token['oauth_token'], $access_token['oauth_token_secret']);
            $user_profile = $this->connection->get("account/verify_credentials");

            $this->agent_sm_model->create_twitter(array(
                'id'                => $user_profile->id,
                'name'              => $user_profile->name,
                'screen_name'       => $user_profile->screen_name,
                'location'          => $user_profile->location,
                'description'       => $user_profile->description,
                'profile_image_url' => $user_profile->profile_image_url,
                'access_token'      => $access_token['oauth_token'],
                'token_secret'      => $access_token['oauth_token_secret']
            ));

            $this->session->set_userdata('twitter_login', TRUE);

            return TRUE;
        }   

        return FALSE;

    }

    public function post($status=NULL) { //post to twitter with plain text only
		
        if(!empty($status)) {

            $tw_user = $this->agent_sm_model->fetch_twitter_data();

            if(!empty($tw_user)) {

                $access_token   = $tw_user->app_token;
                $refresh_token  = $tw_user->token_secret;

                $this->connection = new TwitterOAuth($this->tw_config['consumer_key'], $this->tw_config['consumer_secret'], $access_token, $refresh_token);
                $this->connection->post('statuses/update', ['status' => $status]);

                return TRUE;

            }

        }
        
        return FALSE;

    }

    public function post_media($status=NULL, $img_url=NULL) { //post status with image attached

        $tw_user = $this->agent_sm_model->fetch_twitter_data();

        if(!empty($tw_user) && !empty($status)) {

            $access_token   = $tw_user->app_token;
            $refresh_token  = $tw_user->token_secret;

            $this->connection = new TwitterOAuth($this->tw_config['consumer_key'], $this->tw_config['consumer_secret'], $access_token, $refresh_token);

            $parameters = ['status' => $status];

            if( !empty($img_url) )
            {
                $result = $this->connection->upload('media/upload', ['media' => $img_url]);

                if ( $result )
                {
                    $parameters['media_ids'] = $result->media_id_string;
                }
            }

            $this->connection->post('statuses/update', $parameters);

            return TRUE;

        }

        return FALSE;
    }

    public function clear_session_data() {
        $this->session->unset_userdata('twitter_login');
        $this->session->unset_userdata('tw_oauth_token');
        $this->session->unset_userdata('tw_oauth_token_secret');
    }
}
