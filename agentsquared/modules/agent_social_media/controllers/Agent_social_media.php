<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_social_media extends MX_Controller {

    /** @var array $data */
	public $data;

	/** @var String $img_url */
	public $img_url;

    public function __construct()
    {
		parent::__construct();

		$userData = $this->session->userdata('user_id');

		if(empty($userData))
		{
			$this->session->set_flashdata('flash_data', 'You don\'t have access!');
			redirect('login/auth/login', 'refresh');
		}

		$this->load->library('facebook');
		$this->load->library('twitter_lib');
		$this->load->helper('url');
		$this->load->model("agent_social_media_model","agent_sm_model");
		$this->load->model("agent_sites/Agent_sites_model");
		$this->load->model("dsl/dsl_model");
		$this->load->model("mysql_properties/properties_model");

		// load linkedin config
        $this->config->load('linkedin', true);

        add_jsvar('FACEBOOK_APP_ID', config_item('facebook_app_id'), 'string', true);
        add_jsvar('FACEBOOK_APP_VERSION', config_item('facebook_graph_version'), 'string', true);
        add_jsvar('FACEBOOK_APP_PERM_SCOPES', implode(',', config_item('facebook_permissions')), 'string', true);
	}

	public function index()
    {
		$this->activate();
	}

	public function activate() {

		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["facebook_options"] = $this->agent_sm_model->get_facebook_options('id, app_token, facebook_type, name, social_id, profile_picture, post_value');
		$data["twitter_options"] = $this->agent_sm_model->get_twitter_options();
		$data["linkedin_options"] = $this->agent_sm_model->get_linkedin_options();
		$data['fb_token_expired'] = false;

		//printA($data["facebook_options"] );exit;
		/**************************************
		 *         FACEBOOK                  *
		 **************************************/
		$data['fb'] = false;

		//check if facebook user exist
		if( $this->facebook_account_exist() ) {
			$this->session->set_userdata('facebook_login', true);
			$data['fb'] = true;

			//check if facebook token is over 60 days
			$fetch_fbCreated = $this->agent_sm_model->fetch_facebook_data();

			try
            {
                $created = new \DateTime($fetch_fbCreated ? $fetch_fbCreated->created : 'now');
                $interval = $created->diff(new \DateTime());
                $date_interval = (int)$interval->format('%a');

                if($date_interval > 59 )
                {
                    $this->agent_sm_model->delete_facebook_account();
                    $this->facebook->destroy_session();
                    $this->session->unset_userdata('facebook_login');
                    $data['fb_token_expired'] = true;
                }
            }
            catch ( \Exception $exception )
            {}

			if ( count($data['facebook_options']) > 0 )
			{
				$account = array_filter($data['facebook_options'], function ($user) {
					return $user->facebook_type === 'user';
				});

				if ( count($account) > 0 )
				{
					$this->config->set_item('user_facebook', $account[0]);
				}
			}
		}

		/**************************************
		 *               TWITTER               *
		 **************************************/
		//check if twitter user exist
		if($this->twitter_user_exist()) {
			$this->session->set_userdata('twitter_login', true);
			$data['tw'] = true;
		} else {
			$data['tw'] = false;
		}

		/**************************************
		 *               LINKEDIN              *
		 **************************************/

		//check if linkedin user exist
		$data['ln'] = false;
		if($this->linkedin_user_exist())
		{
			$this->session->set_userdata('linkedin_logged_in', true);
			$data['ln'] = true;

			//check if facebook token is over 60 days
			$fetch_lnCreated = $this->agent_sm_model->fetch_linkedin_data();
			$newdate = strtotime ( '+59 day' , strtotime ($fetch_lnCreated[0]['created']));
			$newdate = date ('Y-m-j' , $newdate);
			$lntime1 = new DateTime($fetch_lnCreated[0]['created']);
			$lntime2 = new DateTime($newdate);
			$interval = $lntime1->diff($lntime2);
			$date_interval = $interval->format('%a');

			if($date_interval  > 59)
			{
				$this->session->unset_userdata('linkedin_logged_in');
				$this->session->unset_userdata('oauth_request_token');
				$this->session->unset_userdata('oauth_request_token_secret');
				$this->session->unset_userdata('oauth_verifier');
				$this->session->unset_userdata('auth');
				$this->agent_sm_model->delete_linkedin_user();
				$this->session->set_flashdata('msg-expire-linkedin', '<div class="alert alert-expire alert-danger text-center"><a href="#" class="text-danger close" data-dismiss="alert" aria-label="close">&times;</a>Your Linkedin token has expired. Please activate to post to your socialmedia account!</div>');
			}
		}

		$data['account_info']    = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		$data['spw_properties']  = $this->getSpwProperties($data['domain_info'], $data['account_info']->Id);
		$data['active_listings'] = $this->getActiveListings($data['domain_info']);
		$data['office_listings'] = $this->getListings('office', 'office/listings', $data['domain_info']);
		$data['new_listings'] = $this->getListings('new', 'listings', $data['domain_info']);
		$data['nearby_listings'] = $this->getListings('nearby', 'listings', $data['domain_info']);
		//printA($data['nearby_listings'] );exit;
		
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];

		$data['website_type'] = $this->agent_sm_model->get_website_type();

		$data['title'] = "Agent Social Media Activate";

		$this->load->view('social_media_activate', $data);
	}

	/**************************************************************************
	 *                              FACEBOOK CONTROLLER                        *
	 ***************************************************************************/

	public function facebook_login()
	{
		/** @var $this->facebook = Libraries\Facebook */
		if ($this->session->userdata('facebook_login') !== true)
		{
			// Check if user is logged in
			if ( null !== $this->facebook->is_authenticated() )
			{
				// User logged in, get user details
				$user = $this->facebook->request('get', '/me?fields=id,name,email');
				$pages = $this->facebook->request('get', '/me/accounts?fields=id,name,access_token,picture');

				if (!isset($user['error']))
				{
					$this->agent_sm_model->create_facebook($user);

					$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success text-center">Facebook account successfully activated!</div>');
				}

				if (!isset($pages['error']))
				{
					if ( isset($pages['data']) && count($pages['data']) > 0 )
					{
						foreach($pages['data'] as $page)
						{
							if ( count($page) > 0 )
							{
								$this->agent_sm_model->create_facebook_page($page);
							}
						}
					}

					$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success text-center">Facebook account successfully activated!</div>');
				}

				$this->session->set_userdata('facebook_login', true);
			}

			redirect('agent_social_media?post_socialmedia=1');
		}
	}

	public function deactivate_facebook(){
		$this->facebook->destroy_session();
		$this->session->unset_userdata('facebook_login');
		$this->agent_sm_model->delete_facebook_account();
		$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger text-center">Facebook account successfully deactivated!</div>');
		redirect('agent_social_media/activate');
	}

	public function facebook_account_exist(){
		$checkaccount = $this->agent_sm_model->check_facebook_account();
		//printA($checkaccount);exit;

		if(empty($checkaccount)) {
			return false;
		}
		return true;
	}

	/*************************************************************************
	 *                              TWITTER CONTROLLER                        *
	 **************************************************************************/

	public function twitter_redirect() {
		$request_token = $this->twitter_lib->request_token();
		$this->twitter_lib->authorize($request_token['oauth_token']);
	}

	public function twitter_callback() {

		$flag = $this->twitter_lib->process_request();

		if($flag) {
			redirect('agent_social_media?post_socialmedia=1');
		}

	}

	public function deactivate_twitter() {
		$this->twitter_lib->clear_session_data();
		$delete = $this->agent_sm_model->delete_twitter_user();
		$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger text-center">Twitter account successfully deactivated!</div>');
		redirect('agent_social_media/activate');
	}

	public function twitter_user_exist() {
		$checkuser = $this->agent_sm_model->check_twitter_user();
		if(empty($checkuser)) {
			return false;
		}
		return true;
	}

	/*************************************************************************
	 *                              LINKEDIN CONTROLLER                        *
	 **************************************************************************/

    /**
     * @return void
     */
	public function linkedin_login()
    {
        $linkedConfig = $this->config->item('linkedin');
        $client = new LinkedIn\Client($linkedConfig['api_key'], $linkedConfig['secret_key']);
        $scopes = [
            LinkedIn\Scope::READ_LITE_PROFILE,
            LinkedIn\Scope::READ_EMAIL_ADDRESS,
            LinkedIn\Scope::SHARING,
        ];

        // get url on LinkedIn to start linking
        $redirectUrl = site_url($linkedConfig['redirect_url']);
        $loginUrl = $client->setRedirectUrl($redirectUrl)->getLoginUrl($scopes);

        $this->session->set_userdata([
            'state'        => $client->getState(),       // save state for future validation
            'redirect_url' => $client->getRedirectUrl(), // save redirect url for future validation
        ]);

        if ( $this->input->is_ajax_request())
        {
            $this
                ->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(\GuzzleHttp\json_encode(['data' => compact('loginUrl')]))
                ->_display();
            exit;
        }

        redirect($loginUrl);
	}

	/**
	 *  Get Access Token
	 */
	public function linkedin_submit()
    {
        if ( !$this->input->get('code') )
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-warning">LinkedIn account activation has been cancelled!</div>');

            redirect('agent_social_media/activate?post_socialmedia=1');
        }

        if ( false === ($this->input->get('state') && $this->session->userdata('state') && ($this->session->userdata('state') === $this->input->get('state'))) )
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger">LinkedIn account activation code is invalid!</div>');
            redirect('agent_social_media/activate?post_socialmedia=1');
        }

        $linkedConfig = $this->config->item('linkedin');
        $client = new LinkedIn\Client($linkedConfig['api_key'], $linkedConfig['secret_key']);

        try
        {
            // you have to set initially used redirect url to be able
            // to retrieve access token
            $client->setRedirectUrl($this->session->userdata('redirect_url'));

            // retrieve access token using code provided by LinkedIn
            $accessToken = $client->getAccessToken($this->input->get('code'));

            // perform api call to get profile information
            $profile = $client->get('me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))');

            $firstName  = $profile['firstName']['localized']["{$profile['firstName']['preferredLocale']['language']}_{$profile['firstName']['preferredLocale']['country']}"];
            $lastName   = $profile['lastName']['localized']["{$profile['lastName']['preferredLocale']['language']}_{$profile['lastName']['preferredLocale']['country']}"];
            $pictureUrl = $profile['profilePicture']['displayImage~']['elements'][0]['identifiers'][0]['identifier'];

            $data = [
                'id'              => $profile['id'],
                'name'            => "$firstName $lastName",
                'app_token'       => $accessToken ? $accessToken->getToken() : '',
                'expires_at'      => $accessToken ? $accessToken->getExpiresIn() : '',
                'token_secret'    => '',
                'social_media_account' => '',
                'profile_picture' => $pictureUrl
            ];

            $this->agent_sm_model->create_linkedin($data);

            $this->session->set_userdata('linkedin_logged_in', true);
        }
        catch ( LinkedIn\Exception $exception )
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger">'.$exception->getMessage().'</div>');
            log_message('error','ERROR: '.$exception->getMessage());
        }
        log_message('error','Testing');
        redirect('agent_social_media/activate?post_socialmedia=1');
	}

	/**
	 *  Delete user's linkedin in the database
	 */
	public function deactivate_linkedin()
    {
		$this->session->unset_userdata('linkedin_logged_in');
		$this->session->unset_userdata('oauth_request_token');
		$this->session->unset_userdata('oauth_request_token_secret');
		$this->session->unset_userdata('oauth_verifier');
		$this->session->unset_userdata('auth');

		$this->agent_sm_model->delete_linkedin_user();

		$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success">LinkedIn account successfully deactivated!</div>');

		redirect('agent_social_media/activate');
	}

    /**
     * Check if linkedin user exist
     * @return bool
     */
	public function linkedin_user_exist()
    {
		$checkuser = $this->agent_sm_model->check_linkedin_user();
        if(empty($checkuser)){
            log_message('error','Count: '.count($checkuser));
        }
		return !empty($checkuser);
	}

	public function social_post()
    {
        $linkedin_post_success = false;

		if(isset($_POST['post_facebook']) && (int)$_POST['post_facebook'] === 1)
		{
			$response = [
				'success' => false
			];

			if( $this->session->userdata('facebook_login') !== false )
			{
				$facebook_pages = $this->agent_sm_model->get_facebook_options('app_token, social_id', 'page', true);

				if( $facebook_pages && !empty($facebook_pages) )
				{
					//scrape facebook url before sharing to facebook
					Modules::run('dashboard/facebook_url_scraper', $this->input->post('property_link'));

					$data = array(
						'message' => strip_tags($this->input->post('message')),
						'link' => $this->input->post('property_link')
					);

					foreach($facebook_pages as $page)
					{
						if ( (int)$_POST['source']['socialId'] === (int)$page->social_id )
						{
							continue;
						}

						$this->facebook->request('post', "/{$page->social_id}/feed", $data, $page->app_token);
					}

					$response['success'] = true;
				}
			}

			if ( $this->input->is_ajax_request() )
			{
				echo json_encode($response);
				exit;
			}
		}

		//check if twitter is selected
		if(isset($_POST['post_twitter']) && $_POST['post_twitter'] == 1) {

			//publish post to twitter account
			if($this->session->userdata('twitter_login') == true) {
				$my_post = strip_tags($this->input->post('your_post')). ' '. $this->input->post('property_link');
				$img_url = $this->input->post('property_photo');
				$this->twitter_lib->post_media($my_post, $img_url);
			}
		}

		// check if linkedin is selected and publish post to linkedin account
		if( $this->input->post('post_linkedin')  && (int)$this->input->post('post_linkedin') === 1 && $this->session->userdata('linkedin_logged_in') === true )
		{
		    if ( $linkedin = $this->agent_sm_model->fetch_linkedin_data() )
            {
                $linkedin = array_shift($linkedin);
            }

            $linkedConfig = $this->config->item('linkedin');

            try
            {
                $client = new LinkedIn\Client($linkedConfig['api_key'], $linkedConfig['secret_key']);
                $client->setAccessToken(new LinkedIn\AccessToken($linkedin['linkedin_token'], $linkedin['expires_at']));

                // Should upload media to linkedin is required? $request->post('submitted_image_url')
                $data = [
                    'author' => "urn:li:person:{$linkedin['social_id']}",
                    'lifecycleState' => 'PUBLISHED',
                    'specificContent' => [
                        'com.linkedin.ugc.ShareContent' => [
                            // Share Comment
                            'shareCommentary' => ['text' => $this->input->post('your_post')],
                            'shareMediaCategory' => 'ARTICLE',
                            'media' => [
                                [
                                    'status' => 'READY',

                                    'description' => ['text' => $this->input->post('your_post')],

                                    // Share article page URL
                                    'originalUrl' => $this->input->post('property_link'),

                                    // Share title
                                    'title' => ['text' => $this->input->post('property_title')]
                                ]
                            ]
                        ],
                    ],
                    'visibility' => [
                        'com.linkedin.ugc.MemberNetworkVisibility' => 'PUBLIC'
                    ]
                ];

                $client->post('ugcPosts', $data);
                $linkedin_post_success = true;
            }
            catch ( LinkedIn\Exception $exception)
            {
                // $exception->getMessage();
                $linkedin_post_success = false;
            }
		}

		$is_facebook_selected = $this->agent_sm_model->is_facebook_post();
		$fb_selected = false;

		if(isset($is_facebook_selected) && !empty($is_facebook_selected))
		{
			$fb_selected = true;
		}

		if($fb_selected === true || $this->session->userdata('twitter_login') === true || $linkedin_post_success === true)
			$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success text-center">Post to Social Media Success!</div>');

		else
			$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger text-center">Please activate at least 1 Social Media!</div>');

		redirect('agent_social_media/activate');
	}

	public function is_page_selected(){

		if( $this->input->is_ajax_request()){

			if( $this->input->post("facebook_id") == "" || empty($this->input->post("facebook_id")) )
			{
				$response = array( "success" => FALSE );
				return json_encode($response);
			}

			$val = ( $this->input->post("is_selected") == 0 ) ? 1 : 0;

			if( $this->agent_sm_model->update_is_selected() )
			{
				$response = array( "success" => TRUE , "val" => $val);
			}
			else
			{
				$response = array( "success" => FALSE, "val" => $val);
			}

			exit(json_encode($response));

		}
	}

	public function links() {

		$fetch_link = $this->agent_sm_model->fetch_link();
        $data = [
            'facebook' => '',
            'twitter'  => '',
            'linkedin' => '',
            'googleplus' => '',
            'instagram' => ''
        ];

		if($fetch_link)
		{
            $data['facebook']   = $this->get_link_uri('facebook.com', $fetch_link['facebook']);
            $data['twitter']    = $this->get_link_uri('twitter.com', $fetch_link['twitter']);
            $data['linkedin']   = $this->get_link_uri('linkedin.com', $fetch_link['linkedin']);
            $data['googleplus'] = $this->get_link_uri('plus.google.com', $fetch_link['googleplus']);
            $data['instagram']  = $this->get_link_uri('instagram.com', $fetch_link['instagram']);
		}

		$data['title'] = "Agent Social Media Links";
		$data['agent_email'] = $this->session->userdata('email');
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");

		$this->load->view('social_media_links', $data);

	}

	public function linksInfo() {

		$fetch_link = $this->agent_sm_model->fetch_link();
        $data = [
            'facebook' => '',
            'twitter'  => '',
            'linkedin' => '',
            'googleplus' => '',
            'instagram' => ''
        ];

		if ($fetch_link)
		{
			$data['facebook']   = $this->get_link_url('facebook.com', $fetch_link['facebook']);
			$data['twitter']    = $this->get_link_url('twitter.com', $fetch_link['twitter']);
			$data['linkedin']   = $this->get_link_url('linkedin.com', $fetch_link['linkedin']);
			$data['googleplus'] = $this->get_link_url('plus.google.com', $fetch_link['googleplus']);
            $data['instagram']  = $this->get_link_url('instagram.com', $fetch_link['instagram']);
		}

		$data['title'] = "Agent Social Media Links";
		$data['agent_email'] = $this->session->userdata('email');
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];

		return $data;
	}

	public function add_link()
    {
        $link = $this->input->post('link');

        if ( null === $link )
        {
            exit( json_encode(['success' => false, 'message' => 'Link type is missing', $link]) );
        }

        $links = [
            'facebook',
            'twitter',
            'linkedin',
            'googleplus',
            'instagram'
        ];

        if ( !in_array(strtolower($link), $links, true) )
        {
            exit( json_encode(['success' => false, 'message' => 'Link type is not supported']) );
        }

        $data = [];

        if ( 'instagram' === $link )
        {
            $instragram_uri = $this->get_link_uri('instagram.com', $this->input->post('instagram_uri'));
            $instragram_uri = rtrim(ltrim($instragram_uri, '/'), '/');

            $data['link'] = $instragram_uri;
            $this->agent_sm_model->save_link(['instagram' => $instragram_uri]);
        }

        if ( 'facebook' === $link )
        {
            $facebook_uri = $this->get_link_uri('facebook.com', $this->input->post('facebook_uri'));
            $facebook_uri = rtrim(ltrim($facebook_uri, '/'), '/');

            $data['link'] = $facebook_uri;
            $this->agent_sm_model->save_link(['facebook' => $facebook_uri]);
        }

        if ( 'twitter' === $link )
        {

            $twitter_uri = $this->get_link_uri('twitter.com', $this->input->post('twitter_uri'));
            $twitter_uri = rtrim(ltrim($twitter_uri, '/'), '/');

            $data['link'] = $twitter_uri;
            $this->agent_sm_model->save_link(['twitter' => $twitter_uri]);
        }

        if ( 'linkedin' === $link )
        {

            $linkedin_uri = $this->get_link_uri('linkedin.com', $this->input->post('linkedin_uri'));
            $linkedin_uri = rtrim(ltrim($linkedin_uri, '/'), '/');

            $data['link'] = $linkedin_uri;
            $this->agent_sm_model->save_link(['linkedin' => $linkedin_uri]);
        }

        if ( 'googleplus' === $link )
        {
            $googleplus_uri = $this->get_link_uri('plus.google.com', $this->input->post('googleplus_uri'));
            $googleplus_uri = rtrim(ltrim($googleplus_uri, '/'), '/');

            $data['link'] = $googleplus_uri;
            $this->agent_sm_model->save_link(['googleplus' => $googleplus_uri]);
        }

        exit( json_encode(['success' => true, 'data' => $data]) );
    }

	public function add_links(){

		$facebook = (isset($_POST["facebook"])) ? $_POST["facebook"] : $this->input->post('facebook_url');
		$twitter = (isset($_POST["twitter"])) ? $_POST["twitter"] :$this->input->post('twitter_url');
		$linkedin = (isset($_POST["linkedin"])) ? $_POST["linkedin"] :$this->input->post('linkedin_url');
		$googleplus = (isset($_POST["googleplus"])) ? $_POST["googleplus"] :$this->input->post('googleplus_url');

		$arr = array(
			'user_id' => $this->agent_sm_model->user_id(),
			'facebook' => $facebook,
			'twitter' => $twitter,
			'linkedin' => $linkedin,
			'googleplus' => $googleplus
		);

		$fetch_link = $this->agent_sm_model->fetch_link('id');

		if (!$fetch_link)
		{
			$this->agent_sm_model->insert_link($arr);
		}
		else
		{
			$this->agent_sm_model->update_link($fetch_link['id'], $arr);
		}

        if ( $this->input->is_ajax_request() )
        {
            exit( json_encode(['success' => true, $fetch_link]) );
        }

		redirect('agent_social_media/links');
	}

	public function delete_link()
    {
        $link = $this->input->get('link');

        $links = [
            'facebook',
            'twitter',
            'linkedin',
            'googleplus',
            'instagram'
        ];

        if ( !in_array(strtolower($link), $links, true) )
        {
            exit( json_encode(['success' => false, 'message' => 'Link type is not supported']) );
        }

        if ( 'instagram' === $link )
        {
            $this->agent_sm_model->save_link(['instagram' => '']);
        }

        if ( 'facebook' === $link )
        {
            $this->agent_sm_model->save_link(['facebook' => '']);
        }

        if ( 'twitter' === $link )
        {
            $this->agent_sm_model->save_link(['twitter' => '']);
        }

        if ( 'linkedin' === $link )
        {
            $this->agent_sm_model->save_link(['linkedin' => '']);
        }

        if ( 'googleplus' === $link )
        {
            $this->agent_sm_model->save_link(['googleplus' => '']);
        }

        exit( json_encode(['success' => true]) );
    }

	public function get_property_activate_data(){

		if( $this->input->is_ajax_request() )
		{
			//get Spw Created sites
			$spw_property = $this->agent_sm_model->get_property_activate_data( $this->input->post("id"),$this->input->post("selected") );

			$obj = Modules::load("mysql_properties/mysql_properties");
			$account_info = $obj->get_account($this->session->userdata("user_id"), "account_info");

			if(isset($account_info) && !empty($account_info)){

				$agent_name = $account_info->Name;
				$agent_phone = $account_info->Phones[0]->Number;
			}

			//get custom domain url
			$domain_info = $this->Agent_sites_model->fetch_domain_info();
			if(!empty($domain_info)) {
				$base =  "https://www.";
				$base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
			} else {
				$base_url = "https://www.agentsquared.com";
			}

			//get spw url in domains table
			$listingid = $spw_property->ListingID;
			$property_name = url_title($spw_property->unparsedaddr, '-', TRUE);
			$property_uri = implode(
				'~',
				array(
					$property_name,
					$listingid,
					$this->session->userdata("user_id")
				)
			);

			$response = [
				'success' => false
			];

			if( isset($spw_property) ){
				//get spw website created data
				$response = array(
					"success" => TRUE,
					"address"             => (!empty($spw_property->address)) ? $spw_property->address : '',
					"unparseAddress"      => (!empty($spw_property->unparsedaddr)) ? $spw_property->unparsedaddr : $spw_property->address,
					"primary_photo"       => (!empty($spw_property->primary_photo)) ? $spw_property->primary_photo : '',
					"site_url"            => $base_url."property-site/".$property_uri,
					"site_description"    => (!empty($spw_property->description)) ? $spw_property->description : '',
					"price"               => (!empty($spw_property->price)) ? $spw_property->price : '',
					"price_previous"      => (!empty($spw_property->price_previous)) ? $spw_property->price_previous : '',
					"broker"              => (!empty($spw_property->broker)) ? $spw_property->broker : '',
					"broker_number"       => (!empty($spw_property->broker_number)) ? $spw_property->broker_number : '',
					"agent_name"          => (!empty($agent_name)) ? $agent_name : '',
					"agent_phone"         => (!empty($agent_phone)) ? $agent_phone : ''
				);
			}

			exit( json_encode($response) );
			//printA($property);exit();
		}
	}

	public function get_property_listing_data(){

		$response = [
			'success' => false
		];

		if($this->input->is_ajax_request()) {

			$dsl = modules::load('dsl/dsl');
			$token = $this->dsl_model->getUserAccessToken($this->session->userdata("agent_id"));
			$access_token = array(
				'access_token' => $token->access_token
			);

			$listing_id = $this->input->post("id");
			$standard_info = $dsl->post_endpoint('spark-listing/'.$listing_id.'/standard-fields', $access_token);

			$active_property = json_decode($standard_info);

			$photos = $dsl->post_endpoint('spark-listing/'.$listing_id.'/photos', $access_token);
			$photodata = json_decode($photos,true);
			$active_property_photo = isset($photodata['data'][0]['StandardFields']['Photos'][0]['Uri640']) ? $photodata['data'][0]['StandardFields']['Photos'][0]['Uri640'] : "";

			$obj = Modules::load("mysql_properties/mysql_properties");
			$account_info = $obj->get_account($this->session->userdata("user_id"), "account_info");

			if($account_info && !empty($account_info)){
				$agent_name = $account_info->Name;
				$agent_phone = $account_info->Phones[0]->Number;
			}

			if(!empty($active_property)){

				$active_description = str_limit($active_property->data->PublicRemarks,197);
				$active_image = $active_property_photo;
				$active_address = isset($active_property->data->UnparsedFirstLineAddress) ? $active_property->data->UnparsedFirstLineAddress : $active_property->data->UnparsedAddress;
				$active_broker = $active_property->data->ListOfficeName;
				$active_broker_number = $active_property->data->ListOfficePhone;
				$listingId = $active_property->data->ListingKey;
				$price = $active_property->data->ListPrice;
				$price_previous = $active_property->data->PreviousListPrice;
				$url_rewrite = url_title("{$active_property->data->UnparsedFirstLineAddress} {$active_property->data->PostalCode}");
			}
			//get custom domain url
			$domain_info = $this->Agent_sites_model->fetch_domain_info();
			if(!empty($domain_info)) {
				$base =  "https://www.";
				$base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
			} else {
				$base_url = "https://www.agentsquared.com";
			}

			if(!empty($active_property)) {
				//get active property data
				$response = array(
					"success" => TRUE,
					"site_url"            => $base_url.'property-details/'.$url_rewrite.'/'.$listingId,
					"site_description"    => (isset($active_description)) ? $active_description : '',
					"primary_photo"       => (isset($active_image)) ? $active_image : '',
					"address"             => (isset($active_address)) ? $active_address : '',
					"broker"              => (isset($active_broker)) ? $active_broker : '',
					"broker_number"       => (isset($active_broker_number)) ? $active_broker_number : '',
					"price"               => (isset($price)) ? $price : '',
					"price_previous"      => (isset($price_previous)) ? $price_previous : '',
					"agent_name"          => (isset($agent_name)) ? $agent_name : '',
					"agent_phone"         => (isset($agent_phone)) ? $agent_phone : ''
				);
			}

			exit(json_encode($response));
		}
	}

	public function get_property_site_data(){

		//get agent listing
		//$active_listings = Modules::run('dsl/getAgentActivePropertyListings');
		$active_listings = Modules::run('single_property_listings/getallActiveProperties');
		if(isset($active_listings)){
			foreach($active_listings as $active){

				$description = str_limit($active->StandardFields->PublicRemarks,197);
				$image = $active->StandardFields->Photos[0]->Uri1024;
			}
		}

		//get agent address
		$obj = Modules::load("mysql_properties/mysql_properties");
		$account_info = $obj->get_account($this->session->userdata("user_id"), "account_info");
		if(isset($account_info) && !empty($account_info)){

			$address = $account_info->Addresses[0]->Address;
			$broker = $account_info->Office;
			$broker_number = $account_info->Phones[0]->Number;
			$agent_name = $account_info->Name;
			$agent_phone = $account_info->Phones[0]->Number;
		}

		$domain_info = $this->Agent_sites_model->fetch_domain_info();

		if(!empty($domain_info)) {
			$base =  "https://www.";
			$base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
		} else {
			$base_url = base_url();
		}

		$response = array("success" => FALSE);

		if( $this->input->is_ajax_request() ){

			if(!empty($account_info)){

				//get agent listing Data
				$response = array(
					"success" => TRUE,
					"site_url"            => $base_url,
					"site_description"    => (isset($description)) ? $description : '',
					"primary_photo"       => (isset($image)) ? $image : '',
					"address"             => (isset($address)) ? $address : '',
					"broker"              => (isset($broker)) ? $broker : '',
					"broker_number"       => (isset($broker_number)) ? $broker_number : '',
					"agent_name"          => (isset($agent_name)) ? $agent_name : '',
					"agent_phone"         => (isset($agent_phone)) ? $agent_phone : ''
				);

			}

			exit( json_encode($response) );
		}
	}

	public function save_fb_user()
    {
        if ( ! $this->input->is_ajax_request() )
        {
            return;
        }

        $this->agent_sm_model->saveFacebookUser($this->input->post('user'));
    }

    public function save_fb_accounts()
    {
        if ( ! $this->input->is_ajax_request() )
        {
            return;
        }

        $this->agent_sm_model->saveFacebookAccounts($this->input->post('accounts'));
    }

	/**
	 * @param array $domain
	 * @param int   $mlsID
	 * @return array
	 */
	private function getSpwProperties(array $domain, $mlsID = 0)
	{
		$data = [];
		$properties = $this->agent_sm_model->get_your_properties_listings( FALSE, ['mlsID' => $mlsID] );
		$base_url   = $this->getSiteBaseUrl($domain);

		if ( $properties && !empty($properties) )
		{
			foreach ( $properties as $property )
			{
				//get spw url in domains table
				$property_name = url_title($property->unparsedaddr, '-', true);
				$property_uri  = implode('~', [$property_name, $property->ListingID, $this->session->userdata('user_id')]);
				$site_url      = "$base_url/property-site/$property_uri";

				$data[] = [
					'id'            => $property->ID,
					'site_url'      => $site_url,
					'primary_photo' => $property->primary_photo,
					'address'       => '' !== $property->unparsedaddr ? $property->unparsedaddr : $property->address
				];
			}
		}

		return $data;
	}


	/**
	 * @param array $domain
	 * @return array
	 */
	private function getActiveListings(array $domain)
	{
		$listings = Modules::run('single_property_listings/getallActiveProperties');
		$base_url = $this->getSiteBaseUrl($domain);
		$data = [];

		if ( $listings && !empty($listings) )
		{
			foreach ( $listings as $listing )
			{
				$fields = $listing->StandardFields;

				// site URL
				$url_rewrite = url_title("{$fields->UnparsedFirstLineAddress} {$fields->PostalCode}");
				$site_url = "{$base_url}/property-details/{$url_rewrite}/{$fields->ListingKey}";

				// primary photo
				$primary_photo = '';
				if ( !empty($fields->Photos) )
				{
					foreach ( $fields->Photos as $photos )
					{
						if ( isset($photos->Uri640) && '' !== $photos->Uri640 )
						{
							$primary_photo = $photos->Uri640;
							break;
						}
					}
				}

				// address
				$address = $fields->UnparsedAddress;
				if ( isset($fields->UnparsedFirstLineAddress) && '' !== $fields->UnparsedFirstLineAddress )
				{
					$address = $fields->UnparsedFirstLineAddress;
				}

				$mls_status = $fields->MlsStatus;
				$listing_key = $fields->ListingKey;

				$data[] = compact('site_url', 'primary_photo', 'address', 'mls_status', 'listing_key');
			}
		}

		return $data;
	}

	/**
	 * @param string $listing_type
	 * @param string $method
	 * @param array $domain
	 * @return array
	 */
	private function getListings($listing_type, $method, array $domain)
	{
		$limit = 4;
		$order = "-OnMarketDate";
		$selectFields = "UnparsedAddress,UnparsedFirstLineAddress,PostalCode,MlsStatus,ListingKey,Photos.Uri640";

		$account_info = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");

		if($listing_type == "new"){
			$lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
		    $today =  gmdate("Y-m-d\TH:i:s\Z");
		    $filterStr = "(MlsStatus Eq 'Active') And (OnMarketDate Bt ".$lastday.",".$today.")";
		    $filterUrl = rawurlencode($filterStr);
		    $filter = substr($filterUrl, 3, -3);

		} else {
			$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";
			$filter_url = rawurlencode($filterStr);
			$filter = substr($filter_url, 3, -3);
		}

		$latlon_info = $this->properties_model->get_account_latlon($this->session->userdata("agent_id"));
		if(!empty($latlon_info->latitude) && !empty($latlon_info->longitude)){

			$latlon = array(
				"lat" => $latlon_info->latitude,
				"lon" => $latlon_info->longitude
			);

		//get latlon from  third party service provider
		} elseif(!empty($account_info->Addresses[0]->Address) && $account_info->Addresses[0]->Address != "********"){
			$latlon = Modules::run('mysql_properties/getlatlon', $account_info->Addresses[0]->Address);
		} else {
			$latlon = Modules::run('mysql_properties/get_coordinates', $this->session->userdata("user_id"));
		}

		$params = "_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter.")&_select=".$selectFields."&_orderby=".$order;

		//no order_by on nearby listings to avoid data duplication on new listings.
		if($listing_type == "nearby"){
			$params = "_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter.")&_lat=".$latlon['lat']."&_lon=".$latlon['lon']."&_distance=20&_units=miles&_select=".$selectFields;
		}

		$listings = Modules::run('dsl/getSparkProxy', $method, $params);
		$base_url = $this->getSiteBaseUrl($domain);
		$data = [];

		if ( $listings && !empty($listings) )
		{
			foreach ( $listings as $listing )
			{
				$fields = $listing->StandardFields;

				// site URL
				$url_rewrite = url_title("{$fields->UnparsedFirstLineAddress} {$fields->PostalCode}");
				$site_url = "{$base_url}/property-details/{$url_rewrite}/{$fields->ListingKey}";

				// primary photo
				$primary_photo = '';
				if ( !empty($fields->Photos) )
				{
					foreach ( $fields->Photos as $photos )
					{
						if ( isset($photos->Uri640) && '' !== $photos->Uri640 )
						{
							$primary_photo = $photos->Uri640;
							break;
						}
					}
				}

				// address
				$address = $fields->UnparsedAddress;
				if ( isset($fields->UnparsedFirstLineAddress) && '' !== $fields->UnparsedFirstLineAddress )
				{
					$address = $fields->UnparsedFirstLineAddress;
				}

				$mls_status = $fields->MlsStatus;
				$listing_key = $fields->ListingKey;

				$data[] = compact('site_url', 'primary_photo', 'address', 'mls_status', 'listing_key');
			}
		}

		return $data;
	}


	/**
	 * @param array $domain
	 * @return string
	 */
	private function getSiteBaseUrl(array $domain)
	{
		$base_url = 'https://www.agentsquared.com';

		if ( ! empty($domain) )
		{
			$base_url =$domain[0]->subdomain_url;

			if ( $domain[0]->status === 'completed' )
			{
				$base_url = 'https://www.'.$domain[0]->domains;
			}

			$base_url = rtrim($base_url, '/');
		}

		return $base_url;
	}

    /**
     * @param        $base_domain
     * @param string $url
     * @return string|string[]|null
     */
	private function get_link_uri($base_domain, $url = '')
    {
        $url = ltrim(rtrim(rtrim($url, "\r\n"), '/'), '/');

        $url = preg_replace(
            '/^((http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.))?('.$base_domain.')?(\/.*)?$/i',
            '$4',
            $url
        );

        return ltrim($url, '/');
    }

    /**
     * @param        $base_domain
     * @param string $uri
     * @return string|string[]|null
     */
    private function get_link_url($base_domain, $uri = '')
    {
        if ( '' === $uri )
        {
            return $uri;
        }

        $uri = $this->get_link_uri($base_domain, $uri);
        $uri = ltrim(rtrim(rtrim($uri, "\r\n"), '/'), '/');

        return "https://www.{$base_domain}/{$uri}/";
    }
}
