<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MX_Controller {
  public function __construct() {
    parent::__construct();
    $userData = $this->session->userdata('user_id');

  }

  public function index() {
    // $config['dsl_username'] 	= 'agentsquared';
    // $config['dsl_password'] 	= '8c15bb00663a4c127286530cfeec02a3';
    // $config['dsl_serverurl'] 	= 'http://138.68.10.202:3000/api/v1/';
    // $config['dsl_serverurl_updater'] 	= 'http://138.68.56.211:3001/api/v1/';

    /*
      Saved Search
    */
    $endpoint = "savedsearches/572";
    $call_url = getenv('DSL_SERVER_URL').$endpoint;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $call_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
      array(
        'X-Auth-Token: '.$this->session->userdata('authToken'),
        'X-User-Id: '.$this->session->userdata('userId'),
        'Content-Type:application/json'
      )
    );
    $content = curl_exec($ch);

    /*
      Test
    */
    echo "<b>Content From DSL</b>";
    echo "<pre>";
    var_dump($content);
    echo "</pre>";

    echo "<b>Config</b>";
    echo "<pre>";
    var_dump(getenv('DSL_SERVER_URL').$call_url);
    var_dump($this->session->userdata('authToken'));
    var_dump($this->session->userdata('userId'));
    echo "</pre>";
  }
}
