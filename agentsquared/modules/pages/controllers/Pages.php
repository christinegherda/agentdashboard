<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Pages extends MX_Controller {
 	
 	public function __construct()
	{
		parent::__construct();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->load->model("Page_model","page");
		$this->load->model("media/Media_model","media");
		$this->load->model("agent_sites/Agent_sites_model", "domain");
		$this->load->library("idx_auth", $this->session->userdata("agent_id"));

		// Load Aws_s3 Library
  		$this->load->library('s3');
  		$this->config->load('s3');

  		$this->load->library('pagination');

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

	}

	public function index() {
		
		$data["title"] = "Manage Pages";
		
		//get published pages
		$param = array();
		$param["keywords"] = isset($_GET["keywords"])? "keywords=".$_GET["keywords"] : "";
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$data["total_pages"] = $this->page->get_total_pages();
		$data["total_published_pages"] = $this->page->get_total_published_pages();
		$data["pages"] = $this->page->get_all_pages(FALSE, $param);

		$data["published_pages"] = $this->page->get_all_published_pages( FALSE, $param );
		$data["group_published_pages"] = $this->page->get_group_published_pages();
		$data["trashed_pages"] = $this->page->get_all_trashed_pages(TRUE, $param);
		$data["group_trashed_pages"] = $this->page->get_group_trashed_pages();

		$config["base_url"] = base_url().'/pages?'.$param["keywords"];
		$config["total_rows"] = $data["total_published_pages"];
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","pages/pages.js");
		$data["css"] = array("agent-haven/msgBoxLight.css");
		
		$this->pagination->initialize($config);

		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		$data['account_info'] = Modules::run('mysql_properties/get_account',$this->session->userdata("user_id"),"account_info");
		
		$domain_info = $data["domain_info"];

        if(!empty($domain_info)) {
            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
        } else {
            $base_url = base_url();
        }

	    $data["base_url"] = $base_url;

		$this->load->view('pages', $data);

	}

	public function trash() {

		$data["title"] = "Trash Pages";

		//get trash pages
		$param = array();
		$param["keywords"] = isset($_GET["keywords"])? "keywords=".$_GET["keywords"] : "";
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$data["pages"] = $this->page->get_all_pages(FALSE, $param);
		$data["total_pages"] = $this->page->get_total_pages();
		$data["total_published_pages"] = $this->page->get_total_published_pages();
		$data["total_trashed_pages"] = $this->page->get_all_trashed_pages(TRUE, $param );
		$data["published_pages"] = $this->page->get_all_published_pages( FALSE, $param );
		$data["group_published_pages"] = $this->page->get_group_published_pages();
		$data["trashed_pages"] = $this->page->get_all_trashed_pages(FALSE, $param );
		$data["group_trashed_pages"] = $this->page->get_group_trashed_pages();
		
		$config["base_url"] = base_url().'/pages/trash?'.$param["keywords"];
		$config["total_rows"] = $data["total_trashed_pages"];
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","pages/pages.js");
		$data["css"] = array("agent-haven/msgBoxLight.css");
		
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();

		$this->load->view('trash_pages', $data);
	}

	public function filter(){

		$data["title"] = "Filter Pages";

		//filter published pages
		$param = array();
		$param["filter"] = isset($_GET["filter_date"])? $_GET["filter_date"] : "";
		$param["pagination_filter"] = isset($_GET["filter_date"])? "filter_date=".$_GET["filter_date"] : "";
		$param["keywords"] = isset($_GET["keywords"])? "&keywords=".$_GET["keywords"] : "";
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$data["total_pages"] = $this->page->get_total_pages();
		$data["total_published_pages"] = $this->page->get_total_published_pages();
		$data["pages"] = $this->page->get_all_pages( FALSE, $param );
		$data["published_pages"] = $this->page->get_all_published_pages( FALSE, $param );
		$data["group_published_pages"] = $this->page->get_group_published_pages();
		$data["trashed_pages"] = $this->page->get_all_trashed_pages( FALSE, $param );
		$data["total_trashed_pages"] = $this->page->get_all_trashed_pages( TRUE, $param );
		$data["group_trashed_pages"] = $this->page->get_group_trashed_pages();
		$data["filter_pages"] = $this->page->filter_date(FALSE,$param);
		$data["total_filter_published_pages"] = $this->page->filter_trash(TRUE,$param);
		
		$config["base_url"] = base_url().'/pages/filter?'.$param["pagination_filter"].$param["keywords"];
		$config["total_rows"] = $data["total_published_pages"];
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","pages/pages.js");
		$data["css"] = array("agent-haven/msgBoxLight.css");
		
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		
        $domain_info = $data["domain_info"];

	    if(!empty($domain_info)) {
            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
	    } else {
            $base_url = base_url();
	    }

	    $data["base_url"] = $base_url;

		$this->load->view('filter_pages', $data);
	}

	public function filter_trash(){

		$data["title"] = "Filter Trash";

		//filtered trash pages
		$param = array();
		$param["filter"] = isset($_GET["filter_date"])? $_GET["filter_date"] : "";
		$param["pagination_filter"] = isset($_GET["filter_date"])? "filter_date=".$_GET["filter_date"] : "";
		$param["keywords"] = isset($_GET["keywords"])? "&keywords=".$_GET["keywords"] : "";
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;
		$data["total_pages"] = $this->page->get_total_pages();
		$data["total_published_pages"] = $this->page->get_total_published_pages();
		$data["total_trashed_pages"] = $this->page->get_all_trashed_pages(TRUE, $param );
		$data["published_pages"] = $this->page->get_all_published_pages( FALSE, $param );
		$data["group_published_pages"] = $this->page->get_group_published_pages();
		$data["trashed_pages"] = $this->page->get_all_trashed_pages( FALSE, $param );
		$data["group_trashed_pages"] = $this->page->get_group_trashed_pages();
		$data["filter_trash"] = $this->page->filter_trash(FALSE,$param);
		$data["total_filter_trashed_pages"] = $this->page->filter_trash(TRUE,$param);
		//printA($data["total_filter_trashed_pages"]);exit;

		$config["base_url"] = base_url().'/pages/filter_trash?'.$param["pagination_filter"].$param["keywords"];
		$config["total_rows"] = $data["total_filter_trashed_pages"];
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","pages/pages.js");
		$data["css"] = array("agent-haven/msgBoxLight.css");
		
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();
		//printA( $data["filter_trash"] ); exit;

		$this->load->view('filter_trash', $data);
	}

	public function getSavedSearchesFromSpark(){

		if(!empty($this->idx_auth->checker())){

			$savedSearches = Modules::run('dsl/getAllAgentSavedSearchFromSpark');

			if(!empty($savedSearches)){

				$savedSearchpages = $savedSearches['pages'];
				$page = $savedSearchpages + 1;

				for($i=1; $i<=$page; $i++) {

					$result = $this->idx_auth->getSavedSearches($i);
						
					$saveSearchesData[] = $result['results'];
				}
				
				if(!empty($saveSearchesData)){

					$results = array_merge(...$saveSearchesData);

					//printA($results);exit;

					return $results;
				}
			}	
		}
	}

	public function add()
	{	
		$data["title"] = "Add New Page";

		if($_POST) {

			$this->form_validation->set_rules('page[title]', 'Title', 'required|trim');
			$this->form_validation->set_rules('page[content]', 'Content', 'required|trim');

			if($this->form_validation->run() == true) {

				$ret_page = $this->page->add();

				if($ret_page["success"]) {
					
					$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Page has been successfully added!'));
					redirect("pages/edit?page_id=".$ret_page["id"], "refresh");
				} else {
					$this->session->set_flashdata('session' , array('status' => FALSE, 'message' => $ret_page["message"]));
				}
				
			} else {
				$this->session->set_flashdata('session' , array('status' => FALSE,'message'=> validation_errors() ));
			}
		}

		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","pages/pages.js","summernote-image-attributes.js","media/media.js");
		$data["css"] = array("agent-haven/msgBoxLight.css","dashboard/media.css");

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["domain_info"] = $this->domain->fetch_domain_info();
		$data["is_reserved_domain"] = $this->domain->is_reserved_domain();

		$this->load->library('Ajax_pagination');
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 30;

		$total = $this->media->get_all_media( TRUE, $param );
		$data["media"] = $this->media->get_all_media( FALSE, $param );
		$data['published_media'] = $this->media->get_all_published_media_new(array('start'=>$param["offset"],'limit'=>$param["limit"]));
		
		$config['target']   = '.insertMediaModal';
		$config["base_url"] = base_url().'/pages/add_media_modal_pagination';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->ajax_pagination->initialize($config);
		$data["pagination"] = $this->ajax_pagination->create_links();

		$this->load->view('add-page-form', $data);
	}

	public function edit(){

		$data["title"] = "Edit Page";

		$pages_id = $this->get_all_agent_pages_id();

		 if (isset($_GET['page_id']) && is_numeric($_GET['page_id'])){

            if(in_array($_GET['page_id'], $pages_id,true)){

               $page_id = $_GET["page_id"];


				if($_POST)
				{
					$flash_data=array("status"=>FALSE);

					$this->form_validation->set_rules('page[title]', 'Title', 'required|trim');
					$this->form_validation->set_rules('page[content]', 'Content', 'required|trim');

					if($this->form_validation->run() == true) {

					 	$page = $this->page->edit($page_id);

					 	$flash_data["status"] = $page["success"];
					 	$flash_data["message"] = $page["message"];
					 	
					} else {
						$flash_data["message"] = validation_errors();
					}

					$this->session->set_flashdata('session', $flash_data);

					redirect("pages/edit?page_id=".$page_id, "refresh");

				}

				$data["info"] = $this->page->get_page( $page_id );

				if ($data["info"]["slug"] == "buyer" || $data["info"]["slug"] == "seller") {
					if ($this->config->item('disallowUser')) {
						redirect("/pages");
					}
				}

				// var_dump($data["info"]["slug"]); exit;

				$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","pages/pages.js","summernote-image-attributes.js","media/media.js");
				$data["css"] = array("agent-haven/msgBoxLight.css","dashboard/media.css");

				$siteInfo = Modules::run('agent_sites/getInfo');
				$data['branding'] = $siteInfo['branding'];
				$data["domain_info"] = $this->domain->fetch_domain_info();
				$data["is_reserved_domain"] = $this->domain->is_reserved_domain();

		        $domain_info = $data["domain_info"];

		        if(!empty($domain_info)) {
		            $base = ($domain_info[0]->is_ssl == "1") ? "https://" : "http://";
		            $base_url = ($domain_info[0]->status == "completed") ? $base.$domain_info[0]->domains.'/' : $domain_info[0]->subdomain_url.'/';
		        } else {
		            $base_url = base_url();
		        }

			    $data["base_url"] = $base_url;

			    $this->load->library('Ajax_pagination');

				$param = array();
				$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
				$param["limit"] = 30;

				$data["media"] = $this->media->get_all_media( FALSE, $param );
				$data['published_media'] = $this->media->get_all_published_media_new(array('start'=>$param["offset"],'limit'=>$param["limit"]));
				
				$total = $this->media->get_all_media( TRUE, $param );
				$config['target']   = '.insertMediaModal';
				$config["base_url"] = base_url().'/pages/edit_media_modal_pagination';
				$config["total_rows"] = $total;
				$config["per_page"] = $param["limit"];
				$config['page_query_string'] = TRUE;
				$config['uri_segment'] = 4;
				$config['query_string_segment'] = 'offset';
				
				$this->ajax_pagination->initialize($config);
				$data["pagination"] = $this->ajax_pagination->create_links();

				$this->session->set_userdata('page_id', $page_id);

				
				$this->load->view('edit-page-form', $data);

            } else{
            	
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to edit this page!'));
                redirect("pages", "refresh"); 
            }

        } else{
            
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to edit this page!'));
            redirect("pages", "refresh"); 

        }
		
	}

	public function edit_media_modal_pagination()
	{

		$this->load->library('Ajax_pagination');

		$page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }

        $param = array();
		$param["limit"] = 30;
		
		$data["info"] = $this->page->get_page($this->session->userdata('page_id'));

		$data["media"] = $this->media->get_all_media( FALSE, $param );
		$data['published_media'] = $this->media->get_all_published_media_new(array('start'=>$offset,'limit'=>$param["limit"]));

		$total = $this->media->get_all_media( TRUE, $param );
		$config['target']   = '.insertMediaModal';
		$config["base_url"] = base_url().'/pages/media_modal_pagination';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->ajax_pagination->initialize($config);
		$data["pagination"] = $this->ajax_pagination->create_links();
        
        $this->load->view('edit_media_modal_pagination', $data);
    }

    public function add_media_modal_pagination()
	{

		$this->load->library('Ajax_pagination');

		$page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }

        $param = array();
		$param["limit"] = 30;
		

		$data["media"] = $this->media->get_all_media( FALSE, $param );
		$data['published_media'] = $this->media->get_all_published_media_new(array('start'=>$offset,'limit'=>$param["limit"]));

		$total = $this->media->get_all_media( TRUE, $param );
		$config['target']   = '.insertMediaModal';
		$config["base_url"] = base_url().'/pages/add_media_modal_pagination';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->ajax_pagination->initialize($config);
		$data["pagination"] = $this->ajax_pagination->create_links();
        
        $this->load->view('add_media_modal_pagination', $data);
    }

	public function delete(){

        $pages_id = $this->get_all_agent_pages_id();

        if (isset($_GET['page_id']) && is_numeric($_GET['page_id'])){

            if(in_array($_GET['page_id'], $pages_id,true)){

                $this->page->delete( $_GET["page_id"]);

                $this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Page has been successfully deleted!'));

            } else{
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to delete this page!'));
                
            }

        } else{
            
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to delete page!'));

        }

        redirect("pages", "refresh");
    }

    public function bulk_delete()

    {
        if(isset($_POST['bulk_action']) AND !empty($_POST['bulk_action'])){

            if( $this->page->bulk_delete()){

            	$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Selected pages has been successfully deleted!'));

            } else {

            	$this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to delete all pages!'));
            }
        }   

        redirect("pages", "refresh");
    }

    public function move_trash(){

        $pages_id = $this->get_all_agent_pages_id();

        if (isset($_GET['page_id']) && is_numeric($_GET['page_id'])){

            if(in_array($_GET['page_id'], $pages_id,true)){

                $this->page->move_trash( $_GET["page_id"]);

                $this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Page has been successfully move to trash!'));

            } else{
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to move this page to trash!'));
                
            }

        } else{
            
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to move to trash!'));

        }

        redirect("pages", "refresh");
    }

	public function restore(){

		$pages_id = $this->get_all_agent_pages_id();

        if (isset($_GET['page_id']) && is_numeric($_GET['page_id'])){

        		$flash_data=array("status"=>FALSE);

            if(in_array($_GET['page_id'], $pages_id,true)){

               $restore = $this->page->restore( $_GET["page_id"],$_GET["page_title"]);

                $flash_data["status"] = $restore["success"];
				$flash_data["message"] = $restore["message"];

				$this->session->set_flashdata('session', $flash_data);

            } else{
                $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'You dont have the authority to restore this page!'));
                
            }

        } else{
            
            $this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to restore page!'));
        }

		redirect("pages", "refresh");
	}

	public function bulk_restore()
	{
		 if(isset($_POST['bulk_action']) AND !empty($_POST['bulk_action'])){

			if( $this->page->bulk_restore()){

				$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Selected pages has been successfully restored!'));
			} else {

				$this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to restore page!'));
			}
		 }	

		redirect("pages", "refresh");
	}

	public function bulk_trash()
	{
		if(isset($_POST['bulk_trash']) AND !empty($_POST['bulk_trash'])){

			if( $this->page->bulk_trash()){

				$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Selected pages has been successfully move to trash!'));
			} else {
				$this->session->set_flashdata('session' , array('status' => FALSE, 'message'=>'Failed to move to trash!'));
			}
		}	

		redirect("pages/trash", "refresh");
	}

	public function insertMedia() {

		if($this->input->is_ajax_request()) {

			if(isset($_POST['bulk_insertMedia']) AND !empty($_POST['bulk_insertMedia'])){

				$media_id = $this->input->post("bulk_insertMedia");
	            $post = "media_url".$media_id."";
	            $media_url = $this->input->post($post);

				$response = array('success'=>TRUE, 'message'=>'Media Successfully added!', 'mediaUrl' => $media_url);

			} else {

				$response = array('success'=>FALSE, 'message'=>'Please insert media.');
			}	

			exit(json_encode($response));
		}

	}

	public function update_details(){

		if($this->input->is_ajax_request()) {

			if($_POST){
				$media_id = (isset($_POST["media_id"])) ? $_POST["media_id"] : $this->input->post('media_id');
				$page_id = (isset($_POST["page_id"])) ? $_POST["page_id"] : $this->input->post('page_id');
				$title = (isset($_POST["title"])) ? $_POST["title"] : $this->input->post('media_title');
				$file_name = (isset($_POST["file_name"])) ? $_POST["file_name"] : $this->input->post('file_name');
		        $link = (isset($_POST["link"])) ? $_POST["link"] :$this->input->post('media_link');
		        $caption = (isset($_POST["caption"])) ? $_POST["caption"] :$this->input->post('media_caption');
		        $alt_text = (isset($_POST["alt_text"])) ? $_POST["alt_text"] :$this->input->post('media_alt_text');
		        $description = (isset($_POST["description"])) ? $_POST["description"] :$this->input->post('media_description');
		        $url = base_url();

		        $arr = array(
		            'title' => $title,
		            'link' => $link,
		            'caption' => $caption,
		            'alt_text' => $alt_text,
		            'description' => $description,
		        );

		        if(!empty($page_id)){
		        	$redirect = $url."pages/edit?page_id=".$page_id."?media_modal";
		        } elseif($page_id == "site_info"){
		        	$redirect = $url."agent_sites/infos?media_modal";
		        } else {
		        	$redirect = $url."pages/add?media_modal";

		        }

				if($media_id){
					
					$this->page->updateImageDetails($media_id,$arr);

					$response = array("success" => TRUE ,"message" => "<span class='alert alert-success' style='display:block;'>Media Successfully Updated!</span>", "media_id"=> $media_id, "redirect"=>$redirect);

				} else {
					$response = array("success" => FALSE ,"message" => "<span class='alert alert-danger' style='display:block;'>Failed to update media!</span>" );
				}

			}	

			exit(json_encode($response));
		} 
	}

	public function upload_media_page(){


		if(!empty($_FILES["userfile"]["name"])){

			if($_FILES["userfile"]["error"] == 0 && $_FILES["userfile"]["size"] < "10485760" ){

				if( $_FILES["userfile"]["type"] == "image/jpeg" || $_FILES["userfile"]["type"] == "image/png" || $_FILES["userfile"]["type"] == "image/gif" || $_FILES["userfile"]["type"] == "text/plain" || $_FILES["userfile"]["type"] == "text/csv" || $_FILES["userfile"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $_FILES["userfile"]["type"] == "application/pdf" || $_FILES["userfile"]["type"] == "application/vnd.openxmlformats-officedocument.word" || $_FILES["userfile"]["type"] == "application/msword"){ 

					//generate random-id
					$userfile = uniqid().'-'.($_FILES['userfile']['name']);
					$file = $_FILES['userfile']['tmp_name'];
					$bucket_name = $this->config->item('bucket_name');
					$file_name = "uploads/file/".$userfile;
					$acl = "public-read";

					$data = array(
						"title" => $userfile,
						"file_name" => $userfile,
						"file_type" => $_FILES['userfile']['type'],
						"file_size" => $_FILES['userfile']['size']
					);

					if(!empty($file)){

						//upload userfile to AWS S3
			 	 		$this->s3->putObjectFile($file,$bucket_name,$file_name,$acl);
					}

					if(!empty($userfile)){

						//save to database
						$media_id = $this->media->insert_media_aws($data);

						if($media_id){

							$status = "success";
			                $msg = "Media has been successfully added!";
			                $media_id = $media_id;
			                $media_title = $data['file_name'];
			                $media_url = $data['file_name'];
			                $media_file_type = $data['file_type'];
			                $is_freemium = TRUE;

						} else { 

							$status = "error";
			                $msg = "Failed to Upload Media!";
			                $media_id = "";
			                $media_title ="";
			                $media_url ="";
			                $media_file_type = "";
			                $is_freemium = TRUE;
						}
					}

				} else {

					$status = "error";
	                $msg = "The filetype you are attempting to upload is not allowed!";
	                $media_id = "";
	                $media_title ="";
	                $media_url ="";
	                $media_file_type = "";
	                $is_freemium = TRUE;
				}

			} else {

				$status = "error";
                $msg = "The uploaded file exceeds the maximum upload filesize!";
                $media_id = "";
                $media_title ="";
                $media_url ="";
                $media_file_type = "";
                $is_freemium = TRUE;

			}

		} else {

			$status = "error";
            $msg = "You did not select a file to upload!";
            $media_id = "";
            $media_title ="";
            $media_url ="";
            $media_file_type = "";
            $is_freemium = TRUE;
		}

	    echo json_encode(array('status' => $status, 'msg' => $msg, 'media_id' => $media_id, 'media_url' => $media_url, 'media_title' => $media_title, 'media_file_type' => $media_file_type, 'is_freemium' => $is_freemium));
	}

	public function get_all_agent_pages_id(){

		$agent_pages_id = $this->page->get_agent_pages_id();
        $pages_id = array();

        foreach($agent_pages_id as $ap_id){
            $pages_id[] = $ap_id->id;
        }

        return $pages_id;
	}

	public function get_saved_search_24(){

		if(isset($_GET["page_id"]) && !empty($_GET["page_id"])){
			$search_id = $this->page->get_search_id($_GET["page_id"]);
		}

		$saved_search_id = !empty($search_id["saved_search_id"])? $search_id["saved_search_id"] : "";

		$savedSearches = Modules::run('dsl/getAllAgentSavedSearchFromSpark');
		$total_count = isset($savedSearches["count"]) ? $savedSearches["count"]: "";

		//get only 24 savedsearches
		if(!empty($savedSearches)){

			$page = 1;

			for($i=1; $i<=$page; $i++) {

				$result = $this->idx_auth->getSavedSearches($i);
					
				$saveSearchesData = $result['results'];
			}
			
			if(!empty($saveSearchesData)){

				echo json_encode(array('success' => TRUE, 'count'=> $total_count, 'items' => $saveSearchesData, 'saved_search_id' => $saved_search_id));
			}
		}

	
	}

	public function get_saved_search_all(){

		if(isset($_GET["page_id"]) && !empty($_GET["page_id"])){
			$search_id = $this->page->get_search_id($_GET["page_id"]);
		}

		$saved_search_id = !empty($search_id["saved_search_id"])? $search_id["saved_search_id"] : "";

		$savedSearches = Modules::run('dsl/getAllAgentSavedSearchFromSpark');
		$total_count = isset($savedSearches["count"]) ? $savedSearches["count"]: "";

		//get all savedsearches
		if(!empty($savedSearches)){

			$savedSearchpages = $savedSearches['pages'];
			$page = $savedSearchpages + 1;

			for($i=1; $i<=$page; $i++) {

				$result = $this->idx_auth->getSavedSearches($i);
					
				$saveSearchesData[] = $result['results'];
			}
			
			if(!empty($saveSearchesData)){

				$results = array_merge(...$saveSearchesData);

				echo json_encode(array('success' => TRUE, 'count' => $total_count,'items' => $results, 'saved_search_id' => $saved_search_id));

			}
		}

	}

}
