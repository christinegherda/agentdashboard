<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>
    <div class="content-wrapper">
     <div class="page-title">
        <h3 class="heading-title">
          <span> Trashed Pages</span>
        </h3>
      </div>              
        <section class="content">
            <div class="container-fluid">
                <?php if( !empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret")) AND !config_item("is_paid") ) : ?>
                    <?php $this->load->view('session/launch-view'); ?>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">

                       <?php $session = $this->session->flashdata('session');
                          if( isset($session) ){?>

                            <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>

                        <?php } ?>

                      <?php if(isset($_GET["keywords"])){?>
                           <a href="<?php echo site_url("pages"); ?>" class="btn btn-default btn-submit"><i class="fa fa-arrow-left"></i> Back to page list</a>
                      <?php } else {?>
                                <a href="<?php echo site_url("pages/add"); ?>" class="btn btn-default btn-submit"><i class="fa fa-plus"></i> Add New Page</a>
                       <?php } ?>
                    </div>
                    <br><br>
                    <div class="page-content">
                     <?php if( !empty($pages) ) { ?>
                        <div class="col-md-12 add-page">
                            <ul class="sub-header">
                                <li class="all"><a href="<?php echo base_url()?>pages">All <span class="count">(<?=isset($total_pages) ? $total_pages : 0?>)</span></a>  |</li>
                                <li class="publish">
                                  <a href="<?php echo base_url()?>pages?status=published">
                                    Published <span class="count">(<?=isset($total_published_pages) ? $total_published_pages : 0?>)</span>
                                  </a>
                                </li>
                                <li class="trash"> |
                                  <a href="<?php echo base_url()?>pages/trash" class="current">
                                  <?php
                                    if($this->uri->segment(2)=="trash") { ?>
                                      <strong>Trash <span class="count">(<?=$total_trashed_pages?>)</span></strong>
                                  <?php
                                    } else { ?>
                                      Trash <span class="count">(<?= $total_trashed_pages?>)</span>
                                  <?php
                                    }
                                  ?>
                                  </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12 mb-30px">
                            <div class="col-md-4">
                                <div class="btn-group">
                                    <form action="" method="POST" id="bulk-trash">
                                         <select name="bulk_action" id="bulk-select" class="form-control">
                                            <option value="">Bulk Actions</option>
                                            <option value="restore">Restore</option>
                                             <option value="delete">Delete Permanently</option>
                                         </select>
                                     </form>
                                </div>

                                <div class="btn-group">
                                    <button type="submit" form="bulk-trash" class="btn btn-default btn-add-featured">Apply</button>
                                </div>
                        
                                <div class="btn-group">
                                    <form action="" method="POST" id="filter-trash">
                                         <select name="filter_month" id="select-trash" class="form-control">
                                            <option>All Dates</option>
                                            <?php if( !empty($group_trashed_pages) ) { 
                                                foreach($group_trashed_pages as $page){?>

                                                    <option value="?filter_date=<?php echo date("Y-m", strtotime($page->post_date)); ?>"> <?php echo date("F Y", strtotime($page->post_date)); ?></option>

                                            <?php  }

                                                } 
                                            ?>         
                                        </select>
                                    </form>   
                                </div>
                                 <div class="btn-group">
                                    <button type="submit" form="filter-trash" class="btn btn-default btn-add-featured">Filter</button>
                                </div>

                            </div>
                            <div class="col-md-4">
                                
                            </div>
                            <div class="col-md-4">
                                <form action="" class="form-horizontal" method="get" >
                                    <div class="input-group">
                                        <input type="text" name="keywords" value="<?php echo (isset($_GET['keywords'])) ? $_GET['keywords'] : "" ;?>" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-submit" type="submit">Search</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    <?php }?> 
                    
                        <div class="col-md-12">
                            <table class="table table-striped table-responsive">
                                  <thead>
                                    <tr>
                                      <?php if( !empty($trashed_pages) ) {?>
                                        <th><label><input type="checkbox" id="checkAll"/></label></th>
                                      <?php }?>
                                      <th>Title</th>
                                      <th class="text-center">Date</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?php if( !empty($trashed_pages) ) {
                                            foreach ($trashed_pages as $page) :
                                        ?> 

                                             <tr>
                                              <td width="1%">
                                                <input type="checkbox" name="bulk_action[]" value="<?php echo $page->id ?>" form="bulk-trash">
                                              </td>
                                              <td width="90%">
                                                <?php echo ucwords($page->title); ?>
                                                <br>
                                                <a href="<?php echo site_url('pages/restore?page_id='.$page->id.'&page_title='.$page->title); ?>">Restore</a> <span>|</span> <a href="<?php echo site_url('pages/delete?page_id='.$page->id); ?>">Delete Permanently</a>
                                                 
                                              </td>
                                              <td width="10%">
                                                  <p class="pull-right"><?php echo date("F d, Y", strtotime($page->post_date)); ?> <br> <?php echo ($page->status) ? ucfirst($page->status) : "Draft"; ?></p>
                                              </td>
                                            </tr> 

                                                <?php endforeach; ?>
    
                                      <?php }  else { ?>
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">No <strong><?php echo isset($_GET["keywords"]) ?  ucwords($_GET["keywords"]) : ""?></strong> <?=($this->uri->segment(2)=="trash" && !isset($_GET["keywords"])) ? "trash" : "page"?>  found!</p>
                                            </td>
                                        </tr>
                                      <?php } ?>
                                  </tbody>
                                </table>
                        </div>
                        <div class="col-md-12 search-pagination">
                            <?php
                          if($total_trashed_pages > 10){
                              if( $pagination ) {?>
                                <div class="pagination-area pull-right">
                                    <nav>
                                        <ul class="pagination-list">
                                            <?php echo $pagination; ?>
                                        </ul>
                                    </nav>
                                </div>
                        <?php }
                            }?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $this->load->view('session/footer'); ?>

