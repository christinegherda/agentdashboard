<?php
    $this->load->view('session/header');
    $this->load->view('session/top-nav');
?>
 	<div class="content-wrapper">
 	  <div class="page-title">
	    <h3 class="heading-title">
	      <span> Add New Page </span>
	    </h3>
	  </div>
      <section class="content">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                	<?php echo form_open('pages/add', array("class" => "new-page-sbt", "id" => "new-page-sbt" ) ); ?>
	                    <div class="col-md-12">
	                        <a href="<?php echo site_url("pages"); ?>" class="btn btn-default btn-submit"><i class="fa fa-arrow-left"></i> Back to page list</a>
	                    </div>
	                    <br>
	                    <br>
	                    <div class="col-md-12">
                    		<?php $session = $this->session->flashdata('session');
                    			if( isset($session) ){?>

                    				<div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>

                    			<?php if($session["status"]){?>

                    				 <div class="alert alert-info alert-dismissable">
									    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
									    <a href="<?php echo site_url("menu?custom_menu"); ?>" target="_blank"><strong>Click Here!</strong></a>  to add Custom Menu to add custom page to your website.
									  </div>
                    			<?php }?>

                    		<?php } ?>
                     	</div>
	                    <div class="col-md-8">
	                    	<div id="publish_ret_msg" style="display: none;">
		                     	<div class="alert alert-danger alert-dismissable">
							    	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
							    	<strong>Error!</strong> Please complete all required fields!
							  	</div>
							</div>
	                        <input type="text" class="page-titlearea form-control" name="page[title]" placeholder="Enter Page Title Here" maxlength="255" pattern="[a-zA-Z0-9-_.,$\s]+" title="Allowed characters: A-Z, 0-9, $, ., -, _">
	                        <br>
	                        <a style="margin-bottom:10px" href="#" type="button" class="btn btn-default btn-submit" data-toggle="modal" data-target="#modalAddPage"><i class="fa fa-file-image-o" aria-hidden="true"></i> Add Media</a>

	                         <div style="padding-top:20px" class="row">
		                        	<div class="col-md-4">
		                        		<i style="display:none" class=" fetch-ss-info-link fa fa-info-circle"></i> <span class="fetch-ss-text-link"></span> <span class="fetch-ss-text-all-link"></span> <i style="display: none;" class="fetch-ss-loading-link fa fa-spinner fa-spin"></i>
		                        		<div class="saved-search-area">
		                        			<select id="SavedSearchSelectLink" name="page[saved_search_id]" class="form-control">
			                                	<!-- populate ajax options here -->
			                            </select>
			                             <i class="fa text-primary fa-info-circle"></i> <small>Select saved search to add a link into your page.</small>
			                              <span style="display:none;text-decoration:underline" class="show-all-ss-link pull-right"><a href="javascript:void(0)"><small>Show all saved search</small></a></span>
		                        		</div>
		                        	</div>
		                        </div> 
		                        <br/>

	                        <textarea id="pageTextarea" name="page[content]" class="page-textarea" placeholder="Enter Page Description Here"></textarea>

													<div style="padding-top:20px" class="row">
	                        	<div class="col-md-4">
	                        		<i style="display:none" class=" fetch-ss-info fa fa-info-circle"></i> <span class="fetch-ss-text"></span> <span class="fetch-ss-text-all"></span> <i style="display: none;" class="fetch-ss-loading fa fa-spinner fa-spin"></i>
	                        		<div class="saved-search-area">
	                        			<select id="SavedSearchSelect" name="page[saved_search_id]" class="form-control">
		                                	<!-- populate ajax options here -->
		                                </select>
		                                 <span style="display:none;text-decoration:underline" class="show-all-ss pull-right"><a href="javascript:void(0)"><small>Show all saved search</small></a></span>
	                        		</div>
	                        	</div>
	                        </div> 

	                    </div>
	                    <div class="col-md-4">
	                        <div class="page-panel">
	                            <div class="page-actions">
	                                <h4>Publish</h4>
	                                <h5><i class="fa fa-key" aria-hidden="true"></i> Status: <strong>Draft</strong> <!--<a href="javascript:void(0);" class="toggle-option text-underline">Edit</a>--></h5>
	                                <div class="options" style="display:none">
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="statusRadios" id="" value="Published" class="radio-input">
	                                            Published
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="statusRadios" id="" value="Draft" class="radio-input">
	                                            Draft
	                                        </label>
	                                    </div>
	                                   <button class="btn btn-submit">Save</button>
	                                   <a href="#" class="toggle-option text-underline">Cancel</a>
	                                </div>
	                            </div>

	                            <div class="page-actions">
	                                <h5><i class="fa fa-eye" aria-hidden="true"></i> Visiblity: <strong>Public</strong> <!--<a href="javascript:void(0);" class="toggle-option text-underline">Edit</a>--></h5>
	                                <div class="options" style="display:none">
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="visibilityRadios" id="" value="Public">
	                                            Public
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" name="visibilityRadios" id="" value="Private">
	                                            Private
	                                        </label>
	                                    </div>
	                                    <button class="btn btn-submit">Save</button>
	                                   <a href="#" class="toggle-option text-underline">Cancel</a>
	                                </div>
	                            </div>
	                             <h5><i class="fa fa-calendar" aria-hidden="true"></i> Publish <strong>immediately</strong></h5>
	                            <br>
	                            <button class="btn btn-default btn-submit page-publish">Publish</button>
	                            <!--<a href="javascript:void(0)" type="button" class="btn btn-default btn-submit">Preview</a>-->
	                            <h4>SEO</h4>
	                            <div class="seo-section">
		                            <div class="form-group">
		                            	<label>
		                            		Title
		                            		<strong class="pull-right text-green title-count">
		                            			62
		                            		</strong>
		                            	</label>
		                            	<input type="text" name="page[meta_title]" class="form-control title-char" maxlength="62" value="<?= isset($info["meta_title"]) ? $info["meta_title"] : ""?>" pattern="[a-zA-Z0-9!@#$%&()|-?,.\s]+" title="Allowed characters: A-Z,0-9,!@#$%&()|-?,.">
		                            </div>
		                            <div class="form-group">
		                            	<label>
		                            		Description
		                            		<strong class="pull-right text-green desc-count">
		                            			160
		                            		</strong>
		                            	</label>
		                            	<textarea class="form-control desc-char" rows="5" name="page[meta_description]" maxlength="160"><?= isset($info["meta_description"]) ? $info["meta_description"] : ""?></textarea>
		                            </div>
		                            <div class="form-group">
		                            	<label>
		                            		Image
		                            	</label>
		                            	<input type="text" name="page[seo_image]" class="form-control" value="<?= isset($info["seo_image"]) ? $info["seo_image"] : ""?>">
		                            </div>
		                           <!--  <div class="form-group">
		                            	<label>
		                            		URL Slug
		                            	</label>
		                            	<input type="text" name="page[seo_slug]" class="form-control" value="<?= isset($info["seo_slug"]) ? $info["seo_slug"] : ""?>">
		                            </div>
		                            <div class="form-group">
		                            	<label>
		                            		Canonical
		                            	</label>
		                            	<input type="text" name="page[canonical]" class="form-control" value="<?= isset($info["canonical"]) ? $info["canonical"] : ""?>">
		                            </div> -->
	                            </div>
	                        </div>
	                    </div>
               		</form>


               		  <div class="modal insertMediaModal fade" id="modalAddPage" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
	                    <div class="modal-dialog">
	                        <div class="modal-content">
	                            <div class="modal-header">
	                            <div class="alert-status-media-upload"></div>
	                                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
	                                 <?php if(!empty($published_media)){?>
	                                	 <strong><h3>Insert Media or Upload New Media</h3></strong> 
	                                <?php }else{?>
	                                	 <strong><h3>Upload New Media</h3></strong> 
	                                <?php }?>
	                               
		                            <form id="upload-media-page" method="POST" action="<?= base_url()?>pages/upload_media_page" enctype="multipart/form-data">
	                      
	                                    <div style="margin-bottom: 5px" class="input-group imageupload">
	                                        <input type="file" name="userfile" class="filestyle" data-buttonBefore="true" data-icon="false">
	                                    </div>

	                                    <small>Max. upload file size: <strong>10 MB</strong></small> | <small>Allowed file type: <strong>jpg</strong> | <strong>jpeg</strong> | <strong>png</strong> | <strong>gif</strong> | <strong>doc</strong> | <strong>docx</strong> | <strong>xls</strong> | <strong>xlsx</strong> | <strong>csv</strong> | <strong>pdf</strong> | <strong>txt</strong></small>

	                                    <div class="box-footer">
	                                        <button type="submit" class="btn btn-default btn-save upload-media-page btn-submit"> <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Upload</button>
	                                    </div>
	                                </form>
	                            </div>

	                            <div class="modal-body addMediaModal">
		                            <div class="row">
		                            	<div class="col-md-9 upload-media-preview">
											<form id="insertMedia" action="<?php echo base_url()?>pages/insertMedia" method="POST">
													<!-- <div class="col-md-9 modal-media"> -->

													 <ul class="list-item">

					                                  <?php if(!empty($media)){

					                                          if(!empty($published_media)) { ?>


					                                              <?php foreach ($published_media as $media){?>

					                                                  <li class="list-item-grid">
					                                                    <div class="media-grid-actions">
					                                                    	<span class="checked">
					                                                    		<i class="fa fa-check"></i>
					                                                    	</span>
					                                                        <div class="ischeckbox" data-id="media<?=$media->id?>">
					                                                        	<input type="checkbox" class="modal-checkbox" name="bulk_insertMedia" value="<?php echo $media->id ?>">
					                                                        </div>
					                                                    </div>

					                                                    	<?php if($media->is_freemium == "1"){

						                                                            $src = getenv('AWS_S3_ASSETS') . "uploads/file/".$media->file_name;
						                                                      } else{

						                                                           $src = base_url()."assets/upload/file/".$media->file_name;
						                                                      }?>

					                                                      <?php if($media->file_type === "application/pdf" ){?>

					                                                          <div class="item-grid media<?=$media->id?>">
					                                                            <img src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?php echo $media->alt_text?>">
																				<input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block" src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>'/>


					                                                          </div>

					                                                         <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.word" || $media->file_type === "application/msword") { ?>

					                                                           <div class="item-grid media<?=$media->id?>">
					                                                                <img src='<?php echo base_url()?>assets/images/doc-icon.png' alt='<?php echo $media->alt_text?>'>
					                                                                 <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>"  target="_blank"><img style="width:15%;display:block" src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>"</a>' />

					                                                            </div>

					                                                         <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") { ?>

					                                                         	<div class="item-grid media<?=$media->id?>">
					                                                                <img src='<?php echo base_url()?>assets/images/excel-icon.png' alt='<?php echo $media->alt_text?>'>
					                                                                 <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block" src="<?php echo base_url()?>assets/images/excel-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" </a>'  />

					                                                            </div>

					                                                         	<?php } elseif($media->file_type === "text/plain"){?>
					                                                         		<div class="item-grid media<?=$media->id?>">
						                                                                <img src='<?php echo base_url()?>assets/images/txt-icon.png' alt='<?php echo $media->alt_text?>'>
						                                                                 <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block" src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>"</a>' />

					                                                            	</div>

					                                                            <?php } elseif($media->file_type === "text/csv"){?>
					                                                         		<div class="item-grid media<?=$media->id?>">
						                                                                <img src='<?php echo base_url()?>assets/images/csv-icon.png' alt='<?php echo $media->alt_text?>'>
						                                                                 <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:15%;display:block" src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>"</a>' />

					                                                            	</div>
					                                                         	<?php } else {?>

						                                                         <div class="item-grid media<?=$media->id?>">

						                                                               <img src='<?php echo $src?>' alt='<?php echo $media->alt_text?>'>
						                                                                <input type="hidden" name="media_url<?=$media->id?>" value='<a href="<?=!empty($media->link) ? $media->link : $src ?>" target="_blank"><img style="width:50%;display:block" src="<?php echo $src ?>" alt="<?=isset($media->alt_text) ? $media->alt_text : ""?>" ></a>' />

						                                                        </div>

					                                                         <?php }?>

					                                                  </li>

					                                          <?php }?>

					                                       <?php }

					                                    }  else { ?>

					                                    <div class="no-media-uploaded">

					                                       <p class="no-media">No files found!<br></p>

					                                    </div>

					                                  <?php } ?>
					                                  </ul>
							                        <!-- </div> -->

					                        </form>
		                            	</div>
		                            	<div class="col-md-3 image-details">

					                        	<h3>Image Details</h3>

					                        	<div class="show-notif"></div>

					                        	<div class="image-deatils-preview">

					                        	  <?php if(!empty($published_media)){

		                                               foreach ($published_media as $media){?>

		                                               		<?php if($media->is_freemium == "1"){

		                                                            $src = getenv('AWS_S3_ASSETS') . "uploads/file/".$media->file_name;
		                                                      } else{

		                                                           $src = base_url()."assets/upload/file/".$media->file_name;
		                                                      }?>


			                                               	<div id="media<?=$media->id?>" class="media-details hide-media clearfix">

			                                               	 <form id="updateMediaForm" class="updateMedia clearfix" action="<?php echo base_url()?>pages/update_details" method="POST">

			                                               	  <input type="hidden" name="media_id" value="<?=$media->id?>">
			                                               	 <input type="hidden" name="file_name" value="<?=$media->file_name?>">

																 <p><label for="media-title">Title</label></p>
					                        					<input type="text" name="media_title" id="media-title" value='<?php echo (isset($media->title)) ? $media->title : "" ?>' class="form-control" maxlength="30" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> 

										                    	<?php if($media->file_type === "application/pdf"){?>

									                            	<p class="media-icon"><img src="<?php echo base_url()?>assets/images/pdf-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

									                           <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.word" || $media->file_type === "application/msword" ) { ?>

									                            	<p class="media-icon"><img src="<?php echo base_url()?>assets/images/doc-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

									                           <?php } elseif($media->file_type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){?>

									                           		<p class="media-icon"><img src="<?php echo base_url()?>assets/images/excel-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

									                           <?php } elseif($media->file_type === "text/plain"){?>

									                           		<p class="media-icon"><img src="<?php echo base_url()?>assets/images/txt-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>

									                           	<?php } elseif($media->file_type === "text/csv"){?>

									                           		<p class="media-icon"><img src="<?php echo base_url()?>assets/images/csv-icon.png" alt="<?php echo $media->alt_text?>" width="80" height="80"></p><br>
									                           		
									                           <?php } else {?>

									                           		<p class="media-icon"><img src='<?php echo $src ?>' alt='<?php echo $media->alt_text?>' width="80" height="80"></p><br>

									                           <?php }?>

									                           <label for="media-link">Image Link</label>
										                    	<input type="text" name="media_link" id="media-link" value='<?php echo (!empty($media->link)) ? $media->link : $src?>' class="form-control" disabled> <br>

										                        <label for="media-caption">Caption</label>
										                    	<input type="text" name="media_caption" id="media-caption" value='<?php echo (!empty($media->caption)) ? $media->caption : "" ?>' class="form-control" maxlength="100" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>

																<label for="media-alt-text">Alternative Text</label>
										                    	<input type="text" name="media_alt_text" id="media-alt-text" value='<?php echo (!empty($media->alt_text)) ? $media->alt_text : "" ?>' class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>

										                        <label for="media-description">Description</label>
										                        <input type="text" name="media_description" id="media-description" value='<?php echo (!empty($media->description)) ? $media->description : "" ?>' class="form-control" maxlength="200" pattern="[a-zA-Z0-9-_.,\s]+" title="Allowed characters: A-Z, 0-9, ., -, _"> <br>

										                    	<button type="submit" class="btn btn-default btn-submit">Update</button>
										                    	 </form>
					                        				</div>
		                                      			<?php }
			                                    	}?>
			                                    </div>

		                            	</div>
		                            </div>
	                            </div>

	                           	<div style="display:none" class="modal-footer insertPage">
				                	<button type="submit" id="insertIntoPage" class="btn btn-default btn-submit pull-left">Insert Into Page</button>
                            	</div>

	                        </div>
	                    </div>
	                </div> <!-- ModalPage!-->
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
<?php $this->load->view('session/footer'); ?>

<script  type="text/javascript">
		tinymce.init({
	    selector: '#pageTextarea',
	    theme: 'silver',
	    content_style: '.mce-content-body {font-size:14px;font-family:Arial,sans-serif;}',
	    fontsize_formats: '12px 14px 16px 18px 20px 24px 36px 42px',
	    plugins: [
	      'autosave autoresize autolink code link',
	      'searchreplace wordcount media lists print',
	      'preview table emoticons paste textcolor imagetools'
	    ],
	    autosave_interval: '10s',
	    min_height: 350,
	    max_height: 500,
	    menubar: 'file edit format table',
	    toolbar_items_size: 'small',
	    toolbar: 'undo redo | fontsizeselect fontselect | blockquote bold italic underline forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink media | emoticons code preview help',
	   default_link_target: "_blank",
	   link_assume_external_targets: true,
	   link_context_toolbar: true,
	   link_title: false
   });
</script>
