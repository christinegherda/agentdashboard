<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>

    <div class="content-wrapper">   
      <div class="page-title">
        <div class="col-md-7 col-sm-6">
          <h3 class="heading-title">
            <span> Pages</span>
          </h3>
        </div>
      </div>
      <div class="col-md-5 col-sm-6 col-md-3 pull-right">
          <div class="otherpage video-box video-collapse">
              <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-5">
                  <h1>Need Help?</h1>
                  <p>See how by watching this short video.</p>
                  <div class="checkbox">
                      <input type="checkbox" id="hide" name="hide" value="hide">
                      <label for="hide">Minimize this tip.</label>
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="text-center">
                    <span class="fa fa-long-arrow-right"></span>
                  </div>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-7">
                  <div class="tour-thumbnail tour-video">
                    <img src="<?php echo base_url()."assets/images/dashboard/v_page.png"; ?>" alt="Google Analytics" title="Google Analytics Guide" style="max-width: 100%;"/>
                  </div>
                </div>
              </div>
          </div>
      </div>    
        <section class="content">
          <?php $this->load->view('session/launch-view'); ?>
            <div class="container-fluid">
                <?php if( !empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret")) AND !config_item("is_paid") ) : ?>
                    <?php $this->load->view('session/launch-view'); ?>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">

                       <?php $session = $this->session->flashdata('session');
                          if( isset($session) ){?>

                            <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>

                        <?php } ?>

                        <?php if(isset($_GET["keywords"])){?>
                           <a href="<?php echo site_url("pages"); ?>" class="btn btn-default btn-submit"><i class="fa fa-arrow-left"></i> Back to page list</a>
                      <?php } else {?>
                                <a href="<?php echo site_url("pages/add"); ?>" class="btn btn-default btn-submit"><i class="fa fa-plus"></i> Add New Page</a>
                       <?php } ?>
                    </div>
                   
                    <br>
                    <br>
                    <div class="page-content">
                     <?php if(!empty($pages)) { ?>
                        <div class="col-md-12 add-page">
                          <ul class="sub-header">
                            <li class="all">
                              <a href="<?=base_url()?>pages">
                              <?php
                                if(!$this->input->get('status')) { ?>
                                  <strong>All <span class="count">(<?=isset($total_pages) ? $total_pages : 0?>)</span></strong>
                              <?php
                                } else { ?>
                                  All <span class="count">(<?=isset($total_pages) ? $total_pages : 0?>)</span>
                              <?php
                                }
                              ?>
                              </a>  |
                            </li>
                            <li class="publish">
                              <a href="<?=base_url()?>pages?status=published">
                              <?php
                                if($this->input->get('status') == 'published') { ?>
                                  <strong>Published <span class="count">(<?=isset($total_published_pages) ? $total_published_pages : 0?>)</span></strong>
                              <?php
                                } else { ?>
                                  Published <span class="count">(<?=isset($total_published_pages) ? $total_published_pages : 0?>)</span>
                              <?php
                                }
                              ?>
                              </a>  |
                            </li>
                            <li class="trash">
                              <a href="<?=base_url()?>pages/trash" class="current">
                                Trash <span class="count">(<?=$trashed_pages?>)</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                        
                        <div class="col-md-12 mb-30px" >
                            <div class="row">
                                <div class="col-lg-4 col-md-8 col-sm-7">
                                    <div class="btn-group">
                                        <form action="" method="POST" id="bulk-action">
                                             <select name="bulk_action" id="bulk-select" class="form-control">
                                                <option>Bulk Actions</option>
                                                <option value="trash">Move to Trash</option>
                                             </select>
                                         </form>
                                    </div>
                                    <div class="btn-group">
                                        <button type="submit" form="bulk-action" class="btn btn-default btn-add-featured">Apply</button>
                                    </div>
                           
                                    <div class="btn-group">
                                        <form action="" method="POST" id="filter-date">
                                             <select name="filter_month" id="select-pages" class="form-control">
                                                <option>All Dates</option>
                                                <?php if( !empty($group_published_pages) ) { 
                                                    foreach($group_published_pages as $page){?>

                                                        <option value="?filter_date=<?php echo date("Y-m", strtotime($page->post_date)); ?>"> <?php echo date("F Y", strtotime($page->post_date)); ?></option>
                                                <?php 
                                     
                                                 }

                                                    } 
                                                ?>         
                                            </select>
                                        </form>   
                                    </div>
                                    <div class="btn-group">
                                        <button type="submit" form="filter-date" class="btn btn-default btn-add-featured">Filter</button>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-5">
                                    <form action="" class="form-horizontal" method="get" >
                                        <div class="input-group">
                                            <input type="text" name="keywords" value="<?php echo (isset($_GET['keywords'])) ? $_GET['keywords'] : "" ;?>" class="form-control" placeholder="Search for..." pattern="[a-zA-Z0-9-_.,$\s]+">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-submit" type="submit">Search</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php } ?> 

                        <div class="col-md-12">
                            <table class="table table-striped table-responsive">
                                 <thead>
                                  <tr>
                                    <?php if( !empty($published_pages) ) {?>
                                      <th><label><input type="checkbox" id="checkAll"/></label></th>
                                    <?php }?>
                                    <th>Title</th>
                                    <th class="text-center">Date</th>
                                  </tr>
                                </thead>
                              <tbody>
                                <?php
                                  if(!empty($pages)) {

                                    if(!empty($published_pages)) { 

                                      foreach ($published_pages as $page) : ?> 

                                         <tr  class="<?php if ($page->slug == 'seller' || $page->slug == 'buyer') {
                                            if ($this->config->item('disallowUser')) {
                                               echo "hide";
                                            }
                                          } ?>">
                                          <td width="1%">
                                            <input type="checkbox" name="bulk_trash[]" value="<?php echo $page->id ?>" form="bulk-action">
                                          </td>
                                          <td width="90%">
                                             <?php if($page->slug == 'seller' || $page->slug == 'buyer' || $page->slug == 'contact'){?>

                                                <a href="<?= $base_url?><?php echo $page->slug?>" target="_blank"><?php echo ucwords($page->title); ?></a> 

                                            <?php } else {?>

                                                <a href="<?= $base_url?>page/<?php echo $page->slug?>" target="_blank"><?php echo ucwords($page->title); ?></a> 

                                            <?php }?>

                                            <br>
                                            <a href="<?php echo site_url('pages/edit?page_id='.$page->id); ?>">Edit</a> <span>|</span>  <a href="<?php echo site_url('pages/move_trash?page_id='.$page->id); ?>">Trash</a> <span>|</span>

                                             <?php if($page->slug == 'seller' || $page->slug == 'buyer' || $page->slug == 'contact'){?>

                                                <a href="<?= $base_url?><?php echo clean_url($page->slug)?>" target="_blank">View</a> 

                                            <?php } else {?>

                                                <a href="<?= $base_url?>page/<?php echo clean_url($page->slug)?>" target="_blank">View</a> 

                                            <?php }?>
                                          </td>
                                          <td width="10%">
                                              <p><?php echo date("F d, Y", strtotime($page->post_date)); ?> <br> <?php echo ($page->status) ? ucfirst($page->status) : "Draft"; ?></p>
                                          </td>
                                        </tr>

                                    <?php 
                                      endforeach; 
                                    }
                                  }  else { ?>
                                    <tr>
                                      <td colspan="3">
                                        <p class="text-center">No <strong><?php echo isset($_GET["keywords"]) ?  ucwords($_GET["keywords"]) : ""?></strong> page found!</p>
                                      </td>
                                    </tr>
                                <?php 
                                  } 
                                ?>
                              </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 search-pagination">
                          <div class="pull-right">
                              <ul class="pagination-list">
                                <?php echo (!empty($pages) && count($pages) > 4) ? $pagination : ''; ?>
                              </ul>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $this->load->view('session/footer'); ?>

