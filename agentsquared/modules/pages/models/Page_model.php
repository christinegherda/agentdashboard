<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Page_model extends CI_Model { 

	public function __construct()
    {
        parent::__construct();
    }
	// --------------------------------------

    public function get_all_pages($counter = FALSE,  $param = array()) {

        $this->db->select("*")
                ->from("pages");

        $sql_str = "agent_id='".$this->session->userdata('agent_id')."' AND page_type='page' ";

        if($this->input->get('status')) {
            $sql_str .= "AND status='".$this->input->get('status')."' ";
        } else {
            $sql_str .= "AND (status='published' OR status='trash') ";
        }

        if($this->input->get('keywords')) {
            $sql_str .=" AND title LIKE '%".$this->input->get('keywords')."%'";
        }

        $this->db->where($sql_str);

        if($counter) {
            return $this->db->count_all_results();
        }

        if(!empty($param["limit"])) { 
            $this->db->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);

        }  

        if($this->input->get('status')) {
            $this->db->where("status", $this->input->get("status"));
        } else {
            $this->db->where("status", "published");
            //$this->db->or_where("status", "trash");
        }

        $this->db->order_by("id", "desc");
        
        $query = $this->db->get();

        if($query->num_rows()>0) {
            return $query->result();
        }

        return FALSE;

    }

    public function get_group_published_pages( )
    {
        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
            'page_type' => 'page',
            'status' => 'published'
        );
        $year = 
        $this->db->select("post_date")
                ->from("pages")
                ->where($array)
                ->where("slug <>","home")
                ->where("slug <>","blog")
                ->where("slug <>","testimonials")
                ->where("slug <>","find-a-home")
                ->group_by ("YEAR(post_date),MONTH(post_date)")
                ->order_by("id","desc");
        
        $query = $this->db->get();


        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;
    }

    public function get_group_trashed_pages( )
    {
        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
            'page_type' => 'page',
            'status' => 'trash'
        );
        $year = 
        $this->db->select("post_date")
                ->from("pages")
                ->where($array)
                ->where("slug <>","home")
                ->where("slug <>","blog")
                ->where("slug <>","testimonials")
                ->where("slug <>","find-a-home")
                ->group_by ("YEAR(post_date),MONTH(post_date)")
                ->order_by("id","desc");
        
        $query = $this->db->get();


        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function get_all_published_pages( $counter = FALSE,  $param = array() )
    {
        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
            'page_type' => 'page',
            'status' => 'published'
        );
        $this->db->select("*")
                ->from("pages")
                ->where($array)
                ->where("slug <>","home")
                ->where("slug <>","blog")
                ->where("slug <>","testimonials")
                ->where("slug <>","find-a-home")
                ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

     public function get_agent_pages_id(){

        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
            'page_type' => 'page',
        );
        $this->db->select("id")
                ->from("pages")
                ->where($array);
        
        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

     public function get_total_pages(){

        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
            'page_type' => 'page',
        );
        $this->db->select("*")
                ->from("pages")
                ->where($array)
                ->where("slug <>","home")
                ->where("slug <>","blog")
                ->where("slug <>","testimonials")
                ->where("slug <>","find-a-home")
                ->order_by("id","desc");

        return $this->db->count_all_results();

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function get_total_published_pages()
    {
        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
            'page_type' => 'page',
            'status' => 'published'
        );
        $this->db->select("*")
                ->from("pages")
                ->where($array)
                ->where("slug <>","home")
                ->where("slug <>","blog")
                ->where("slug <>","testimonials")
                ->where("slug <>","find-a-home")
                ->order_by("id","desc");

        return $this->db->count_all_results();

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

     public function get_total_trashed_pages()
    {
        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
            'page_type' => 'page',
            'status' => 'trash'
        );
        $this->db->select("*")
                ->from("pages")
                ->where($array)
                ->where("slug <>","home")
                ->where("slug <>","blog")
                ->where("slug <>","testimonials")
                ->where("slug <>","find-a-home")
                ->order_by("id","desc");

        return $this->db->count_all_results();

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }


    public function get_all_trashed_pages( $counter = FALSE,  $param = array() )
    {
        $array = array(
            'agent_id' => $this->session->userdata('agent_id'),
            'page_type' => 'page',
            'status' => 'trash'
        );
        $this->db->select("*")
                ->from("pages")
                ->where($array)
                ->order_by("id","desc");

        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

        

        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function filter_date($counter = FALSE ,$param = array()  )
    {
        $this->db->select("*")
                ->from("pages")
                ->where("page_type" , "page")
                ->where("status" , "published")
                ->where("agent_id" , $this->session->userdata('agent_id'))
                ->where("post_date like '%".$param["filter"]."%'")
                ->where("slug <>","home")
                ->where("slug <>","blog")
                ->where("slug <>","testimonials")
                ->where("slug <>","find-a-home")
                ->order_by("id","desc");
        
        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

    
        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function filter_trash($counter = FALSE ,$param = array() )
    {
        $this->db->select("*")
                ->from("pages")
                ->where("page_type" , "page")
                ->where("status" , "trash")
                ->where("agent_id" , $this->session->userdata('agent_id'))
                ->where("post_date like '%".$param["filter"]."%'")
                ->where("slug <>","home")
                ->where("slug <>","blog")
                ->where("slug <>","testimonials")
                ->where("slug <>","find-a-home")
                ->order_by("id","desc");


        if( isset($_GET["keywords"]) )
        {
            $this->db->where(" (title like '%".$_GET["keywords"]."%') ");
        }

        if( $counter )
        {
            return $this->db->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db->offset( $param["offset"] );
        }
        
        $query = $this->db->get();

    
        if( $query->num_rows() > 0 )
        {
            return $query->result();

        }

        return FALSE;

    }

    public function add($page_id = NULL, $datas = array()) {

        $post = $this->input->post("page");
        $post["content"] = html_entity_decode($post["content"]);
        $post["post_date"] = date("Y-m-d H:i:s");
        $post["post_modified"] = date("Y-m-d H:i:s");
        $post["status"] = "published";
        $post["page_type"] = "page";
        $post["menu_type"] = "custom_menu";
        $post["slug"] = strtolower(str_replace(" ", "-", clean_url($post["title"])));
        $post["agent_id"] = $this->session->userdata('agent_id');

        $this->db->select("title")
                ->from("pages")
                ->where("agent_id", $this->session->userdata("agent_id"))
                ->where("title", $post["title"])
                ->where("status", "published")
                ->limit(1);

        $query = $this->db->get();

        return ($query->num_rows()>0) ? array("success"=>FALSE, "message"=>"Duplicate page name! Please enter another page title!") : (($this->db->insert("pages", $post)) ? array("success"=>TRUE, "id"=>$this->db->insert_id()) : array("success"=>FALSE, "message"=>"Failed to add page!")); 

    } 

    public function edit($page_id = NULL, $datas = array()) {

        $ret = array("success"=>FALSE, "message"=>"Failed to update page!");

        if($page_id) {

            $page = $this->input->post("page");
            $page["slug"] = strtolower(str_replace(" ", "-", clean_url($page["title"])));
            $page["post_modified"] = date("Y-m-d H:i:s");

            $this->db->select("slug")
                    ->from("pages")
                    ->where("id !=", $page_id)
                    ->where("title", $page["title"])
                    ->where("status!=","trash")
                    ->where("agent_id" , $this->session->userdata('agent_id'));

            $query = $this->db->get();

            if($query->num_rows()>0) {

                $ret["success"] = FALSE;
                $ret["message"] = "Failed to update: Duplicate page title!";

            } else {

                $this->db->where("id", $page_id);
                $this->db->update("pages", $page);

                $ret["success"] = ($this->db->affected_rows()>0) ? TRUE : FALSE;
                $ret["message"] = ($this->db->affected_rows()>0) ? "Page successfully updated!" : "Failed to update page!";
            }

        }

        return $ret;
        
    }

    
    public function updateImageDetails( $image_id = NULL, $datas = array() )
    {
        $media["title"] = $datas["title"];
        $media["caption"] = $datas["caption"];
        $media["link"] = $datas["link"];
        $media["alt_text"] = $datas["alt_text"];
        $media["description"] = $datas["description"];
        $media["post_modified"] = date("Y-m-d H:i:s");

        $this->db->select("title")
            ->from("media")
            ->where("id" , $image_id)
            ->where("author" , $this->session->userdata('user_id'));

        $query = $this->db->get();

        if( $this->db->update("media", $media, array("id" => $image_id) )){

            return TRUE;
        } else {

            return FALSE;
        }
    }
    
    public function insert_seller_page($agent_id) {

      $seller_page = array(
        'agent_id'      => $agent_id,
        'title'         => 'Seller',
        'slug'          => 'seller',
        'content'       => "<h3>What's My Home Worth?</h3><p>Receive a custom report showing what your home is worth, including comparable homes that have recently sold or that are currently on the market.</p><p>To find out what your home is worth, fill out the form below:</p>",
        'is_menu'        => '1',
        'menu_type'     => 'default_menu',
        'menu_order'    => '0',
        'parent_menu_id'=> '0',
        'saved_search_id'=> '',
        'page_type'     => 'page',
        'status'        => 'published',
        'post_date'     => date("Y-m-d H:i:s"),
        'post_modified' => date("Y-m-d H:i:s")
      );

      $this->db->insert('pages', $seller_page);
    }

    public function insert_buyer_page($agent_id) {

      $buyer_page = array(
        'agent_id'       => $agent_id,
        'title'          => 'Buyer',
        'slug'           => 'buyer',
        'content'        => '<h3>Buyer Page</h3><p>Buyer Page content Here</p>',
        'is_menu'        => '1',
        'menu_type'      => 'default_menu',
        'menu_order'     => '0',
        'parent_menu_id' => '0',
        'saved_search_id'=> '',
        'page_type'      => 'page',
        'status'         => 'published',
        'post_date'      => date("Y-m-d H:i:s"),
        'post_modified'  => date("Y-m-d H:i:s")
      );

      $this->db->insert('pages', $buyer_page);
    }

    public function insert_home_page($agent_id) {

      $home_page = array(
        'agent_id'      => $agent_id,
        'title'         => 'Home',
        'slug'          => 'home',
        'content'       => '',
        'is_menu'       => '1',
        'menu_type'     => 'default_menu',
        'menu_order'    => '0',
        'parent_menu_id'=> '0',
        'saved_search_id'=> '',
        'page_type'     => 'page',
        'status'        => 'published',
        'post_date'     => date("Y-m-d H:i:s"),
        'post_modified' => date("Y-m-d H:i:s")
      );

      $this->db->insert('pages', $home_page);

    }

    public function insert_find_home_page($agent_id) {

      $find_home_page = array(
        'agent_id'      => $agent_id,
        'title'         => 'Find a Home',
        'slug'          => 'find-a-home',
        'content'       => '',
        'is_menu'       => '1',
        'menu_type'     => 'default_menu',
        'menu_order'    => '0',
        'parent_menu_id'=> '0',
        'saved_search_id'=> '',
        'page_type'     => 'page',
        'status'        => 'published',
        'post_date'     => date("Y-m-d H:i:s"),
        'post_modified' => date("Y-m-d H:i:s")
      );

      $this->db->insert('pages', $find_home_page);
    }

    public function insert_contact_page($agent_id) {

      $contact_page = array(
        'agent_id'      => $agent_id,
        'title'         => 'Contact',
        'slug'          => 'contact',
        'content'       => '',
        'is_menu'       => '1',
        'menu_type'     => 'default_menu',
        'menu_order'    => '0',
        'parent_menu_id'=> '0',
        'saved_search_id'=> '',
        'page_type'     => 'page',
        'status'        => 'published',
        'post_date'     => date("Y-m-d H:i:s"),
        'post_modified' => date("Y-m-d H:i:s")
      );

      $this->db->insert('pages', $contact_page);

    }

    public function insert_blog_page($agent_id){

      $blog_page = array(
        'agent_id'      => $agent_id,
        'title'         => 'Blog',
        'slug'          => 'blog',
        'content'       => '',
        'is_menu'       => '0',
        'menu_type'     => 'default_menu',
        'menu_order'    => '0',
        'parent_menu_id'=> '0',
        'saved_search_id'=> '',
        'page_type'     => 'page',
        'status'        => 'published',
        'post_date'     => date("Y-m-d H:i:s"),
        'post_modified' => date("Y-m-d H:i:s")
      );

      $this->db->insert('pages', $blog_page);
    }

    public function insert_testimonial_page($agent_id){

      $testimonial_page = array(
        'agent_id'      => $agent_id,
        'title'         => 'Testimonials',
        'slug'          => 'testimonials',
        'content'       => '',
        'is_menu'       => '0',
        'menu_type'     => 'default_menu',
        'menu_order'    => '0',
        'parent_menu_id'=> '0',
        'saved_search_id'=> '',
        'page_type'     => 'page',
        'status'        => 'published',
        'post_date'     => date("Y-m-d H:i:s"),
        'post_modified' => date("Y-m-d H:i:s")
      );

      $this->db->insert('pages', $testimonial_page);
    }

    public function delete($page_id = NULL) {

      $this->db->where("id", $page_id);
      $this->db->delete( "pages" );

      return ($this->db->affected_rows()>0) ? TRUE : FALSE;

    }

    public function bulk_delete()
    {   
       if(gettype($_POST['bulk_action'])=="array"){
            foreach($_POST['bulk_action'] as $val){
             $menu_id = $val;
             
              $array = array(
                'id' => $menu_id,
                'agent_id' => $this->session->userdata('agent_id'),
                'page_type' => 'page'
            );

           $this->db->where($array)
                ->delete('pages');
            }

            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }
    }

    public function move_trash( $page_id = NULL )
    {
         $this->db->set('status', "trash")
            ->where("id", $page_id)
            ->update('pages'); 
       

    }

    public function restore( $page_id = NULL,$page_title = NULL )
    {
        $ret = array("success"=>FALSE, "message"=>"Failed to restore page!");

        if($page_id) {

            $this->db->select("slug")
                    ->from("pages")
                    ->where("id !=", $page_id)
                    ->where("title", $page_title)
                    ->where("status !=", "trash")
                    ->where("agent_id" , $this->session->userdata('agent_id'));

            $query = $this->db->get();

            if($query->num_rows()>0) {

                $arr = array(
                    'slug' => strtolower(str_replace(" ", "-", clean_url($page_title.'-copy'))),
                    'title' => $page_title.'-copy',
                    'status' => "published"
                );

                $this->db->where("id", $page_id);
                $this->db->where("agent_id" , $this->session->userdata('agent_id'));
                $this->db->update("pages", $arr);

                $ret["success"] = TRUE;
                $ret["message"] = "Duplicate page copy successfully restored!";

            } else {

               $this->db->set('status', "published")
                    ->where("id", $page_id)
                    ->update('pages'); 

                $ret["success"] = ($this->db->affected_rows()>0) ? TRUE : FALSE;
                $ret["message"] = ($this->db->affected_rows()>0) ? "Page successfully restored!" : "Failed to restore page!";
            }

        }

        return $ret;

    }

    public function bulk_restore()
    {   
       if(gettype($_POST['bulk_action'])=="array"){
            foreach($_POST['bulk_action'] as $val){
             $menu_id = $val;
             
              $array = array(
                'id' => $menu_id,
                'agent_id' => $this->session->userdata('agent_id'),
                'page_type' => 'page'
            );

           $this->db->set('status', "published")
                ->where($array)
                ->update('pages');
            }

            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }
    }

    public function get_page( $page_id = 0 )
    {
        $this->db->select("*")
                ->from("pages")
                ->where("id" , $page_id);

        return $this->db->get()->row_array();
    }

    public function get_search_id($page_id  = NULL)
    {
        $array = array(
            'id' => $page_id,
            'agent_id' => $this->session->userdata('agent_id')
             );
        $this->db->select("saved_search_id")
                ->from("pages")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function bulk_trash()
    {   
       if(gettype($_POST['bulk_trash'])=="array"){
            foreach($_POST['bulk_trash'] as $val){
             $menu_id = $val;
             
              $array = array(
                'id' => $menu_id,
                'agent_id' => $this->session->userdata('agent_id'),
                'page_type' => 'page'
            );

           $this->db->set('status', "trash")
                ->where($array)
                ->update('pages');
            }

            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }
    }
}


 