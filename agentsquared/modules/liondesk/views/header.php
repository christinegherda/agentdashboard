<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Website Settings | Agent Dashboard</title>
  <link rel="icon" type="image/x-icon"  href="<?php if (isset($branding->favicon) AND !empty($branding->favicon)) {?><?php echo AGENT_DASHBOARD_URL . "assets/upload/favicon/$branding->favicon"?><?php } else { ?><?php echo AGENT_DASHBOARD_URL . "assets/images/default-favicon.png"?><?php } ?>"/>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="apple-touch-icon" href="<?= base_url()?>assets/img/home/apple-touch-icon.png">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/fonts/fontello/css/fontello.css" type="text/css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/ionicons.min.css?version=<?=AS_VERSION?>">
  <!-- <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard-compressed.css.php"> -->
   <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/AdminLTE.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/skin-blue.min.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/common.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard/dashboard.css?version=<?=AS_VERSION?>">
  <link rel="stylesheet" href="/assets/css/dashboard/marketing-codes.css?version=<?=AS_VERSION?>">

</head>