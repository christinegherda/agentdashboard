<!-- Agent IDX Website -->
<div id="agent-idx-website-modal" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="col-md-offset-5">
          <div class="main-header-container">
            <h3 class="main-header">Sell More Homes</h3>
            <h4 class="main-header">Build Your Online Presence Today!</h4>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="col-md-offset-5">
          <div class="main-body">
            <h4>Websites integrate with your MLS through a direct connection in FlexMLS. Buyers can search your website in your local market with real-time, instant updates.</h4>
            <ul class="idx-modal-list">
              <li>Capture Leads</li>
              <li>Add Unlimited Pages</li>
              <li>Add Saved Searches to any page</li>
            </ul>
            <div class="pricing-section clearfix">
              <div class="pricing-header">
                <h4 class="text-center">PRICING PLANS</h4>
              </div>
              <div class="col-sm-offset-3 col-md-offset-3 col-md-6 col-sm-6 no-padding-left">
                <div class="large-monthly pricing">
                  <h1>
                    <span class="dollar">$</span>
                    99
                    <span class="permonth">/month</span>
                  </h1>
                  <p>Billed Monthly<!--  - <span class="discount">Save 20%</span> --></p>
                  <div class="bottom-trial">
                    <p>15 days FREE trial<!-- . $59 monthly until canceled. --></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <a href="http://www.sparkplatform.com/appstore/apps/instant-idx-website-powered-by-flexmls" class="btn btn-default btn-block" target="_blank">Click Here to Start Your FREE 15 Days Trial</a>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal SPW Pricing -->
<div class="modal fade" id="modalSpwPricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Purchase Single Property Website</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <!-- <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="price-item">
              <h4 class="price-title">Single Property Website</h4>
              <div class="price-desc">
                 <p><sup>$</sup>99.00</p>
                 <small>per year</small>
              </div>
              <a href="javascript:void(0)" data-type="monthly" data-price="99" data-prop-form-id="" class="price-text purchase-spw">Purchase</a>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <div class="modal-body">
          <div class="row">
            <div class="col-md-5 col-sm-5">
                <div class="price-item">
                  <h4 class="price-title">Single Property Website</h4>
                  <div class="price-desc">
                      <p><sup>$</sup>10</p>
                      <small>per month</small>
                  </div>
                  <a href="javascript:void(0)" data-type="monthly" data-price="10" data-prop-form-id="" class="price-text purchase-spw">Purchase</a>
                </div>
            </div>
            <div class="col-md-1 col-sm-1">
                <p class="or-text">OR</p>
            </div>
            <div class="col-md-5 col-sm-5">
                <div class="price-item">
                  <h4 class="price-title">Single Property Website Annual</h4>
                  <div class="price-desc">
                      <p><sup>$</sup>99</p>
                      <small>per year</small>
                  </div>
                  <a href="javascript:void(0)" data-type="annual" data-price="99" data-prop-form-id="" class="price-text purchase-spw">Purchase</a>
                </div>
            </div>
          </div>
      </div>
  </div>
</div>
</div>

<!-- Modal Payment -->
<div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-label" id="myModalLabel">Checkout</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <p class="modal-text">Review Your Order:</p>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Amount</th>
                    <th>Sub-Total</th>
                    <!-- <th></th> -->
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span id="product-name"></span></td>
                    <td>$<span id="product-amount"></span></td>
                    <td>$<span class="product-sub-amount"></span></td>
                    <!-- <td><a href="#"><i class="fa fa-close"></i></a></td> -->
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="col-md-12 text-right">
            <p class="text-total">Total:$<span class="product-sub-amount"></span></p>
          </div>
          <div class="col-md-12">
            <div class="border-gray"></div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-5">
                <p class="modal-text">Payment Method:</p>
              </div>
              <div class="col-md-6">
                <img src="<?= base_url()?>assets/images/dashboard/payment_method.png" class="cc-accepted">
              </div>
            </div>
            <div class="cc-detail">
              <h3>Credit Card Details:</h3> </br>
              <form class="form-horizontal trial_stripe_sbt" id="trial_stripe_sbt" action="" method="POST">
                <div class="col-md-12"><h3><div class="col-md-12 payment-errors label label-danger"></div></h3><br></div>
                <div class="col-md-12"><h3><div class="col-md-12 payment-message label label-success"></div></h3><br></div>

                <input type="hidden" name="product_amount" value="" class="product-mount" >
                <input type="hidden" name="product_type" value="" class="product-type" >
                <input type="hidden" name="property_id" value="" class="property_id" >

                <div class="row">
                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="cardnumber" class="col-md-4">Card number:</label>
                      <div class="col-md-8 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="number" placeholder="Card Number" required >
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="cardnumber" class="col-md-4">CVV code:</label>
                      <div class="col-md-8 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="cvc" placeholder="CVV" required >
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 row mb-15px">
                    <div class="form-group">
                      <label for="expirationdate" class="col-md-4">Expiration date:</label>
                      <div class="col-md-2 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="exp-month" placeholder="MM" required >
                      </div>
                      <div class="col-md-2 no-padding">
                        <input type="input" class="form-control" size="20" id="" data-stripe="exp-year" placeholder="YYYY" required >
                      </div>
                    </div>
                  </div>

                </div>

            </div>
          </div>
          <div class="col-md-6 text-center mclear">
            <h1 class="payment-total">$<span class="product-sub-amount"></span></h1>
            <p class="payment-total-note1">Total is inclusive of VAT</p>
            <p class="text-left payment-total-note2">
              By clicking on the 'Place Order' button at the end of the order process, you are consenting to be bound by our terms and conditions contained in these <a href="<?=base_url()?>dashboard/terms_conditions" target="_blank">Terms and Conditions</a> and appearing anywhere on AgentSquared website.
            </p>
            <!-- <a href="#" class="btn btn-border-green">Place Order</a> -->
            <button type="submit" class="btn btn-border-green" id="trial_pay_now_btn"> Place Order</button>
          </div>
          </form>
          <div class="col-md-12">
            <div class="col-md-6">
              <ul class="list-link">
                <li><a href="#" target="_blank">Disclaimer</a></li>
                <li><a href="<?=base_url()?>dashboard/privacy_policy" target="_blank">Privacy Policy</a></li>
                <li><a href="<?=base_url()?>dashboard/terms_conditions" target="_blank">Terms &amp; Conditions</a></li>
              </ul>
            </div>
            <div class="col-md-6 text-right">
              <p class="powered-by">Powered By: <img src="<?= base_url()?>assets/images/dashboard/stripe-logo.png"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Sync Update -->
<div class="modal fade" id="myModalSyncUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <p class="check-mark-icon"><i class="fa fa-check-circle-o"></i></p>
        <p>Already Updated</p>
      </div>
    </div>
  </div>
</div>
<!-- Reset password modal -->
<div id="reset_pass_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reset Password</h4>
      </div>
      <div class="modal-body">
        <?php
          //$session = $this->session->flashdata('session');
          //if(isset($session)) { ?>
            <!-- <div class="alert <?=($session['status']) ? 'alert-success' : 'alert-danger'?>" role='alert'><?= $session["message"]; ?></div> -->
        <?php //} ?>
        <div class="message_password"></div>
        <form action="<?=base_url();?>login/auth/change_password_ajax" method="POST" id="change_pass_form" role="form">
          <div class="form-group">
            <label for="default">Default Password</label> <!--(Your default password is: test1234)-->
            <input type="text" name="old" class="form-control" id="old" value="test1234">
            <!-- <input type="text" class="form-control" name="default" id="default" value="test1234" disabled/> -->
          </div>
          <div class="form-group">
            <label for="new_password">New Password</label>
            <!-- <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter New Password" required/> -->
            <input type="password" name="new" value="" class="form-control" id="new" placeholder="Enter New Password">
          </div>
          <div class="form-group">
            <label for="confirm_new_password">Confirm New Password</label>
            <input type="password" name="new_confirm" value="" class="form-control" id="new_confirm" placeholder="Confirm New Password">
            <!-- <input type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password" required/> -->
          </div>
          <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6Lf9eRsUAAAAAB5lP4Em8vm4L-c1m0gSyAKOTort"></div>
          </div>
          <button type="submit" class="btn btn-submit" id="submit_change_pass_btn">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<footer class="main-footer footer-links">
    <div class="mobile-center">
      <b>AgentSquared</b>
      <a href="https://www.facebook.com/AgentSquared/"><i class="fa fa-facebook-f"></i></a>
      <a href="https://twitter.com/agentsquared"><i class="fa fa-twitter"></i></a>
      <a href="https://plus.google.com/+Agentsquared/posts"><i class="fa fa-google"></i></a>
      <a href="https://www.linkedin.com/company/agentsquared"><i class="fa fa-linkedin"></i></a>
    </div>
  </div>
</footer>
</div>
<!-- ./wrapper -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MNK2G28"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <script src="<?= base_url()?>assets/js/dashboard/jquery.js?version=<?=AS_VERSION?>"></script>
  <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js?version=<?=AS_VERSION?>"></script>
  <script src="/assets/js/dashboard/marketing-codes.js?version=<?=AS_VERSION?>"></script>

</html>