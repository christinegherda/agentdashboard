<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>

<main class="content-wrapper">
  <h3 class="heading-title">
    <span><?php echo $title;?></span>
  </h3>
  <div id="main-content" class="content">
    <?php
      if(isset($user) && !empty($user)){
        $expiry = new DateTime($user->expires);
    ?>
      <?php
        //if(isset($contacts) && !empty($contacts) && $contacts->total > 0){
          if($this->devices->is_mobile()){
      ?>
        Not accessible through mobile device yet.
      <?php
          }else{
      ?>
        <p><a href="https://www.liondesk.com/admin/clients.html" target="_blank">Click here</a> to do more with your leads from within Liondesk platform.</p>
        <p><strong>Note:</strong> Due to Liondesk's API limitation, we can only display up to 10 contacts and up to 10 tasks per contact. To fully manage your Liondesk contacts, please <a href="https://www.liondesk.com/admin/clients.html" target="_blank">login</a> to Liondesk.</p>
        <p><em>* Syncing your customer activities from your agent site to Liondesk will only work for Premium users. I strongly suggest you take advantage of our Premium product.</em></p>
        <div style="display: flex;">
          <div id="magictable"></div>
          <div id="magiccontact"></div>
        </div>
        <div id="pagination"></div>
      <?php
          }
          //printa($contacts);
        /* }else{
      ?>
        <div style="display: flex;">
          <div id="magictable"></div>
          <div id="magiccontact"></div>
        </div>
        <div id="pagination"></div>
      <?php } */ ?>
      <table class="table" style="display: none; max-width: 500px; border: 1px solid rgba(0,0,0,0.1); border-radius: 5px;">
        <tbody>
          <tr>
            <td>Access Token</td>
            <td><?php echo $api->access_token; ?></td>
          </tr>
          <tr>
            <td>Refresh Token</td>
            <td><?php echo $api->refresh_token; ?></td>
          </tr>
          <tr>
            <td>Token Expires On</td>
            <td><?php echo $expiry->format('M d, Y @ H:i:s a T'); ?></td>
          </tr>
        </tbody>
      </table>
    <?php }else{ ?>
      <p><a href="https://api-v2.liondesk.com/oauth2/authorize?response_type=code&client_id=<?php echo LIONDESK_CLIENT_ID; ?>&scope=read%20write&redirect_uri=<?php echo LIONDESK_REDIRECT_URI; ?>">Connect with LionDesk</a></p>
    <?php } ?>
    <?php if(isset($unsynced) && $unsynced > 0){ ?>
      <p>
        <button class="btn btn-primary" id="syncToLiondesk">Sync <strong><?php echo $unsynced; ?></strong> Contacts from Agentsquared to Liondesk</button>
      </p>
    <?php } ?>
  </div>
</main>
<?php $this->load->view('session/footer'); ?>
