<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liondesk_model extends CI_Model{

  public function __construct(){
    parent::__construct();

    $this->createTable();
  }

  /**
  * Checks if agent already exists in the liondesk table
  * @param string $agent_id (required) the long id of the agent.
  */
  public function ifExist($agent_id){
    if($this->checkTable()){
      $this->db->select('id, agent_id');
      $this->db->from('liondesk');
      $this->db->where('agent_id', $agent_id);

      $query = $this->db->get()->row();

      if(!empty($query)){
        return true;
      }
    }
    return false;
  }

  /**
  * Checks if agent's liondesk access token has already expired
  * @param string $agent_id (required) the long id of the agent.
  * 
  * @return boolean True if expired, false if not. Default: true
  */
  public function ifExpired($agent_id){
    $query = $this->get($agent_id);

    if($query !== false){
      $expires = new DateTime( gmdate("Y-m-d H:i:s", time($query->expires)) );

      $now = new DateTime( gmdate("Y-m-d H:i:s") );

      if($expires->getTimestamp() > $now->getTimestamp()){
        return false;
      }

    }

    return true;

  }

  /**
  * Adds an agent into the liondesk table
  * @param string $agent_id (required) the long id of the agent.
  * @param assoc $data (required) the fields to be filled in with designated data.
  * 
  * @return boolean True if success, false if not. Default: false
  */
  public function add($agent_id, $data){

    if(!$this->ifExist($agent_id)){

      $data['agent_id'] = $agent_id;

      if($this->db->insert('liondesk', $data)){
        return true;
      }
    }
    return false;
  }

  /**
  * Updates an agent that's already in the liondesk table.
  * @param string $agent_id (required) the long id of the agent.
  * @param assoc $data (required) the fields to be filled in with designated data.
  * 
  * @return boolean True if success, false if not. Default: false
  */
  public function update($agent_id, $data){
    if($this->ifExist($agent_id)){

      $this->db->where('agent_id', $agent_id);

      if($this->db->update('liondesk', $data)){
        return true;
      }
    }
    return false;
  }

  /**
  * Get the liondesk access information from the liondesk table
  * @param string $agent_id (required) the long id of the agent.
  * 
  * @return mixed Returns an object of the row, or boolean false. Default: false
  */
  public function get($agent_id){
    if($this->checkTable()){
      $this->db->select('*');
      $this->db->from('liondesk');
      $this->db->where('agent_id', $agent_id);

      $query = $this->db->get()->row();

      if(!empty($query)){
        return $query;
      }
    }
    return false;
  }

  /**
  * Get all old contacts that was not synced over to liondesk
  * @param string $agent_id (required) Long id of agent.
  * @param boolean $minimal (optional) only return contacts primary key column for faster response.
  */
  public function getUnSyncedContacts($agent_id, $minimal = false){
    if($this->checkTable()){

      $this->db->select('id');
      if(!$minimal){
        $this->db->select('user_id');
        $this->db->select('contact_id');
        $this->db->select('agent_id');
        $this->db->select('first_name');
        $this->db->select('last_name');
        $this->db->select('email');
        $this->db->select('phone');
        $this->db->select('mobile');
        $this->db->select('address');
      }

      $this->db->from('contacts');
      $this->db->where(array(
        'agent_id' => $agent_id,
        'liondesk_contact_id' => NULL
      ));

      $query = $this->db->get();

      if($query->num_rows() > 0){
        return $query->result();
      }
    }
    return false;
  }

  /**
  * Get number of unsynced contacts from Agentsquared.
  * @param string $agent_id (required) long id of agent.
  */
  public function getAmountUnSynced($agent_id){
    if($this->checkTable()){

      $this->db->select('COUNT(id) AS total');
      $this->db->from('contacts');
      $this->db->where(array(
        'agent_id' => $agent_id,
        'liondesk_contact_id' => NULL
      ));

      $query = $this->db->get();
      if($query->num_rows() > 0){

        $result = $query->result();
        return $result[0]->total;
      }
    }
    return 0;
  }

  public function updateASContact($index, $liondesk_id){
    $this->db->where('id', $index);
    return $this->db->update('contacts', array(
      'liondesk_contact_id' => $liondesk_id,
    ));
  }

  /**
  * Checks if liondesk table already exist.
  */ 
  private function checkTable(){

    if(!$this->db->field_exists('liondesk_contact_id', 'contacts')){
      
      $this->load->dbforge();

      $this->dbforge->add_column('contacts', array(
        'liondesk_contact_id' => array(
          'type' => 'VARCHAR',
          'constraint' => '20',
        )
      ));
    }

    return $this->db->table_exists('liondesk');
  }
  
  /**
  * Creates the liondesk table if it does not exist.
  */
  private function createTable(){

    if(!$this->checkTable()){

      $this->load->dbforge();

      $this->dbforge->add_field(array(
        'id' => array(
          'type' => 'INT',
          'constraint' => '11',
          'auto_increment' => TRUE
        ),
        'agent_id' => array(
          'type' => 'VARCHAR',
          'constraint' => '100',
        ),
        'code' => array(
          'type' => 'VARCHAR',
          'constraint' => '120',
        ),
        'access_token' => array(
          'type' => 'VARCHAR',
          'constraint' => '120',
        ),
        'refresh_token' => array(
          'type' => 'VARCHAR',
          'constraint' => '120',
        ),
        'expires' => array(
          'type' => 'DATETIME',
        ),
      ));
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('liondesk');
    }
  }

}
