<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//define('LIONDESK_CLIENT_ID', '926aff1db81f5bb2979ab7965c75a319c6bd48c7');
//define('LIONDESK_CLIENT_SECRET', '038c8254f79f916a33bf758af72727f636baffe6');
define('LIONDESK_CLIENT_ID', getenv('LIONDESK_CLIENT_ID'));
define('LIONDESK_CLIENT_SECRET', getenv('LIONDESK_CLIENT_SECRET'));

if(!function_exists('returnJSON')){
  /**
  * Function to return the values as json string automatically
  * @param mixed $value (required) The value you want to convert to json and return it to requestor.
  */
  function returnJSON($value = null){
    header('Content-Type: application/json');
    print_r(json_encode($value));
    exit();
  }
}


class Liondesk extends MX_Controller {
  public $api = null;

  public function __construct(){

    $userData = $this->session->userdata('user_id');
    if(empty($userData)) {
      $this->session->set_flashdata('flash_data', 'You don\'t have access!');
      redirect('login/auth/login', 'refresh');
    }

    $this->load->library('liondesk_api');
    $this->load->library('user_agent');
    $this->load->library('devices');
    $this->load->model('liondesk_model', 'ld');

    $this->api = $this->liondesk_api;

  }

  /**
  * ===============================================================================================
  * DASHBOARD PAGES
  */

  /**
  * Main Liondesk dashboard page
  */
  public function index(){
    define('LIONDESK_REDIRECT_URI', base_url().'liondesk');

    $data = array();
    $data['title'] = 'Liondesk Leads Integration';
    $agent_id = $this->session->userdata('agent_id');
    $code = $this->input->get('code');
    
    enqueue_style('liondesk', base_url().'assets/css/dashboard/liondesk.css');
    enqueue_script('liondesk', base_url().'assets/js/dashboard/liondesk.js');

    if(!empty($code)){
      $success = $this->processToken($agent_id, $code);
      if($success !== false){
        redirect('/liondesk');
      }
    }

    $this->load->model("agent_sites/Agent_sites_model");

    $data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
    $data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

    if($user = $this->ld->get( $agent_id )){
      $this->api->loadToken($user->access_token);

      $data['user'] = $user;
      $data['contacts'] = $this->api->get('contacts?$sort[created_at]=-1');
      $data['reminder_type'] = array(
        '',
        'Email',
        'Call',
        'Text',
      );
      $data['unsynced'] = $this->ld->getAmountUnSynced($agent_id);

      add_jsvar('contacts', $data['contacts']);
      add_jsvar('unsynced', $data['unsynced']);
      add_jsvar('reminder_type', $data['reminder_type']);
    }

    $this->load->view('main', $data);
  }

  /**
  * Liondesk contacts dashboard page
  */
  public function tasks(){
    
  }

  /**
  * ===============================================================================================
  * AJAX CALLS
  */
  public function ajax(){
    if( $this->input->is_ajax_request() ){

      $ajax = new Ajax();

      returnJSON(array(
        'success' => false,
        'message' => 'No data specified.',
      ));
    }
  }

  /**
  * ===============================================================================================
  * PRIVATE PROPERTIES
  */
  /**
  * Function to process token. Made to make the controller public method simplier.
  * @param string $agent_id (required) The agent long id.
  * @param string $code (required) the code return from Liondesk OAuth process.
  * 
  * @return boolean True if success, false if not. Default: false
  */
  private function processToken($agent_id, $code){
    $success = false;

    if($this->ld->ifExpired($agent_id)){

      // get a fresh access token based on code
      $response = $this->api->requestToken(array(
        'code' => $code,
        'client_id' => LIONDESK_CLIENT_ID,
        'client_secret' => LIONDESK_CLIENT_SECRET,
        'redirect_uri' => LIONDESK_REDIRECT_URI
      ));

      if($response !== false){

        if($this->ld->ifExist( $agent_id )){

          if(!empty($response->access_token) && !empty($response->refresh_token) && !empty($response->expires)){
            $success = $this->ld->update( $agent_id, array(
              'code' => $code,
              'access_token' => $response->access_token,
              'refresh_token' => $response->refresh_token,
              'expires' => $response->expires,
            ));
          }
        }else{

          $success = $this->ld->add( $agent_id, array(
            'code' => $code,
            'access_token' => $response->access_token,
            'refresh_token' => $response->refresh_token,
            'expires' => $response->expires,
          ));

        }
      }
    }
    return $success;
  }
}

class Ajax extends Liondesk{

  public function __construct(){
    parent::__construct();

    if($user = $this->ld->get( $this->session->userdata('agent_id') )){
      $this->api->loadToken($user->access_token);
    }

    $params = $this->uri->uri_to_assoc();
    $main = $this->uri->segment(3);

    switch($main){
      case 'contact':

        if(isset($params['task'])){
          if($params['task'] > -1){
            // get specific 
            $this->getContactTask();
          }else{
            // get all tasks for this specific contact 
            $this->getContactTasks($params['contact']);
          }
        }

        break;

      case 'sync':

        $this->syncContact();

        break;

      case 'unsynced':

        $this->getUnSyncedContacts($this->session->userdata('agent_id'));

        break;

      case 'contacts':

        returnJSON(array(
          'params' => $params,
        ));

        break;
    }
  }

  public function syncContact(){
    $data = $this->getBody();

    $id = $data->id;
    unset($data->id);

    $result = $this->api->submitContact($data);

    if(!empty($result) && is_object($result)){
      if(property_exists($result, 'error')){
        
        returnJSON(array(
          'success' => false,
          'params' => $data,
          'result' => $result,
        ));
      }
      $this->ld->updateASContact($id, $result->id);
    }

    returnJSON(array(
      'success' => true,
      'params' => $data,
      'result' => $result,
    ));
  }

  public function getUnSyncedContacts($agent_id){

    $query = $this->ld->getUnSyncedContacts($agent_id);

    returnJSON(array(
      'success' => true,
      'query' => $query
    ));

  }

  public function getContactTask($contact_id, $task_id){

    $query = http_build_query(array(
      'contact_id' => $contact_id,
    ));

    $data = $this->api->get( 'tasks/'.$task_id.'?'. $query );

    returnJSON(array(
      'success' => true,
      'query' => $data,
    ));
  }

  public function getContactTasks($contact_id){

    $query = http_build_query(array(
      'contact_id' => $contact_id,
    ));

    $data = $this->api->get( 'tasks?'. $query . '&$sort[created_at]=-1' );

    returnJSON(array(
      'success' => true,
      'query' => $data,
    ));
  }

  private function getBody(){

    $params = file_get_contents('php://input');

    return json_decode($params);
  }
}
