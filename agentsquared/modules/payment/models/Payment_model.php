<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');
class Payment_model extends CI_Model { 
	public function __construct()
    {
        parent::__construct();
    }

	// --------------------------------------
    public function update_receipt( $infos = NULL )
    {
    	$receipt["agent_id"] = $this->session->userdata("user_id");
    	$receipt["receipt_details"] = $infos["description"];
    	$receipt["amount"] = $this->convert_to_cent( $infos["amount"] );
    	$receipt["transaction_details"] = $infos["balance_transaction"];
    	$receipt["currency"] = $infos["currency"];
    	$receipt["created_on"] = strtotime(date("Y-m-d H:m:s"));
    	//printA($receipt); exit;
    	if( $this->db->insert("receipt", $receipt) )
    	{
    		return $this->db->insert_id();
    	}
    	return FALSE;
    }

    private function convert_to_cent( $price = NULL )
    {
    	return ($price / 100); 
    }
    

    public function launch_site_free( $infos = array() )
    {   
        $receipt["agent_id"] = $this->session->userdata("user_id");
        $receipt["receipt_details"] = "free";
        $receipt["amount"] = "free";
        $receipt["transaction_details"] = "free";
        $receipt["created_on"] = strtotime(date("Y-m-d H:m:s"));
        //printA($receipt); exit;
        if( $this->db->insert("receipt", $receipt) )
        {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function trial_update_receipt( $receipt = NULL )
    {
                
        if( $this->db->insert("receipt", $receipt) )
        {
            return $this->db->insert_id();
        }

        return FALSE;
    }

    public function insert_to_paid_purchased( $datas = array() ){

        if( $this->db->insert("purchased_subscription", $datas) )
        {
            return $this->db->insert_id();
        }

        return FALSE;
    }

    public function update_user_trial()
    {
        $this->db->where("id", $this->session->userdata("user_id"));
        $this->db->where("agent_id", $this->session->userdata("agent_id"));

        $this->db->update("users", array("trial" => 0));

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function update_freemium_user()
    {
        $this->db->where("subscriber_user_id", $this->session->userdata("user_id"));

        $update = array(
            "plan_id" => 2,
            "require_cc_update" => 0
        );

        return $this->db->update("freemium", $update);
    }

    public function pay_setup_fee($paid = 0){
        if(!$this->db->field_exists('paid_sf', 'freemium')){
            $this->load->dbforge();

            $this->dbforge->add_column('freemium', array(
                'paid_sf' => array(
                    'type' => 'BOOLEAN',
                    'default' => 0,
                ),
            ));
        }

        $this->db->where('subscriber_user_id', $this->session->userdata("user_id"));
        $this->db->update('freemium', array( 'paid_sf' => $paid ));

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function check_setup_fee(){
        if($this->db->field_exists('paid_sf', 'freemium')){

            $this->db->where('subscriber_user_id', $this->session->userdata("user_id"));
            $result = $this->db->limit(1)->get('freemium')->row();

            if($result && $result->paid_sf){
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Update pricing data in pricing table depending on what the agent choosed in stripe plan
     *
     * @param  Int  $user_id
     * @param  Array  $update
     *
     * @return view redirect to agentsite
     */
    public function update_pricing(Int $user_id, Array $update) {

        $ret = FALSE;

        if($user_id && $update) {

            $this->db->where('user_id', $user_id);
            $this->db->update('pricing', $update);

            $ret = $this->db->affected_rows() ? TRUE : FALSE;
        }

        return $ret;

    }
}