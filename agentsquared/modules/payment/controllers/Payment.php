<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'third_party/stripe/lib/Stripe.php';

class Payment extends MX_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model("Payment_model", "payment");
        $this->load->model('idx_login/idx_model');
        $this->load->library('salesforce');
        $this->load->library('Gohighlevel');
    }

	public function pay()
	{ 
		$this->load->model("agent_sites/Agent_sites_model", "Agent_sites_model");
		$result = $this->Agent_sites_model->get_branding_infos();
		Stripe::setApiKey(getenv('STRIPE_API_KEY'));
		//Stripe::setApiKey('sk_test_wqhdjXo4OAgHexxdJTpROkyt');
		$token = $this->input->post("stripeToken");
		$total_price = $this->input->post("domain_price");
		
		try{
			$customer = Stripe_Customer::create(array(
	                'card' 		  => $token,
	                'description' => $result->first_name." ".$result->last_name." Paid for domain: ".$this->input->post("domain_name")
	            ));
			$charge = Stripe_Charge::create(array(
					'customer' => $customer->id,
					'amount' => $this->convert_cents($total_price), 
					'currency' => 'usd',
					'description' => $result->first_name." ".$result->last_name." Paid for domain:".$this->input->post("domain_name"),
					'receipt_email'			=> $this->session->userdata('email'), // add agent's email here. 
				));
			
			//update datas and receipt
			if( $this->payment->update_receipt( $charge ) )
			{
				$this->purchase_domain();
				$this->config->set_item('is_paid',TRUE);
			}

			exit( json_encode(array("success" => TRUE, "message" => "Your payment has been successfully process!" )) );  

		}
		catch (Stripe_InvalidRequestError $e)
        {
            // Invalid parameters were supplied to Stripe's API
            $response = array('success' => FALSE, 'type' => 'error', 'message' => $e->getMessage());
            exit( json_encode($response) );  
            throw new Exception($e->getMessage());
            
        }
        catch(Stripe_CardError $e)
        {
             // Since it's a decline, Stripe_CardError will be caught 
        	$response = array('success' => FALSE, 'type' => 'error', 'message' => $e->getMessage());	        
          	
          	exit( json_encode($response) ); 
	        throw new Exception($e->getMessage());	
        }
	
	}

	private function convert_cents( $price = 0 )
	{
		return ( $price * 100 );
	}

	private function purchase_domain() {	

		$this->load->model("agent_sites/Agent_sites_model", "domain");

		$response = array("success" => FALSE);

		if(isset($_POST['domain_name'])) {

			$post['domain_name'] = $_POST['domain_name'];
            $post['user_name'] = $this->session->userdata("user_id");

            if( $_POST["type"] == "spw" ) {
	          	$post['spw_username'] = $_POST['user_name'];
	          	$post['type'] = $_POST["type"];
            }

            $result = $this->domain->add_purchase_domain($post);
            
            if( $result["success"] ==  1 ) 
            {
            	//purchase to ENUM API
	        	$id = 'troymccas';
				$pw = 'b5n7RolJukp3';
				$d['status'] = 0;
				if( isset( $_POST['domain_name'] ) ) { 
					$domain_name = explode(".", $_POST['domain_name']);
					$sld = trim($domain_name[0]);
					$tld = trim($domain_name[1]);
					$apiurl = "https://reseller.enom.com/";

					//send email to admin
					$agent_name = $this->session->userdata("agentdata");
					
					$domain_array["user_name"] = $agent_name["agent_name"];
					$domain_array["domain_name"] = $_POST['domain_name'];

					$this->domain->send_notification_to_admin( $domain_array );

					//load xml document for to check if the domain is available or not
					/*$check = $apiurl.'interface.asp?command=Purchase&sld='.$sld.'&tld='.$tld.'&responsetype=xml&uid='.$id.'&pw='.$pw;
					$xmlCheck = simplexml_load_file($check); //or die("Error: Cannot create object");
					//print_r($xmlCheck ); exit;
					$rrpCode = $xmlCheck->RRPCode;*/
					$rrpCode = '200';
					if($rrpCode == '200') {						
						//update to database API REST
						//$res = $this->domain->update_agent_domain_status_post();

						// Create Domain Files

						if( !in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1')) ){ // Check if localhost
						    // not local
							// do operation

							if($post['type'] == "spw") {
								// $filedir = '/etc/httpd/spwv2/';
								// $filename = $_POST['domain_name'].'.conf';
								// shell_exec('touch '.$filedir.$filename);

								// $siteconf = fopen($filedir.$filename, "w");
								// $siteconfTXT = "<VirtualHost *:80>
								// 	Options Indexes FollowSymLinks

								// 	ServerName www.".$_POST['domain_name']."
								//     ServerAlias ".$_POST['domain_name']."
								// 	DocumentRoot /var/www/html/spw/".$_POST['site_dir']."
								// 	ErrorLog /var/www/html/spw/".$_POST['site_dir']."/error.log
								//     CustomLog /var/www/html/spw/".$_POST['site_dir']."/requests.log combined
								// </VirtualHost>";
								// fwrite($siteconf, $siteconfTXT);
								// fclose($siteconf);


							}
							else {
								// $filedir = '/etc/httpd/agent-sites-created/';
								// $filename = $_POST['domain_name'].'.conf';

								// shell_exec('touch '.$filedir.$filename);
								// shell_exec("mkdir /var/www/html/mapped-sites/domains/".$_POST['domain_name']."/");
								// shell_exec("mkdir /var/www/html/mapped-sites/domains/".$_POST['domain_name']."/public/");

								// shell_exec("cp -R /var/www/html/mapped-sites/base/agent-site-home-base/* /var/www/html/mapped-sites/domains/".$_POST['domain_name']."/public/");
								// shell_exec("cp -R /var/www/html/mapped-sites/base/agent-site-home-base/.htaccess /var/www/html/mapped-sites/domains/".$post['domain_name']."/public/");
								// /*UPDATE AGENTSQUARED FILE*/
								// $asconf = fopen("/var/www/html/mapped-sites/domains/".$_POST['domain_name']."/public/application/config/agentsquared.php", "w");
								// $asconfTXT = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
								// \$config = array(
								// 	'agent_email' => '".$this->session->userdata("email")."',
								// 	'agent_id' => '".$this->session->userdata("agent_id")."',
								// 	'agent_user_id' => '".$this->session->userdata("user_id")."',
								// 	'agent_old_last_login' => '".$this->session->userdata("old_last_login")."',
								// 	'agent_code' => '".$this->session->userdata("code")."',
								// 	'agent_identity' => '".$this->session->userdata("email")."',
								// );

								// ";
								// fwrite($asconf, $asconfTXT);
								// fclose($asconf);

								// $siteconf = fopen($filedir.$filename, "w");
								// $siteconfTXT = "<VirtualHost *:80>
								// 	Options Indexes FollowSymLinks

								// 	ServerName www.".$_POST['domain_name']."
								//     ServerAlias ".$_POST['domain_name']."
								// 	DocumentRoot /var/www/html/mapped-sites/domains/".$_POST['domain_name']."/public/
								// 	ErrorLog /var/www/html/mapped-sites/domains/".$_POST['domain_name']."/error.log
								//     CustomLog /var/www/html/mapped-sites/domains/".$_POST['domain_name']."/requests.log combined
								// </VirtualHost>";
								// fwrite($siteconf, $siteconfTXT);
								// fclose($siteconf);

								$this->create_as_domain($_POST['domain_name']);

							}
						}
					}

				}

				$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'You have successfully reserve a domain name [ '.$_POST["domain_name"].']. We will contact you as soon as possible once your domain is ready.'));

            	$response = array("success" => TRUE, 'message' => 'You have successfully reserve a domain name [ '.$_POST["domain_name"].']. We will contact you as soon as possible once your domain is ready.');
            }
            else
            {
            	$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Failed to add your custom domain. Please contact support@agentsquared.com for more information!'));
            	$response = array("success" => FALSE, 'error' => $result["error"]);
            }
		}

		return $response;
		//exit(json_encode($response));
	}


	public function pay_trial()
	{  
		$this->load->model("agent_sites/Agent_sites_model", "Agent_sites_model");
		$result = $this->Agent_sites_model->get_branding_infos();

		Stripe::setApiKey(getenv('STRIPE_API_KEY'));
		//Stripe::setApiKey('sk_test_wqhdjXo4OAgHexxdJTpROkyt');
		$token = $this->input->post("stripeToken");
		$total_price = $this->input->post("product_amount");
		$product_type = $this->input->post("product_type");
		$property_id = $this->input->post("property_id");
		
		if(!empty($property_id)) {
			$product_name = "Single Property Website";
		} else {
			$product_name = ($product_type == "monthly") ? "Instant IDX Website" : "IDX Annual";
		}

		try{
			$customer = Stripe_Customer::create(array(
	                'card' 		  => $token,
	                'email'			=> $this->session->userdata('email'),
	                'description' => $result->first_name." ".$result->last_name." [".$this->session->userdata("user_id")."] Paid for ".$product_name
	            ));

			// $charge = Stripe_Charge::create(array(
			// 		'customer' => $customer->id,
			// 		'amount' => $this->convert_cents($total_price), 
			// 		'currency' => 'usd',
			// 		'description' => $result->first_name." ".$result->last_name." Paid for ".$product_name."-".$this->session->userdata("email")
			// 	));
			
			// property id is equal to spw type
			if( !empty($property_id))
			{
				if( $product_type == "monthly" )
				{
					$subscription = Stripe_Subscription::create(array(
						'customer' => $customer->id,
						'plan' => 'P3',
					));
				}
				else
				{
					$subscription = Stripe_Subscription::create(array(
						'customer' => $customer->id,
						'plan' => 'P4',
					));
				}

			}
			else
			{
				if( $product_type == "monthly" )
				{
					$subscription = Stripe_Subscription::create(array(
						'customer' => $customer->id,
						'plan' => 'p1',
					));
				}
				else
				{
					$subscription = Stripe_Subscription::create(array(
						'customer' => $customer->id,
						'plan' => 'p2',
					));
				}
			}

			$to_update = array(
				"customer" => $customer->id,
				"amount" => $this->convert_cents($total_price), 
				"receipt_details" => $result->first_name." ".$result->last_name." Paid for ".$product_name,
				"created_on" => strtotime(date("Y-m-d H:m:s")),
				//"transaction_details" => $charge["balance_transaction"],
				"type" => (!empty($property_id)) ? "spw" : "idx",
				"agent_id" => $this->session->userdata("user_id"),
				"currency" => "usd",
				"product_type" => $product_type,
				"property_id" => $property_id,
				"sub_id" => $subscription->id,
			);

			//update datas and receipt
			$id = $this->payment->trial_update_receipt( $to_update );

			$to_insert = array(
				"receipt_id" => $id,
				"customer" => $customer->id,
				"amount" => $this->convert_cents($total_price) / 100, 
				"date_purchased" => date("Y-m-d"),
				"product_type" => $product_type
			);

			//update users table
			$this->payment->update_user_trial();

			//update to paid purchased
			$this->payment->insert_to_paid_purchased( $to_insert );

			//send upgrade email to sales
			$this->send_upgrade_email($result);

			//update Paying_Customer__c salesforce field to YES
			$this->salesforce->update($this->session->userdata('user_id'), 'Lead', array('Status'=>'Converted','Paying_Customer__c'=>'YES'));

			//create account in salesforce when leads upgraded
			if($product_name == 'Single Property Website') {
				$sf_plan = $total_price.' SPW';
			} else {
				$sf_plan = ($product_type == 'monthly') ? '$'.$total_price.' per month' : '$'.$to_insert['amount'].' per year';
			}

			$this->salesforce->lead_converter(array(
				'cc_id'	=> $this->session->userdata('user_id'),
				'plan'	=> $sf_plan,
				'date_of_upgrade'	=> date('Y-m-d')
			));

			// create purchase campaign in GHL
			$this->create_purchase_campaign($this->session->userdata('user_id'), $product_type, $sf_plan);

			exit(json_encode(array("success" => TRUE, "message" => "Your payment has been successfully process!", "property_id" => $property_id)));  

		}
		catch (Stripe_InvalidRequestError $e)
        {
            // Invalid parameters were supplied to Stripe's API
            $response = array('success' => FALSE, 'type' => 'error', 'message' => $e->getMessage());
            exit( json_encode($response) );  
            throw new Exception($e->getMessage());
            
        }
        catch(Stripe_CardError $e)
        {
             // Since it's a decline, Stripe_CardError will be caught 
        	$response = array('success' => FALSE, 'type' => 'error', 'message' => $e->getMessage());	        
          	
          	exit( json_encode($response) ); 
	        throw new Exception($e->getMessage());	
        }
	
	}

    /*
     * @Deprecated: Deprecated since we are now using single source directory 
     */
	public function create_as_domain($domain = NULL) {
		$ret = false;

		return $ret;
	}

	public function pay_freemium(){

		$this->load->model("agent_sites/Agent_sites_model", "Agent_sites_model");
		$result = $this->Agent_sites_model->get_branding_infos();

		Stripe::setApiKey(getenv('STRIPE_API_KEY'));
		//Stripe::setApiKey('sk_test_wqhdjXo4OAgHexxdJTpROkyt');
		$token = $this->input->post("stripeToken");
		$total_price = $this->input->post("product_amount");
		$product_type = $this->input->post("product_type");
		$property_id = $this->input->post("property_id");
		
		if(!empty($property_id)) {
			$product_name = "Single Property Website";
		} else {
			$product_name = ($product_type == "monthly") ? "Instant IDX Website" : "IDX Annual";
		}

		try{
			$customer = Stripe_Customer::create(array(
	                'card' 		  => $token,
	                'email'			=> $this->session->userdata('email'),
	                'description' => $result->first_name." ".$result->last_name." [".$this->session->userdata("user_id")."] Paid for ".$product_name
	            ));

			// property id is equal to spw type
			if( !empty($property_id))
			{
				//for spw
				if( $product_type == "monthly" )
				{
					$subscription = Stripe_Subscription::create(array(
						'customer' => $customer->id,
						'plan' => 'P3',
					));
				}
				else
				{
					$subscription = Stripe_Subscription::create(array(
						'customer' => $customer->id,
						'plan' => 'P4',
					));
				}

			}
			else
			{
				// for idx
				if( $product_type == "monthly" )
				{
					$subscription = Stripe_Subscription::create(array(
						'customer' => $customer->id,
						'plan' => 'aws1',
					));

					// if(!$this->payment->check_setup_fee()){

					// 	$response = Stripe_Charge::create(array(
					// 		'customer' => $customer->id,
					// 	  'amount' => 9999,
					// 	  'currency' => 'usd',
					// 	  'description' => 'One-Time Setup Fee',
					// 	  'statement_descriptor' => '99OTSFM'
					// 	));

					// 	if($response->code == 200 || $response->status == 'succeeded'){
					// 		$this->payment->pay_setup_fee(1);
					// 	}
					// }
				}
				else
				{
					$subscription = Stripe_Subscription::create(array(
						'customer' => $customer->id,
						'plan' => 'aws2',
					));
				}
			}

			$to_update = array(
				"customer" => $customer->id,
				"amount" => $this->convert_cents($total_price), 
				"receipt_details" => $result->first_name." ".$result->last_name." Paid for ".$product_name,
				"created_on" => strtotime(date("Y-m-d H:m:s")),
				//"transaction_details" => $charge["balance_transaction"],
				"type" => (!empty($property_id)) ? "spw" : "idx",
				"agent_id" => $this->session->userdata("user_id"),
				"currency" => "usd",
				"product_type" => $product_type,
				"property_id" => $property_id,
				"sub_id" => $subscription->id,
			);

			//update datas and receipt
			$id = $this->payment->trial_update_receipt( $to_update );

			//update freemuim table
			$this->payment->update_freemium_user();

			//send upgrade email to sales
			$this->send_upgrade_email($result);

			//update Paying_Customer__c salesforce field to YES
			$this->salesforce->update($this->session->userdata('user_id'), 'Lead', array('Status'=>'Converted','Paying_Customer__c'=>'YES'));
			
			//create account in salesforce when leads upgraded
			if($product_name == 'Single Property Website') {
				$sf_plan = $total_price.' SPW';
			} else {
				$sf_plan = ($product_type == 'monthly') ? '$'.$total_price.' per month' : '$'.$to_update['amount'].' per year';
			}

			$this->salesforce->lead_converter(array(
				'cc_id'	=> $this->session->userdata('user_id'),
				'plan'	=> $sf_plan,
				'date_of_upgrade'	=> date('Y-m-d')
			));

			// create purchase campaign in GHL
			$this->create_purchase_campaign($this->session->userdata('user_id'), $product_type, $sf_plan);

			exit(json_encode(array("success" => TRUE, "message" => "Your payment has been successfully process!", "property_id" => $property_id, "redirect" => base_url().'/dashboard' )));  

		}
		catch (Stripe_InvalidRequestError $e)
        {
            // Invalid parameters were supplied to Stripe's API
            $response = array('success' => FALSE, 'type' => 'error', 'message' => $e->getMessage());
            exit( json_encode($response) );  
            throw new Exception($e->getMessage());
            
        }
        catch(Stripe_CardError $e)
        {
             // Since it's a decline, Stripe_CardError will be caught 
        	$response = array('success' => FALSE, 'type' => 'error', 'message' => $e->getMessage());	        
          	
          	exit( json_encode($response) ); 
	        throw new Exception($e->getMessage());	
        }
	}

	private function send_upgrade_email($info=array()) {

		if(!empty($info)) {

			$this->load->library('email');

			if($this->input->post()) {

				$total_price 	= $this->input->post('product_amount');
				$product_type 	= $this->input->post('product_type');
				$property_id 	= $this->input->post('property_id');

				//Send Email Through Sendgrid
				$this->email->send_email_v3(
		        	'admin@agentsquared.com', //sender email
		        	'AgentSquared', //sender name
		        	'sales@agentsquared.com', //recipient email
		        	'honradokarljohn@gmail.com,rolbru12@gmail.com,paul@agentsquared.com,joce@agentsquared.com', //bccs
		        	'f724e88b-3c72-4e31-9040-9d2f4966a35a',//'562e3b7e-d87f-4681-9e5a-905a5bec803a', //template_id
		        	array(
		        		'subs' => array(
		    				'agent_name' 	=> $info->first_name.' '.$info->last_name,
		    				'agent_email'	=> $this->session->userdata('email'),
		    				'agent_id'		=> $this->session->userdata('agent_id'),
		    				'agent_phone'	=> $info->phone,
		    				'agent_mls'		=> $info->mls_name,
		    				'product_name'	=> (!empty($property_id)) ? 'Single Property Website' : (($product_type=='monthly') ? 'Instant IDX Website' : 'IDX Annual'),
		    				'product_type'	=> $product_type,
		    				'product_amount'=> (string)$total_price //float value should convert to string; else, bad request will return from sendgrid
		        		)
					)
		        );
			}

		}

		return TRUE;

	}

	/**
     * Create a purchase campaign in GoHighLevel CRM
     *   Updatet lead campaign data in GHL
     *
     * @param  String  $user_id
     * @return  void
     */
	private function create_purchase_campaign(Int $user_id, String $product_type, String $plan) {

		$ret = FALSE;

		if($user_id && $product_type && $plan) {

			$ghl = new Gohighlevel;

			$user = $ghl::agent_data($user_id);

			if($user) {

				$user['Plan__c'] = $plan;
				$ghl->process($user, $product_type);

				$ret = TRUE;

			}

		}

		return $ret;

	}

	private function create_salesforce_account($data=array()) {

		if(!empty($data)) {

			$user_data = $this->idx_model->saleforce_pre_data($this->session->userdata('agent_id'));
			
		   	if(!empty($user_data)) {

		   		$this->salesforce->create(array(
			    	'Command_Center_Id__c'				=> $this->session->userdata('user_id'),
			    	'Name' 								=> $user_data['user']['first_name'].' '. $user_data['user']['last_name'],
			    	'Email__c'							=> $user_data['user']['email'],
			    	'Phone'								=> $user_data['user']['phone'],
			    	'Company__c'						=> $user_data['user']['company'],
			    	'Plan__c'							=> $data['plan'],
			    	'Plan_Status__c'					=> 'ACTIVE',
			    	'Product__c'						=> $data['app_type'],
			    	'Subscribed_Via__c'					=> ($user_data['user']['isFromSpark']) ? 'Spark' : 'Flex',
			    	'Token_Status__c'					=> 'LIVE',
			    	'MLS_Association__c'				=> $user_data['user']['mls_name'],
			    	'Registration_Date__c'				=> date('Y-m-d', strtotime($user_data['user']['date_signed'])),
			    	'Domain_URL__c'						=> 'http://'.$user_data['user']['domains'],
			    	'Subdomain_URL__c'					=> $user_data['user']['subdomain_url'],
			    	'Facebook_Link__c'					=> $user_data['user']['facebook'],
			    	'Twitter_Link__c'					=> $user_data['user']['twitter'],
			    	'LinkedIn_Link__c'					=> $user_data['user']['linkedin'],
			    	'Google_Plus__c'					=> $user_data['user']['googleplus'],
			    	'Last_Payment_Date__c'				=> date('Y-m-d'),
			    	'Total_Properties_Sold__c'			=> $user_data['listings_scale']['total_sold'],
			    	'Total_Active_Properties__c'		=> $user_data['listings_scale']['total_active'],
			    	'Recent_Active_Property_Date__c'	=> $user_data['listings_scale']['recent_active_date'],
			    	'Recent_Sold_Property_Date__c'		=> $user_data['listings_scale']['recent_sold_date'],
			    	'Access_Token__c'					=> $user_data['user']['access_token'],
			    	'Agent_ID__c'						=> $this->session->userdata('agent_id'),
			    	'Paying_Customer__c'				=> 'YES',
			    	'Date_of_Upgrade__c' 				=> date('Y-m-d'),
		    	), 'Account');

			}

		}

		return TRUE;

	}

	/**
     * Process agent's 15 day trial subscription
     *
     * Create customer and subscription in stripe
     *
     * @param  array $_POST data
     * @return view redirect to agentsite
     */
	public function create_stripe_subscription() {

		if(!$this->input->post('stripeToken')) {
			exit(json_encode(['success' => FALSE, 'message' => 'stripe token not found!']));
		}

		if(!$this->input->post('product_amount') || !$this->input->post('product_type')) {
			exit(json_encode(['success' => FALSE, 'message' => 'product amount or product plan not defined!']));
		}

		$stripeToken = $this->input->post('stripeToken');
		$amount = $this->input->post('product_amount');
		$plan = $this->input->post('product_type');
		$email = $this->input->post('email');
		$name = $this->input->post('name');
		$agent_id = $this->input->post('agent_id');
		$subdomain_url = $this->input->post('subdomain_url');

		Stripe::setApiKey('sk_test_HLH5sWFow4y98oKSECNg4Qf2');

		try {

			$customer = Stripe_Customer::create(array(
				'card' => $stripeToken,
				'email' => $email,
				'description' => $name.'\'s IDX Website'
			));

			if($customer) {

				$subscription = Stripe_Subscription::create(array(
					'customer' => $customer->id,
					'plan' => 'plan_Eif3qf08U0NqQI',//($plan === 'monthly') ? 'aws1' : 'aws2',
					'trial_period_days' => 15
				));

				if($subscription) {

					$stripe = array(
						'agent_id' => $agent_id,
						'customer' => $customer->id,
						'sub_id' => $subscription->id,
						'amount' => $this->convert_cents($amount),
						'receipt_details' => $customer->description,
						'created_on' => $subscription->created,
						'type' => 'idx',
						'currency' => 'usd',
						'product_type' => $plan
					);

					if($this->payment->trial_update_receipt($stripe)) {
						$this->payment->update_pricing($agent_id, ['price' => $amount, 'plan' => $plan]);
						exit(json_encode(['success' => TRUE, 'redirect' => $subdomain_url])); 
					}

				}

			}

		} catch (Stripe_InvalidRequestError $e) {

			$response = array('success' => FALSE, 'type' => 'error', 'message' => $e->getMessage());
			exit(json_encode($response));
			throw new Exception($e->getMessage());

		} catch (Stripe_CardError $e) {

			$response = array('success' => FALSE, 'type' => 'error', 'message' => $e->getMessage());
			exit(json_encode($response));
			throw new Exception($e->getMessage());

		}

	}

}
