<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login extends CI_Controller {

	public function __construct() {

        parent::__construct();

        $this->load->model("Agent_model");

	}

	public function index() {
		$this->load->view('admin_login_view');
	}

	public function auth() {

		if($this->input->is_ajax_request()) {

			if($this->input->post('username') && $this->input->post('password')) {

				$data['username'] = $this->input->post('username');
				$data['password'] = $this->input->post('password');

				$is_auth = $this->Agent_model->auth_admin($data);

				$response['success'] = ($is_auth) ? TRUE : FALSE;
				$response['message'] = ($is_auth) ? "Successfully logged in!" : "Invalid log in details!";

				exit(json_encode($response));
			}

		}

	}

	public function test() {
		echo password_hash("=dB\V)5L-zF9YRaH", PASSWORD_DEFAULT);
	}

}

?>