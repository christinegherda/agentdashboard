<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super Class
 *
 * @package     Package Name
 * @subpackage  Subpackage
 * @category    Category
 * @author      Karl John Honrado
 * @link        http://example.com
 */

class Agent extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->library('email');
		$this->load->library('salesforce');
		$this->load->model('Agent_model');

	}

	/**
	 * Send email notification to agent that his/her custom domain is now up and runnning
	 *  Update salesforce data
	 *
	 * @param       string  $agent_id    string $status
	 * @return      boolean
	 */
	public function domain($agent_id=NULL, $status=NULL) { // Send autocompleted domains to agent

		if(!empty($agent_id) && !empty($status)) {

			if($status==='completed') {

				$agent_domain = $this->Agent_model->agent_domain_data($agent_id);

				if(!empty($agent_domain)) {

					//Update Salesforce Data
					$lead = $this->salesforce->update($agent_domain->id, 'Lead', array('Domain_URL__c' => 'http://'.$agent_domain->domains));
					$account = $this->salesforce->update($agent_domain->id, 'Account', array('Domain_URL__c' => 'http://'.$agent_domain->domains));

					//Send Email Through Sendgrid
					$this->email->send_email_v3(
				    	'support@agentsquared.com', //sender email
				    	'AgentSquared', //sender name
				    	$agent_domain->email, //recipient email
				    	'paul@agentsquared.com,joce@agentsquared.com,aldwinj12345@gmail.com,raul@agentsquared.com', //bccs
				    	'f2611d53-b3a5-4ad2-a8a8-0a8f12042bbf', //'3fefb4e7-55fa-4d5b-af73-a4633b66ab11', //template_id
				    	$email_data = array(
				    		'subs' => array(
								'first_name' 	=> $agent_domain->first_name,
								'custom_domain'	=> $agent_domain->domains,
								'redirect_url'	=> getenv('AGENT_DASHBOARD_URL').'cron/redirect_to_pages?id='.$agent_domain->id
				    		)
						)
				    );

					return TRUE;

				}
				
			}

		}

		return FALSE;

	}

	/**
	 * Stripe webhook method (call when stripe cancellation is triggered)
	 *
	 * @param json 
	 * @return (void)
	 */
	public function downgrade_subscription() {

		$input = @file_get_contents('php://input');
		$event_json = json_decode($input);
		
        if(!empty($event_json) && isset($event_json->data->object->items->data[0]->subscription)) {
            
            // Do something with $event_json
            $sub_id = $event_json->data->object->items->data[0]->subscription;
            $user_id = $this->Agent_model->downGradeSubscription($sub_id);

            http_response_code(200); // PHP 5.4 or greater

            exit(json_encode(array('post'=>$event_json, 'get'=>$event_json)));  

        }   
		
	}

	/**
	 * Stripe webhook method (call when stripe trial subscription has ended)
	 *
	 * @param json 
	 * @return (void)
	 */
	public function upgrade_subscription() {

		$input = @file_get_contents('php://input');
		$event_json = json_decode($input);

		if(!empty($event_json) && isset($event_json->data->object->subscription)) {

			// Do something with $event_json
			$sub_id = $event_json->data->object->subscription;

			$this->Agent_model->upgradeSubscription($sub_id);

			http_response_code(200); // PHP 5.4 or greater
			exit(json_encode(array('post'=>$event_json, 'get'=>$event_json)));  

       	}

	}

	/**
	 * Stripe webhook method (send notification email to agent that trial will end in 3 days)
	 *
	 * @param json 
	 * @return (void)
	 */
	public function trial_ending_notification() {

		$input = @file_get_contents('php://input');
		$event_json = json_decode($input);

		if(!empty($event_json) && isset($event_json->data->object->items->data[0]->subscription)) {
            
            // Do something with $event_json
            $sub_id = $event_json->data->object->items->data[0]->subscription;
            
       	}

	}

	/**
	 * Stripe webhook method (call when stripe expired cc event is triggered)
	 *
	 * @param json
	 * @return (void)
	 */
	public function expired_cc() {

		$input = @file_get_contents('php://input');
		$event_json = json_decode($input);

        if(!empty($event_json) && isset($event_json->data->object->owner->email)) {

			// Do something with $event_json
			$this->Agent_model->expiredCCData($event_json->data->object->owner->email);
            http_response_code(200); // PHP 5.4 or greater

            exit(json_encode(array('post'=>$event_json, 'get'=>$event_json)));

		}

	}

}