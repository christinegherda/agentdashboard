<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function change_existing_domain_old() {

		$this->load->view('change-existing-domain-aws');
	}

	public function change_existing_domain() {
		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Agent_model");


		$data["agent"] = $this->Agent_model->getAgent($this->session->userdata('user_id'));
		$data['cname'] = $this->Agent_model->getCname($this->session->userdata('user_id'));
		$this->load->view('change-existing-domain-aws',$data);
	}

	public function do_env(){
		echo "here <br />";
		echo "<br />";
		echo "SPARK_CLIENT_ID = ".getenv('SPARK_CLIENT_ID');
		echo "<br />";
		echo "SPARK_CLIENT_SECRET = ".getenv('SPARK_CLIENT_SECRET');
		echo "<br />";
		echo "AGENT_DASHBOARD_URL = ".getenv('AGENT_DASHBOARD_URL');
	}

}
