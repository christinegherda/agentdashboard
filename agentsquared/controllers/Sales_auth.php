<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_auth extends MX_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('landing_page/registration_model', 'reg_model');
    $this->load->model("Salesauth_model", "sales_model");
  }

  public function authenticate($agent_id="", $token=""){
    
    if(isset($_GET["agent_id"])){
      $agent_id = $_GET["agent_id"];
      $token = $_GET["token"];
    }
    

    if(empty($agent_id)) exit(json_encode(array("status" => FALSE, "message" => "Agent ID is empty")));

    unset($this->session->userdata);

    $is_agent = $this->sales_model->is_agent($agent_id , $token);
    
    if(!$is_agent)
    {
      exit(json_encode(array("status" => FALSE, "message" => "Agent is not exist in our database")));
    }

    $user_info = $this->sales_model->get_user_info($agent_id);

    if(!empty($user_info)) {

      $this->set_session($user_info);

      $this->session->set_userdata('logged_in_auth', true);

      $this->load->library("idx_auth", $this->session->userdata('agent_id'));

      if($this->idx_auth->isTokenValid()) {
        
        $row = $this->reg_model->check_sync_status($this->session->userdata('agent_id'));

        if($row->sync_status) {

          $meta = modules::load('mysql_properties/Mysql_properties/');
          $account_information = $meta->get_account($this->session->userdata('user_id'), 'account_info');

          $agent_arr = array(
            'agent_name'    => (isset($account_information->FirstName) && ($account_information->FirstName)) ? $account_information->FirstName : 'Admin',
            'agent_photo' => (isset($account_information->Images[0]->Uri) && ($account_information->Images[0]->Uri)) ? $account_information->Images[0]->Uri : AGENT_DASHBOARD_URL . 'assets/images/no-profile-img.gif',
            'site_title'  => (isset($row->site_name) && ($row->site_name)) ? $row->site_name : 'Your Website',
          );

          $this->session->set_userdata('agentdata', $agent_arr);

          $subscription = Modules::run('idx_login/agent_subscription', $this->session->userdata('agent_id'));

          if($subscription == "idx") {

            Modules::run('idx_login/agent_listings_updater', array('user_id'=>$user_info->id, 'agent_id'=>$agent_id, 'access_token'=>$token));
            redirect('dashboard', 'refresh');

          } elseif($subscription == "spw") {

            redirect('single_property_listings', 'refresh');

          } else {
            $data['message'] = "You do not have authority to login.";
            $logout = $this->ion_auth->logout();
            $this->_render_page('login/auth/login', $data);
          }

        } else {
          $obj = modules::load('idx_login/Idx_login/');
          $obj->resync_data_view($this->session->userdata('user_id'), $this->session->userdata('agent_id'));
        }

      } else {
        $this->unset_session($this->session->userdata()); // destroy user session if access token is invalid
        exit(json_encode(array("status"=>FALSE, "message"=>"Access Denied: The user's access token is no longer valid!")));
      }

    } else {
      exit(json_encode(array("status"=>FALSE, "message"=>"User not found in database!")));
    }

  }

  public function authenticate_saleforce() {

    if(isset($_GET['token']) && !empty($_GET['token'])) {

      $token    = trim($_GET['token']);
      $decrypt  = base64_decode($token);
      $param    = explode(' ', $decrypt);

      if(count($param) != 3) {
        die('Invalid token provided!');
      } else {

        $code     = $param[0];
        $agent_id = $param[1];
        $user_id  = $param[2];

        $cond_param = array(
          'id'      => $user_id,
          'agent_id'=> $agent_id,
          'code'    => $code
        );

        $user = $this->sales_model->authenticate_salesforce($cond_param);

        if(!empty($user)) {
          $this->set_session((object) array(
            'id'        => $user_id,
            'agent_id'  => $agent_id,
            'email'     => $user->email,
            'last_login'=> $user->last_login,
            'code'      => $code
          ));
          $this->session->set_userdata('logged_in_auth', true);
          redirect('dashboard', 'refresh');
        } else {
          die('User does not exist with your token provided!');
        }
      }
    } else {
      die('Token Not Provided.');
    }

  }

  public function set_session($user) {

    $session_data = array(
      'identity'             => $user->email,
      'email'                => $user->email,
      'user_id'              => $user->id, //everyone likes to overwrite id so we'll use user_id
      'old_last_login'       => $user->last_login,
      'code'                 => $user->code,
      'agent_id'             => $user->agent_id
    );

    $this->session->set_userdata($session_data);

    return TRUE;
  }

  /**
   * Destroy sessions dynamically
   *
   * @param  Array  $data
   * @return  Boolean
  */
  private function unset_session(Array $data) {

    foreach($data as $key => $val) {
      $this->session->unset_userdata($key);
    }

    return TRUE;

  }

  public function agent_scale() {
    
    if(isset($_GET['agent_id']) && !empty($_GET['agent_id'])) {

      $agent_id = $_GET['agent_id'];

      $this->load->library('idx_auth', $agent_id);

      $cancel = $this->sales_model->get_cancel_info($agent_id);

      $agent_data['cancel_reason'] = isset($cancel->cancel_reason) ? $cancel->cancel_reason : '';
      $agent_data['active_listings'] = $this->get_active();
      $agent_data['sold_listings'] = $this->get_sold();

      exit(json_encode($agent_data));
    }

    return FALSE;
    
  }

  private function get_active() {

    $params = array(
      '_pagination' => 1,
      '_page'       => 1,
      '_limit'      => 1,
      '_orderby'    => '-OnMarketDate'
    );
    
    $active = $this->idx_auth->GetMyListings($params);

    $data['total']                  = (isset($active['pagination']->TotalRows) && !empty($active['pagination']->TotalRows)) ? $active['pagination']->TotalRows : 0;
    $data['recent_active_date']     = (isset($active['results'][0]->StandardFields->OnMarketDate)) ? date("F d, Y", strtotime($active['results'][0]->StandardFields->OnMarketDate)) : '';
    $data['recent_active_listing']  = (isset($active['results'][0]->StandardFields->UnparsedAddress)) ? $active['results'][0]->StandardFields->UnparsedAddress : '';

    return $data;

  }

  private function get_sold() {

    $filterStr = "(MlsStatus Eq 'Closed' Or MlsStatus Eq 'Sold' Or MajorChangeType Eq 'Closed')";
    $filter_url = rawurlencode($filterStr);
    $filter_string = substr($filter_url, 3, -3);

    $param = array(
      '_pagination' => 1,
      '_page'       => 1,
      '_limit'      => 1,
      '_filter'     => $filter_string,
      '_orderby'    => '-CloseDate'
    );

    $sold = $this->idx_auth->GetMyListings($param);

    $data['total']                = (isset($sold['pagination']->TotalRows) && !empty($sold['pagination']->TotalRows)) ? $sold['pagination']->TotalRows : 0;
    $data['recent_sold_date']     = (isset($sold['results'][0]->StandardFields->CloseDate)) ? date("F d, Y", strtotime($sold['results'][0]->StandardFields->CloseDate)) : '';
    $data['recent_sold_listing']  = (isset($sold['results'][0]->StandardFields->UnparsedAddress)) ? $sold['results'][0]->StandardFields->UnparsedAddress : '';

    return $data;

  }

  public function delete_rules_sendgrid(){

    $results = $this->sales_model->get_all_sendgrid_rule_id();
    
    if ($results) {
      
      foreach ($results as $key) {
        echo $key->rule_id.', </br>';
        $data = json_decode(sendgrid_curl_delete_rule_id("https://api.sendgrid.com/v3/access_settings/whitelist/".$key->rule_id));
        printA($data); exit;
        //$this->sales_model->remove_sendgrid_rule_id($row->rule_id);

        //sleep(10);
      }
      echo 'done';
      return;
          
    }
  }

  public function login_update_cc() {

    if(isset($_GET['token']) && !empty($_GET['token'])) {

      $token    = trim($_GET['token']);
      $decrypt  = base64_decode($token);
      $param    = explode(' ', $decrypt);

      if(count($param) != 3) {
        die('Invalid token provided!');
      } else {

        $code     = $param[0];
        $agent_id = $param[1];
        $user_id  = $param[2];

        $cond_param = array(
          'id'      => $user_id,
          'agent_id'=> $agent_id,
          'code'    => $code
        );

        $user = $this->sales_model->authenticate_sendgrid_email($cond_param);

        if(!empty($user)) {

          $this->set_session((object) array(
            'id'        => $user_id,
            'agent_id'  => $agent_id,
            'email'     => $user->email,
            'last_login'=> $user->last_login,
            'code'      => $code
          ));

          $this->session->set_userdata('logged_in_auth', true);

          redirect('dashboard?updatecc=1', 'refresh');

        } else {
          die('User does not exist with your token provided!');
        }
      }
    } else {
      die('Token Not Provided.');
    }
    
  }

}