<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model("Agent_model");
	}
	public function index() {
		if($this->input->get('ApplicationName')) {
			$data = array(
			   'ApplicationName'	=> $this->input->get('ApplicationName'),
			   'ApplicationUri' 	=> $this->input->get('ApplicationUri'),
			   'CallbackName' 		=> $this->input->get('CallbackName'),
			   'EventType'	 		=> $this->input->get('EventType'),
			   'PricingPlan' 		=> $this->input->get('PricingPlan'),
			   'TestOnly' 			=> $this->input->get('TestOnly'),
			   'Timestamp' 			=> $this->input->get('Timestamp'),
			   'UserEmail' 			=> $this->input->get('UserEmail'),
			   'UserId' 			=> $this->input->get('UserId'),
			   'UserName' 			=> $this->input->get('UserName')
			);

			$this->db->insert('agent_subscription', $data); 

			$url = "http://admindashboard.agentsquared.com/api/Mail/send_email_to_agentsquared_admin_post/format/json";

			$post['UserId'] = $this->input->get('UserId');
			$post['UserName'] = $this->input->get('UserName');
			$post['UserEmail'] = $this->input->get('UserEmail');
			$post['TestOnly'] = $this->input->get('TestOnly');
			$post['PricingPlan'] = $this->input->get('PricingPlan');
			$post['ApplicationName'] = $this->input->get('ApplicationName');

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL,$url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

		}
		else{
			echo "Invalid Application";
		}
	}

	public function reviewed() {
		if( $this->input->post('ApplicationName')) {
			$data = array(
			   'ApplicationName' 	=> $this->input->post('ApplicationName'),
			   'ApplicationUri' 	=> $this->input->post('ApplicationUri'),
			   'CallbackName'		=> $this->input->post('CallbackName'),
			   'EventType' 			=> $this->input->post('EventType'),
			   'PricingPlan'	 	=> $this->input->post('PricingPlan'),
			   'TestOnly'	 		=> $this->input->post('TestOnly'),
			   'Timestamp' 			=> $this->input->post('Timestamp'),
			   'UserEmail' 			=> $this->input->post('UserEmail'),
			   'UserId' 			=> $this->input->post('UserId'),
			   'UserName' 			=> $this->input->post('UserName'),
			   'ApplicationType' 	=> 'Single_Property_Website',
			);

			$this->db->insert('agent_subscription', $data); 
		}
		else{
			echo "Invalid Application";
		}
	}

	public function cancelled() {
		if($this->input->post('ApplicationName')) {
			$data = array(
			   'ApplicationName' 	=> $this->input->post('ApplicationName'),
			   'ApplicationUri'	 	=> $this->input->post('ApplicationUri'),
			   'CallbackName' 		=> $this->input->post('CallbackName'),
			   'EventType' 			=> $this->input->post('EventType'),
			   'PricingPlan' 		=> $this->input->post('PricingPlan'),
			   'TestOnly' 			=> $this->input->post('TestOnly'),
			   'Timestamp' 			=> $this->input->post('Timestamp'),
			   'UserEmail' 			=> $this->input->post('UserEmail'),
			   'UserId'		 		=> $this->input->post('UserId'),
			   'UserName' 			=> $this->input->post('UserName'),
			   'ApplicationType' 	=> 'Single_Property_Website',
			);

			$arr = array(
				//'ApplicationName' 	=> $this->input->post('ApplicationName'),
				'UserEmail' 		=> $this->input->post('UserEmail'),
				'UserId' 			=> $this->input->post('UserId'),
				'EventType' 		=> 'purchased',
				'ApplicationType' 	=> 'Single_Property_Website',
			);

			$this->db->where($arr);
			$this->db->update('agent_subscription', $data);
		}
		else{
			echo "Invalid Application";
		}
	}

	public function single_property_website() {

		if($this->input->get('ApplicationName')) {

			$data = array(
			   'ApplicationName' 	=> $this->input->get('ApplicationName'),
			   'ApplicationUri' 	=> $this->input->get('ApplicationUri'),
			   'CallbackName' 		=> $this->input->get('CallbackName'),
			   'EventType' 			=> $this->input->get('EventType'),
			   'PricingPlan' 		=> $this->input->get('PricingPlan'),
			   'TestOnly' 			=> $this->input->get('TestOnly'),
			   'Timestamp' 			=> $this->input->get('Timestamp'),
			   'UserEmail' 			=> $this->input->get('UserEmail'),
			   'UserId' 			=> $this->input->get('UserId'),
			   'UserName' 			=> $this->input->get('UserName'),
			   'ApplicationType' 	=> 'Single_Property_Website',
			);

			//Check if the agent already purchased
			$arr = array(
				'EventType'	 		=> 'purchased',
				'UserEmail' 		=> $this->input->get('UserEmail'),
				'UserId' 			=> $this->input->get('UserId'),
				'ApplicationType' 	=> 'Single_Property_Website',
			);

			$agent_already_purchased = $this->Agent_model->check_agent_subscription($arr);

			if($agent_already_purchased) {
				$this->Agent_model->update_agent_subscription($arr, $data);
			} else {
				$this->db->insert('agent_subscription', $data); 
			}

			$url = "http://admindashboard.agentsquared.com/api/Mail/spw_send_email_to_agentsquared_admin/format/json";

			$post['UserId'] = $this->input->get('UserId');
			$post['UserName'] = $this->input->get('UserName');
			$post['UserEmail'] = $this->input->get('UserEmail');
			$post['TestOnly'] = $this->input->get('TestOnly');
			$post['PricingPlan'] = $this->input->get('PricingPlan');
			$post['ApplicationName'] = $this->input->get('ApplicationName');

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL,$url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

		} else {

			echo "Invalid Application";

		}
	}

	public function agent_idx() {

		if($this->input->get('ApplicationName')) {

			$this->load->library('email');

			$data = array(
			  	'ApplicationName' 	=> $this->input->get('ApplicationName'),
				'ApplicationUri' 	=> $this->input->get('ApplicationUri'),
				'CallbackName' 		=> $this->input->get('CallbackName'),
				'EventType' 		=> $this->input->get('EventType'),
				'PricingPlan' 		=> $this->input->get('PricingPlan'),
				'TestOnly' 			=> $this->input->get('TestOnly'),
				'Timestamp' 		=> $this->input->get('Timestamp'),
				'UserEmail' 		=> $this->input->get('UserEmail'),
				'UserId' 			=> $this->input->get('UserId'),
				'UserName' 			=> $this->input->get('UserName'),
				'ApplicationType' 	=> 'Agent_IDX_Website',
				'LaunchFrom'		=> 'Spark'
			);
			
			$arr = array(
				'UserEmail' 		=> $this->input->get('UserEmail'),
				'UserId' 			=> $this->input->get('UserId'),
				'EventType' 		=> 'canceled',
				'ApplicationType' 	=> 'Agent_IDX_Website',
			);

            $agent_already_purchased = $this->Agent_model->check_agent_subscription($arr);

			if($agent_already_purchased) {
				$this->Agent_model->update_agent_subscription($arr, $data);
			} else {
				$this->db->insert('agent_subscription', $data); 
			}

			//Send new purchased notification to sales
			$this->email->send_email_v3(
	        	'support@agentsquared.com', //sender email
	        	'AgentSquared', //sender name
	        	'joce@agentsquared.com', //recipient email
	        	'honradokarljohn@gmail.com,aldwinj12345@gmail.com', //bccs
	        	'a050d096-a23c-4cde-b43b-5a47ad9b420e', //'6ea9410b-22ec-4565-a053-4b60f9cc58b3', //template_id
	        	$email_data = array(
	        		'subs' => array(
	    				'user_id' 			=> $this->input->get('UserId'),
	    				'user_name' 		=> $this->input->get('UserName'),
	    				'user_email' 		=> $this->input->get('UserEmail'),
	    				'test_only'			=> $this->input->get('TestOnly'),
	    				'pricing_plan' 		=> $this->input->get('PricingPlan'),
	    				'application_name'	=> $this->input->get('ApplicationName')
	        		)
				)
	        );

			//send welcome email to purchased user
			$this->email->send_email_v3(
	        	'support@agentsquared.com', //sender email
	        	'AgentSquared', //sender name
	        	$this->input->get('UserEmail'), //recipient email
	        	'honradokarljohn@gmail.com,joce@agentsquared.com,aldwinj12345@gmail.com', //bccs
	        	'7cf1a9fe-35fb-461b-a62a-43cca08a1d01', //'ae7fddf9-f86c-4fa6-bfc7-a21dbe41ab9f', //template_id
	        	$email_data = array(
	        		'subs' => array(
	    				'first_name' => $this->input->get('UserName'),
	    				'click_here' => getenv('AGENT_DASHBOARD_URL').'idx_login?from=spark'
	        		)
				)
	        );

		} else {
			die("Invalid Application");
		}
	}

	public function cancel_agent_idx() {

		if($this->input->post('UserId')) {

			$data = array(
			   'ApplicationName' 	=> $this->input->post('ApplicationName'),
			   'ApplicationUri' 	=> $this->input->post('ApplicationUri'),
			   'CallbackName' 		=> $this->input->post('CallbackName'),
			   'EventType' 			=> $this->input->post('EventType'),
			   'PricingPlan' 		=> $this->input->post('PricingPlan'),
			   'TestOnly' 			=> $this->input->post('TestOnly'),
			   'Timestamp' 			=> $this->input->post('Timestamp'),
			   'UserEmail' 			=> $this->input->post('UserEmail'),
			   'UserId' 			=> $this->input->post('UserId'),
			   'UserName' 			=> $this->input->post('UserName'),
			   'ApplicationType' 	=> 'Agent_IDX_Website',
			);

			$arr = array(
				'UserId' 			=> $this->input->post('UserId'),
				'EventType' 		=> 'purchased',
				'ApplicationType' 	=> 'Agent_IDX_Website',
			);

			$agent_info = $this->Agent_model->check_agent_subscription($arr);

			if($agent_info) {

				$account_meta = $this->Agent_model->get_account_meta(array('user_id'=>$agent_info->id, 'type'=>'account_info'));

				if($account_meta) {
					$account_meta = gzdecode($account_meta->account);
					$account_meta = json_decode($account_meta);
				}

				$data['UserEmail'] = ($this->input->post('UserEmail')) ? $this->input->post('UserEmail') : $agent_info->UserEmail;
				$this->Agent_model->update_agent_subscription($arr, $data);

			} else {

				$data['UserEmail'] = ($this->input->post('UserEmail')) ? $this->input->post('UserEmail') : 'Not Specified';
				$this->db->insert('agent_subscription', $data); 

			}

			$post['UserId'] = $this->input->post('UserId');
			$post['FirstName'] = ($agent_info->first_name) ? $agent_info->first_name : ((isset($account_meta->FirstName) && !empty($account_meta->FirstName)) ? $account_meta->FirstName : 'Not Specified');
			$post['LastName'] = ($agent_info->last_name) ? $agent_info->last_name : ((isset($account_meta->LastName) && !empty($account_meta->LastName)) ? $account_meta->LastName : 'Not Specified');
			$post['UserEmail'] = ($this->input->post('UserEmail')) ? $this->input->post('UserEmail') : ((isset($account_meta->Emails[0]->Address) && !empty($account_meta->Emails[0]->Address)) ? $account_meta->Emails[0]->Address : 'Not Specified') ;
			$post['PhoneNumber'] = ($agent_info->phone) ? $agent_info->phone : ((isset($account_meta->Phones[0]->Number) && !empty($account_meta->Phones[0]->Number)) ? $account_meta->Phones[0]->Number : 'Not Specified');
			$post['Mls'] = (isset($account_meta->Mls) && !empty($account_meta->Mls)) ? $account_meta->Mls : 'Not Specified';
			$post['UserName'] = $this->input->post('UserName');
			$post['Website'] = ($agent_info->domains) ? $agent_info->domains : 'Not Specified';
			$post['PricingPlan'] = $this->input->post('PricingPlan');
			$post['ApplicationName'] = $this->input->post('ApplicationName');

			$url = "http://admindashboard.agentsquared.com/api/Mail/test_sendMe/format/json";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL,$url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

		}
	}

	public function test_cancel() {

		$arr = array(
			'UserEmail' 		=> 'info@flexmls.com',
			'UserId' 			=> '20151119193022123409000000',
			'EventType' 		=> 'purchased',
			'ApplicationType' 	=> 'Agent_IDX_Website',
		);

		$agent_info = $this->Agent_model->check_agent_subscription($arr);

		if($agent_info) {

			$account_meta = $this->Agent_model->get_account_meta(array('user_id'=>$agent_info->id, 'type'=>'account_info'));

			if($account_meta) {

				$account_meta = gzdecode($account_meta->account);
				$account_meta = json_decode($account_meta);
			
			}

			$post['UserId'] = $this->input->post('UserId');
			$post['FirstName'] = ($agent_info->first_name) ? $agent_info->first_name : ((isset($account_meta->FirstName) && !empty($account_meta->FirstName)) ? $account_meta->FirstName : 'Not Specified');
			$post['LastName'] = ($agent_info->last_name) ? $agent_info->last_name : ((isset($account_meta->LastName) && !empty($account_meta->LastName)) ? $account_meta->LastName : 'Not Specified');
			$post['UserEmail'] = ($this->input->post('UserEmail')) ? $this->input->post('UserEmail') : ((isset($account_meta->Emails[0]->Address) && !empty($account_meta->Emails[0]->Address)) ? $account_meta->Emails[0]->Address : 'Not Specified') ;
			$post['PhoneNumber'] = ($agent_info->phone) ? $agent_info->phone : ((isset($account_meta->Phones[0]->Number) && !empty($account_meta->Phones[0]->Number)) ? $account_meta->Phones[0]->Number : 'Not Specified');
			$post['Mls'] = (isset($account_meta->Mls) && !empty($account_meta->Mls)) ? $account_meta->Mls : 'Not Specified';
			$post['UserName'] = $this->input->post('UserName');
			$post['Website'] = ($agent_info->domains) ? $agent_info->domains : 'Not Specified';
			$post['PricingPlan'] = $this->input->post('PricingPlan');
			$post['ApplicationName'] = $this->input->post('ApplicationName');

			$url = "http://admindashboard.agentsquared.com/api/Mail/idx_cancel/format/json";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL,$url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

		} else {

			die('There\'s nothing to cancel here. You haven\'t purchased yet!'); // Kamatay mocancel nyah wala pa diay nipalit. Nayabag na.

		}

	}

	public function cancel_agent_idx_old() {

		if($this->input->post('UserId')) {

			$data = array(
			   'ApplicationName' 	=> $this->input->post('ApplicationName'),
			   'ApplicationUri' 	=> $this->input->post('ApplicationUri'),
			   'CallbackName' 		=> $this->input->post('CallbackName'),
			   'EventType' 			=> $this->input->post('EventType'),
			   'PricingPlan' 		=> $this->input->post('PricingPlan'),
			   'TestOnly' 			=> $this->input->post('TestOnly'),
			   'Timestamp' 			=> $this->input->post('Timestamp'),
			   'UserEmail' 			=> $this->input->post('UserEmail'),
			   'UserId' 			=> $this->input->post('UserId'),
			   'UserName' 			=> $this->input->post('UserName'),
			   'ApplicationType' 	=> 'Agent_IDX_Website',
			);

			$arr = array(
				'UserId' 			=> $this->input->post('UserId'),
				'EventType' 		=> 'purchased',
				'ApplicationType' 	=> 'Agent_IDX_Website',
			);

			$agent_already_purchased = $this->Agent_model->check_agent_subscription($arr);

			if($agent_already_purchased) {

				$data['UserEmail'] = ($this->input->post('UserEmail')) ? $this->input->post('UserEmail') : $agent_already_purchased->UserEmail;
				$this->Agent_model->update_agent_subscription($arr, $data);

			} else {

				$data['UserEmail'] = ($this->input->post('UserEmail')) ? $this->input->post('UserEmail') : 'Not Specified';
				$this->db->insert('agent_subscription', $data); 

			}

			$url = "http://admindashboard.agentsquared.com/api/Mail/idx_cancel/format/json";

			$post['UserId'] = $this->input->post('UserId');
			$post['UserEmail'] = $this->input->post('UserEmail');
			$post['UserName'] = $this->input->post('UserName');
			$post['PricingPlan'] = $this->input->post('PricingPlan');
			$post['ApplicationName'] = $this->input->post('ApplicationName');

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL,$url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

		}

	}

	public function review_agent_idx() {

		if($this->input->post('ApplicationName')) {

			$data = array(
			   'ApplicationName' 	=> $this->input->post('ApplicationName'),
			   'ApplicationUri' 	=> $this->input->post('ApplicationUri'),
			   'CallbackName'		=> $this->input->post('CallbackName'),
			   'EventType' 			=> $this->input->post('EventType'),
			   'PricingPlan'	 	=> $this->input->post('PricingPlan'),
			   'TestOnly'	 		=> $this->input->post('TestOnly'),
			   'Timestamp' 			=> $this->input->post('Timestamp'),
			   'UserEmail' 			=> $this->input->post('UserEmail'),
			   'UserId' 			=> $this->input->post('UserId'),
			   'UserName' 			=> $this->input->post('UserName'),
			   'ApplicationType' 	=> 'Agent_IDX_Website',
			);

			$this->db->insert('agent_subscription', $data); 

		} else {
			echo "Invalid Application";
		}
	}

}