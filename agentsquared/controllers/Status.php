<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {
	public function index()
	{
        header('Content-Type: application/json');

        $status = array(
            'env' => ENVIRONMENT,
            'ci_version' => CI_VERSION,
            'branch' => exec('git rev-parse --abbrev-ref HEAD'),
            'commit' => exec('git rev-parse --short HEAD'),
            'database' => array(
                'stat' => $this->db->conn_id->stat,
                'benchmark' => $this->db->benchmark
            )
        );

        echo json_encode($status);
	}
}
