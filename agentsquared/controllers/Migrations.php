<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migrations extends MX_Controller {

    public function get_domain_lists()
    {
        $this->load->model("agent_sites/Agent_sites_model", "agent_site");
        $lists = $this->agent_site->get_domains();
        exit( json_encode($lists));
    }
    // alter by rolen
    public function get_agent_tokens()
    {  
        if(isset($_GET['token']) && $_GET['token'] == "nvBXloUwK19Ea5"){
            $this->load->model("Cron_model");
            $lists = $this->Cron_model->fetch_valid_access_token();
            exit(json_encode($lists));
        }else{
            echo 'Error'; exit;
        }
    }

    public function get_agent_bearer_tokens()
    {
        $this->load->model("Cron_model");
        $lists = $this->Cron_model->fetch_bearer_token();
        exit( json_encode($lists));
    }
    public function myaccount($access_token = "") {
      if($access_token) {
        $url = "https://sparkapi.com/v1/my/account";
        $headers = array(
          'Content-Type: application/json',
          'User-Agent: Spark API PHP Client/2.0',
          'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
            'Authorization: OAuth '. $access_token,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        $result_info = json_decode($result, true);
        if(empty($result_info)) {
          $result_info = curl_strerror($ch);
        }

        return $result_info;
        //return ($result_info["D"]["Success"]) ? TRUE : FALSE;
      }
      return FALSE;
    }

    public function save_failed_token_to_php(){
      $this->load->model("Cron_model");
      $accessToken = $this->input->post('access_token');
      $token = $this->Cron_model->fetch_agent_token($this->input->post('agent_id'));
      $result_info = $this->myaccount($token->access_token);
      //if(!$isLiveToken)
      if(!$result_info["D"]["Success"])
      {

        $this->load->library("email");            
        $this->email->send_email_v3(
            'admin@agentsquared.com', //sender email
            'AgentSquared', //sender name
            'rolbru12@gmail.com', //recipient email
            'rolen@agentsquared.com', //bccs
            'f1bd638d-fe3b-4b1e-acd1-17c8335b7342', //'b581f7e2-befe-4162-94b3-d5f470216a34', //template_id
            $email_data = array(
                'subs' => array(
                    'agent_id'          => $this->input->post('agent_id'),
                    'failed_idx_reason' => $result_info
                )
            )
        );

        if($result_info["D"]["Code"] == '1550'){
            exit(json_encode(array("success" => TRUE)));
        }elseif($result_info["D"]["Code"] == '1510'){
            exit(json_encode(array("success" => TRUE)));
        }elseif($result_info["D"]["Code"] == '1010'){
            exit(json_encode(array("success" => TRUE)));
        }

        $failed_token = array(
            'is_valid' => 0,
            'date_modified'     => date("Y-m-d h:m:s"),
        );

        $this->Cron_model->update_token($this->input->post('agent_id'),$failed_token);
        $user_id = $this->Cron_model->get_user_id($this->input->post('agent_id'));
        //$this->send_failed_token($this->input->post('agent_id'),$user_id->first_name);       

        exit(json_encode(array("success" => TRUE)));        
      }
      exit(json_encode(array("success" => FALSE)));
    }

    public function save_failed_token_to_php_old(){
      $this->load->model("Cron_model");
      $accessToken = $this->input->post('access_token');
      $result_info = $this->myaccount($accessToken);
      //if(!$isLiveToken)
      if(!$result_info["D"]["Success"])
      {

        $this->load->library("email");            
        $this->email->send_email_v3(
            'admin@agentsquared.com', //sender email
            'AgentSquared', //sender name
            'rolbru12@gmail.com', //recipient email
            'rolen@agentsquared.com', //bccs
            'f1bd638d-fe3b-4b1e-acd1-17c8335b7342', //'b581f7e2-befe-4162-94b3-d5f470216a34', //template_id
            $email_data = array(
                'subs' => array(
                    'agent_id'          => $this->input->post('agent_id'),
                    'failed_idx_reason' => $result_info
                )
            )
        );

        if($result_info["D"]["Code"] == '1550'){
           exit(json_encode(array("success" => TRUE)));
        }

        $db_idx_token = $this->Cron_model->fetch_agent_token($this->input->post('agent_id'));

        if(isset($db_idx_token->counter)){
            if($db_idx_token->counter < 4){
                $counter = $db_idx_token->counter + 1;
                $counter_array = array('counter' => $counter,'date_modified' => date("Y-m-d h:m:s"));

                $this->Cron_model->update_counter($this->input->post('agent_id'),$counter_array);

                exit(json_encode(array("success" => TRUE)));
            }else{

                $failed_token = array(
                    'is_valid' => 1,
                    'date_modified'     => date("Y-m-d h:m:s"),
                );

                $this->Cron_model->update_token($this->input->post('agent_id'),$failed_token);
                $user_id = $this->Cron_model->get_user_id($this->input->post('agent_id'));
                //$this->send_failed_token($this->input->post('agent_id'),$user_id->first_name);       

                exit(json_encode(array("success" => TRUE)));
            }
        }
        
        exit(json_encode(array("success" => TRUE)));
        
      }
      exit(json_encode(array("success" => TRUE)));
    }
    public function send_failed_token($agent_id="", $user_name="") {
      $this->load->library("email");
      $content = "
        Failed Refresh Tokens <br /><br />
        Agent ID: ".$agent_id."<br />
        Date: ".date("Y-m-d")."<br />
        Time: ".date("H:i:s")."<br />
        Date AND Time: ".date("Y-m-d H:i:s")."<br />
      ";
      $subject = "Failed Refresh Tokens";
      return $this->email->send_email(
        'automail@agentsquared.com',
        'Agent Squared',
        'rolbru12@gmail.com',
        $subject,
        'email/template/default-email-body',
        array(
          'message' => $content,
          'subject' => $subject,
          'to_bcc' => "rolbru12@gmail.com"
        )
      );
    }
    public function save_new_token_from_dsl()
    {
        $this->load->model("Cron_model");
        $new_token = array(
            'access_token'  => $this->input->post('access_token'),
            'refresh_token' => $this->input->post('refresh_token'),
            'oauth_key' => $this->input->post('oauthkey'),
            'date_modified' => date("Y-m-d h:m:s"),
            'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+23 hours')),
            'is_valid' => 1,
        );
        $user_id = $this->Cron_model->get_user_id($this->input->post('agent_id'));
        $this->Cron_model->update_token($this->input->post('agent_id'),$new_token);
        $this->update_spark_token_2($user_id->id,$this->input->post('agent_id'),$this->input->post('access_token'),$this->input->post('refresh_token'));
        //$this->test1($this->input->post);
        exit(json_encode(array("success" => TRUE)));
    }
    public function update_spark_token_2($user_id, $agent_id, $access_token, $refresh_token) {
        $obj = modules::load("dsl");
        $body = array(
            'system_user_id'    => (int)$user_id,
            'agent_id'        => $agent_id,
            'access_token'      => $access_token,
            'refresh_token'     => $refresh_token,
            'date_created'      => date("Y-m-d h:m:s"),
            'date_modified'     => date("Y-m-d h:m:s"),
            'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
        );
        $endpoint = "agent_idx_token/".$agent_id."/update";
        $ret = $obj->put_endpoint($endpoint, $body);
        return $ret;
    }


    public function testing_v3() {

        $this->load->library("email");

        $content = $this->input->post("content");
        $recipient  = $this->input->post("recipient");
        $subject = $this->input->post("subject");
        $from = $this->input->post("from");

        $return = $this->email->send_email_v3(
            $from,
            'Agent Squared',
            $recipient,
            $subject,
            'email/template/default-email-body',
            array(
                'message' => $content,
                'subject' => $subject,
                'to_bcc' => "rolbru12@gmail.com"
            )
        );
        printA($return); exit;
        exit(json_encode(array("success" => $return)));
    }

    public function drip_callback_url() {
        printA($this->input->get());
    }

    public function fetch_failed_tokens() {

        $this->load->model("idx_login/idx_model");
        $idx_token = $this->idx_model->fetch_all_token();
        exit(json_encode($idx_token));
        
    }

    public function print_failed_tokens() {
        $this->load->model("idx_login/idx_model");
        $flag = FALSE;
        $idx_token = $this->idx_model->fetch_all_token();
        echo count($idx_token);
        printA($idx_token);
    }

    public function manual_refresh_token(){
        if(isset($_GET["access_token"])){
            $refresh_token = $_GET["refresh_token"];
            $type = $_GET["type"];
            $agent_id = $_GET["agent_id"];

            if($type == 'armls'){
                $this->armls_refresh_token($refresh_token, $agent_id);
            }else{
                $this->default_refresh_token($refresh_token, $agent_id);
            }
        }
    }

    public function default_refresh_token($refresh_token, $agent_id = NULL) {
        $this->load->model("idx_login/idx_model");
        $url = "https://sparkapi.com/v1/oauth2/grant";
        $post = array(
            'client_id'     => getenv('SPARK_CLIENT_ID'),
            'client_secret' => getenv('SPARK_CLIENT_SECRET'),
            'grant_type'    => 'refresh_token',
            'redirect_uri'  => getenv('AGENT_DASHBOARD_URL') . 'idx_login/idx_callback',
        );
        $headers = ['X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0'];
        $post['refresh_token'] = $refresh_token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch); 
        $result_info = json_decode($result, true);
        
        if(isset($result_info['error'])){
            echo "Invalid GRANT! AgentID: ".$agent_id;
            printA($result_info); exit;
        }
        $this->idx_model->save_manual_tokens($result_info, $agent_id);
        echo "Success!! SAVED TO DATABASES! AgentID: ".$agent_id;
        printA($result_info); 

        return true;
        // echo 'md - '.date("Y-m-d h:m:s");
        // echo "br /";
        // echo 'ed -'.date("Y-m-d h:m:s", strtotime('+23 hours'));
        exit;
    }

    public function armls_refresh_token($refresh_token, $agent_id = NULL) {
        $this->load->model("idx_login/idx_model");
        $url = "https://sparkapi.com/v1/oauth2/grant";
        $post = array(
            'client_id'     => 'c417twiz2tfscsj1f73e2zg7y',
            'client_secret' => '81e57w3hijt0howtpk5gjath3',
            'grant_type'    => 'refresh_token',
            'redirect_uri'  => getenv('AGENT_DASHBOARD_URL') . 'idx_login/idx_callback/armls',
        );
        $headers = ['X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0'];
        $post['refresh_token'] = $refresh_token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        $result_info = json_decode($result, true);
        if(isset($result_info['error'])){
            echo "Invalid GRANT! AgentID: ".$agent_id;
            printA($result_info); exit;
        }
        $this->idx_model->save_manual_tokens($result_info, $agent_id);
        echo "Success!! SAVED TO DATABASES! AgentID: ".$agent_id;
        printA($result_info);
        return true;
        // echo 'md - '.date("Y-m-d h:m:s");
        // echo "br /";
        // echo 'ed -'.date("Y-m-d h:m:s", strtotime('+23 hours'));
        exit;
    }

    public function get_premium_agent_tokens()
    {
        if(isset($_GET['token']) && $_GET['token'] == "nvBXloUwK19Ea5"){
            $this->load->model("Cron_model");
            $lists = $this->Cron_model->fetch_premium_valid_access_token();
            exit(json_encode($lists));
        }else{
            echo 'Error'; exit;
        }
    }

    public function get_free_agent_tokens($token="")
    {
        
        if(isset($_GET['token']) && $_GET['token'] == "nvBXloUwK19Ea5"){
            $this->load->model("Cron_model");
            $lists = $this->Cron_model->fetch_free_agent_valid_access_token();
            exit(json_encode($lists));
        }else{
            echo 'Error'; exit;
        }
    }

    public function set_freemium(){

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            
            exit('Invalid Request.');
        }

        $raw = @file_get_contents('php://input');
        $post = json_decode($raw);

        // access_token should be 'ce114e4501d2f4e2dcea3e17b546f339'
        if( property_exists($post->custom, 'access_token')
            && strlen($post->custom->access_token) == 32
            && password_verify($post->custom->access_token, '$2y$10$13fuTnXtFuNtpqVdDiparey6zPFQKoUTrIX5K4CIS1w2jPJe.q91i')){

            $this->load->model('agent_model');

            $agent = $this->agent_model->getAgentByEmail($post->subscriber->email);

            if($agent !== false){

                if($this->agent_model->setFreemium($agent->id) !== false){

                    exit('Set Freemium success.');
                }
            }
        }
        exit('Failed setting to freemium.');
    }

    public function require_cc_update(){

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            
            exit('Invalid Request.');
        }

        $raw = @file_get_contents('php://input');
        $post = json_decode($raw);

        // access_token should be 'ce114e4501d2f4e2dcea3e17b546f339'
        if( property_exists($post->custom, 'access_token') 
            && strlen($post->custom->access_token) == 32
            && password_verify($post->custom->access_token, '$2y$10$13fuTnXtFuNtpqVdDiparey6zPFQKoUTrIX5K4CIS1w2jPJe.q91i')){

            $this->load->model('agent_model');

            $agent = $this->agent_model->getAgentByEmail($post->subscriber->email);

            if($agent !== false){

                if($this->agent_model->requireUpdateCC($agent->id) !== false){

                    exit('Required CC Update on this agent right after logging into the dashboard.');
                }
            }
        }
        exit('There was a problem requiring this agent to update its CC.');
    }
}
