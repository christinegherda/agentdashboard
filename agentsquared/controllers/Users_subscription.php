<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_subscription extends CI_Controller {

	public function __construct() {

        parent::__construct();

        if(!$this->session->userdata('logged_in_auth_admin'))
			redirect('admin_login', 'refresh');
		
        $this->load->model("Agent_model");

	}

	public function index() {

		$this->load->library('pagination');

		$param["limit"] = ($this->input->get('limit')) ? $this->input->get('limit') : 24;
		$param["offset"] = ($this->input->get('offset')) ? $this->input->get('offset') : 0;

		$total = $this->Agent_model->fetch_all_agent(TRUE, $param);
		$users = $this->Agent_model->fetch_all_agent(FALSE, $param);

		$data["users"] = $users;
		
		$config["base_url"] = base_url().'users_subscription';

		if($this->input->get('type') || $this->input->get('keywords')) {
			$config["base_url"] .= '?type='.$this->input->get('type').'&keywords='.$this->input->get('keywords');
		}

		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->pagination->initialize($config);

		$data["pagination"] = $this->pagination->create_links();

		$this->load->view('subscription_view', $data);
		
	}

	public function removed_users_record() {
			
		if($this->input->is_ajax_request()) {
			
			$response = array('success'=>FALSE, 'message'=>'Failed to delete user!');

			if($this->input->post('user_id') && $this->input->post('agent_id')) {

				$data['user_id'] = $this->input->post('user_id');
				$data['agent_id'] = $this->input->post('agent_id');

				$delete = $this->Agent_model->removed_users_record($data);

				$response["success"] = ($delete) ? TRUE : FALSE;
				$response["message"] = ($delete) ? "User successfully deleted!" : "Failed to delete user!";

			}

			exit(json_encode($response));

		}

	}

	public function admin_logout() {

		$this->session->unset_userdata('logged_in_auth_admin');
		redirect("admin_login", "refresh");

	}

	public function test() {

		//$to_time = strtotime("2008-12-13 10:42:00");
		//$from_time = strtotime("2008-12-13 10:21:00");
		//echo round(abs($to_time - $from_time) / 60,2). " minute";	

		$from 	= date_create(date('Y-m-d h:m:s'));
		printA($from);
		$to 	= date_create(date('Y-m-d', strtotime('2018-01-17 09:59:03')));
		$diff 	= date_diff($from, $to);
		printA($diff);//exit;
		$days 	= $diff->days;
		
		$minute	= $diff->i;
		echo $minute;
	}


}

?>