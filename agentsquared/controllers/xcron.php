<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MX_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("idx_login/idx_model");
		$this->load->model("featured_listings/featured_listings_model", "featured");
		$this->load->model("agent_sites/Agent_sites_model");
		$this->load->model("cron_model");
	}

	public function auto_refresh_token() {

		$this->load->helper('date');

		$flag = FALSE;

		$idx_token = $this->idx_model->fetch_all_token();
		$dsl_tokens = $this->check_dsl_tokens();

		$current = date('Y-m-d H:i:s');

		foreach($idx_token as $token) {
			if($token->expected_token_expired=="0000-00-00 00:00:00") {
				#continue;
			} else {
				//Check date modified range
				$range = date_range($token->expected_token_expired, $current);

				if(count($range) < 7) {

					if($this->api->Grant($token->refresh_token, 'refresh_token')) {

						$new = TRUE;

						$access_token = $this->api->oauth_access_token;
						$refresh_token = $this->api->oauth_refresh_token;

						$token_info = array(
				    		'access_token' 	=> $access_token,
				    		'refresh_token' => $refresh_token,
				    		'date_modified' => date("Y-m-d h:m:s"),
				    		'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours')),
				    	);

				    	$this->idx_model->update_refresh_token($token->id, $token_info);

				    	$user_id = ($this->cron->fet_user_id($token->agent_id)) ? $this->cron->fet_user_id($token->agent_id) : 0;

				    	if($dsl_tokens) {
				    		foreach($dsl_tokens as $dsl) {
				    			if($dsl["agent_id"] == $token->agent_id) {
				    				$new = FALSE;
				    				if($user_id != 0) {
				    					$this->update_spark_token($user_id, $token->agent_id, $access_token, $refresh_token);
				    					$this->update_spark_token_test_server($user_id, $token->agent_id, $access_token, $refresh_token);
				    				}
				    				break;
				    			}
				    		}
				    	}

				    	if($new) {
				    		if($user_id != 0) {
								$this->save_spark_tokens($user_id, $token->agent_id, $access_token, $refresh_token);
								$this->save_spark_tokens_test_server($user_id, $token->agent_id, $access_token, $refresh_token);
							}
				    	}

				    	$flag = TRUE;
					}
				}
			}
		}

		if($flag) {
			$this->send_email_test();
		}

		//Unsetting DSL Logins Session
		$this->destroy_dsl_sessions();
	}

	public function grant_tokens(){

		$idx_token = $this->idx_model->fetch_all_token();

		foreach($idx_token as $token) {
			$this->grant($token->refresh_token);
		}

	}

	public function grant($code = "") {

		$url = "https://sparkapi.com/v1/oauth2/grant";

		$post = array(
			'client_id' 	=> 'a6b5t2xvveynb5ohmhohn4moc',
			'client_secret' => 'c0ss5y0u9yi38ahn3c2oyurdv',
		 	'grant_type' 	=> 'refresh_token',
			'redirect_uri' 	=> AGENT_DASHBOARD_URL . 'idx_login/idx_callback',
			'refresh_token' => $code,
		);

		$headers = ['X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0'];

		$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, TRUE);

		return TRUE;
	}

	public function auto_update_tokens(){

		$idx_token = $this->idx_model->fetch_all_token();

		foreach($idx_token as $token) {
			$this->update_spark_token($token->user_id, $token->agent_id, $token->access_token, $token->refresh_token);
		}

	}

	public function update_spark_token($user_id, $agent_id, $access_token, $refresh_token) {

		$url = "http://138.68.10.202:3000/api/v1/agent_idx_token/".$agent_id."/update";

    	$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: ERI9I4ku3vDyLKlOit7m_A7hbXiuOakCLgIuszmbGnw',
    		'X-User-Id: wfnbdL5z4Z7e6Ef6i'
    	);

    	$data = array(
    		'system_user_id'		=> (int)$user_id,
    		'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);

		$data_json = json_encode($data, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result  = curl_exec($ch);
		$result_info = json_decode($result, true);

	}
	
	public function post_spark_token($user_id, $agent_id, $access_token, $refresh_token) {

		$url = "http://138.68.10.202:3000/api/v1/agent_idx_token";

    	$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: ERI9I4ku3vDyLKlOit7m_A7hbXiuOakCLgIuszmbGnw',
    		'X-User-Id: wfnbdL5z4Z7e6Ef6i'
    	);

    	$data = array(
    		'system_user_id'		=> (int)$user_id,
    		'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);

		$data_json = json_encode($data, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result  = curl_exec($ch);
		$result_info = json_decode($result, true);
		
		return TRUE;
	}

	public function delete_test($user_id=NULL) {

		if($user_id) {

			$url = "http://138.68.10.202:3000/api/v1/agent_idx_token/userid/".$user_id."/delete/";

	    	$headers = array(
	    		'Content-Type: application/json',
	    		'X-Auth-Token: ERI9I4ku3vDyLKlOit7m_A7hbXiuOakCLgIuszmbGnw',
	    		'X-User-Id: wfnbdL5z4Z7e6Ef6i'
	    	);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	 		curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE"); 
			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
			
			printA($result_info);

	    }
	}

}
