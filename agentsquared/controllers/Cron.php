<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cron extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model("idx_login/idx_model");
		$this->load->model("featured_listings/featured_listings_model", "featured");
		$this->load->model("agent_sites/Agent_sites_model");
		$this->load->model("cron_model", "cron");
	}

	public function fetch_failed_tokens() {
		
		$this->load->helper('date');
		$flag = FALSE;
		$idx_token = $this->idx_model->fetch_all_token();

		return ($idx_token);
	}

	public function print_failed_tokens() {
		$this->load->helper('date');
		$flag = FALSE;
		$idx_token = $this->idx_model->fetch_all_token();
		echo count($idx_token);
		printA($idx_token);
	}

	public function auto_refresh_token() {
		$this->load->helper('date');
		$flag = FALSE;
		$idx_token = $this->idx_model->fetch_all_token();
		printA($idx_token);exit;
		$dsl_tokens = $this->check_dsl_tokens();
		$current = date('Y-m-d H:i:s');
		foreach($idx_token as $token) {
			if($token->expected_token_expired=="0000-00-00 00:00:00") {
				#continue;
			} else {
				//Check date modified range
				$range = date_range($token->expected_token_expired, $current);
				if(count($range) < 7) {
					$fetch = $this->fetch_new_token($token->agent_id, $token->refresh_token);
					if($fetch["success"]) {//If access token successfully refresh,
						$new = TRUE;
						$token_info = array(
				    		'access_token' 	=> $fetch['access_token'],
				    		'refresh_token' => $fetch['refresh_token'],
				    		'date_modified' => date("Y-m-d h:m:s"),
				    		'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours')),
				    		'is_valid' => 1,
				    	);
						$this->cron->update_token($token->agent_id, $data);
				    	$user_id = ($this->cron->fet_user_id($token->agent_id)) ? $this->cron->fet_user_id($token->agent_id) : 0;
				    	if($dsl_tokens) {
				    		foreach($dsl_tokens as $dsl) {
				    			if($dsl["agent_id"] == $token->agent_id) {
				    				$new = FALSE;
				    				if($user_id != 0) {
				    					$this->update_spark_token($user_id, $token->agent_id, $access_token, $refresh_token);
				    					$this->update_spark_token_test_server($user_id, $token->agent_id, $access_token, $refresh_token);
				    				}
				    				break;
				    			}
				    		}
				    	}
				    	if($new) {
				    		if($user_id != 0) {
								$this->save_spark_tokens($user_id, $token->agent_id, $access_token, $refresh_token);
								$this->save_spark_tokens_test_server($user_id, $token->agent_id, $access_token, $refresh_token);
							}
				    	}
				    	$flag = TRUE;
					} else {//if not successfully refreshed
						$data = array('is_valid' => 0);
						$this->cron->update_token($token->agent_id, $data);
					}
				}
			}
		}
		if($flag) {
			$this->send_email_test();
		}
		//Unsetting DSL Logins Session
		$this->destroy_dsl_sessions();
	}

	public function refresh_token($refresh_token) {

		$url = "https://sparkapi.com/v1/oauth2/grant";
		$post = array(
			'client_id' 	=> getenv('SPARK_CLIENT_ID'),
			'client_secret' => getenv('SPARK_CLIENT_SECRET'),
		 	'grant_type' 	=> 'refresh_token',
			'redirect_uri' 	=> getenv('AGENT_DASHBOARD_URL') . 'idx_login/idx_callback',
		);
		$headers = ['X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0'];
		$post['refresh_token'] = $refresh_token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		printA($result_info);
	}

	public function refresh_dying_token() {
		$this->load->library("email");
		$flag = FALSE;
		$idx_token = $this->idx_model->fetch_all_token();
		$token_updated = array();
		$token_success = array();
		$token_failed = array();
		foreach($idx_token as $token) {
			if($token->expected_token_expired=="0000-00-00 00:00:00") {
				#continue;
			} else {
				if($this->myaccount($token->access_token)) {
					$fetch = $this->fetch_new_token($token->agent_id, $token->refresh_token);
					if($fetch["success"]) {
						$token_info = array(
				    		'access_token' 	=> $fetch['access_token'],
					    	'refresh_token' => $fetch['refresh_token'],
				    		'date_modified' => date("Y-m-d h:m:s"),
				    		'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours')),
				    		'is_valid' => 1,
				    	);
						$token_success[] = $token_info;
				    	$this->idx_model->update_refresh_token($token->id, $token_info);
				    	$user_id = ($this->cron->fet_user_id($token->agent_id)) ? $this->cron->fet_user_id($token->agent_id) : 0;
				    	if($user_id != 0) {
	    					$this->update_spark_token($user_id, $token->agent_id, $fetch['access_token'], $fetch['refresh_token']);
	    					$this->update_spark_token_test_server($user_id, $token->agent_id, $fetch['access_token'], $fetch['refresh_token']);
	    				}
				    	$flag = TRUE;
				    	$content = "
							Hi,
							You receive this email because the system has Successfully Refresh the Agent's token.
							Access Token: ".$fetch['access_token']."
							Refresh Token: ".$fetch['refresh_token']."
							Agent ID: ".$token->agent_id."
							Thank you!
				        ";
				        $subject ="Agent Token Refresh : Success!" ;
				        $this->email->send_email(
				            'automail@agentsquared.com',
				            'Token Authentication Failure',
				            'alvin@agentsquared.com',
				            $subject,
				            'email/template/default-email-body',
				            array(
				                "message" => $content,
				                "subject" => $subject,
				                "to_bcc" => "honradokarljohn@gmail.com, raul.agentsquared@gmail.com"
				            )
				        );
				   	} else {
			   			$token_failed[] = array("status" => $refresh_status['status'], 'data' => $token);
			   			$content = "
							Hi,
							You receive this email because the system has failed to refresh the following token. You have 2 hours left to manually re-authenticate this token.
							Access Token: ".$token->access_token."
							Refresh Token: ".$token->refresh_token."
							Agent ID: ".$token->agent_id."
							Thank you!
				        ";
				        $subject ="Agent Token Refresh : Failed!" ;
				        $this->email->send_email(
				            'automail@agentsquared.com',
				            'Token Authentication Failure',
				            'alvin@agentsquared.com',
				            $subject,
				            'email/template/default-email-body',
				            array(
				                "message" => $content,
				                "subject" => $subject,
				                "to_bcc" => "honradokarljohn@gmail.com, raul.agentsquared@gmail.com"
				            )
				        );
				   	}
				} else {
					$token_failed[] = array("status" => $refresh_status['status'], 'data' => $token);
		   			$content = "
						Hi,
						This email is an indication that the token was already invalid before refreshing.
						Access Token: ".$token->access_token."
						Refresh Token: ".$token->refresh_token."
						Agent ID: ".$token->agent_id."
						Thank you!
			        ";
			        $subject ="Agent Token Refresh : Check Account If Token is still valid!" ;
			        $this->email->send_email(
			            'automail@agentsquared.com',
			            'Token Authentication Failure',
			            'alvin@agentsquared.com',
			            $subject,
			            'email/template/default-email-body',
			            array(
			                "message" => $content,
			                "subject" => $subject,
			                "to_bcc" => "honradokarljohn@gmail.com, raul.agentsquared@gmail.com"
			            )
			        );
				}
				//$token_updated[] = $refresh_status;
			}
		}
		if($flag){
			$this->send_email_test();
		}
		echo "Token Success<br>====================";
		echo "<pre>";
		var_dump($token_success);
		echo "</pre>";
		echo "Token Failed<br>====================";
		echo "<pre>";
		var_dump($token_failed);
		echo "</pre>";
		echo "DB IDX Tokens Before<br>====================";
		echo "<pre>";
		var_dump($idx_token);
		echo "</pre>";
		//Destroy sessions in DSL
		$this->destroy_dsl_sessions();
	}
	public function send_email_test() {
		$this->load->library("email");
		$content = "Hello there, this is a test email indicating that the refresh token is working.";
		$subject = "Refresh Token Response";
		$this->email->send_email(
			'automail@agentsquared.com',
			'Agent Squared',
			'karljohnhonrado@gmail.com',
			$subject,
			'email/template/default-email-body',
			array(
				'message' => $content,
				'subject' => $subject,
				'to_bcc' => "alvin@agentsquared.com, honradokarljohn@gmail.com, rolbru12@gmail.com, agentsquared888@gmail.com"
			)
		);
	}
	public function send_notification_email() {
		/*$this->load->library("email");
		$content = "Hello there, this is a test email indicating that the save down sites info is working.";
		$subject = "Save Down Sites Info Indicator";
		$this->email->send_email(
			'automail@agentsquared.com',
			'Agent Squared',
			'honradokarljohn@gmail.com',
			$subject,
			'email/template/default-email-body',
			array(
				'message' => $content,
				'subject' => $subject,
				'to_bcc' => "alvin@agentsquared.com, rolbru12@gmail.com, agentsquared888@gmail.com"
			)
		);*/
		return true;
	}
	public function save_down_sites_info() {
		$flag = FALSE;
		$user_info = $this->cron->get_user_info();
		if($user_info) {
			foreach($user_info as $user) {
				$data = array(
					'ip_address'		=> (!empty($user->ip_address)) ? $user->ip_address : "Not Specified",
					'agent_id'			=> (!empty($user->agent_id)) ? $user->agent_id : "Not Specified",
					'agent_name'		=> (!empty($user->agent_name)) ? $user->agent_name : "Not Specified",
					'email'				=> (!empty($user->email)) ? $user->email : "Not Specified",
					'access_token'		=> (!empty($user->access_token)) ? $user->access_token : "Not Specified",
					'refresh_token'		=> (!empty($user->refresh_token)) ? $user->refresh_token : "Not Specified",
					'application_type' 	=> (!empty($user->ApplicationType)) ? $user->ApplicationType: "Not Specified",
					'date_time_down'	=> date("Y-m-d h:m:s")
				);
				$this->cron->save_down_time_info($data);
				$flag = TRUE;
			}
		}
		if($flag) {
			$this->send_notification_email();
		}
	}
	public function check_token_validity() {
		$data = $this->cron->fetch_token_data();
		if($data) {
			foreach($data as $key) {
				printA($key);
				$account_information = $this->myaccount($key->access_token);
				if(!$account_information) {
					$agent['first_name'] = $key->first_name;
					$agent['email'] = $key->email;
					//$this->send_invalid_token($agent, getenv('AGENT_DASHBOARD_URL') . "idx_login", ($key->isFromSpark == "1") ? "www.sparkplatform.com/appstore/apps/instant-agent-idx-website" : "www.agentsquared.com/launchyoursitenow/");
				}
			}
		}
		return true;
	}

	public function relaunch_idx_website() {

		$data = $this->cron->fetch_token_data();
		printA($data);
		if($data) {

			foreach($data as $key) {

				$account_information = $this->myaccount($key->access_token);

				if(!$account_information) {

					$agent['first_name'] 	= $key->first_name;
					$agent['email'] 		= $key->email;
					$agent['url']			= ($key->oauth_key == "armls") ? 'https://armls.flexmls.com/ticket' : getenv('AGENT_DASHBOARD_URL')."idx_login";
					$agent['content_msg'] 	= ($key->oauth_key == "armls") ? 'FlexMLS account. Click the menu tab in the corner left, then click AgentSquared - Instant IDX Website' : 'Spark account using your FlexMLS login then click on the launch button.';

					$this->send_invalid_token($agent);

				}

			}

		}

	}

	private function send_invalid_token($agent=array()) {

		if(!empty($agent)) {

			$this->load->library("email");
			
			$this->email->send_email_v3(
	        	'support@agentsquared.com', //sender email
	        	'AgentSquared', //sender name
	        	$agent['email'], //recipient email
	        	'honradokarljohn@gmail.com, aldwinj12345@gmail.com', //bccs
	        	'e527e351-5863-4381-910e-381dafebd3a2', //'49d31025-6527-46a0-8741-986d6a718099', //template_id
	        	$email_data = array(
	        		'subs' => array(
	    				'first_name' 	=> $agent['first_name'],
	    				'relaunch_url' 	=> $agent['url'],
	    				'content_msg' 	=> $agent['content_msg'],
	        		)
				)
	        );

		}

		return true;
	}

	public function check_dsl_tokens() {
		$dsl = modules::load('dsl');
		$url = "agent_idx_token";
		$result = $dsl->get_endpoint($url);
		$result_info = json_decode($result, true);
		return $result_info["data"];
	}
	public function destroy_dsl_sessions() {
		//detroying session in dsl
		$this->session->unset_userdata('login_status');
		$this->session->unset_userdata('authToken');
		$this->session->unset_userdata('userId');
		//detroying session in dsl test server
		$this->session->unset_userdata('dsl_test_login_status');
		$this->session->unset_userdata('dsl_test_authToken');
		$this->session->unset_userdata('dsl_test_userId');
	}
	public function save_spark_tokens($user_id, $agent_id, $access_token, $refresh_token) {
		$obj = modules::load("dsl");
		$body = array(
    		'system_user_id'		=> (int)$user_id,
			'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours')),
		);
		$endpoint = "agent_idx_token";
		$ret = $obj->post_endpoint($endpoint, $body);
	}
	public function save_spark_tokens_test_server($user_id, $agent_id, $access_token, $refresh_token) {
		if(!$this->session->userdata("dsl_test_login_status")) {
			$this->dsl_login_test_server();
		}
		$url = "http://138.68.16.176:3000/api/v1/agent_idx_token";
		$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: '.$this->session->userdata("dsl_test_authToken"),
    		'X-User-Id: '.$this->session->userdata("dsl_test_userId")
    	);
		$data = array(
    		'system_user_id'		=> (int)$user_id,
			'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);
		$data_json = json_encode($data, true);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
	}
	public function update_spark_token($user_id, $agent_id, $access_token, $refresh_token) {
		$obj = modules::load("dsl");
		$body = array(
    		'system_user_id'		=> (int)$user_id,
			'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);
		$endpoint = "agent_idx_token/".$agent_id."/update";
		$ret = $obj->put_endpoint($endpoint, $body);
	}
	public function update_spark_token_test_server($user_id, $agent_id, $access_token, $refresh_token) {
		if(!$this->session->userdata("dsl_test_login_status")) {
			$this->dsl_login_test_server();
		}
		$url = "http://138.68.16.176:3000/api/v1/agent_idx_token/".$agent_id."/update";
    	$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: '.$this->session->userdata("dsl_test_authToken"),
    		'X-User-Id: '.$this->session->userdata("dsl_test_userId")
    	);
    	$data = array(
    		'system_user_id'		=> (int)$user_id,
    		'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);
		$data_json = json_encode($data, true);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result  = curl_exec($ch);
		$result_info = json_decode($result, true);
	}
	public function dsl_login() {
		$ret = FALSE;
		$url = 'http://138.68.10.202:3000/api/v1/login/';
		$username = 'agentsquared';
		$password = '8c15bb00663a4c127286530cfeec02a3';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$username.'&password='.$password);
		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		if(!empty($result_info) && $result_info["status"] == "success") {
			$this->session->set_userdata(
				array(
					"login_status" => true,
					"authToken" => $result_info["data"]["authToken"],
					"userId" => $result_info["data"]["userId"]
				)
			);
			$ret = TRUE;
		}
		return $ret;
	}
	public function dsl_login_test_server() {
		$ret = FALSE;
		$url = 'http://138.68.16.176:3000/api/v1/login/';
		$username = 'agentsquared';
		$password = '8c15bb00663a4c127286530cfeec02a3';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$username.'&password='.$password);
		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		if(!empty($result_info) && $result_info["status"] == "success") {
			$this->session->set_userdata(
				array(
					"dsl_test_login_status" => true,
					"dsl_test_authToken" => $result_info["data"]["authToken"],
					"dsl_test_userId" => $result_info["data"]["userId"]
				)
			);
			$ret = TRUE;
		}
		return $ret;
	}
	public function send_existing_domain_email() {
		$result = $this->cron->fetch_domains();
		if($result) {
			$this->load->library("email");
			foreach($result as $res) {
				$content = "Hi ".$res->first_name."! <br><br>
		            Your agent site (".$res->domain_name.") is almost ready. Since you have an existing domain, you need to point your domain to our DNS. Follow the instructions in the link below. <br><br>
		            <a href='". AGENT_DASHBOARD_URL . "faq/change_existing_domain' target='_blank'>" . AGENT_DASHBOARD_URL . "faq/change_existing_domain</a>
		            <br><br>
		            Best Regards,<br><br>
		            AgentSquared Team
		        ";
		        $subject ="Agent Site with Existing Domain" ;
		        $this->email->send_email(
		            'automail@agentsquared.com',
		            'AgentSquared',
		            $res->email,
		            $subject,
		            'email/template/default-email-body',
		            array(
		                "message" => $content,
		                "subject" => $subject,
		                "to_bcc" => "troy@agentsquared.com,albert@agentsquared.com,jocereymondelo@gmail.com,agentsquared888@gmail.com,honradokarljohn@gmail.com"
		            )
		        );
			}
		}
	}
	public function myaccount($access_token) {
		if($access_token) {
			$url = "https://sparkapi.com/v1/my/account";
			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $access_token,
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);
			$result = curl_exec($ch);
			$result_info = json_decode($result, true);
			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
			return ($result_info["D"]["Success"]) ? TRUE : FALSE;
		}
	}
	public function refresh_valid_access_tokens() {
		$flag = FALSE;
		$tokens = $this->cron->fetch_valid_access_token();
		$dsl_tokens = $this->check_dsl_tokens();
		if(!empty($tokens) && !empty($dsl_tokens)) {
			foreach($tokens as $token) {
				$counter = $token->counter;
				if($counter >= 3) {
					$data = array('counter' => 0);
					$this->cron->update_token($token->agent_id, $data);
					continue;
				}
				if($counter <= 1) {
					$fetch = $this->fetch_new_token($token->agent_id, $token->refresh_token);
					if($fetch["success"]) {//If access token successfully refresh,
						$new = TRUE;
						$data = array(
							'access_token'	=> $fetch['access_token'],
				    		'refresh_token' => $fetch['refresh_token'],
				    		'date_modified' => date("Y-m-d h:m:s"),
				    		'expected_token_expired' => date("Y-m-d H:i:s", strtotime('+24 hours')),
				    		'is_valid' => 1,
				    		'counter' => ($counter+1),
						);
						$this->cron->update_token($token->agent_id, $data);
						$user_id = ($this->cron->fet_user_id($token->agent_id)) ? $this->cron->fet_user_id($token->agent_id) : 0;
						foreach($dsl_tokens as $dsl) {
							if($dsl["agent_id"] == $token->agent_id) {//if agent id is already in dsl, update token
			    				$new = FALSE;
			    				if($user_id != 0) {
			    					$this->update_spark_token($user_id, $token->agent_id, $fetch['access_token'], $fetch['refresh_token']);
			    					$this->update_spark_token_test_server($user_id, $token->agent_id, $fetch['access_token'], $fetch['refresh_token']);
			    				}
			    				break;
			    			}
						}
						if($new) {//If agent is not yet in dsl, save it
				    		if($user_id != 0) {
								$this->save_spark_tokens($user_id, $token->agent_id, $fetch['access_token'], $fetch['refresh_token']);
								$this->save_spark_tokens_test_server($user_id, $token->agent_id, $fetch['access_token'], $fetch['refresh_token']);
							}
				    	}
				    	$flag = TRUE;
					} else {//if not successfully refreshed
						$data = array('is_valid' => 0, 'counter' => 0);
						$this->cron->update_token($token->agent_id, $data);
						$this->for_brua_email($token->agent_id, $token->UserName);
					}
				} else {
					$data = array('counter' => ($counter+1));
					$this->cron->update_token($token->agent_id, $data);
				}
			}
		}
		if($flag) {
			$this->send_email_test();
		}
		//Unsetting DSL Logins Session
		$this->destroy_dsl_sessions();
	}
	public function fetch_new_token($agent_id, $refresh_token) {
		$ret = array('success' => FALSE);
		if($agent_id && $refresh_token) {
			$url = "https://sparkapi.com/v1/oauth2/grant";
			$post = array(
				'client_id' 	=> getenv('SPARK_CLIENT_ID'),
				'client_secret' => getenv('SPARK_CLIENT_SECRET'),
			 	'grant_type' 	=> 'refresh_token',
				'redirect_uri' 	=> getenv('AGENT_DASHBOARD_URL') . 'idx_login/idx_callback',
			);
			$headers = ['X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0'];
			$post['refresh_token'] = $refresh_token;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL, $url);
			$result = curl_exec($ch);
			$result_info = json_decode($result, true);
			if(isset($result_info['access_token']) && isset($result_info['refresh_token'])) {
				$ret["success"] = TRUE;
				$ret["access_token"] = $result_info['access_token'];
				$ret["refresh_token"] = $result_info['refresh_token'];
			}
		}
		return $ret;
	}

	public function save_listings_updates() {
		$this->load->model('crm/leads_model', 'leads');
		$agents = $this->cron->get_idx_agents();
		foreach($agents as $agent) {
			if($agent->is_email_market) {
				$token = array(
					'access_token' => $agent->access_token,
					'bearer_token' => ""
				);
				$searches = $this->cron->get_contacts($agent->user_id);

				
				if($searches) {
					foreach($searches as $search) {
						if($search->json_search) {
							$params = json_decode($search->json_search);
							$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold') And (MajorChangeType Eq 'New Listing')";
							if(isset($params->Search) && !empty($params->Search)) {
								$filterStr .= " And (PublicRemarks Eq '*".$params->Search."*' Or UnparsedAddress Eq '*".$params->Search."*')";
							}
							$properties = $this->run_search($token, $filterStr);
							if($properties) {
								$data = array(
									'user_id' 		=> $agent->user_id,
									'contact_id' 	=> $search->contact_id,
									'search_header'	=> (isset($params->Search)) ? $params->Search : "New Listing",
									'listings_json' => json_encode($properties),
									'date_created'	=> date("Y-m-d h:i:sa")
								);
								$this->cron->save_listings($data);
							}
						}
						if ($search->csid) {
							$this->cron->insert_cron_job_history(array(
								'pk_id' => $search->csid,
								'user_id' => $agent->user_id,
								'run_on' => date('Y-m-d H:i:s'),
								'type' => 'NEW_LISTING_SAVED_SEARCH',
								'email_schedule' => $search->email_schedule
							));
						}
					}
				}

				
			}

			if ($agent->user_id) {
				$this->cron->insert_cron_job_history(array(
					'pk_id' => $agent->user_id,
					'user_id' => $agent->user_id,
					'run_on' => date('Y-m-d H:i:s'),
					'type' => 'NEW_LISTING_SAVED_SEARCH_IDX_AGENT'
				));
			}
		}
		return true;
	}
	public function run_search($token, $filterStr) {
		$obj = modules::load("dsl");
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);
		$endpoint = "spark-listings?_limit=3&_pagination=1&_page=1&_filter=(".$filter_string.")&_expand=PrimaryPhoto";
		$results = $obj->post_endpoint($endpoint, $token);
		$result_listings = json_decode($results, true);
		$data = array();
		$counter = 0;
		if($result_listings["data"]) {
			foreach($result_listings["data"] as $result) {
				$data[$counter]["Id"] 				= $result['StandardFields']['ListingKey'];
				$data[$counter]["UnparsedAddress"] 	= $result['StandardFields']['UnparsedAddress'];
				$data[$counter]["BedsTotal"] 		= $result['StandardFields']['BedsTotal'];
				$data[$counter]["BathsTotal"] 		= $result['StandardFields']['BathsTotal'];
				$data[$counter]["BuildingArea"] 	= $result['StandardFields']['BuildingAreaTotal'];
				$data[$counter]["LotArea"] 			= $result['StandardFields']['LotSizeSquareFeet'];
				$data[$counter]["CurrentPrice"] 	= $result['StandardFields']['CurrentPrice'];
				$data[$counter]["Photo"] 			= $result['Photos']['Uri300'];
				$data[$counter]['PublicRemarks']	= $result['StandardFields']['PublicRemarks'];
				$counter++;
			}
		}

		return $data;
	}
	public function send_customer_email_updates($schedule) {
		$this->load->library("email");
		$this->load->model('crm/leads_model', 'leads');
		$listings = $this->cron->get_saved_listings($schedule);
		
		if($listings) {
			foreach($listings as $listing) {
				if($listing->is_email_market) {
					$agent_name = $listing->first_name." ".$listing->last_name;
					$agent_email = $listing->agent_email;
					$agent_domain = $listing->domains;
					$agent_num = $listing->phone;
					$logo_url = base_url()."assets/images/email-logo.png";
					$img_tag = "";
					$customer_email = $listing->customer_email;
					$customer_name = $listing->contact_name;
					$account = json_decode(gzdecode($listing->account), true);
					$mls_name = $listing->mls_name;
					$agent_photo = $listing->agent_photo;
					$agent_logo = $listing->logo;
					$mls_name = ($account) ? $account['Mls'] : "";
					$list = json_decode($listing->listings_json, true);

					$content = "";
					if($list) {
						foreach($list as $prop) {
							
							$prop_url = 'http://'.$listing->domains.'/other-property-details/'.$prop['Id'];
							$prop_address = $prop['UnparsedAddress'];
							$prop_photo = $prop['Photo'];
							$bed_total = $prop['BedsTotal'];
							$bath_total = $prop['BathsTotal'];
							$bldg_area = isset($prop['BuildingArea']) ? $prop['BuildingArea'] : '';
							$lot_area = isset($prop['LotArea']) ? $prop['LotArea'] : '';
							$public_remarks = isset($prop['PublicRemarks']) ? $prop['PublicRemarks'] : '';
							$current_price = is_numeric($prop['CurrentPrice'])  ? number_format($prop['CurrentPrice']) : $prop['CurrentPrice'];
							$content .= '<table>
											<thead>
												<tr>
													<th colspan="3" style="text-align:center">
														<a href="'.$prop_url.'" target="_blank">
															<h3 style="margin-bottom:10px;">'.$prop_address.'</h3>
														</a>
													</th>
												</tr>
											</thead>
											<tbody>
											  	<tr>
												    <td>
												    	<a href="'.$prop_url.'" target="_blank">
												    		<img src="'.$prop_photo.'" style="width:100%; height:auto;">
												    	</a>
												    </td>
											  	</tr>
											  	<tr>
											  		<td style="font-family: Segoe UI,Tahoma,Helvetica,Arial,sans-serif; padding: 12px 0 5px 30px; font-size: 14px; color: #969696; font-weight:bold">'
											  			.$bed_total.' beds | '
											  			.$bath_total.' baths | '
											  			.($bldg_area ? $bldg_area .' sqft | ' : '')
											  			.($lot_area ? $lot_area .' lot | ' : ''). '
											  			<span style="color: #1891f6; font-size: 16px;"> $ '.$current_price.'</span>
											  		</td>
											  	</tr>
											  	<tr>
											  		<td style="
											  			font-family: Segoe UI,Tahoma,Helvetica,Arial,sans-serif;
											  			padding: 10px 30px 6px;
											  			color: #515666;
											  			font-size: 14px;
											  			line-height: 150%;">'
											  			.$public_remarks.
											  		'</td>
											  	</tr>
											</tbody>
										</table>';
						}
					}

					$params = json_decode($listing->json_search);
					$lastday = gmdate('Y-m-d\TH:i:s\Z', strtotime('- 1 day'));
					$today =  gmdate('Y-m-d\TH:i:s\Z');
					$redirect_url = $params ? $agent_domain.'/home/new_listings_search?Search='.$params->Search.'&from_time='.$lastday.'&to_time='.$today : '';

					$this->email->send_email_v3(
				        	'support@agentsquared.com', //sender email
				        	'AgentSquared', //sender name
				        	$customer_email, //recipient email
				        	'honradokarljohn@gmail.com,aldwinj12345@gmail.com,joce@agentsquared.com,sergio.casquejo@gmail.com', //bccs
				        	'85ae4f9b-8736-4096-b135-3d1a3bf4648f', //'1302de8d-8107-4ec3-a0d0-39499fc6f5df', //template_id
				        	array(
				        		'subs' => array(
				    				'customer_name' => $customer_name,
				    				'agent_name' 	=> $agent_name,
				    				'listing_count' => (string) count($list),
				    				'search_query' 	=> $params ? $params->Search : '',
				    				'new_listing_html_template' => $content,
				    				'redirect_url' 	=> $redirect_url,
				    				'agent_mls'		=> $mls_name,
				    				'agent_phone' 	=> $agent_num,
				    				'agent_email' 	=> $agent_email,
				    				'agent_site'	=> $agent_domain,
				    				'agent_company'	=> $account['Office'],
				    				'agent_website_url'	=> 'http://'.$agent_domain,
				    				'agent_website_photo' => !empty($agent_photo) ? getenv('AGENT_DASHBOARD_URL').'assets/uploads/photo/'.$agent_photo : getenv('AGENT_DASHBOARD_URL').'assets/images/no-profile-img.gif',
				    				'logo_url' => !empty($agent_logo) ? getenv('AGENT_DASHBOARD_URL').'assets/uploads/logo/'.$agent_logo : getenv('AGENT_DASHBOARD_URL').'assets/images/email-logo.png'
				        		)
							)
				        );



					$delete_searches = $this->cron->delete_searches($listing->csl_id);
					
				}

				
			}

			if ($listing->csl_id) {
				$this->cron->insert_cron_job_history(array(
					'pk_id' => $listing->csl_id,
					'user_id' => $listing->agent_user_id,
					'run_on' => date('Y-m-d H:i:s'),
					'type' => 'CONTACT_SEARCH_LISTINGS'
				));
			}
		}
		return true;
	}
	public function get_account_info($user_id) {
		$obj = modules::load("dsl");
		$endpoint = "agent/".$user_id."/account-info";
		$account_info = $obj->get_endpoint($endpoint);
		$content = json_decode($account_info, true);
		return $content;
	}

	public function run_cron_job() {
		$schedule = $this->input->get('schedule');
		$this->send_customer_email_updates("daily");
		$this->send_customer_email_updates("weekly");
		$this->send_customer_email_updates("monthly");
		$this->save_listings_updates();


		// switch ($schedule) {
		// 	case 'hourly':
		// 		// $this->post_email_notification_hourly();
		// 		// $this->post_email_notification_daily();
		// 		$this->send_customer_email_updates("hourly");
		// 		// $this->check_token_validity();
		// 		$this->save_listings_updates();

		// 		break;
		// 	case 'daily':
		// 		// $this->post_email_notification_daily();
		// 		$this->send_customer_email_updates("daily");
		// 		// $this->check_token_validity();
		// 		$this->save_listings_updates();
		// 		break;
		// 	case 'weekly':
		// 		$this->send_customer_email_updates("weekly");
		// 		break;
		// 	case 'monthly':
		// 		$this->send_customer_email_updates("monthly");
		// 		break;
		// 	default:
		// 		# code...
		// 		break;
		// }
	}
	
	public function for_brua_email($agent_id="", $user_name="") {
		$this->load->library("email");
		$content = "
			Failed Refresh Tokens <br /><br />
			Agent Name: ".$user_name."<br />
			Agent ID: ".$agent_id."<br />
			Date: ".date("Y-m-d")."<br />
			Time: ".date("H:i:s")."<br />
			Date AND Time: ".date("Y-m-d H:i:s")."<br />
		";
		$subject = "Failed Refresh Tokens";
		return $this->email->send_email(
			'automail@agentsquared.com',
			'AgentSquared',
			'rolbru12@gmail.com',
			$subject,
			'email/template/default-email-body',
			array(
				'message' => $content,
				'subject' => $subject,
				'to_bcc' => "agentsquared888@gmail.com, honradokarljohn@gmail.com"
			)
		);
	}
	public function expired_trial() {
		$data = $this->cron->trial_is_over();
		//printA($data);exit;
		if($data) {
			$this->load->library("email");
			$subject = "Free Trial Expired";
			foreach($data as $key) {
				if(isset($key->email) && !empty($key->email)) {
					$content = "Hi ".$key->first_name.",<br><br/>
						Thanks for signing up for AgentSquared. We hope you have been enjoying your free trial.<br><br/>
						<strong>Unfortunately, your 15 days free trial already expired.</strong><br><br/>
						We'd love to keep you, and there is still time to complete and launch your own Agent site to the public. Simply visit your account dashboard to subscribe.<br><br>
						Dashboard login URL: <a href='" . AGENT_DASHBOARD_URL . "login/auth/login' target='_blank'>" . AGENT_DASHBOARD_URL . "login/auth/login</a><br>
						Username: ".$key->email."<br>
						Password: test1234<br><br>
						If you have any questions or feedback, just contact us through our AgentSquared <a href='https://www.agentsquared.com/support/' target='_blank'>support </a>.
						<br><br/>
			            All the best,<br><br/>
			            AgentSquared Team";
			        $this->email->send_email(
						'automail@agentsquared.com',
						'AgentSquared',
						$key->email,
						$subject,
						'email/template/default-email-body',
						array(
							'message' => $content,
							'subject' => $subject,
							'logo_url' 	=> AGENT_DASHBOARD_URL . "assets/images/email-logo.png",
							'to_bcc' => "honradokarljohn@gmail.com, rolbru12@gmail.com, agentsquared888@gmail.com, automail@agentsquared.com"
						)
					);
					$this->cron->expired_trial_sent($key->id);
				} else {
					continue;
				}
			}
		}
	}
	public function post_email_notification_daily() {
		//Customize the Branding and Home Page On Your Site
		echo "<h2>Customize the Branding and Home Page On Your Site</h2>";
		$result1 = $this->cron->get_new_signups(1);
		if($result1) {
			foreach($result1 as $key) {
				printA($key);
				$subs = array(
					'first_name' => $key->first_name,
					'redirect_url' => AGENT_DASHBOARD_URL.'cron/redirect_to_pages?id='.$key->id
				);
				$this->send_post_email_notification($key->email, "6dbf988b-83ec-4e7e-8e7d-42f9d02f6292", $subs);
			}
		}
		//Customize Listings on Your Home Page
		/*$result2 = $this->cron->get_new_signups(2);
		if($result2) {
			foreach($result2 as $key) {
				$this->send_post_email_notification($key->email"], "Not yet made", $subs);
			}
		}*/
		//Configure Lead Capture and CRM
		echo "<h2>Configure Lead Capture and CRM</h2>";
		$result3 = $this->cron->get_new_signups(3); 
		if($result3) {
			foreach($result3 as $key) {
				printA($key);
				$subs = array(
					'first_name' => $key->first_name,
					'redirect_url' => AGENT_DASHBOARD_URL.'cron/redirect_to_pages?id='.$key->id
				);
				$this->send_post_email_notification($key->email, "742056a8-9e5e-4fa4-b5c0-b23c070a51c3", $subs);
			}
		}
		//Promote Your Active Listings on Social Media 
		echo "<h2>Promote Your Active Listings on Social Media</h2>";
		$result4 = $this->cron->get_new_signups(4); 
		if($result4) {
			foreach($result4 as $key) {
				printA($key);
				$subs = array(
					'first_name' => $key->first_name,
					'redirect_url' => AGENT_DASHBOARD_URL.'cron/redirect_to_pages?id='.$key->id
				);
				$this->send_post_email_notification($key->email, "a77a1341-601d-49fd-8a69-1d5d7eeb76df", $subs);
			}
		}
		//Promote Your New New Listing 
		/*$result5 = $this->cron->get_new_signups(5); 
		if($result5) {
			foreach($result5 as $key) {
				$this->send_post_email_notification($key->email, "not yet created", $subs);
			}
		}*/
		//Create Custom Pages Featuring Your Farm
		echo "<h2>Create Custom Pages Featuring Your Farm</h2>";
		$result6 = $this->cron->get_new_signups(6); 
		if($result6) {
			foreach($result6 as $key) {
				printA($key);
				$this->send_post_email_notification($key->email, "becea46d-47f4-4b9d-97a0-12ef9f283172", array('first_name' => $key->first_name));
			}
		}
		/*
		//Get Great Reviews
		$result7 = $this->cron->get_new_signups(7); 
		if($result7) {
			foreach($result7 as $key) {
				$this->send_post_email_notification($key->email, "not yet created", $subs);
			}
		}
		//Use Your Own Google Analytics
		$result8 = $this->cron->get_new_signups(8); 
		if($result8) {
			foreach($result8 as $key) {
				$this->send_post_email_notification($key->email, "not yet created", $subs);
			}
		}
		//Using Google Adwords to Drive Traffic to Your Site to Capture buyer and seller leads
		$result9 = $this->cron->get_new_signups(9); 
		if($result9) {
			foreach($result9 as $key) {
				$this->send_post_email_notification($key->email, "not yet created", $subs);
			}
		}
		//Using Facebook How to Use Facebook to Seller Leads
		$result10 = $this->cron->get_new_signups(10);
		if($result10) {
			foreach($result10 as $key) {
				$this->send_post_email_notification($key->email, "not yet created", $subs);
			}
		}		
		//Using Search Engine Optimization to get organic traffic and leads
		$result11 = $this->cron->get_new_signups(11);
		if($result11) {
			foreach($result11 as $key) {
				$this->send_post_email_notification($key->email, "not yet created", $subs);
			}
		}*/
		//We noticed you haven't customized Your Site - Schedule a call with one of our marketing 
		echo "<h2>We noticed you haven't customized Your Site - Schedule a call with one of our marketing </h2>";
		$result13 = $this->cron->get_new_signups(13);
		if($result13) {
			foreach($result13 as $key) {
				printA($key);
				$this->send_post_email_notification($key->email, "6676384b-47d6-4cd3-abaa-d1d7f240da52", array('first_name'=>$key->first_name));
			}
		}		
		/*
		//Your Free Trial is about to expire in 1 day
		$result14= $this->cron->get_new_signups(14);
		if($result14) {
			foreach($result14 as $key) {
				$this->send_post_email_notification($key->email, "not yet created", $subs);
			}
		}
		//Sorry to see you go, you are welcome back anytime.
		$result15 = $this->cron->get_new_signups(15);
		if($result15) {
			foreach($result15 as $key) {
				$this->send_post_email_notification($key->email, "not yet created", $subs);
			}
		}*/
	}
	public function post_email_notification_hourly() {
		//How to select a custom domain name
		echo "<h2>How to select a custom domain name</h2>";
		$result = $this->cron->get_new_hourly();
		if($result) {
			foreach($result as $key) {
				printA($key);
				$subs = array(
					'first_name' => $key->first_name,
					'agent_domain' => $key->domains,
					'custom_domain_url' => AGENT_DASHBOARD_URL.'cron/redirect_to_pages?id='.$key->id
					//'custom_domain_url' => AGENT_DASHBOARD_URL.'cron/redirect_to_pages?email='.$key->email.'&user_id='.$key->id.'&agent_id='.$key->agent_id.'&created_on='.$key->created_on
				);
				$this->send_post_email_notification($key->email, "365b54ae-4042-4200-9acb-b799732f79ce", $subs);
			}
		}
		return true;
	}
	public function redirect_to_pages() {

		if($this->input->get('id')) {

			$user = $this->cron->get_user_info_post_email($this->input->get('id'));

			$session = array(
				'user_id' 			=> $user->id,
				'agent_id' 			=> $user->agent_id,
				'email' 			=> $user->email,
				'identity' 			=> $user->email,
				'agent_name' 		=> $user->first_name,
				'site_title' 		=> $user->site_name,
				'old_last_login' 	=> $user->created_on,
				'logged_in_auth' 	=> true,
			);

			$this->session->set_userdata($session);

			redirect('dashboard', 'refresh');
		
		} else {
			redirect(AGENT_DASHBOARD_URL, 'refresh');
		}

	}

	private function send_post_email_notification($recipient=NULL, $template_id=NULL, $subs=array()) {
		$this->load->library("email");
		$this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	'honradokarljohn@gmail.com',//$recipient, //recipient email
        	'troy@agentsquared.com,eugene@agentsquared.com,albert@agentsquared.com,raul@agentsquared.com,automail@agentsquared.com,rolbru12@gmail.com,honradokarljohn@gmail.com,alvin@agentsquared.com,paul@agentsquared.com,joce@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	$template_id, //template_id
        	$email_data = array('subs'=>$subs)
        );
	}
	public function test_mail() {
		
		$this->load->library("email");
        $ret = $this->email->send_email_v3(
        	'automail@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	'honradokarljohn@gmail.com', //recipient
        	'karljohnhonrado@gmail.com, jessiedelarnado@gmail.com, christine.gonx@gmail.com', //bccs
        	'bcd9e749-d8a3-4a9b-9b48-c292ace96ce5', //'3a128de2-9f5e-4fa5-b1f9-b280077b6e4d', //template_id
        	$email_data = array(
			        		'subs' => array(
			    				'agent_domain' => 'karljohn.agentsquared.com',
			    				'first_name' => 'Karl',
			    				'login_url' => 'http://karljohn.agentsquared.com',
			    				'agent_email' => 'karljohnhonrado@gmail.com',
			    				'agent_password' => 'test1234'
			        		)
        				)
        );
        printA($ret);
	}

	
}