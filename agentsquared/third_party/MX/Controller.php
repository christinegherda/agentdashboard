<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__FILE__).'/Base.php';
require 'vendor/autoload.php';
/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller
{
	public $autoload = array();

	public function __construct()
	{	
		
		$this->load->helper('cookie');

		$this->load->model("home/home_model", "home_model");
		/*
		$server_ip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '127.0.0.1';

		if (!get_cookie('is_server_ip_whitelisted')) {

			$result = $this->home_model->get_sendgrid_rule_id_by_ip($server_ip);
			
			if (!$result) {

				$data = json_decode(sendgrid_curl("https://api.sendgrid.com/v3/access_settings/whitelist", array(
					'ips' => array(
						'ip' => $server_ip
					)
				)));
				
				if (isset($data->result[0])) {
					$this->home_model->save_whitelisted_ips(array(
						'rule_id' => $data->result[0]->id,
						'ip_address' => explode('/', $data->result[0]->ip)[0],
					));
				}
			}

			set_cookie('is_server_ip_whitelisted', true);
		}*/


		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		log_message('debug', $class." MX_Controller Initialized");
		Modules::$registry[strtolower($class)] = $this;

		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);

		/* autoload module items */
		$this->load->_autoloader($this->autoload);

		$module = $this->router->fetch_module();
		$mod_arr = array('idx_login', 'mysql_properties');

		if(!in_array($module, $mod_arr)) {

			if(empty(config_item('is_paid'))) {

				CI::$APP->db->select('rid')
							->from('receipt')
							->where('agent_id', CI::$APP->session->userdata('user_id'))
							->limit(1);

				$receipt = CI::$APP->db->get()->row();

				if($receipt) {

					CI::$APP->config->set_item('is_paid', TRUE );

					CI::$APP->db->select('id')
								->from('domains')
								->where('agent', CI::$APP->session->userdata('user_id'))
								->where('type' , 'agent_site_domain')
								->where('status', 'completed')
								->limit(1);
								
					$domain = CI::$APP->db->get()->row();

					CI::$APP->config->set_item('is_domain_completed', ($domain) ? TRUE : FALSE);

				} else {

					CI::$APP->config->set_item('is_paid', FALSE);

					if(!$this->config->item('trial')) {

						CI::$APP->db->select('u.trial, u.date_signed, p.price, p.plan')
					                ->from('users u')
					                ->join('pricing p', 'u.id = p.user_id', 'left')
					                ->where('u.id', $this->session->userdata('user_id'))
					                ->where('u.trial', 1)
					                ->where('p.type', 'idx')
					                ->limit(1);

				        $res = CI::$APP->db->get()->row();

				        if(!empty($res)) {

				            $num = 0;

				            if(!empty($res->date_signed)) {

				                $from=date_create(date('Y-m-d'));
				                $to=date_create(date('Y-m-d', strtotime($res->date_signed)));
				                $diff=date_diff($from,$to);
				                $num = $diff->days;

				            }

				            $this->config->set_item('trial', TRUE);
							$this->config->set_item('left', $num);
							$this->config->set_item('pricing', $res->trial);
							$this->config->set_item('price', $res->price);
				        }
					}
				}
			}

			if(!$this->config->item('agent_idx_website')) {

				$arr = array(
					'UserId' => $this->session->userdata('agent_id'),
					'EventType' => 'purchased',
					'ApplicationType' => 'Agent_IDX_Website'
				);

				CI::$APP->db->select('UserId')
			                ->from('agent_subscription')
			                ->where($arr);

	            $ret = CI::$APP->db->get()->result();

	            if(!empty($ret)) {
	            	$this->config->set_item('agent_idx_website', TRUE);
	            }
			}
		}
	}

	public function _remap($method, $params = array()) {

		$module = $this->router->fetch_module();
		$class = $this->router->fetch_class();

		if($module == 'idx_login' || $module == 'mysql_properties' || $module=='dsl') {
			$this->$method();
		} else {
			$freemium = Modules::load('freemium/base');
			$details = $freemium->fetch_details(); // Fetch all methods that are allowable only to the user's current plan
			$all_details = $freemium->fetch_all_details(); // Fetch all list of methods

			/*
				- Check if feature is listed on all details
				- If feature is listed, check on details
				- If not, bypass
			*/
		
			$isFreemiumUser = 0;
			$isPermitted = 0;
			$isFreemiumFeature = 0;

			$user_plan = $freemium->isFreemiumUser(); //Fetching user's plan details

			if(!empty($user_plan)) {
				$isFreemiumUser = 1;
				if($user_plan->plan_id == 1) {
					$this->config->set_item('disallowUser', TRUE);
				}
			}
			
			foreach($all_details as $var) {
				if($var->module == $module && $var->class == $class && $var->method == $method) {
					$isFreemiumFeature = 1;
				}
			}

			if($isFreemiumFeature && $isFreemiumUser) {
				// Check if plan has permission
				foreach($details as $detail) {
					if($detail->module == $module && $detail->class == $class && $detail->method == $method) {
						// Capable
						$isPermitted = 1;
					}
				}
				if($isPermitted) {
					$this->$method();
				} else {
					if($this->input->post()) {
						unset($_POST);
						exit(header('location: '.base_url().$module.'/'.$class.'/'.$method.'?modal_premium=true'));
					} elseif($this->input->get()) {
						if(!array_key_exists('modal_premium', $_GET)) {
							exit(header('location: '.base_url().$module.'/'.$class.'/'.$method.'?modal_premium=true'));
						}
					} elseif($this->input->is_ajax_request()) {
						exit(header('location: '.base_url().$module.'/'.$class.'/'.$method.'?modal_premium=true'));
					} else {
						exit(header('location: '.base_url().$module.'/'.$class.'/'.$method.'?modal_premium=true'));
					}
					$this->$method();
				}
			}
			else {
				$this->$method();
			}
		}

		
	}

	public function __get($class)
	{
		return CI::$APP->$class;
	}
}
