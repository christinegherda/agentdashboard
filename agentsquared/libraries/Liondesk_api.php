<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Liondesk API Library
* 
* Description: PHP library for accessing Liondesk API services with ease.
* Author: Jovanni G
* Author URL: http://jovanni.mousbook.com
*/
class Liondesk_api{
  private $access_token = false;
  public $url = 'https://api-v2.liondesk.com/';
  
  public function __construct(){
    # code...
  }

  /**
  * Manual request of token via SSO process.
  * @param assoc $args (required) Consist of OAuth Liondesk requirements.
  */
  public function requestToken($args = array()){
    $body = array(
      'code' => $args['code'],
      'client_id' => $args['client_id'],
      'client_secret' => $args['client_secret'],
      'redirect_uri' => $args['redirect_uri'],
      'grant_type' => "authorization_code"
    );

    $body_json = json_encode($body, JSON_UNESCAPED_SLASHES);

    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json' ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
    curl_setopt($ch, CURLOPT_URL, $this->url.'oauth2/token');

    $content = curl_exec($ch);
    $result = json_decode($content);

    if(!$result) {
      $result = curl_error($ch);

      // add code to email me error logs
      return false;
    }
    return $result;
  }

  public function refreshToken($args = array()){

    if(isset($args['refresh_token'])){
      $header = array(
        'Content-Type: application/json'
      );

      $body_json = json_encode(array(
        'refreshToken' => $args['refresh_token'],
        'client_id' => $args['client_id'],
        'client_secret' => $args['client_secret'],
        'redirect_uri' => $args['redirect_uri'],
        'grant_type' => "refresh_token"
      ), true);

      $ch = curl_init();
      
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
      curl_setopt($ch, CURLOPT_URL, $this->url.'oauth2/token' );

      $content = curl_exec($ch);
      $result = json_decode($content);

      if(!$result) {
        $result = curl_error($ch);

        // add code to email me error logs
        return array(
          'error' => true,
          'message' => $result
        );
      }
      return $result;
    }
    return false;

  }

  public function loadToken($access_token){

    $this->access_token = $access_token;
  }

  public function get($endpoint){
    if($this->access_token){
      $header = array(
        'Authorization: Bearer '.$this->access_token,
        'Content-Type: application/json'
      );

      $ch = curl_init();
      
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $this->url.$endpoint );

      $content = curl_exec($ch);
      $result = json_decode($content);

      if(!$result) {
        $result = curl_error($ch);

        // add code to email me error logs
        return array(
          'error' => true,
          'message' => $result
        );
      }
      return $result;
    }
    return array();
  }

  public function post($endpoint, $data = array()){
    if($this->access_token){
      $header = array(
        'Authorization: Bearer '.$this->access_token,
        'Content-Type: application/json'
      );

      $body_json = json_encode($data, true);

      $ch = curl_init();
      
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
      curl_setopt($ch, CURLOPT_URL, $this->url.$endpoint );

      $content = curl_exec($ch);
      $result = json_decode($content);

      if(!$result) {
        $result = curl_error($ch);

        // add code to email me error logs
        return array(
          'error' => true,
          'message' => $result
        );
      }
      return $result;
    }
    return array();
  }

  /**
  * Submit a contact to LionDesk.
  * @param assoc $data (required) Check format below:
  array(
    'first_name' => 'Customer Name', // required
    'last_name' => 'Customer last name', // required
    'email' => 'customer email', // required
    'mobile_phone' => '',
    'home_phone' => '', // required either home phone or mobile phone
    'fax' => '',
    'company' => 'Agentsquared',
    'birthday' => '1857-10-23',
    'anniversary' => '1857-10-23',
    'spouse_name' => 'Maria de la Paz Pardo de Tavera',
    'spouse_email' => 'maria@agentsquared.com',
    'spouse_phone' => '',
    'spouse_birthday' => ''
  )
  * Note: This returns data from LionDesk with the contact_id, save the contact_id somewhere else. Else you wont be able to submit tasks for that contact.
  */
  public function submitContact($data = array()){

    return $this->post('contacts', $data);

  }

  /**
  * Submit a task for that specific contact to LionDesk.
  * @param assoc $data (required) Check format below:
  array(
    'contact_id' => 123456, // required.
    'reminder_type_id' => 1, // required. please refer to LionDesk API docs for id reference
    'due_at' => 'yyyy-MM-dd HH:mm:ssZ', // required. follow the format.
    'description' => 'This is a task', // required.
    'notes' => '', // optional. but might be required for customer message.
  )
  */
  public function submitTask($data = array()){

    return $this->post('tasks', $data);

  }
}
