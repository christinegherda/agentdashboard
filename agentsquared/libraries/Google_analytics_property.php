<?php 

   if (!defined('BASEPATH')) exit('No direct script access allowed');

   set_include_path(APPPATH . 'third_party/' . PATH_SEPARATOR .     get_include_path());

   require APPPATH . 'third_party/google-api-php-client-2.1.0/vendor/autoload.php';

class Google_analytics_property{
	
	public $id = '';
	public $view_id = '';
	
	function __construct($accountid ="", $name ="", $website_url =""){
		
		/**
		 * Note: This code assumes you have an authorized Analytics service object.
		 * See the Web Property Developer Guide for details.
		 */

		/**
		 * This request creates a new property.
		 */
		try {
			//initialize Google Client
			$client = $this->initializeAnalytics();
			
			//create google analytics object
			$analytics = new Google_Service_Analytics($client);
			
			//create google analytics web property object
			$property = new Google_Service_Analytics_Webproperty($client);
			$property->setName($name);
			$property->setWebsiteUrl('http://'.$website_url);
			$property->setIndustryVertical('REAL_ESTATE');
			
			//create google analytics view object
			$profile = new Google_Service_Analytics_Profile($client);
			$profile->setName('All Website Data');
			$profile->setECommerceTracking(True);
			
			//add new property to our google analytics account
			$prop = $analytics->management_webproperties->insert($accountid, $property);
			//add new view profile then associate it with the recently created property into our account
			$view = $analytics->management_profiles->insert($accountid, $prop->id, $profile);
			//associate the newly created profile view into the recently created property
			$property->setDefaultProfileId($view->id);
			//update the property from our google analytics account
			$analytics->management_webproperties->update($accountid, $prop->id, $property);
			
			$this->id = $prop->id;
			$this->view_id = $view->id;
			
		} catch (apiServiceException $e) {
			print 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
		} catch (apiException $e) {
			print 'There was a general API error ' . $e->getCode() . ':' . $e->getMessage();
		}
		
	}
	function initializeAnalytics(){
		// Creates and returns the Analytics Reporting service object.
	
		// Use the developers console and download your service account
		// credentials in JSON format. Place them in this directory or
		// change the key file location if necessary.
		$KEY_FILE_LOCATION = APPPATH . 'third_party/google-api-php-client-2.1.0/service-account-credentials.json';
	
		// Create and configure a new client object.
		$client = new Google_Client();
		$client->setApplicationName("AgentSquared Analytics Reporting");
		$client->setAuthConfig($KEY_FILE_LOCATION);
		$client->setScopes([
			'https://www.googleapis.com/auth/analytics',
			'https://www.googleapis.com/auth/analytics.edit'
		]);
		
		return $client;
	}
	
}