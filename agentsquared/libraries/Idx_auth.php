<?php if(!defined('BASEPATH') ) exit('No direct script access allowed');

class Idx_auth{

	/**
	  Created By: Rolen Brua
	**/

	private $access_token;
	private $refresh_token;

	function __construct($agent_id = "") {
		
		$CI =& get_instance();
		$CI->load->database();
		$CI->load->library('session');
		$CI->load->library('email');

		if(isset($agent_id)) {

			$query = $CI->db->select('access_token, refresh_token')
		   					->from('users_idx_token')
		   					->where('agent_id', $agent_id)
		   					->limit(1)
		   					->get();

	    	$token = $query->row();

	    	if($token) {

		   		$this->access_token = $token->access_token;
		    	$this->refresh_token = $token->refresh_token;

		    } else {

		    	$query = $CI->db->select('bearer_token')
			   					->from('user_bearer_token')
			   					->where('agent_id', $agent_id)
			   					->limit(1)
			   					->get();

	    		$x = $query->row();

	    		$this->access_token = $x->bearer_token;
		    }
		}
	}

	public function checker() {
		return $this->get('my/account');
	}

	public function getSavedSearch($search_id = "") {
		return $this->get('savedsearches/'.$search_id);
	}
	public function getSavedSearches($i = 0) {
		return $this->get_params_no_expand('savedsearches', array('_pagination' => 1, '_page' => $i, '_limit' => 24));
	}

	public function GetMyAccount() {
		return $this->get_param_objects('my/account');
	}

	public function GetSystemInfo() {
		return $this->get_param_objects('system');	
	}

	public function GetListing($property_id = ""){
		return $this->get('listings/'.$property_id);
	}

	public function GetMyListings($param = array()) {
		return $this->get_param_objects('my/listings', $param);
	}

	public function GetOfficeListings($param = array()) {
		return $this->get_params('office/listings', $param);
	}

	public function GetListings($param=array()) {
		return $this->get_params('listings', $param);
	}

	public function GetNearbyListings($param=array()) {
		return $this->get_params('listings/nearby', $param);
	}

	public function getContact($contact_id = ""){
		return $this->get('contacts/'.$contact_id);
	}

	public function GetContacts($i = 0) {

		$select = 'GivenName,FamilyName,PrimaryEmail,PrimaryPhoneNumber,ModificationTimestamp,DisplayName';

		$limit=25;

		$url = "https://sparkapi.com/v1/contacts?_pagination=1&_page=".$i."&_limit=".$limit."&_select=".$select;

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls',
			'Accept-Encoding: gzip, deflate',
		    'Authorization: OAuth '. $this->access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_ENCODING , "gzip, deflate");

		$result = curl_exec($ch);
		// $result = gzdecode($result);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return ($result_info["D"]["Success"]) ? array('results' => $result_info["D"]["Results"], 'pagination'=>$result_info["D"]["Pagination"]) : FALSE;

		//return $this->get_params_no_expand('contacts', array('_pagination'=>1, '_page'=>$i, '_limit'=>10));

	}


	public function addContact($data) {
		return $this->post('contacts', $data);
	}

	public function updateContact($id, $data) {
		return $this->put('contacts/' . $id, $data);
	} 

	public function GetCustomFields() {
		return $this->get('customfields');
	}

	public function get($service) {

		$url = "https://sparkapi.com/v1/".$service;

		
		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

	

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_ENCODING , "gzip, deflate");

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return ($result_info["D"]["Success"]) ? $result_info["D"]["Results"] : FALSE;
	}

	public function get_params($service, $param=array()) {

		if(isset($param['_filter'])) {
			if(isset($param['_lat']) && isset($param['_lon'])) {
				$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit']."&_filter=(".$param['_filter'].")&_lat=".$param['_lat']."&_lon=".$param['_lon']."&_expand=PrimaryPhoto";
			} else {
				$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit']."&_filter=(".$param['_filter'].")&_expand=PrimaryPhoto";
			}
		} else {
			$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit']."&_expand=PrimaryPhoto";
		}

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_ENCODING , "gzip, deflate");

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return ($result_info["D"]["Success"]) ? array('results' => $result_info["D"]["Results"], 'pagination'=>$result_info["D"]["Pagination"]) : FALSE;

	}

	public function get_params_no_expand($service, $param=array()) {

		if(isset($param['_filter'])) {
			if(isset($param['_lat']) && isset($param['_lon'])) {
				$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit']."&_filter=(".$param['_filter'].")&_lat=".$param['_lat']."&_lon=".$param['_lon']."&_expand=PrimaryPhoto";
			} else {
				$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit']."&_filter=(".$param['_filter'].")&_expand=PrimaryPhoto";
			}
		} else {
			$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit'];
		}

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_ENCODING , "gzip, deflate");

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return ($result_info["D"]["Success"]) ? array('results' => $result_info["D"]["Results"], 'pagination'=>$result_info["D"]["Pagination"]) : FALSE;

	}

	public function GetMySoldListing($param = array()) {
 	 		
  		$url = "https://sparkapi.com/v1/my/listings?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit']."&_filter=(".$param['_filter'].")&_orderby_=-CloseDate&_expand=PrimaryPhoto";
 
 		$headers = array(
 			'Content-Type: application/json',
 			'User-Agent: Spark API PHP Client/2.0',
 			'X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls',
 		    'Authorization: OAuth '. $this->access_token,
 		);
 
 		$ch = curl_init();
 		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 		curl_setopt($ch, CURLOPT_URL, $url);
 		curl_setopt($ch, CURLOPT_ENCODING , "gzip, deflate");
 
 		$result = curl_exec($ch);
 		$result_info = json_decode($result);
 
 		if(empty($result_info)) {
 			$result_info = curl_strerror($ch);
 		}
 
 		if(empty($param)) {
 			return ($result_info->D->Success) ? $result_info->D->Results : FALSE;
 		} else {
 			return ($result_info->D->Success) ? array('results' => $result_info->D->Results, 'pagination' => $result_info->D->Pagination) : FALSE;
 		}
 
 		//return $this->get_param_objects('my/listings', $param);
  	}

	public function get_param_objects($service, $param=array()) {

		if(empty($param)) {
 			$url = "https://sparkapi.com/v1/".$service;
 		} else {
			if(isset($param['_filter'])) {
 				$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit']."&_filter=(".$param['_filter'].")&_expand=PrimaryPhoto";
 			} else {
 				$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit']."&_expand=PrimaryPhoto";
 			}
 		}
 
		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: Instant IDX Website powered by Flexmls',
		    'Authorization: OAuth '. $this->access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_ENCODING , "gzip, deflate");

		$result = curl_exec($ch);
		$result_info = json_decode($result);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		if(empty($param)) {
			return ($result_info->D->Success) ? $result_info->D->Results : FALSE;
		} else {
			return ($result_info->D->Success) ? array('results' => $result_info->D->Results, 'pagination' => $result_info->D->Pagination) : FALSE;
		}
	}

	public function post($service, $data=array()) {

		$url = "https://sparkapi.com/v1/".$service;

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$post = array('D'=>$data);
		$body = json_encode($post);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return ($result_info["D"]["Success"]) ? $result_info["D"]["Results"] : FALSE;

	}


	public function put($service, $data=array()) {

		$url = "https://sparkapi.com/v1/".$service;

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$post = array('D'=>$data);
		$body = json_encode($post);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		print_r($result_info);
		die;

		return ($result_info["D"]["Success"]) ? $result_info["D"]["Results"] : FALSE;

	}

	public function isTokenValid() {

		$url = "https://sparkapi.com/v1/my/account";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_ENCODING , "gzip, deflate");

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return ($result_info["D"]["Success"]) ? TRUE : FALSE;

	}
}