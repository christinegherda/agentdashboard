<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super Class
 *
 * @package     Gohighlevel CRM
 * @subpackage  Subpackage
 * @category    CRM
 * @author      Karl John Honrado
 * @link        https://help.gohighlevel.com/help
 */
class Gohighlevel {

    protected $rr_campaign_id = 'Jw9JAW9HwFJbeA3RBvWl'; // Round robin campaign ID
    protected $monthly_campaign = 'HwHnjtxHluRn2OpryGOi'; // monthly purchased campaign ID
    protected $annual_campaign = 'nyd4vS6xSZ35HxxXoZ3s'; // annual purchased campaign ID
    protected $cancel_campaign = 'KtsqO0M4Vy4IDkuGK5oA'; // cancel campaign ID
    protected $access_token = '8f53f3982a3e424ae266bb7f435a1637'; //access token for testing
    protected $test_campaign_id = 'Ya77AOJLqYsjkHYIbFc3'; // test campaign id
    protected $endpoint = 'https://app.adbotformula.com/api/crm/agentsquared/'; // adbotformula endpoint, slingshot url to gohighlevel

    public function __construct() {
        // Assign the CodeIgniter super-object
        ci()->load->model('agent_model');
    }

    /**
     * insert lead data in GHL
     *
     * @param  Array $params
     * @return void
     */
    public function process(array $params, string $type) {

        if($params && $type) {

            switch ($type) {
                case 'monthly':
                    $campaign_id = $this->monthly_campaign;
                    break;

                case 'annual':
                    $campaign_id = $this->annual_campaign;
                    break;

                case 'cancel':
                    $campaign_id = $this->cancel_campaign;
                    break;

                default:
                    $campaign_id = $this->rr_campaign_id;
                    break;
            }

            $lead = [
                'campaign_id' => $campaign_id,
                'lead' => $params
            ];

            return $this->_post('leads', $lead);

        }

        return FALSE;

    }

    /**
     * Prepare data for updating / inserting to GHL
     *
     * @param  Integer  $user_id
     * @return  Array of object
     */
    public static function agent_data(Int $user_id) {

        $user = ci()->agent_model->fetchOnBoardData($user_id);

        if($user) {

            $token_string = $user->code.' '.$user->agent_id.' '.$user_id;
            $token = base64_encode($token_string);

            return [
                'Command_Center_Id__c' => $user_id,
                'FirstName' => $user->first_name,
                'LastName' => $user->last_name,
                'Email' => $user->email,
                'Phone' => $user->phone,
                'Company' => $user->company,
                'Plan__c' => $user->plan,
                'Product__c' => $user->type,
                'Subscribed_Via__c' => $user->LaunchFrom,
                'Token_Status__c' => 'LIVE',
                'MLS_Association__c' => $user->mls_name,
                'Registration_Date__c' => $user->date_signed,
                'Domain_URL__c' => $user->domains,
                'Subdomain_URL__c' => $user->subdomain_url,
                'Total_Properties_Sold__c' => $user->total_sold,
                'Total_Active_Properties__c' => $user->total_active,
                'Recent_Active_Property_Date__c' => $user->recent_active_date,
                'Recent_Sold_Property_Date__c' => $user->recent_sold_date,
                'Access_Token__c' => $user->access_token,
                'Agent_ID__c' => $user->agent_id,
                'Paying_Customer__c' => $user->rid ? 'YES' : 'NO',
                'LeadSource' => 'AgentSquared Instant IDX Website',
                'Agent_Dashboard_Url__c' => AGENT_DASHBOARD_URL.'sales_auth/authenticate_saleforce?token='.$token,
                'Date_of_Upgrade__c' => $user->created_on ? date('Y-m-d H:i:s', $user->created_on) : '',
                'Cancellation_Date__c' => $user->cancel_date ? $user->cancel_date : '',
                'Reason_for_Cancellation_from_Customer__c' => $user->cancel_reason ? $user->cancel_reason : ''
            ];

        }

        return FALSE;

    }

    /**
     * Send post http request
     *
     * @param  String service  Array $params
     * @return void
     */
    private function _post(string $service, array $params) {

        $headers = array(
            'Content-Type: application/json',
            'g-access-token: '.$this->access_token,
        );

        try {
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params, true));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $this->endpoint.'/'.$service);

            $result = curl_exec($ch);
            $result_info = json_decode($result, true);

            return isset($result_info['error']) ? FALSE : TRUE;

        } catch (Exception $e) {
            echo $e->getMessage();
        } catch (InvalidArgumentException $e) {
            echo $e->getMessage();
        }

        return FALSE;

    }

}