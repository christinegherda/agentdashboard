<?php
namespace CentralApps\MortgageCalculator;

/**
 * PHP Mortgage Calculator Class
 * @author Michael Peacock
 * @url www.michaelpeacock.co.uk
 */
class Calculator {

    /**
     * The amount borrowed
     * @access private
     */
    private $amountBorrowed = 0;

    /**
     * The interest rate
     * @access private
     */
    private $interestRate = 0;

    private $interestRateValue = 0;

    /**
     * Number of years to spread mortgage across
     * @access private
     */
    private $years = 0;

    /**
     * Custom addition
     * @access private
     */
    private $downPayment = 0;

    private $downPaymentValue = 0;

    public function __construct() {}

    /**
     * Calculates the annual cost of an interest only mortgage
     * @return int
     */
    public function calculateAnnualInterest()
    {
        $payment = $this->clean_number($this->amountBorrowed) * $this->interestRate;
        return number_format($payment,2);
    }
    /**
     * Calculates the monthly cost of an interest only mortgage
     * @return int
     */
    public function calculateMonthlyInterest()
    {
        $payment = ( $this->amountBorrowed * $this->interestRate ) / 12;
        return number_format($payment,2);
    }

    /**
     * Calculates the monthly cost of a repayment mortgage
     * @return int
     */
    public function calculateMonthlyPayment()
    {
        $payment = ( ($this->amountBorrowed - $this->downPayment) * $this->interestRate ) / ( 1 - pow( ( 1 + $this->interestRate ), -$this->years ) ) / 12; 
        return number_format($payment,2);
    }

    public function calculateDownpayment()
    {
       $payment = $this->downPayment;
        return number_format($payment,2);
    }

    public function calculateAmountFinanced()
    {
       $payment = $this->amountBorrowed - $this->downPayment;
        return number_format($payment,2);
    }


    function clean_number($input) 
    {
        return (float) preg_replace('/[^0-9.]/', '', $input);
    }

    /**
     * Sets the amount borrowed
     * @param int $amount the amount borrowed
     * @return void
     */
    public function setAmountBorrowed( $amount )
    {
        $this->amountBorrowed = $this->clean_number($amount);
    }

    /**
     * Sets the interest rate to calculate against
     * @param int $rate the interest rate
     * @return void
     */
    public function setInterestRate( $rate )
    {
        $this->interestRate = $this->clean_number(( $rate / 100 ));
        $this->interestRateValue = ($rate);
    }

    /**
     * Set the number of years to spread the mortgage across
     * @param int $years the number of years
     * @return void
     */
    public function setYears( $years )
    {
        $this->years = $this->clean_number($years);
    }

     /**
     * Custom Addition for Downpayment
     */
    public function setDownpayment( $downpayment )
    {
        $this->downPayment = $this->clean_number($this->amountBorrowed *( $downpayment / 100 ));
        $this->downPaymentValue = ($downpayment);
    }

    /**
     * Calculate the number of monthly payments
     * @return int
     */
    private function numberOfPayments()
    {
        return $this->years / 12;
    }

    /**
     * Calculate the amount of mortgage remaining based off how long remains
     * @param int $monthsLeft the number of months left on the mortgage
     * @return int
     */
    private function calculateRemainingAmount( $monthsLeft )
    {
        return $this->calculateRepayment() * $monthsLeft;
    }

     public function get_price_value()
    {
       $price = $this->amountBorrowed;
        return number_format($price,2);
    }
     public function get_downpayment_value()
    {
       $donwnpayment = $this->downPaymentValue;
        return ($donwnpayment);
    }
     public function get_interest_rate_value()
    {
       $interest_rate = $this->interestRateValue;
        return ($interest_rate);
    }
     public function get_loan_terms_value()
    {
       $loan_term = $this->years;
        return ($loan_term);
    }
}