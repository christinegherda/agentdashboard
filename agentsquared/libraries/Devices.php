<?php

/**
* Checks for types of devices
*/
class Devices extends CI_User_agent{
  var $is_tablet  = FALSE;

  var $tablets = array();

  var $tablet = '';

  function __construct(){
    parent::__construct();

    if (isset($_SERVER['HTTP_USER_AGENT'])){

    }
    if ( ! is_null($this->agent)){
      if ($this->_load_agent_file()){

        $this->_set_tablet();
      }
    }
  }

  protected function _load_agent_file()
  {
    if (defined('ENVIRONMENT') AND is_file(APPPATH.'config/'.ENVIRONMENT.'/user_agents.php'))
    {
      include(APPPATH.'config/'.ENVIRONMENT.'/user_agents.php');
    }
    elseif (is_file(APPPATH.'config/user_agents.php'))
    {
      include(APPPATH.'config/user_agents.php');
    }
    else
    {
      return FALSE;
    }

    $return = FALSE;

    if (isset($mobiles))
    {
      $this->tablets = $tablets;
      unset($tablets);
      $return = TRUE;
    }

    return $return;
  }

  private function _set_tablet(){
    if (is_array($this->tablets) AND count($this->tablets) > 0)
    {
      foreach ($this->tablets as $key => $val)
      {
        if (FALSE !== (strpos(strtolower($this->agent), $key))) {
          $this->is_tablet = TRUE;
          $this->tablet = $val;
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  public function is_tablet(){
    if ( ! $this->is_tablet){
      return FALSE;
    }

    // No need to be specific, it's a mobile
    if ($key === NULL){
      return TRUE;
    }

    // Check for a specific robot
    return array_key_exists($key, $this->tablets) AND $this->tablet === $this->tablets[$key];
  }
}