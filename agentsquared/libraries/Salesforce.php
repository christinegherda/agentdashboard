<?php if(!defined('BASEPATH') ) exit('No direct script access allowed');

class Salesforce {

	private $access_token;
	private $instance_url;
	private $isLogin = false;

	function __construct() {

		if(!$this->isLogin)
			$this->login();

	}

	private function login() {

		if(getenv('CI_ENV') == 'production') {

			$client_id = getenv('SF_CLIENT_ID');
			$client_secret = getenv('SF_CLIENT_SECRET');
			$username = getenv('SF_USERNAME');
			$password = getenv('SF_PASSWORD');
			$uri = getenv('SF_URI');

		} else {

			$client_id = "3MVG9RHx1QGZ7Osh5BytgB5fhKHCTIFo9bJuZfPENX1c7g.67lfe4RiLd8NvUdqaULSmm7zVLmKIK2jVI45s0";
			$client_secret = "617205105176382761";
			$username = "commandcenterapi@agentsquared.com.dev";
			$password = "AgentSquared18!";
			$uri = "https://test.salesforce.com/";

		}

		$url = $uri."services/oauth2/token?grant_type=password&client_id=".$client_id."&client_secret=".$client_secret."&username=".$username."&password=".$password;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$sf_data = curl_exec($ch);
		$sf_data = json_decode($sf_data, true);

		curl_close($ch);

		if(isset($sf_data['access_token']) && isset($sf_data['instance_url'])) {
			$this->access_token=$sf_data['access_token'];
			$this->instance_url=$sf_data['instance_url'];
			$this->isLogin=true;
			return TRUE;
		}

		return FALSE;

	}

	public function create($data=array(), $type=NULL) {

		if(!empty($data) && !empty($type)) {

			$headers = array(
		    	"Authorization: OAuth ".$this->access_token,
		        "Content-type: application/json"
		    );

			$body = json_encode($data);

			$ch = curl_init();

		    curl_setopt($ch, CURLOPT_HEADER, false);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		    curl_setopt($ch, CURLOPT_URL, $this->instance_url."/services/data/v20.0/sobjects/".$type."/");

		    $result = curl_exec($ch);
			$result_info = json_decode($result, true);

			curl_close($ch);
			
			return TRUE;
		}	

		return FALSE;

	}

	public function read($user_id=NULL, $type=NULL) {

		if(!empty($user_id) && !empty($type)) {

			$headers = array(
		    	"Authorization: OAuth ".$this->access_token,
		        "Content-type: application/json"
		    );

			$ch = curl_init();

		    curl_setopt($ch, CURLOPT_HEADER, false);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_URL, $this->instance_url."/services/data/v20.0/query/?q=SELECT+Command_Center_Id__c+FROM+".$type."+WHERE+Command_Center_Id__c+=+'".$user_id."'");

		    $result = curl_exec($ch);
			$result_info = json_decode($result, true);

			curl_close($ch);

			if(isset($result_info['records']) && !empty($result_info['records'])) {
				return $result_info['records'][0]['attributes']['url'];
			}

		}

		return FALSE;

	}

	public function update($user_id=NULL, $type=NULL, $data=array()) {

		if(!empty($user_id) && !empty($type) && !empty($data)) {

			$sf_agent_uri = $this->read($user_id, $type);

			if(!empty($sf_agent_uri)) {

				$headers = array(
			    	"Authorization: OAuth ".$this->access_token,
			        "Content-type: application/json"
			    );

				$body = json_encode($data);

			    $ch = curl_init();

			    curl_setopt($ch, CURLOPT_HEADER, false);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			    curl_setopt($ch, CURLOPT_URL, $this->instance_url.''.$sf_agent_uri);

			    $result = curl_exec($ch);
				$result_info = json_decode($result, true);

				curl_close($ch);

				return TRUE;

			}

		}

		return FALSE;

	}

	public function describe() { //get all fields and description

		$headers = array(
	    	"Authorization: OAuth ".$this->access_token,
	        "Content-type: application/json"
	    );

		$ch = curl_init();

	    curl_setopt($ch, CURLOPT_HEADER, false);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_URL, $this->instance_url."/services/data/v20.0/sobjects/lead/describe");

	    $result = curl_exec($ch);
		$result_info = json_decode($result, true);

		curl_close($ch);

		return $result_info;

	}

	public function lead_converter($data=array()) {

		$headers = array(
	    	"Authorization: OAuth ".$this->access_token,
	        "Content-type: application/json"
	    );

		$body = json_encode($data);

		$ch = curl_init();

	    curl_setopt($ch, CURLOPT_HEADER, false);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
	    curl_setopt($ch, CURLOPT_URL, $this->instance_url."/services/apexrest/api/v1/converter");

	    $result = curl_exec($ch);
		$result_info = json_decode($result, true);

		curl_close($ch);

		return $result_info;
		
	}

}