<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/idx/sparkapi4p2/lib/Core.php';

class IDX_Auth extends MX_Controller {

	public $api;
	public $result;	

	public $access_token = "75kl13zqbt38c6xwich6n460z";
	public $refresh_token = "79xls3h0mbk5jilvy4p8p1942";
	
	function __construct() {

		parent::__construct();
		
		$GLOBALS['client_id']     = "a6b5t2xvveynb5ohmhohn4moc"; //"ahz16nuym5fzlk5gbnvbli04e a6b5t2xvveynb5ohmhohn4moc";
		$GLOBALS['client_secret'] = "c0ss5y0u9yi38ahn3c2oyurdv"; //"de76ykm90o521aecvlozd72e8 c0ss5y0u9yi38ahn3c2oyurdv";
		$GLOBALS['redirect_uri']  = AGENT_DASHBOARD_URL . "idx_login/idx_callback"; //"https://agentsquared.com/agent-site/idx_login/idx_callback";

		$this->api = new SparkAPI_Hybrid($GLOBALS['client_id'], $GLOBALS['client_secret'], $GLOBALS['redirect_uri']);
		
		// Let's identify our new application.
		$this->api->SetApplicationName("PHP-APP-In-Fifteen-Minutes/1.0");

		if(!empty($this->session->userdata('access_token')) && !empty($this->session->userdata('refresh_token'))) {
		    
	    	$this->access_token = $this->session->userdata('access_token');
	    	$this->refresh_token = $this->session->userdata('refresh_token');

		}
		else
		{
			if( empty( $this->api->oauth_access_token ) AND empty( $this->api->oauth_refresh_token ) )
	    	{
	    		$this->api->SetAccessToken($this->access_token);
	    		$this->api->SetRefreshToken($this->access_token);

	    	}
	    	else
	    	{
	    		$this->access_token = $this->api->oauth_access_token;
	    		$this->refresh_token = $this->api->oauth_refresh_token;

	    		$this->api->SetAccessToken($this->api->oauth_access_token);
	    		$this->api->SetRefreshToken($this->api->oauth_refresh_token);
	    	}

		} 
		
		$this->api->SetAccessToken($this->access_token);
	    $this->api->SetRefreshToken($this->refresh_token); 

	}
	
}
