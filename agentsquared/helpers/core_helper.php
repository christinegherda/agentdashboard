<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Returns the CodeIgniter object.
 *
 * Example: ci()->db->get('table');
 *
 * @return \CI_Controller
 */
function ci()
{
	return get_instance();
}

if( !function_exists('printA') )
{
	function printA($arr, $exit = false, $var_dump = false) {
		
		echo '<pre>';
		if($var_dump){
			var_dump($arr);
		}else{
			print_r($arr);
		}
		echo '</pre>';
		if($exit){
			exit();
		}
	}
}

if( !function_exists('get_idx_records') )
{
		
	function get_idx_records( $type = NULL) {

		if( isset($_SESSION["user_id"]) )
		{

			$idx = ci()->db->select("api_key, api_secret")
							->from("property_idx")
							->where("user_id", ci()->session->userdata("user_id") )
							->limit(1)
							->get()->row();



			if( !empty($idx) )
			{
				ci()->config->set_item('idx_api_key', $idx->api_key );
				ci()->config->set_item('idx_api_secret', $idx->api_secret );	

				return TRUE;		
			}
			else
			{
				return FALSE;
			}

		}
		return FALSE;
	}
}

if( !function_exists('generate_code') )
{
	function generate_key( $characters = "" ) {

		/* numbers, letters */
		//$possible = '1234567890987654321aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ';
		$possible = '1234567890987654321abcdefghijklmnopqrstuvwxyzyxwvutsrqponmlkjihgfedcba';
		$code = '';
		$i = 0;
		while ($i < $characters) { 
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		
		return date("Y").$code;
	}
}

if( !function_exists('isCustomerIdxActive') )
{
	function isCustomerIdxActive( $code = NULL ) {

		if( empty($code) ) 
		{
			return FALSE;
		}

		$code = ci()->db->select('code,id')
						->from('users')
						->where("code", $code)
						->limit(1)
						->get()->row();

		if( !empty($code) )
		{
			$idx = ci()->db->select("api_key, api_secret")
						->from("property_idx")
						->where("user_id", $code->id )
						->limit(1)
						->get()->row();

			if( !empty($idx) )
			{
				ci()->config->set_item('idx_api_key', $idx->api_key );
				ci()->config->set_item('idx_api_secret', $idx->api_secret );	

				return TRUE;		
			}
			else
			{
				return FALSE;
			}

		}
		
		return FALSE;

	}

}

if( !function_exists('str_replace_limit') )
{
	function str_replace_limit($search, $replace, $string, $limit = 1) {
        $pos = strpos($string, $search);

        if ($pos === false) {
          return $string;
        }

        $searchLen = strlen($search);

        for ($i = 0; $i < $limit; $i++) {
             $string = substr_replace($string, $replace, $pos, $searchLen);

            $pos = strpos($string, $search);

             if ($pos === false) {
             	break;
             }
        }

        return $string;
    }
}

if( !function_exists('str_limit') )
{
	function str_limit($str, $len = 100)
	{
	    if(strlen($str) < $len)
	    {
	        return $str;
	    }

	    $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

	    if(strlen($str) <= $len)
	    {
	        return $str;
	    }

	    $out = '';
	    foreach(explode(' ', trim($str)) as $val)
	    {
	        $out .= $val . ' ';

	        if(strlen($out) >= $len)
	        {
	            $out = trim($out);
	            return (strlen($out) == strlen($str)) ? $out : $out;
	        }
	    }
	}
}


if( !function_exists('save_activities') )
{
		
	function save_activities( $table_id = NUll , $activity_id = "", $activity = "" , $activity_type= "send_email", $user_type = "agent", $contact_id = "" ) {

		$save_activity["table_id"] = $table_id;
		$save_activity["activity_id"] = $activity_id;
		$save_activity["activity"] = $activity ;
		$save_activity["activity_type"] = $activity_type;
		$save_activity["user_type"] = $user_type;
		$save_activity["created"] = date("Y-m-d H:i:s");
		$save_activity["contact_id"] = $contact_id;
		$save_activity["agent_id"] = ci()->session->userdata("agent_id");

		ci()->db->insert("core_activities", $save_activity);

		return ( ci()->db->insert_id() ) ? ci()->db->insert_id() : FALSE;
	}
}

if( !function_exists('clean_url') ){

	function clean_url($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
}

if( !function_exists('display_ga_widget')){
  
  function display_ga_widget(){
    echo Modules::run('analytics/widget');
  }
}


if( !function_exists('is_freemium')){

	function is_freemium() {

	  $ci = ci();

	  $ci->db->select("subscriber_user_id,plan_id")
	        ->from("freemium")
	        ->where("subscriber_user_id", $ci->session->userdata("user_id"))
	        ->limit(1);

	  $result = $ci->db->get()->row();

	  return (!empty($result)) ? $result : FALSE;

	}
}

if( !function_exists('check_agent_ip_cookies')){

	function check_agent_ip_cookies() {

	  	$ci = ci();

	  	$ci->db->select('agent_ip.agent_id')
				->from('agent_ip')
				->where('agent_ip.ip_address', $ci->input->ip_address())
				->limit(1);

	  	$result = $ci->db->get()->row();

	  	if(!empty($result)) {

	  		$flag = FALSE;

	  		if(isset($_COOKIE['agent_ip_'.$result->agent_id])) {
	        	if(in_array($ci->input->ip_address(), $_COOKIE['agent_ip_'.$result->agent_id])) {
	        		$flag = TRUE;
	        	}
	        }

	        if($flag) {

		  		$ci->db->select('users.id, users.agent_id, users.email, users.code, users.created_on')
			  			->from('users')
			  			->where('users.agent_id', $result->agent_id)
			  			->limit(1);

		  		$agent_result = $ci->db->get()->row();

		  		if(!empty($agent_result)) {

			  		$ci->session->set_userdata(
				  		array(
							'user_id'		=> $agent_result->id,
							'agent_id'		=> $agent_result->agent_id,
							'email'	 		=> $agent_result->email,
							'code' 			=> $agent_result->code,
							'identity' 		=> $agent_result->email,
							'old_last_login'=> $agent_result->created_on,
							'logged_in_auth'=> true
						)
			  		);

			  		return TRUE;
				}

			}

	  	}

	  	return FALSE;

	}

}

function strip_protocol($url) {
    return str_replace(array("http://", "https://"), '', $url);
}

if( !function_exists('gunzip')){

  function gunzip($zipped) {

    $offset = 0;
    if (substr($zipped,0,2) == "\x1f\x8b")
       $offset = 2;
    if (substr($zipped,$offset,1) == "\x08")  {
       return gzinflate(substr($zipped, $offset + 8));
    }
    return "Unknown Format";

  }
}


function sendgrid_curl( $url, $params = array(), $is_post = true ) {
    $headers = array(
      'Content-Type: application/json',
      'Authorization: Bearer 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',
    );
    $params = json_encode($params, true);
	
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if ($is_post) {
    	curl_setopt($ch, CURLOPT_POST, true);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);

    $result = curl_exec($ch);
		
    return $result;
}

function sendgrid_curl_delete_rule_id( $url) {
    $headers = array(
      'Content-Type: application/json',
      'Authorization: Bearer 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',
    );
    	
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

    $result = curl_exec($ch);
	curl_close($ch);
    return $result;
}
