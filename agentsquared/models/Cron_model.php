<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_model extends CI_Model {

    public function __construct() {
        parent::__construct();

    }

    public function insert_cron_job_history($data) {
        return $this->db->insert('cron_jobs_history', $data);
    }

    public function get_idx_agents() {
        $data = $this->db->select('GROUP_CONCAT(DISTINCT pk_id) as pk_id')
            ->from('cron_jobs_history')
            ->where('type = "NEW_LISTING_SAVED_SEARCH_IDX_AGENT" AND DATE(NOW()) = DATE(run_on)')
            // ->group_by('pk_id')
            ->get()->row();
     

        $this->db->select('users.is_email_market, users.id as user_id, users.agent_id, users_idx_token.access_token, users_idx_token.refresh_token, domains.domains')
                ->from('users')
                ->join('agent_subscription', 'agent_subscription.UserId = users.agent_id', 'left')
                ->join('users_idx_token', 'users_idx_token.agent_id = users.agent_id', 'left')
                ->join('domains', 'domains.agent = users.id', 'left')
                ->where('users_idx_token.is_valid', 1)
                ->where('agent_subscription.ApplicationType','Agent_IDX_Website')
                ->where('agent_subscription.EventType','purchased')
                ->where('domains.type','agent_site_domain')
                ->group_by('user_id')
                ->limit(5);
        
        if ($data && $data->pk_id) {

            $this->db->where_not_in('users.id', explode(',', $data->pk_id));
        }

        $query = $this->db->get();

        return $query->result();

    }

    public function get_contacts($user_id) {

        $data = $this->db->select('GROUP_CONCAT(DISTINCT pk_id) as pk_id')
            ->from('cron_jobs_history')
            ->where('type = "NEW_LISTING_SAVED_SEARCH" AND DATE(NOW()) = DATE(run_on) AND user_id = '. $user_id)
            // ->group_by('pk_id')
            ->get()->row();

        
        
        $this->db->select('contacts.id as contact_id, contacts.email as contact_email, customer_searches.json_search, customer_searches.csid, contacts.email_schedule')
                ->from('contacts')
                ->join('customer_searches', 'customer_searches.customer_id = contacts.id')
                ->where('contacts.user_id', $user_id)
                ->where('customer_searches.json_search <>', '0')
                ->where('contacts.is_subscribe_email', 1)
                ->order_by('customer_searches.csid', 'desc')
                ->group_by('customer_searches.csid')
                ->limit(5);
        if ($data && $data->pk_id) {
            $csids = array();
       
            $this->db->where_not_in('customer_searches.csid', explode(',', $data->pk_id));
        }
    
        return  $this->db->get()->result();

    }

    public function save_listings($data=array()) {

        if($data) {

            $this->db->where("search_header", $data["search_header"]);
            $this->db->where("contact_id", $data["contact_id"]);
            $this->db->where("user_id", $data["user_id"]);
            $this->db->update("contact_searched_listings", $data);

            $res = ($this->db->affected_rows() > 0) ? TRUE : FALSE;

            if(!$res) {
                $this->db->insert('contact_searched_listings', $data);
            }
        }

    }

    public function get_saved_listings($schedule) {
        $where = '';
        if ($schedule == 'daily' || $schedule == 'hourly') {
            $where = 'AND DATE(NOW()) = DATE(run_on) AND email_schedule = "'. $schedule .'"';
        }elseif($schedule == 'weekly') {
            $where = 'AND YEARWEEK(NOW()) = YEARWEEK(run_on) AND email_schedule = "'. $schedule .'"';
        }else {
            $where = 'AND MONTH(NOW()) = MONTH(run_on) AND email_schedule = "'. $schedule .'"';
        }
        
        
        $data = $this->db->select('GROUP_CONCAT(DISTINCT pk_id) as pk_id')
            ->from('cron_jobs_history')
            ->where('type = "CONTACT_SEARCH_LISTINGS" '. $where)
            ->get()->row();


        $this->db->select("u.mls_name, u.first_name, u.last_name, u.logo, u.email as agent_email, u.id as agent_user_id, u.agent_id as agent_id, u.phone, u.agent_photo, d.domains, csl.search_header, csl.id as csl_id, csl.contact_id, csl.listings_json, c.email as customer_email, CONCAT(c.first_name ,' ', c.last_name) as contact_name, am.account, u.is_email_market, cs.json_search")
                ->from("contact_searched_listings csl")
                ->join("users u", "u.id = csl.user_id")
                ->join("domains d", "d.agent = u.id")
                ->join("contacts c", "c.id = csl.contact_id")
                ->join("customer_searches cs", "cs.customer_id = c.id", 'left')
                ->join("account_meta am", "am.user_id = u.id")
                ->where("c.is_subscribe_email", 1)
                ->where("c.email_schedule", $schedule)
                ->where("c.is_deleted", 0)
                ->where('u.is_email_market', 1)
                ->where("d.type", "agent_site_domain")
                 ->limit(5)
                 ->group_by('csl_id');
                 

        if ($data && $data->pk_id) {
            $this->db->where_not_in('contact_searched_listings.id', explode(',', $data->pk_id));
        }
        
        $query = $this->db->get();

        return $query->result();
    }

    public function get_account_meta($user_id=NULL) {

        $ret = FALSE;

        if($user_id) {
            $this->db->select("json_account_info")
                    ->from("account_mls_meta")
                    ->where("user_id", $user_id);

            $query = $this->db->get();

            if($query) {
                $ret = $query->row()->json_account_info;
            }
        }

        return $ret;
    }

    public function delete_searches($csl_id=NULL) {

        $ret = FALSE;

        if($csl_id) {
            $this->db->where('id', $csl_id);
            $this->db->delete('contact_searched_listings');
            $ret = TRUE;
        }

        return $ret;
    }

    public function get_token_info() {

        $this->db->select("*")
                ->from("users_idx_token");

        return $this->db->get()->result();
    }

    public function get_user_info() {

        $ret = FALSE;

        $this->db->select("CONCAT(u.first_name, ' ', u.last_name) as agent_name, u.ip_address, u.agent_id, u.email, a.ApplicationType, uit.access_token, uit.refresh_token")
                ->from("users_idx_token uit")
                ->join("agent_subscription a", "a.UserId = uit.agent_id", "left")
                ->join("users u", "u.agent_id = uit.agent_id", "left")
                ->where("uit.is_valid", 0)
                ->where("a.ApplicationType", "Agent_IDX_Website")
                ->where("a.EventType", "purchased");

        $ret = $this->db->get()->result();

        return $ret;
    }

    public function save_down_time_info($data=array()) {

        if($data) {
            
            $this->db->where("agent_id", $data["agent_id"]);
            $this->db->update("downtime_logs", $data);

            $res = ($this->db->affected_rows() > 0) ? TRUE : FALSE;

            if(!$res) {
                $this->db->insert("downtime_logs", $data);
            }

        }

    }

    public function fetch_domains() {

        $this->db->select("d.domains as domain_name, u.first_name, u.email")
                ->from("domains d")
                ->join("users u", "u.id = d.agent")
                ->join("agent_subscription as", "as.UserId = u.agent_id")
                ->where("d.type", "agent_site_domain")
                ->where("d.existing_domain", 1)
                ->where("d.status", "registered")
                ->where("as.EventType", "purchased")
                ->group_by("u.agent_id");

        return $this->db->get()->result();

    }

    public function fetch_agent_email($agent_id=NULL) {

        $ret = FALSE;

        if($agent_id) {

            $this->db->select("a.UserId, u.email, u.first_name, a.isFromSpark")
                    ->from("agent_subscription a")
                    ->join("users u", "u.agent_id = a.UserId")
                    ->where("a.EventType", "purchased")
                    ->where("a.UserId", $agent_id)
                    ->where("a.ApplicationType", "Agent_IDX_Website");

            $ret = $this->db->get()->row();
        }

        return $ret;
    }

    public function fetch_token_data() {

        $this->db->select("uit.access_token, uit.refresh_token, uit.oauth_key, u.first_name, u.last_name, u.email, as.isFromSpark")
                ->from("users_idx_token uit")
                ->join("users u", "u.agent_id=uit.agent_id", "inner")
                ->join("agent_subscription as", "as.UserId=uit.agent_id", "inner")
                ->join("domains dom", "dom.agent=u.id", "inner")
                ->where("uit.is_valid", 0)
                ->where("as.EventType", "purchased")
                ->where("as.ApplicationType", "Agent_IDX_Website")
                ->where("dom.is_subdomain", 0)
                ->where("dom.status", "completed")
                ->where("u.trial", 0)
                ->group_by("u.agent_id");

        return $this->db->get()->result();

    }

    public function fet_user_id($agent_id=NULL) {

        $ret = FALSE;

        if($agent_id) {
            $this->db->select("id")
                    ->from("users")
                    ->where("agent_id", $agent_id);

            $query = $this->db->get()->row();

            if($query) {
                $ret = $query->id;
            }

        }

        return $ret;
    }

    public function fetch_agent_token($agent_id=NULL) {

        $ret = FALSE;

        if($agent_id) {

            $this->db->select("*")
                    ->from("users_idx_token")
                    ->where("agent_id", $agent_id);


            $query = $this->db->get()->row();

            if($query) {
                $ret = $query;
            }
        }

        return $ret;
    }

    public function fetch_valid_access_token() {

        $this->db->select("uit.*, as.UserName, u.id as system_user_id")
                ->from("users_idx_token uit")
                ->join("agent_subscription as", "as.UserId = uit.agent_id", "left")
                ->join("users u", "u.agent_id = uit.agent_id", "left")
                ->where("uit.is_valid", 1)
                ->where("as.EventType", "purchased")
                ->group_by("uit.agent_id");

        $query = $this->db->get()->result();

        return ($query > 0) ? $query : FALSE;
    }

    public function fetch_bearer_token() {

        $this->db->select("ubt.*, as.UserName, u.id as system_user_id")
                ->from("user_bearer_token ubt")
                ->join("agent_subscription as", "as.UserId = ubt.agent_id", "left")
                ->join("users u", "u.agent_id = ubt.agent_id", "left")
                ->where("as.EventType", "purchased")
                ->group_by("ubt.agent_id");

        $query = $this->db->get()->result();

        return ($query > 0) ? $query : FALSE;
    }

    public function fetch_all_active_user_token() {

        $this->db->select("uit.*")
                ->from("users_idx_token uit")
                ->join("agent_subscription as", "as.UserId = uit.agent_id")
                ->where("as.EventType", "purchased")
                ->group_by("uit.agent_id");

        return $this->db->get()->result();
    }

    public function update_token($agent_id=NULL, $data=array()) {

        $this->db->where('agent_id', $agent_id);
        $this->db->update('users_idx_token', $data);

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }

    public function trial_is_over() {

        $this->db->select("u.id, u.first_name, u.last_name, u.email")
                ->from("users u")
                ->join("agent_subscription as", "as.UserId=u.agent_id", "left")
                ->where("as.EventType", "purchased")
                ->where("u.trial", 1)
                ->where("as.isFromSpark", 0)
                ->where("as.ApplicationType", "Agent_IDX_Website")
                ->where("u.date_signed <= (curdate() - INTERVAL 15 DAY)")
                ->where("u.isSent", 0)
                ->group_by("u.agent_id");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function expired_trial_sent($id=NULL) {

        $this->db->where('id', $id);
        $this->db->update('users', array('isSent'=>1));

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;

    }

    public function get_user_id($agent_id = ""){
        $this->db->select("id,first_name")
                ->from("users")
                ->where("agent_id",$agent_id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function get_new_signups($days=0) {

        $this->db->select("u.id, u.agent_id, u.first_name, u.last_name, u.email, u.code, d.domains")
                ->from("users u")
                ->join("domains d", "d.agent=u.id", "inner")
                ->where("date(u.date_signed) = (curdate() - INTERVAL ".$days." DAY)");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function get_new_hourly() {

        $this->db->select("u.id, u.agent_id, u.first_name, u.last_name, u.email, u.code, u.created_on, d.domains")
                ->from("users u")
                ->join("domains d", "d.agent=u.id", "inner")
                ->where("u.date_signed >= (NOW() - INTERVAL 1 HOUR)");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function get_user_info_post_email($id=NULL) {

        $this->db->select("id, agent_id, first_name, last_name, email, code, created_on, site_name")
                ->from("users")
                ->where("id", $id)
                ->limit(1);

        $query = $this->db->get()->row();

        return (empty($query)) ? FALSE : $query;

    }

    public function update_counter($agent_id=NULL, $data=array()) {

        $this->db->where('agent_id', $agent_id);
        $this->db->update('users_idx_token', $data);

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    }

    public function fetch_premium_valid_access_token() {

        $this->db->select("uit.*, as.UserName, u.id as system_user_id")
                ->from("users_idx_token uit")
                ->join("agent_subscription as", "as.UserId = uit.agent_id", "left")
                ->join("users u", "u.agent_id = uit.agent_id", "left")
                ->join("freemium f", "f.subscriber_user_id = u.id", "left")
                ->where("uit.is_valid", 1)
                ->where("as.EventType", "purchased")
                ->where("f.plan_id", 2)
                ->order_by("u.id","desc")
                ->group_by("uit.agent_id");

        $query = $this->db->get()->result();

        return ($query > 0) ? $query : FALSE;
    }

    public function fetch_free_agent_valid_access_token() {
        $isFreeAgent = array();
        $this->db->select("uit.*, as.UserName, u.id as system_user_id")
                ->from("users_idx_token uit")
                ->join("agent_subscription as", "as.UserId = uit.agent_id", "left")
                ->join("users u", "u.agent_id = uit.agent_id", "left")
                ->where("uit.is_valid", 1)
                ->where("as.EventType", "purchased")
                ->group_by("uit.agent_id");

        $query = $this->db->get()->result();

        if($query > 0) {
            foreach ($query as $value) {
                $isTrue = $this->isPremiumAgent($value->system_user_id);
                
                if(!$isTrue){
                    $isFreeAgent[] = $value;
                }
            }
            return $isFreeAgent;
        }

        return FALSE;
        // return ($query > 0) ? $query : FALSE;
    }

    private function isPremiumAgent($user_id = 0){
        $this->db->select("id,plan_id,subscriber_user_id")
                ->from("freemium")
                ->where("subscriber_user_id", $user_id)
                ->where("plan_id", 2)
                ->limit(1);

        $queryString = $this->db->get()->row(); 
        return (count($queryString) > 0) ? TRUE : FALSE;
    }
}