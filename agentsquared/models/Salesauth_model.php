<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Salesauth_model extends CI_Model {

    public function __construct() {
        parent::__construct();

    }

    public function is_agent($agent_id="" , $token=""){

        $this->db->select("id")
                ->from("users_idx_token")
                ->where(array("agent_id" =>$agent_id, "access_token" => $token))
                ->limit(1);
        $query =  $this->db->get();
        //return $this->db->get()->row();
        if( $query->num_rows() == 0)
        {
            $this->db->select("id")
                ->from("user_bearer_token")
                ->where(array("agent_id" =>$agent_id, "bearer_token" => $token))
                ->limit(1);

            return $this->db->get()->row();

        }

        return $query->row();

    }

    public function get_user_info($agent_id=""){

        $this->db->select("*")
                ->from("users")
                ->where("agent_id", $agent_id)
                ->limit(1);

        return $this->db->get()->row();
       
    }

    public function get_cancel_info($agent_id=NULL) {

        if(!empty($agent_id)) {

            $this->db->select('cancel_reason')
                    ->from('agent_cancelled')
                    ->where('agent_id', $agent_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            return (!empty($query)) ? $query : FALSE;

        }

        return FALSE;

    }

    public function get_all_sendgrid_rule_id() {
        $this->db->select("rule_id")
            ->from("whitelisted_ips")
            ->limit(100);

         return $this->db->get()->result();
    }

    public function remove_sendgrid_rule_id($rule_id)
    {
        $this->db->where('rule_id', $rule_id);
        $this->db->delete('whitelisted_ips');
    }

    public function authenticate_sendgrid_email($param = array()) {

        $query = $this->db->select('id, agent_id, email, last_login, code')
                            ->from('users')
                            ->where($param)
                            ->limit(1)
                            ->get()->row();

        return !empty($query) ? $query : FALSE;

    }
}