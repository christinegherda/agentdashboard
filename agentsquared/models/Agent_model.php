<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Agent_model extends CI_Model {

	public function __construct()
    {
        parent::__construct();
        
        $this->load->library('email');
    }
	// --------------------------------------

    public function save_property_status( $property_status = NULL, $property_ListingID = NULL){

        $update["property_status"] = $property_status;
        $this->db->where("ListingID", $property_ListingID );

        $this->db->update("property_activated", $update);

        return ( $this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }
    
	public function sync_old_agents()
    {
        $agents = $this->db->select("*")
                ->from("agent_subscription")
                ->get()->result();

        return $agents;
    }
	
	public function save_broker_infos( $broker = "", $broker_num = "" , $agent_id ="" )
	{
		$update["broker"] = $broker;
		$update["broker_number"] = $broker_num;
		
        $this->db->where("list_agentId", $agent_id );

        $this->db->update("property_activated", $update);

        return ( $this->db->affected_rows() > 0) ? TRUE : FALSE ;
	}

    public function check_agent_subscription($data = array()) {

        $ret = FALSE;

        if($data) {
            $this->db->select("a.ApplicationName, a.PricingPlan, a.UserEmail, a.UserId, a.UserName, u.id, u.first_name, u.last_name, u.phone, u.mobile, d.domains")
                ->from("agent_subscription a")
                ->join("users u", "u.agent_id=a.UserId", "left")
                ->join("domains d", "u.id=d.agent", "left")
                ->where($data)
                ->limit(1);

            $ret = $this->db->get()->row();
        }
        
        return $ret;
    }

    public function get_account_meta($data=array()) {

        $this->db->select("account")
                ->from("account_meta")
                ->where($data)
                ->limit(1);

        return $this->db->get()->row();
    }
    /*
    public function get_property($user_id=NULL, $type=NULL) {

        if($user_id && $type) {

            $this->db->select("listings")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    ->limit(1);

            $query = $this->db->get()->row();

            return ($query) ? $query : FALSE;
        }

    }
    */

    public function update_agent_subscription($arr = array(), $data = array()) {
        $this->db->where($arr);
        $this->db->update("agent_subscription", $data);
    }

    public function fetch_all_agent($counter = FALSE, $param = array()) {

        $this->db->select("u.id, u.agent_id, u.first_name, u.last_name, u.email, u.mls_name, u.trial, u.sync_status, u.date_signed, uit.access_token")
                ->from("users u")
                ->join("users_idx_token uit", "uit.agent_id=u.agent_id");


        if($this->input->get('type')) {

            $type = $this->input->get('type');

            if($type=="trial") {

                $this->db->where("u.trial", 1);

            } elseif($type=="paid") {

                $this->db->where("u.trial", 0);

            } 
        }

        if($this->input->get('keywords')) {

            $keywords = $this->input->get('keywords');
            $keyword_arr = explode(" ", $keywords);

            $this->db->where_in("u.first_name", $keyword_arr);
            $this->db->or_where("u.agent_id", $keywords);
            $this->db->or_like("u.first_name", $keywords);
            $this->db->or_like("u.last_name", $keywords);
            $this->db->or_like("u.mls_name", $keywords);
            $this->db->or_like("u.email", $keywords);

        }

        if($counter) {
            return $this->db->count_all_results();
        }

        if(!empty($param["limit"])) {
            $this->db->limit($param["limit"]);
        }

        if(!empty($param["offset"])) {
            $this->db->offset($param["offset"]);
        }

        $this->db->order_by("u.date_signed", "DESC");

        $query = $this->db->get()->result();

        return (!empty($query)) ? $query : FALSE;

    }

    public function removed_users_record($data=array()) {

        if($data) {

            //User ID
            $this->db->where("id", $data['user_id']);
            $this->db->delete("users");

            $this->db->where("user_id", $data['user_id']);
            $this->db->delete("account_meta");

            $this->db->where("user_id", $data['user_id']);
            $this->db->delete("homepage_properties");

            $this->db->where("user_id", $data['user_id']);
            $this->db->delete("pricing");

            $this->db->where("subscriber_user_id", $data['user_id']);
            $this->db->delete("freemium");

            $this->db->where("agent", $data['user_id']);
            $this->db->delete("domains");

            $this->db->where("user_id", $data['user_id']);
            $this->db->delete("leads_config");

            $this->db->where("author", $data['user_id']);
            $this->db->delete("media");

            $this->db->where("user_id", $data['user_id']);
            $this->db->delete("contact_searched_listings");

            $this->db->where("user_id", $data['user_id']);
            $this->db->delete("testimonials");
            
            //Agent ID
            $this->db->where("UserId", $data['agent_id']);
            $this->db->delete("agent_subscription");

            $this->db->where("agent_id", $data['agent_id']);
            $this->db->delete("pages");

            $this->db->where("agent_id", $data['agent_id']);
            $this->db->delete("contacts");

            $this->db->where("agent_id", $data['agent_id']);
            $this->db->delete("home_options");

            $this->db->where("agent_id", $data['agent_id']);
            $this->db->delete("core_activities");

            $this->db->where("list_agentId", $data['agent_id']);
            $this->db->delete("property_activated");

            $this->db->where("agent_id", $data['agent_id']);
            $this->db->delete("receipt");

            $this->db->where('agent_id', $data['agent_id']);
            $this->db->delete('agent_ip');
 
            //clear cookie for particular agent
            if(isset($_COOKIE['agent_ip_'.$data['agent_id']])) {
                
                $count = count($_COOKIE['agent_ip_'.$data['agent_id']]);

                for($i=0; $i<$count; $i++) {
                    setcookie($_COOKIE['agent_ip_'.$data['agent_id']][$i], '', time() - 3600);
                }

            }

            return TRUE;

        }  

        return FALSE;

    }

    public function auth_admin($data) {

        $ret = FALSE;

        if($data) {

            $this->db->select("password")
                    ->from("admin_users")
                    ->where("username", $data["username"])
                    ->limit(1);

            $query = $this->db->get()->row();

            if(!empty($query)) {

                if(password_verify($data["password"], $query->password)) {
                    $this->session->set_userdata('logged_in_auth_admin', TRUE);
                    $ret = TRUE;
                }

            }

        }

        return $ret;

    }

    public function getAgent($user_id=NULL){
        if(empty($user_id)) return false;

        $this->db->select("id,first_name,last_name")
                ->from("users")
                ->where("id", $user_id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function getCname($user_id=NULL){
        $this->db->select("id,domains,subdomain_url")
                ->from("domains")
                ->where("agent", $user_id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function agent_domain_data($agent_id) {

        if(!empty($agent_id)) {

            $this->db->select('u.id, u.first_name, u.email, d.domains')
                ->from('users u')
                ->join('domains d', 'u.id=d.agent', 'left')
                ->where('u.id', $agent_id)
                ->where('d.type', 'agent_site_domain')
                ->limit(1);

            $query = $this->db->get()->row();

            return (!empty($query)) ? $query : FALSE;
            
        }

        return FALSE;
    }

    /**
    * Self explainatory
    * @author Jovanni G
    */

    public function getAgentByEmail($email = null){

        if($email != null){

            $this->db->where('email', $email);
            $query = $this->db->get('users')->row();

            if(!empty($query)){
                return $query;
            }
        }
        return false;
    }

    /**
    * Self explainatory
    * @author Jovanni G
    */

    public function setFreemium($user_id = null){

        if($user_id != null){

            $this->db->where('subscriber_user_id', $user_id);
            
            return $this->db->update('freemium', array( 'plan_id' => 1 ));
        }
        return false;
    }


    /**
    * Self explainatory
    * @author Jovanni G
    */
    public function setPremium($user_id = null, $data = array()){

        if($user_id !== null){

            $this->db->where('subscriber_user_id', $user_id);

            $data['plan_id'] = 2;
            
            return $this->db->update('freemium', $data);
        }

        return false;
    }

    /**
    * Update the column `require_cc_update` to true.
    * @author Jovanni G
    */
    public function requireUpdateCC($user_id = null, $value = 1){

        $this->updateColumn();

        if($user_id !== null){

            $this->db->where('subscriber_user_id', $user_id);

            return $this->db->update('freemium', array(
                'require_cc_update' => $value,
            ));
        }
        return false;
    }

    /**
     * Checks if dashboard user requires CC update and will create a column field `require_cc_update` if it does not exist.
     * @author Jovanni G
     * @param null|mixed $user_id
     * @return bool
     */
    public function isRequireUpdateCC($user_id = null)
    {
        if ($user_id !== null)
        {
            $query = $this->db
                ->select('require_cc_update')
                ->where('subscriber_user_id', $user_id)
                ->limit(1)
                ->get('freemium')
                ->row();

            return ( $query && null !== $query->require_cc_update && 1 === (int)$query->require_cc_update );
        }

        return false;
    }

    /**
     * Self explainatory
     * @author Jovanni G
     * @param null $user_id
     * @return bool
     */
    public function getStripeCustID($user_id = null){

        if($user_id !== null)
        {
            $query = $this->db
                ->where('agent_id', $user_id)
                ->select('customer')
                ->order_by('created_on', 'DESC')
                ->limit(1)
                ->get('receipt')
                ->row();

            if( !empty($query) && null !== $query->customer )
            {
                return $query->customer;
            }
        }

        return false;
    }

    /**
    * Add new columns here.
    * @author Jovanni G
    */
    private function updateColumn(){

        if(!$this->db->field_exists('require_cc_update', 'freemium')){
            $this->load->dbforge();

            $this->dbforge->add_column('freemium', array(
                'require_cc_update' => array(
                    'type' => 'BOOLEAN',
                    'default' => 0,
                    'after' => 'subscriber_user_id'
                ),
            ));
        }
    }

    public function downGradeSubscription($subs_id=NULL) {

        if(!empty($subs_id)) {

            $query_receipt = $this->db->select('agent_id')
                                    ->from('receipt')
                                    ->where('sub_id', $subs_id)
                                    ->limit(1)
                                    ->get()->row();

            if(!empty($query_receipt)) {
                    
                $domain_query = $this->db->select('subdomain_url')
                                        ->from('domains')
                                        ->where('agent', $query_receipt->agent_id)
                                        ->limit(1)
                                        ->get()->row();

                if(!empty($domain_query) && isset($domain_query->subdomain_url)) {

                    $domain = preg_replace('#^https?://#', '', $domain_query->subdomain_url);

                    $this->db->where('agent', $query_receipt->agent_id);
                    $this->db->update('domains', array('domains'=>$domain, 'status'=>'pending', 'is_ssl'=>0));

                    if($this->db->affected_rows()) {

                        $this->db->where('subscriber_user_id', $query_receipt->agent_id);
                        $this->db->update('freemium', array('plan_id'=>1));

                        $this->db->where('agent_id', $query_receipt->agent_id);
                        $this->db->update('receipt', array('agent_id'=>'100000'.$query_receipt->agent_id));

                    }

                }

                return $query_receipt->agent_id;

            }

        }

        return FALSE;

    }

    /**
     * Upgrade user's subscription data
     * 
     *  Set trial column to 0, Set freemium to 2
     *
     * @param  array $_POST data
     * @return view redirect to agentsite
     */
    public function upgradeSubscription(String $sub_id) {

        $ret = FALSE;

        if(!empty($sub_id)) {

            $query = $this->db->select('agent_id')
                            ->from('receipt')
                            ->where('sub_id', $sub_id)
                            ->limit(1)->get()->row();

            if(!empty($query->agent_id)) {

                $this->db->where('id', $query->agent_id);
                $this->db->update('users', array('trial'=>0));

                $ret = $this->db->affected_rows() ? TRUE : FALSE;

            }

        }

        return $ret;

    }

    /**
     * Fetch agent's data similar with the fresh onboard data
     *
     * @param  Int  $user_id
     * @return  Array of object
     */
    public function fetchOnBoardData(Int $user_id) {

        if(!$user_id)
            return FALSE;

        return $this->db->select(
                    'u.agent_id, u.first_name, u.last_name, u.email, u.mls_name, u.phone, u.company, u.date_signed, u.code,
                    uit.access_token,
                    d.domains, d.subdomain_url,
                    p.price, p.plan, p.type,
                    as.LaunchFrom,
                    ac.cancel_reason, ac.cancel_date,
                    r.created_on, r.rid,
                    als.total_sold, als.total_active, als.recent_sold_date, als.recent_active_date')
                ->from('users u')
                ->join('users_idx_token uit', 'uit.agent_id=u.agent_id', 'left')
                ->join('agent_subscription as', 'as.UserId=u.agent_id')
                ->join('domains d', 'd.agent=u.id')
                ->join('pricing p', 'p.user_id=u.id', 'left')
                ->join('agent_cancelled ac', 'ac.user_id=u.id', 'left')
                ->join('receipt r', 'r.agent_id=u.id', 'left')
                ->join('agent_listings_scale als', 'als.agent_id=u.agent_id', 'left')
                ->where('u.id', $user_id)
                ->get()->row();

    }

    /**
     * Fetch agent's data similar with the fresh onboard data
     *
     * @param  String  $subs_id
     * @return  Array of object
     */
    public function expiredCCData($email=NULL) {

        if($email) {

            $data = $this->db->select('id, agent_id, first_name, email, code')
                            ->from('users')
                            ->where('email', $email)
                            ->limit(1)
                            ->get()->row();
            
            if(!empty($data)) {

                $token_string = $data->code.' '.$data->agent_id.' '.$data->id;
                $token = base64_encode($token_string);

                //Send Email Through Sendgrid
                $this->email->send_email_v3(
                    'support@agentsquared.com', //sender email
                    'AgentSquared', //sender name
                    $data->email, //recipient email
                    'paul@agentsquared.com,joce@agentsquared.com,honradokarljohn@gmail.com', //bccs
                    '9749fca1-a73f-48ce-af8e-ae0521f728ee', // required cc template_id
                    array(
                        'subs' => array(
                            'first_name' => $data->first_name,
                            'update_cc_url' => AGENT_DASHBOARD_URL.'sales_auth/login_update_cc?token='.$token
                        )
                    )
                );

            }

        }

    }

}