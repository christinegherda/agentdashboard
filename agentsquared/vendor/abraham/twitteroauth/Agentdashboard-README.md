This library is modified to suit in the `Agentdashboard` use case.

List of bugs and it's fixes:
- `\Abraham\TwitterOAuth\TwitterOAuth::uploadMediaNotChunked()`
  - In `uploadMediaNotChunked()` method, the `is_readable()` does not work in with the image URL's. A work around is added to this method to fixed the issue.
  
- `\Abraham\TwitterOAuth\TwitterOAuth::mediaInitParameters()`
  - In `mediaInitParameters()` method `filesize()` does not work with image URL's. A work around is provided to get the image size by the supplied image URL.

---

Un-resolve bugs:
- Upload media using chunk throws a `PHP Notice Array to String conversion`.
