<?php
/**
 * Manual autoload libraries using composer
 */

$loader = require dirname(__DIR__, 2) . '/vendor/autoload.php';

/**
 * autoload libraries using PSR-4
 */
$loader->addPsr4('LinkedIn\\', __DIR__ .'/linkedin-api-php-client/src');
$loader->addPsr4('Abraham\\TwitterOAuth\\', __DIR__ .'/abraham/twitteroauth/src');
