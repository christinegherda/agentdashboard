# Agent Dashboard

## Contributing
Follow our [Git General Guidelines](https://github.com/Gromby/cicd/wiki/Git-General-Guidelines)    

## Release Information
See the file "version" for the latest production release

## Installation
Read the [cicd](https://github.com/Gromby/cicd) usage guide
